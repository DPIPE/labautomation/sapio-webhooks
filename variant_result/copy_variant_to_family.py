import traceback

from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult

from utilities.data_models import VariantResultModel, FamilyVariantModel, SampleModel, PatientModel, FamilyModel
from utilities.webhook_handler import OsloWebhookHandler


class CopyVariantResultToFamily(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            result: ClientCallbackResult = context.client_callback_result
            if result:
                if isinstance(result, OptionDialogResult):
                    if result.button_text is None or result.button_text == 'OK':
                        return SapioWebhookResult(True)
                if result.user_cancelled:
                    return SapioWebhookResult(True)

            # Get the current variant result record
            variant_result: VariantResultModel = self.inst_man.add_existing_record_of_type(context.data_record,
                                                                                           VariantResultModel)

            # Get the parent sample record
            self.rel_man.load_parents_of_type([variant_result], SampleModel)
            parent_sample = variant_result.get_parent_of_type(SampleModel)
            if not parent_sample:
                raise Exception("Variant result is not linked to a parent sample")

            # Get the patient record that is an ancestor of the sample
            self.an_man.load_ancestors_of_type([parent_sample.backing_model], PatientModel.DATA_TYPE_NAME.__str__())
            patients = self.an_man.get_ancestors_of_type(parent_sample.backing_model, 'Patient')
            if not patients:
                raise Exception("Sample linked to this variant result doesn't have an Patient in the hierarchy of records")
            patient = self.inst_man.wrap(patients.pop(), PatientModel)

            # Get the families that the patient is related to
            self.rel_man.load_parents_of_type([patient], FamilyModel)
            family_models = patient.get_parents_of_type(FamilyModel)
            if not family_models:
                raise Exception(
                    "Patient record related to this variant result is not linked to a family")

            # Create a new family variant for each family
            for family_model in family_models:
                self.create_and_populate_family_variant(family_model, variant_result)
            self.rec_man.store_and_commit()

            return SapioWebhookResult(True, display_text="Copied Variant to the family(s) under related patient.")
        except Exception as exception:
            self.logger.error(
                "Error ({user:s}):\n{trc:s}".format(user=context.user.username, trc=traceback.format_exc()))
            return PopupUtil.display_ok_popup("ERROR", str(exception))

    def create_and_populate_family_variant(self, family_model: FamilyModel, variant_result: VariantResultModel):
        family_variant_model = self.inst_man.add_new_record_of_type(FamilyVariantModel)

        # Copy relevant fields from the variant result to the family variant
        family_variant_model.set_VariantName_field(variant_result.get_Variant_field())
        family_variant_model.set_Classification_field(variant_result.get_Classification_field())

        family_model.add_child(family_variant_model)
