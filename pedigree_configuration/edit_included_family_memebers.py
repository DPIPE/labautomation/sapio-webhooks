from typing import List, cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import RawReportTerm, RawTermOperation, ReportColumn, CustomReportCriteria
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.DataType import DataTypeDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType, AbstractVeloxFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import TempTableSelectionRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import ListDialogResult, DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import CallbackType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PedigreeConfigurationModel, FamilyModel, PedigreeNodeModel
from utilities.webhook_handler import OsloWebhookHandler


class EditIncludedFamilyMembers(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        display_fields =[PedigreeNodeModel.FIRSTNAME__FIELD_NAME.field_name, PedigreeNodeModel.LASTNAME__FIELD_NAME.field_name,
         "RecordId", PedigreeNodeModel.FAMILYID__FIELD_NAME.field_name, PedigreeNodeModel.SEX__FIELD_NAME.field_name,
         PedigreeNodeModel.ISADOPTED__FIELD_NAME.field_name, PedigreeNodeModel.ISDECEASED__FIELD_NAME.field_name,
         PedigreeNodeModel.ISDECEASED__FIELD_NAME.field_name]

        # get pedigree configuration
        pedigree_configuration_record: DataRecord = context.data_record
        pedigree_configuration: PedigreeConfigurationModel = self.inst_man.add_existing_records_of_type(
            [pedigree_configuration_record], PedigreeConfigurationModel).pop()

        if not context.client_callback_result:
            # get the family ID
            family_record_id = pedigree_configuration.get_FamilyID_field()
            if not family_record_id:
                return PopupUtil.ok_popup("Error", "No Family Record Associated With The Pedigree Configuration")
            family_record: DataRecord = self.dr_man.query_data_records_by_id(FamilyModel.DATA_TYPE_NAME,
                                                                             [family_record_id]).result_list.pop()
            family: FamilyModel = self.inst_man.add_existing_records_of_type([family_record], FamilyModel).pop()

            # get all pedigree records already added
            included_family_members = pedigree_configuration.get_IncludedFamilyMembers_field()
            if included_family_members:
                included_family_members = included_family_members.split(",")
                included_family_members = [int(included_family_member) for included_family_member in
                                           included_family_members]
            else:
                included_family_members = []

            # retrieve all pedigree node records
            pedigree_node_record_ids: [] = self.__get_pedigree_node_record_ids_for_the_family_id(
                family.get_FamilyId_field())
            if not pedigree_node_record_ids:
                return PopupUtil.ok_popup("Error", "No Pedigree Nodes Associated With The Family Id")
            pedigree_node_record_ids = [item for item in pedigree_node_record_ids if item not in included_family_members]

            pedigree_node_records: List[DataRecord] = self.dr_man.query_data_records_by_id(PedigreeNodeModel
                                                                                           .DATA_TYPE_NAME,
                                                                                           pedigree_node_record_ids)\
                .result_list
            if not pedigree_node_records:
                return PopupUtil.ok_popup("Error", "There are no eligible Pedigree Nodes that can be added to"
                                                   " configuration.", request_context="Add")

            # show user the data selection record pop up
            return PopupUtil.record_selection_popup(context, "Select Records To Add", display_fields,
                                                    pedigree_node_records,  multi_select=True, request_context="Add")

        elif context.client_callback_result.user_cancelled:
            return SapioWebhookResult(True)

        elif context.client_callback_result.callback_context_data == "Add":
            # get all pedigree records already added
            included_family_members = pedigree_configuration.get_IncludedFamilyMembers_field()
            included_family_member_records: List[DataRecord] = []
            if included_family_members:
                included_family_members = included_family_members.split(",")
                included_family_members = [int(included_family_member) for included_family_member in
                                           included_family_members]
                included_family_member_records = self.dr_man.query_data_records_by_id(
                    PedigreeNodeModel.DATA_TYPE_NAME, included_family_members).result_list

            # add records selected
            if context.client_callback_result.get_callback_type() == CallbackType.DATA_RECORD_SELECTION:
                result: DataRecordSelectionResult = cast(DataRecordSelectionResult, context.client_callback_result)
                records = result.selected_field_map_list
            else:
                records = []

            record_ids = []
            for record in records:
                record_ids.append(str(record.get("RecordId")))
            records_to_add = pedigree_configuration.get_IncludedFamilyMembers_field()
            if record_ids:
                records_to_add = ",".join(record_ids)
                if pedigree_configuration.get_IncludedFamilyMembers_field():
                    records_to_add = pedigree_configuration.get_IncludedFamilyMembers_field() + "," + records_to_add
                pedigree_configuration.set_IncludedFamilyMembers_field(records_to_add)
                self.rec_man.store_and_commit()

            if not included_family_member_records:
                return SapioWebhookResult(True)
            else:
                return PopupUtil.record_selection_popup(context, "Select Records To Remove", display_fields,
                                                        included_family_member_records, multi_select=True,
                                                        request_context=records_to_add)

        elif context.client_callback_result.callback_context_data != "Add":
            if context.client_callback_result.get_callback_type() == CallbackType.DATA_RECORD_SELECTION:
                result: DataRecordSelectionResult = cast(DataRecordSelectionResult, context.client_callback_result)
                records = result.selected_field_map_list
            else:
                records = []
            record_ids = []
            for record in records:
                record_ids.append(str(record.get("RecordId")))

            if record_ids:
                existing_records = context.client_callback_result.callback_context_data.split(",")
                for record_id in record_ids:
                    existing_records.remove(record_id)
                pedigree_configuration.set_IncludedFamilyMembers_field(",".join(existing_records))
                self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def __get_pedigree_node_record_ids_for_the_family_id(self, family_id) -> []:
        term = RawReportTerm(
            PedigreeNodeModel.DATA_TYPE_NAME,
            PedigreeNodeModel.FAMILYID__FIELD_NAME.field_name,
            RawTermOperation.EQUAL_TO_OPERATOR,
            family_id,
            is_negated=False,
        )
        column_list = [ReportColumn(PedigreeNodeModel.DATA_TYPE_NAME, "RecordId", FieldType.LONG)]
        custom_report_manager = DataMgmtServer.get_custom_report_manager(self.context.user)
        request = CustomReportCriteria(column_list, term)
        report = custom_report_manager.run_custom_report(request)
        record_ids = []
        for row in report.result_table:
            record_ids.append(row[0])
        return record_ids