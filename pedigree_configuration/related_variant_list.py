from typing import List

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import RawReportTerm, RawTermOperation, ReportColumn, CustomReportCriteria
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PedigreeConfigurationModel, FamilyModel, PedigreeNodeModel, PatientModel, \
    VariantResultModel
from utilities.webhook_handler import OsloWebhookHandler


class RelatedVariantList(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get pedigree configuration
        pedigree_configuration_record: DataRecord = context.data_record
        pedigree_configuration: PedigreeConfigurationModel = self.inst_man.add_existing_records_of_type(
            [pedigree_configuration_record], PedigreeConfigurationModel).pop()

        # get the family ID
        family_record_id = pedigree_configuration.get_FamilyID_field()
        if not family_record_id:
            return SapioWebhookResult(True, list_values=[])
        family_record: DataRecord = self.dr_man.query_data_records_by_id(FamilyModel.DATA_TYPE_NAME,
                                                                         [family_record_id]).result_list.pop()
        family: FamilyModel = self.inst_man.add_existing_records_of_type([family_record], FamilyModel).pop()

        # retrieve all pedigree node records
        pedigree_node_record_ids: [] = self.__get_pedigree_node_record_ids_for_the_family_id(
            family.get_FamilyId_field())
        if not pedigree_node_record_ids:
            return SapioWebhookResult(True, list_values=[])
        pedigree_node_records: List[DataRecord] = self.dr_man.query_data_records_by_id(PedigreeNodeModel
                                                                                       .DATA_TYPE_NAME,
                                                                                       pedigree_node_record_ids) \
            .result_list
        pedigree_nodes: List[PedigreeNodeModel] = self.inst_man.add_existing_records_of_type(pedigree_node_records,
                                                                                             PedigreeNodeModel)

        # get related patient records
        patient_record_ids = set()
        for pedigree_node in pedigree_nodes:
            if pedigree_node.get_Patient_field():
                patient_record_ids.add(pedigree_node.get_Patient_field())
        if not patient_record_ids:
            return SapioWebhookResult(True, list_values=[])
        patient_records = self.dr_man.query_data_records_by_id(PatientModel.DATA_TYPE_NAME,
                                                               list(patient_record_ids)).result_list
        patients = self.inst_man.add_existing_records_of_type(patient_records, PatientModel)

        # get all descendant variant results
        self.an_man.load_descendant_of_type([patient.backing_model for patient in patients],
                                            VariantResultModel.DATA_TYPE_NAME)
        variant_results = set()
        for patient in patients:
            curr_variant_results = self.an_man.get_descendant_of_type(patient.backing_model,
                                                                      VariantResultModel.DATA_TYPE_NAME)
            for variant_result in curr_variant_results:
                variant_results.add(self.inst_man.wrap(variant_result, VariantResultModel))

        # get the list of all variant results
        results = set()
        for variant_result in variant_results:
            if variant_result.get_Variant_field():
                results.add(variant_result.get_Variant_field())
        return SapioWebhookResult(True, list_values=list(results))

    def __get_pedigree_node_record_ids_for_the_family_id(self, family_id) -> []:
        term = RawReportTerm(
            PedigreeNodeModel.DATA_TYPE_NAME,
            PedigreeNodeModel.FAMILYID__FIELD_NAME.field_name,
            RawTermOperation.EQUAL_TO_OPERATOR,
            family_id,
            is_negated=False,
        )
        column_list = [ReportColumn(PedigreeNodeModel.DATA_TYPE_NAME, "RecordId", FieldType.LONG)]
        custom_report_manager = DataMgmtServer.get_custom_report_manager(self.context.user)
        request = CustomReportCriteria(column_list, term)
        report = custom_report_manager.run_custom_report(request)
        record_ids = []
        for row in report.result_table:
            record_ids.append(row[0])
        return record_ids
