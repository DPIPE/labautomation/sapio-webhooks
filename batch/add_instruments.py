from sapiopylib.rest.pojo.CustomReport import CustomReportCriteria, ReportColumn, RawReportTerm, RawTermOperation
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType, VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import SearchType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import BatchModel, InstrumentModel
from utilities.webhook_handler import OsloWebhookHandler


class AddInstruments(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get all the instruments currently under the batch.
        batch: BatchModel = self.rec_handler.wrap_model(context.data_record, BatchModel)
        self.rel_man.load_children_of_type([batch], InstrumentModel)
        instruments: list[InstrumentModel] | None = batch.get_children_of_type(InstrumentModel)

        # Prompt the user to select an instrument registered in the system.
        columns: list[ReportColumn] = [
            ReportColumn(InstrumentModel.DATA_TYPE_NAME, InstrumentModel.INSTRUMENTTYPE__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(InstrumentModel.DATA_TYPE_NAME, InstrumentModel.INSTRUMENTNAME__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(InstrumentModel.DATA_TYPE_NAME, InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name,
                         FieldType.STRING)]
        term: RawReportTerm = (
            RawReportTerm(InstrumentModel.DATA_TYPE_NAME, InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name,
                          RawTermOperation.NOT_EQUAL_TO_OPERATOR, ""))
        selected_instruments: list[InstrumentModel] | None = (
            self.callback.input_selection_dialog(InstrumentModel, "Select instruments to track.", True, False,
                                                 [SearchType.QUICK_SEARCH], None,
                                                 CustomReportCriteria(columns, term)))
        if not selected_instruments:
            return SapioWebhookResult(True)

        # Only add reagents selected by the user that aren't already under the batch.
        warning_table_data: list[dict[str, str]] = []
        for instrument in selected_instruments:
            try:
                matching_instrument: InstrumentModel = \
                    [x for x in instruments if x.get_SerialNumber_field() == instrument.get_SerialNumber_field()
                     and x.get_InstrumentName_field() == instrument.get_InstrumentName_field()
                     and x.get_InstrumentType_field() == instrument.get_InstrumentType_field()][0]
                warning_table_data.append({
                    "Instrument": matching_instrument.get_InstrumentName_field(),
                    "SerialNumber": matching_instrument.get_SerialNumber_field()
                })
            except Exception:
                batch.add_child(instrument)

        self.rec_man.store_and_commit()

        # For any reagents selected that were already under the batch, show a table of them to the user.
        if warning_table_data:
            self.callback.table_dialog("Warning", "The following instruments were selected but are already being"
                                                  " tracked under this batch.", [
                                           VeloxStringFieldDefinition("Instrument", "Instrument", "Instrument"),
                                           VeloxStringFieldDefinition("SerialNumber", "SerialNumber", "Serial Number")
                                       ], warning_table_data)

        return SapioWebhookResult(True)
