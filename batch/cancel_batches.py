from typing import Any

from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import BatchModel
from utilities.webhook_handler import OsloWebhookHandler


class CancelBatches(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get the batches the user selected and create table rows with them.
        batches: list[BatchModel] = self.rec_handler.wrap_models(context.data_record_list, BatchModel)
        rows: list[dict[str, Any]] = [{"BatchId": x.get_BatchId_field()} for x in batches]

        # Prompt a confirmation message to the user.
        result: list[dict[str, Any]] | None = (
            self.callback.table_dialog("Cancel Batches",
                                       "The following batches will be cancelled. Are you sure you want to continue?"
                                       " Close this dialog to cancel the operation.",
                                       [VeloxStringFieldDefinition("BatchId", "BatchId", "Batch ID")], rows))
        if not result:
            return SapioWebhookResult(True)

        # Cancel the batches if the user confirms.
        for batch in batches:
            batch.set_ExemplarBatchStatus_field("Cancelled")

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
