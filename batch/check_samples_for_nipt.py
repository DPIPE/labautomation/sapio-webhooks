import re

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from sdms.nipt_file_handlers import NIPTBatchInititationReportFileHandler, NIPTLibraryReagentReportHandler, \
    NIPTInstrumentFileHandler
from utilities import utils
from utilities.constants import Constants
from utilities.data_models import BatchModel, AttachmentModel, RequestModel, SampleModel
from utilities.instrument_utils import InstrumentUtils
from utilities.webhook_handler import OsloWebhookHandler


class CheckSamplesForNIPT(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get the file from the batch.
        batch: BatchModel = self.rec_handler.wrap_model(context.data_record, BatchModel)
        self.rel_man.load_children_of_type([batch], AttachmentModel)
        attachments: list[AttachmentModel] | None = batch.get_children_of_type(AttachmentModel)
        if not attachments:
            self.callback.ok_dialog("Error", "No files have been uploaded yet to this batch.")
            return SapioWebhookResult(True)

        # Go through each of the files and run their respective checks.
        init_pattern = re.compile(Constants.RE_BATCH_INIT)
        lib_reagent_pattern = re.compile(Constants.RE_BATCH_LIB_REAGENT)
        process_log_pattern = re.compile(Constants.RE_BATCH_LIB_PROCESS_LOG)
        sequencing_report_pattern = re.compile(Constants.RE_BATCH_SEQ_REPORT)
        try:
            init_file: AttachmentModel = [x for x in attachments if re.match(init_pattern, x.get_FilePath_field())][0]
            self.init_check(batch, init_file)
        except Exception:
            ...
        try:
            lib_reagent_files: list[AttachmentModel] = \
                [x for x in attachments if re.match(lib_reagent_pattern, x.get_FilePath_field())]
            for lib_reagent_file in lib_reagent_files:
                self.lib_reagent_check(batch, lib_reagent_file)
        except Exception:
            ...
        try:
            process_log_file: AttachmentModel = \
                [x for x in attachments if re.match(process_log_pattern, x.get_FilePath_field())][0]
            self.instrument_file_check(batch, process_log_file, True)
        except Exception:
            ...
        try:
            sequecning_report_file: AttachmentModel = \
                [x for x in attachments if re.match(sequencing_report_pattern, x.get_FilePath_field())][0]
            self.instrument_file_check(batch, sequecning_report_file, False)
        except Exception:
            ...

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def init_check(self, batch: BatchModel, init_file: AttachmentModel) -> None:
        file_bytes: bytes = AttachmentUtil.get_attachment_bytes(self.context, init_file)

        # Check to make sure the batch doesn't already have a sample sheet generated for it.
        if "Sample sheet generated" in batch.get_ExemplarBatchStatus_field():
            raise Exception

        # Check the samples and ensure that all are present. If they aren't all in the system, then don't generate the
        # sample sheet.
        samples_present, samples = NIPTBatchInititationReportFileHandler.check_and_add_samples(self.rec_handler,
                                                                                               self.rel_man,
                                                                                               init_file.get_FilePath_field(),
                                                                                               file_bytes,
                                                                                               batch)

        # Check to make sure all the orders for the samples are approved. If they are, then generate the sample sheet.
        # Otherwise, log errors to the batch.
        orders_to_samples: dict[RequestModel, list[SampleModel]] = utils.map_orders_to_samples(self.rel_man, samples)
        not_all_orders_approved: bool = (
            NIPTBatchInititationReportFileHandler.check_order_approvals(self, batch, orders_to_samples,
                                                                        init_file.get_FilePath_field()))

        # Don't generate the sample sheet if not all samples are present or if not all orders are approved
        if not samples_present or not_all_orders_approved:
            raise Exception

        # Generate the sample sheet and add it as an attachment to the batch.
        NIPTBatchInititationReportFileHandler.generate_sample_sheet(self, self.context, batch, orders_to_samples)

    def lib_reagent_check(self, batch: BatchModel, lib_reagent_file: AttachmentModel) -> None:
        file_bytes: bytes = AttachmentUtil.get_attachment_bytes(self.context, lib_reagent_file)

        # Check to see if any reagents that were previously missing have been added to the system.
        NIPTLibraryReagentReportHandler.check_and_add_reagents(self.context, self.rec_handler, self.rel_man, batch,
                                                               InstrumentUtils(self.context),
                                                               lib_reagent_file.get_FilePath_field(), file_bytes)

    def instrument_file_check(self, batch: BatchModel, process_log_file: AttachmentModel,
                              contains_hamilton: bool) -> None:
        file_bytes: bytes = AttachmentUtil.get_attachment_bytes(self.context, process_log_file)

        # Check to see if any reagents that were previously missing have been added to the system.
        NIPTInstrumentFileHandler.check_instruments(self.rec_handler, self.rel_man, batch,
                                                    process_log_file.get_FilePath_field(), file_bytes,
                                                    contains_hamilton)
