from sapiopylib.rest.pojo.CustomReport import CustomReportCriteria, ReportColumn, RawReportTerm, RawTermOperation
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType, VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import SearchType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import BatchModel, TrackedReagentModel, ConsumableItemModel
from utilities.webhook_handler import OsloWebhookHandler


class AddReagents(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get all the reagents currently under the batch.
        batch: BatchModel = self.rec_handler.wrap_model(context.data_record, BatchModel)
        self.rel_man.load_children_of_type([batch], TrackedReagentModel)
        tracked_reagents: list[TrackedReagentModel] | None = batch.get_children_of_type(TrackedReagentModel)

        # Prompt the user to select a reagent registered in the system.
        columns: list[ReportColumn] = [
            ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, ConsumableItemModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, ConsumableItemModel.PARTNUMBER__FIELD_NAME.field_name,
                         FieldType.STRING)]
        term: RawReportTerm = (
            RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME, ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                          RawTermOperation.NOT_EQUAL_TO_OPERATOR, "<ENTER LOT NUMBER HERE>"))
        selected_reagents: list[ConsumableItemModel] | None = (
            self.callback.input_selection_dialog(ConsumableItemModel, "Select reagents to track.", True, False,
                                                 [SearchType.QUICK_SEARCH], None,
                                                 CustomReportCriteria(columns, term)))
        if not selected_reagents:
            return SapioWebhookResult(True)

        # Only add reagents selected by the user that aren't already under the batch.
        warning_table_data: list[dict[str, str]] = []
        for reagent in selected_reagents:
            try:
                tracked_reagent: TrackedReagentModel = \
                    [x for x in tracked_reagents if x.get_ReagentType_field() == reagent.get_ConsumableType_field()
                     and x.get_LotNumber_field() == reagent.get_LotNumber_field()][0]
                warning_table_data.append({
                    "ReagentType": tracked_reagent.get_ReagentType_field(),
                    "LotNumber": tracked_reagent.get_LotNumber_field()
                })
            except Exception:
                tracked_reagent: TrackedReagentModel = self.rec_handler.add_model(TrackedReagentModel)
                tracked_reagent.set_ReagentType_field(reagent.get_ConsumableType_field())
                tracked_reagent.set_LotNumber_field(reagent.get_LotNumber_field())
                batch.add_child(tracked_reagent)

        self.rec_man.store_and_commit()

        # For any reagents selected that were already under the batch, show a table of them to the user.
        if warning_table_data:
            self.callback.table_dialog("Warning", "The following reagents were selected but are already being tracked"
                                                  " under this batch.", [
                                           VeloxStringFieldDefinition("ReagentType", "ReagentType", "Reagent Type"),
                                           VeloxStringFieldDefinition("LotNumber", "LotNumber", "Lot Number")
                                       ], warning_table_data)

        return SapioWebhookResult(True)
