from xml.etree.ElementTree import Element

from docx.table import _Cell, _Row


class DOCXUtils:
    @staticmethod
    def get_checkbox_values_from_cell(cell: _Cell) -> dict[str, bool]:
        checkbox_values: dict[str, bool] = {}

        # Check both the cell and it's paragraphs for checkbox values.
        cell_sdts: list[Element] = (
            cell._element.findall('.//w:sdt', {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
        if cell_sdts:
            for sdt in cell_sdts:
                # Check if it's a checkbox content control
                sdt_pr = sdt.find('.//w:sdtPr',
                                  {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
                if sdt_pr is None:
                    continue
                checkbox_element = (
                    sdt_pr.find('.//w14:checkbox', {'w14': 'http://schemas.microsoft.com/office/word/2010/wordml'}))
                if checkbox_element is None:
                    continue
                # Get checkbox state
                checked_state: Element = (
                    checkbox_element.find('.//w14:checked',
                                          {'w14': 'http://schemas.microsoft.com/office/word/2010/wordml'}))
                if checked_state is not None:
                    value: str = checked_state.attrib[list(checked_state.attrib.keys())[0]]
                    if sdt.text:
                        text: str = sdt.text.strip()
                    else:
                        text = "filler"
                    checkbox_values[text] = (value == '1')

        for p in cell.paragraphs:
            # Search for content controls (newer format)
            sdt_elements: list[Element] = p._element.findall(
                './/w:sdt',
                {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
            )
            if sdt_elements:
                for sdt in sdt_elements:
                    # Check if it's a checkbox content control
                    sdt_pr = sdt.find('.//w:sdtPr',
                                      {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
                    if sdt_pr is None:
                        continue
                    checkbox_element = (
                        sdt_pr.find('.//w14:checkbox', {'w14': 'http://schemas.microsoft.com/office/word/2010/wordml'}))
                    if checkbox_element is None:
                        continue
                    # Get checkbox state
                    checked_state: Element = (
                        checkbox_element.find('.//w14:checked',
                                              {'w14': 'http://schemas.microsoft.com/office/word/2010/wordml'}))
                    if checked_state is not None:
                        value: str = checked_state.attrib[list(checked_state.attrib.keys())[0]]
                        if p.text:
                            text: str = p.text.strip()
                            if text != expected_text:
                                text = "filler"
                        else:
                            text = "filler"
                        checkbox_values[text] = (value == '1')

        return checkbox_values

    @staticmethod
    def get_text_from_cell(cell: _Cell) -> str | None:
        """
        Get the text from a table cell in a .docx file.
        :param cell: The cell to get the text from.
        :return: The text from the cell or None if no text is found or unable to find the text in the xml.
        """
        # Try and grab the text directly from the cell.
        text: str = cell.text
        if text:
            return text

        # If the cell is empty, then try and grab the text from the cell paragraphs.
        for paragraph in cell.paragraphs:
            p_text: str = paragraph.text
            if p_text:
                return p_text
            sdts: list[Element] = (
                paragraph._element.findall(".//w:sdt",
                                           {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
            if not sdts:
                return None
            for sdt in sdts:
                text_box_value: str | None = DOCXUtils._get_text_box_value_from_element(sdt)
                if text_box_value:
                    text += text_box_value

        return text

    @staticmethod
    def get_text_box_values_from_cell(cell: _Cell) -> list[str] | None:
        values: list[str] = []

        for paragraph in cell.paragraphs:
            sdts: list[Element] = (
                paragraph._element.findall(".//w:sdt",
                                           {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
            if not sdts:
                return None
            for sdt in sdts:
                text: str | None = DOCXUtils._get_text_box_value_from_element(sdt)
                if text:
                    values.append(text)

        return values

    @staticmethod
    def _get_text_box_value_from_element(element: Element) -> str | None:
        try:
            content: Element = (
                element.find(".//w:sdtContent", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
            wr: Element = (
                content.find(".//w:r", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
            if wr is None:
                tc: Element = content.find(".//w:tc",
                                           {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
                p: Element = tc.find(".//w:p", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
                wr: Element = p.find(".//w:r", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
            t: Element = wr.find(".//w:t", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'})
        except Exception:
            return None

        return t.text

    @staticmethod
    def get_text_box_values_from_row(row_: _Row) -> list[str]:
        """
        Get the text box values from a table row in a .docx file.
        :param row_: The row to get the text box values from.
        :return: A list of values stored in the text boxes.
        """
        text_box_values: list[str] = []

        # Get any text box values within the row.
        row_sdts: list[Element] = (
            row_._element.findall(".//w:sdt", {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}))
        if row_sdts:
            for sdt in row_sdts:
                text_box_value: str | None = DOCXUtils._get_text_box_value_from_element(sdt)
                if text_box_value:
                    text_box_values.append(text_box_value)

        return text_box_values

    @staticmethod
    def get_all_checkbox_values_in_row(row: _Row, true_checkbox_char: str = "☒",
                                       false_checkbox_char: str = "☐") -> list[str] | None:
        """
        Get all the checkbox values in a row in a .docx file.
        :param row: The row to grab checkbox values from.
        :param true_checkbox_char: The character representing a marked checkbox.
        :param false_checkbox_char: The character representing an unmarked checkbox.
        :return: A list of checkbox values.
        """
        text_box_values: list[str] = DOCXUtils.get_text_box_values_from_row(row)
        if text_box_values:
            return [x for x in text_box_values if x == true_checkbox_char or x == false_checkbox_char]
        return None
