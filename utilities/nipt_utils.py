from utilities.data_models import BatchModel


class NIPTUtils:
    @staticmethod
    def add_batch_status(batch: BatchModel, status: str) -> None:
        statuses: list[str] = batch.get_ExemplarBatchStatus_field().split(",")
        if "New" in statuses:
            statuses.remove("New")
        if status not in statuses:
            statuses.append(status)
        batch.set_ExemplarBatchStatus_field(",".join(statuses))

    @staticmethod
    def remove_batch_status(batch: BatchModel, status: str) -> None:
        statuses: list[str] = batch.get_ExemplarBatchStatus_field().split(",")
        if "New" in statuses:
            statuses.remove("New")
        if status in statuses:
            statuses.remove(status)
            batch.set_ExemplarBatchStatus_field(",".join(statuses))
