class Constants:
    PLATE_LOCATIONS: list[str] = [
        'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1',
        'A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2',
        'A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3',
        'A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4',
        'A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5',
        'A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6',
        'A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7',
        'A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8',
        'A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9',
        'A10', 'B10', 'C10', 'D10', 'E10', 'F10', 'G10', 'H10',
        'A11', 'B11', 'C11', 'D11', 'E11', 'F11', 'G11', 'H11',
        'A12', 'B12', 'C12', 'D12', 'E12', 'F12', 'G12', 'H12'
    ]

    PLATE_LOCATIONS_LEADING_ZEROES: list[str] = [
        'A01', 'A02', 'A03', 'A04', 'A05', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12',
        'B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B09', 'B10', 'B11', 'B12',
        'C01', 'C02', 'C03', 'C04', 'C05', 'C06', 'C07', 'C08', 'C09', 'C10', 'C11', 'C12',
        'D01', 'D02', 'D03', 'D04', 'D05', 'D06', 'D07', 'D08', 'D09', 'D10', 'D11', 'D12',
        'E01', 'E02', 'E03', 'E04', 'E05', 'E06', 'E07', 'E08', 'E09', 'E10', 'E11', 'E12',
        'F01', 'F02', 'F03', 'F04', 'F05', 'F06', 'F07', 'F08', 'F09', 'F10', 'F11', 'F12',
        'G01', 'G02', 'G03', 'G04', 'G05', 'G06', 'G07', 'G08', 'G09', 'G10', 'G11', 'G12',
        'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'H07', 'H08', 'H09', 'H10', 'H11', 'H12'
    ]

    PLATE_LOCATIONS_MAP: dict[int, str] = {
        1: 'A1', 2: 'B1', 3: 'C1', 4: 'D1', 5: 'E1', 6: 'F1', 7: 'G1', 8: 'H1',
        9: 'A2', 10: 'B2', 11: 'C2', 12: 'D2', 13: 'E2', 14: 'F2', 15: 'G2', 16: 'H2',
        17: 'A3', 18: 'B3', 19: 'C3', 20: 'D3', 21: 'E3', 22: 'F3', 23: 'G3', 24: 'H3',
        25: 'A4', 26: 'B4', 27: 'C4', 28: 'D4', 29: 'E4', 30: 'F4', 31: 'G4', 32: 'H4',
        33: 'A5', 34: 'B5', 35: 'C5', 36: 'D5', 37: 'E5', 38: 'F5', 39: 'G5', 40: 'H5',
        41: 'A6', 42: 'B6', 43: 'C6', 44: 'D6', 45: 'E6', 46: 'F6', 47: 'G6', 48: 'H6',
        49: 'A7', 50: 'B7', 51: 'C7', 52: 'D7', 53: 'E7', 54: 'F7', 55: 'G7', 56: 'H7',
        57: 'A8', 58: 'B8', 59: 'C8', 60: 'D8', 61: 'E8', 62: 'F8', 63: 'G8', 64: 'H8',
        65: 'A9', 66: 'B9', 67: 'C9', 68: 'D9', 69: 'E9', 70: 'F9', 71: 'G9', 72: 'H9',
        73: 'A10', 74: 'B10', 75: 'C10', 76: 'D10', 77: 'E10', 78: 'F10', 79: 'G10', 80: 'H10',
        81: 'A11', 82: 'B11', 83: 'C11', 84: 'D11', 85: 'E11', 86: 'F11', 87: 'G11', 88: 'H11',
        89: 'A12', 90: 'B12', 91: 'C12', 92: 'D12', 93: 'E12', 94: 'F12', 95: 'G12', 96: 'H12'
    }

    ACCESSIONING_SAMPLE_ID: str = "OSLO"
    ACCESSIONING_POOL_SAMPLE_ID: str = "Pool-"
    ACCESSIONING_TUBE_BARCODE: str = "TESTTUBE"
    ACCESSIONING_VERSO_TUBE_BARCODE: str = "HZ"
    ACCESSIONING_TEST_PLATE_ID: str = "TESTPLATE"
    ACCESSIONING_PLATE_ID: str = "[WORKFLOW NAME]_[yymmdd]_"
    ACCESSIONING_FAMILY_ID: str = "TESTFAM_"
    ACCESSIONING_CONTROL: str = "CTRL-"
    ACCESSIONING_TEST_PROJECT: str = "TestBatch"

    CONTROL_TYPE_BLANK: str = "BLANK"

    TAG_INSTRUMENT_TRACKING: str = "INSTRUMENT TRACKING"
    TAG_QUANT_IT_SAMPLES_ENTRY: str = "QUANT-IT SAMPLES"

    RE_QIASYMPHONY: str = "[0-9A-Za-z]+_[0-9A-Za-z]+\\.xml"
    RE_QIAXPERT: str = "[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}_[0-9]{2}h[0-9]{2}m[0-9]{2}_[0-9]+_[A-Z]+_[0-9A-Z]+_[0-9]+\\.csv"
    RE_IDREADER: str = "Rack\sFile\s\-\s[A-Za-z0-9\-_]+\.csv"
    RE_QUANTSTUDIO: str = "SNP\\-ID_[0-9]{8}_Plate[0-9]+_[0-9]+\\.eds"
    RE_TAQMAN: str = "TaqManResult_[0-9]{8}_[0-9]+\\.txt"
    RE_BATCH_INIT: str = "[A-Za-z0-9]+_batch_initiation_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_LIB_REAGENT: str = "[A-Za-z0-9]+_library_reagent_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_LIB: str = "[A-Za-z0-9]+_library_quant_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_LIB_PROCESS_LOG: str = "[A-Za-z0-9]+_library_process_log\\.(tab\\.MD5|tab)"
    RE_BATCH_POOL_REPORT: str = "[A-Za-z0-9]+_[A-Za-z0-9]+_pool_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_LIB_QUANT_REPORT: str = "[A-Za-z0-9]+_library_quant_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_SEQ_REPORT: str = "[A-Za-z0-9]+_[A-Z]_[A-Za-z0-9]+_[A-Za-z0-9]+_sequencing_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_BATCH_SAMPLE_SHEET: str = "NIPT-sample-sheet-[A-Za-z0-9]+\\.txt"
    RE_BATCH_CONCENTRATIONS: str = "Concentrations\\.csv"
    RE_BATCH_CONCENTRATION_MAP: str = "concentration_map\\.png"
    RE_BATCH_NIPT_REPORT: str = "[A-Za-z0-9]+_[A-Z]_[A-Za-z0-9]+_[A-Za-z0-9]+_nipt_report_[0-9]{8}_[0-9]{6}\\.(tab\\.MD5|tab)"
    RE_SHORT_READ: str = "sample-submission-form-\\(illumina\\)-17\\.5\\.docx"
    RE_SHORT_READ_ONLINE: str = "OnlineSubmissionForm\\.docx"
    RE_LONG_READ: str = "sample-submission-form-\\(ONT\\)-1\\.5\\.docx"
