from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep


class OsloCallbackHelper:
    context: SapioWebhookContext
    callback: CallbackUtil
    exp_handler: ExperimentHandler

    def __init__(self, context: SapioWebhookContext) -> None:
        self.context = context
        self.callback = CallbackUtil(context)
        if context.eln_experiment:
            self.exp_handler = ExperimentHandler(context)

    def show_error_popup(self, msg: str, uninitialize_step: bool = True,
                         step: ElnEntryStep | None = None) -> SapioWebhookResult:
        """
        Show an error popup to the user.
        :param msg: The message to show to the user.
        :param uninitialize_step: If true, the step specified in the step parameter will be uninitialized.
        :param step: The step to be uninitialized.
        :return:
        """
        self.callback.ok_dialog("Error", msg)
        if uninitialize_step and step and self.exp_handler:
            self.exp_handler.uninitialize_step(step)
        return SapioWebhookResult(True)
