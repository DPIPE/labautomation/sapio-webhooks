from typing import List

from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo

from utilities.constants import Constants
from utilities.data_models import PlateModel, SampleModel, PlateDesignerWellElementModel, PatientModel, InHouseModel, \
    PromegaModel
from utilities.sample_utils import SampleUtils
from utilities.utils import initialize_entry
from workflows.acgh.acgh_util import ACGHUtil, auto_plate_acgh_samples_and_controls


class PlatingUtils:
    # entry names
    PLATES_ENTRY_NAME = "Sample Plating"
    ELISA_PLATES_ENTRY_NAME = "Setup Plasma Plate"
    PICK_FILE_ENTRY = "Generate Pick Files for Samples"

    @staticmethod
    def generate_unique_process_plate_id(acc_man: AccessionManager, process_name: str) -> str:
        prefix: str = Constants.ACCESSIONING_PLATE_ID.replace("[yymmdd]", TimeUtil.now_in_format("%Y%m%d"))
        prefix = prefix.replace("[WORKFLOW NAME]", process_name)
        accession_num: str = acc_man.accession_for_system(1, AccessionSystemCriteriaPojo(prefix))[0]
        return prefix + accession_num

    @staticmethod
    def generate_plate_in_experiment(rec_handler: RecordHandler, acc_man: AccessionManager,
                                     process_name: str, rows: int = 8, cols: int = 12) -> PlateModel:
        if process_name == "Array Comparative Genomic Hybridization":
            process_name = "aCGH"
        plate_id: str = PlatingUtils.generate_unique_process_plate_id(acc_man, process_name)
        return rec_handler.add_models_with_data(PlateModel, [{"PlateId": plate_id, "PlateRows": rows,
                                                              "PlateColumns": cols}])[0]

    @staticmethod
    def auto_plate_samples_and_controls(self_, samples: list[SampleModel], controls: list[SampleModel],
                                        process_name: str) -> None:
        if not samples:
            raise Exception("Samples not found in the experiment to do auto-plating")

        # Create the plate and add the samples and controls to it.
        plate: PlateModel = PlatingUtils.generate_plate_in_experiment(self_.rec_handler, self_.acc_manager,
                                                                      process_name)

        # Store and commit changes so that we get a plate ID generated.
        if "[WORKFLOW NAME]" in plate.get_PlateId_field():
            if process_name == "Array Comparative Genomic Hybridization":
                process_name = "aCGH"
            plate.set_PlateId_field(plate.get_PlateId_field().replace("[WORKFLOW NAME]", process_name))
        self_.rec_man.store_and_commit()

        # Sort the samples by their sample name values before they get added to the plate.
        def sort_criteria(sample_rec: SampleModel) -> int:
            try:
                return int(sample_rec.get_OtherSampleId_field())
            except:
                return 0

        samples.sort(key=sort_criteria)

        # Remove samples from existing plate if present
        self_.rel_man.load_parents_of_type(samples, PlateModel)
        for sample in samples:
            if sample.get_parent_of_type(PlateModel):
                sample.remove_parent(sample.get_parent_of_type(PlateModel))
        self_.rec_man.store_and_commit()

        # [OSLO-1120]: If we are in ELISA, run a different plating algorithm.
        if process_name == "ELISA":
            PlatingUtils.plate_for_elisa(self_, samples, controls, plate)
        elif process_name == "16S":
            PlatingUtils.plate_for_sixteenS(self_, samples, controls, plate)
        elif process_name == "aCGH":
            PlatingUtils.plate_for_acgh(self_, samples, plate)
        else:
            # Add the samples to the plate and also create plate designer well elements
            # create plate designer well elements for the controls and the sample
            plate_wel_designer_elements: List[PlateDesignerWellElementModel] = self_.inst_man.add_new_records_of_type(
                len(
                    samples) + (len(controls) if controls else 0), PlateDesignerWellElementModel)
            current_plate_index: int = 0
            for sample in samples:
                well_location: str = Constants.PLATE_LOCATIONS[current_plate_index]
                sample.set_RowPosition_field(well_location[0:1])
                sample.set_ColPosition_field(well_location[1:len(well_location)])
                sample.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                plate.add_child(sample)
                plate_wel_designer_elements[current_plate_index].set_PlateRecordId_field(plate.record_id)
                plate_wel_designer_elements[current_plate_index].set_Layer_field(1)
                plate_wel_designer_elements[current_plate_index].set_RowPosition_field(well_location[0:1])
                plate_wel_designer_elements[current_plate_index].set_ColPosition_field(
                    well_location[1:len(well_location)])
                plate_wel_designer_elements[current_plate_index].set_SourceDataTypeName_field(
                    SampleModel.DATA_TYPE_NAME)
                plate_wel_designer_elements[current_plate_index].set_SourceRecordId_field(sample.record_id)
                current_plate_index += 1

            # Add the controls after all the samples ( if available in arguments )
            if controls:
                for controls in controls:
                    well_location: str = Constants.PLATE_LOCATIONS[current_plate_index]
                    controls.set_RowPosition_field(well_location[0:1])
                    controls.set_ColPosition_field(well_location[1:len(well_location)])
                    controls.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                    plate.add_child(controls)
                    plate_wel_designer_elements[current_plate_index].set_PlateRecordId_field(plate.record_id)
                    plate_wel_designer_elements[current_plate_index].set_Layer_field(1)
                    plate_wel_designer_elements[current_plate_index].set_RowPosition_field(well_location[0:1])
                    plate_wel_designer_elements[current_plate_index].set_ColPosition_field(
                        well_location[1:len(well_location)])
                    plate_wel_designer_elements[current_plate_index].set_SourceDataTypeName_field(
                        SampleModel.DATA_TYPE_NAME)
                    plate_wel_designer_elements[current_plate_index].set_SourceRecordId_field(controls.record_id)
                    plate_wel_designer_elements[current_plate_index].set_IsControl_field(True)
                    current_plate_index += 1

        # set up 3d plater to show this plate
        # [OSLO-1120]: Account for ELISA.
        if process_name != "ELISA":
            plate_entry = self_.exp_handler.get_step(PlatingUtils.PLATES_ENTRY_NAME)
        else:
            plate_entry = self_.exp_handler.get_step(PlatingUtils.ELISA_PLATES_ENTRY_NAME)
        options = plate_entry.get_options()
        if "MultiLayerPlating_Plate_RecordIdList" not in options:
            options["MultiLayerPlating_Plate_RecordIdList"] = ""
        options["MultiLayerPlating_Plate_RecordIdList"] = str(plate.record_id)
        plate_entry.set_options(options)
        # [OSLO-1120]: Since this webhook is being invoked on init of the plating entry during ELISA, we don't need to
        # initialize it here.
        if process_name != "ELISA":
            initialize_entry(plate_entry.eln_entry, self_.context)
        self_.rec_man.store_and_commit()

    @staticmethod
    def plate_for_elisa(self_, samples: list[SampleModel], controls: list[SampleModel],
                        plate: PlateModel) -> None:

        # Map all the patient samples according to their patient.
        patients_to_samples: dict[PatientModel, list[SampleModel]] = (
            SampleUtils.map_patients_to_samples(self_.rel_man, samples))

        # Create the plate well designer elements for all the samples and controls.
        well_index: int = 0
        plate_well_designer_elements: List[PlateDesignerWellElementModel] = (
            self_.inst_man.add_new_records_of_type(len(samples) * 9 + 39, PlateDesignerWellElementModel))
        current_plate_index: int = 0

        # Plate 6 of the no serum/ig NTCs
        for x in range(2):
            well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index]
            _plate_sample(None, plate, well_location, plate_well_designer_elements[well_index])
            plate_well_designer_elements[well_index].set_IsControl_field(True)
            plate_well_designer_elements[well_index].set_ControlType_field("No serum, no IgG")
            current_plate_index += 1
            well_index += 1

        for x in range(4):
            well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index]
            _plate_sample(None, plate, well_location, plate_well_designer_elements[well_index])
            plate_well_designer_elements[well_index].set_IsControl_field(True)
            plate_well_designer_elements[well_index].set_ControlType_field("No serum")
            current_plate_index += 1
            well_index += 1

        # Set the current plate index to B1 and plate the diluted positive control samples in triplicates.
        current_plate_index = 12
        pos_ctrl: SampleModel = \
            [x for x in controls if x.get_child_of_type(InHouseModel).get_ConsumableType_field() == "GPIHBP1-Pos ctrl"][
                0]
        for x in range(4):
            for y in range(3):
                well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index]
                _plate_sample(pos_ctrl, plate, well_location, plate_well_designer_elements[well_index])
                plate_well_designer_elements[well_index].set_IsControl_field(True)
                plate_well_designer_elements[well_index].set_ControlType_field("Positive")
                if current_plate_index % 3 == 2:
                    current_plate_index += 10
                else:
                    current_plate_index += 1
                well_index += 1

        # At this point, we should be at well F1. Plate the in-house normal control samples in triplicates. Once we hit
        # position A4, move down one row since we already have a no serum plated here.
        control_index: int = 1
        normal_controls: list[SampleModel] = \
            [x for x in controls if "GPIHBP1-C" in x.get_child_of_type(InHouseModel).get_ConsumableType_field()]
        while control_index < 8:
            if (f"GPIHBP1-C{control_index}" != normal_controls[control_index - 1].get_child_of_type(InHouseModel)
                    .get_ConsumableType_field()):
                continue
            well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index]
            _plate_sample(normal_controls[control_index - 1], plate, well_location,
                          plate_well_designer_elements[well_index])
            plate_well_designer_elements[well_index].set_IsControl_field(True)
            if current_plate_index % 3 == 2:
                if Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index] == "H03":
                    current_plate_index = 15
                else:
                    current_plate_index += 10
                control_index += 1
            else:
                current_plate_index += 1
            well_index += 1

        # At this point, we should be at F4, and can go ahead and plate all the patient samples 3x3 each, while moving
        # up to the A row when reaching the H row.
        for patient in patients_to_samples:
            for sample in patients_to_samples[patient]:
                for x in range(3):
                    for y in range(3):
                        well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[current_plate_index]
                        _plate_sample(sample, plate, well_location, plate_well_designer_elements[well_index])
                        if current_plate_index % 3 == 2:
                            if well_location == "H06":
                                current_plate_index = 6
                            elif well_location == "H09":
                                current_plate_index = 9
                            else:
                                current_plate_index += 10
                        else:
                            current_plate_index += 1
                        well_index += 1

    @staticmethod
    def plate_for_sixteenS(self_, samples: list[SampleModel], controls: list[SampleModel] | None,
                           plate: PlateModel) -> None:
        # Create the plate well designer elements for all the samples and controls.
        plate_well_designer_elements: List[PlateDesignerWellElementModel] = (
            self_.inst_man.add_new_records_of_type(len(samples) + len(controls), PlateDesignerWellElementModel))
        curr_well_index: int = 0

        # Sort the samples by their sample names and plate them.
        sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(samples)
        for sample in sorted_samples:
            well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
            _plate_sample(sample, plate, well_location, plate_well_designer_elements[curr_well_index])
            curr_well_index += 1

        # Plate the controls immediately after the samples. Positive controls will be plated first.
        if controls:
            positive_controls: list[SampleModel] = [x for x in controls if x.get_ControlType_field() != "Negative"]
            if positive_controls:
                negative_controls: list[SampleModel] = list(set(controls) - set(positive_controls))
            else:
                negative_controls = controls

            sorted_positive_controls: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(positive_controls)
            for control in sorted_positive_controls:
                well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
                _plate_sample(control, plate, well_location, plate_well_designer_elements[curr_well_index])
                curr_well_index += 1

            sorted_negative_controls: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(negative_controls)
            for control in sorted_negative_controls:
                well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
                _plate_sample(control, plate, well_location, plate_well_designer_elements[curr_well_index])
                curr_well_index += 1

    @staticmethod
    def plate_for_acgh(self_, samples, plate):
        controls = [s for s in samples if s.get_ExemplarSampleType_field() == "Promega"]
        samples = [s for s in samples if s.get_ExemplarSampleType_field() != "Promega"]

        # Create the plate well designer elements for all the samples and controls.
        plate_well_designer_elements: List[PlateDesignerWellElementModel] = (
            self_.inst_man.add_new_records_of_type(len(samples)*2, PlateDesignerWellElementModel))

        # Sort the samples by their well position
        sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_well_positions(samples)

        acgh_util = ACGHUtil(self_.rel_man, self_.logger, self_.an_man)
        samples_by_array_size, sample_id_to_values = acgh_util.group_samples_by_array_size(sorted_samples)

        control_details = self_.exp_handler.get_step_records("Append Controls")
        sample_id_to_promega_lot = {}

        for detail in control_details:
            sample_id_to_promega_lot[detail.get_field_value("SampleId")] = detail.get_field_value("LotNumber")
        self_.rel_man.load_children_of_type(controls, PromegaModel)

        auto_plate_acgh_samples_and_controls(samples_by_array_size, controls, plate, plate_well_designer_elements, sample_id_to_promega_lot)


def _plate_sample(sample_: SampleModel, plate_: PlateModel, well_location_: str,
                  plate_well_designer_element_: PlateDesignerWellElementModel) -> None:
    plate_well_designer_element_.set_PlateRecordId_field(plate_.record_id)
    plate_well_designer_element_.set_Layer_field(1)
    plate_well_designer_element_.set_RowPosition_field(well_location_[0:1])
    plate_well_designer_element_.set_ColPosition_field(well_location_[1:len(well_location_)])
    plate_well_designer_element_.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
    if sample_:
        plate_well_designer_element_.set_SourceRecordId_field(sample_.record_id)
