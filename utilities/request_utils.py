from enum import Enum

from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities import utils
from utilities.data_models import RequestModel, AssayDetailModel, InvoiceModel, TestCodeConfigurationModel


class OrderStatus(Enum):
    SAMPLES_PENDING: str = "Samples pending"
    READY_FOR_TECHNICIAN: str = "Ready for technician"
    READY_FOR_APPROVAL: str = "Ready for approval"
    ORDER_APPROVED: str = "Order approved"
    ORDER_APPROVAL_ERRORS: str = "Order approval errors"


class RequestUtils:
    PRIORITY_URGENT: str = "Urgent"
    PRIORITY_HIGH: str = "High"
    PRIORITY_NORMAL: str = "Normal"

    @staticmethod
    def add_request(rec_handler: RecordHandler, acc_manager: AccessionManager) -> RequestModel:
        """
        Adds a request record and automatically assigns it an accession request name
        :param rec_handler: RecordHandler
        :param acc_manager: AccessionManager
        :return: The new request
        """
        request: RequestModel = rec_handler.add_model(RequestModel)
        request.set_RequestName_field(RequestUtils.generate_order_name(acc_manager))
        return request

    @staticmethod
    def get_orders_to_assay_details(rel_man: RecordModelRelationshipManager,
                                    orders: list[RequestModel]) -> dict[RequestModel, list[AssayDetailModel]]:
        """
        Get a dict of orders -> assay details from the specified orders
        :param rel_man:
        :param orders: The orders to generate the dict from
        :return:  dict of orders -> assay details
        """
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = {}

        rel_man.load_children_of_type(orders, InvoiceModel)
        invoices: list[InvoiceModel] = []
        for o in orders:
            invoices.extend(o.get_children_of_type(InvoiceModel))

        rel_man.load_children_of_type(invoices, AssayDetailModel)
        assay_details: list[AssayDetailModel] = []
        for i in invoices:
            assay_details.extend(i.get_children_of_type(AssayDetailModel))

        for o in orders:
            for i in o.get_children_of_type(InvoiceModel):
                if o not in orders_to_assay_details:
                    orders_to_assay_details[o] = []

                orders_to_assay_details[o].extend(i.get_children_of_type(AssayDetailModel))

        return orders_to_assay_details

    @staticmethod
    def generate_order_name(acc_manager: AccessionManager) -> str:
        """
        Generate the order name in the format of %yxxxxx, where %y is the current two-digit year, and xxxxx is the
        xth order of the year, padded by 0's. For ex: 2400045 if the 45th order of the year 2024.
        :param acc_manager: AccessionManager
        :return: The new accessioned order name.
        """
        year: str = TimeUtil.now_in_format("%y")
        accession_value: str = acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(year))[0]
        accession_value = utils.add_leading_zeroes(accession_value, 5)
        return f"{year}{accession_value}"

    @staticmethod
    def generate_assay_detail(rec_handler: RecordHandler, analysis_name: str,
                              order_record_id: int) -> AssayDetailModel | None:
        """
        Generate an assay detail for the specified analysis.
        :param rec_handler: RecordHandler
        :param analysis_name: The name of the analysis to create an assay detail for.
        :param order_record_id: The record ID of the order.
        :return: The new assay detail, or None if any errors occur.
        """
        assay_detail: AssayDetailModel = rec_handler.add_model(AssayDetailModel)
        assay_detail.set_Assay_field(analysis_name)
        try:
            test_code_config = rec_handler.query_models(TestCodeConfigurationModel,
                                                        TestCodeConfigurationModel.ASSAY__FIELD_NAME.field_name,
                                                        [analysis_name])[0]
            test_code_id: str = (test_code_config.get_TestCodeID_field())
            assay_detail.set_TestCodeId_field(test_code_id)
            assay_detail.set_OrderRecordID_field(order_record_id)
            assay_detail.set_AnalysisLabel_field(test_code_config.get_AnalysisLabel_field())
        except Exception:
            return None
        return assay_detail

    @staticmethod
    def generate_invoice(rec_handler: RecordHandler, order_name: str, analysis_name: str) -> InvoiceModel:
        """
        Generate an invoice record.
        :param rec_handler: RecordHandler
        :param order_name: The name of the order.
        :param analysis_name: The name of the analysis.
        :return: The new invoice.
        """
        invoice: InvoiceModel = rec_handler.add_model(InvoiceModel)
        invoice.set_InvoiceId_field(f"{order_name}{analysis_name}")
        invoice.set_AssayName_field(analysis_name)
        return invoice

    @staticmethod
    def generate_trio_id(context: SapioWebhookContext) -> str:
        accession_man: AccessionManager = DataMgmtServer.get_accession_manager(context.user)
        pojo: AccessionSystemCriteriaPojo = AccessionSystemCriteriaPojo("WGS-TrioID")
        year: str = TimeUtil.now_in_format("%y")
        pojo.prefix = "T" + year
        pojo.initial_sequence_value = 1
        trio_id: str = accession_man.accession_for_system(1, pojo).pop()
        trio_id: str = utils.add_leading_zeroes(trio_id, 4)
        return trio_id
