from sapiopycommons.general.exceptions import SapioException


class OsloException(SapioException):
    pass
