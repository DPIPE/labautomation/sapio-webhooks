import traceback
from abc import abstractmethod

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.webhook.webhook_handlers import CommonsWebhookHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import WebhookErrorLogModel
from utilities.oslo_callback_helper import OsloCallbackHelper


class OsloWebhookHandler(CommonsWebhookHandler):
    """
    Overriding some of CommonsWebhookHandler's functionality for more custom logging.
    """
    @abstractmethod
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        """
        The business logic of the webhook, implemented in all subclasses that are called as endpoints.
        """
        pass

    def handle_unexpected_exception(self, e: Exception) -> SapioWebhookResult:
        result: SapioWebhookResult | None = self.handle_any_exception(e)
        if result is not None:
            return result
        msg = traceback.format_exc()
        # Log to the apprunner.
        self.log_error(msg)

        # Create a webhook error log record so the error can be viewed in the system
        self.log_to_system(msg)

        # If the user is a part of Sapio Sciences, also log the error directly to the client.
        if self.context.user.username.lower().endswith("@sapiosciences.com"):
            return PopupUtil.string_field_popup("Error", "", "Reason", msg, editable=False,
                                                request_context="SapioCriticalErrorException", num_lines=15)
        return SapioWebhookResult(False, display_text="Unexpected error occurred during webhook execution. "
                                                      "Please contact Sapio support.")

    def log_to_system(self, msg: str) -> None:
        exp_id: str | None = None
        if self.context.eln_experiment is not None:
            exp_id = f"{self.context.user.url.replace('webservice/api', 'veloxClient')}/VeloxClient.html?app={self.context.user.guid}#notebookExperimentId={self.context.eln_experiment.notebook_experiment_id};view=eln"
        group: str = self.context.user.session_additional_data.current_group_name
        self.dr_man.add_data_records_with_data(WebhookErrorLogModel.DATA_TYPE_NAME,
                                               [{
                                                   WebhookErrorLogModel.EXPERIMENTID__FIELD_NAME.field_name: exp_id,
                                                   WebhookErrorLogModel.DETAILS__FIELD_NAME.field_name: msg,
                                                   WebhookErrorLogModel.GROUPNAME__FIELD_NAME.field_name: group
                                               }])

