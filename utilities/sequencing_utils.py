from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.data_models import SampleModel, SequencingMetadataModel


class SequencingUtils:
    TAG_SEQUENCER_TYPE: str = "SEQUENCER TYPE"

    PROCESS_NAME: str = "WGS"
    STEP_NAME: str = "Sequencing"

    SEQUENCER_NOVASEQ_6000: str = "NovaSeq 6000"
    SEQUENCER_NOVASEQ_X: str = "NovaSeq X"

    RSB_CONSTANT: float = 56.7
    PHIX_2_5_CONSTANT: float = 2.5

    SEQUENCING_STEP_NUM: int = 6

    ENTRY_NAME_TUBE_AMOUNTS_PER_SAMPLE: str = "Robot File Generation"
    ENTRY_NAME_ALIQUOTED_SAMPLES: str = "Aliquoted Samples"

    HAMILTON_HEADERS_NOVASEQ_6000: list[str] = ["Sample Name", "Per Sample Volume (ul)",
                                                "Buffer Volume (ul) to Add to Pool", "PhiX v3 (ul)",
                                                "Volume of Pool to Denature (ul)", "NaOH Volume (ul)",
                                                "Tris-HCl Volume (ul)"]
    HAMILTON_HEADERS_NOVASEQ_X: list[str] = ["Source Plate", "Source Position",	"OutPut Plate Position", "Library Name",
                                             "Library Volume", "RSB Volume"]

    @staticmethod
    def get_sequencer_type(rel_man: RecordModelRelationshipManager, pools: list[SampleModel]) -> str:
        rel_man.load_parents_of_type(pools, SequencingMetadataModel)

        sequencer: str = None
        for p in pools:
            sm: SequencingMetadataModel = p.get_parent_of_type(SequencingMetadataModel)
            if sm is None:
                return None

            sm_sequencer: str = sm.get_SequencerType_field()
            if sm_sequencer is None or sm_sequencer == "":
                return None

            if sequencer is None:
                sequencer = sm_sequencer

            else:
                if sm_sequencer not in sequencer:
                    return None

        return sequencer
