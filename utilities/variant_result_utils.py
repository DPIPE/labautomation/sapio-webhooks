from sapiopycommons.recordmodel.record_handler import RecordHandler

from utilities.data_models import VariantResultModel, PrimerModel


class VariantResultUtils:
    STATUS_COMPLETED: str = "Completed"
    STATUS_BLOCKED: str = "Blocked"
    STATUS_REVIEW_NEEDED: str = "Review Needed"

    @staticmethod
    def get_matching_primers(rec_handler: RecordHandler,
                             variant_results: list[VariantResultModel]) -> dict[VariantResultModel, PrimerModel]:
        """
        Get a map of VariantResult records mapped to their corresponding Primer records.
        :param rec_handler: RecordHandler
        :param variant_results: List of VariantResult records.
        :return: Map of VariantResult records mapped to their corresponding Primer Records. If there is not associated
        Primer record, the VariantResult will be mapped to a None value.
        """
        variants_to_primers: dict[VariantResultModel, PrimerModel] = {}
        variants: list[str] = [v.get_Variant_field() for v in variant_results]
        primers: list[PrimerModel] = rec_handler.query_models(PrimerModel, PrimerModel.VARIANT__FIELD_NAME.field_name,
                                                              variants)
        for v in variant_results:
            try:
                variants_to_primers[v] = [p for p in primers if p.get_Variant_field() == v.get_Variant_field()][0]
            except Exception:
                variants_to_primers[v] = None
        return variants_to_primers

