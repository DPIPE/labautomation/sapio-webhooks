import json
import re

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CompositeReportTerm, \
    CompositeTermOperation, CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager, \
    RecordModelInstanceManager
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities import utils
from utilities.data_models import RequestModel, SampleModel, AssignedProcessModel, AssayDetailModel, InvoiceModel, \
    ExemplarConfigModel, TestCodeConfigurationModel, ExemplarSDMSFileModel, VariantResultModel, PatientModel, \
    SampleRegistrationErrorModel, FamilyDetailsModel, ProbeMixSampleModel, PrimerDesignReportModel, ConsumableItemModel, \
    InHouseModel
from utilities.family_utils import FamilyUtils
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class WorkflowUtils:
    ASSAY_NAME_QIASYMPHONY_DNA_EXTRACTION: str = "QIAsymphony DNA Extraction"
    ASSAY_NAME_MAXWELL_DNA_EXTRACTION: str = "Maxwell DNA Extraction"
    ASSAY_NAME_DNA_QUALITY_CONTROL: str = "DNA Quality Control"

    PRIMER_HANDLING__ALWAYS_CREATE = "Always design new primer"

    @staticmethod
    def send_samples_to_workflows(context: SapioWebhookContext,
                                  rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                                  orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]],
                                  samples: list[SampleModel]) -> dict[RequestModel, list[SampleRegistrationErrorModel]]:
        """
        Send the specified samples to the assays linked to the specified orders.
        :param context: SapioWebhookContext
        :param rec_handler: RecordHandler
        :param rel_man: RecordModelRelationshipManager
        :param orders_to_assay_details: Dict of orders -> assay details.
        :param samples: The samples to send to workflows.
        :return: A list of sample registration error records.
        """

        def send_sample(sample_rec: SampleModel, config: TestCodeConfigurationModel,
                        request: RequestModel,
                        assay_detail_rec: AssayDetailModel) -> SampleRegistrationErrorModel | None:
            try:
                # If the assay detail extension has a probe mix field, make sure the probe mix is specified.
                extension_type_name: str = re.sub("[^a-zA-Z]+", "", assay_detail_rec.get_Assay_field()) + "AssayDetail"
                extension_rec: PyRecordModel = request.backing_model.get_child_of_type(extension_type_name)
                if "ProbeMix" in [x.data_field_name for x in DataMgmtServer.get_data_type_manager(context.user).get_field_definition_list(extension_type_name)]:
                    probe_mix: str | None = extension_rec.get_field_value("ProbeMix")
                    if not probe_mix:
                        return rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                [{"Assay": config.get_Assay_field(),
                                                                  "SampleId": sample.get_SampleId_field(),
                                                                  "Reason": "No probe mix was specified."}])[0]
                    # If the assay detail has a probe mix specified, then create a probe mix sample extension for the
                    # sample if it doesn't already have one. Just update the probe mix on the extension if it already
                    # exists.
                    else:
                        probe_mix_sample: ProbeMixSampleModel | None = sample_rec.get_child_of_type(ProbeMixSampleModel)
                        if not probe_mix_sample:
                            probe_mix_sample = rec_handler.add_model(ProbeMixSampleModel)
                            sample_rec.add_child(probe_mix_sample)
                        probe_mix_sample.set_ProbeMix_field(probe_mix)

                # Queue the sample for the process.
                ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [sample_rec.record_id],
                                                  config.get_Assay_field(), 1, None, request)

                return None
            except Exception as e:
                return rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                        [{"Assay": config.get_Assay_field(),
                                                          "SampleId": sample.get_SampleId_field(),
                                                          "Reason": f"{sample.get_SampleId_field()}: {repr(e)}"}])[0]

        errors: dict[RequestModel, list[SampleRegistrationErrorModel]] = {}

        # Get the exemplar config record. (Used for WGS concentration evals)
        exemplar_config: ExemplarConfigModel = rec_handler.query_all_models(ExemplarConfigModel)[0]

        # Map each assay detail with its matching analysis configuration
        assay_details: list[AssayDetailModel] = []
        for order in orders_to_assay_details:
            assay_details.extend(orders_to_assay_details[order])
        assay_details_to_analysis_configs: dict[AssayDetailModel, TestCodeConfigurationModel] = (
            WorkflowUtils.get_matching_analysis_configurations(rec_handler, assay_details))

        # Initialize the error list for each of the orders.
        orders: list[RequestModel] = [o for o in orders_to_assay_details]
        for order in orders:
            errors[order] = []

        # Get all the top level samples
        rel_man.load_children_of_type(orders, SampleModel)
        top_level_samples: list[SampleModel] = []
        for o in orders:
            top_level_samples.extend(o.get_children_of_type(SampleModel))

        # Assign each sample to the assays linked to the order, if they meet the right conditions. If any conditions are
        # not met, show an error to the user.
        rel_man.load_parents_of_type(samples, AssignedProcessModel)
        rel_man.load_parents_of_type(top_level_samples, RequestModel)
        rel_man.load_children_of_type(samples, VariantResultModel)
        rel_man.load_children_of_type(samples, ProbeMixSampleModel)

        assay_detail_extensions: list[str] = \
            [re.sub("[^a-zA-Z]+", "", x.get_Assay_field()) + "AssayDetail" for x in assay_details_to_analysis_configs]
        for extension in assay_detail_extensions:
            rel_man.load_children(rec_handler.inst_man.unwrap_list(orders), extension)

        in_extraction_qc: bool = False
        ddpcr_samples: list[SampleModel] = []
        check_trio = False
        trio_assay_detail: AssayDetailModel | None = None
        trio_order: RequestModel | None = None
        trio_sample = None

        for sample in samples:
            top_level_sample: SampleModel = [t for t in top_level_samples
                                             if t.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
            order: RequestModel = [x for x in top_level_sample.get_parents_of_type(RequestModel) if x in orders][0]
            for assay_detail in orders_to_assay_details[order]:
                # Check if the sample is still in process of extraction or QC. If so, append the error to the error list
                # to show the user, and move on to the next assay.
                for assigned_process in sample.get_parents_of_type(AssignedProcessModel):
                    if "Extraction" in assigned_process.get_ProcessName_field() and (
                            "Ready for -" in assigned_process.get_Status_field() or
                            "In Process -" in assigned_process.get_ProcessName_field()):
                        errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                              [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                "SampleId": sample.get_SampleId_field(),
                                                                                "Reason": "Sample is still in extraction."}])[
                                                 0])
                        in_extraction_qc = True
                        break
                    elif "Quality Control" in assigned_process.get_ProcessName_field() and (
                            "Ready for -" in assigned_process.get_Status_field() or
                            "In Process -" in assigned_process.get_ProcessName_field()):
                        errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                              [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                "SampleId": sample.get_SampleId_field(),
                                                                                "Reason": "Sample is still in QC."}])[
                                                 0])
                        in_extraction_qc = True
                        break
                if in_extraction_qc:
                    in_extraction_qc = False
                    continue

                # Check conditions unique for each assay.
                assay: str = assay_detail.get_Assay_field()
                analysis_label = assay_detail.get_AnalysisLabel_field()

                # [OSLO-314]: Don't do anything for NIPT assays since there's no process for them.
                if "NIPT" in assay:
                    continue

                # If the current assay is WGS or WES, check conditions unique for them.
                elif ("WGS" in assay or "aCGH" in assay) and "Trio" in analysis_label:
                    check_trio = True
                    trio_assay_detail = assay_detail
                    trio_order = order
                    trio_sample = sample
                elif ("WGS" in assay or "aCGH" in assay) and "Single" in analysis_label:
                    # [OSLO-900]: If the analysis label is "Single," then skip the family check and queue the sample for
                    # WGS.
                    if "WGS" in assay:
                        can_add, reason = WorkflowUtils.can_add_to_wgs(sample, exemplar_config)
                        if can_add:
                            errors[order].append(send_sample(sample, assay_details_to_analysis_configs[assay_detail],
                                                             order, assay_detail))
                        else:
                            errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                                  [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                    "SampleId": sample.get_SampleId_field(),
                                                                                    "Reason": reason}])[0])
                    else:
                        errors[order].append(send_sample(sample, assay_details_to_analysis_configs[assay_detail], order,
                                                         assay_detail))
                elif "WES" in assay:
                    can_add, reason = WorkflowUtils.can_add_to_wes(sample, exemplar_config)
                    if can_add:
                        errors[order].append(
                            send_sample(sample, assay_details_to_analysis_configs[assay_detail], order, assay_detail))
                    else:
                        errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                              [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                "SampleId": sample.get_SampleId_field(),
                                                                                "Reason": reason}])[0])
                # [OSLO-749]: Create a Variant Result child record for samples under a ddPCR assay.
                elif "ddPCR" in assay:
                    variant_result: VariantResultModel = rec_handler.add_model(VariantResultModel)
                    variant_result.set_Variant_field(assay_detail.get_Variant_field())
                    variant_result.set_SuggestedVerification_field("ddPCR")
                    sample.add_child(variant_result)
                    ddpcr_samples.append(sample)
                # [OSLO-968]: Allow OGM samples to be queued via adding an assay to an order.
                elif "OGM" in assay:
                    if sample.get_ExemplarSampleType_field() != "EDTA Blood":
                        errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                              [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                "SampleId": sample.get_SampleId_field(),
                                                                                "Reason": "Sample is not an EDTA blood sample."}])[
                                                 0])
                    else:
                        errors[order].append(
                            send_sample(sample, assay_details_to_analysis_configs[assay_detail], order, assay_detail))
                # [OSLO-1118]: Queue plasma samples for ELISA.
                elif "ELISA" in assay:
                    if sample.get_ExemplarSampleType_field() != "Plasma":
                        errors[order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                              [{"Assay": assay_details_to_analysis_configs[assay_detail].get_Assay_field(),
                                                                                "SampleId": sample.get_SampleId_field(),
                                                                                "Reason": "Sample is not a plasma sample."}])[
                                                 0])
                    else:
                        errors[order].append(send_sample(sample, assay_details_to_analysis_configs[assay_detail], order,
                                                         assay_detail))

                else:  # For all the other assays, just try and send the sample and catch any errors.
                    errors[order].append(send_sample(sample, assay_details_to_analysis_configs[assay_detail], order,
                                                     assay_detail))

        if check_trio:
            proband_sample = None
            family_id = None
            if trio_sample:
                rel_man.load_children_of_type([trio_sample], FamilyDetailsModel)
                family_details = trio_sample.get_children_of_type(FamilyDetailsModel)
                if family_details:
                    family_role = family_details[0].get_FamilyRole_field()
                    if "Proband" == family_role:
                        proband_sample = trio_sample
                        family_id = family_details[0].get_FamilyId_field()
                else:
                    errors[trio_order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                               [{"Assay": assay_details_to_analysis_configs[trio_assay_detail].get_Assay_field(),
                                                                                 "SampleId": trio_sample.get_SampleId_field(),
                                                                                 "Reason": "Sample is not part of a trio."}])[0])

            if not proband_sample:
                errors[trio_order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                           [{"Assay": assay_details_to_analysis_configs[trio_assay_detail].get_Assay_field(),
                                                                             "SampleId": trio_sample.get_SampleId_field(),
                                                                             "Reason": "Please add proband sample of the family"}])[0])
            else:
                assay: str = re.sub("[^a-zA-Z]+", "", trio_assay_detail.get_Assay_field())
                extension_type: str = f"{assay}AssayDetail"
                order_model: PyRecordModel = rec_handler.inst_man.unwrap(trio_order)
                rel_man.load_children([order_model], extension_type)
                assay_detail_ext: PyRecordModel = order_model.get_child_of_type(extension_type)
                trio_id: str = ""
                if assay_detail_ext:
                    trio_id: str = assay_detail_ext.get_field_value("TrioId")
                # Set Trio ID on family samples
                family_samples: list[SampleModel] = FamilyUtils.get_family_samples(rec_handler, rel_man, family_id)
                for sample in family_samples:
                    value: str = sample.get_TrioID_field()
                    if not value:
                        sample.set_TrioID_field(trio_id)
                    else:
                        values = str(value).split(",")
                        values.append(trio_id)
                        sample.set_TrioID_field(",".join(set(values)))
                rec_handler.rec_man.store_and_commit()

                if len(family_samples) < 3:
                    errors[trio_order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                               [{"Assay":
                                                                                     assay_details_to_analysis_configs[
                                                                                         trio_assay_detail].get_Assay_field(),
                                                                                 "SampleId": trio_sample.get_SampleId_field(),
                                                                                 "Reason": f"Family {family_id} doesn't have all the sample required for trio analysis"}])[
                                                  0])
                else:
                    # [OSLO-900]: Assign to WGS if the samples meet the concentration threshold and are DNA samples.
                    # Send mother and father samples as well along with proband sample
                    samples_to_assign: list[SampleModel] = [trio_sample]
                    rel_man.load_children_of_type(family_samples, FamilyDetailsModel)
                    for sample in family_samples:
                        family_details: list[FamilyDetailsModel] = sample.get_children_of_type(FamilyDetailsModel)
                        if family_details:
                            role: str = family_details[0].get_FamilyRole_field()
                            if "Father" == role or "Mother" == role:
                                samples_to_assign.append(sample)

                    if len(samples_to_assign) < 3:
                        errors[trio_order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                                   [{"Assay":
                                                                                         assay_details_to_analysis_configs[
                                                                                             trio_assay_detail].get_Assay_field(),
                                                                                     "SampleId": trio_sample.get_SampleId_field(),
                                                                                     "Reason": f"Mother or Father sample NOT found under the family {family_id}"}])[
                                                      0])
                    if trio_assay_detail.get_Assay_field() == "WGS":
                        all_can_add: bool = True
                        for sample in samples_to_assign:
                            can_add, reason = WorkflowUtils.can_add_to_wgs(sample, exemplar_config)
                            if not can_add:
                                all_can_add = False
                                errors[trio_order].append(rec_handler.add_models_with_data(SampleRegistrationErrorModel,
                                                                                           [{"Assay":
                                                                                                 assay_details_to_analysis_configs[
                                                                                                     trio_assay_detail].get_Assay_field(),
                                                                                             "SampleId": sample.get_SampleId_field(),
                                                                                             "Reason": reason}])[0])
                        if all_can_add:
                            for sample in samples_to_assign:
                                errors[trio_order].append(
                                    send_sample(sample, assay_details_to_analysis_configs[trio_assay_detail],
                                                trio_order, trio_assay_detail))
                    else:
                        for sample in samples_to_assign:
                            errors[trio_order].append(
                                send_sample(sample, assay_details_to_analysis_configs[trio_assay_detail],
                                            trio_order, trio_assay_detail))

        # For analysis configs that say to do so, create a new primer design report
        rel_man.load_children_of_type(orders, PrimerDesignReportModel)
        for order, assay_details in orders_to_assay_details.items():
            for assay_detail in assay_details:
                analysis_config = assay_details_to_analysis_configs[assay_detail]
                if analysis_config.get_PrimerHandling_field() != WorkflowUtils.PRIMER_HANDLING__ALWAYS_CREATE:
                    continue
                primer_design_reports = order.get_children_of_type(PrimerDesignReportModel)
                if primer_design_reports:
                    continue

                primer_design_report = rec_handler.add_model(PrimerDesignReportModel)

                assay = assay_detail.get_Assay_field()
                if assay == "RNA Analysis":
                    primer_design_report.set_PrimerMethod_field("RNA")
                if assay == "Sanger":
                    primer_design_report.set_PrimerMethod_field("Sanger")
                primer_design_report.set_Sample_field(samples[0].record_id)
                primer_design_report.set_Transcript_field(assay_detail.get_Transcript_field())
                primer_design_report.set_Gene_field(assay_detail.get_Gene_field())
                primer_design_report.set_Variant_field(assay_detail.get_Variant_field())
                order.add_child(primer_design_report)

        # [OSLO-749]: If there were any ddPCR samples, then store and commit the new Variant Result records and let
        # rules handle the logic of queueing the samples.
        if ddpcr_samples:
            rec_handler.rec_man.store_and_commit()

        return errors

    @staticmethod
    def can_add_to_wgs(sample: SampleModel, exemplar_config: ExemplarConfigModel = None) -> (bool, str):
        """
        Checks if the sample meets the concentration criteria for WGS.
        :param sample: The sample to check
        :param exemplar_config: The ExemplarConfig record in the system. If not provided, it will be queried for.
        :return: True if queueing is possible, False otherwise, along with the reason why.
        """
        if sample.get_ExemplarSampleType_field() != "DNA":
            return False, "Sample is not a DNA sample."

        if (sample.get_Concentration_field() is None or
                sample.get_Concentration_field() <
                exemplar_config.get_WGSLowConcentrationThreshold_field()):
            msg: str = f"Sample concentration of {sample.get_Concentration_field()}{sample.get_ConcentrationUnits_field()} is too low." \
                if sample.get_Concentration_field() else "Sample concentration is blank."
            return False, msg

        return True, None

    @staticmethod
    def can_add_to_wes(sample: SampleModel, exemplar_config: ExemplarConfigModel = None) -> (bool, str):
        """
        Checks if the sample meets the criteria to be added to WES.
        [OSLO-681]
        :param sample: The sample to check.
        :param exemplar_config: The ExemplarConfig record in the system. If not provided, it will be queried for.
        :return: True if queueing is possible, False otherwise, along with the reason why.
        """
        if sample.get_ExemplarSampleType_field() != "DNA":
            return False, "Sample is not a DNA sample."

        if (sample.get_Concentration_field() is None or
                sample.get_Concentration_field() <
                exemplar_config.get_WESLowConcentrationThreshold_field()):
            msg: str = f"Sample concentration of {sample.get_Concentration_field()}{sample.get_ConcentrationUnits_field()} is too low." \
                if sample.get_Concentration_field() else "Sample concentration is blank."
            return False, msg

        return True, None

    @staticmethod
    def get_orders_to_assay_details(rel_man: RecordModelRelationshipManager,
                                    assay_details: list[AssayDetailModel]) \
            -> dict[RequestModel, list[AssayDetailModel]]:
        """
        Creates a dict of orders to their linked assay details
        :param rel_man:
        :param assay_details: The assay details to get the orders for
        :return: dict of orders -> assay details
        """
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = {}

        # Get all the invoices
        rel_man.load_parents_of_type(assay_details, InvoiceModel)
        invoices: list[InvoiceModel] = []
        for a in assay_details:
            invoices.extend(a.get_parents_of_type(InvoiceModel))

        # Get all the orders from the invoices
        rel_man.load_parents_of_type(invoices, RequestModel)
        orders: list[RequestModel] = []
        for i in invoices:
            orders.extend(i.get_parents_of_type(RequestModel))

        # Map each order to their respective assay details
        rel_man.load_children_of_type(orders, InvoiceModel)
        rel_man.load_children_of_type(invoices, AssayDetailModel)
        for o in orders:
            current_assays: list[AssayDetailModel] = []
            for i in o.get_children_of_type(InvoiceModel):
                current_assays.extend(i.get_children_of_type(AssayDetailModel))

            orders_to_assay_details[o] = current_assays

        return orders_to_assay_details

    @staticmethod
    def get_matching_analysis_configurations(rec_handler: RecordHandler,
                                             assay_details: list[AssayDetailModel]) -> dict[
        AssayDetailModel, TestCodeConfigurationModel]:
        """
        Get the associated TestCodeConfiguration records of the specified AssayDetail records
        :param rec_handler: RecordHandler instance
        :param assay_details: List of AssayDetail records
        :return: Dict of AssayDetail records to their matching TestCodeConfiguration record
        """
        assay_details_to_test_code_configs: dict[AssayDetailModel, TestCodeConfigurationModel] = {}
        test_code_ids: list[str] = [a.get_TestCodeId_field() for a in assay_details]
        test_code_configs: list[TestCodeConfigurationModel] = rec_handler.query_models(TestCodeConfigurationModel,
                                                                                       TestCodeConfigurationModel.TESTCODEID__FIELD_NAME.field_name,
                                                                                       test_code_ids)
        for a in assay_details:
            assay_details_to_test_code_configs[a] = \
                [t for t in test_code_configs if t.get_TestCodeID_field() == a.get_TestCodeId_field()][0]

        return assay_details_to_test_code_configs

    @staticmethod
    def get_instrument_tracking_entry(exp_handler: ExperimentHandler) -> ElnEntryStep:
        """
        Gets the instrument tracking entry of an experiment by returning the entry with the INSTRUMENT TRACKING tag.
        :param exp_handler: ExperimentHandler
        :return: The instrument tracking entry.
        """
        return [e for e in exp_handler.get_all_steps(None) if "INSTRUMENT TRACKING" in e.get_options().keys()][0]

    @staticmethod
    def add_or_update_sdms_attachment_on_entry(eln_man: ElnManager, inst_man: RecordModelInstanceManager,
                                               experiment_id: int, entry_name: str,
                                               attachment: ExemplarSDMSFileModel) -> None:
        """
        Attach an ExemplarSDMSFile attachment to an entry. If the attachment is already on the entry
        :param eln_man: ElnManager
        :param inst_man: RecordModelInstanceManager
        :param experiment_id: The experiment ID.
        :param entry_name: The name of the attachment entry.
        :param attachment: The attachment record (ExemplarSDMSFileModel).
        """
        try:
            entry: ExperimentEntry = [e for e in eln_man.get_experiment_entry_list(experiment_id)
                                      if e.entry_name == entry_name][0]
        except Exception:
            raise Exception(f"Entry '{entry_name}' not found in experiment '{experiment_id}'")
        attachments: list[ExemplarSDMSFileModel] = (
            inst_man.add_existing_records_of_type(eln_man.get_data_records_for_entry(experiment_id, entry.entry_id)
                                                  .result_list, ExemplarSDMSFileModel))
        if not attachments or attachment not in attachments:
            crit = ElnAttachmentEntryUpdateCriteria()
            crit.record_id = attachment.record_id
            crit.attachment_name = attachment.get_FilePath_field()
            crit.template_item_fulfilled_timestamp = TimeUtil.now_in_millis()
            eln_man.update_experiment_entry(experiment_id, entry.entry_id, crit)

    @staticmethod
    def get_reagents_from_reagent_tracking_entry(user: SapioUser, rec_handler: RecordHandler,
                                                 inst_man: RecordModelInstanceManager,
                                                 exp_handler: ExperimentHandler,
                                                 reagent_tracking_entry: ElnEntryStep | ExperimentEntry) -> list[
                                                                                                                ConsumableItemModel] | None:
        """
        Get the reagents from the reagent tracking entry.
        :param user: SapioUser
        :param rec_handler: RecordHandler
        :param inst_man: RecordModelInstanceManager
        :param exp_handler: ExperimentHandler
        :param reagent_tracking_entry: The reagent tracking entry.
        :return: A list of reagents based on the entries values.
        """
        if isinstance(reagent_tracking_entry, ExperimentEntry):
            reagent_tracking_entry = exp_handler.get_step(reagent_tracking_entry.entry_name)
        reagent_tracking_records: list[PyRecordModel] = (
            inst_man.add_existing_records(exp_handler.get_step_records(reagent_tracking_entry)))
        if not reagent_tracking_records:
            return None

        columns: list[ReportColumn] = [ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)]

        raw_terms: list[list[RawReportTerm]] = []
        for record in reagent_tracking_records:
            raw_terms.append([RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            record.get_field_value("ConsumableType")),
                              RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            record.get_field_value("ConsumableLot"))])
        composite_terms: list[CompositeReportTerm] = []
        for raw_term in raw_terms:
            composite_terms.append(CompositeReportTerm(raw_term[0], CompositeTermOperation.AND_OPERATOR, raw_term[1]))
        root_term: CompositeReportTerm = composite_terms[0]
        if len(composite_terms) > 1:
            for composite_term in composite_terms[1:]:
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.OR_OPERATOR, composite_term)
        results: list[list[str]] = (
            DataMgmtServer.get_custom_report_manager(user)
            .run_custom_report(CustomReportCriteria(columns, root_term)).result_table)
        try:
            return rec_handler.query_models_by_id(ConsumableItemModel, [int(x[0]) for x in results])
        except Exception:
            return None

    @staticmethod
    def get_in_house_reagents_from_reagent_tracking_entry(user: SapioUser, rec_handler: RecordHandler,
                                                          inst_man: RecordModelInstanceManager,
                                                          exp_handler: ExperimentHandler,
                                                          reagent_tracking_entry: ElnEntryStep | ExperimentEntry) -> \
    list[InHouseModel] | None:
        """
        Get the reagents from the reagent tracking entry.
        :param user: SapioUser
        :param rec_handler: RecordHandler
        :param inst_man: RecordModelInstanceManager
        :param exp_handler: ExperimentHandler
        :param reagent_tracking_entry: The reagent tracking entry.
        :return: A list of reagents based on the entries values.
        """
        if isinstance(reagent_tracking_entry, ExperimentEntry):
            reagent_tracking_entry = exp_handler.get_step(reagent_tracking_entry.entry_name)
        reagent_tracking_records: list[PyRecordModel] = (
            inst_man.add_existing_records(exp_handler.get_step_records(reagent_tracking_entry)))

        columns: list[ReportColumn] = [ReportColumn(InHouseModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)]

        raw_terms: list[list[RawReportTerm]] = []
        for record in reagent_tracking_records:
            if not record.get_field_value("PartNumber"):
                continue
            raw_terms.append([RawReportTerm(InHouseModel.DATA_TYPE_NAME,
                                            InHouseModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            record.get_field_value("ConsumableType")),
                              RawReportTerm(InHouseModel.DATA_TYPE_NAME,
                                            InHouseModel.LOTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            record.get_field_value("ConsumableLot"))])
        composite_terms: list[CompositeReportTerm] = []
        for raw_term in raw_terms:
            composite_terms.append(CompositeReportTerm(raw_term[0], CompositeTermOperation.AND_OPERATOR, raw_term[1]))
        root_term: CompositeReportTerm = composite_terms[0]
        if len(composite_terms) > 1:
            for composite_term in composite_terms[1:]:
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.OR_OPERATOR, composite_term)
        results: list[list[str]] = (
            DataMgmtServer.get_custom_report_manager(user)
            .run_custom_report(CustomReportCriteria(columns, root_term)).result_table)
        try:
            return rec_handler.query_models_by_id(InHouseModel, [int(x[0]) for x in results])
        except Exception:
            return None


class SendSamplesToSNPID(OsloWebhookHandler):
    STATUS: str = "status"
    STATUS_ERROR: str = "error"
    STATUS_NOTICE: str = "notice"

    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {self.STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[self.STATUS] == self.STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[self.STATUS] == self.STATUS_NOTICE:
                return SapioWebhookResult(True)

        # Check to make sure that the sample is the only non-control sample in the project. If it is, then grab it.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        non_control: SampleModel | None = None
        num_non_controls: int = 0
        for sample in samples:
            if not sample.get_IsControl_field():
                num_non_controls += 1
                if num_non_controls > 1:
                    return SapioWebhookResult(True)
                non_control = sample

        # Get the order the sample is under and check if it's Send to SNP-ID field is marked as True. If so, then send
        # the source DNA sample to the SNP-ID workflow and notify the user about the action.
        top_level_sample: SampleModel = SampleUtils.get_top_level_samples(self.rec_handler, [non_control])[0]
        self.rel_man.load_parents_of_type([top_level_sample], RequestModel)
        order: RequestModel = top_level_sample.get_parent_of_type(RequestModel)
        process_name: str = context.active_protocol.get_options()["PROCESS"]
        try:
            order_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
                RequestUtils.get_orders_to_assay_details(self.rel_man, [order]))
            assay_detail: AssayDetailModel = [assay_detail for assay_detail in order_to_assay_details[order]
                                              if assay_detail.get_Assay_field() == process_name][0]
            test_code_config: TestCodeConfigurationModel = (
                self.rec_handler.query_models(TestCodeConfigurationModel,
                                              TestCodeConfigurationModel.TESTCODEID__FIELD_NAME.field_name,
                                              [assay_detail.get_TestCodeId_field()]))[0]
        except Exception:
            return SapioWebhookResult(True)

        if test_code_config.get_SendToSNPID_field():
            ProcessTracking.assign_to_process(context.user, SampleModel.DATA_TYPE_NAME, [non_control], "SNP-ID")
            self.response_map[self.STATUS] = self.STATUS_NOTICE
            return PopupUtil.ok_popup("Notice", f"The only non-control sample '{non_control.get_SampleId_field()}' has "
                                                f"been queued for SNP-ID.",
                                      request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)


class PopulateCompletedTests(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager
    response_map: dict[str, str]

    STATUS: str = "status"
    STATUS_ERROR: str = "error"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[self.STATUS] == self.STATUS_ERROR:
                return SapioWebhookResult(True)

        # Get the first entry in the experiment that contains samples. First, just try grabbing the first entry. If that
        # entry doesn't contain samples, look for the entry via tag. If we can't find the entry, throw an error to the
        # user.
        samples: list[SampleModel] | None = self.get_samples(context)
        if not samples:
            self.response_map[self.STATUS] = self.STATUS_ERROR
            return PopupUtil.ok_popup("Error", "No initial samples entry could be found in this experiment, so"
                                               " patients Completed Test field's cannot be updated. If the samples"
                                               " entry is not the first entry in this experiment, please add"
                                               " the \"INITIAL SAMPLES ENTRY\" tag to the entry.",
                                      request_context=json.dumps(self.response_map))

        # Get the ancestor Patient records from the samples.
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_path_of_type(top_level_samples, RelationshipPath().parent_type(RequestModel)
                                       .parent_type(PatientModel))
        patients: list[PatientModel] = []
        for top_level_sample in top_level_samples:
            patients.append(top_level_sample.get_parent_of_type(RequestModel).get_parent_of_type(PatientModel))

        # Get the current process and add it to each patient's Completed Tests field if it isn't already there.
        process: str | None = self.get_process_name(samples[0])
        if not process:
            return SapioWebhookResult(True)

        for patient in patients:
            completed_tests: str | None = patient.get_CompletedTests_field()
            if completed_tests:
                completed_tests_set: set[str] = set(patient.get_CompletedTests_field().split(","))
                completed_tests_set.add(process)
                patient.set_CompletedTests_field(",".join(completed_tests_set))
            else:
                patient.set_CompletedTests_field(process)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def get_samples(self, context: SapioWebhookContext) -> list[SampleModel] | None:
        entries: list[ExperimentEntry] = (
            self.eln_man.get_experiment_entry_list(context.eln_experiment.notebook_experiment_id))
        try:
            samples_entry: ExperimentEntry = entries[1]
            return self.rec_handler.wrap_models(self.eln_man.get_data_records_for_entry(
                context.eln_experiment.notebook_experiment_id, samples_entry.entry_id), SampleModel)
        except Exception:
            try:
                samples_entry: ExperimentEntry = utils.get_entry_by_option(context, "INITIAL SAMPLES ENTRY", entries)
                return self.rec_handler.wrap_models(self.eln_man.get_data_records_for_entry(
                    context.eln_experiment.notebook_experiment_id, samples_entry.entry_id), SampleModel)
            except Exception:
                return None

    def get_process_name(self, parent_sample: SampleModel) -> str | None:
        # Get the process name by grabbing it from the initial sample's parent assigned process record, or by one of the
        # aliquots if there's no assigned process record linked to the initial sample.
        self.rel_man.load_parents_of_type([parent_sample], AssignedProcessModel)
        try:
            return parent_sample.get_parent_of_type(AssignedProcessModel).get_ProcessName_field()
        except Exception:
            def sort_by_date_created(aliquot: SampleModel) -> int:
                return int(aliquot.get_field_value("DateCreated"))

            self.rel_man.load_children_of_type([parent_sample], SampleModel)
            aliquots: list[SampleModel] | None = parent_sample.get_children_of_type(SampleModel)
            if not aliquots:
                return None

            aliquots.sort(key=sort_by_date_created)
            self.rel_man.load_parents_of_type([aliquots[len(aliquots) - 1]], AssignedProcessModel)

            return aliquots[len(aliquots) - 1].get_parent_of_type(AssignedProcessModel).get_ProcessName_field()
