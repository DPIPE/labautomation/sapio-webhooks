from sapiopycommons.files.file_util import FileUtil
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FilePromptResult
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import IDReaderScanModel, SampleModel
from utilities.instrument_utils import InstrumentUtils

HEADERS: list[str] = ["RackID", "LocationCell", "TubeCode"]


class IDReaderUtils:
    @staticmethod
    def tokenize_file(result: FilePromptResult) -> list[dict[str, str]]:
        # Tokenize the data in the file
        try:
            file_data: list[dict[str, str]] = FileUtil.tokenize_csv(result.file_bytes, HEADERS)[0]

            return file_data

        except Exception as e:
            InstrumentUtils.log_instrument_error("IDReader", result.file_path, repr(e))

            return None

    @staticmethod
    def get_plate_id(file_name: str, file_data: list[dict[str, str]]) -> str:
        try:
            return file_name.rsplit("-")[1].replace(" ", "").replace(".csv", "")

        except Exception:
            plate_id: str = ""
            for i, d in enumerate(file_data, start=2):
                plate_id = d[HEADERS[0]]

                if plate_id != "":
                    return plate_id

            return plate_id

    @staticmethod
    def is_match(id_reader_scan: IDReaderScanModel, sample: SampleModel) -> bool:
        """
        Check if the ID reader scan matches with the sample
        :param id_reader_scan: the IDReader scan record
        :param sample: The sample
        :return: True if there is a match, False otherwise
        """
        return (id_reader_scan.get_TubeBarcode_field() == sample.get_TubeBarcode_field() and
                id_reader_scan.get_RowPosition_field() == sample.get_RowPosition_field() and
                id_reader_scan.get_ColPosition_field() ==  sample.get_ColPosition_field())
