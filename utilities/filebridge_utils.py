from sapiopycommons.files.file_bridge import FileBridge
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext

from utilities.instrument_utils import InstrumentUtils


class FileBridgeUtils:
    @staticmethod
    def upload_file(context: SapioWebhookContext, connection: str, file_name: str, file_bytes: bytes,
                    instrument_name: str, instrument_utils: InstrumentUtils = None) -> bool:
        """
        Upload a file to the specified FileBridge connection. If the upload fails, this function automatically creates
        and instrument file error upload record with details about the error.
        :param context: SapioWebhookContext
        :param connection: The name of the FileBridge connection configured in the app
        :param file_name: The name of the file to upload
        :param file_bytes: The data of the file to upload
        :param instrument_name: The name of the instrument
        :param instrument_utils: InstrumentUtils instance for writing files. Can be optionally passed in.
        :return: True if upload was successful, False otherwise
        """
        try:
            FileBridge.write_file(context, connection, file_name, file_bytes)
            return True
        except Exception as e:
            if instrument_utils is None:
                instrument_utils: InstrumentUtils = InstrumentUtils(context)
            instrument_utils.log_instrument_error(instrument_name, file_name, f"Error writing file with FileBridge."
                                                                              f" Details: {repr(e)}")
            return False
