from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.data_models import SampleModel, FamilyModel, FamilyDetailsModel, AssignedProcessModel


class FamilyUtils:
    @staticmethod
    def get_families(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, sample: SampleModel,
                     family_ids: list[str] = None) -> list[FamilyModel]:
        """
        Get the families that the specified sample belongs to with the specified family IDs
        :param rec_handler:
        :param rel_man:
        :param sample: The sample we're getting the families for
        :param family_ids: The family IDs to search for
        :return: list of family records
        """
        rel_man.load_children_of_type([sample], FamilyDetailsModel)

        if family_ids is None:
            family_details: list[FamilyDetailsModel] = sample.get_children_of_type(FamilyDetailsModel)
            family_ids: list[str] = [f.get_FamilyId_field() for f in family_details]

        return rec_handler.query_models(FamilyModel, FamilyModel.FAMILYID__FIELD_NAME.field_name, family_ids)

    @staticmethod
    def get_family_details(rec_handler: RecordHandler, family_id: str) -> list[FamilyDetailsModel]:
        """
        Get all the family details records matching the specified family ID
        :param rec_handler:
        :param family_id:
        :return: list of family details records
        """
        return rec_handler.query_models(FamilyDetailsModel, FamilyDetailsModel.FAMILYID__FIELD_NAME.field_name,
                                        [family_id])

    @staticmethod
    def increment_family_count(family: FamilyModel) -> None:
        """
        Increment the family count of the specified family record
        :param family: The family record
        """
        family_count: int = family.get_FamilyCount_field()
        if family_count is not None:
            family.set_FamilyCount_field(family_count + 1)

        else:
            family.set_FamilyCount_field(1)

    @staticmethod
    def get_family_map(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                       samples: list[SampleModel]) -> dict[FamilyModel, list[SampleModel]]:
        """
        Generate a dict of Family -> Samples
        :param rec_handler:
        :param rel_man:
        :param samples: The samples to map to their respective families
        :return: dict of Family -> Samples
        """
        family_map: dict[FamilyModel, list[SampleModel]] = {}

        # Get all the families of all the samples
        rel_man.load_children_of_type(samples, FamilyDetailsModel)

        all_family_details: list[FamilyDetailsModel] = []
        for s in samples:
            all_family_details.extend(s.get_children_of_type(FamilyDetailsModel))

        all_families: list[FamilyModel] = rec_handler.query_models(FamilyModel,
                                                                   FamilyModel.FAMILYID__FIELD_NAME.field_name,
                                                                   [fd.get_FamilyId_field() for fd in
                                                                    all_family_details])

        # Map each sample to its respective family
        rel_man.load_parents_of_type(all_family_details, SampleModel)
        for s in samples:
            family_detailses: list[FamilyDetailsModel] = [fd for fd in all_family_details if
                                                          fd.get_parent_of_type(SampleModel) == s]

            families: list[FamilyModel] = []
            for fd in family_detailses:
                families.extend([f for f in all_families if f.get_FamilyId_field() == fd.get_FamilyId_field()
                                 and f not in families])

            for f in families:
                if f not in family_map:
                    family_map[f] = []

                family_map[f].append(s)

        return family_map

    @staticmethod
    def get_family_samples(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                           family_id: str) -> list[SampleModel]:
        """
        Get all the samples under the specified family
        :param rec_handler:
        :param rel_man:
        :param family_id: The Family ID of the family to get the samples for
        :return: List of samples under the specified family
        """
        family_samples: list[SampleModel] = []

        family_detailses: list[FamilyDetailsModel] = FamilyUtils.get_family_details(rec_handler, family_id)

        rel_man.load_parents_of_type(family_detailses, SampleModel)

        for f in family_detailses:
            family_samples.append(f.get_parent_of_type(SampleModel))

        return family_samples

    @staticmethod
    def wgs_family_check(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, sample: SampleModel,
                         family_map: dict[FamilyModel, list[SampleModel]] = None) -> bool:
        """
        Evaluate the criteria for if the specified sample can be queued for the WGS process
        :param rec_handler:
        :param rel_man:
        :param sample: The sample to evaluate
        :param family_map: The family map of the sample. Can be optionally passed in to save on runtime
        :return: True if the sample can be queued for WGS, False otherwise
        """
        if (sample.get_ExemplarSampleType_field() != "DNA"
                and sample.get_SampleId_field() != sample.get_TopLevelSampleId_field()):
            return False

        if family_map is None:
            family_map: dict[FamilyModel, list[SampleModel]] = FamilyUtils.get_family_map(rec_handler, rel_man,
                                                                                          [sample])

        if family_map is None or len(family_map) == 0:
            return False

        family_samples: list[SampleModel] = []
        for f in family_map:
            family_samples.extend(family_map[f])

        rel_man.load_parents_of_type(family_samples, AssignedProcessModel)

        for f in family_map:
            if f.get_FamilyCount_field() != 3 and not f.get_SendAllSamplesToQueue_field():
                return False

            for s in family_map[f]:
                assigned_processes: list[AssignedProcessModel] = s.get_parents_of_type(AssignedProcessModel)
                for a in assigned_processes:
                    process_name: str = a.get_ProcessName_field()

                    if "Extraction" in process_name or "WGS" in process_name:
                        status: str = a.get_Status_field()

                        if "In Process -" in status or "Ready for -" in status:
                            return False

            if f.get_FamilyCount_field() == 3 or f.get_SendAllSamplesToQueue_field():
                return True
