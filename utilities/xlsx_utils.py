from typing import Any

from openpyxl.cell import Cell
from openpyxl.styles import Border, PatternFill
from openpyxl.styles.styleable import StyleableObject
from openpyxl.worksheet.worksheet import Worksheet


class XLSXUtils:
    @staticmethod
    def write_cell(worksheet: Worksheet, index: int, col: int, data: Any, border: Border | None = None,
                   fill: PatternFill | None = None) -> None:
        """
        Writes data to a cell in the worksheet.
        :param worksheet: Worksheet
        :param index: The row index.
        :param col: The column index.
        :param data: The data to write to the cell.
        :param border: The border to apply to the cell. Default is None.
        :param fill: The color to fill the cell with. Default is None.
        """
        cell: Cell = worksheet.cell(index, col)
        if fill:
            cell.fill = fill
        if border:
            cell.border = border
        if isinstance(data, float) and data:
            cell.value = round(data, 2)
        elif data:
            cell.value = str(data)
        else:
            cell.value = ""
