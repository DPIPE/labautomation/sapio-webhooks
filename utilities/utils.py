import time
from datetime import datetime
from typing import Optional, List, Dict, Any

from natsort import natsorted, ns
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import AbstractElnEntryUpdateCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import OptionDialogRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager
from sapiopylib.rest.utils.recordmodel.RecordModelWrapper import WrappedType

from utilities.data_models import SampleModel, RequestModel


def get_entry_by_option(context: SapioWebhookContext, option: str,
                        entry_list: Optional[List[ExperimentEntry]] = None) -> Optional[ExperimentEntry]:
    """
    Get the first entry in the context with the given entry option key, or None if no entry has that entry option.
    Providing the entry list avoids additional calls if one needs multiple entries from an experiment.
    """
    eln_manager = context.eln_manager
    exp_id = context.eln_experiment.notebook_experiment_id
    if entry_list is None:
        entry_list = eln_manager.get_experiment_entry_list(exp_id, False)

    for entry in entry_list:
        if eln_manager.get_experiment_entry_options(exp_id, entry.entry_id).keys().__contains__(option):
            return entry
    return None


def get_entries_by_option(context: SapioWebhookContext, option: str,
                          entry_list: Optional[List[ExperimentEntry]] = None) -> Optional[List[ExperimentEntry]]:
    """
    Get the first entry in the context with the given entry option key, or None if no entry hsa that name.
    Providing the entry list avoids additional calls if one needs multiple entries from an experiment.
    """
    return_entries: List[ExperimentEntry] = []
    eln_manager = context.eln_manager
    exp_id = context.eln_experiment.notebook_experiment_id
    if entry_list is None:
        entry_list = eln_manager.get_experiment_entry_list(exp_id, False)

    for entry in entry_list:
        if eln_manager.get_experiment_entry_options(exp_id, entry.entry_id).keys().__contains__(option):
            return_entries.append(entry)
    return return_entries


def get_records_from_entry_with_option(context: SapioWebhookContext, option: str,
                                       entry_list: Optional[List[ExperimentEntry]] = None) \
        -> Optional[List[DataRecord]]:
    eln_manager = context.eln_manager
    entry_with_option: ExperimentEntry = get_entry_by_option(context, option)

    if entry_with_option:
        return eln_manager.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                      entry_with_option.entry_id).result_list
    else:
        return None


def map_wrapped_model_by_field_value(models: List[WrappedType], field_name: str) -> Dict[str, WrappedType]:
    value_to_model: Dict[Any, WrappedType] = {}
    for model in models:
        value = model.get_field_value(field_name)
        value_to_model[value] = model
    return value_to_model


def get_current_date_in_format(date_format: str) -> str:
    now = datetime.now()
    date: str = now.strftime(date_format)
    return date


def initialize_entry(entry: ExperimentEntry, context: SapioWebhookContext):
    exp_id = context.eln_experiment.notebook_experiment_id
    update_crit = AbstractElnEntryUpdateCriteria(entry.entry_type)
    update_crit.template_item_fulfilled_timestamp = round(time.time() * 1000)
    context.eln_manager.update_experiment_entry(exp_id, entry.entry_id, update_crit)


def uninitialize_entry(entry: ExperimentEntry, context: SapioWebhookContext):
    exp_id = context.eln_experiment.notebook_experiment_id
    update_crit = AbstractElnEntryUpdateCriteria(entry.entry_type)
    update_crit.template_item_fulfilled_timestamp = None
    context.eln_manager.update_experiment_entry(exp_id, entry.entry_id, update_crit)


def disable_step(entry: ExperimentEntry, context: SapioWebhookContext) -> None:
    exp_id = context.eln_experiment.notebook_experiment_id
    update_crit = AbstractElnEntryUpdateCriteria(entry.entry_type)
    update_crit.entry_status = ExperimentEntryStatus.Disabled
    context.eln_manager.update_experiment_entry(exp_id, entry.entry_id, update_crit)


def collapse_entry(context, eln_entry: ExperimentEntry):
    exp_id = context.eln_experiment.notebook_experiment_id
    update_crit = AbstractElnEntryUpdateCriteria(eln_entry.entry_type)
    update_crit.collapse_entry = True
    context.eln_manager.update_experiment_entry(exp_id, eln_entry.entry_id, update_crit)


def get_file_bridge_name() -> str:
    return "baylor-file-bridge"


def error_dialog(error_message: str, title: str = "Error") -> OptionDialogRequest:
    return OptionDialogRequest(title, error_message, ["Ok"], 0, True)


def add_leading_zeroes(value: str, total_length: int) -> str:
    zeroes_to_add: int = total_length - len(value)
    zeroes_added_value: str = "0" * zeroes_to_add + value if zeroes_to_add > 0 else value
    return zeroes_added_value


def get_sorted_records_by_field(records, field_name):
    if not records:
        return list()
    sorted_records = natsorted(records, key=lambda rec: (
        rec.get_field_value(field_name)), alg=ns.IGNORECASE)
    return sorted_records


def get_days_since_date(date: int) -> int | None:
    """
    Calculate the number of days since the specified date.
    :param date: The date to calculate the number of days since.
    :return: The number of days since the specified date.
    """
    try:
        date_labelled = datetime.strptime(TimeUtil.millis_to_format(date, "%m/%d/%Y"), "%m/%d/%Y")
        today = datetime.strptime(TimeUtil.now_in_format("%m/%d/%Y"), "%m/%d/%Y")
        return max(0, (today - date_labelled).days)
    except Exception:
        return None


def map_orders_to_samples(rel_man: RecordModelRelationshipManager,
                          samples: list[SampleModel]) -> dict[RequestModel, list[SampleModel]]:
    """
    Map the orders to the samples.
    :param rel_man: RecordModelRelationshipManager
    :param samples: The list of samples to map to their parent orders.
    :return: A map if orders to their child samples.
    """
    orders_to_samples: dict[RequestModel, list[SampleModel]] = {}
    rel_man.load_parents_of_type(samples, RequestModel)
    for sample in samples:
        requests: list[RequestModel] = sample.get_parents_of_type(RequestModel)
        for request in requests:
            if request not in orders_to_samples:
                orders_to_samples[request] = []
            orders_to_samples[request].append(sample)
    return orders_to_samples
