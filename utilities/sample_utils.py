from typing import Type, Any

from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager, RecordModelManager
from sapiopylib.rest.utils.recordmodel.RecordModelWrapper import WrappedRecordModel, WrappedType
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath
from sqlalchemy.testing import AssertsExecutionResults
from sapiopylib.rest.utils.recordmodel.RecordModelWrapper import WrappedRecordModel

from utilities import utils
from utilities.constants import Constants
from utilities.data_models import SampleModel, RequestModel, InvoiceModel, AssayDetailModel, PatientModel, PlateModel, \
    AssignedProcessModel
from utilities.request_utils import RequestUtils


class SampleUtils:
    @staticmethod
    def register_sample(rec_handler: RecordHandler, acc_manager: AccessionManager,
                        is_pool: bool = False, sample_type: str = None, container_type: str = None,
                        shire: bool = False) -> SampleModel:
        """
        Adds a sample record and automatically assigns it an accession sample ID
        :param is_pool: Boolean determining if the sample is a pool sample or not. This affects the generated sample ID
        :param sample_type: Sample type
        :param container_type: Container type
        :param shire: If True, a Shire tube barcode will be generated instead of a default one. Default value is False.
        :param rec_handler: RecordHandler
        :param acc_manager: AccessionManager
        :return: The new sample
        """
        sample: SampleModel = rec_handler.add_model(SampleModel)

        if not is_pool:
            sample_id: str = f"{Constants.ACCESSIONING_SAMPLE_ID}{acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_SAMPLE_ID))[0]}"

        else:
            sample_id: str = f"{Constants.ACCESSIONING_POOL_SAMPLE_ID}{acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_POOL_SAMPLE_ID))[0]}"

        sample.set_SampleId_field(sample_id)
        sample.set_TopLevelSampleId_field(sample_id)
        if not shire:
            # tube_barcode: str = f"{Constants.ACCESSIONING_TUBE_BARCODE}{acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE))[0]}"
            tube_barcode: str = f"HZ{acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_VERSO_TUBE_BARCODE))[0]}"
        else:
            tube_barcode: str = SampleUtils.generate_shire_tube_barcode(acc_manager)
        sample.set_TubeBarcode_field(tube_barcode)

        if sample_type is not None:
            sample.set_ExemplarSampleType_field(sample_type)

        if container_type is not None:
            sample.set_ContainerType_field(container_type)

        return sample

    @staticmethod
    def register_samples(rec_handler: RecordHandler, acc_manager: AccessionManager, num_samples: int,
                         are_pools: bool = False, sample_type: str = None, container_type: str = None,
                         shire: bool = False) -> list[SampleModel]:
        samples: list[SampleModel] = rec_handler.add_models(SampleModel, num_samples)
        if not are_pools:
            sample_id_accs: list[str] = (
                acc_manager.accession_for_system(num_samples,
                                                 AccessionSystemCriteriaPojo(Constants.ACCESSIONING_SAMPLE_ID)))
        else:
            sample_id_accs: list[str] = (
                acc_manager.accession_for_system(num_samples,
                                                 AccessionSystemCriteriaPojo(Constants.ACCESSIONING_POOL_SAMPLE_ID)))

        if not shire:
            tube_barcode_accs: list[str] = (
                acc_manager.accession_for_system(num_samples,
                                                 AccessionSystemCriteriaPojo(
                                                     Constants.ACCESSIONING_VERSO_TUBE_BARCODE)))
        else:
            tube_barcode_accs: list[str] = SampleUtils.generate_shire_tube_barcode(acc_manager, num_samples)

        for x, sample in enumerate(samples):
            sample_id: str = f"{Constants.ACCESSIONING_SAMPLE_ID}{sample_id_accs[x]}"
            sample.set_SampleId_field(sample_id)
            sample.set_TopLevelSampleId_field(sample_id)
            if not shire:
                sample.set_TubeBarcode_field(f"HZ{tube_barcode_accs[x]}")
            else:
                sample.set_TubeBarcode_field(tube_barcode_accs[x])
            if sample_type:
                sample.set_ExemplarSampleType_field(sample_type)
            if container_type:
                sample.set_ContainerType_field(container_type)

        return samples

    @staticmethod
    def get_top_level_samples(rec_handler: RecordHandler, samples: list[SampleModel]) -> list[SampleModel]:
        """
        Gets all the top level samples of the specified samples. This function is mostly used for aliquots
        :param rec_handler:
        :param samples: The samples to get the top level samples of
        :return: list of top level samples
        """
        top_level_sample_ids: list[str] = [s.get_TopLevelSampleId_field() for s in samples]
        return rec_handler.query_models(SampleModel, SampleModel.SAMPLEID__FIELD_NAME.field_name, top_level_sample_ids)

    @staticmethod
    def create_aliquot(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                       acc_manager: AccessionManager, parent_sample: SampleModel, sample_type: str = None,
                       shire: bool | None = False, tube_barcode: str | None = None) -> SampleModel:
        """
        Creates an aliquot of the specified sample of the optionally specified type. Automatically sets the top level
        sample ID and generates an accessioned sample ID in the same fashion that Sapio does. 
        :param rec_handler:
        :param rel_man:
        :param acc_manager:
        :param parent_sample: The sample to create an aliquot for
        :param sample_type: The sample type of the aliquot. If no value is specified, the sample type is blank.
        :param shire: If True, a Shire tube barcode will be generated instead of a default one. Default value is False.
        :param tube_barcode: If specified, the aliquot will be assigned this tube barcode.
        :return: The new aliquot
        """
        rel_man.load_children_of_type([parent_sample], SampleModel)
        existing_children: list[SampleModel] | None = parent_sample.get_children_of_type(SampleModel)
        if not existing_children:
            num_children: int = 0
        else:
            num_children: int = len(existing_children)
        sample_id: str = f"{parent_sample.get_SampleId_field()}_{num_children + 1}"

        aliquot: SampleModel = rec_handler.add_model(SampleModel)
        aliquot.set_SampleId_field(sample_id)
        if not parent_sample.get_TopLevelSampleId_field() or parent_sample.get_TopLevelSampleId_field() == "":
            aliquot.set_TopLevelSampleId_field(parent_sample.get_SampleId_field())
        else:
            aliquot.set_TopLevelSampleId_field(parent_sample.get_TopLevelSampleId_field())

        if tube_barcode:
            aliquot.set_TubeBarcode_field(tube_barcode)
        else:
            if not shire:
                acc_tube_barcode: str = f"HZ{acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE))[0]}"
            else:
                acc_tube_barcode: str = SampleUtils.generate_shire_tube_barcode(acc_manager)
            aliquot.set_TubeBarcode_field(acc_tube_barcode)

        if sample_type is not None:
            aliquot.set_ExemplarSampleType_field(sample_type)

        parent_sample.add_child(aliquot)

        return aliquot

    @staticmethod
    def get_sample_order_test_codes(rel_man: RecordModelRelationshipManager, sample: SampleModel,
                                    order: RequestModel = None) -> list[str]:
        """
        Get all the test codes of the specified sample's order
        :param rel_man:
        :param sample: The sample to get the test codes for
        :param order: The order the sample is under. Optionally provided to reduce runtime
        :return: List of test codes
        """
        test_codes: list[str] = []

        if order is None:
            rel_man.load_parents_of_type([sample], RequestModel)
            order: RequestModel = sample.get_parent_of_type(RequestModel)

        rel_man.load_children_of_type([order], InvoiceModel)
        invoices: list[InvoiceModel] = order.get_children_of_type(InvoiceModel)

        rel_man.load_children_of_type(invoices, AssayDetailModel)
        assay_details: list[AssayDetailModel] = []
        for i in invoices:
            assay_details.extend(i.get_children_of_type(AssayDetailModel))

        test_codes.extend([a.get_TestCodeId_field() for a in assay_details])

        return test_codes

    @staticmethod
    def get_orders_to_assay_details(rel_man: RecordModelRelationshipManager,
                                    samples: list[SampleModel]) -> dict[RequestModel, list[AssayDetailModel]]:
        """
        Get a dict of orders -> assay details from the specified samples
        :param rel_man:
        :param samples: The list of samples to get the orders from
        :return: Dict of orders -> assay details
        """
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = {}

        # Get all the orders from the samples
        rel_man.load_parents_of_type(samples, RequestModel)
        orders: list[RequestModel] = [s.get_parent_of_type(RequestModel) for s in samples]

        # Get the invoices from each order
        rel_man.load_children_of_type(orders, InvoiceModel)
        invoices: list[InvoiceModel] = []
        for o in orders:
            invoices.extend(o.get_children_of_type(InvoiceModel))

        # Map each order to their respective assay details
        rel_man.load_children_of_type(orders, InvoiceModel)
        rel_man.load_children_of_type(invoices, AssayDetailModel)
        for o in orders:
            current_assays: list[AssayDetailModel] = []
            for i in o.get_children_of_type(InvoiceModel):
                current_assays.extend(i.get_children_of_type(AssayDetailModel))

            orders_to_assay_details[o] = current_assays

        return orders_to_assay_details

    @staticmethod
    def is_control(sample: SampleModel) -> bool:
        """
        Check if a sample is a control sample
        :param sample: The sample to check
        :return: True if the sample is a control sample, and False otherwise
        """
        return sample.get_IsControl_field() or (sample.get_ControlType_field() is not None
                                                and sample.get_ControlType_field() != "")

    @staticmethod
    def sort_samples_by_priority(samples: list[SampleModel], rec_handler: RecordHandler,
                                 rel_man: RecordModelRelationshipManager, assay_type: Type[WrappedRecordModel],
                                 top_level_samples: list[SampleModel] = None) -> None:
        """
        Sort the specified samples by priority, highest to lowest. If two samples have the same priority, sort them by
        their record ids.
        :param samples: The samples to sort
        :param rec_handler:
        :param rel_man:
        :param assay_type: The extension record type to grab the assay priority from.
        :param top_level_samples: List of top level samples of the samples. Can be optionally provided to save runtime
        """
        if top_level_samples is None:
            top_level_samples = SampleUtils.get_top_level_samples(rec_handler, samples)

        rel_man.load_parents_of_type(top_level_samples, RequestModel)
        orders: list[RequestModel] = []
        for x in top_level_samples:
            order = x.get_parent_of_type(RequestModel)
            if order:
                orders.append(order)
        if orders:
            rel_man.load_children_of_type(orders, assay_type)

        def criteria(sample: SampleModel) -> (int, int):
            try:
                top_level_sample: SampleModel = [t for t in top_level_samples
                                                 if t.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
                order: RequestModel = top_level_sample.get_parent_of_type(RequestModel)
            except IndexError:
                order: RequestModel | None = None

            if not order:
                priority: int = 4
            else:
                try:
                    assay_detail_ext: assay_type = order.get_child_of_type(assay_type)
                    if assay_detail_ext.get_field_value("Priority") == RequestUtils.PRIORITY_URGENT:
                        priority: int = 1
                    elif assay_detail_ext.get_field_value("Priority") == RequestUtils.PRIORITY_HIGH:
                        priority: int = 2
                    elif assay_detail_ext.get_field_value("Priority") == RequestUtils.PRIORITY_NORMAL:
                        priority: int = 3
                    else:
                        priority: int = 4
                except Exception:
                    priority: int = 4

            return priority, sample.record_id

        samples.sort(key=criteria)

    @staticmethod
    def get_patients_from_samples(context: SapioWebhookContext, rec_handler: RecordHandler,
                                  samples: list[SampleModel]) -> dict[PatientModel, list[SampleModel]]:
        """
        Get a dict of patients mapped to their corresponding samples.
        :param context:
        :param rec_handler: RecordHandler
        :param samples: The samples to link with patients.
        :return: A dict of patients -> samples.
        """
        patients_to_samples: dict[PatientModel, list[SampleModel]] = {}
        result_map: dict[int, list[DataRecord]] = DataMgmtServer.get_data_record_manager(
            context.user).get_ancestors_list([s.record_id for s in samples], SampleModel.DATA_TYPE_NAME,
                                             PatientModel.DATA_TYPE_NAME).result_map
        if not result_map:
            return None

        for record_id in result_map:
            patient: PatientModel = rec_handler.wrap_model(result_map[record_id][0], PatientModel)
            if patient not in patients_to_samples.keys():
                patients_to_samples[patient] = []
            patients_to_samples[patient].append([s for s in samples if s.record_id == record_id][0])

        return patients_to_samples

    @staticmethod
    def get_samples_to_patients(context: SapioWebhookContext, rec_handler: RecordHandler,
                                samples: list[SampleModel]) -> dict[SampleModel, PatientModel]:
        """
        Get a dict of samples mapped to their corresponding patients.
        :param context: SapioWebhookContext
        :param rec_handler: RecordHandler
        :param samples: The samples to link with patients.
        :return: A dict of sample -> patient.
        """
        samples_to_patients: dict[SampleModel, PatientModel] = {}
        result_map: dict[int, list[DataRecord]] = DataMgmtServer.get_data_record_manager(
            context.user).get_ancestors_list([s.record_id for s in samples], SampleModel.DATA_TYPE_NAME,
                                             PatientModel.DATA_TYPE_NAME).result_map
        for record_id in result_map:
            sample: SampleModel = [s for s in samples if s.record_id == record_id][0]
            samples_to_patients[sample] = rec_handler.wrap_model(result_map[record_id][0], PatientModel)

        return samples_to_patients

    @staticmethod
    def generate_shire_tube_barcode(acc_manager: AccessionManager, num_barcodes: int | None = None) -> list[str] | str:
        """
        Generate a Shire tube barcode.
        :param acc_manager: AccessionManager
        :return: A unique Shire tube barcode.
        """
        if not num_barcodes:
            accession_value: str = acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo("D"))[0]
            accession_value = utils.add_leading_zeroes(accession_value, 6)
            return f"D{accession_value[0:2]}-{accession_value[2:len(accession_value)]}"
        else:
            accession_values: list[str] = acc_manager.accession_for_system(num_barcodes,
                                                                           AccessionSystemCriteriaPojo("D"))
            accessions: list[str] = [utils.add_leading_zeroes(x, 6) for x in accession_values]
            return [f"D{x[0:2]}-{x[2:len(x)]}" for x in accessions]

    @staticmethod
    def remove_samples_from_plates(samples: list[SampleModel], rec_man: RecordModelManager,
                                   rel_man: RecordModelRelationshipManager) -> None:
        """
        Removes the specified samples from their plates.
        :param samples: List of samples to remove from their plates.
        :param rec_man: RecordModelManager (used for a store_and_commit() call)
        :param rel_man: RecordModelRelationshipManager
        """
        rel_man.load_parents_of_type(samples, PlateModel)
        for sample in samples:
            parent_plate: PlateModel | None = sample.get_parent_of_type(PlateModel)
            if not parent_plate:
                continue
            sample.set_StorageLocationBarcode_field(None)
            sample.set_RowPosition_field(None)
            sample.set_ColPosition_field(None)
            parent_plate.remove_child(sample)

        rec_man.store_and_commit()

    @staticmethod
    def get_current_process(sample: SampleModel,
                            rel_man: RecordModelRelationshipManager) -> AssignedProcessModel | None:
        """
        Get the current active process that a sample is under.
        :param sample: The sample object for which the current process is to be determined.
        :type sample: SampleModel
        :param rel_man: Relationship manager used to load and manage parent relationships of the sample.
        :type rel_man: RecordModelRelationshipManager
        :return: The current assigned process model if it exists and is "In Process", otherwise None.
        """
        rel_man.load_parents_of_type([sample], AssignedProcessModel)
        assigned_processes: list[AssignedProcessModel] = sample.get_parents_of_type(AssignedProcessModel)
        if not assigned_processes:
            return None
        try:
            return [assigned_process for assigned_process in assigned_processes
                    if "In Process -" in assigned_process.get_Status_field()][0]
        except Exception:
            return None

    @staticmethod
    def generate_next_sample_name(rel_man: RecordModelRelationshipManager, order: RequestModel | None = None,
                                  order_samples: list[SampleModel] | None = None) -> str | None:
        """
        Generate the next sample name for a sample under the specified order.
        :param rel_man: RecordModelRelationshipManager
        :param order: The order the sample is under. If the order is None, then the order_samples parameter must not be.
        :param order_samples: The samples under the order.
        :return: The new sample name (<order name><#>, where <#> is the number of samples under the order.)
        """
        if order_samples is None and not order:
            return None
        elif order_samples is None:
            rel_man.load_children_of_type([order], SampleModel)
            order_samples = order.get_children_of_type(SampleModel)

        if len(order_samples) == 0:
            val: int = 1
        else:
            sample_names: list[str] | None = \
                [x.get_OtherSampleId_field() for x in order_samples
                 if f"{order.get_RequestName_field()}" in x.get_OtherSampleId_field()]
            if not sample_names:
                val: int = 1
            else:
                val: int = max([int(x[len(order.get_RequestName_field()):len(x)]) for x in sample_names]) + 1
        return f"{order.get_RequestName_field()}{utils.add_leading_zeroes(str(val), 2)}"

    @staticmethod
    def map_patients_to_samples(rel_man: RecordModelRelationshipManager,
                                samples: list[SampleModel]) -> dict[PatientModel, list[SampleModel]]:
        """
        Map patients to their order's samples.
        :param rel_man: RecordModelRelationshipManager
        :param samples: The samples to map to their order IDs.
        :return: A dict of patients mapped to their corresponding samples.
        """
        patients_to_samples: dict[PatientModel, list[SampleModel]] = {}
        rel_man.load_path_of_type(samples, RelationshipPath().parent_type(RequestModel).parent_type(PatientModel))
        for sample in samples:
            patient: PatientModel = sample.get_parents_of_type(RequestModel)[0].get_parent_of_type(PatientModel)
            patients_to_samples.setdefault(patient, []).append(sample)
        return patients_to_samples

    @staticmethod
    def get_row_col_position_str(sample: SampleModel, include_zero: bool = False) -> str:
        """
        Get the row and column position of a sample as a string.
        :param sample: The sample to get the row and column position of.
        :param include_zero: If True, the column position will include a zero if the value is less than 10.
        :return: The row and column position of the sample as a string.
        """
        if include_zero and sample.get_ColPosition_field() and int(sample.get_ColPosition_field()) < 10:
            return f"{sample.get_RowPosition_field()}0{sample.get_ColPosition_field()}"
        return f"{sample.get_RowPosition_field()}{sample.get_ColPosition_field()}"

    @staticmethod
    def sort_samples_by_well_positions(samples: list[SampleModel], descending: bool = False,
                                       horizontal: bool = True) -> list[SampleModel]:
        """
        Sort the samples by their well positions. Default sorting is in ascending order. (A1 -> H12)
        :param samples: The samples to sort.
        :param descending: If True, the samples will be sorted in descending order. (H12 -> A1)
        :param horizontal: If True, the samples will be sorted horizontally (A1, B1, C1, ...). If False, (A1, A2, ...)
        :return: A list of the sorted samples.
        """

        def horizontal_sort(sample: SampleModel) -> (str, int):
            if sample.get_RowPosition_field() and sample.get_ColPosition_field():
                return sample.get_RowPosition_field(), int(sample.get_ColPosition_field())
            else:
                return "Z", 0

        def vertical_sort(sample: SampleModel) -> (int, str):
            if sample.get_RowPosition_field() and sample.get_ColPosition_field():
                return int(sample.get_ColPosition_field()), sample.get_RowPosition_field()
            else:
                return "Z", 0

        if horizontal:
            return sorted(samples, key=horizontal_sort, reverse=descending)
        else:
            return sorted(samples, key=vertical_sort, reverse=descending)

    @staticmethod
    def sort_samples_by_other_sample_id(samples: list[SampleModel]) -> list[SampleModel]:
        """
        Sort the samples by their OtherSampleId field, accounting for None values.
        :param samples: The samples to sort.
        :return: A list of the sorted samples.
        """
        return sorted(samples,
                      key=lambda sample: (sample.get_OtherSampleId_field() is None, sample.get_OtherSampleId_field()))
