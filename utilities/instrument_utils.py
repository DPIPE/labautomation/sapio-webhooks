from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import InstrumentFileUploadErrorModel


class InstrumentUtils:
    rec_handler: RecordHandler

    def __init__(self, context: SapioWebhookContext):
        self.rec_handler = RecordHandler(context)

    def log_instrument_error(self, instrument_name: str, file_name: str, description: str) -> SapioWebhookResult:
        error: InstrumentFileUploadErrorModel = self.rec_handler.add_model(InstrumentFileUploadErrorModel)
        error.set_Instrument_field(instrument_name)
        error.set_FileName_field(file_name)
        error.set_Description_field(description)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(False, "An instrument file error occurred. Check the Instrument Error logs for "
                                         "details.")
