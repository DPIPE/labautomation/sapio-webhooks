from typing import Any

from utilities.data_models import SampleModel


class PoolingUtils:
    HEADERS_HAMILTON_INPUT_FILE: list[str] = ["SourceLabware", "Source Well", "Destination Well", "Sample Volume",
                                              "Pool Volume"]
    HEADERS_PIPETTE_FILE: list[str] = ["Sample ID", "Sample Name", "Sample Volume", "Target Pool", "Pool Volume",
                                       "Pool Molarity"]

    PLATE_LOCATIONS: list[str] = [
        'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1',
        'A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2',
        'A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3',
        'A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4',
        'A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5',
        'A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6',
        'A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7',
        'A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8',
        'A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9',
        'A10', 'B10', 'C10', 'D10', 'E10', 'F10', 'G10', 'H10',
        'A11', 'B11', 'C11', 'D11', 'E11', 'F11', 'G11', 'H11',
        'A12', 'B12', 'C12', 'D12', 'E12', 'F12', 'G12', 'H12'
    ]

    HAMILTON_INPUT_FILE_NAME: str = "HamiltonPooling-[experiment ID]-[date].xls"
    PIPETTE_FILE_NAME: str = "ManualPipettingWorklist-[experiment ID]-[date].csv"

    HAMILTON_STAR_INSTRUMENT_NAME: str = "Hamilton Star"
    NOVASEQ_6000_INSTRUMENT_NAME: str = "NovaSeq 6000"
    NOVASEQ_X_INSTRUMENT_NAME: str = "NovaSeq X"

    DEST_TYPE_TUBE: str = "Tubes"
    DEST_TYPE_PLATE: str = "Plates"

    POOL_ACCESSION_VAL: str = "Pool-"

    MIN_HAMILTON_POOL_VOL: float = 1.5
    MAX_HAMILTON_POOL_VOL: float = 18.0

    PLATE_ROWS: int = 12
    PLATE_COLS: int = 8

    @staticmethod
    def calculate_volume_to_use(sample: SampleModel, pool_mol: float, pool_vol: float,
                                samples_in_pool: int) -> (float, list[dict[str, Any]]):
        try:
            sample_vol: float = (((pool_mol / samples_in_pool) * pool_vol) / sample.get_Concentration_field())
        except ZeroDivisionError:
            return 0, None

        if not (PoolingUtils.MIN_HAMILTON_POOL_VOL <= sample_vol <= PoolingUtils.MAX_HAMILTON_POOL_VOL):
            invalid_range_warning: dict[str, Any] = {
                "SampleId": sample.get_field_value("SampleId"),
                "Volume": sample_vol
            }

            return sample_vol, invalid_range_warning

        return sample_vol, None
