from dataclasses import dataclass
from typing import TypeAlias

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnEntryCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnEntryType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelInstanceManager
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager

from sapiopylib.rest.pojo.eln.ExperimentEntry import EntryRecordAttachment

from utilities.data_models import AttachmentModel


@dataclass
class FilePair:
    name: str
    data: bytes


@dataclass
class FileAttachmentParams:
    context: SapioWebhookContext
    file_name: str
    file_bytes: bytes

    entry_name: str | None = None
    exp_handler: ExperimentHandler | None = None
    inst_man: RecordModelInstanceManager | None = None
    rec_man: RecordModelManager | None = None


@dataclass
class MultiFileParams:
    context: SapioWebhookContext
    file_pairs: list[FilePair] | dict[str, bytes]

    exp_handler: ExperimentHandler | None = None
    inst_man: RecordModelInstanceManager | None = None
    rec_man: RecordModelManager | None = None


params_t: TypeAlias = FileAttachmentParams | MultiFileParams


def compute(param_pack, file_name: str, file_bytes: bytes, entry_name: str):
    file_record = param_pack.inst_man.add_new_record_of_type(AttachmentModel)
    file_record.set_FilePath_field(file_name)
    file_record.set_VersionNumber_field(1)
    param_pack.rec_man.store_and_commit()

    AttachmentUtil.set_attachment_bytes(
        param_pack.context,
        file_record,
        file_name,
        file_bytes
    )
    step = param_pack.exp_handler.get_step(entry_name)
    param_pack.exp_handler.update_step(step=step, entry_height=250)
    param_pack.exp_handler.set_step_records(step, [file_record])
    param_pack.exp_handler.initialize_step(step)
    param_pack.exp_handler.complete_step(step)


def compute_into_entry(param_pack: params_t, file: FilePair, exp_entry: ExperimentEntry):
    attach = param_pack.inst_man.add_new_record_of_type(AttachmentModel)
    attach.set_FilePath_field(file.name)
    attach.set_VersionNumber_field("1")
    param_pack.rec_man.store_and_commit()

    AttachmentUtil.set_attachment_bytes(
        param_pack.context,
        attach,
        file.name,
        file.data,
    )

    criteria = ElnAttachmentEntryUpdateCriteria()
    criteria.entry_attachment_list = [
        EntryRecordAttachment(file.name, attach.record_id)
    ]
    criteria.template_item_fulfilled_timestamp = TimeUtil.now_in_millis()
    criteria.entry_height = 250

    param_pack.context.eln_manager.update_experiment_entry(
        param_pack.context.eln_experiment.notebook_experiment_id,
        exp_entry.entry_id,
        criteria
    )


# TODO: Move the generated entries to below the entry that generates them
def add_attachment_entry(param_pack: params_t, name: str) -> ExperimentEntry:
    return param_pack.context.eln_manager.add_experiment_entry(
        param_pack.context.eln_experiment.notebook_experiment_id,
        ElnEntryCriteria(
            ElnEntryType.Attachment,
            name,
            "Attachment",
            param_pack.context.experiment_entry.order + 1
        )
    )


def set_managers(param_pack: params_t) -> params_t:
    if param_pack.rec_man is None:
        param_pack.rec_man = RecordModelManager(param_pack.context.user)

    if param_pack.inst_man is None:
        param_pack.inst_man = param_pack.rec_man.instance_manager

    if param_pack.exp_handler is None:
        param_pack.exp_handler = ExperimentHandler(param_pack.context)

    return param_pack


def attach_file_to_entry(param_pack: params_t) -> None:
    param_pack = set_managers(param_pack)

    # Collect names of entries to insert to
    if isinstance(param_pack, MultiFileParams):
        param_pack.entry_names: list[str] = []

        if isinstance(param_pack.file_pairs, list):
            for idx, elem in enumerate(param_pack.file_pairs):
                compute_into_entry(
                    param_pack,
                    elem,
                    add_attachment_entry(param_pack, f"Generated file ({idx})")
                )

        elif isinstance(param_pack.file_pairs, dict):
            for name, data in param_pack.file_pairs.items():
                compute_into_entry(
                    param_pack,
                    FilePair(name, data),
                    add_attachment_entry(param_pack, f"Generated file ({name})")
                )

    # If no entry is given (single obj mode only)
    elif param_pack.entry_name is None:
        compute_into_entry(
            param_pack,
            FilePair(param_pack.file_name, param_pack.file_bytes),
            add_attachment_entry(param_pack, "Generated file")
        )

    # # Insert file into entry
    # if isinstance(param_pack, MultiFileParams):
    #     if isinstance(param_pack.file_pairs, list):
    #         for idx, pp in enumerate(param_pack.file_pairs):
    #             compute(param_pack, pp.file_name, pp.data, pp.entry_names[idx])
    #     if isinstance(param_pack.file_pairs, dict):
    #         for name, data in param_pack.file_pairs.items():
    #             compute(param_pack, name, data, f"Generated file ({name})")
    else:
        compute(
            param_pack,
            param_pack.file_name,
            param_pack.file_bytes,
            param_pack.entry_name
        )
