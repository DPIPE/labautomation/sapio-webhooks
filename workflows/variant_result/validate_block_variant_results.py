import json
from typing import Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.MessengerService import SapioMessenger
from sapiopylib.rest.pojo.Message import VeloxMessage
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxIntegerFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import VariantResultModel
from utilities.variant_result_utils import VariantResultUtils
from utilities.webhook_handler import OsloWebhookHandler


class ValidateVariantResults(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        variant_results: list[VariantResultModel] = self.rec_handler.wrap_models(context.data_record_list,
                                                                                 VariantResultModel)

        for v in variant_results:
            v.set_Status_field(VariantResultUtils.STATUS_COMPLETED)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)


class BlockVariantResults(OsloWebhookHandler):
    KEY_INTERPRETER_VARIANT_RESULTS: str = "interpreter_variant_results"
    KEY_INTERPRETERLESS_VARIANT_RESULTS: str = "interpreterless_variant_results"

    rec_handler: RecordHandler
    sapio_messenger: SapioMessenger

    response_data: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.sapio_messenger = SapioMessenger(context.user)
        self.response_data = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            else:
                return self.confirm_blockage(context)

        # Get all the selected variant results
        variant_results: list[VariantResultModel] = self.rec_handler.wrap_models(context.data_record_list,
                                                                                 VariantResultModel)
        # Separate the variant results with and without interpreters
        interpreter_variant_results, interpreterless_variant_results = self.separate_results(variant_results)

        # If there are no interpreterless variant results, set the statuses to "Blocked" and notify each interpreter
        if len(interpreterless_variant_results) < 1:
            for v in variant_results:
                v.set_Status_field(VariantResultUtils.STATUS_BLOCKED)

                interpreters: list[str] = v.get_Interpreter_field().split(",")
                for i in interpreters:
                    self.sapio_messenger.send_message(VeloxMessage(f"Analysis {v.get_AnalysisId_field()} has been"
                                                                   f" blocked by {context.user.username}. Click the "
                                                                   f"link to review the results in Sapio, or view them "
                                                                   f"in ELLA to request another validation. "
                                                                   f"{self.create_record_url(context, v.record_id)}",
                                                                   interpreters))

            self.rec_man.store_and_commit()

            return SapioWebhookResult(True)

        # Otherwise, show a confirmation message, listing each variant result
        dict_list: list[dict[str, str]] = []
        for v in interpreterless_variant_results:
            dict_list.append({
                "Variant": v.get_Variant_field(),
                "RecordId": v.record_id
            })

        json_dict: dict[str, list[int]] = {
            self.KEY_INTERPRETER_VARIANT_RESULTS: [v.record_id for v in interpreter_variant_results],
            self.KEY_INTERPRETERLESS_VARIANT_RESULTS: [v.record_id for v in interpreterless_variant_results]
        }

        return PopupUtil.table_popup("Confirm Blockage", "Are you sure you want to block the following Variant Results"
                                                         "with no interpreters? Close this popup to cancel.",
                                     [VeloxStringFieldDefinition("confirm", "Variant", "Variant", "Variant"),
                                      VeloxIntegerFieldDefinition("confirm", "RecordId", "Record ID")],
                                     dict_list, request_context=json.dumps(json_dict))

    def separate_results(self, variant_results: list[VariantResultModel]) -> \
            (list[VariantResultModel], list[VariantResultModel]):
        interpreter_variant_results: list[VariantResultModel] = []
        interpreterless_variant_results: list[VariantResultModel] = []

        for v in variant_results:
            if v.get_Interpreter_field() is None or v.get_Interpreter_field() == "":
                interpreterless_variant_results.append(v)

            else:
                interpreter_variant_results.append(v)

        return interpreter_variant_results, interpreterless_variant_results

    def confirm_blockage(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.response_data = json.loads(context.client_callback_result.callback_context_data)

        record_ids: list[int] = (self.response_data[self.KEY_INTERPRETER_VARIANT_RESULTS] +
                                 self.response_data[self.KEY_INTERPRETERLESS_VARIANT_RESULTS])

        variant_results: list[VariantResultModel] = self.rec_handler.query_models_by_id(VariantResultModel, record_ids)

        interpreter_variant_results, interpreterless_variant_results = self.separate_results(variant_results)

        for v in interpreter_variant_results:
            v.set_Status_field(VariantResultUtils.STATUS_BLOCKED)

            interpreters: list[str] = v.get_Interpreter_field().split(",")
            for i in interpreters:
                self.sapio_messenger.send_message(VeloxMessage(f"Analysis {v.get_AnalysisId_field()} has been"
                                                               f"blocked by {context.user.username}. Click the link to "
                                                               f" review the results in Sapio, or view them in ELLA"
                                                               f" to request another validation. "
                                                               f"{self.create_record_url(context, v.record_id)}",
                                                               interpreters))

        for v in interpreterless_variant_results:
            v.set_Status_field(VariantResultUtils.STATUS_BLOCKED)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def create_record_url(self, context: SapioWebhookContext, record_id: int) -> str:
        user_url: str = context.user.url
        user_url = user_url[0:user_url.rfind("/")]
        user_url = user_url[0:user_url.rfind("/")]

        user_url += (f"/veloxClient/VeloxClient.html?app={context.user.guid}"
                     f"#dataType={VariantResultModel.DATA_TYPE_NAME};recordId={record_id};view=dataRecord")

        return user_url
