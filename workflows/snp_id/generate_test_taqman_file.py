import json
import random

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ELNExperimentModel
from utilities.data_models import SampleModel
from utilities.data_models import StudyModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_DOWNLOAD: str = "fileDownload"
TAQMAN_FILE_HEADERS: str = ("# Exported By : [USERNAME]\r\n"
                            "# Export Date : [DATE_TIME] [TIME_ZONE]\r\n"
                            "# Study Name : [STUDY_NAME]\r\n"
                            "# Experiment Type : Endpoint\r\n"
                            "# Instrument Type : QuantStudio™ 12K Flex Real-Time PCR System\r\n"
                            "# Software Version Number : 1.3\r\n"
                            "# Creation Date : [DATE_TIME] [TIME_ZONE]\r\n"
                            "# Created By : [USERNAME]\r\n"
                            "# Last Modified Date : [DATE_TIME] [TIME_ZONE]\r\n"
                            "# Last Modified By : [USERNAME]\r\n"
                            "# Template File Name : None\r\n"
                            "# Template Originating Study Name : None\r\n"
                            "# Template Creation Date : [DATE_TIME] [TIME_ZONE]\r\n"
                            "# Template Created By User ID : [USERNAME]\r\n"
                            "# Template Software Version Number :  1.3\r\n\r\n\r\n")
TAQMAN_SAMPLE_HEADERS: list[str] = ["Assay ID", "NCBI SNP Reference", "Sample ID", "Call"]


class GenerateTestTaqManFile(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_FILE_DOWNLOAD:
                return SapioWebhookResult(True)

        # Check if there's any samples. If there aren't, don't generate the file.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Pick Files for Samples", SampleModel)
        if not samples:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "There are no samples in the experiment, so a TaqMan Results file"
                                                 " cannot be generated.", request_context=json.dumps(self.response_map))

        # Write the headers and add the relevant info to them.
        file_data: str = ""
        headers: str = TAQMAN_FILE_HEADERS
        headers = headers.replace("[USERNAME]", context.user.username)
        headers = headers.replace("[DATE_TIME]", TimeUtil.now_in_format("%m/%d/%Y %H:%M"))
        headers = headers.replace("[TIME_ZONE]", str(TimeUtil.get_default_timezone()))
        experiment: ELNExperimentModel = (
            self.rec_handler.query_models_by_id(ELNExperimentModel, [context.eln_experiment.experiment_record_id]))[0]
        self.rel_man.load_parents_of_type([experiment], StudyModel)
        if experiment.get_parent_of_type(StudyModel):
            headers = headers.replace("[STUDY_NAME]", experiment.get_parent_of_type(StudyModel).get_StudyName_field())
        else:
            headers = headers.replace("[STUDY_NAME]", "None")
        file_data += headers

        # Write the data for each sample, passing each one just for simplicity.
        file_data += "\t".join(TAQMAN_SAMPLE_HEADERS) + "\r\n"
        for sample in samples:
            for x in range(16):
                file_data += (str(context.eln_experiment.notebook_experiment_id) + "\t" +
                              str(random.randint(1000, 9999)) + "\t" + sample.get_SampleId_field() + "\t" + "U/U\r\n")

        self.response_map[STATUS] = STATUS_FILE_DOWNLOAD

        current_time = TimeUtil.now_in_format('%m%d%Y')
        notebook_id = context.eln_experiment.notebook_experiment_id
        file_name: str = f"TaqManResult_{current_time}_{notebook_id}.txt"
        file_bytes: bytes = file_data.encode("utf-8")

        # Attach the file to the attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                file_bytes,
                "Generated Taqman File",
                self.exp_handler,
                self.inst_man
            )
        )

        # Download the file to the user with ANSI encoding.
        return FileUtil.write_file(
            file_name,
            file_bytes,
            request_context=json.dumps(self.response_map)
        )
