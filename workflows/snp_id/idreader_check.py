from dataclasses import dataclass
from functools import reduce
from operator import add
from typing import Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.ClientCallbackService import ClientCallback
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import DisplayPopupRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import PopupType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.data_models import IDReaderScanModel
from utilities.webhook_handler import OsloWebhookHandler

@dataclass
class FoundMap:
    sample: bool
    scan: bool
    
    def to_status(self) -> str:
        if self.sample and self.scan:
            return "Found"
        elif self.sample:
            return "Not found on plate"
        elif self.scan:
            return "Not found in experiment"


@dataclass
class SampleInfo:
    rack_id: str
    scan: IDReaderScanModel | None
    found: FoundMap


class IDReaderCheck(OsloWebhookHandler):
    exp_handler: ExperimentHandler

    def find_scan_by_barcode(self, scans: list[IDReaderScanModel], barcode: str) -> IDReaderScanModel | None:
        for scan in scans:
            if scan.get_TubeBarcode_field() == barcode:
                return scan
        return None

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)

        try:
            submitted_samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Pick Files for Samples",
                                                                                    SampleModel)
        except:
            try:
                submitted_samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
            except:
                cb = ClientCallback(context.user)
                cb.display_popup(DisplayPopupRequest("Alert", "Error: Cannot find required Entries", PopupType.Error))
                return SapioWebhookResult(True)

        self.rec_man.relationship_manager.load_parents_of_type(submitted_samples, PlateModel)
        parent_plates: list[PlateModel] = reduce(add, [s.get_parents_of_type(PlateModel) for s in submitted_samples])

        lookup: dict[str, SampleInfo] = {}

        # [OSLO-952]: Moved webservice calls outside of loop.
        self.rec_man.relationship_manager.load_children_of_type(parent_plates, SampleModel)
        id_reader_scans: list[IDReaderScanModel] | None = (
            self.rec_handler.query_models(IDReaderScanModel, IDReaderScanModel.PLATE__FIELD_NAME,
                                          [x.record_id for x in parent_plates]))
        for plate in parent_plates:
            children_of_plate: list[SampleModel] = plate.get_children_of_type(SampleModel)
            plate_scans: list[IDReaderScanModel] | None = \
                [x for x in id_reader_scans if x.get_Plate_field() == plate.record_id]

            for smpl in children_of_plate:
                lookup[smpl.get_TubeBarcode_field()] = SampleInfo(plate.get_PlateId_field(), None,
                                                                  FoundMap(True, False))

            for scan in plate_scans:
                barcode = scan.get_TubeBarcode_field()
                if lookup.get(barcode):
                    lookup[barcode].found.scan = True
                else:
                    lookup[barcode] = SampleInfo(plate.get_PlateId_field(), scan, FoundMap(False, True))

        # [OSLO-952]: Updated to work with existing records and updated entry name.
        row_records: list[DataRecord] | None = self.exp_handler.get_step_records("IDReader Values")
        if not row_records:
            rows: list[PyRecordModel] = self.exp_handler.add_eln_rows(self.exp_handler.get_step("IDReader Values"),
                                                                      len(lookup))
        else:
            rows: list[PyRecordModel] = self.inst_man.add_existing_records(row_records)

        def generate_well_pos(info: SampleInfo) -> str:
            if info.scan:
                return f"{info.scan.get_RowPosition_field()}{info.scan.get_ColPosition_field()}"
            else:
                return ""

        fields: list[dict[str, Any]] = []
        for barcode, info in lookup.items():
            row_fields: dict[str, Any] = {
                "RackID": info.rack_id,
                "IDReaderTubeBarcode": barcode,
                "WellPosition": generate_well_pos(info),
                "IdReaderStatus": info.found.to_status()
                }

            fields.append(row_fields)

        for idx, field_row in enumerate(fields):
            rows[idx].set_field_values(field_row)

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
