import json
from typing import cast

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentAttachmentEntry
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import ExemplarSDMSFileModel
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"


class AnalyzeTaqManQCResults(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # Check if there's a TaqMan file uploaded. If not, then show an error to the user.
        try:    # TODO: This case works if the record was uploaded manually directly to the entry without SDMS. Not sure why but should be looked into in the future along with the below routine.
            record: DataRecord = self.exp_handler.get_step_records("TaqMan Upload")[0]
        except Exception:
            # TODO: For some reason, we need to get the record ID of the attachment on the entry in order to grab it and can't use any of the helper functions. This should be looked into in the future.
            attachment: ExperimentAttachmentEntry = cast(ExperimentAttachmentEntry,
                                                         self.exp_handler.get_step("TaqMan Upload").eln_entry)
            try:
                record: DataRecord = (
                    DataMgmtServer.get_data_record_manager(context.user)
                    .query_data_records_by_id(ExemplarSDMSFileModel.DATA_TYPE_NAME,
                                              [attachment.record_id]).result_list)[0]
            except Exception:
                self.response_map[STATUS] = STATUS_ERROR
                return PopupUtil.ok_popup("Error", "No TaqMan Genotyper file has been uploaded to this experiment.",
                                          request_context=json.dumps(self.response_map))

        # Get all the replicates in the file and their passed/failed values.
        sample_ids_to_results: dict[str, list[str]] = self.get_sample_ids_to_results(context, record)

        # Check whether each sample passed or failed.
        pass_fail_results: dict[str, str] = self.get_pass_fail_results(sample_ids_to_results)

        # Append or update each result in the summary table entry.
        self.update_table(pass_fail_results)

        return SapioWebhookResult(True)

    def get_sample_ids_to_results(self, context: SapioWebhookContext, attachment: DataRecord) -> dict[str, list[str]]:
        sample_ids_to_results: dict[str, list[str]] = {}

        # Parse the file data.
        try:
            file_data: str = AttachmentUtil.get_attachment_bytes(context, attachment).decode("ANSI")
        except Exception:
            file_data: str = AttachmentUtil.get_attachment_bytes(context, attachment).decode()
        rows: list[str] = file_data.split("\n")

        # Get the first row with relevant data so that we can iterate from there.
        row_index: int = 0
        for row in rows:
            if "Assay ID" in row:
                row_index += 1
                break
            row_index += 1

        # Map each result to it's corresponding sample ID, stopping iteration at the next blank row.
        for x in range(row_index, len(rows)):
            if rows[x] == "\r" or rows[x] == "" or rows[x] == "\r\t\r":
                break
            values: list[str] = rows[x].split("\t")
            if values[2] not in list(sample_ids_to_results.keys()):
                sample_ids_to_results[values[2]] = []
            sample_ids_to_results[values[2]].append(values[3])

        return sample_ids_to_results

    def get_pass_fail_results(self, sample_ids_to_results: dict[str, list[str]]) -> dict[str, str]:
        pass_fail_results: dict[str, str] = {}
        pass_count: int = 0
        for sample_id in sample_ids_to_results:
            for result in sample_ids_to_results[sample_id]:
                if "/" in result:
                    pass_count += 1
            if pass_count >= 12:
                pass_fail_results[sample_id] = f"{pass_count}/{len(sample_ids_to_results[sample_id])}:Passed"
            else:
                pass_fail_results[sample_id] = f"{pass_count}/{len(sample_ids_to_results[sample_id])}:Failed"
            pass_count = 0
        return pass_fail_results

    def update_table(self, pass_fail_results: dict[str, str]) -> None:
        summary_entry: ElnEntryStep = self.exp_handler.get_step("TaqMan QC Summary")
        existing_records: list[DataRecord] = summary_entry.get_records()
        data_type_name: str = summary_entry.get_data_type_names()[0]

        for sample_id in pass_fail_results:
            try:
                record: DataRecord = [record for record in existing_records
                                      if record.get_field_value("SampleId") == sample_id][0]
                table_record: PyRecordModel = self.inst_man.add_existing_record(record)
            except Exception:
                table_record: PyRecordModel = self.inst_man.add_new_record(data_type_name)
                table_record.set_field_value("SampleId", sample_id)
            result: list[str] = pass_fail_results[sample_id].split(":")
            table_record.set_field_value("TaqManQC", result[1])
            table_record.set_field_value("TaqManResult", result[0])

        self.rec_man.store_and_commit()
