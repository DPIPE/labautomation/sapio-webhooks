import json
from typing import Any
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FilePromptResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import PlateModel, SampleModel, BatchModel, ELNSampleDetailModel
from utilities.webhook_handler import OsloWebhookHandler


class ParseQIASymphonyFile(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        result: ClientCallbackResult = context.client_callback_result

        if result is None:
            return FileUtil.request_file(context, "Upload File", ["xml"])[0]

        if result.user_cancelled:
            return SapioWebhookResult(True)

        if isinstance(result, FilePromptResult):
            return self.process_file_upload(result)

        return SapioWebhookResult(True)

    def process_file_upload(self, result: FilePromptResult) -> SapioWebhookResult:
        # Create list of aliquots
        dna_aliquots: list[SampleModel] = []

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_children_of_type(samples, SampleModel)

        # Get the Derived Samples entry
        derived_samples: ElnEntryStep = self.exp_handler.get_step("Derived Samples")

        # Get the root element
        root: Element = ElementTree.fromstring(result.file_bytes.decode('utf-8'))

        # Create a plate record if the plate doesn't already exist in the system
        plate_id: str = root.findall("PlateID")[0].text
        plates: list[PlateModel] = self.rec_handler.query_models(PlateModel, PlateModel.PLATEID__FIELD_NAME.field_name,
                                                                 [plate_id])

        if not plates:
            plate: PlateModel = self.rec_handler.add_model(PlateModel)
            plate.set_PlateId_field(plate_id)

        else:
            plate: PlateModel = plates[0]

        # Get the batch and sample data from the file
        batch_elements: list[Element] = root.findall("BatchTrack")
        if not batch_elements:
            return PopupUtil.ok_popup("Error", "No batches present in the file.")

        for b in batch_elements:
            batch: BatchModel = self.rec_handler.add_model(BatchModel)
            try:
                batch.set_BatchId_field(b.find("BatchID").text)

            except Exception:
                return PopupUtil.ok_popup("Error", "Invalid BatchID.")

            sample_elements: list[Element] = b.findall("SampleTrack")
            if not sample_elements:
                return PopupUtil.ok_popup("Error", "No samples found in the file.")

            for s in sample_elements:
                dna_aliquot: SampleModel = self.rec_handler.add_model(SampleModel)
                dna_aliquots.append(dna_aliquot)

                try:
                    parent_sample: SampleModel = \
                    [x for x in samples if x.get_SampleId_field() == s.find('SampleCode').text][0]
                    num_children: int = len(parent_sample.get_children_of_type(SampleModel))
                    if num_children > 0:
                        sample_id: str = parent_sample.get_SampleId_field() + f"_{num_children + 1}"

                    else:
                        sample_id: str = parent_sample.get_SampleId_field() + "_1"

                    dna_aliquot.set_SampleId_field(sample_id)
                    dna_aliquot.set_TubeBarcode_field(s.find('EluateTubeBarcode').text)
                    dna_aliquot.set_RowPosition_field(s.find('SampleOutputPos').text.split(":")[0])
                    dna_aliquot.set_ColPosition_field(s.find('SampleOutputPos').text.split(":")[1])
                    dna_aliquot.set_ExemplarSampleType_field("DNA")
                    dna_aliquot.set_TopLevelSampleId_field(parent_sample.get_TopLevelSampleId_field())
                    dna_aliquot.set_PlateId_field(plate_id)

                except Exception:
                    return PopupUtil.ok_popup("Error", "Invalid sample values.")

                # Add the new aliquot as a child of the sample, plate, and batch
                ([s for s in samples if s.get_SampleId_field() in dna_aliquot.get_SampleId_field()][0]
                 .add_child(dna_aliquot))
                plate.add_child(dna_aliquot)
                batch.add_child(dna_aliquot)

        # Store and commit
        self.rec_man.store_and_commit()

        # Add the sample to the Derived Samples entry
        derived_samples.add_records([d.get_data_record() for d in dna_aliquots])

        # Store and commit
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def xml_to_dict(self, xml_data: str) -> dict[str, Any]:
        data_dict: dict[str, Any] = {}

        # Get run data from the file
        root: Element = ElementTree.fromstring(xml_data)

        data_dict["PlateID"] = root.findall("PlateID")[0].text

        batch_elements: list[Element] = root.findall("BatchTrack")
        for b in batch_elements:
            batch_id: str = b.find("BatchID").text

            data_dict[batch_id] = {}

            sample_elements: list[Element] = b.findall("SampleTrack")
            for s in sample_elements:
                sample_id: str = s.find("SampleCode").text
                data_dict[batch_id][sample_id] = {}

                data_dict[batch_id][sample_id]["Tube Barcode"] = s.find("EluateTubeBarcode").text
                data_dict[batch_id][sample_id]["Rack Position"] = s.find("SamplePosition").text
                data_dict[batch_id][sample_id]["Plate Position"] = s.find("SampleOutputPos").text

        # Return a dialog of the data
        return data_dict
