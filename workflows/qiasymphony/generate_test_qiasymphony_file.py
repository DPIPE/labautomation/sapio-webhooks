from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.constants import Constants
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class GenerateTestQiasymphonyFile(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    acc_manager: AccessionManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)

        # Get all the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_children_of_type(samples, SampleModel)

        # Generate plate ID
        plate_id: str = (
            self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PLATE_ID)))[0]

        # Generate batch IDs
        batch_ids: list[str] = self.acc_manager.accession_for_system(4, AccessionSystemCriteriaPojo("TEST_BATCH_"))

        # Generate data for the samples.
        tube_barcodes: list[str] = (
            self.acc_manager.accession_for_system(len(samples),
                                                  AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE)))

        # Write data to a string
        file_data: str = (f"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                          f"<FullPlateTrack Type=\"String\" Class=\"FullPlateTrack\">\n"
                          f"\t<NofCols>12</NofCols>\n"
                          f"\t<NofRows>8</NofRows>\n"
                          f"\t<PlateID Type=\"String\">{Constants.ACCESSIONING_TEST_PLATE_ID}{plate_id}</PlateID>\n")

        batch_runs: int = 0
        for b in batch_ids:
            file_data += (f"\t<BatchTrack Type=\"Object\" Class=\"BatchTrack\">\n"
                          f"\t\t<BatchID Type=\"UInt\">{b}</BatchID>\n")

            for x in range(0 + (24 * batch_runs), (24 * (batch_runs + 1))):
                file_data += (f"\t\t<SampleTrack Type=\"Object\" Class=\"SampleTrack\">\n"
                              f"\t\t\t<SampleCode Type=\"String\">{samples[x].get_SampleId_field()}</SampleCode>\n"
                              f"\t\t\t<EluateTubeBarcode Type=\"String\">{tube_barcodes[x]}</EluateTubeBarcode>\n"
                              f"\t\t\t<SamplePosition Type=\"String\">{x + 1}</SamplePosition>\n"
                              f"\t\t</SampleTrack>\n")
            file_data += "\t</BatchTrack>\n"
            batch_runs += 1

        file_data += "</FullPlateTrack>"
        file_name: str = "QIAsymphony TEST.xml"

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(file_data, "utf-8"),
                None,
                self.exp_handler,
            )
        )

        # Download the file to the user
        return FileUtil.write_file(file_name, bytes(file_data, "utf-8"))
