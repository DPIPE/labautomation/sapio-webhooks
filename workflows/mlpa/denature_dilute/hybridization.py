from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import PlateModel, ProbeMixSampleModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class MLPA_Hybridization(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        plate_entry = PlateDesignerEntry(exp_handler.get_step("Dilution & Denature"), exp_handler)
        plates: list[PlateModel] = plate_entry.get_plates(PlateModel)
        self.rel_man.load_children_of_type(plates, SampleModel)

        all_samples: list[SampleModel] = []
        for plate in plates:
            all_samples += plate.get_children_of_type(SampleModel)

        self.rel_man.load_children_of_type(all_samples, ProbeMixSampleModel)
        all_probes: dict[str, list[ProbeMixSampleModel]] = {}

        for smpl in all_samples:
            probe = smpl.get_child_of_type(ProbeMixSampleModel)
            if not probe:
                continue

            probe_name = probe.get_ProbeMix_field()
            if probe_name not in all_probes.keys():
                all_probes[probe_name] = []

            all_probes[probe_name].append(probe)

        new_rows: list[PyRecordModel] = exp_handler.add_eln_rows("Probe Hybridization", len(all_probes.keys()))
        idx = 0
        for name, probes in all_probes.items():
            new_rows[idx].set_field_values({
                "ProbeMix": name,
                "NumOfReactions": len(probes),
                "VolumeOfProbes": round((len(probes) * 1.5) + 15) * 5,
                "VolumeOfSalsaMLPABuffer": round((len(probes) * 1.5) + 15) * 5,
                "TotalVolume": (round((len(probes) * 1.5) + 15) * 5) * 2,
                })

            idx += 1

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
