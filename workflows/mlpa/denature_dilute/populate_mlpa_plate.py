from collections import Counter

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PlateDesignerWellElementModel, ProbeMixSampleModel, PromegaModel, InHouseModel
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class Populate_MLPA_Plate(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        plate_entry = PlateDesignerEntry(exp_handler.get_step("Sample Plating"), exp_handler)

        PLATE_SIZE = 96 if (exp_handler.get_template_name().split(" ")[0] == "MLPA") else 48

        samples: list[SampleModel] = exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_children_of_type(samples, ProbeMixSampleModel)

        # First check if there are more than 96 samples/controls with any single probe 
        # mix. If so, the user cannot continue, because one probe mix cannot spread 
        # across multiple plates.
        sample_counter: dict[str, int] = Counter()
        sample_probe_map: dict[str, list[SampleModel]] = {}
        for smpl in samples:
            probe: ProbeMixSampleModel | None = smpl.get_child_of_type(ProbeMixSampleModel)

            if probe:
                probe_name: str = probe.get_ProbeMix_field()
                if not sample_counter[probe_name]:
                    sample_counter[probe_name] = 0
                    sample_probe_map[probe_name] = []

                sample_counter[probe_name] += 1
                sample_probe_map[probe_name].append(smpl)

                # If there are any prenatal samples, they need to be replicated.
                if smpl.get_Prenatal_field():
                    sample_probe_map[probe_name].append(smpl)

        for k, v in sample_counter.items():
            if v > PLATE_SIZE:
                return PopupUtil.ok_popup(
                        "Warning",
                        f"Too many samples in ProbeMix {k}. Please submit less samples"
                        )

        # Then check if there are more than 192 samples/controls in general. 
        # We can only include up to 2 plates in the run.`
        if sum(sample_counter.values()) > PLATE_SIZE * 2:
            return PopupUtil.ok_popup(
                    "Warning",
                    "Maximum 192 samples for 2 plates. Please submit less samples"
                    )

        # Sort by probe mix name alphabetically
        sorted_map: dict[str, list[SampleModel]] = dict(sorted(sample_probe_map.items()))

        samples_to_display = sorted(samples, key=SampleModel.get_OtherSampleId_field)

        # The plate name(s) must be generated as well. Name it “MLPA_[yymmdd]_[accessioned number]”. 
        # If there are two plates, append “_A” and “_B”.
        date = str(TimeUtil.now_in_format("%y%m%d"))
        accession_id = 1  # TODO

        plates: list[PlateModel] = plate_entry.get_plates(PlateModel)
        for idx, p in enumerate(plates):
            if len(plates) > 1:
                p.set_PlateId_field(f"MLPA_{date}_{accession_id}_{chr(ord('A') + idx)}")
            else:
                p.set_PlateId_field(f"MLPA_{date}_{accession_id}")

        if len(plates):
            self.rel_man.load_children_of_type(samples_to_display, PromegaModel)
            self.rel_man.load_children_of_type(samples_to_display, InHouseModel)

            # plate samples
            for idx, smpl in enumerate(samples_to_display):
                well_elem = self.inst_man.add_new_record_of_type(PlateDesignerWellElementModel)
                well_elem.set_field_value("SourceRecordId", smpl.get_field_value("RecordId"))
                well_elem.set_field_value("SourceDataTypeName", "Sample")
                well_elem.set_field_value("Layer", 1)

                if smpl.get_child_of_type(PromegaModel) or smpl.get_child_of_type(InHouseModel):
                    well_elem.set_field_value("IsControl", True)
                    well_elem.set_field_value("ControlType", "Probe Mix")
                else:
                    well_elem.set_field_value("IsControl", False)
                    well_elem.set_field_value("ControlType", None)

                if exp_handler.get_template_name().split(" ")[0] == "MLPA":
                    if idx < PLATE_SIZE:
                        row = chr(ord("A") + idx % 8)
                        col = int(idx / 8) + 1

                        well_elem.set_field_value("RowPosition", row)
                        well_elem.set_field_value("ColPosition", col)
                        well_elem.set_field_value("PlateRecordId", plates[0].get_field_value("RecordId"))

                    else:
                        i = idx - PLATE_SIZE
                        row = chr(ord("A") + i % 8)
                        col = int(i / 8) + 1

                        well_elem.set_field_value("RowPosition", row)
                        well_elem.set_field_value("ColPosition", col)
                        well_elem.set_field_value("PlateRecordId", plates[1].get_field_value("RecordId"))
                else:
                    if idx < PLATE_SIZE / 2:
                        row = chr(ord("A") + idx % 8)
                        col = int(idx / 8) + 1

                        well_elem.set_field_value("RowPosition", row)
                        well_elem.set_field_value("ColPosition", col)
                        well_elem.set_field_value("PlateRecordId", plates[0].get_field_value("RecordId"))

                    else:
                        i = idx - PLATE_SIZE
                        row = chr(ord("A") + i % 8)
                        col = int(i / 8) + 1

                        well_elem.set_field_value("RowPosition", row)
                        well_elem.set_field_value("ColPosition", col)
                        well_elem.set_field_value("PlateRecordId", plates[1].get_field_value("RecordId"))

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
