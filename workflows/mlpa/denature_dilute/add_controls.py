import re
from collections import Counter

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.ClientCallbackService import ClientCallback
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import DisplayPopupRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import PopupType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import InHouseModel
from utilities.data_models import ProbeMixModel
from utilities.data_models import ProbeMixSampleModel
from utilities.data_models import PromegaModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class MLPA_Add_Controls(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_handler = RecordHandler(context)
        exp_handler = ExperimentHandler(context)

        samples_counter: dict[str, int] = Counter()
        controls = set()

        samples: list[SampleModel] = exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_children_of_type(samples, ProbeMixSampleModel)

        for smpl in samples:

            probe_mix_sample: ProbeMixSampleModel | None = smpl.get_child_of_type(ProbeMixSampleModel)
            if not probe_mix_sample:
                cb = ClientCallback(context.user)
                cb.display_popup(DisplayPopupRequest("", f"No probe mix found for sample {smpl.get_SampleId_field()}", PopupType.Warning))
                return SapioWebhookResult(True)

            probe_name = probe_mix_sample.get_ProbeMix_field()

            probes: list[ProbeMixModel] = rec_handler.query_models(ProbeMixModel, "ProbeMixName", [probe_name])

            if probes:
                samples_counter[probe_name] += 1

                when_to_use = probes[0].get_WhenToUse_field()
                if when_to_use == "Always":
                    controls.add(probes[0].get_NormalControl1_field())
                    controls.add(probes[0].get_NormalControl2_field())
                    controls.add(probes[0].get_NormalControl3_field())
                    controls.add(probes[0].get_NormalControl4_field())
                    controls.add(probes[0].get_NormalControl5_field())
                else:
                    match = re.search(r"\d+", when_to_use)
                    if match:  # This should always trigger (<22)
                        if samples_counter[probe_name] >= int(match.group()):
                            controls.remove(probe_name)
                        else:
                            controls.add(probes[0].get_NormalControl1_field())
                            controls.add(probes[0].get_NormalControl2_field())
                            controls.add(probes[0].get_NormalControl3_field())
                            controls.add(probes[0].get_NormalControl4_field())
                            controls.add(probes[0].get_NormalControl5_field())
            else:
                cb = ClientCallback(context.user)
                cb.display_popup(DisplayPopupRequest("", f"No probe mix found for sample {smpl.get_SampleId_field()}", PopupType.Warning))
                return SapioWebhookResult(True)

        in_house_models: list[InHouseModel] = rec_handler.query_models(InHouseModel, "ConsumableType", controls)
        promega_models: list[PromegaModel] = rec_handler.query_models(PromegaModel, "ConsumableType", controls)

        if exp_handler.get_template_name().split(" ")[0] == "MLPA":
            filtered_inhouses = {item for item in in_house_models if item.get_LotNumber_field().endswith("_12")}
            filtered_promegas = {item for item in promega_models if item.get_LotNumber_field().endswith("_12")}
        else:
            filtered_inhouses = {item for item in in_house_models if item.get_LotNumber_field().endswith("_15")}
            filtered_promegas = {item for item in promega_models if item.get_LotNumber_field().endswith("_15")}

        self.rel_man.load_parents_of_type(list(filtered_inhouses), SampleModel)
        self.rel_man.load_parents_of_type(list(filtered_promegas), SampleModel)

        inhouses_to_insert = [x.get_parent_of_type(SampleModel) for x in filtered_inhouses]
        promegas_to_insert = [x.get_parent_of_type(SampleModel) for x in filtered_promegas]
        samples_to_insert = inhouses_to_insert + promegas_to_insert
        exp_handler.add_step_records("Samples", samples_to_insert)
        return SapioWebhookResult(True)
