import re
from typing import NamedTuple

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import MultiFileParams
from utilities.data_models import AssayDetailModel
from utilities.data_models import InvoiceModel
from utilities.data_models import PlateModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class SamplePair(NamedTuple):
    sample: SampleModel
    rack_pos: str


class Generate_dilution_files(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        cb_handler = CallbackUtil(context)
        exp_handler = ExperimentHandler(context)
        plate_entry = PlateDesignerEntry(exp_handler.get_step("Sample Plating"), exp_handler)

        samples: list[SampleModel] = exp_handler.get_step_models("Sample Plating", SampleModel)
        self.an_man.load_ancestors_of_type(self.inst_man.unwrap_list(samples), "Request")

        file_columns: list[str] = [
                 "Sample Name",
                 "Buffer Source Name",
                 "Buffer Source Pos",
                 "Destination Plate Name",
                 "Destination Plate Pos",
                 "Sample Source Name",
                 "Sample Source Pos",
                 "DNA Kons",
                 "DNA Vol",
                 "Buffer Vol",
                 "Probemix Rack Name",
                 "Probemix Rack Pos",
                 "Final Destination Plate Name",
                 "Final Destination Plate Pos",
                 "Probemix Vol",
                ]

        verso_pattern = re.compile(r"^H[A-Z]\w+")
        verso_samples = []
        not_verso_samples = []
        probe_mix_sample_map = {}

        global_row = "A"
        global_col = 1

        for smpl in samples:
            requests_recs: list[PyRecordModel] = list(self.an_man.get_ancestors_of_type(self.inst_man.unwrap(smpl), "Request"))
            requests: list[RequestModel] =  self.inst_man.wrap_list(requests_recs, RequestModel)

            if requests:
                self.rel_man.load_children_of_type(requests, InvoiceModel)
                invoice: InvoiceModel | None = requests[0].get_child_of_type(InvoiceModel)

                if invoice:
                    self.rel_man.load_children_of_type([invoice], AssayDetailModel)
                    assay = invoice.get_child_of_type(AssayDetailModel)

                    if assay:
                        probe_name = assay.get_ProbeMix_field()
                        if (probe_name) not in probe_mix_sample_map.keys():
                            probe_mix_sample_map[probe_name] = []

                        probe_mix_sample_map[probe_name].append(SamplePair(smpl, f"{global_row}{global_col}"))
                        if global_col < 8:
                            global_col += 1
                        else:
                            global_col = 0
                            global_row = chr(ord(global_row) + 1)

        sorted_dict = dict(
            sorted(
                probe_mix_sample_map.items(),
                key=lambda item: (-len(item[1]), item[0])
            )
        )

        for k, v in sorted_dict.items():
            for smpl in v:
                if re.search(verso_pattern, smpl.sample.get_TubeBarcode_field()):
                    verso_samples.append(smpl)
                else:
                    not_verso_samples.append(smpl)

        files = {
            "biomek_2d_dna.csv": self.to_bytes(self.generate_file_cols(file_columns, verso_samples, plate_entry)),
            "biomek_gdna.csv": self.to_bytes(self.generate_file_cols(file_columns, not_verso_samples, plate_entry)),
            "biomek_tebuffer_probemix.csv": self.to_bytes(self.generate_file_cols(file_columns, verso_samples + not_verso_samples, plate_entry)),
        }

        count = 0
        for k, v in files.items():
            if len(v) > len(self.to_bytes([file_columns])):
                cb_handler.write_file(k, v)
                count += 1

        not_count = 3 - count
        file_plural = "file" if count == 1 else "files"
        not_downloaded_message = ""

        if not_count > 0:
            not_file_plural = "file" if not_count == 1 else "files"
            was_were = "was" if not_count == 1 else "were"
            not_downloaded_message = f" {not_count} {not_file_plural} contained no data and {was_were} not downloaded."

        msg = f"{count} {file_plural} downloaded.{not_downloaded_message}"
        cb_handler.ok_dialog("Downloaded Files", msg)

        # Add to attachment entries
        attach_file_to_entry(
            MultiFileParams(context, files, self.exp_handler)
        )
        return SapioWebhookResult(True)

    def to_bytes(self, file: list[list[str]], *, sep: str = ",") -> bytes:
        csv_bytes = ""
        for row in file:
            r = sep.join([str(x) for x in row])
            csv_bytes += r + "\r\n"

        return bytes(csv_bytes, "utf-8")


    def generate_file_cols(self, columns: list[str], samples: list[SamplePair], plate_entry: PlateDesignerEntry) -> list[list[str]]:
        ret: list[list[str]] = []
        ret.append(columns)

        plates: list[PlateModel] = plate_entry.get_plates(PlateModel)
        self.rel_man.load_parents_of_type([x.sample for x in samples], SampleModel)
        self.rel_man.load_parents_of_type([x.sample for x in samples], PlateModel)

        for pair in samples:
            smpl = pair.sample
            parent_sample = smpl.get_parents_of_type(SampleModel)
            plate = smpl.get_parents_of_type(PlateModel)

            if len(plate):
                if plate[0] == plates[0]:
                    plate_name = "Plate_A"
                elif plate[0] == plates[1]:
                    plate_name = "Plate_B"
                else:
                    plate_name = " "

            dna_vol = smpl.get_Volume_field() if smpl.get_Concentration_field() >= 25 else 25

            line: list[str] = [
                smpl.get_OtherSampleId_field(),
                "TE",
                "1",
                smpl.get_StorageLocationBarcode_field(),
                f"{smpl.get_RowPosition_field()}{smpl.get_ColPosition_field()}",
                parent_sample[0].get_OtherSampleId_field(),
                parent_sample[0].get_StorageLocationBarcode_field(),
                smpl.get_Concentration_field(),
                dna_vol,
                str(25 - dna_vol), 
                "Probemixer",
                "", # Rack Pos
                pair.rack_pos,
                plate_name,
                smpl.get_StorageLocationBarcode_field(),
                "3",
            ]

            ret.append(line)

        return ret
