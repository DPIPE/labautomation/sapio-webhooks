from logging import Logger
from typing import List, Dict, Any, Tuple

from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager, \
    RecordModelInstanceManager
from sapiopylib.rest.utils.recordmodel.ancestry import RecordModelAncestorManager

from utilities.data_models import SampleModel, RequestModel, aCGHAssayDetailModel, AssayDetailModel, InvoiceModel, \
    PlateModel, PlateDesignerWellElementModel, PromegaModel
from utilities.sample_utils import SampleUtils


class PlateConfig:
    ROWS = 8
    COLS = 12
    PATIENT_COLUMNS = [1, 3, 5]
    REFERENCE_COLUMNS = [7, 9, 11]
    EXTENDED_COLUMNS = [4, 10]
    REFERENCE_COLUMN_MAPPING = {
        1: 7,
        3: 9,
        4: 10,
        5: 11
    }


class ACGHUtil:
    ARRAY_180K: str = "180k"
    ARRAY_1M: str = "1M"
    CAPACITY_180K: int = 4
    CAPACITY_1M: int = 1
    PROMEGA_SAMPLE_TYPE: str = "Promega"

    def __init__(self, rel_man: RecordModelRelationshipManager, logger: Logger, an_man: RecordModelAncestorManager):
        self.rel_man = rel_man
        self.logger = logger
        self.an_man = an_man

    def group_samples_by_array_size(self, samples: List[SampleModel]) -> tuple[
        dict[str, list[SampleModel]], dict[str | None, list[str | None]]]:

        self.an_man.load_ancestors_of_type([RecordModelInstanceManager.unwrap(model) for model in samples],
                                           RequestModel.DATA_TYPE_NAME.__str__())
        grouped_samples = {self.ARRAY_180K: [], self.ARRAY_1M: []}
        sample_id_to_values = {}
        samples = [s for s in samples if s.get_ExemplarSampleType_field() != self.PROMEGA_SAMPLE_TYPE]
        for sample in samples:
            array_size, priority = self.get_array_size_and_priority_for_sample(sample.backing_model)
            sample_id_to_values[sample.get_SampleId_field()] = [array_size, priority]
            if array_size:
                grouped_samples.get(array_size, []).append(sample)
            else:
                raise Exception(f"Could not determine array size for sample {sample.get_SampleId_field()}")

        return grouped_samples, sample_id_to_values

    def get_array_size_and_priority_for_sample(self, sample: PyRecordModel) -> tuple[str | None, str | None]:
        """
        Get the array size for a sample by checking its parent requests.

        :param sample: SampleModel object
        :return: Array size (ARRAY_180K or ARRAY_1M) or None if not found
        """
        requests: list[PyRecordModel] = list(
            self.an_man.get_ancestors_of_type(sample, RequestModel.DATA_TYPE_NAME.__str__()))
        if requests:
            self.rel_man.load_children(requests, aCGHAssayDetailModel.DATA_TYPE_NAME.__str__())
            for request in requests:
                acgh_detail = request.get_child_of_type(aCGHAssayDetailModel.DATA_TYPE_NAME.__str__())
                if acgh_detail:
                    array_size = acgh_detail.get_field_value(aCGHAssayDetailModel.C_ARRAY__FIELD_NAME.field_name)
                    priority = acgh_detail.get_field_value(aCGHAssayDetailModel.PRIORITY__FIELD_NAME.field_name)
                    if array_size in [self.ARRAY_180K, self.ARRAY_1M]:
                        return array_size, priority
                    else:
                        raise Exception(
                            f"Unknown array size {array_size} for sample {sample.get_field_value(SampleModel.SAMPLEID__FIELD_NAME.field_name)}"
                            f" in request {request.get_field_value(RequestModel.REQUESTID__FIELD_NAME.field_name)}")

        raise Exception(
            f"No valid aCGHAssayDetail found for sample {sample.get_field_value(SampleModel.SAMPLEID__FIELD_NAME.field_name)}")

    @staticmethod
    def calculate_arrays_needed(grouped_samples: Dict[str, List[SampleModel]]) -> Dict[str, int]:
        """
        Calculate the number of arrays needed based on grouped samples.

        :param grouped_samples: Dictionary with array sizes as keys and lists of samples as values
        :return: Dictionary with array sizes as keys and number of arrays needed as values
        """
        if not all(key in grouped_samples for key in [ACGHUtil.ARRAY_180K, ACGHUtil.ARRAY_1M]):
            raise Exception("grouped_samples dictionary is missing required keys")

        arrays_needed = {
            ACGHUtil.ARRAY_180K: (len(grouped_samples.get(ACGHUtil.ARRAY_180K, [])) + ACGHUtil.CAPACITY_180K - 1) // ACGHUtil.CAPACITY_180K,
            ACGHUtil.ARRAY_1M: len(grouped_samples.get(ACGHUtil.ARRAY_1M, []))
        }
        return arrays_needed

    # def get_sample_to_invoices(self, sample_to_orders: Dict[SampleModel, List[RequestModel]]) -> Dict[
    #     str, List[InvoiceModel]]:
    #     # Map samples to their corresponding invoices.
    #     all_orders = [order for orders in sample_to_orders.values() for order in orders]
    #     self.rel_man.load_children_of_type(all_orders, InvoiceModel)
    #
    #     sample_to_invoices = {}
    #     for sample, orders in sample_to_orders.items():
    #         for order in orders:
    #             invoices = order.get_children_of_type(InvoiceModel)
    #             if invoices:
    #                 sample_to_invoices.setdefault(sample.get_SampleId_field(), []).extend(invoices)
    #     return sample_to_invoices
    #
    # def get_samples_to_assay_details(self, sample_to_invoices: Dict[str, List[InvoiceModel]]) -> Dict[
    #     str, List[AssayDetailModel]]:
    #     # Map samples to their corresponding assay details."""
    #     all_invoices = [invoice for invoices in sample_to_invoices.values() for invoice in invoices]
    #     self.rel_man.load_children_of_type(all_invoices, AssayDetailModel)
    #     samples_to_assay_details = {}
    #     for sample_id, invoices in sample_to_invoices.items():
    #         for invoice in invoices:
    #             assay_details = invoice.get_children_of_type(AssayDetailModel)
    #             if assay_details:
    #                 for assay_detail in assay_details:
    #                     if assay_detail.get_Assay_field() == "aCGH":
    #                         samples_to_assay_details.setdefault(sample_id, []).append(assay_detail)
    #     return samples_to_assay_details

    def get_sample_to_orders_and_assay_detail(self, samples: List[SampleModel]) -> tuple[
        dict[str | None, RequestModel], dict[Any, Any]]:
        # Map samples to their corresponding orders.
        samples = [s for s in samples if s.get_ExemplarSampleType_field() != self.PROMEGA_SAMPLE_TYPE]
        sample_to_orders = {}
        self.an_man.load_ancestors_of_type([RecordModelInstanceManager.unwrap(model) for model in samples],
                                           RequestModel.DATA_TYPE_NAME.__str__())
        samples_to_assay_details = {}

        for sample in samples:
            requests: list[PyRecordModel] = list(
                self.an_man.get_ancestors_of_type(sample.backing_model, RequestModel.DATA_TYPE_NAME.__str__()))
            if requests:
                self.rel_man.load_children(requests, aCGHAssayDetailModel.DATA_TYPE_NAME.__str__())
                orders = [order for order in requests
                          if order.get_child_of_type(aCGHAssayDetailModel.DATA_TYPE_NAME.__str__())]
                if orders:
                    sample_to_orders[sample.get_SampleId_field()] = RecordModelInstanceManager.wrap(orders[0],
                                                                                                    RequestModel)

                    self.rel_man.load_children(orders, InvoiceModel.DATA_TYPE_NAME.__str__())
                    invoices = orders[0].get_children_of_type(InvoiceModel.DATA_TYPE_NAME.__str__())
                    if invoices:
                        self.rel_man.load_children(invoices, AssayDetailModel.DATA_TYPE_NAME.__str__())
                        assay_invoice = [
                            x for x in invoices
                            if x.get_field_value("AssayName") == "aCGH"
                        ]
                        if assay_invoice:
                            assay_detail = assay_invoice[0].get_children_of_type(
                                AssayDetailModel.DATA_TYPE_NAME.__str__())
                            if assay_detail:
                                if assay_detail[0].get_field_value("Assay") == "aCGH":
                                    samples_to_assay_details.setdefault(sample.get_SampleId_field(), []).append(
                                        assay_detail[0])
        return sample_to_orders, samples_to_assay_details


def get_acgh_column_layout(samples_by_array_size: Dict[str, List[SampleModel]]) -> Tuple[List[int], List[int]]:
    patient_columns = PlateConfig.PATIENT_COLUMNS.copy()
    reference_columns = PlateConfig.REFERENCE_COLUMNS.copy()

    total_samples = sum(len(samples) for samples in samples_by_array_size.values())

    if total_samples > 24 or "1M" in samples_by_array_size:
        patient_columns.append(PlateConfig.EXTENDED_COLUMNS[0])
        reference_columns.append(PlateConfig.EXTENDED_COLUMNS[1])

    return patient_columns, reference_columns


def auto_plate_acgh_samples_and_controls(samples_by_array_size: Dict[str, List[SampleModel]],
                                         controls: List[SampleModel], plate: PlateModel,
                                         well_elements: List[PlateDesignerWellElementModel],
                                         sample_id_to_promega_lot) -> None:
    patient_columns, reference_columns = get_acgh_column_layout(samples_by_array_size)
    well_index = 0
    a180k_samples = samples_by_array_size.get("180k", [])
    a1m_samples = samples_by_array_size.get("1M", [])
    a180k_samples: list[SampleModel] = SampleUtils.sort_samples_by_well_positions(a180k_samples)
    a1m_samples: list[SampleModel] = SampleUtils.sort_samples_by_well_positions(a1m_samples)

    _plate_samples_and_controls(patient_columns, a180k_samples, a1m_samples, plate, well_elements,
                                well_index, sample_id_to_promega_lot, controls)

    # add samples to the plate
    if a180k_samples:
        plate.add_children(a180k_samples)
    if a1m_samples:
        plate.add_children(a1m_samples)


def _plate_samples_and_controls(patient_columns: List[int], a180k_samples: List[SampleModel],
                                a1m_samples: List[SampleModel], plate: PlateModel,
                                well_elements: List[PlateDesignerWellElementModel], well_index: int,
                                sample_id_to_promega_lot, controls):
    for col in patient_columns:
        samples_to_plate = []
        if col != 4 and a180k_samples:
            for _ in range(min(8, len(a180k_samples))):
                samples_to_plate.append(a180k_samples.pop(0))
            for row, sample in enumerate(samples_to_plate):
                if well_index >= len(well_elements):
                    break
                well_location = f"{chr(65 + row)}{col}"
                _plate_sample(sample, plate, well_location, well_elements[well_index])
                well_index += 1
                lot_number = sample_id_to_promega_lot[sample.get_SampleId_field()]
                if sample and lot_number:
                    for control in controls:
                        promega = control.get_child_of_type(PromegaModel)
                        if promega and lot_number == promega.get_LotNumber_field():
                            ref_column = PlateConfig.REFERENCE_COLUMN_MAPPING.get(col)
                            well_location = f"{chr(65 + row)}{ref_column}"
                            _plate_control(control, plate, well_location, well_elements[well_index])
                            break
                well_index += 1
        elif col == 4 and a1m_samples:
            for _ in range(min(8, len(a1m_samples))):
                samples_to_plate.append(a1m_samples.pop(0))
            for row, sample in enumerate(samples_to_plate):
                if well_index >= len(well_elements):
                    break
                well_location = f"{chr(65 + row)}{col}"
                _plate_sample(sample, plate, well_location, well_elements[well_index])
                well_index += 1
                lot_number = sample_id_to_promega_lot[sample.get_SampleId_field()]
                if sample and lot_number:
                    for control in controls:
                        promega = control.get_child_of_type(PromegaModel)
                        if promega and lot_number == promega.get_LotNumber_field():
                            ref_column = PlateConfig.REFERENCE_COLUMN_MAPPING.get(col)
                            well_location = f"{chr(65 + row)}{ref_column}"
                            _plate_control(control, plate, well_location, well_elements[well_index])
                            break
                well_index += 1
        if not samples_to_plate:
            continue


def _plate_sample(sample_: SampleModel, plate_: PlateModel, well_location_: str,
                  plate_well_designer_element_: PlateDesignerWellElementModel) -> None:
    plate_well_designer_element_.set_PlateRecordId_field(plate_.record_id)
    plate_well_designer_element_.set_Layer_field(1)
    plate_well_designer_element_.set_RowPosition_field(well_location_[0:1])
    plate_well_designer_element_.set_ColPosition_field(well_location_[1:len(well_location_)])
    plate_well_designer_element_.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
    plate_well_designer_element_.set_IsControl_field(False)
    if sample_:
        plate_well_designer_element_.set_SourceRecordId_field(sample_.record_id)
    sample_.set_PlateId_field(plate_.get_PlateId_field())


def _plate_control(sample_: SampleModel, plate_: PlateModel, well_location_: str,
                   plate_well_designer_element_: PlateDesignerWellElementModel) -> None:
    plate_well_designer_element_.set_PlateRecordId_field(plate_.record_id)
    plate_well_designer_element_.set_Layer_field(1)
    plate_well_designer_element_.set_RowPosition_field(well_location_[0:1])
    plate_well_designer_element_.set_ColPosition_field(well_location_[1:len(well_location_)])
    plate_well_designer_element_.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
    plate_well_designer_element_.set_IsControl_field(True)
    plate_well_designer_element_.set_ControlType_field("Promega")
    if sample_:
        plate_well_designer_element_.set_SourceRecordId_field(sample_.record_id)
    sample_.set_PlateId_field(plate_.get_PlateId_field())
