import os
from io import StringIO
from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import AssayDetailModel
from utilities.data_models import ELNSampleDetailModel
from utilities.data_models import PatientModel
from utilities.data_models import PlateModel
from utilities.data_models import PrenatalSampleModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.acgh.acgh_util import ACGHUtil


class GenerateNxclinicalDescriptionFile(OsloWebhookHandler):
    HEADERS = [
        "Sample Name", "Filename", "Sample Type", "Processing Setting",
        "Array-ID", "Gender", "Control Gender", "Referanse", "Skjema",
        "Vev", "Test", "Henv.grunn", "SWL-ordrenummer",
        "SWL-prøvenummer", "Notes"
    ]
    FIELD_MAPPINGS = {
        "Sample Name": "OtherSampleId",
        "Filename": "FileName",
        "Sample Type": "SampleType2",
        "Processing Setting": "ProcessingSetting",
        "Array-ID": "ArrayID2",
        "Gender": "Gender",
        "Control Gender": "ControlGender",
        "Referanse": "Referanse",
        "Skjema": "Skjema",
        "Vev": "Vev",
        "Test": "Test",
        "Henv.grunn": "Henvgrunn",
        "SWL-ordrenummer": "SWLordrenummer",
        "SWL-prøvenummer": "SWLordrenummer2",
        "Notes": "Notes"
    }

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            exp_handler = ExperimentHandler(context)
            entry_rows = exp_handler.get_step_models("Generate NxClinical description file", ELNSampleDetailModel)

            step = self.exp_handler.get_step("Sample Plating")
            plates = PlateDesignerEntry(step, self.exp_handler).get_plates(PlateModel)
            plate_id = plates[0].get_PlateId_field() if plates else "plate_id"

            file_content: str | None = self._generate_file_content(entry_rows)
            if file_content:
                file_name = f"{plate_id}_Description.txt"

                params = FileAttachmentParams(
                    context,
                    file_name,
                    bytes(file_content, "utf-8"),
                    "Generated Description File",
                    exp_handler,
                    self.inst_man
                )
                attach_file_to_entry(params)

                self.callback.write_file(file_name, file_content.encode("utf-8"))
                return SapioWebhookResult(True)
            else:
                return SapioWebhookResult(False, "No data available to generate file content")
        except Exception as e:
            self.log_to_system(f"Error in GenerateNxclinicalDescriptionFile: {str(e)}")
            return SapioWebhookResult(False, f"An error occurred: {str(e)}")

    def _generate_file_content(self, entry_rows: List[ELNSampleDetailModel]) -> str | None:
        if not entry_rows:
            return None
        output = StringIO()
        output.write("\t".join(self.HEADERS) + "\n")
        for row in entry_rows:
            try:
                values = [str(row.get_field_value(self.FIELD_MAPPINGS[header]) or "") for header in self.HEADERS]
                output.write("\t".join(values) + "\n")
            except Exception as e:
                raise Exception(f"Error processing data: {str(e)}")

        return output.getvalue()


class PopulateNxClinicalDescStep(OsloWebhookHandler):
    DEFAULT_PATH = r"K:\Systemdata\MedtekUtstyr\UL-KLM-AMG-Microarray_Skanner-Prod\Data\Kjøringer\Resultatfiler\Prod\Skjema_1730"
    ENTRY_NAME = "Generate NxClinical description file"
    APPEND_CONTROLS_STEP = "Append Controls"
    FILE_SUFFIX = "_S001_CGH_1201_Sep17"
    ROW_LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    MAX_COLUMN = 12
    PLATE_ID = ""

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            # self.promega_controls = self._get_promega_controls()
            entry_rows = self._get_entry_rows()
            self._populate_entry_fields(entry_rows)
            self.rec_man.store_and_commit()
            return SapioWebhookResult(True)
        except Exception as e:
            self.logger.error(f"Unexpected error: {str(e)}", exc_info=True)
            return SapioWebhookResult(False, f"An unexpected error occurred: {str(e)}")

    def _get_entry_rows(self) -> List[ELNSampleDetailModel]:
        self.logger.info(f"Retrieving entry rows for {self.ENTRY_NAME}")
        return self.exp_handler.get_step_models(self.ENTRY_NAME, ELNSampleDetailModel)

    def _populate_entry_fields(self, entry_rows: List[ELNSampleDetailModel]) -> None:
        sample_ids = [row.get_field_value("SampleId") for row in entry_rows]
        samples = self._get_samples_with_relations(sample_ids)

        acgh_util = ACGHUtil(self.rel_man, self.logger, self.an_man)
        # Ignore controls
        samples = [s for s in samples if s.get_ExemplarSampleType_field() != ACGHUtil.PROMEGA_SAMPLE_TYPE]

        sample_to_orders, samples_to_assay_details = acgh_util.get_sample_to_orders_and_assay_detail(samples)
        sample_id_to_array_size = {}

        for sample in samples:
            array_size, priority = acgh_util.get_array_size_and_priority_for_sample(sample.backing_model)
            if array_size:
                sample_id_to_array_size[sample.get_SampleId_field()] = array_size
            else:
                sample_id_to_array_size[sample.get_SampleId_field()] = "N/A"

        step = self.exp_handler.get_step("Sample Plating")
        plates = PlateDesignerEntry(step, self.exp_handler).get_plates(PlateModel)
        self.PLATE_ID = plates[0].get_PlateId_field()
        self.rel_man.load_children_of_type(samples, PrenatalSampleModel)

        for row in entry_rows:
            sample = next((s for s in samples if s.get_SampleId_field() == row.get_field_value("SampleId")), None)
            if not sample:
                self.logger.warning(f"Sample not found for SampleId: {row.get_field_value('SampleId')}")
                continue
            assay_details = self.inst_man.wrap_list(samples_to_assay_details[sample.get_SampleId_field()],
                                                    AssayDetailModel)
            self._populate_row_fields(row, sample, assay_details,
                                      sample_id_to_array_size, sample_to_orders)

    def _get_samples_with_relations(self, sample_ids: List[str]) -> List[SampleModel]:
        """Retrieve samples and load their relations."""
        samples = self.rec_handler.query_models(SampleModel, "SampleId", sample_ids)
        self.rel_man.load_parents_of_type(samples, PlateModel)
        self.an_man.load_ancestors_of_type(self.inst_man.unwrap_list(samples), PatientModel.DATA_TYPE_NAME.__str__())
        return samples

    def _populate_row_fields(self, row: ELNSampleDetailModel, sample: SampleModel,
                             assay_details: List[AssayDetailModel], sample_id_to_array_size, sample_to_orders) -> None:

        order = sample_to_orders[sample.get_SampleId_field()]
        if order:
            row.set_field_value("OrderName",
                                order.get_RequestName_field() if order.get_RequestName_field() else order.get_RequestId_field())
            row.set_field_value("SWLordrenummer",
                                order.get_RequestName_field() if order.get_RequestName_field() else order.get_RequestId_field())

        patient = self.inst_man.wrap(
            self.an_man.get_ancestors_of_type(sample.backing_model, PatientModel.DATA_TYPE_NAME.__str__()).pop(),
            PatientModel)

        gender = row.get_field_value("ControlGender")
        gender_mapping = {
            "Male": ("XY", "Promega Male"),
            "Female": ("XX", "Promega Female")
        }
        if gender in gender_mapping:
            row.set_field_value("Gender", gender_mapping[gender][0])
            row.set_field_value("Referanse", gender_mapping[gender][1])

        row.set_field_value("Skjema", self.PLATE_ID if self.PLATE_ID else "")

        row.set_field_value("FileName", self._generate_filename(row, sample))
        row.set_field_value("SampleType2", assay_details[0].get_TestCodeId_field() if assay_details else "")
        row.set_field_value("Vev", sample.get_SampleSource_field() if assay_details else "")

        array_size = sample_id_to_array_size[sample.get_SampleId_field()]
        row.set_field_value("ArraySize", array_size)

        row.set_field_value("Test", assay_details[0].get_TestCodeId_field() if assay_details else "")
        row.set_field_value("Henvgrunn", patient.get_ReasonForReferral_field() if patient else "")
        row.set_field_value("Notes", patient.get_Comments_field() if patient else "")
        row.set_field_value("SWLordrenummer2", sample.get_OtherSampleId_field() if sample else "")

    def _generate_filename(self, row: ELNSampleDetailModel, sample: SampleModel) -> str:
        array_id = row.get_field_value("ArrayID2")
        if not array_id:
            return ""
        position = self._get_sample_position(sample)
        default_path = row.get_field_value("FileName") if row.get_field_value("FileName") else self.DEFAULT_PATH
        file_name = os.path.join(default_path,
                                 f"{sample.get_PlateId_field()}\\{array_id}{self.FILE_SUFFIX}_{position}.txt")
        return file_name

    def _get_sample_position(self, sample: SampleModel) -> str:
        row_letter = str(sample.get_RowPosition_field())
        try:
            row = self.ROW_LETTERS.index(row_letter) + 1
            col = int(sample.get_ColPosition_field())
            if 1 <= col <= self.MAX_COLUMN:
                return f"{row}_{col}"
        except (ValueError, IndexError):
            pass
        return ""
