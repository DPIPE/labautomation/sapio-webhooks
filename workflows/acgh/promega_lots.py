import time
from collections import defaultdict

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import PromegaModel, ELNSampleDetailModel, SampleModel, RequestModel, PrenatalSampleModel, \
    PatientModel, PromegaPartModel
from utilities.webhook_handler import OsloWebhookHandler


class AddPromegaLotsToAppendControlsStep(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        sample_details: list[ELNSampleDetailModel] = self.exp_handler.get_step_models("Append Controls",
                                                                                      ELNSampleDetailModel)
        self.rel_man.load_parents_of_type(sample_details, SampleModel)
        samples = [sample_detail.get_parent_of_type(SampleModel) for sample_detail in sample_details]
        sample_dict = {sample.get_SampleId_field(): sample for sample in samples}
        self.rel_man.load_path_of_type(samples, RelationshipPath().parent_type(RequestModel).parent_type(PatientModel))
        self.rel_man.load_children_of_type(samples, PrenatalSampleModel)

        # Query promega records
        promega_parts = self.rec_handler.query_all_models(PromegaPartModel)
        promega_records = self.rec_handler.query_all_models(PromegaModel)
        promega_dict = defaultdict(list)
        for promega in promega_records:
            promega_dict[promega.get_ConsumableType_field()].append(promega)
        current_millis = int(time.time() * 1000)

        lot_not_found_samples = list()

        for sample_detail in sample_details:
            sample = sample_dict[sample_detail.get_SampleId_field()]
            sex = ""
            # if prenatal sample, get prenatal sex
            if sample.get_Prenatal_field():
                prenatal_sample = sample.get_child_of_type(PrenatalSampleModel)
                if prenatal_sample:
                    sex = prenatal_sample.get_FetalSex_field()
                    sample_detail.set_field_value("FetalSex", sex)
            else:
                # Get sex of the patient under the order
                patient: PatientModel = sample.get_parents_of_type(RequestModel)[0].get_parent_of_type(PatientModel)
                if patient:
                    sex = patient.get_Sex_field()
                    sample_detail.set_field_value("PatientSex", sex)

            found_lot = False
            if sex in ["Male", "Female"]:
                suffix = "_M" if sex == "Male" else "_F"
                for promega_part in promega_parts:
                    consumable_type = promega_part.get_ConsumableType_field()

                    if consumable_type and consumable_type.endswith(suffix):
                        promega_records = promega_dict.get(consumable_type, [])
                        for promega in promega_records:
                            expiration_date = promega.get_ExpirationDate_field()

                            if expiration_date and int(expiration_date) > current_millis:
                                sample_detail.set_field_value("LotNumber", promega.get_LotNumber_field())
                                sample_detail.set_field_value("PromegaType", consumable_type)
                                found_lot = True
                                break
                    if found_lot:
                        break  # Exit the outer loop once a valid lot is found
            if not found_lot:
                lot_not_found_samples.append(
                    sample.get_OtherSampleId_field() if sample.get_OtherSampleId_field() else sample.get_SampleId_field())

        if lot_not_found_samples:
            self.callback.ok_dialog("Error",
                                    "Promega NOT found for the following samples \nOR the required promega lot is expired\nOR Fetal sex / Patient's sex is not found.\n\n" + ", ".join(lot_not_found_samples))

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)


class ACGHPromegaLots(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec = context.data_record_list[0]
        promega_type = rec.get_field_value("PromegaType")

        promega_records = self.rec_handler.query_models(PromegaModel,
                                                        PromegaModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                                                        [promega_type])

        # Filter non expired items only and get lot numbers list
        current_millis = int(time.time() * 1000)
        lot_numbers_to_return = [rec.get_LotNumber_field() for rec in promega_records if rec.get_LotNumber_field() and (
                 rec.get_ExpirationDate_field() and int(rec.get_ExpirationDate_field()) > current_millis)]

        return SapioWebhookResult(True, list_values=lot_numbers_to_return)


class UpdateSampleStepWithPromegaControls(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        # This webhook is expected to trigger from "Append Controls" entry submission
        sample_details: list[ELNSampleDetailModel] = self.exp_handler.get_step_models("Append Controls",
                                                                                      ELNSampleDetailModel)

        # Get selected lots
        lot_numbers = [rec.get_field_value("LotNumber") for rec in sample_details if rec.get_field_value("LotNumber")]
        if not lot_numbers:
            return SapioWebhookResult(True)

        # Get respective promega records
        promega_records = self.rec_handler.query_models(PromegaModel, PromegaModel.LOTNUMBER__FIELD_NAME.field_name,
                                                        lot_numbers)
        if not promega_records:
            return SapioWebhookResult(True, display_text="Promega records not found for the selected lot number(s)")

        # Get parent samples
        self.rel_man.load_parents_of_type(promega_records, SampleModel)
        parent_samples = []
        for promega in promega_records:
            sample = promega.get_parent_of_type(SampleModel)
            if sample:
                parent_samples.append(sample)

        if parent_samples:
            # Add promega samples to the samples step
            self.exp_handler.add_step_records("Samples", parent_samples)

        return SapioWebhookResult(True)
