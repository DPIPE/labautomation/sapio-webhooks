from collections import defaultdict
from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import SampleModel, ELNExperimentDetailModel, ELNSampleDetailModel, PatientModel, \
    PrenatalSampleModel, AssayDetailModel, PromegaModel, QCDatumModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.acgh.acgh_util import ACGHUtil


class PopulatePipettingDetailsSteps(OsloWebhookHandler):
    mass_conditions = {
        "Qubit": 150,
        "QiaXpert": 200
    }

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)

        # Using Aliquoted DNA Samples as it has the diluted sample values
        samples: List[SampleModel] = exp_handler.get_step_models("Aliquoted DNA Samples", SampleModel)
        if not samples:
            raise Exception("No samples found in the 'Aliquoted DNA Samples' step")

        acgh_util = ACGHUtil(self.rel_man, self.logger, self.an_man)

        sample_id_by_sample = RecordModelUtil.map_model_by_field_value(self.inst_man.unwrap_list(samples),
                                                                       "SampleId")

        microarray_details = exp_handler.get_step_models("Apply Samples to Microarrays", ELNSampleDetailModel)
        sample_id_by_array_details = RecordModelUtil.map_model_by_field_value(
            self.inst_man.unwrap_list(microarray_details),
            "SampleId")
        grouped_details = {}
        for detail in microarray_details:
            array_size = detail.get_field_value("ArraySize")
            if array_size not in grouped_details:
                grouped_details[array_size] = []
            grouped_details[array_size].append(detail)

        self.set_samples_count_for_hybridization(grouped_details, exp_handler)

        sample_details = self.inst_man.add_existing_records(self.exp_handler.get_step_records("Append Controls"))
        sample_id_by_control_details = RecordModelUtil.map_model_by_field_value(sample_details, "SampleId")

        sample_to_orders, samples_to_assay_details = acgh_util.get_sample_to_orders_and_assay_detail(samples)
        self.rel_man.load_parents_of_type(list(sample_to_orders.values()), PatientModel)

        table1_records: list[ELNSampleDetailModel] = self.exp_handler.get_step_models(
            "Pipetting amount for the patient samples", ELNSampleDetailModel)

        self.rel_man.load_path_of_type(samples,
                                       RelationshipPath().parent_type(SampleModel).child_type(PrenatalSampleModel))
        self.rel_man.load_path_of_type(samples,
                                       RelationshipPath().parent_type(SampleModel).child_type(QCDatumModel))

        _150g_mastermix_ref_180k_samples = defaultdict(list)
        samples_180k = grouped_details[ACGHUtil.ARRAY_180K]

        for row in table1_records:
            sample_id = row.get_field_value("SampleId")
            sample = self.inst_man.wrap(sample_id_by_sample[sample_id], SampleModel)
            source_sample = sample.get_parent_of_type(SampleModel)

            order = sample_to_orders[sample_id]
            if order:
                row.set_field_value("OrderName",
                                    order.get_RequestName_field() if order.get_RequestName_field() else order.get_RequestId_field())

            patient: PatientModel = order.get_parent_of_type(PatientModel)
            if patient:
                row.set_field_value("Nationalnumber", patient.get_SocialSecurityNumber_field())

            prenatal_sample = source_sample.get_child_of_type(PrenatalSampleModel)
            if prenatal_sample and prenatal_sample.get_FetalSex_field():
                sex = prenatal_sample.get_FetalSex_field()
            else:
                sex = patient.get_Sex_field()
            row.set_field_value("Sex", sex)

            row.set_field_value("SampleConcentration", sample.get_Concentration_field())
            if source_sample.get_ColPosition_field() and source_sample.get_RowPosition_field():
                row.set_field_value("SampleWellPosition",
                                    str(source_sample.get_RowPosition_field()) + str(source_sample.get_ColPosition_field()))

            latest_qc_datum = self.get_latest_qc_datum(source_sample)
            if latest_qc_datum:
                qc_method = latest_qc_datum.get_DatumType_field()
                row.set_field_value("QCMethod", qc_method)
                is_180k_sample = next((rec for rec in samples_180k if
                                       rec.get_field_value("OtherSampleId") == sample.get_OtherSampleId_field()),
                                      None)
                if is_180k_sample:
                    if qc_method in self.mass_conditions and sample.get_TotalMass_field()>= \
                            self.mass_conditions[qc_method]:
                        _150g_mastermix_ref_180k_samples[sex].append(sample)

            sample_volume = float(sample.get_Volume_field()) if sample.get_Volume_field() else 0
            water_volume = 18 - sample_volume

            row.set_field_value("SampleVolume2", sample_volume)
            row.set_field_value("SampleWaterVolume", water_volume)

            row.set_field_value("SampleMass", sample.get_TotalMass_field())

            assay_details = self.inst_man.wrap_list(samples_to_assay_details[sample.get_SampleId_field()],
                                                    AssayDetailModel)

            row.set_field_value("AnalysisCode", (assay_details[0].get_TestCodeId_field()) if assay_details else "")

            array_detail = sample_id_by_array_details[source_sample.get_SampleId_field()]
            row.set_field_value("ArrayID", array_detail.get_field_value("ArrayID") if array_detail else "")

            row.set_field_value("SampleType", sample.get_SampleSource_field())

            # Get values for promega 1M samples and <150ng 180k samples
            control_sample_detail = sample_id_by_control_details[source_sample.get_SampleId_field()]
            if control_sample_detail:
                lot_number = control_sample_detail.get_field_value("LotNumber")
                promega_models = self.rec_handler.query_models(PromegaModel,
                                                               PromegaModel.LOTNUMBER__FIELD_NAME.field_name,
                                                               [lot_number])
                if promega_models:
                    self.rel_man.load_parents_of_type(promega_models, SampleModel)
                    promega_sample: SampleModel = promega_models[0].get_parent_of_type(SampleModel)
                    ref_vol = float(
                        promega_sample.get_Volume_field()) if promega_sample.get_Volume_field() else 0
                    ref_water_volume = 18 - ref_vol

                    row.set_field_value("RefDNAVolume", ref_vol)
                    row.set_field_value("RefWaterVolume", ref_water_volume)

        # Populate table 2 records
        table2_records: list[ELNExperimentDetailModel] = self.exp_handler.get_step_models(
            "Pipetting amounts for reference mastermix", ELNExperimentDetailModel)
        for row in table2_records:
            if row.get_field_value("Typeofreference") == "Promega Male":
                promega_male_samples = _150g_mastermix_ref_180k_samples["Male"]
                row.set_field_value("Ref180k", len(promega_male_samples))
            if row.get_field_value("Typeofreference") == "Promega Female":
                promega_female_samples = _150g_mastermix_ref_180k_samples["Female"]
                row.set_field_value("Ref180k", len(promega_female_samples))

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def set_samples_count_for_hybridization(self, grouped_samples: dict[str, list[ELNSampleDetailModel]], exp_handler):
        pipetting_hybridiz_rows: List[ELNExperimentDetailModel] = exp_handler.get_step_models(
            "Pipetting amounts for hybridization master mix", ELNExperimentDetailModel)
        if pipetting_hybridiz_rows:
            samples_180k = grouped_samples.get(ACGHUtil.ARRAY_180K, [])
            samples__1M = grouped_samples.get(ACGHUtil.ARRAY_1M, [])
            for row in pipetting_hybridiz_rows:
                row.set_field_value("NumberOfA180kSamples", len(samples_180k))
                row.set_field_value("NumberOfA1MSamples", len(samples__1M))

    def get_latest_qc_datum(self, sample: SampleModel) -> QCDatumModel | None:
        qc_records = sample.get_children_of_type(QCDatumModel)
        if qc_records:
            latest_qc_datum = max(qc_records, key=lambda x: x.get_field_value("DateCreated"))
            if latest_qc_datum:
                return latest_qc_datum
