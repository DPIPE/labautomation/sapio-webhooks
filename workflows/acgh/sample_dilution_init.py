from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil

from utilities.data_models import PromegaModel, QCDatumModel, RequestModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.acgh.acgh_util import ACGHUtil


class ACGHSampleDilutionSet(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        rec_handler = RecordHandler(context)

        sample_detail_records = exp_handler.get_step_records("Append Controls")
        samples: list[SampleModel] = exp_handler.get_step_models("Generate Pick Files for Samples", SampleModel)

        sample_details = self.inst_man.add_existing_records(sample_detail_records)
        sample_id_by_sample_dict = RecordModelUtil.map_model_by_field_value(self.inst_man.unwrap_list(samples),
                                                                            "SampleId")

        sample_dilution_details = self.inst_man.add_existing_records(exp_handler.get_step_records("Sample Dilution"))
        sample_id_by_dilution_detail = RecordModelUtil.map_model_by_field_value(sample_dilution_details, "SampleId")

        self.rel_man.load_children_of_type(samples, QCDatumModel)
        self.an_man.load_ancestors_of_type([self.inst_man.unwrap(model) for model in samples],
                                           RequestModel.DATA_TYPE_NAME.__str__())
        acgh_util = ACGHUtil(self.rel_man, self.logger, self.an_man)

        samples_by_array_size, sample_id_to_values = acgh_util.group_samples_by_array_size(samples)

        a180k_samples = samples_by_array_size.get("180k", [])
        a1m_samples = samples_by_array_size.get("1M", [])

        for sample_detail in sample_details:

            sample = sample_id_by_sample_dict[sample_detail.get_field_value("SampleId")]
            vol = sample.get_field_value(SampleModel.VOLUME__FIELD_NAME.field_name) if sample.get_field_value(
                SampleModel.VOLUME__FIELD_NAME.field_name) else 0
            mass = sample.get_field_value(SampleModel.TOTALMASS__FIELD_NAME.field_name)
            if not mass:
                mass = 0.0

            # Determine target mass
            latest_qc_datum = self.get_latest_qc_datum(sample)
            qc_type = ""
            if latest_qc_datum:
                qc_type = latest_qc_datum.get_field_value(QCDatumModel.DATUMTYPE__FIELD_NAME.field_name)

            # For A180k samples and DNA quantified with QiaXpert = 200 ng
            # For A180k samples and DNA quantified with Qubit = 150 ng
            # For 1M samples and DNA quantified with QiaXpert = 400 ng
            # For 1M samples and DNA quantified with Qubit = 300 ng
            array_size = ""
            target_mass = 150
            for sample_ in a180k_samples:
                if sample_.get_SampleId_field() == sample.get_field_value("SampleId"):
                    array_size = "180k"
                    if qc_type == "QiaXpert":
                        target_mass = 200
                    break
            for sample_ in a1m_samples:
                if sample_.get_SampleId_field() == sample.get_field_value("SampleId"):
                    array_size = "1M"
                    if qc_type == "Qubit":
                        target_mass = 300
                    if qc_type == "QiaXpert":
                        target_mass = 400
                    break

            # Calculate the required volume to get 150 ng of mass
            if mass > target_mass:
                resulting_volume = 18
                resulting_sample_mass = target_mass
            elif mass <= target_mass and vol > 18:
                vol_ratio = 18 / vol
                resulting_sample_mass = mass * vol_ratio
                resulting_volume = 18
            else:
                # Take the entire available volume and mass
                resulting_volume = vol
                resulting_sample_mass = mass

            # Calculate the resulting concentration
            resulting_conc = resulting_sample_mass / resulting_volume if resulting_volume > 0 else 0

            # Set values on promega
            lot_number = sample_detail.get_field_value("LotNumber")
            promega_models = rec_handler.query_models(PromegaModel, PromegaModel.LOTNUMBER__FIELD_NAME.field_name,
                                                      [lot_number])

            if promega_models:
                self.rel_man.load_parents_of_type(promega_models, SampleModel)
                promega_sample: SampleModel = promega_models[0].get_parent_of_type(SampleModel)
                if promega_sample:
                    promega_mass = promega_sample.get_TotalMass_field()
                    if not promega_mass:
                        promega_mass = 0.0
                    promega_vol = promega_sample.get_Volume_field()
                    if not promega_vol:
                        promega_vol = 0.0

                    # Calculate the required volume to get 150 ng of mass from Promega
                    if promega_mass > target_mass:
                        if resulting_sample_mass > promega_mass:
                            promega_mass = target_mass
                        else:
                            promega_mass = resulting_sample_mass
                        required_volume_promega = 18
                    elif promega_mass <= target_mass and promega_vol > 18:
                        if resulting_sample_mass < target_mass:
                            vol_ratio_promega = 18 / promega_vol
                            promega_mass *= vol_ratio_promega
                        required_volume_promega = 18
                    else:
                        required_volume_promega = promega_vol

                    # Calculate the resulting concentration for Promega
                    resulting_promega_conc = promega_mass / required_volume_promega if required_volume_promega > 0 else 0

                    promega_sample.set_TotalMass_field(promega_mass)
                    promega_sample.set_Concentration_field(resulting_promega_conc)
                    promega_sample.set_Volume_field(required_volume_promega)

                    #Doesn't work
                    # # Set values on "Sample Dilution" record
                    # promega_dilution_detail = sample_id_by_dilution_detail[promega_sample.get_field_value("SampleId")]
                    # promega_dilution_detail.set_field_values({
                    #     "SourceMass2": promega_mass,
                    #     "SourceVolume": required_volume_promega
                    # })

            # Set values on related sample
            sample.set_field_values({
                "TotalMass": resulting_sample_mass,
                "Volume": resulting_volume,
                "Concentration": resulting_conc
            })

            # Set values on "Sample Dilution" record
            dilution_detail = sample_id_by_dilution_detail[sample.get_field_value("SampleId")]
            dilution_detail.set_field_values({
                "SourceMass2": resulting_sample_mass,
                "SourceVolume": resulting_volume,
                "QCMethod": qc_type,
                "ArraySize": array_size
            })

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def get_latest_qc_datum(self, sample: PyRecordModel) -> PyRecordModel | None:
        qc_records = sample.get_children_of_type(QCDatumModel.DATA_TYPE_NAME.__str__())
        if qc_records:
            latest_qc_datum = max(qc_records, key=lambda x: x.get_field_value("DateCreated"))
            if latest_qc_datum:
                return latest_qc_datum
