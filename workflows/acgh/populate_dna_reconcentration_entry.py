from typing import cast

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class PopulateDNAReconcentrationEntry(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get all the samples from the initial samples entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Pick Files for Samples", SampleModel)
        if not samples:
            return SapioWebhookResult(True)

        # Grab all the non-promega samples and add them to the table.
        non_promega_samples: list[SampleModel] | None = \
            [x for x in samples if x.get_ExemplarSampleType_field() != "Promega"]
        if not non_promega_samples:
            return SapioWebhookResult(True)
        entry: ElnEntryStep = cast(ElnEntryStep, context.active_step)
        self.exp_handler.add_step_records(entry, non_promega_samples)

        # Set the requires_grabber_plugin field on the entry to False so that we don't get prompted to add new or
        # existing samples upon clicking the entry.
        self.exp_handler.update_step(cast(entry, context.active_step), requires_grabber_plugin=False)

        return SapioWebhookResult(True)
