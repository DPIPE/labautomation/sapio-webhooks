from typing import List, Dict

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, ELNExperimentDetailModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.acgh.acgh_util import ACGHUtil


class CreateRowsForMicroarrayScanning(OsloWebhookHandler):
    SCAN_MICROARRAYS_STEP: str = "Scan Microarrays"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)

        try:
            samples: List[SampleModel] = exp_handler.get_step_models("Samples", SampleModel)
            if not samples:
                raise Exception("No samples found in the experiment.")

            acgh_util = ACGHUtil(self.rel_man, self.logger, self.an_man)
            grouped_samples, sample_id_to_priority = acgh_util.group_samples_by_array_size(samples)
            arrays_needed = acgh_util.calculate_arrays_needed(grouped_samples)

            self.create_and_populate_scan_microarray_rows(exp_handler, arrays_needed)

            self.rec_man.store_and_commit()
            return SapioWebhookResult(True)
        except Exception as e:
            self.log_to_system(f"Error: {str(e)}")
            CallbackUtil(context).ok_dialog("Error", str(e))
            return SapioWebhookResult(True)

    def create_and_populate_scan_microarray_rows(self, exp_handler: ExperimentHandler,
                                                 arrays_needed: Dict[str, int]) -> None:
        """
        Create and populate rows in the Scan Microarrays entry with the appropriate ArraySize.
        """
        scan_microarrays_step = exp_handler.get_step(self.SCAN_MICROARRAYS_STEP)
        if not scan_microarrays_step:
            raise ValueError(f"{self.SCAN_MICROARRAYS_STEP} step not found in the experiment.")

        for array_size, count in arrays_needed.items():
            for _ in range(count):
                row = exp_handler.add_eln_rows(self.SCAN_MICROARRAYS_STEP, 1)[0]
                row.set_field_value("ArraySize", array_size)
