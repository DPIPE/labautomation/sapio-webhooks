from collections import defaultdict
from typing import List, Dict, Tuple, Optional

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, aCGHAssayDetailModel, RequestModel, ELNExperimentDetailModel, \
    ELNSampleDetailModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.acgh.acgh_util import ACGHUtil


class SortSamplesAndGenerateArrayIds(OsloWebhookHandler):
    SCAN_MICROARRAYS_STEP: str = "Scan Microarrays"
    SAMPLE_MICROARRAYS_STEP: str = "Apply Samples to Microarrays"
    SAMPLE_PLATING_STEP: str = "Sample Plating"
    array_size_cache: Dict[int, str] = {}

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            self.check_required_entries()

            plated_samples = self._get_plated_samples()

            sorted_samples = self._sort_samples_by_plate_position(plated_samples)

            acgh_util = ACGHUtil(self.rel_man, self.logger, self.an_man)
            grouped_samples, sample_id_to_values = acgh_util.group_samples_by_array_size(sorted_samples)

            array_barcodes = self._get_array_barcodes()

            assigned_samples = self._assign_samples_to_arrays(grouped_samples, array_barcodes)

            self._update_apply_samples_entry(assigned_samples, sample_id_to_values)

            self.rec_man.store_and_commit()
            return SapioWebhookResult(True, "Successfully sorted samples and generated array IDs.")
        except Exception as e:
            self.log_to_system(f"Unexpected error: {str(e)}")
            CallbackUtil(context).ok_dialog("Error" ,str(e))
            return SapioWebhookResult(False)

    def _get_plated_samples(self) -> List[SampleModel]:
        step = self.exp_handler.get_step(self.SAMPLE_PLATING_STEP)
        plated_samples = PlateDesignerEntry(step,self.exp_handler).get_sources(SampleModel)
        if plated_samples:
            plated_samples = [s for s in plated_samples if s.get_ExemplarSampleType_field() != ACGHUtil.PROMEGA_SAMPLE_TYPE]
        if not plated_samples:
            raise Exception("No samples found in Sample Plating step.")
        return plated_samples

    def _sort_samples_by_plate_position(self, samples: List[SampleModel]) -> List[SampleModel]:
        def sort_key(sample: SampleModel) -> tuple[int | str, str]:
            row = sample.get_RowPosition_field() if sample.get_RowPosition_field() else "Z"
            col = int(sample.get_ColPosition_field()) if sample.get_ColPosition_field() else "99"
            return col, row
        return sorted(samples,key=sort_key)

    def _get_array_barcodes(self) -> Dict[str, List[str]]:
        scan_microarrays_rows = self.exp_handler.get_step_models(self.SCAN_MICROARRAYS_STEP, ELNExperimentDetailModel)
        array_barcodes = defaultdict(list)
        for row in scan_microarrays_rows:
            array_size = row.get_field_value("ArraySize")
            array_barcode = row.get_field_value("ArrayBarcode")
            if array_size and array_barcode:
                array_barcodes[array_size].append(array_barcode)
        return dict(array_barcodes)

    def _assign_samples_to_arrays(self, grouped_samples: Dict[str, List[SampleModel]],
                                  array_barcodes: Dict[str, List[str]]) -> List[Tuple[SampleModel, str]]:
        assigned_samples = []

        for array_type in [ACGHUtil.ARRAY_180K, ACGHUtil.ARRAY_1M]:
            samples = grouped_samples.get(array_type, [])
            barcodes = array_barcodes.get(array_type, [])
            capacity = ACGHUtil.CAPACITY_180K if array_type == ACGHUtil.ARRAY_180K else ACGHUtil.CAPACITY_1M
            for i, sample in enumerate(samples):
                array_index = i // capacity
                position = i % capacity + 1
                if array_index < len(barcodes):
                    array_id = f"{barcodes[array_index]}_1_{position}"
                    assigned_samples.append((sample, array_id))
                else:
                    raise Exception(
                        f"Not enough array barcodes for {array_type} samples.")

        return assigned_samples

    def _update_apply_samples_entry(self, assigned_samples: List[Tuple[SampleModel, str]], sample_id_to_values: dict[str, list[str]]) -> None:
        apply_samples_rows = self.exp_handler.get_step_models(self.SAMPLE_MICROARRAYS_STEP, ELNSampleDetailModel)
        sample_id_to_row = {row.get_field_value("SampleId"): row for row in apply_samples_rows}

        for sample, array_id in assigned_samples:
            sample_id = sample.get_SampleId_field()
            if sample_id in sample_id_to_row:
                array_size, priority = sample_id_to_values[sample_id]
                row = sample_id_to_row[sample_id]
                row.set_field_value("ArrayID", array_id)
                row.set_field_value("Priority", priority)
                row.set_field_value("ArraySize", array_size)

    def check_required_entries(self):
        required_steps = [self.SCAN_MICROARRAYS_STEP, self.SAMPLE_MICROARRAYS_STEP, self.SAMPLE_PLATING_STEP]
        for step in required_steps:
            if not self.exp_handler.get_step(step):
                raise Exception(f"Required step '{step}' not found.")
