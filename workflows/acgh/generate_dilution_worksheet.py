import io
import json
from typing import Any

import openpyxl
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import FileAttachmentParams
from utilities.attach_files_util import attach_file_to_entry
from utilities.data_models import ELNSampleDetailModel
from utilities.data_models import PrenatalSampleModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_DOWNLOAD: str = "fileDownload"
FILE_NAME: str = "aCGH_Dilution_Worksheet_[EXP ID]_[DATE].xlsx"


def write_cell(worksheet: Worksheet, index: int, col: int, data: Any) -> None:
    cell = worksheet.cell(index, col)

    # Check if the cell is part of a merged cell
    for merged_cell_range in worksheet.merged_cells.ranges:
        if cell.coordinate in merged_cell_range:
            # Find the top-left cell of the merged range
            cell = worksheet.cell(merged_cell_range.min_row, merged_cell_range.min_col)
            break

    try:
        if isinstance(data, float) and data:
            cell.value = round(data, 2)
        elif data:
            cell.value = str(data)
        else:
            cell.value = ""
    except AttributeError:
        # If cell is read-only, try to unmerge or find an editable cell
        try:
            # Attempt to unmerge if it's a merged cell
            worksheet.unmerge_cells(f"{cell.coordinate}")

            if isinstance(data, float) and data:
                cell.value = round(data, 2)
            elif data:
                cell.value = str(data)
            else:
                cell.value = ""
        except Exception:
            # If all else fails, skip writing to this cell
            pass


class GenerateDilutionWorksheetForaCGH(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]
    context: SapioWebhookContext

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}
        self.context = context

        required_step = self.exp_handler.get_step("Pipetting amounts for reference mastermix")

        if required_step.is_available():
            self.callback.ok_dialog("Error",
                                    "Please complete 'Pipetting amounts for reference mastermix' before generating dilution worksheet")
            return SapioWebhookResult(False)

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_FILE_DOWNLOAD:
                return SapioWebhookResult(True)

        # Read data from the template file so that we can add to it.
        workbook: Workbook = openpyxl.load_workbook("./template_files/aCGH_dilution_template_file.xlsx")
        worksheet: Worksheet = workbook.get_sheet_by_name("Current worksheet")

        # Write data from each of the entries to the file.
        self.write_data(worksheet)

        # Download the file to the user.
        self.download_file(workbook)

        return SapioWebhookResult(True, display_text="Successfully generated dilution worksheet")

    def write_data(self, worksheet: Worksheet) -> None:
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        # Ignore promegas
        samples = [sample for sample in samples if sample.get_ExemplarSampleType_field() != "Promega"]

        table2_records: list[DataRecord] = self.exp_handler.get_step_records(
            "Pipetting amounts for reference mastermix")
        table3_records: list[DataRecord] = self.exp_handler.get_step_records(
            "Pipetting amounts for labelling master mix")
        table4_records: list[DataRecord] = self.exp_handler.get_step_records(
            "Pipetting amounts for hybridization master mix")
        reagent_details: list[DataRecord] = self.exp_handler.get_step_records("Reagent Tracking")

        write_cell(worksheet, 2, 2, TimeUtil.now_in_format("%Y-%m-%d"))
        write_cell(worksheet, 3, 2, self.context.user.username)
        write_cell(worksheet, 4, 2, str(self.context.eln_experiment.notebook_experiment_id))

        self.rel_man.load_children_of_type(samples, PrenatalSampleModel)
        self.an_man.load_ancestors_of_type(self.inst_man.unwrap_list(samples), RequestModel.DATA_TYPE_NAME.__str__())

        curr_row_index = 7
        table1_records: list[ELNSampleDetailModel] = self.exp_handler.get_step_models(
            "Pipetting amount for the patient samples", ELNSampleDetailModel)

        sample_no = 1
        for x, sample_detail in enumerate(table1_records, start=curr_row_index):
            write_cell(worksheet, x, 1, sample_detail.get_field_value("SampleWellPosition"))
            sample_no += 1
            write_cell(worksheet, x, 2, sample_detail.get_field_value("OrderName"))
            write_cell(worksheet, x, 3, sample_detail.get_field_value("OtherSampleId"))
            write_cell(worksheet, x, 4, sample_detail.get_field_value("Nationalnumber"))
            write_cell(worksheet, x, 5, sample_detail.get_field_value("SampleConcentration"))
            write_cell(worksheet, x, 6, sample_detail.get_field_value("QCMethod"))
            write_cell(worksheet, x, 7, sample_detail.get_field_value("SampleVolume2"))
            write_cell(worksheet, x, 8, sample_detail.get_field_value("SampleWaterVolume"))
            write_cell(worksheet, x, 9, sample_detail.get_field_value("Sex"))
            write_cell(worksheet, x, 10, sample_detail.get_field_value("SampleMass"))
            write_cell(worksheet, x, 11, sample_detail.get_field_value("RefDNAVolume"))
            write_cell(worksheet, x, 12, sample_detail.get_field_value("RefWaterVolume"))
            write_cell(worksheet, x, 13, sample_detail.get_field_value("AnalysisCode"))
            write_cell(worksheet, x, 14, sample_detail.get_field_value("ArrayID"))
            write_cell(worksheet, x, 15, sample_detail.get_field_value("SampleType"))
            worksheet.insert_rows(curr_row_index + 1)
            curr_row_index += 1

        # update based on table 1
        curr_row_index += 3

        # Write data from Table 2
        for x, record in enumerate(table2_records, start=curr_row_index):
            write_cell(worksheet, x, 2, record.get_field_value("Typeofreference"))
            write_cell(worksheet, x, 3, record.get_field_value("Ref180k"))
            write_cell(worksheet, x, 4, record.get_field_value("Extrareferences"))
            write_cell(worksheet, x, 5, record.get_field_value("Qubit"))
            write_cell(worksheet, x, 6, record.get_field_value("DNAVolumeperRef"))
            write_cell(worksheet, x, 7, record.get_field_value("DNAVolumeforRefMasterMix"))
            write_cell(worksheet, x, 8, record.get_field_value("WaterVolumeperRef"))
            write_cell(worksheet, x, 9, record.get_field_value("WaterVolumeforRefMasterMix"))
            write_cell(worksheet, x, 10, record.get_field_value("PrimerMixVolumeforRefMasterMix"))
            curr_row_index += 1

        curr_row_index += 3

        write_cell(worksheet, curr_row_index - 2, 3, len(table1_records))

        # Write data from the Table 3
        table3_total_perSample_volume = 0
        table3_total_mastermix_volume = 0
        for x, record in enumerate(table3_records, start=curr_row_index):
            write_cell(worksheet, x, 1, record.get_field_value("Table3"))
            perSampleVol = record.get_field_value("PerSample2")
            write_cell(worksheet, x, 2, perSampleVol)
            masterMix = record.get_field_value("MasterMix3")
            write_cell(worksheet, x, 3, masterMix)
            curr_row_index += 1
            table3_total_perSample_volume += float(perSampleVol) if perSampleVol else 0
            table3_total_mastermix_volume += float(masterMix) if masterMix else 0


        write_cell(worksheet, curr_row_index, 1, "Total Volume")
        write_cell(worksheet, curr_row_index, 2, table3_total_perSample_volume)
        write_cell(worksheet, curr_row_index, 3, table3_total_mastermix_volume)
        curr_row_index -= 1

        # Write data from the Table 4
        table4_total_perSampleVol_a180k = 0
        table4_total_perSampleVol_volume_a1m = 0
        table4_total_mastermix_a180k = 0
        table4_total_mastermix_volume_a1m = 0

        no_of_180k_samples = table4_records[0].get_field_value("NumberOfA180kSamples")
        no_of_1m_samples = table4_records[0].get_field_value("NumberOfA1MSamples")

        write_cell(worksheet, curr_row_index-1, 7, float(no_of_180k_samples))
        write_cell(worksheet, curr_row_index-1, 11, float(no_of_1m_samples))

        for x, record in enumerate(table4_records, start=curr_row_index):
            write_cell(worksheet, x, 4, record.get_field_value("Table4"))
            perSampleVol_a180k = record.get_field_value("PerSampleA180k")
            write_cell(worksheet, x, 6, perSampleVol_a180k)
            mastermixA180k = record.get_field_value("MasterMixTable4A180k")
            write_cell(worksheet, x, 7, mastermixA180k)

            write_cell(worksheet, x, 8, record.get_field_value("Table4"))
            perSampleVol_a1m = record.get_field_value("PerSampleA1M")
            write_cell(worksheet, x, 10, perSampleVol_a1m)
            mastermixA1M = record.get_field_value("MasterMixTable4A1M")
            write_cell(worksheet, x, 11, mastermixA1M)
            curr_row_index += 1

            table4_total_perSampleVol_a180k += float(perSampleVol_a180k) if perSampleVol_a180k else 0
            table4_total_perSampleVol_volume_a1m += float(perSampleVol_a1m) if perSampleVol_a1m else 0
            table4_total_mastermix_a180k += float(mastermixA180k) if mastermixA180k else 0
            table4_total_mastermix_volume_a1m += float(mastermixA1M) if mastermixA1M else 0


        write_cell(worksheet, curr_row_index, 4, "Total Volume")
        write_cell(worksheet, curr_row_index, 6, table4_total_perSampleVol_a180k)
        write_cell(worksheet, curr_row_index, 7, table4_total_mastermix_a180k)
        write_cell(worksheet, curr_row_index, 8, "Total Volume")
        write_cell(worksheet, curr_row_index, 10, table4_total_perSampleVol_volume_a1m)
        write_cell(worksheet, curr_row_index, 11, table4_total_mastermix_volume_a1m)

        curr_row_index -= 4
        for reagent in reagent_details:
            write_cell(worksheet, curr_row_index, 13, reagent.get_field_value("ConsumableType"))
            write_cell(worksheet, curr_row_index, 14, reagent.get_field_value("ConsumableLot"))
            write_cell(worksheet, curr_row_index, 15, reagent.get_field_value("ExpirationDate"))
            curr_row_index += 1

    def download_file(self, workbook: Workbook):
        # Generate the file name.
        file_name: str = FILE_NAME

        # Add the date to the file name.
        file_name = file_name.replace("[EXP ID]", str(self.context.eln_experiment.notebook_experiment_id))
        file_name = file_name.replace("[DATE]", TimeUtil.now_in_format("%Y%m%d"))

        # Save the file data to the in-memory buffer and download the file to the user.
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)
        self.response_map[STATUS] = STATUS_FILE_DOWNLOAD

        # Add the generated file to the workflow
        params = FileAttachmentParams(
            self.context,
            file_name,
            buffer.getvalue(),
            self.exp_handler,
            self.inst_man,
            self.rec_man,
            "Generated Dilution Worksheet",
        )
        attach_file_to_entry(params)
        self.callback.write_file(
            file_name,
            buffer.getvalue())
