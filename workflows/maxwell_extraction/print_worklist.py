from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


HEADERS: list[str] = ["Sample ID", "Tube Barcode", "National ID", "Sample Type", "Concentration"]


class PrintWorklist(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        # Get all the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models(context.active_step, SampleModel)

        # Write the file data
        file_data: str = ",".join(HEADERS) + "\n"

        for s in samples:
            file_data += (f"{s.get_SampleId_field()},{s.get_TubeBarcode_field()},{s.get_PatientID_field()},"
                          f"{s.get_ExemplarSampleType_field()},,\n")

        # Download the file to the user
        experiment_name: str = (DataMgmtServer.get_eln_manager(context.user)
                                .get_eln_experiment_by_record_id(self.exp_handler.get_experiment_record().record_id)
                                .notebook_experiment_name)

        file_name: str = f"WORKLIST - {experiment_name}.csv"

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(file_data, "utf-8"),
                None,
                self.exp_handler,
            )
        )
        return FileUtil.write_file(file_name, bytes(file_data, "utf-8"))
