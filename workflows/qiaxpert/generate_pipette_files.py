from collections import OrderedDict

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ELNExperimentModel
from utilities.data_models import ProjectModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class GeneratePipetteFiles(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        # Get all the samples on the plate
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Generate file data for each sample
        file_data: OrderedDict[str, bytes] = self.generate_file_data(samples)

        # Add the first generated file
        first_file = file_data.popitem(False)
        file_data[first_file[0]] = first_file[1]

        params = FileAttachmentParams(
            context,
            first_file[0],
            first_file[1],
            "Generated Pipetting File",
            self.exp_handler,
            self.inst_man
        )
        attach_file_to_entry(params)

        # Write the files to the user
        return FileUtil.write_files(file_data)

    # Get batch id and slide no, then get file form that

    # Iterate through each sample in the workflow
    # Generate file name for sample with batch_id_slide_no
    # Based on well pos to slide no
    # dict[str (project_id_slide_no), list[SampleModel]] = {}
    # For each thing in dict, write the file data with the coordinates
        # and then match with the samples under it
    # dict[slide_pos, sample_id]

    def generate_file_data(self, samples: list[SampleModel]) -> OrderedDict[str, bytes]:
        file_name_to_samples_dict: dict[str, list[SampleModel]] = {}
        file_data_dict: OrderedDict[str, bytes] = OrderedDict()

        exp: ELNExperimentModel = self.exp_handler.get_experiment_model(ELNExperimentModel)
        self.rel_man.load_parents_of_type([exp], ProjectModel)
        project_id: str = exp.get_parent_of_type(ProjectModel).get_ProjectId_field()
        for s in samples:
            slide_no: int = self.get_slide_no_from_sample(s)

            file_name: str = f"{project_id}_{slide_no}.csv"

            if file_name not in file_name_to_samples_dict:
                file_name_to_samples_dict[file_name] = []

            file_name_to_samples_dict[file_name].append(s)

        for file_name in file_name_to_samples_dict.keys():
            file_data: str = ""
            slide_data_dict: dict[str, str] = self.create_slide_dict()

            for s in file_name_to_samples_dict[file_name]:
                slide_data_dict[self.calc_slide_well_pos(s)] = s.get_TubeBarcode_field()

            for well_location in slide_data_dict:
                file_data += f"{well_location},{slide_data_dict[well_location]}\n"

            file_data_dict[file_name] = bytes(file_data, "utf-8")

        return file_data_dict

    def get_slide_no_from_sample(self, sample: SampleModel) -> int:
        slide_no: int = 0

        try:
            col: int = int(sample.get_ColPosition_field())
        except ValueError:
            # Empty value
            return -1

        if col == 1 or col == 2:
            slide_no = 1

        elif col == 3 or col == 4:
            slide_no = 2

        elif col == 5 or col == 6:
            slide_no = 3

        elif col == 7 or col == 8:
            slide_no = 4

        elif col == 9 or col == 10:
            slide_no = 5

        elif col == 11 or col == 12:
            slide_no = 6

        return slide_no

    def create_slide_dict(self) -> dict[str, str]:
        return {
            "A1": "",
            "B1": "",
            "C1": "",
            "D1": "",
            "E1": "",
            "F1": "",
            "G1": "",
            "H1": "",
            "A2": "",
            "B2": "",
            "C2": "",
            "D2": "",
            "E2": "",
            "F2": "",
            "G2": "",
            "H2": "",
        }

    def calc_slide_well_pos(self, sample: SampleModel) -> str:
        col: str = ""

        try:
            if int(sample.get_ColPosition_field()) % 2 == 0:
                col = "2"
            else:
                col = "1"
        except ValueError:
            # empty column, throws on int() call
            col = "1"

        return f"{sample.get_RowPosition_field()}{col}"
