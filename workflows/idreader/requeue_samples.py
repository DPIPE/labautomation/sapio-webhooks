from typing import cast, Any

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel, AssignedProcessModel, ReturnPointModel, RequestModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class RequeueSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.callback_util = CallbackUtil(context)

        # Get all the samples from the samples entry. If there aren't any, just return.
        try:
            samples_entry: ElnEntryStep = self.exp_handler.get_step("Samples")
        except Exception:
            self.callback_util.ok_dialog("Error", "Cannot find required Entries")
            return SapioWebhookResult(True)
        submitted_samples: list[SampleModel] = self.exp_handler.get_step_models(samples_entry, SampleModel)
        if not submitted_samples:
            return SapioWebhookResult(True)

        # Get all the samples selected for requeue. If there aren't any, just return.
        records: list[DataRecord] = self.exp_handler.get_step_records(cast(ElnEntryStep, context.active_step))
        if not records:
            return SapioWebhookResult(True)
        requeue_barcodes: list[str] = \
            [x.get_field_value("IDReaderTubeBarcode") for x in records if x.get_field_value("Requeue")]
        if not requeue_barcodes:
            return SapioWebhookResult(True)
        requeue_samples: list[SampleModel] = \
            [sample for sample in submitted_samples if sample.get_TubeBarcode_field() in requeue_barcodes]

        # Remove the requeue samples from the samples entry and requeue them for the process. Show a table dialog to the
        # user listing all the samples that were requeued.
        if requeue_samples:
            requeued_samples: list[dict[str, str]] = (
                self.handle_requeue_samples(context, requeue_samples, samples_entry))
            if requeued_samples:
                self.callback_util.table_dialog("Notice", "The following Verso samples have been requeued for the"
                                                          " process.",
                                                [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID"),
                                                 VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode",
                                                                            "Tube Barcode")],
                                                requeued_samples)

        return SapioWebhookResult(True)

    def handle_requeue_samples(self, context: SapioWebhookContext, requeue_samples: list[SampleModel],
                               samples_entry: ElnEntryStep) -> list[dict[str, str]]:
        # Remove the requeue samples from the samples entry.
        self.exp_handler.remove_step_records(samples_entry, requeue_samples)

        # Remove samples from "Generate Pick Files for Samples" as well
        try:
            self.exp_handler.remove_step_records("Generate Pick Files for Samples", requeue_samples)
        except Exception:
            pass

        # Get the step number to ensure we send samples back to the right step instead of the beginning of the process.
        step_number: int = (
            SampleUtils.get_current_process(requeue_samples[0], self.rel_man).get_ProcessStepNumber_field())

        self.rel_man.load_parents_of_type(requeue_samples, AssignedProcessModel)
        process_name: str = ""
        for sample in requeue_samples:
            current_process: AssignedProcessModel | None = self.get_current_process(sample)
            if not current_process:
                continue
            process_name = current_process.get_ProcessName_field()
            current_process.delete()
            statuses: list[str] = sample.get_ExemplarSampleStatus_field().split(",")
            sample.set_ExemplarSampleStatus_field(",".join([x for x in statuses if "In Process" not in x]))

        self.rec_man.store_and_commit()

        # Requeue the samples for the process.
        new_orders: bool = False
        orders_to_samples: dict[RequestModel, list[SampleModel]] = {}
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, requeue_samples)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        for sample in top_level_samples:
            requeue_sample: SampleModel = \
                [x for x in requeue_samples if x.get_TopLevelSampleId_field() == sample.get_SampleId_field()][0]
            order: RequestModel | None = sample.get_parent_of_type(RequestModel)
            if not order:
                order = RequestUtils.add_request(self.rec_handler, DataMgmtServer.get_accession_manager(context.user))
                order.add_child(sample)
                new_orders = True
            if order not in orders_to_samples.keys():
                orders_to_samples[sample.get_parent_of_type(RequestModel)] = []
            orders_to_samples[order].append(requeue_sample)
        requeued_samples: list[dict[str, str]] = []

        if new_orders:
            self.rec_man.store_and_commit()

        # TODO: Figure out why ProcessTracking.requeue() doesn't seem to be working. There's a lack of documentation.
        for order in orders_to_samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, requeue_samples, process_name,
                                              step_number, request=order)
            requeued_samples.extend([{"SampleId": x.get_SampleId_field(),
                                      "TubeBarcode": x.get_TubeBarcode_field()} for x in orders_to_samples[order]])

        requeue_table: list[dict[str, Any]] = \
            [{"SampleId": x.get_SampleId_field(), "TubeBarcode": x.get_TubeBarcode_field()} for x in requeue_samples]
        return requeue_table

    def get_current_process(self, sample: SampleModel) -> AssignedProcessModel | None:
        assigned_processes: list[AssignedProcessModel] | None = sample.get_parents_of_type(AssignedProcessModel)
        if not assigned_processes:
            return None
        for process in assigned_processes:
            if "In Process" in process.get_Status_field():
                return process
        return None

    def get_return_points(self, samples: list[SampleModel]) -> list[ReturnPointModel] | None:
        return_points: list[ReturnPointModel] = []
        assigned_processes: list[AssignedProcessModel] = []
        self.rel_man.load_parents_of_type(samples, AssignedProcessModel)
        for sample in samples:
            assigned_processes.append(self.get_current_process(sample))
        self.rel_man.load_children_of_type(assigned_processes, ReturnPointModel)
        for assigned_process in assigned_processes:
            return_points.append(assigned_process.get_child_of_type(ReturnPointModel))
        return return_points
