import re
from functools import reduce
from operator import add
from typing import cast

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.data_models import IDReaderScanModel
from utilities.utils import collapse_entry
from utilities.webhook_handler import OsloWebhookHandler

MANUAL_VALIDATION = "REQUIRES MANUAL VALIDATION"


class IDReaderCheck(OsloWebhookHandler):
    exp_handler: ExperimentHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)
        self.callback_util = CallbackUtil(context)

        try:
            submitted_samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        except Exception:
            self.callback_util.ok_dialog("Error", "Cannot find 'Samples' entry")
            return SapioWebhookResult(True)

        # [OSLO-910]: Get all the Verso samples from the samples entry. If there are no Verso samples in the experiment,
        # then just auto-submit the entry and return.
        verso_pattern = re.compile(r"^H[A-Z]\w+")
        verso_samples: list[SampleModel] | None = \
            [sample for sample in submitted_samples if verso_pattern.match(sample.get_TubeBarcode_field())]
        if not verso_samples:
            self.exp_handler.update_step(cast(ElnEntryStep, context.active_step),
                                         entry_status=ExperimentEntryStatus.Completed)
            self.update_fridge_samples_check_step(context, submitted_samples, verso_samples)
            return SapioWebhookResult(True)

        self.update_fridge_samples_check_step(context, submitted_samples, verso_samples)

        # [OSLO-910]: Get all the IDReader scans from the parent plates of the samples.
        self.rel_man.load_parents_of_type(verso_samples, PlateModel)
        parent_plates: list[PlateModel] = reduce(add, [s.get_parents_of_type(PlateModel) for s in verso_samples])
        id_reader_scans: list[IDReaderScanModel] = []
        if parent_plates:
            id_reader_scans.extend(self.rec_handler.query_models(IDReaderScanModel,
                                                                 IDReaderScanModel.PLATE__FIELD_NAME.field_name,
                                                                 [x.record_id for x in parent_plates]))

        # [OSLO-910]: Add all IDReader scans to the table or update existing ones. If there is no IDReader scan
        # corresponding to a sample, then create a row record and set its status to "Not Found In Plate" and set it to
        # be requeued.
        row_records: list[DataRecord] = self.exp_handler.get_step_records(cast(ElnEntryStep, context.active_step))
        row_models: list[PyRecordModel] = []
        added_barcodes: list[str] = []
        if row_records:
            row_models.extend(self.inst_man.add_existing_records(row_records))
        for verso_sample in verso_samples:
            try:
                id_reader_scan: IDReaderScanModel = \
                    [x for x in id_reader_scans if x.get_TubeBarcode_field() == verso_sample.get_TubeBarcode_field()][0]
                row_model: PyRecordModel = (
                    self.get_or_generate_row_record(context.experiment_entry.data_type_name, row_models,
                                                    id_reader_scan.get_TubeBarcode_field()))
                row_model.set_field_value("IDReaderTubeBarcode", id_reader_scan.get_TubeBarcode_field())
                row_model.set_field_value("RackID", [x.get_PlateId_field() for x in parent_plates if
                                                     x.record_id == id_reader_scan.get_Plate_field()][0])
                row_model.set_field_value("WellPosition", f"{id_reader_scan.get_RowPosition_field()}"
                                                          f"{id_reader_scan.get_ColPosition_field()}")
                row_model.set_field_value("IdReaderStatus", "Found")
                row_model.set_field_value("Requeue", False)
                added_barcodes.append(id_reader_scan.get_TubeBarcode_field())
            except Exception:
                row_model: PyRecordModel = (
                    self.get_or_generate_row_record(context.experiment_entry.data_type_name, row_models,
                                                    verso_sample.get_TubeBarcode_field()))
                row_model.set_field_value("IDReaderTubeBarcode", verso_sample.get_TubeBarcode_field())
                row_model.set_field_value("RackID", "")
                row_model.set_field_value("WellPosition", "")
                row_model.set_field_value("IdReaderStatus", "Not Found In Plate")
                row_model.set_field_value("Requeue", True)
                added_barcodes.append(verso_sample.get_TubeBarcode_field())

        # [OSLO-910]: There maybe IDReader scans for samples that aren't in the experiment, which the user needs to be
        # warned about, so add them to the table as well.
        for id_reader_scan in id_reader_scans:
            if id_reader_scan.get_TubeBarcode_field() not in added_barcodes:
                row_model: PyRecordModel = (
                    self.get_or_generate_row_record(context.experiment_entry.data_type_name, row_models,
                                                    id_reader_scan.get_TubeBarcode_field()))
                row_model.set_field_value("IDReaderTubeBarcode", id_reader_scan.get_TubeBarcode_field())
                row_model.set_field_value("RackID", [x.get_PlateId_field() for x in parent_plates if
                                                     x.record_id == id_reader_scan.get_Plate_field()][0])
                row_model.set_field_value("WellPosition", f"{id_reader_scan.get_RowPosition_field()}"
                                                          f"{id_reader_scan.get_ColPosition_field()}")
                row_model.set_field_value("IdReaderStatus", "Not Found In Experiment")

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def update_fridge_samples_check_step(self, context, submitted_samples, verso_samples):
        # If all samples are verso samples, then collapse the fridge samples manual check entry
        try:
            step = ExperimentHandler(context).get_step("Manual Check For Fridge Samples")
            if verso_samples and len(verso_samples) == len(submitted_samples):
                collapse_entry(context, step.eln_entry)
                options = step.get_options()
                if MANUAL_VALIDATION in options:
                    options.pop(MANUAL_VALIDATION)
                    step.set_options(options)
            else:
                # at least one fridge sample is present. so user need to be forced to do manual check
                options = step.get_options()
                options[MANUAL_VALIDATION] = ""
                step.set_options(options)
        except Exception as e:
            pass

    def get_or_generate_row_record(self, data_type_name: str, row_models: list[PyRecordModel],
                                   tube_barcode: str) -> PyRecordModel:
        try:
            row_model: PyRecordModel = \
                [x for x in row_models if x.get_field_value("IDReaderTubeBarcode") == tube_barcode][0]
            return row_model
        except Exception:
            return self.inst_man.add_new_record(data_type_name)
