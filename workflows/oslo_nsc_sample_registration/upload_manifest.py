import re
from typing import cast

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.webhook_handler import OsloWebhookHandler


SHORT_HEADERS: list[str] = []
LONG_HEADERS: list[str] = []


class UploadManifest(OsloWebhookHandler):
    step: ElnEntryStep

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Prompt the user to upload a manifest file.
        self.step = cast(ElnEntryStep, context.active_step)
        file_data, file_bytes = self.callback.request_file("Upload Manifest File", [".xlsx"])
        if not file_data and not file_bytes:
            return self._error_and_uninit_entry()

        # Depending on the file uploaded, read the data from it. The only difference between the two file types is that
        # ONT files contain index assignment data.
        ...

        return SapioWebhookResult(True)

    def _error_and_uninit_entry(self, msg: str | None = None) -> SapioWebhookResult:
        if msg:
            self.callback.ok_dialog("Error", msg)
        self.exp_handler.uninitialize_step(self.step)
        return SapioWebhookResult(True)
