from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnExperimentStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import HomePageDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.webhook_handler import OsloWebhookHandler


class CompleteNSCSampleRegistration(OsloWebhookHandler):
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)

        self.exp_handler.update_experiment(context.eln_experiment.notebook_experiment_name,
                                           ElnExperimentStatus.Completed)
        return SapioWebhookResult(True,  directive=HomePageDirective())
