from datetime import datetime
from datetime import timedelta
from typing import Any

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.DataTypeLayout import DataTypeLayout
from sapiopylib.rest.pojo.datatype.FieldDefinition import ListMode
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxDateFieldDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxSelectionFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.webhook_handler import OsloWebhookHandler

class CreateBooking(OsloWebhookHandler):
    rec_man: DataRecordManager

    def check_overlap(self, this: tuple[int, int], other: tuple[int, int]) -> bool:
        # The +- 1 is to ensure that two concecutivee bookings don't cancel
        # eg 1-2 doesn't overlap with 2-3
        if this[0] >= other[0] + 1 and this[0] <= other[1] - 1:
            return True
        if this[1] >= other[0] + 1 and this[1] <= other[1] - 1:
            return True

        return False

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_man = context.data_record_manager

        if context.client_callback_result is None:
            # Leaving this line commented here just in case - way too much time was
            # wasted trying to get this to work with DateField formatting
            # default_end = int((datetime.now() + timedelta(hours=1)).timestamp()) # unix timestamp

            form_builder: FormBuilder = FormBuilder()
            form_builder.add_field(VeloxDateFieldDefinition("StartDate", "start_date", "Start date", "yyyy/MM/dd HH:mm", editable=True, required=True))
            form_builder.add_field(VeloxDateFieldDefinition("EndDate", "end_date", "End date", "yyyy/MM/dd HH:mm", editable=True, required=True))
            form_builder.add_field(
                    VeloxSelectionFieldDefinition("Username", "given_username", "Username: ", ListMode.USER, default_value=context.user.username, editable=True, required=True)
                    )
            form_builder.add_field(
                    VeloxSelectionFieldDefinition("Status", "status", "Status: ", ListMode.LIST, pick_list_name="Event Status", default_value="Active", editable=True, required=True)
                    )

            temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()

            request: FormEntryDialogRequest = FormEntryDialogRequest(
                    "ADD RESERVATION EVENT",
                    "Book a timeslot for this instrument",
                    temp_dt,
                    )

            return SapioWebhookResult(True, client_callback_request=request)

        elif isinstance(context.client_callback_result, FormEntryDialogResult):
            #{'end_date': None, 'given_username': 'ash.isbitt', 'start_date': 1714490880000, 'status': 'Active'}
            result: dict[str, Any] = context.client_callback_result.user_response_map

            if result is None:
                return SapioWebhookResult(True)

            other_reservations: list[DataRecord] = self.rec_man.get_children(
                    context.data_record.get_record_id(),
                    "ReservationEvent",
                    ).result_list

            for reserve in other_reservations:
                other = reserve.get_field_value("ReservedTime").split("/")
                other[0] = int(other[0])
                other[1] = int(other[1])

                if self.check_overlap((result["start_date"], result["end_date"]), other):
                    return SapioWebhookResult(True, display_text="This timeslot is already reserved. Please book another slot")

            new_event = self.rec_man.create_children_fields_for_record(
                    context.data_record,
                    "ReservationEvent",
                    [
                        {
                            "Description": "",
                            "InstrumentName": context.data_record.get_field_value("InstrumentName"),
                            "InstrumentType": context.data_record.get_field_value("InstrumentType"),
                            "ReservedTime": f"{result['start_date']}/{result['end_date']}",
                            "Status": result["status"],
                            "Username": result["given_username"]
                            }
                        ]
                    )

            self.rec_man.commit_data_records(new_event)
            return SapioWebhookResult(True)
