from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.DateRange import DateRange
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities import utils
from utilities.constants import Constants
from utilities.data_models import InstrumentModel, ReservationEventModel
from utilities.webhook_handler import OsloWebhookHandler


class AutoFillReservedInstruments(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get the instrument tracking entry by tag
        entries: list[ExperimentEntry] = self.eln_man.get_experiment_entry_list(context.eln_experiment.notebook_experiment_id)
        try:
            instrument_tracking: ExperimentEntry = [e for e in entries if Constants.TAG_INSTRUMENT_TRACKING in
                                                    self.eln_man.get_experiment_entry_options(context.eln_experiment.notebook_experiment_id, e.entry_id).keys()][0]
        except Exception:
            return SapioWebhookResult(False, "No instrument tracking entry found.")

        # Get the instruments in each record of the entry
        records: list[DataRecord] = utils.get_records_from_entry_with_option(context, Constants.TAG_INSTRUMENT_TRACKING,
                                                                             [instrument_tracking])
        if not records:
            return SapioWebhookResult(True)

        instrument_types: list[str] = [r.get_field_value("InstrumentType") for r in records]
        if instrument_types is None or len(instrument_types) == 0:
            return SapioWebhookResult(True)

        instruments: list[InstrumentModel] = self.rec_handler.query_models(InstrumentModel,
                                                                           InstrumentModel.INSTRUMENTTYPE__FIELD_NAME.field_name,
                                                                           instrument_types)
        if instruments is None or len(instruments) == 0:
            return SapioWebhookResult(True)

        # Get the reservation children of each instrument
        self.rel_man.load_children_of_type(instruments, ReservationEventModel)

        # Check if there is a reservation for the current user, matching the current date and time, and if so,
        # automatically select that instrument in the table
        reserved_instrument: InstrumentModel = None

        for i in instruments:
            reservations: list[ReservationEventModel] = i.get_children_of_type(ReservationEventModel)

            for r in reservations:
                if r.get_Username_field() == context.user.username:
                    date_range: DateRange = r.get_ReservedTime_field()
                    current_time: int = TimeUtil.now_in_millis()

                    if date_range.start_time < current_time < date_range.end_time:
                        reserved_instrument = i
                        break

            if reserved_instrument is not None:
                matching_records: list[DataRecord] = \
                    [r for r in records if r.get_field_value("InstrumentType") == i.get_InstrumentType_field()]

                for m in matching_records:
                    m.set_field_value("InstrumentUsed", i.get_InstrumentName_field())
                    m.set_field_value("Technician", context.user.username)
                    m.set_field_value("Date", TimeUtil.now_in_millis())

            reserved_instrument = None

        DataMgmtServer.get_data_record_manager(context.user).commit_data_records(records)

        return SapioWebhookResult(True)
