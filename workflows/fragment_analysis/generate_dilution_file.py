import csv
import io
from datetime import datetime

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-1038
Description : Generate a dilution file
"""


class GenerateDilutionFile(OsloWebhookHandler):
    # Entry Names
    RESULT_SAMPLES_ENTRY = "Result Samples"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples
        samples_step = exp_handler.get_step(self.RESULT_SAMPLES_ENTRY)
        sample_records = exp_handler.get_step_records(samples_step)
        if not sample_records:
            raise Exception(f"Sample records not found in the entry named {self.RESULT_SAMPLES_ENTRY}")
        samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get the file details
        headers = ["Plate Name", "Well", "Priority", "Sample Name",
                 "National ID", "Reaction Name", "Source Volume to Use", "Diluent Volume to Use",
                  "Target Concentration", "Target Volume"]
        lines = []
        for sample in samples:
            lines.append([sample.get_StorageLocationBarcode_field(), sample.get_RowPosition_field() +
                          sample.get_ColPosition_field(), sample.get_Priority_field(),
                          sample.get_OtherSampleId_field()])
            # TODO : upon a clarity of how we represent our link for sample to fragment analysis kit
            #  and how these dilutions will be performed need to add value for the other columns

        # sort date by plate id and well positions
        lines.sort(key=lambda row: (row[0], row[1], row[2]))
        final_lines = [headers]
        for line in lines:
            final_lines.append([line[0], line[1] + line[2]] + line[2:])
        lines = final_lines

        # generate the file
        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer)
            csv_writer.writerows(lines)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()

        formatted_datetime = datetime.now().strftime("%Y%m%d_%H%M%S")
        file_name = f"{context.eln_experiment.notebook_experiment_name} {formatted_datetime}.csv"

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                self.context,
                file_name,
                bytes(csv_bytes, "utf-8"),
                "Generated Results File",
                exp_handler,
                self.inst_man
            )
        )

        # exp_handler.complete_step("Result Samples")

        # provide file to users
        client_callback.write_file(file_name, csv_bytes)
        return SapioWebhookResult(True)
