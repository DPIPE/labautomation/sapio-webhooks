from typing import List, Dict

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, FragmentAnalysisKitModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.fragment_analysis.add_controls_pcr_preparation import map_sample_to_fragment_analysis_kit
from workflows.fragment_analysis.map_fragment_kits_to_reaction_mixes import MAP_FRAGMENT_KITS_TO_REACTION_MIXES

"""
Ticket : OSLO-1038
Description : Add reagents to reagent tracking entry based on the fragment analysis kits
"""


class AddReagentsAsPerFragmentAnalysisKit(OsloWebhookHandler):
    # entry names
    REAGENT_TRACKING_ENTRY = "Reagent Tracking"
    REACTION_ENTRY = "Reaction Mix Recipe"

    # Experiment details fields
    CONSUMABLE_TYPE_FIELD = "ConsumableType"
    CONSUMABLE_QTY_FIELD = "ConsumableQty"
    REACTION_FIELD = "ReactionMix"
    INGREDIENT_FIELD = 'Ingredient'
    NUMBER_OF_REACTIONS = 'Numberofreactions'
    VOLUME_FIELD = 'VolumeL1reaction'

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples entry
        samples_entry = context.active_step

        # get the samples
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get a map of samples to their fragment analysis kits
        sample_to_fragment_analysis_kits: Dict[SampleModel, List[FragmentAnalysisKitModel]] = \
            map_sample_to_fragment_analysis_kit(context, samples)

        # generate a map of reagent name to volume used
        reagent_to_reagent_used = dict()
        kit_name_to_reagent_to_reagent_used = dict()
        kits_not_found = set()
        for sample in sample_to_fragment_analysis_kits:
            for fragment_kit in sample_to_fragment_analysis_kits[sample]:
                kit_name = fragment_kit.get_KitName_field()
                if kit_name in MAP_FRAGMENT_KITS_TO_REACTION_MIXES:
                    reaction_mix_reagents_map = MAP_FRAGMENT_KITS_TO_REACTION_MIXES[kit_name]
                    for reaction_mix_reagent in reaction_mix_reagents_map:
                        if reaction_mix_reagent not in ["DNA", "MQ-H2O"]:
                            if reaction_mix_reagent not in reagent_to_reagent_used:
                                reagent_to_reagent_used[reaction_mix_reagent] = 0
                            reagent_to_reagent_used[reaction_mix_reagent] += \
                                reaction_mix_reagents_map[reaction_mix_reagent]
                            if kit_name not in kit_name_to_reagent_to_reagent_used:
                                kit_name_to_reagent_to_reagent_used[kit_name] = dict()
                            if reaction_mix_reagent not in kit_name_to_reagent_to_reagent_used[kit_name]:
                                kit_name_to_reagent_to_reagent_used[kit_name][reaction_mix_reagent] = 0
                            kit_name_to_reagent_to_reagent_used[kit_name][reaction_mix_reagent] += \
                                reaction_mix_reagents_map[reaction_mix_reagent]
                else:
                    kits_not_found.add(kit_name)

        # prompt user with kits not found
        if kits_not_found:
            client_callback.ok_dialog("Warning", "The following kits do not correspond to a known reaction mix: "
                                      + " ,".join(kits_not_found))

        # build a field map list
        field_map_list = []
        for reagent in reagent_to_reagent_used:
            fields = {self.CONSUMABLE_TYPE_FIELD: reagent, self.CONSUMABLE_QTY_FIELD: reagent_to_reagent_used[reagent]}
            field_map_list.append(fields)
        reaction_mix_field_map_list = []
        for kit_name in kit_name_to_reagent_to_reagent_used:
            for reagent in kit_name_to_reagent_to_reagent_used[kit_name]:
                fields = {self.REACTION_FIELD: kit_name,
                          self.INGREDIENT_FIELD: reagent,
                          self.NUMBER_OF_REACTIONS: len(samples)+1,
                          self.VOLUME_FIELD: kit_name_to_reagent_to_reagent_used[kit_name][reagent]}
                reaction_mix_field_map_list.append(fields)

        # add the reagents to reagent tracking entry
        reagent_tracking_step = exp_handler.get_step(self.REAGENT_TRACKING_ENTRY, True)
        reagent_records = self.dr_man.add_data_records_with_data(reagent_tracking_step.get_data_type_names().pop(),
                                                                 field_map_list)
        reagent_tracking_step.add_records(reagent_records)

        reaction_mix_step = exp_handler.get_step(self.REACTION_ENTRY, True)
        reaction_records = self.dr_man.add_data_records_with_data(reaction_mix_step.get_data_type_names().pop(),
                                                                  reaction_mix_field_map_list)
        reaction_mix_step.add_records(reaction_records)

        return SapioWebhookResult(True)
