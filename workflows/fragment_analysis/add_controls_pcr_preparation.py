from typing import List, Dict

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager
from sapiopylib.rest.utils.recordmodel.ancestry import RecordModelAncestorManager

from utilities.data_models import SampleModel, RequestModel, InvoiceModel, AssayDetailModel, FragmentAnalysisKitModel, \
    InHouseModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-1038
Description : Add controls as per the fragment analysis
"""

class AddControlsPcrPreparation(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)

        # get the samples entry
        samples_entry = context.active_step

        # get the samples
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get a map of samples to their fragment analysis kits
        sample_to_fragment_analysis_kits: Dict[SampleModel, List[FragmentAnalysisKitModel]] = \
            map_sample_to_fragment_analysis_kit(context, samples)

        # as per the fragment analysis kit add controls that are mentioned on the fragment analysis kit
        controls, missing_lot_numbers = self.get_controls(sample_to_fragment_analysis_kits)
        if missing_lot_numbers:
            client_callback.ok_dialog("Warning", "Unable to identify in house controls with following lot numbers : "
                                      + " ,".join(missing_lot_numbers))

        # add controls to the samples entry
        samples_entry.add_records([control.get_data_record() for control in controls])
        return SapioWebhookResult(True)

    def get_controls(self, sample_to_fragment_analysis_kits):
        controls = []
        in_house_models = []
        missing_lot_numbers = []
        control_lot_numbers_visited = set()
        for sample in sample_to_fragment_analysis_kits:
            fragment_analysis_kits = sample_to_fragment_analysis_kits[sample]
            for fragment_analysis_kit in fragment_analysis_kits:
                control_lot_number = fragment_analysis_kit.get_Control_field()
                if not control_lot_number:
                    missing_lot_numbers.append(control_lot_number)
                else:
                    if control_lot_number not in control_lot_numbers_visited:
                        result = self.dr_man.query_data_records(InHouseModel.DATA_TYPE_NAME,
                                                                InHouseModel.LOTNUMBER__FIELD_NAME.field_name,
                                                                [control_lot_number]).result_list
                        if result:
                            in_house = self.inst_man.add_existing_record_of_type(result.pop(), InHouseModel)
                            in_house_models.append(in_house)
                        else:
                            missing_lot_numbers.append(control_lot_number)
                        control_lot_numbers_visited.add(control_lot_number)
        self.rel_man.load_parents_of_type(in_house_models, SampleModel)
        for in_house in in_house_models:
            sample = in_house.get_parent_of_type(SampleModel)
            controls.append(sample)
        return controls, missing_lot_numbers


def map_sample_to_fragment_analysis_kit(context: SapioWebhookContext, samples):
    rec_man = RecordModelManager(context.user)
    dr_man = context.data_record_manager
    rel_man = rec_man.relationship_manager
    inst_man = rec_man.instance_manager
    an_man = RecordModelAncestorManager(rec_man)

    # map sample to orders
    sample_to_orders = dict()
    rel_man.load_parents_of_type(samples, RequestModel)
    an_man.load_ancestors_of_type([sample.backing_model for sample in samples], RequestModel.DATA_TYPE_NAME)
    all_orders = []
    for sample in samples:
        orders = an_man.get_ancestors_of_type(sample.backing_model, RequestModel.DATA_TYPE_NAME)
        orders = inst_man.wrap_list(list(orders), RequestModel)
        curr_orders = sample.get_parents_of_type(RequestModel)
        if curr_orders:
            orders.extend(curr_orders)
        if orders:
            sample_to_orders[sample] = orders
            all_orders.extend(orders)

    # map sample to invoices
    rel_man.load_children_of_type(all_orders, InvoiceModel)
    sample_to_invoices = dict()
    all_invoices = []
    for sample in sample_to_orders:
        orders = sample_to_orders[sample]
        for order in orders:
            invoices = order.get_children_of_type(InvoiceModel)
            if invoices:
                if sample not in sample_to_invoices:
                    sample_to_invoices[sample] = []
                sample_to_invoices[sample].extend(invoices)
                all_invoices.extend(invoices)

    # map sample detail to assay details
    rel_man.load_children_of_type(all_invoices, AssayDetailModel)
    samples_to_assay_details = dict()
    for sample in sample_to_invoices:
        invoices = sample_to_invoices[sample]
        for invoice in invoices:
            assay_details = invoice.get_children_of_type(AssayDetailModel)
            if assay_details:
                if sample not in samples_to_assay_details:
                    samples_to_assay_details[sample] = []
                samples_to_assay_details[sample].extend(assay_details)

    # map sample to fragment analysis
    sample_to_fragment_analysis_kit = dict()
    analysis_code_to_fragment_analysis = dict()
    for sample in samples_to_assay_details:
        assay_details: List[AssayDetailModel] = samples_to_assay_details[sample]
        analysis_codes = [assay_detail.get_TestCodeId_field() for assay_detail in assay_details]
        analysis_codes = list(set(analysis_codes))
        fragment_analysis = []
        for analysis_code in analysis_codes:
            if analysis_code in analysis_code_to_fragment_analysis:
                if analysis_code_to_fragment_analysis[analysis_code]:
                    fragment_analysis.extend(analysis_code_to_fragment_analysis[analysis_code])
            else:
                fragment_analysis_records = dr_man.query_data_records(FragmentAnalysisKitModel.DATA_TYPE_NAME,
                                                                      FragmentAnalysisKitModel
                                                                      .ANALYSISCODE__FIELD_NAME.field_name,
                                                                      [analysis_code]).result_list
                if fragment_analysis_records:
                    fragment_analysis_models = inst_man.add_existing_records_of_type(fragment_analysis_records,
                                                                                     FragmentAnalysisKitModel)
                    fragment_analysis.extend(fragment_analysis_models)
                    analysis_code_to_fragment_analysis[analysis_code] = fragment_analysis_models
                else:
                    analysis_code_to_fragment_analysis[analysis_code] = []
        sample_to_fragment_analysis_kit[sample] = fragment_analysis
    return sample_to_fragment_analysis_kit
