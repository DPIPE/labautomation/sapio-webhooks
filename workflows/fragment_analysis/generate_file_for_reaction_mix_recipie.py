import csv
import io
from datetime import datetime

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-1038
Description : Generate a file for reaction mix recipie
"""


class GenerateFileForReactionMixRecipie(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)

        # get the records
        entry = context.experiment_entry
        if not entry:
            records = context.active_step.get_records()
        else:
            records = context.eln_manager.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                                     entry.entry_id)
        # get the file details
        lines = [["Reaction Mix", "Ingredient", "Number of reactions", "Volume (uL) (1 reaction)",
                 "Volume (ul) (all reactions)"]]
        for record in records:
            lines.append([record.get_field_value('ReactionMix'), record.get_field_value('Ingredient'),
                          record.get_field_value('Numberofreactions'),
                          record.get_field_value('VolumeL1reaction'), record.get_field_value('Volumelallreactions')])

        # generate the file
        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer)
            csv_writer.writerows(lines)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y%m%d_%H%M%S")
        file_name = context.eln_experiment.notebook_experiment_name + " " + formatted_datetime + ".csv"

        # Attach to workflow
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(csv_bytes, "utf-8"),
                "Generated Recipe File",
                None,
                self.inst_man
            )
        )

        # provide file to users
        client_callback.write_file(file_name, csv_bytes)
        return SapioWebhookResult(True)
