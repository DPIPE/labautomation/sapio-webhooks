from typing import List

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, InHouseModel, ConsumableItemModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-1038
Description : Add reagents to reagent tracking entry based on the fragment analysis kits
"""


class AddReagentsAsPerFragmentAnalysisKit(OsloWebhookHandler):
    # entry names
    SAMPLES_ENTRY = "Samples"
    REAGENT_TRACKING_ENTRY = "Reagent Tracking"

    # Experiment details fields
    CONSUMABLE_TYPE_FIELD = "ConsumableType"
    CONSUMABLE_QTY_FIELD = "ConsumableQty"

    # handlers
    exp_handler: ExperimentHandler
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)
        self.exp_handler: ExperimentHandler = ExperimentHandler(context)
        self.rec_handler = RecordHandler(context)

        # get samples controls reagents
        samples: List[SampleModel]
        controls: List[SampleModel]
        reagents: List[SampleModel]
        samples, controls, reagents = self.get_samples_controls_and_reagents()

        # TODO : upon a clarity of how we represent our link for sample to fragment analysis kit
        #  and how these dilutions will be performed need to add value for the other columns

        # map samples across pcr programs
        # create NTC Control for each kit
        # map samples to their reaction mixes, if multiple then add that many samples
        # now traverse across different pcr programs and then create plates as neccessary
        # set the plate id
        # add well position to samples
        # add plate well designer elems as needed
        return SapioWebhookResult(True)

    def get_samples_controls_and_reagents(self):
        # get the samples
        samples_step = self.exp_handler.get_step(self.SAMPLES_ENTRY)
        sample_records = self.exp_handler.get_step_records(samples_step)
        if not sample_records:
            raise Exception(f"Sample records not found in the entry named {self.SAMPLES_ENTRY}")
        samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # split samples into actual samples and controls
        actual_samples = []
        controls = []
        for sample in samples:
            if sample.get_ExemplarSampleType_field() == InHouseModel.DATA_TYPE_NAME:
                controls.append(sample)
            else:
                actual_samples.append(sample)
        samples = actual_samples

        # get the reagents
        reagents: List[ConsumableItemModel] = self.__get_reagents()

        return samples, controls, reagents

    def __get_reagents(self):
        reagent_step = self.exp_handler.get_step(self.REAGENT_TRACKING_ENTRY)
        reagent_tracking_records = self.exp_handler.get_step_records(reagent_step)
        if not reagent_tracking_records:
            raise Exception(f"Reagents not found in the entry named '{self.REAGENT_TRACKING_ENTRY}'")
        reagent_lot_numbers: List[str] = list()
        for reagent in reagent_tracking_records:
            part_number = reagent.get_field_value("PartNumber")
            if part_number:
                reagent_lot_numbers.append(part_number)
        reagents: list[ConsumableItemModel] = (
            self.rec_handler.query_models(ConsumableItemModel, ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                                          reagent_lot_numbers))
        return reagents
