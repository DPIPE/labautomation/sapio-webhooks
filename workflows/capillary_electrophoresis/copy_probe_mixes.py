from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, ProbeMixSampleModel
from utilities.webhook_handler import OsloWebhookHandler


class CopyProbeMixes(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        # Get the parent samples and the aliquots.
        parent_samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        aliquots: list[SampleModel] = self.exp_handler.get_step_models("Resulting Samples", SampleModel)

        # For each aliquot, grab it's respective parent and assign the parent's probe mix record as a child of the
        # aliquot as well.
        self.rel_man.load_children_of_type(parent_samples, ProbeMixSampleModel)
        self.rel_man.load_parents_of_type(aliquots, SampleModel)
        for aliquot in aliquots:
            parent_sample: SampleModel = [x for x in parent_samples if x in aliquot.get_parents_of_type(SampleModel)][0]
            parent_probe_mix: ProbeMixSampleModel = parent_sample.get_child_of_type(ProbeMixSampleModel)
            aliquot.add_child(parent_probe_mix)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
