from utilities.data_models import SampleModel, PlateModel


class CapillaryElectrophoresisUtils:
    @staticmethod
    def map_new_to_old_plates(aliquots: list[SampleModel],
                              all_parent_samples: list[SampleModel]) -> dict[PlateModel, set[PlateModel]]:
        new_to_old_plates: dict[PlateModel, set[PlateModel]] = {}
        for aliquot in aliquots:
            try:
                parent_sample: SampleModel = \
                    [x for x in all_parent_samples if x in aliquot.get_parents_of_type(SampleModel)][0]
                new_plate: PlateModel = aliquot.get_parent_of_type(PlateModel)
                if new_plate not in new_to_old_plates.keys():
                    new_to_old_plates[new_plate] = set()
                new_to_old_plates[new_plate].add(parent_sample.get_parent_of_type(PlateModel))
            except Exception:
                ...
        return new_to_old_plates
