from typing import cast

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnEntryCriteria, ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnEntryType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities import utils
from utilities.constants import Constants
from utilities.data_models import SampleModel, PlateModel, CapillaryElectrophoresisSampleModel, AttachmentModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class PrintABIInputFiles(OsloWebhookHandler):
    callback_util: CallbackUtil
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.callback_util = CallbackUtil(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get the resulting samples as well as their current plates, and load their capillary electrophoresis extension
        # children.
        aliquots: list[SampleModel] = (
            self.exp_handler.get_step_models(cast(ElnEntryStep, context.active_step), SampleModel))
        if not aliquots:
            return SapioWebhookResult(True)
        self.rel_man.load_parents_of_type(aliquots, PlateModel)
        plates: list[PlateModel] = [x.get_parent_of_type(PlateModel) for x in aliquots]
        self.rel_man.load_children_of_type(aliquots, CapillaryElectrophoresisSampleModel)

        # Get the process name.
        try:
            process_name: str = (
                SampleUtils.get_current_process(self.exp_handler.get_step_models("Samples", SampleModel)[0],
                                                self.rel_man)).get_ProcessName_field()
        except Exception:
            process_name: str = self.exp_handler.get_experiment_template().template_name

        # Write the file data.
        self.rel_man.load_children_of_type(plates, SampleModel)
        plate_ids_to_file_data: dict[str, bytes] = (
            self.write_file_data(plates, context.eln_experiment.created_by, process_name))

        # For each plate, create an attachment entry as well as an ABI input file attachment to add to each entry.
        self.create_attachment_entries(context, plate_ids_to_file_data)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def write_file_data(self, plates: list[PlateModel], author: str, process_name: str) -> dict[str, bytes]:
        # Write file data for each of the merged plates.
        plate_ids_to_file_data: dict[str, bytes] = {}
        for plate in plates:
            file_data: str = ("Container Name\tPlate ID\tDescription\tContainer Type\tAppType\tOwner\tOperator"
                              f"\tPlateSealing\tSchedulingPref\n{process_name}\tBARCODE\t\t96-Well\tRegular"
                              f"\t{author}"
                              f"\t{author}\tHeat sealing\t1234\nAppServer\tAppInstance"
                              f"\nGeneMapper\tGeneMapper_Generic_Instance\nWell\tSample Name\tComment\tSample Type"
                              f"\tSnp Set\tAnalysis Method\tPanel\tUser-Defined 3\tSize Standard\t User-Defined 2"
                              "\tUser-Defined 1\tResults Group 1\tInstrument Protocol 1")
            if process_name == "Fragment Analysis":
                file_data += "\tResults Group 2\tInstrument Protocol 2"
            file_data += "\n"

            well_to_sample: dict[str, SampleModel] = {}
            samples: list[SampleModel] = plate.get_children_of_type(SampleModel)
            for sample in samples:
                well_to_sample[sample.get_RowPosition_field() +
                               utils.add_leading_zeroes(sample.get_ColPosition_field(), 2)] = sample

            # Get the instrument protocol to use as a substitute for samples that don't have one.
            filler_instrument_protocol: str = self.get_filler_instrument_protocol(samples)

            for location in Constants.PLATE_LOCATIONS_LEADING_ZEROES:
                try:
                    sample: SampleModel = well_to_sample[location]
                    sample_name: str = self.get_sample_name(sample, process_name)
                    sample_type: str = sample.get_ExemplarSampleType_field()
                    instrument_protocol = self.get_instrument_protocol(sample, filler_instrument_protocol)
                except Exception:
                    sample_name: str = "_"
                    sample_type: str = ""
                    instrument_protocol: str = filler_instrument_protocol
                file_data += (f"{location}\t{sample_name}\t\t"
                              f"{sample_type}\t\t\t\t\t\t\t\t"
                              f"{self.get_results_group_1(process_name)}\t"
                              f"{instrument_protocol}\t")
                if process_name == "Fragment Analysis":
                    file_data += f"{self.get_results_group_1(process_name)}\t{instrument_protocol}"
                file_data += "\n"

            plate_ids_to_file_data[plate.get_PlateId_field()] = bytes(file_data, "utf-8")

        return plate_ids_to_file_data

    def get_sample_name(self, sample: SampleModel | None, process_name: str) -> str:
        if not sample:
            return "_"
        ce: CapillaryElectrophoresisSampleModel | None = sample.get_child_of_type(CapillaryElectrophoresisSampleModel)
        if not ce:
            return sample.get_OtherSampleId_field()
        if process_name == "MLPA" or process_name == "Fragment Analysis":
            return f"{ce.get_ProbeMixName_field()}_{sample.get_OtherSampleId_field()}"
        if process_name == "MS-MLPA":
            return f"{ce.get_ProbeMixName_field()}_A/B_{sample.get_OtherSampleId_field()}"

    def get_results_group_1(self, process_name: str) -> str:
        if process_name == "MLPA":
            return "MLPA"
        else:
            return "Generell_GM"

    def get_instrument_protocol(self, sample: SampleModel, filler_instrument_protocol: str) -> str:
        ce: CapillaryElectrophoresisSampleModel | None = sample.get_child_of_type(CapillaryElectrophoresisSampleModel)
        if not ce or not ce.get_InstrumentProtocol_field():
            return filler_instrument_protocol
        return ce.get_InstrumentProtocol_field()

    def get_filler_instrument_protocol(self, samples: list[SampleModel]) -> str:
        for sample in samples:
            instrument_protocol: str = (sample.get_child_of_type(CapillaryElectrophoresisSampleModel)
                                        .get_InstrumentProtocol_field())
            if instrument_protocol:
                return instrument_protocol
        return "N/A"

    def create_attachment_entries(self, context: SapioWebhookContext, plate_ids_to_file_data: dict[str, bytes]) -> None:
        last_step_num: int = len(self.exp_handler.get_all_steps())
        for plate_id in plate_ids_to_file_data:
            # Create the attachment entry if it doesn't already exist.
            entry_exists: bool = False
            try:
                entry: ElnEntryStep = self.exp_handler.get_step(f"{plate_id} ABI 3730 Analyzer Input File")
                attachment_entry: ExperimentEntry = entry.eln_entry
                entry_exists = True
            except Exception:
                entry_criteria: ElnEntryCriteria = (
                    ElnEntryCriteria(ElnEntryType.Attachment, f"{plate_id} ABI 3730 Analyzer Input File",
                                     AttachmentModel.DATA_TYPE_NAME, last_step_num, False))
                attachment_entry: ExperimentEntry = (
                    self.eln_man.add_experiment_entry(context.eln_experiment.notebook_experiment_id, entry_criteria))
                last_step_num += 1

            # If the attachment entry exists, then just update the attachment. Otherwise, add a new attachment to the
            # entry.
            if entry_exists:
                attachment: AttachmentModel = (
                    self.exp_handler.get_step_models(f"{plate_id} ABI 3730 Analyzer Input File", AttachmentModel))[0]
                AttachmentUtil.set_attachment_bytes(context, attachment, f"{plate_id}_ABI.txt",
                                                    plate_ids_to_file_data[plate_id])
            else:
                attachment: AttachmentModel = (
                    AttachmentUtil.create_attachment(context, f"{plate_id}_ABI.txt", plate_ids_to_file_data[plate_id],
                                                     AttachmentModel))
                crit: ElnAttachmentEntryUpdateCriteria = ElnAttachmentEntryUpdateCriteria()
                crit.record_id = attachment.record_id
                crit.attachment_name = attachment.get_FilePath_field()
                context.eln_manager.update_experiment_entry(context.eln_experiment.notebook_experiment_id,
                                                            attachment_entry.entry_id, crit)
