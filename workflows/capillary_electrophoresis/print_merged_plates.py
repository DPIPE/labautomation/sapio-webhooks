from typing import cast

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.capillary_electrophoresis.capillary_electrophoresis_utils import CapillaryElectrophoresisUtils


class PrintMergedPlates(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.callback_util = CallbackUtil(context)

        # Grab all the samples from both the resulting and initial samples entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        if not samples:
            return SapioWebhookResult(True)
        aliquots: list[SampleModel] = self.exp_handler.get_step_models(
            cast(ElnEntryStep, context.active_step),
            SampleModel
        )
        if not aliquots:
            return SapioWebhookResult(True)

        # Map each child sample's plate ID to their parent sample's plate ID.
        self.rel_man.load_parents_of_type(samples, PlateModel)
        self.rel_man.load_parents_of_type(aliquots, PlateModel)
        self.rel_man.load_parents_of_type(aliquots, SampleModel)
        new_to_old_plates: dict[PlateModel, set[PlateModel]] = (
            CapillaryElectrophoresisUtils.map_new_to_old_plates(aliquots, samples))

        # Write the file data based on the map.
        file_data: str = ""
        for new_plate in new_to_old_plates:
            file_data += f"{new_plate.get_PlateId_field()}:\n"
            for old_plate in new_to_old_plates[new_plate]:
                file_data += f"Plate name: {old_plate.get_PlateId_field()}\n"
            file_data += "\n"

        expr_id: int = context.eln_experiment.notebook_experiment_id
        now = TimeUtil.now_in_format('%m%d%Y')
        file_name: str = f"CE_{expr_id}_{now}_plates.csv"

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(file_data, "utf-8"),
                None,
                self.exp_handler,
            )
        )

        # Download the file to the user.
        self.callback_util.write_file(
            file_name, bytes(file_data, "utf-8"))

        return SapioWebhookResult(True)
