from typing import cast

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel, PlateModel, CapillaryElectrophoresisSampleModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.capillary_electrophoresis.capillary_electrophoresis_utils import CapillaryElectrophoresisUtils


class GeneratePlateIDs(OsloWebhookHandler):
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)

        # Get the aliquots from the entry.
        aliquots: list[SampleModel] = (
            self.exp_handler.get_step_models(cast(ElnEntryStep, context.active_step), SampleModel))
        if not aliquots:
            return SapioWebhookResult(True)

        # Get the parent samples from the samples entry.
        all_parent_samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Load all necessary related records.
        self.rel_man.load_parents_of_type(all_parent_samples, PlateModel)
        self.rel_man.load_parents_of_type(aliquots, PlateModel)
        self.rel_man.load_parents_of_type(aliquots, SampleModel)
        self.rel_man.load_children_of_type(aliquots, CapillaryElectrophoresisSampleModel)

        # Map the merged plates to their old plates.
        new_to_old_plates: dict[PlateModel, set[PlateModel]] = (
            CapillaryElectrophoresisUtils.map_new_to_old_plates(aliquots, all_parent_samples))

        # Grab all the new merged plates.
        merged_plates_set: set[PlateModel] = set()
        for aliquot in aliquots:
            merged_plates_set.add(aliquot.get_parent_of_type(PlateModel))
        merged_plates: list[PlateModel] = list(merged_plates_set)
        self.rel_man.load_children_of_type(merged_plates, SampleModel)

        # Grab all the old plates.
        old_plates: list[PlateModel] = []
        for sample in all_parent_samples:
            old_plates.append(sample.get_parent_of_type(PlateModel))

        # Map each probe mix to their respective plates. There should only be one probe mix per plate.
        probe_mixes_to_plates: dict[str, set[PlateModel]] = {}
        for plate in merged_plates:
            probe_mix: str | None = (
                plate.get_children_of_type(SampleModel)[0].get_child_of_type(CapillaryElectrophoresisSampleModel)
                .get_ProbeMixName_field())
            if not probe_mix:
                continue
            if probe_mix not in probe_mixes_to_plates.keys():
                probe_mixes_to_plates[probe_mix] = set()
            probe_mixes_to_plates[probe_mix].add(plate)

        # Set the proper plate names for all the merged plates as long as a probe mix is present.
        plates: list[PlateModel] = []
        for aliquot in aliquots:
            plates.append(aliquot.get_parent_of_type(PlateModel))
        for plate in plates:
            probe_mix: str | None = (
                plate.get_children_of_type(SampleModel)[0].get_child_of_type(CapillaryElectrophoresisSampleModel)
                .get_ProbeMixName_field())
            if probe_mix:
                plate.set_PlateId_field(self.calculate_merged_plate_id(plate, list(new_to_old_plates[plate]),
                                                                       list(probe_mixes_to_plates[probe_mix])))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def calculate_merged_plate_id(self, merged_plate: PlateModel, old_plates: list[PlateModel],
                                  shared_probe_mix_plates: list[PlateModel]) -> str:
        # Remove the date sections from all the old plate IDs and append them to the merged plate ID.
        formatted_plate_ids: list[str] = [self.remove_date_from_plate_id(x.get_PlateId_field()) for x in old_plates]
        merged_plate_id: str = "_".join(formatted_plate_ids)

        # Set the name of the merged plate while accounting for other plates that may share the same probe mix.
        if len(shared_probe_mix_plates) > 1:
            index: int = shared_probe_mix_plates.index(merged_plate) + 1
            merged_plate_id += f"_{self.int_to_char(index)}"
        return merged_plate_id

    def remove_date_from_plate_id(self, plate_id: str) -> str:
        try:
            id_parts: list[str] = plate_id.split("_")
            return f"{id_parts[0]}_{id_parts[2]}"
        except Exception:
            return plate_id

    def int_to_char(self, n: int) -> str | None:
        if n < 1 or n > 26:
            return None
        return chr(64 + n)
