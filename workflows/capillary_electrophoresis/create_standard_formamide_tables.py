from typing import cast, Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.datatype.FieldDefinition import AbstractVeloxFieldDefinition, VeloxStringFieldDefinition, \
    VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnEntryCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnEntryType, ElnBaseDataType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import SampleModel, PlateModel, CapillaryElectrophoresisSampleModel
from utilities.webhook_handler import OsloWebhookHandler


STANDARDS_TO_VOLUMES: dict[str, float] = {
    "500 LIZ": 0.2,
    "600 LIZ": 0.5,
    "1200 LIZ": 0.2,
    "ROX1000": 2.0
}


class CreateStandardFormamideTables(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get all the plates that the new samples are on and load their needed children.
        aliquots: list[SampleModel] = self.exp_handler.get_step_models(cast(ElnEntryStep, context.active_step),
                                                                       SampleModel)
        if not aliquots:
            return SapioWebhookResult(False, "There were no samples aliquoted in the experiment.")
        self.rel_man.load_parents_of_type(aliquots, PlateModel)
        plates: list[PlateModel] = list(set([x.get_parent_of_type(PlateModel) for x in aliquots]))
        self.rel_man.load_children_of_type(aliquots, CapillaryElectrophoresisSampleModel)

        # Load all the child samples under the plates.
        self.rel_man.load_children_of_type(plates, SampleModel)

        # Create a list of field definitions for the table entries.
        field_defs: list[AbstractVeloxFieldDefinition] = [
            VeloxStringFieldDefinition("Reagents", "Reagents", "Reagents"),
            VeloxDoubleFieldDefinition("Volume", "Volume", "Volume")
        ]

        # Create a table entry for each standard per plate.
        for x, plate in enumerate(plates, start=4):
            # Get the total number of controls on the plate.
            plate_samples: list[SampleModel] = plate.get_children_of_type(SampleModel)
            num_controls: int = 0
            for plate_sample in plate_samples:
                if plate_sample.get_IsControl_field():
                    num_controls += 1

            # Get all the standards on the plate and create table entries for each of them.
            standards: list[str] = \
                list(set([x.get_child_of_type(CapillaryElectrophoresisSampleModel).get_Standard_field()
                          for x in plate_samples if x.get_child_of_type(CapillaryElectrophoresisSampleModel)]))
            for standard in standards:
                self.generate_standard_table(standard, num_controls, len(aliquots), len(standards),
                                             plate.get_PlateId_field(), field_defs,
                                             context.eln_experiment.notebook_experiment_id, x)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def generate_standard_table(self, standard: str, num_controls: int, num_samples: int, num_standards: int,
                                plate_id: str, field_defs: list[AbstractVeloxFieldDefinition], exp_id: int,
                                order_count: int) -> None:
        # Add the initial row.
        table_data: list[dict[str, Any]] = [{"Reagents": "Formamide", "Volume": 12.0 if "LIZ" in standard else 11}]

        # Calculate the volume field for the standard row.
        standard_row: dict[str, Any] = {"Reagents": standard, "Volume": 0.0}
        volume: float = STANDARDS_TO_VOLUMES[standard] * (num_controls + num_samples)
        volume += 14 if num_standards == 1 else 5
        standard_row["Volume"] = volume
        table_data.append(standard_row)

        # Calculate the total volume field for the final row.
        total_volume: float = 0.0
        for row in table_data:
            total_volume += row["Volume"]
        table_data.append({"Reagents": "Total volume", "Volume": total_volume})

        # Generate a table and add it to the experiment. If an entry already exists, then remove the existing rows and
        # add new ones.
        try:
            entry: ElnEntryStep = self.exp_handler.get_step(f"{plate_id} {standard}")
            self.exp_handler.remove_eln_rows(entry, self.exp_handler.get_step_records(entry))

            data_type: str = entry.get_data_type_names()[0]
            for row in table_data:
                row_record: PyRecordModel = self.inst_man.add_new_record(data_type)
                row_record.set_field_values(row)

            self.rec_man.store_and_commit()
        except Exception:
            criteria: ElnEntryCriteria = ElnEntryCriteria(ElnEntryType.Table, f"{plate_id} {standard}",
                                                          ElnBaseDataType.EXPERIMENT_DETAIL.data_type_name,
                                                          order_count, field_map_list=table_data,
                                                          field_definition_list=field_defs, is_renamable=False)
            self.eln_man.add_experiment_entry(exp_id, criteria)
