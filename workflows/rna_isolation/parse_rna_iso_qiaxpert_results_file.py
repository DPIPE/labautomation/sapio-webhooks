from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.files.file_util import FileUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities import utils
from utilities.data_models import ELNExperimentDetailModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler

MAX_CONCENTRATION: float = 150.0


class ParseRnaIsoQiaxpertResultsFile(OsloWebhookHandler):
    callback_util: CallbackUtil
    samples_by_name: dict[str, SampleModel] = {}

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        self.callback_util = CallbackUtil(context)

        # Request file upload and parse into CSV lines.
        _, file_bytes = self.callback_util.request_file("Upload QIAxpert results file", ["csv"])
        lines, _ = FileUtil.tokenize_csv(file_bytes, seperator="\t")

        # Delete all existing exp details and recalculate, in case this is invoked as a toolbar button.
        existing_records = context.eln_manager.get_data_records_for_entry(
            context.eln_experiment.notebook_experiment_id,
            context.experiment_entry.entry_id
        ).result_list
        self.dr_man.delete_data_record_list(existing_records)

        name_to_detail: dict[str, ELNExperimentDetailModel] = {}
        for line in lines:
            name = line["Sample name"]
            conc = float(line["A260 Concentration (ng/ul)"])
            try:
                a260a280 = float(line["A260/A280"])
            except TypeError:
                a260a280 = None
            try:
                a260a230 = float(line["A260/A230"])
            except TypeError:
                a260a230 = None

            if name in name_to_detail:
                detail = name_to_detail[name]
            else:
                detail = self.exp_handler.add_eln_row(context.experiment_entry.entry_name, ELNExperimentDetailModel)
                detail.set_field_value("SampleName2", name)
                name_to_detail[name] = detail

            # Otherwise, we need to check if there's a first value already set, and calculate mean if so.
            # Annoyingly, there doesn't seem to be a way to get exp detail record definitions accurately...
            # ... but we can set fields even if the definition doesn't know they exist. So we just catch
            # the KeyError and let it get set properly later. Simple and effective.
            try:
                existing_conc = float(detail.get_field_value("Concentration1nguL"))
            except KeyError:
                existing_conc = False
            if existing_conc:
                mean_conc = (existing_conc + conc) / 2
                detail.set_field_value("Concentration2nguL", conc)
                detail.set_field_value("MeanConcentrationnguL", mean_conc)

                # Calculate final concentration, which may require a dilution.
                final_conc = mean_conc
                if mean_conc > MAX_CONCENTRATION:
                    sample = self.get_sample_by_name(context, name)

                    source_volume = sample.get_Volume_field()
                    source_mass = mean_conc * source_volume
                    target_volume = source_mass / MAX_CONCENTRATION
                    buffer_volume = target_volume - source_volume

                    final_conc = MAX_CONCENTRATION

                    detail.set_field_value("RNAseFreeWateruL", buffer_volume)

                detail.set_field_value("FinalConcentrationnguL", final_conc)
            else:
                detail.set_field_value("Concentration1nguL", conc)
                detail.set_field_value("MeanConcentrationnguL", conc)

            try:
                existing_a280 = float(detail.get_field_value("FirstA260A280"))
            except KeyError:
                existing_a280 = False
            if existing_a280:
                detail.set_field_value("SecondA260A280", a260a280)
                detail.set_field_value("MeanA260A280", (existing_a280 + a260a280) / 2)
            else:
                detail.set_field_value("FirstA260A280", a260a280)
                detail.set_field_value("MeanA260A280", a260a280)

            try:
                existing_a230 = float(detail.get_field_value("FirstA260A230"))
            except KeyError:
                existing_a230 = False
            if existing_a230:
                detail.set_field_value("SecondA260A230", a260a230)
                detail.set_field_value("MeanA260A230", (existing_a230 + a260a230) / 2)
            else:
                detail.set_field_value("FirstA260A230", a260a230)
                detail.set_field_value("MeanA260A230", a260a230)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def get_sample_by_name(self, context, name) -> SampleModel:
        """
        This method initializes a dictionary of samples mapped by sample name, unless it was called before.
        In that case, it takes the cached dictionary.
        Then it uses that dictionary to return the sample associated with the passed name.
        """

        if self.samples_by_name:
            return self.samples_by_name[name]

        samples_entry = utils.get_entry_by_option(context, "INITIAL SAMPLES ENTRY")
        samples = self.rec_handler.wrap_models(
            context.eln_manager.get_data_records_for_entry(
                context.eln_experiment.notebook_experiment_id,
                samples_entry.entry_id),
            SampleModel)
        self.samples_by_name: dict[str, SampleModel] = \
            {samples[i].get_OtherSampleId_field(): samples[i] for i in range(0, len(samples))}

        return self.samples_by_name[name]
