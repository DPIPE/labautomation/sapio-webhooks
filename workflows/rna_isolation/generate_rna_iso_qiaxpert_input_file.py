from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PlateDesignerWellElementModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author: Austin Decker

Invocation: Entry submit, and entry toolbar button (RNA Isolation | Setup QIAxpert Plate)

Description: Locates the plate record, and related PDWE records. Generates a file from those records. Note that
no aliquots are actually be created.
"""


class GenerateRnaIsoQiaxpertInputFile(OsloWebhookHandler):
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        self.callback_util = CallbackUtil(context)

        entry_options = context.eln_manager.get_experiment_entry_options(
            context.eln_experiment.notebook_experiment_id,
            context.experiment_entry.entry_id
        )

        plate_record_id = int(entry_options["MultiLayerPlating_Plate_RecordIdList"])
        pdwes = self.rec_handler.query_models(PlateDesignerWellElementModel,
                                              PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME,
                                              [plate_record_id])

        # Later, we will be iterating through all possible plate positions, but not all will be filled.
        # This dict tells us which ones are filled.
        pos_to_pdwe_names: dict[str, str] = {}

        # We know they will be using replicates, so build a cache to avoid unnecessary calls to the server.
        rec_id_to_found_sample_names: dict[int, str] = {}

        for pdwe in pdwes:
            pos = pdwe.get_RowPosition_field() + pdwe.get_ColPosition_field()

            if pdwe.get_IsControl_field():
                name = pdwe.get_ControlType_field()
            else:
                source_record_id = pdwe.get_SourceRecordId_field()
                if source_record_id in rec_id_to_found_sample_names:
                    name = rec_id_to_found_sample_names[source_record_id]
                else:
                    sample = self.rec_handler.query_models_by_id(SampleModel, [source_record_id])[0]
                    name = sample.get_OtherSampleId_field()
                    rec_id_to_found_sample_names[source_record_id] = name

            pos_to_pdwe_names[pos] = name

        # Since there's a limited number of positions, not worth it to do anything fancy.
        positions = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1",
                     "A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"]

        # Extremely simple file - no headers, just two tab delimited columns.
        file_data: str = ""
        for pos in positions:
            name = pos_to_pdwe_names.get(pos, "")
            file_data += f"{pos}\t{name}\n"

        file_name = f"TILS_{context.eln_experiment.notebook_experiment_id}_{context.user.username}.txt"

        # add to Attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(file_data, "utf-8"),
                "Generated Input File",
                None,
                self.inst_man
            )
        )

        self.callback_util.write_file(file_name, file_data)

        return SapioWebhookResult(True)
