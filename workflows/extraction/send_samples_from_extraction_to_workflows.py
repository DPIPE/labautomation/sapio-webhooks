from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, RequestModel, AssayDetailModel, SampleRegistrationErrorModel
from utilities.request_utils import RequestUtils, OrderStatus
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class SendSamplesFromExtractionToWorkflows(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.callback_util = CallbackUtil(context)

        # Get all the samples from the entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        if not samples:
            return SapioWebhookResult(True)

        # Get the orders that the samples are under by grabbing their top level samples and getting their order
        # parents, then get all the assay details under the orders. Also, only grab orders which have been approved,
        # since we don't want to queue samples from unapproved orders.
        error_orders: list[dict[str, str]] = []

        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        orders: list[RequestModel] = [t.get_parent_of_type(RequestModel) for t in top_level_samples]
        if not orders:
            return SapioWebhookResult(True)
        approved_orders: list[RequestModel] = \
            [x for x in orders if x.get_OrderStatus_field() == OrderStatus.ORDER_APPROVED.value]
        if not approved_orders:
            self.callback_util.ok_dialog("Warning", "None of the orders that the extracted samples are under have been"
                                                    " approved, so these samples will not be sent to workflows.")
            return SapioWebhookResult(True)
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
            RequestUtils.get_orders_to_assay_details(self.rel_man, approved_orders))

        # Add rows in the error table for any unapproved orders.
        unapproved_orders: list[RequestModel] = list(set(orders) - set(approved_orders))
        for order in unapproved_orders:
            error_orders.append({"OrderName": order.get_RequestName_field(),
                                 "Reason": "Order not approved."})

        # Create a list of samples under approved orders.
        approved_samples: list[SampleModel] = []
        for sample in samples:
            top_level_sample: SampleModel = \
                [x for x in top_level_samples if x.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
            if top_level_sample.get_parent_of_type(RequestModel) in approved_orders:
                approved_samples.append(sample)

        # Delete all current sample registration errors under the orders.
        self.rel_man.load_children_of_type(approved_orders, SampleRegistrationErrorModel)
        for order in approved_orders:
            order.remove_children(order.get_children_of_type(SampleRegistrationErrorModel))

        # Attempt to send the samples to their workflows, capturing any errors that occur when doing so.
        errors: dict[RequestModel, list[SampleRegistrationErrorModel]] = (
            WorkflowUtils.send_samples_to_workflows(context, self.rec_handler, self.rel_man, orders_to_assay_details,
                                                    approved_samples))
        for order in errors:
            filtered_errors: list[SampleRegistrationErrorModel] = [x for x in errors[order] if x]
            order.set_SampleRegistrationSuccessful_field(not filtered_errors)
            # [OSLO-1136]: Set the order status appropriately.
            if filtered_errors:
                order.add_children(filtered_errors)
                order.set_OrderStatus_field(OrderStatus.ORDER_APPROVAL_ERRORS.value)
                error_orders.append({"OrderName": order.get_RequestName_field(),
                                     "Reason": "See order for details."})
            else:
                order.set_OrderStatus_field(OrderStatus.ORDER_APPROVED.value)

        self.rec_man.store_and_commit()

        # If there were any errors sending samples to workflows, show a table of the orders that errored to the user and
        # tell them to check the orders for more details about the failure. Otherwise, show an OK dialog telling the
        # user that all samples were sent successfully.
        if error_orders:
            self.callback_util.table_dialog("Warning", "Some or all of the samples under the following orders could not"
                                                       " be sent to all or some of their workflows. Check each order"
                                                       " for more details about the failure.",
                                            [VeloxStringFieldDefinition("OrderName", "OrderName", "Order Name"),
                                             VeloxStringFieldDefinition("Reason", "Reason", "Reason")],
                                            error_orders)
        else:
            self.callback_util.ok_dialog("Notice", "All samples successfully sent to workflows.")

        return SapioWebhookResult(True)
