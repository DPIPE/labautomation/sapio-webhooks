from typing import Any

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import CustomReportCriteria, ReportColumn, RawReportTerm, RawTermOperation, \
    CompositeReportTerm, CompositeTermOperation
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ConsumableItemModel
from utilities.data_models import ELNExperimentDetailModel
from utilities.data_models import ReagentPlateColumnModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class LastUsedCol(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        reagent_details: ELNExperimentDetailModel = self.rule_handler.get_models(ELNExperimentDetailModel)[0]
        part_number: str = reagent_details.get_field_value("PartNumber")
        lot_number: str = reagent_details.get_field_value("ConsumableLot")

        # Get the matching reagent record in the system. Before, this was just querying by lot number, which was
        # returning reagents outside the selected consumable type.
        terms: list[RawReportTerm] = [RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME, "LotNumber",
                                                    RawTermOperation.EQUAL_TO_OPERATOR, lot_number),
                                      RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME, "PartNumber",
                                                    RawTermOperation.EQUAL_TO_OPERATOR, part_number),
                                      RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME, "ConsumableType",
                                                    RawTermOperation.EQUAL_TO_OPERATOR, "Covaris 96 microTUBE plate")]
        root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeTermOperation.AND_OPERATOR, terms[1])
        root_term = CompositeReportTerm(root_term, CompositeTermOperation.AND_OPERATOR, terms[2])
        results: list[list[Any]] = (
            DataMgmtServer.get_custom_report_manager(context.user).run_custom_report(
                CustomReportCriteria([ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)],
                                     root_term)).result_table)
        if not results:
            self.callback.ok_dialog("Error", "No Covaris reagent with specified part and lot number found.")
            return SapioWebhookResult(True)
        reagent: ConsumableItemModel = self.rec_handler.query_models_by_id(ConsumableItemModel, [int(results[0][0])])[0]

        # Set the last used column field on the reagent plate column record.
        self.rel_man.load_parents_of_type([reagent], SampleModel)
        linked_sample: SampleModel | None = reagent.get_parent_of_type(SampleModel)
        if linked_sample:
            self.rel_man.load_children_of_type([linked_sample], ReagentPlateColumnModel)
            column: ReagentPlateColumnModel | None = linked_sample.get_child_of_type(ReagentPlateColumnModel)
            if column:
                col_label: str = column.get_Col_field()
                reagent_details.set_field_value("LastUsedCol", col_label)

                self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
