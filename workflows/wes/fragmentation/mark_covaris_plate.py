import json
import math

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ConsumableItemModel, ReagentPlateColumnModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"


class MarkCovarisPlate(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                self.exp_handler.update_step(context.active_step.get_name(),
                                             entry_status=ExperimentEntryStatus.UnlockedChangesRequired)
                return SapioWebhookResult(True)

        # Check to make sure the user selected a Covaris plate. If they didn't, give them a warning and un-submit the
        # entry.
        records: list[DataRecord] | None = self.exp_handler.get_step_records(context.active_step.get_name())
        if not records:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "You must select a Covaris plate to proceed.",
                                      request_context=json.dumps(self.response_map))
        try:
            covaris_record: DataRecord = \
                [x for x in records if x.get_field_value("ConsumableType") == "Covaris 96 microTUBE plate"][0]
        except Exception:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "You must select a Covaris plate to proceed.",
                                      request_context=json.dumps(self.response_map))

        # Get the reagent from the entry.
        # [OSLO-1050]: If a lot number or part number isn't selected, then throw an error to the user since this
        # technically means no reagent was selected.
        try:
            reagents: list[ConsumableItemModel] = (
                self.rec_handler.query_models(ConsumableItemModel,
                                              ConsumableItemModel.PARTNUMBER__FIELD_NAME.field_name,
                                              [covaris_record.get_field_value("PartNumber")]))
        except Exception:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "No part number was selected. Please select a part and lot number for"
                                               " the Covaris reagent in order to continue.",
                                      request_context=json.dumps(self.response_map))
        try:
            reagent: ConsumableItemModel = [reagent for reagent in reagents if reagent.get_LotNumber_field() ==
                                            covaris_record.get_field_value("ConsumableLot")][0]
        except Exception:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "No lot number was selected. Please select a part and lot number for"
                                               " the Covaris reagent in order to continue.",
                                      request_context=json.dumps(self.response_map))

        # Check if the number of samples in the experiment is larger than the total number of available wells on the
        # covaris plate. If there are, give the user a warning and prevent them from submitting the entry.
        self.rel_man.load_parents_of_type([reagent], SampleModel)
        parent_sample: SampleModel = reagent.get_parent_of_type(SampleModel)
        self.rel_man.load_children_of_type([parent_sample], ReagentPlateColumnModel)
        columns: list[ReagentPlateColumnModel] = parent_sample.get_children_of_type(ReagentPlateColumnModel)
        available_wells: int = 0
        for column in columns:
            if not column.get_Used_field():
                available_wells += 1
        available_wells = available_wells * 8
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        if len(samples) > available_wells:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "The selected Covaris plate does not have enough available wells to"
                                                 " fit all samples.", request_context=json.dumps(self.response_map))

        # Occupy each available well in the Covaris plate in order from A1 -> A12.
        wells_to_occupy: int = math.ceil(len(samples) / 8)
        columns.sort(key=self.column_sorting_criteria)
        for x in range(wells_to_occupy):
            columns[x].set_Used_field(True)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def column_sorting_criteria(self, column: ReagentPlateColumnModel) -> int:
        return int(column.get_Col_field()[1:len(column.get_Col_field())])
