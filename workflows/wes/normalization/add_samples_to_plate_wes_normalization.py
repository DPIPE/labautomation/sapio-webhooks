import json

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.constants import Constants
from utilities.data_models import PlateModel, SampleModel, PlateDesignerWellElementModel, FamilyDetailsModel, \
    WESAssayDetailModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_CONCENTRATION_WARNING: str = "concentrationWarning"
STATUS_ERROR: str = "error"


class AddSamplesToPlateWESNormalization(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_CONCENTRATION_WARNING:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # If the entry has already been submitted, tell the user that they can't plate the samples again
        status: ExperimentEntryStatus = self.exp_handler.get_step(context.active_step.get_name()).eln_entry.entry_status
        if status == ExperimentEntryStatus.Completed or status == ExperimentEntryStatus.CompletedApproved:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "The entry has already been completed.",
                                      request_context=json.dumps(self.response_map))

        # Get the plate record
        plate: PlateModel = self.get_plate()

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Group each sample onto the plate
        return self.group_samples(plate, samples)

    def get_plate(self) -> PlateModel:
        plate_entry: ElnEntryStep = self.exp_handler.get_step("Normalization Setup Plate")
        record_id: int = int(plate_entry.get_options()["MultiLayerPlating_Plate_RecordIdList"])
        return self.rec_handler.query_models_by_id(PlateModel, [record_id])[0]

    def group_samples(self, plate: PlateModel, samples: list[SampleModel]) -> SapioWebhookResult:
        # Remove any existing well assignments to avoid layers
        existing_wells: list[PlateDesignerWellElementModel] = (
            self.rec_handler.query_models(PlateDesignerWellElementModel,
                                          PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                          [plate.record_id]))
        if existing_wells:
            for e in existing_wells:
                e.delete()
            self.rec_man.store_and_commit()

        # Sort the list of samples by priority
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        SampleUtils.sort_samples_by_priority(samples, self.rec_handler, self.rel_man, WESAssayDetailModel, top_level_samples)

        # Place each sample onto the plate
        pdwes: list[PlateDesignerWellElementModel] = []
        pl_count: int = 0
        self.rel_man.load_children_of_type(top_level_samples, FamilyDetailsModel)
        for i, s in enumerate(samples):
            pdwe: PlateDesignerWellElementModel = self.create_well_element(s, plate)
            well_location: str = Constants.PLATE_LOCATIONS[i]
            pdwe.set_RowPosition_field(well_location[0:1])
            pdwe.set_ColPosition_field(well_location[1: len(well_location)])
            pdwes.append(pdwe)
            if pl_count >= len(Constants.PLATE_LOCATIONS):
                self.response_map[STATUS] = STATUS_ERROR
                return PopupUtil.ok_popup("Error", f"Plate {plate.get_PlateId_field()} does not have enough wells"
                                                   f"to contain all samples.",
                                          request_context=json.dumps(self.response_map))

        # Store and commit to get the record IDs of the aliquots
        self.rec_man.store_and_commit()

        # Show a table warning of too-low concentrations and their new values if there are any
        low_concentrations: list[dict[str, str]] = self.get_low_concentrations(pdwes)
        if len(low_concentrations) > 0:
            # Store and commit changes
            self.rec_man.store_and_commit()

            self.response_map[STATUS] = STATUS_CONCENTRATION_WARNING
            return PopupUtil.table_popup("Warning", "The following well locations have too low of concentrations to the"
                                                    " maximum volume of 50 uL. The concentration values were"
                                                    " automatically recalculated and can be seen below.",
                                         [VeloxStringFieldDefinition("Warning", "WellLocation", "Well Location"),
                                          VeloxDoubleFieldDefinition("Warning", "Concentration", "Previous "
                                                                                                 "Concentration"),
                                          VeloxDoubleFieldDefinition("Warning", "NewConcentration",
                                                                     "New Concentration")],
                                         low_concentrations,
                                         request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)

    def create_well_element(self, sample: SampleModel, plate: PlateModel) -> PlateDesignerWellElementModel:
        pdwe: PlateDesignerWellElementModel = self.rec_handler.add_model(PlateDesignerWellElementModel)
        pdwe.set_PlateRecordId_field(plate.record_id)
        pdwe.set_Layer_field(1)
        pdwe.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
        pdwe.set_SourceRecordId_field(sample.record_id)

        # If the sample is a control sample, then just return the well element without doing any normalization.
        if SampleUtils.is_control(sample):
            return pdwe

        # Calculate and set the normalization values for the sample. This is wrapped in a try-catch to prevent a
        # division by zero error ruining the entire plating process.
        try:
            target_mass: float = 1000
            target_vol: float = 50

            pdwe.set_SourceSampleMass_field(sample.get_TotalMass_field())

            if sample.get_TotalMass_field() < target_mass:
                target_mass = sample.get_TotalMass_field()

            pdwe.set_TargetMass_field(target_mass)

            src_conc: float = sample.get_Concentration_field()
            pdwe.set_SourceSampleConcentration_field(src_conc)

            src_vol_to_use: float = target_mass / src_conc
            pdwe.set_SourceVolumeToRemove_field(src_vol_to_use)

            pdwe.set_SourceSampleVolume_field(sample.get_Volume_field())

            pdwe.set_Volume_field(target_vol)

            target_conc: float = target_mass / target_vol
            pdwe.set_Concentration_field(target_conc)
        except Exception:
            ...

        return pdwe

    # noinspection PyMethodMayBeStatic
    def get_low_concentrations(self, well_elements: list[PlateDesignerWellElementModel]) -> list[dict[str, str]]:
        # Check the normalization values of each sample, and warn for each normalization that needed greater than 50 uL
        # of volume to get the target mass
        low_concentrations: list[dict[str, str]] = []

        for pdwe in well_elements:
            try:
                src_vol_to_use: float = pdwe.get_SourceVolumeToRemove_field()
                if src_vol_to_use > 50:
                    prev_conc: float = pdwe.get_SourceSampleConcentration_field()
                    pdwe.set_SourceVolumeToRemove_field(50)
                    new_conc: float = pdwe.get_TargetMass_field() / 50
                    pdwe.set_Concentration_field(new_conc)

                    low_concentrations.append({
                        "WellLocation": f"{pdwe.get_RowPosition_field()}{pdwe.get_ColPosition_field()}",
                        "Concentration": prev_conc,
                        "NewConcentration": new_conc
                    })
            except Exception:
                continue

        return low_concentrations
