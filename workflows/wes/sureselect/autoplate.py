from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PlateDesignerWellElementModel
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class SureSelectAutoPlate(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        samples = exp_handler.get_step_models("Samples", SampleModel)

        plate_entry = PlateDesignerEntry(exp_handler.get_step("Quant-iT QC Plating Details"), exp_handler)
        plates: list[PlateModel] = plate_entry.get_plates(PlateModel)

        for idx, sample in enumerate(samples):
            well_elem = self.inst_man.add_new_record_of_type(PlateDesignerWellElementModel)
            well_elem.set_field_value("SourceRecordId", sample.get_field_value("RecordId"))
            well_elem.set_field_value("SourceDataTypeName", "Sample")
            well_elem.set_field_value("Layer", 1)
            well_elem.set_field_value("IsControl", False)
            well_elem.set_field_value("ControlType", None)

            well_elem.set_field_value("RowPosition", chr(ord("A") + idx))
            well_elem.set_field_value("ColPosition", 2)
            well_elem.set_field_value("PlateRecordId", plates[0].get_field_value("RecordId"))

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
