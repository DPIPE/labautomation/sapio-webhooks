import json

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ExperimentEntryCriteriaUtil
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import SampleModel, QCDatumModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"


class CheckAdapterDimerReadings(OsloWebhookHandler):
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                self.exp_handler.update_step(context.active_step.get_name(),
                                             entry_status=ExperimentEntryStatus.UnlockedChangesRequired)
                return SapioWebhookResult(True)

        # Set each pass/fail status in the entry accordingly. Samples should still be able to continue through the
        # workflow even if they "failed" both QC tests. The QC Datum record should still indicate that the sample
        # failed. If there is not maximum pass threshold specified, show an error to the user and un-submit the entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_children_of_type(samples, QCDatumModel)
        adapter_dimer_entry: ElnEntryStep = self.exp_handler.get_step(context.active_step.get_name())
        try:
            max_threshold: float = float(adapter_dimer_entry.get_options()["MAX PASS THRESHOLD"])
        except Exception:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "No maximum pass threshold was set in the MAX PASS THRESHOLD tag on "
                                               "this step.", request_context=json.dumps(self.response_map))
        records: list[PyRecordModel] = self.inst_man.add_existing_records(
            DataMgmtServer.get_eln_manager(context.user)
            .get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                        adapter_dimer_entry.eln_entry.entry_id).result_list)
        un_hide: bool = False
        for record in records:
            sample_id: str = record.get_field_value("SampleId")
            if float(record.get_field_value("AdapterDimers")) > max_threshold:
                un_hide = True
                status: str = "Failed"
                sample: SampleModel = [sample for sample in samples if sample.get_SampleId_field() == sample_id][0]
                qc_datum: QCDatumModel = [qc_datum for qc_datum in sample.get_children_of_type(QCDatumModel)
                                          if qc_datum.get_ExperimentRecordId_field() ==
                                          context.eln_experiment.experiment_record_id][0]
                if qc_datum.get_QCStatus_field() != "Failed - Continue" and qc_datum.get_QCStatus_field() != "Failed":
                    qc_datum.set_QCStatus_field("Failed - Continue")
            else:
                status: str = "Passed"
            record.set_field_value("AdapterDimersStatus", status)

        self.rec_man.store_and_commit()

        # Show the "Purification Material Tracking" entry if any of the Adapter-dimers readings failed and update the
        # instrument tracking entry to show the new status.
        eln_man: ElnManager = DataMgmtServer.get_eln_manager(context.user)
        entries: list[ExperimentEntry] = (
            eln_man.get_experiment_entry_list(context.eln_experiment.notebook_experiment_id))
        if un_hide:
            material_tracking_entry: ExperimentEntry = [entry for entry in entries
                                                        if entry.entry_name == "Purification Material Tracking"][0]
            criteria = ExperimentEntryCriteriaUtil.create_empty_criteria(material_tracking_entry)
            criteria.is_hidden = False
            eln_man.update_experiment_entry(context.eln_experiment.notebook_experiment_id,
                                            material_tracking_entry.entry_id, criteria)
        return SapioWebhookResult(True, eln_entry_refresh_list=entries)
