import csv
import logging
import traceback
from io import StringIO
from typing import List, Dict, Any, Final

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNSampleDetailModel, AttachmentModel
from utilities.webhook_handler import OsloWebhookHandler


class GenerateNormalizationSheet(OsloWebhookHandler):
    FIELD_MAPPINGS: Final[Dict[str, str]] = {
        "Project": "ProjectID",
        "Sample": "OtherSampleId",
        "Prep well": "Prepwellposition",
        "Dest. cont.": "DestinationContainer",
        "Dest. well": "DestinationWell",
        "Input molarity": "InputMolarity",
        "Input volume": "InputVolume",
        "Normalised molarity": "NormalizedMolaritynM",
        "Buffer volume": "BufferVolume"
    }

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        file_name = f"NormalizationSheet_{context.eln_experiment.notebook_experiment_id}.csv"
        try:
            # Get Normalization Details
            normalization_step = self.exp_handler.get_step("Normalization")
            sample_details = self.exp_handler.get_step_models(normalization_step, ELNSampleDetailModel)
            if not sample_details:
                return SapioWebhookResult(True)

            # Generate content
            csv_content = self._generate_csv_content(sample_details)

            # Generate file
            callback_util = CallbackUtil(context)
            callback_util.write_file(file_name, csv_content)

            # Add the attachment to the entry.
            attachment_step = self.exp_handler.get_step("Generated Normalization Worksheet")
            attachment: AttachmentModel = (
                AttachmentUtil.create_attachment(self.context, file_name, csv_content, AttachmentModel))
            crit: ElnAttachmentEntryUpdateCriteria = ElnAttachmentEntryUpdateCriteria()
            crit.record_id = attachment.record_id
            crit.attachment_name = attachment.get_FilePath_field()
            crit.collapse_entry = False
            crit.template_item_fulfilled_timestamp = TimeUtil.now_in_millis()
            context.eln_manager.update_experiment_entry(attachment_step.protocol.get_id(),
                                                        attachment_step.get_id(), crit)
            return SapioWebhookResult(True, display_text=f"{file_name} has been generated.")
        except Exception as e:
            self.logger.error(traceback.format_exc())
            self.callback.ok_dialog("Error", str(e))
            return SapioWebhookResult(False)

    def _generate_csv_content(self, sample_details: List[ELNSampleDetailModel]) -> bytes:
        """Generate CSV content from the provided records."""
        output = StringIO()
        writer = csv.DictWriter(output, fieldnames=self.FIELD_MAPPINGS.keys())
        writer.writeheader()

        valid_records = 0
        for sample_detail in sample_details:
            row_data = self._extract_record_data(sample_detail)
            if row_data:
                writer.writerow(row_data)
                valid_records += 1

        logging.info(f"Generated CSV content with {valid_records} valid records.")
        return output.getvalue().encode('utf-8')

    def _extract_record_data(self, sample_detail: ELNSampleDetailModel) -> Dict[str, Any]:
        row_data = {}
        for header, record_field in self.FIELD_MAPPINGS.items():
            try:
                value = sample_detail.get_field_value(record_field)
                if value:
                    if isinstance(value, float):
                        value = round(value, 2)
                    row_data[header] = value
            except Exception as e:
                raise Exception(
                    f"Unexpected error processing field {record_field} in record {sample_detail.get_SampleId_field()}: {str(e)}")
        return row_data
