import traceback

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil

from entry_toolbar.populate_ddPCR_plate import get_sorted_records_by_field
from utilities.data_models import SampleModel, QCDatumModel, ELNSampleDetailModel
from utilities.webhook_handler import OsloWebhookHandler


class SetNormalizationTableFields(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)

        try:
            # Get Samples
            samples_step = self.exp_handler.get_step("Samples")
            samples = self.exp_handler.get_step_models(samples_step, SampleModel)
            if not samples:
                raise Exception("Sample records not found in the entry named 'Samples'")

            # Get Normalization Details
            normalization_step = self.exp_handler.get_step("Normalization")
            sample_details = self.exp_handler.get_step_models(normalization_step, ELNSampleDetailModel)
            if not sample_details:
                return SapioWebhookResult(True)
            sample_id_by_sample_detail = RecordModelUtil.map_model_by_field_value(
                self.inst_man.unwrap_list(sample_details), "SampleId")

            # Load Tapestation results
            self.rel_man.load_children_of_type(samples, QCDatumModel)

            # Sort samples by well position
            samples = get_sorted_records_by_field(samples, SampleModel.COLPOSITION__FIELD_NAME.field_name)
            samples = get_sorted_records_by_field(samples, SampleModel.ROWPOSITION__FIELD_NAME.field_name)

            # Get values for destination wells
            wells = well_label_generator()

            errors: list[str] = list()
            for sample in samples:
                dest_well = next(wells)

                sample_detail = sample_id_by_sample_detail[sample.get_SampleId_field()]
                sample_detail.set_field_value("DestinationWell", dest_well)

                qc_datum_models: list[QCDatumModel] = sample.get_children_of_type(QCDatumModel)
                if not qc_datum_models:
                    errors.append(sample.get_SampleId_field())
                    continue
                latest_result = max(qc_datum_models, key=lambda rec: rec.get_DateCreated_field())
                molarity = latest_result.get_Molarity_field()
                if molarity:
                    sample_detail.set_field_value("InputMolarity", molarity)
            self.rec_man.store_and_commit()
            if errors:
                self.callback.display_error(
                    f"The following sample(s) don't have Tapestation QC results.\n  {str(errors)}")
        except Exception as exception:
            self.logger.error(traceback.format_exc())
            self.callback.ok_dialog("Error", str(exception))
        return SapioWebhookResult(True)


def well_label_generator():
    rows = "ABCDEFGH"
    columns = range(1, 13)
    for row in rows:
        for col in columns:
            yield f"{row}{col}"
