from typing import List

from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult

from utilities.data_models import SampleModel, ELNSampleDetailModel, QCDatumModel, TapeStationResultModel
from utilities.webhook_handler import OsloWebhookHandler


class CalculateMolarity(OsloWebhookHandler):
    FRAGMENT_SIZE_FIELD: str = 'FragmentSizebp'

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        """
        Calculate and update fragment sizes for sample details.
        """
        try:
            sample_details = self.exp_handler.get_step_models("Calculate Molarity", ELNSampleDetailModel)
            if not sample_details:
                return SapioWebhookResult(True, "No sample details found in 'Calculate Molarity' step to process.")

            samples = self._get_samples_from_details(sample_details)
            if not samples:
                return SapioWebhookResult(True, "No samples found to process.")

            self._load_qc_data(samples)
            fragment_sizes = self._calculate_fragment_sizes(samples)
            # Update fragment sizes in sample details."""
            sample_to_detail = {detail.get_parent_of_type(SampleModel).get_SampleId_field(): detail for detail in sample_details if
                                detail.get_parent_of_type(SampleModel)}
            for sample_id, sizes in fragment_sizes.items():
                if sizes:
                    avg_size = sum(sizes) / len(sizes)
                    if detail := sample_to_detail.get(sample_id):
                        detail.set_field_value(self.FRAGMENT_SIZE_FIELD, avg_size)
            self.rec_man.store_and_commit()
            return SapioWebhookResult(True, f"Processed molarity calculation {len(fragment_sizes)} samples.")
        except Exception as e:
            self.log_to_system(f"Error calculating fragment size: {str(e)}")
            self.callback.ok_dialog("Error", f"Error calculating fragment size: {str(e)}")
            return SapioWebhookResult(False)

    def _get_samples_from_details(self, sample_details: List[ELNSampleDetailModel]) -> List[SampleModel]:
        """Get parent samples from sample details."""
        self.rel_man.load_parents_of_type(sample_details, SampleModel)
        return [detail.get_parent_of_type(SampleModel) for detail in sample_details if detail.get_parent_of_type(SampleModel)]

    def _load_qc_data(self, samples: List[SampleModel]) -> None:
        """Load QC data and TapeStation results for samples."""
        self.rel_man.load_children_of_type(samples, QCDatumModel)
        qc_data = [qc for sample in samples for qc in sample.get_children_of_type(QCDatumModel)]
        self.rel_man.load_children_of_type(qc_data, TapeStationResultModel)

    def _calculate_fragment_sizes(self, samples: List[SampleModel]) -> dict[str | None, list[float]]:
        """Calculate fragment sizes for all samples."""
        return {sample.get_SampleId_field(): self._get_sample_bp_size(sample) for sample in samples if self._get_sample_bp_size(sample)}

    def _get_sample_bp_size(self, sample: SampleModel) -> List[float]:
        """Get fragment sizes for a single sample, handling pooled and individual samples."""
        return (self._get_pooled_sample_sizes(sample)
                if sample.get_IsPooled_field()
                else [self._get_individual_sample_sizes(sample)])

    def _get_individual_sample_sizes(self, sample: SampleModel) -> float | None:
        """Get fragment sizes for an individual sample."""
        self.rel_man.load_children_of_type([sample], QCDatumModel)
        qc_records = sample.get_children_of_type(QCDatumModel)
        if qc_records:
            latest_qc_datum = max(qc_records, key=lambda x: x.get_DateCreated_field())

            if latest_qc_datum is not None:
                self.rel_man.load_children_of_type([latest_qc_datum], TapeStationResultModel)
                tapestation_results = latest_qc_datum.get_children_of_type(TapeStationResultModel)
                if tapestation_results:
                    return tapestation_results[0].get_AvgBPSize_field()

    def _get_pooled_sample_sizes(self, pooled_sample: SampleModel) -> List[float]:
        """Get fragment sizes for a pooled sample by aggregating its child samples."""
        self.rel_man.load_parents_of_type([pooled_sample], SampleModel)
        parent_samples = pooled_sample.get_parents_of_type(SampleModel)
        sizes =[]
        for child_sample in parent_samples:
            size = self._get_individual_sample_sizes(child_sample)
            if size:
                sizes.append(size)
        return sizes
