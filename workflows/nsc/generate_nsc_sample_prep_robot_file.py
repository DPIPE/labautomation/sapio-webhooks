from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from entry_toolbar.add_samples_to_plate_ngs_snp_id import ElnEntryStep
from utilities.data_models import SampleModel, IndexBarcodeModel, ProjectModel
from utilities.oslo_exception import OsloException
from utilities.sample_utils import SampleUtils
from workflows.nsc.generate_nsc_file_base import GenerateNSCFileBase


ENTRY_TAG: str = "GENERATED ROBOT FILE"
SAMPLES_ENTRY_TAG: str = "NSC SAMPLES"

"""
Illumina DNA Sample Prep *
Thruplex *
TruSeq Stranded mRNA Prep *
miRNA Sample Prep *
TruSeq Nano Sample Prep *


"""


class GenerateNSCSamplePrepRobotFile(GenerateNSCFileBase):
    def set_attachment_step(self) -> None:
        try:
            self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{ENTRY_TAG}\" was found in the experiment.")

    def generate_file_data(self) -> (str, bytes):
        # Get the samples from the samples entry.
        try:
            samples_step: ElnEntryStep = self.exp_handler.get_step_by_option(SAMPLES_ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{SAMPLES_ENTRY_TAG}\" was found in the experiment.")
        samples: list[SampleModel] | None = self.exp_handler.get_step_models(samples_step, SampleModel)
        if not samples:
            raise OsloException(f"No samples have been added to the {samples_step.get_name()} entry.")

        # Load the related records needed for the file generation.
        self.rel_man.load_path_of_type(samples, RelationshipPath().parent_type(SampleModel).parent_type(ProjectModel))
        parent_samples: list[SampleModel] = [x.get_parent_of_type(SampleModel) for x in samples]
        self.rel_man.load_children_of_type(parent_samples, IndexBarcodeModel)

        # Write the file header data.
        file_data: str = (f"Version 2\n"
                          f"ID\t{TimeUtil.now_in_format('%Y-%m-%d')}\n"
                          f"Assay\t{self.exp_handler.get_template_name()}\n"
                          f"IndexReads\t2\n"
                          f"IndexCycles\t8\n")

        # Sort the samples so that they'll appear in the file in order, and write their data to the file.
        sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_well_positions(samples)
        for sample in sorted_samples:
            row_col: str = SampleUtils.get_row_col_position_str(sample, True)
            file_data += f"{row_col} "
            try:
                file_data += (sample.get_parent_of_type(SampleModel).get_parent_of_type(ProjectModel)
                              .get_ProjectName_field())
            except Exception:
                file_data += ""
            # TODO: Figure out how to get the index tag populated from the sample manifest uploaded. Use the index ID for now for the demo.
            try:
                # If there are for some reason multiple index barcode children, just use the first one.
                # TODO: See if this is feasible or not outside of the S20 demo.
                index: IndexBarcodeModel = (
                    sample.get_parent_of_type(SampleModel).get_children_of_type(IndexBarcodeModel))[0]
                file_data += f".{index.get_OtherSampleId_field()}\t{index.get_IndexId_field()}\n"
            except Exception:
                file_data += f".{sample.get_parent_of_type(SampleModel).get_OtherSampleId_field()}\t\n"
        file_data += "[AssaySettings]\n"

        # Return the file extension and the bytes of the file data.
        return "txt", file_data.encode()
