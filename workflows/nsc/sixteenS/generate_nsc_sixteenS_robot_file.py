import io

from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath
from xlwt import Workbook, Worksheet

from utilities.data_models import ELNSampleDetailModel, SampleModel, PrimerModel, PrimerDesignReportModel, \
    IndexBarcodeModel
from utilities.oslo_exception import OsloException
from utilities.sample_utils import SampleUtils
from workflows.nsc.generate_nsc_file_base import GenerateNSCFileBase


ENTRY_TAG: str = "GENERATED ROBOT FILE"
SAMPLES_ENTRY_TAG: str = "LIBRARY PREP SAMPLES"

HEADERS: list[str] = ["Sample Name", "Plate Number", "Sample Position", "Stock Conc", "Sample Input", "Buffer Volume",
                      "Forward Primer", "Reverse Primer"]


class GenerateNSCSixteenSRobotFile(GenerateNSCFileBase):
    def set_attachment_step(self) -> None:
        try:
            self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{ENTRY_TAG}\" was found in the experiment.")

    def generate_file_data(self) -> (str, bytes):
        try:
            # Get the samples and map them to their corresponding sample details.
            try:
                samples_step: ElnEntryStep = self.exp_handler.get_step_by_option(SAMPLES_ENTRY_TAG)
            except Exception:
                raise OsloException(f"No entry with tag \"{SAMPLES_ENTRY_TAG}\" was found in the experiment.")
            samples: list[SampleModel] | None = self.exp_handler.get_step_models(samples_step, SampleModel)
            if not samples:
                raise OsloException(f"No samples have been added to the {samples_step.get_name()} entry.")
            samples_to_details: dict[SampleModel, ELNSampleDetailModel] = self.map_samples_to_sample_details(samples)

            # Load the indexes of the parent samples to populate the primer data in the sheet.
            self.rel_man.load_path_of_type(samples, RelationshipPath().parent_type(SampleModel)
                                           .child_type(IndexBarcodeModel))

            # Start the workbook for the xls file.
            workbook: Workbook = Workbook()
            sheet: Worksheet = workbook.add_sheet("Sheet1")

            # Write the headers of the file.
            for x, header in enumerate(HEADERS):
                sheet.write(0, x, header)

            # Write data for each of the samples, sorting the samples by sample name.
            sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(samples)
            for x, sample in enumerate(sorted_samples, start=1):
                sample_detail: ELNSampleDetailModel = samples_to_details[sample]
                sheet.write(x, 0, sample.get_OtherSampleId_field())
                sheet.write(x, 1, sample.get_StorageLocationBarcode_field())
                sheet.write(x, 2, SampleUtils.get_row_col_position_str(sample))
                sheet.write(x, 3, sample_detail.get_field_value("NSCTargetConcentration"))
                sheet.write(x, 4, sample_detail.get_field_value("InputVolume"))
                sheet.write(x, 5, sample_detail.get_field_value("NSCBufferVolume"))

                # Write the primer data if it exists.
                try:
                    index_barcode: IndexBarcodeModel = (sample.get_parent_of_type(SampleModel)
                                                        .get_child_of_type(IndexBarcodeModel))
                    if index_barcode and index_barcode.get_IndexId_field():
                        parts: list[str] = index_barcode.get_IndexId_field().split("-")
                        sheet.write(x, 6, parts[0])
                        sheet.write(x, 7, parts[1])
                except Exception:
                    continue

            # Save the file data to the buffer and return it along with the file extension.
            buffer: io.BytesIO = io.BytesIO()
            workbook.save(buffer)
            return "xls", buffer.getvalue()
        except Exception as e:
            self.exp_handler.uninitialize_step(self.active_step)
            raise e
