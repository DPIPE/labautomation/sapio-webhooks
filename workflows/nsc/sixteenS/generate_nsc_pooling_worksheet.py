import io

from sapiopylib.rest.utils.Protocols import ElnEntryStep
from xlwt import Workbook, Worksheet, easyxf, XFStyle

from utilities.data_models import ELNSampleDetailModel, SampleModel
from utilities.oslo_exception import OsloException
from utilities.sample_utils import SampleUtils
from workflows.nsc.generate_nsc_file_base import GenerateNSCFileBase

ENTRY_TAG: str = "GENERATED POOLING WORKSHEET"
SAMPLE_DETAILS_ENTRY_TAG: str = "POOLING DETAILS"
HEADERS: list[str] = ["Pool Name", "Dest Cont", "Pool Well", "Pool Molarity", "Pool Volume", "Buffer Volume", "Sample",
                      "Source Cont", "Source Well", "Sample Molarity", "Sample Volume"]


class GenerateNSCPoolingWorksheet(GenerateNSCFileBase):
    def set_attachment_step(self) -> None:
        try:
            self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{ENTRY_TAG}\" was found in the experiment.")

    def generate_file_data(self) -> (str, bytes):
        def _sample_details_sort(sample_detail_: ELNSampleDetailModel) -> (str, int):
            source_position = sample_detail_.get_field_value("SourcePosition")
            if source_position:
                row: str = source_position[0:1]
                column: int = int(source_position[1:])
                return row, column
            return "Z", 0

        # Grab the data from the sample details entry as well as the parent samples.
        try:
            sample_details_step: ElnEntryStep = self.exp_handler.get_step_by_option(SAMPLE_DETAILS_ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{SAMPLE_DETAILS_ENTRY_TAG}\" was found in the experiment.")
        sample_details: list[ELNSampleDetailModel] | None = self.exp_handler.get_step_models(sample_details_step,
                                                                                             ELNSampleDetailModel)
        if not sample_details:
            raise OsloException(f"No sample details have been added to \"{sample_details_step.eln_entry.entry_name}\" "
                                f"entry.")
        self.rel_man.load_parents_of_type(sample_details, SampleModel)
        samples_to_details: dict[ELNSampleDetailModel, SampleModel] = self.rec_handler.map_to_parent(sample_details,
                                                                                                     SampleModel)

        # Generate a workbook.
        workbook: Workbook = Workbook()
        sheet: Worksheet = workbook.add_sheet("Sheet1")

        # Write the headers of the file.
        header_style: XFStyle = easyxf("pattern: pattern solid, fore_colour yellow;")
        for x in range(len(HEADERS)):
            sheet.write(0, x, HEADERS[x], header_style)

        # Map the pools to their corresponding sorted sample details.
        pools: list[SampleModel] | None = self.exp_handler.get_step_models(self.active_step, SampleModel)
        if not pools:
            raise OsloException(f"No pools are present in this entry.")
        pools_to_details: dict[SampleModel, list[ELNSampleDetailModel]] = {}
        for pool in pools:
            pool_details: list[ELNSampleDetailModel] = \
                [sample_detail for sample_detail in sample_details
                 if sample_detail.get_field_value("PoolName") == pool.get_OtherSampleId_field()]
            pools_to_details[pool] = sorted(pool_details, key=_sample_details_sort)

        # Write data to the file.
        row_index: int = 1
        for pool in pools:
            sheet.write(row_index, 0, pool.get_OtherSampleId_field())
            sheet.write(row_index, 1, pool.get_StorageLocationBarcode_field())
            sheet.write(row_index, 2, SampleUtils.get_row_col_position_str(pool))
            sheet.write(row_index, 3, pool.get_Molarity_field())
            sheet.write(row_index, 4, pool.get_Volume_field())
            sheet.write(row_index, 5, pools_to_details[pool][0].get_field_value("PoolBufferVolume"))

            if pools_to_details[pool]:
                for sample_detail in pools_to_details[pool]:
                    sheet.write(row_index, 6, sample_detail.get_OtherSampleId_field() if sample_detail.get_OtherSampleId_field() else "")
                    sheet.write(row_index, 7, sample_detail.get_field_value("SourcePlate") if sample_detail.get_field_value("SourcePlate") else "")
                    sheet.write(row_index, 8, SampleUtils.get_row_col_position_str(samples_to_details[sample_detail]))
                    sheet.write(row_index, 9, sample_detail.get_field_value("SourceSampleMolarity"))
                    sheet.write(row_index, 10, sample_detail.get_field_value("SampleVolumeForPool2"))
                    row_index += 1
            else:
                row_index += 1

        # Write the file data to a buffer and return the bytes and extension.
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)
        return "xls", buffer.getvalue()
