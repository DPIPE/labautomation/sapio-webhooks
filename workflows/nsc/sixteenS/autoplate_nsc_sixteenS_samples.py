from typing import cast, Any

from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.constants import Constants
from utilities.data_models import SampleModel, ConsumableItemModel, PlateDesignerWellElementModel, PlateModel, \
    AssignedProcessModel
from utilities.oslo_exception import OsloException
from utilities.plating_utils import PlatingUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


PLATE_SAMPLES_ENTRY_TAG: str = "PLATE SAMPLES"
REAGENT_TRACKING_ENTRY_TAG: str = "REAGENT TRACKING"


class AutoplateNSCSixteenSSamples(OsloWebhookHandler):
    acc_man: AccessionManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.acc_man = DataMgmtServer.get_accession_manager(self.user)
        plate_setup_entry: ElnEntryStep = cast(ElnEntryStep, context.active_step)

        try:
            # Grab the samples to be plated.
            try:
                samples_step = self.exp_handler.get_step_by_option(PLATE_SAMPLES_ENTRY_TAG)
            except Exception:
                raise OsloException(f"No entry with tag \"{PLATE_SAMPLES_ENTRY_TAG}\" was found in the "
                                    f"experiment.")
            samples: list[SampleModel] = self.exp_handler.get_step_models(samples_step, SampleModel)
            if not samples:
                raise OsloException("No samples have been added to the Quant-iT plate.")

            # Get the control samples.
            control_samples: list[SampleModel] | None = self._get_controls()
            if not control_samples:
                control_samples = []

            # Depending on the total number of samples + controls, we may need to make two plates. If there's more than
            # two plates needing to be made, throw an error to the user.
            total_samples = len(samples) + len(control_samples)
            if total_samples > 194:
                raise OsloException(
                    "The total number of samples and controls is greater than 194 and two plates will not"
                    " be enough to plate them all. Please contact the lab manager.")

            # Split the list of samples into 96 each and create the plates.
            assigned_process: AssignedProcessModel | None = SampleUtils.get_current_process(samples[0], self.rel_man)
            if not assigned_process:
                process_name: str = "NSC"
            else:
                process_name: str = assigned_process.get_ProcessName_field()
            sample_lists: list[list[SampleModel]] = [samples[i:i + 96] for i in range(0, len(samples), 96)]
            plates: list[PlateModel] = []
            for x in range(len(sample_lists)):
                plates.append(PlatingUtils.generate_plate_in_experiment(self.rec_handler, self.acc_man, process_name))

            self.rec_man.store_and_commit()

            for x, sample_list in enumerate(sample_lists):
                self._plate_samples(sample_list, control_samples, plates[x])

            # Add the plates to the plate setup entry.
            options: dict[str, Any] = plate_setup_entry.get_options()
            options.setdefault("MultiLayerPlating_Plate_RecordIdList", ",".join([str(x.record_id) for x in plates]))
            plate_setup_entry.set_options(options)

            self.rec_man.store_and_commit()
        except Exception as e:
            self.exp_handler.uninitialize_step(plate_setup_entry)
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
            else:
                raise e

        return SapioWebhookResult(True)

    def _get_controls(self) -> list[SampleModel] | None:
        # Grab the controls from the reagent tracking entries. If there are no controls or more than two, raise an
        # error to the user.
        try:
            reagent_tracking_entry: ElnEntryStep = self.exp_handler.get_step_by_option(REAGENT_TRACKING_ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{REAGENT_TRACKING_ENTRY_TAG}\" was found in the experiment.")
        controls: list[ConsumableItemModel] | None = (
            WorkflowUtils.get_reagents_from_reagent_tracking_entry(self.user, self.rec_handler, self.inst_man,
                                                                   self.exp_handler, reagent_tracking_entry))
        if not controls:
            return None
        self.rel_man.load_parents_of_type(controls, SampleModel)
        control_samples: list[SampleModel] = \
            [x.get_parent_of_type(SampleModel) for x in controls if x.get_parent_of_type(SampleModel)]
        if not control_samples or len(control_samples) != 2:
            raise OsloException("One positive and one negative control are required for the plate(s).")
        return control_samples

    def _plate_samples(self, samples: list[SampleModel], controls: list[SampleModel], plate: PlateModel) -> None:
        def _plate_sample(sample_: SampleModel, plate_: PlateModel, well_location_: str,
                          plate_well_designer_element_: PlateDesignerWellElementModel) -> None:
            plate_well_designer_element_.set_PlateRecordId_field(plate_.record_id)
            plate_well_designer_element_.set_Layer_field(1)
            plate_well_designer_element_.set_RowPosition_field(well_location_[0:1])
            plate_well_designer_element_.set_ColPosition_field(well_location_[1:len(well_location_)])
            plate_well_designer_element_.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
            plate_well_designer_element_.set_SourceRecordId_field(sample_.record_id)

        # Create the plate well designer elements for all the samples and controls.
        plate_well_designer_elements: list[PlateDesignerWellElementModel] = (
            self.rec_handler.add_models(PlateDesignerWellElementModel, len(samples) + len(controls)))

        # Sort the samples by their sample names and plate them.
        sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(samples)
        curr_well_index: int = 0
        for sample in sorted_samples:
            well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
            _plate_sample(sample, plate, well_location, plate_well_designer_elements[curr_well_index])
            curr_well_index += 1

        # Plate the controls immediately after the samples. Positive controls will be plated first.
        if controls:
            positive_controls: list[SampleModel] = [x for x in controls if x.get_ControlType_field() != "Negative"]
            if positive_controls:
                negative_controls: list[SampleModel] = list(set(controls) - set(positive_controls))
            else:
                negative_controls = controls

            sorted_positive_controls: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(positive_controls)
            for control in sorted_positive_controls:
                well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
                _plate_sample(control, plate, well_location, plate_well_designer_elements[curr_well_index])
                curr_well_index += 1

            sorted_negative_controls: list[SampleModel] = SampleUtils.sort_samples_by_other_sample_id(negative_controls)
            for control in sorted_negative_controls:
                well_location: str = Constants.PLATE_LOCATIONS_LEADING_ZEROES[curr_well_index]
                _plate_sample(control, plate, well_location, plate_well_designer_elements[curr_well_index])
                curr_well_index += 1
