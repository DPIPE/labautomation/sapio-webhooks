from utilities.data_models import SampleModel
from utilities.oslo_exception import OsloException
from workflows.nsc.generate_nsc_file_base import GenerateNSCFileBase


ENTRY_TAG: str = "GENERATED QUANT-IT WORKSHEET"
ENTRY_2_TAG: str = "GENERATED QUANT-IT WORKSHEET TWO"
LIBRARY_PREP_SAMPLES_ENTRY_TAG: str = "LIBRARY PREP SAMPLES"


class GenerateNSCQuantITWorksheet(GenerateNSCFileBase):
    def set_attachment_step(self) -> None:
        if LIBRARY_PREP_SAMPLES_ENTRY_TAG in self.active_step.get_options().keys():
            try:
                self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_2_TAG)
            except Exception:
                raise OsloException(f"No entry with a tag of \"{ENTRY_2_TAG}\" was found in the experiment.")
        else:
            try:
                self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_TAG)
            except Exception:
                raise OsloException(f"No entry with a tag of \"{ENTRY_TAG}\" was found in the experiment.")

    def generate_file_data(self) -> (str, bytes):
        quant_it_samples: list[SampleModel] | None = self.exp_handler.get_step_models(self.active_step, SampleModel)
        if not quant_it_samples:
            raise OsloException(f"No samples are present in the \"{self.active_step.get_name()}\" entry.")

        # Write the worksheet data.
        num_reactions: int = len(quant_it_samples) + 8 + 6
        file_data: str = (f"Number of reactions: {num_reactions}\nQuant-iT reagent: {num_reactions}\n"
                          f"Quant-iT buffer: {199 * num_reactions}\n")
        return "csv", file_data.encode()
