from typing import cast

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import WebhookEndpointType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel, ELNSampleDetailModel
from utilities.oslo_exception import OsloException
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


POOLING_DETAILS_ENTRY_TAG: str = "POOLING DETAILS"


class CopyPoolingFields(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            if context.end_point_type == WebhookEndpointType.VELOXELNRULEACTION:
                pools: list[SampleModel] = self.rule_handler.get_models(SampleModel)
                sample_details: list[ELNSampleDetailModel] = self.rule_handler.get_models(ELNSampleDetailModel)
            else:
                pools: list[SampleModel] = self.exp_handler.get_step_models(cast(ElnEntryStep, context.active_step),
                                                                            SampleModel)
                try:
                    sample_details_step: ElnEntryStep = self.exp_handler.get_step_by_option(POOLING_DETAILS_ENTRY_TAG)
                    sample_details: list[ELNSampleDetailModel] = self.exp_handler.get_step_models(sample_details_step,
                                                                                                  ELNSampleDetailModel)
                except Exception:
                    raise OsloException(f"No entry with tag \"{POOLING_DETAILS_ENTRY_TAG}\" was found in the "
                                        f"experiment.")

            # Copy the pooling fields from the sample details down to the pools.
            for pool in pools:
                pos: str = SampleUtils.get_row_col_position_str(pool)
                pool_location: str = pool.get_StorageLocationBarcode_field() + " " + pos
                try:
                    sample_detail: ELNSampleDetailModel = \
                        [x for x in sample_details
                         if x.get_field_value("TargetPlate") + " " +
                         x.get_field_value("TargetPool") == pool_location][0]
                except Exception:
                    raise OsloException(f"No sample detail found for pool \"{pool.get_PoolName_field()}\" at location "
                                        f"\"{pool_location}\"")

                pool.set_OtherSampleId_field(sample_detail.get_field_value("PoolName"))
                pool.set_Volume_field(sample_detail.get_field_value("PoolVolume"))
                pool.set_Molarity_field(sample_detail.get_field_value("PoolMolarity"))

            self.rec_man.store_and_commit()

        except Exception as e:
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            raise e

        return SapioWebhookResult(True)
