from typing import Any, cast

from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxDoubleFieldDefinition, AbstractVeloxFieldDefinition, \
    VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import ELNSampleDetailModel, SampleModel
from utilities.oslo_exception import OsloException
from utilities.webhook_handler import OsloWebhookHandler


class PoolingDialog(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        active_step: ElnEntryStep = cast(ElnEntryStep, context.active_step)
        try:
            # Get the sample detail records.
            sample_details: list[ELNSampleDetailModel] | None = self.exp_handler.get_step_models(active_step,
                                                                                                 ELNSampleDetailModel)
            if not sample_details:
                raise OsloException(f"No samples are present in this entry.")
            for sample_detail in sample_details:
                if not sample_detail.get_field_value("SourceSampleMolarity"):
                    raise OsloException("All samples must have a molarity to be pooled.")

            # Grab all the "pools".
            target_pools: list[str] = list(set([sample_detail.get_field_value("TargetPlate") + " " +
                                                sample_detail.get_field_value("TargetPool") for sample_detail in
                                                sample_details]))

            # Prompt the user to enter the volume and molarity for each of the pools.
            pool_details: list[dict[str, Any]] | None = self._prompt_vol_mol(target_pools)
            if not pool_details:
                return SapioWebhookResult(True)

            # Set the sample detail values to the values entered by the user and do pooling calculations.
            self._set_pooling_details_fields(sample_details, pool_details)

            self.rec_man.store_and_commit()

        except Exception as e:
            self.exp_handler.uninitialize_step(active_step)
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            raise e

        return SapioWebhookResult(True)

    def _prompt_vol_mol(self, target_pools: list[str]) -> list[dict[str, Any]] | None:
        # Create fields for the form to display
        fields: list[AbstractVeloxFieldDefinition] = []

        target_pool_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("PoolingTable", "target_pool",
                                                                                   "Target Pool")
        fields.append(target_pool_field)

        pool_name_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("PoolingTable", "pool_name",
                                                                                 "Pool Name")
        pool_name_field.editable = True
        pool_name_field.required = True
        fields.append(pool_name_field)

        pool_volume_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("PoolingTable", "pool_volume",
                                                                                   "Pool Volume")
        pool_volume_field.editable = True
        pool_volume_field.required = True
        fields.append(pool_volume_field)

        pool_molarity_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("PoolingTable", "pool_molarity",
                                                                                     "Pool Molarity")
        pool_molarity_field.editable = True
        pool_molarity_field.required = True
        fields.append(pool_molarity_field)

        # Create a list of default values for the table dialog.
        values: list[dict[str, Any]] = [{
            "target_pool": pool_name,
            "pool_name": "",
            "pool_volume": 0.0,
            "pool_molarity": 0.0
        } for pool_name in target_pools]

        return self.callback.table_dialog("Pooling", f"Enter pooling details.", fields, values,
                                          data_type="PoolingTable")

    def _set_pooling_details_fields(self, sample_details: list[ELNSampleDetailModel],
                                    pooling_details: list[dict[str, Any]]) -> None:
        # Set the fields on the sample details according to the values entered by the user.
        for pooling_detail in pooling_details:
            matching_sample_details: list[ELNSampleDetailModel] = \
                [sample_detail for sample_detail in sample_details
                 if sample_detail.get_field_value("TargetPlate") + " " + sample_detail.get_field_value("TargetPool")
                 == pooling_detail["target_pool"]]
            if not matching_sample_details:
                continue

            for sample_detail in matching_sample_details:
                sample_detail.set_field_value("PoolName", pooling_detail["pool_name"])
                sample_detail.set_field_value("PoolVolume", pooling_detail["pool_volume"])
                sample_detail.set_field_value("PoolMolarity", pooling_detail["pool_molarity"])

                # Calculate the sample volume for the pool if the sample has a molarity.
                molarity: float | None = sample_detail.get_field_value("SourceSampleMolarity")
                if not molarity:
                    continue
                sample_volume: float = (((pooling_detail["pool_volume"] * pooling_detail["pool_molarity"]) /
                                         len(matching_sample_details)) / molarity)
                sample_detail.set_field_value("SampleVolumeForPool2", sample_volume)

            # Calculate the buffer volumes for each of the pools.
            total_sample_volumes_for_pool: float = sum([(sample_detail.get_field_value(
                "SampleVolumeForPool2") if sample_detail.get_field_value("SampleVolumeForPool2") else 0) for
                                                        sample_detail in matching_sample_details])

            for sample_detail in matching_sample_details:
                sample_detail.set_field_value("PoolBufferVolume", pooling_detail["pool_volume"] -
                                              total_sample_volumes_for_pool)
