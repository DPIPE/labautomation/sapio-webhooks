from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import ELNSampleDetailModel, QCDatumModel, SampleModel
from utilities.oslo_exception import OsloException
from utilities.webhook_handler import OsloWebhookHandler


PLATE_SAMPLES_ENTRY_TAG: str = "PLATE SAMPLES"


class CopyQuantITConcentrations(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            sample_details: list[ELNSampleDetailModel] = self.rule_handler.get_models(ELNSampleDetailModel)
            qc_datums: list[QCDatumModel] = self.rule_handler.get_models(QCDatumModel)
            lib_prep_samples: list[SampleModel] = self.rule_handler.get_models(SampleModel)

            # Get the parent samples from the entry.
            try:
                parent_samples_entry: ElnEntryStep = self.exp_handler.get_step_by_option(PLATE_SAMPLES_ENTRY_TAG)
            except Exception:
                raise OsloException(f"No entry with the \"{PLATE_SAMPLES_ENTRY_TAG}\" tag was found in the experiment.")
            parent_samples: list[SampleModel] | None = self.exp_handler.get_step_models(parent_samples_entry,
                                                                                        SampleModel)
            if not parent_samples:
                raise OsloException("This experiment has no parent samples.")

            # Load the parents of the lib prep samples and copy the qc concentration readings to their child lib prep
            # sample detail record.
            self.rel_man.load_path_of_type(qc_datums,
                                           RelationshipPath().parent_type(SampleModel).parent_type(SampleModel))
            parent_samples_to_qc_datums: dict[SampleModel, QCDatumModel] = {}
            for qc_datum in qc_datums:
                try:
                    parent_sample: SampleModel = \
                        [x for x in parent_samples
                         if x in qc_datum.get_parent_of_type(SampleModel).get_parents_of_type(SampleModel)][0]
                    parent_samples_to_qc_datums[parent_sample] = qc_datum
                except Exception:
                    continue

            self.rel_man.load_path_of_type(lib_prep_samples,
                                           RelationshipPath().parent_type(SampleModel).child_type(QCDatumModel))
            for lib_prep_sample in lib_prep_samples:
                try:
                    parent_sample: SampleModel = \
                        [x for x in parent_samples if x in lib_prep_sample.get_parents_of_type(SampleModel)][0]
                    qc_datum: QCDatumModel = parent_samples_to_qc_datums[parent_sample]
                    sample_detail: ELNSampleDetailModel = \
                        [x for x in sample_details if x.get_SampleId_field() == lib_prep_sample.get_SampleId_field()][0]
                    sample_detail.set_field_value("NSCConcentration", qc_datum.get_Concentration_field())
                except Exception:
                    continue

            self.rec_man.store_and_commit()

        except Exception as e:
            if isinstance(e, OsloException):
                try:
                    self.callback.ok_dialog("Error", str(e))
                except Exception:
                    ...
            else:
                raise e

        return SapioWebhookResult(True)
