# from decimal import Decimal, ROUND_HALF_UP
# from enum import Enum
# from typing import List, Dict, Optional, Union, cast
#
# from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnEntryCriteria
# from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnEntryType, ElnBaseDataType
# from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
# from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
# from sapiopylib.rest.utils.ProtocolUtils import ELNStepFactory
# from sapiopylib.rest.utils.Protocols import ElnEntryStep, ElnExperimentProtocol
#
# from utilities.data_models import SampleModel, ELNSampleDetailModel
# from utilities.webhook_handler import OsloWebhookHandler
#
#
# class PoolingType(Enum):
#     EVEN = "even"
#     UNEVEN = "uneven"
#
#
# class DynamicPoolingEntries(OsloWebhookHandler):
#     DECIMAL_PLACES = 2
#     MIN_PERCENTAGE = Decimal('0.01')
#     MAX_PERCENTAGE = Decimal('100.00')
#
#     def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
#         try:
#             # Get Samples
#             samples_step = self.exp_handler.get_step("Samples")
#             samples = self.exp_handler.get_step_models(samples_step, SampleModel)
#             if not samples:
#                 raise Exception("Sample records not found in the entry named 'Samples'")
#             pooling_type = self.get_pooling_type()
#             pool_molarity, pool_volume = self.get_pool_parameters()
#
#             if pooling_type == PoolingType.EVEN:
#                 pool_records = self.calculate_even_pooling(samples, pool_molarity, pool_volume)
#             else:
#                 pool_records = self.calculate_uneven_pooling(samples, pool_molarity, pool_volume)
#
#             # if not self.check_indexes(samples):
#             #     return SapioWebhookResult(False, "Index conflict detected. Please review sample indexes.")
#
#             self.create_pooling_entries(context, pool_records)
#             return SapioWebhookResult(True, "Pooling entries created successfully.")
#         except Exception as e:
#             return SapioWebhookResult(False, f"An error occurred: {str(e)}")
#
#     def get_pooling_type(self) -> PoolingType:
#         pooling_type = self.get_user_input("Select pooling type (even/uneven):", ["even", "uneven"])
#         return PoolingType(pooling_type)
#
#     def get_pool_parameters(self) -> tuple[Decimal, Decimal]:
#         pool_molarity = self.get_decimal_input("Enter desired pool molarity (nM):", min_value=Decimal('0.01'))
#         pool_volume = self.get_decimal_input("Enter desired pool volume (µL):", min_value=Decimal('0.01'))
#         return pool_molarity, pool_volume
#
#     def get_decimal_input(self, prompt: str, min_value: Decimal) -> Decimal:
#         while True:
#             try:
#                 value = Decimal(self.get_user_input(prompt))
#                 if value < min_value:
#                     raise ValueError(f"Value must be at least {min_value}")
#                 return value.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)
#             except ValueError as e:
#                 print(f"Invalid input: {e}")
#
#     def get_user_input(self, prompt: str, options: Optional[List[str]] = None) -> str:
#         while True:
#             response = input(prompt + " ").strip().lower()
#             if options and response not in options:
#                 print(f"Invalid input. Please choose from: {', '.join(options)}")
#             else:
#                 return response
#
#     # def get_latest_molarity(self, sample: SampleModel) -> Decimal:
#     #     qpcr_molarity = self.get_qpcr_molarity(sample)
#     #     return qpcr_molarity if qpcr_molarity is not None else self.get_tapestation_molarity(sample)
#
#     # def get_qpcr_molarity(self, sample: SampleModel) -> Optional[Decimal]:
#     #     qpcr_data = self.rel_man.load_children_of_type(sample, QCDatumModel)
#     #     if qpcr_data:
#     #         latest_qpcr = max(qpcr_data, key=lambda x: x.get_DateCreated_field())
#     #         return Decimal(str(latest_qpcr.get_Concentration_field()))
#     #     return None
#     #
#     # def get_tapestation_molarity(self, sample: SampleModel) -> Decimal:
#     #     tapestation_data = self.rel_man.get_children_of_type(sample, TapestationResultModel)
#     #     if tapestation_data:
#     #         latest_tapestation = max(tapestation_data, key=lambda x: x.get_DateCreated_field())
#     #         return Decimal(str(latest_tapestation.get_Molarity_field()))
#     #     raise ValueError(f"No molarity data found for sample {sample.get_SampleId_field()}")def
#
#     def calculate_even_pooling(self, samples: List[SampleModel], pool_molarity: Decimal, pool_volume: Decimal) -> List[
#         Dict[str, Union[SampleModel, Decimal]]]:
#         num_samples = len(samples)
#         return [self.calculate_sample_volumes(sample, pool_molarity, pool_volume / num_samples) for sample in samples]
#
#     def calculate_uneven_pooling(self, samples: List[SampleModel], pool_molarity: Decimal, pool_volume: Decimal) -> \
#             List[Dict[str, Union[SampleModel, Decimal]]]:
#         pool_entries = []
#         remaining_percentage = Decimal('100.00')
#
#         for i, sample in enumerate(samples):
#             if i == len(samples) - 1:
#                 percentage = remaining_percentage
#             else:
#                 percentage = self.get_decimal_input(
#                     f"Enter percentage for sample {sample.get_SampleId_field()} (%):",
#                     min_value=self.MIN_PERCENTAGE
#                 )
#                 if percentage > remaining_percentage:
#                     raise ValueError(f"Percentage exceeds remaining available percentage ({remaining_percentage}%)")
#                 remaining_percentage -= percentage
#
#             sample_volume = (pool_volume * percentage / Decimal('100.00'))
#             pool_entries.append(self.calculate_sample_volumes(sample, pool_molarity, sample_volume))
#
#         return pool_entries
#
#     def calculate_sample_volumes(self, sample: SampleModel, pool_molarity: Decimal, sample_pool_volume: Decimal) -> \
#             Dict[str, Union[SampleModel, Decimal]]:
#         sample_molarity = 10
#         sample_volume = (sample_pool_volume * pool_molarity) / sample_molarity
#         buffer_volume = sample_pool_volume - sample_volume
#
#         return {
#             "sample": sample,
#             "sample_volume": sample_volume.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP),
#             "buffer_volume": buffer_volume.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)
#         }
#
#     # def check_indexes(self, samples: List[SampleModel]) -> bool:
#     #     indexes = set()
#     #     for sample in samples:
#     #         index = self.get_sample_index(sample)
#     #         if index in indexes:
#     #             return False
#     #         indexes.add(index)
#     #     return True
#     #
#     # def get_sample_index(self, sample: SampleModel) -> str:
#     #     self.rel_man.load_children_of_type([sample], IndexBarcodeModel)
#     #     index_barcode = sample.get_children_of_type(IndexBarcodeModel)
#     #     if not index_barcode:
#     #         raise ValueError(f"No index found for sample {sample.get_SampleId_field()}")
#     #     return index_barcode[0].get_IndexId_field()
#
#     def create_pooling_entries(self, context: SapioWebhookContext,
#                                pool_entries: List[Dict[str, Union[SampleModel, Decimal]]]):
#         pooling_step = self.get_or_create_pooling_step(context)
#         for entry in pool_entries:
#             self.create_pooling_entry(context, pooling_step, entry)
#
#     def get_or_create_pooling_step(self, context: SapioWebhookContext) -> ElnEntryStep:
#         pooling_step = next(
#             (step for step in context.active_protocol.get_sorted_step_list() if step.get_name() == "Pooling"), None)
#         if not pooling_step:
#             criteria: ElnEntryCriteria = ElnEntryCriteria(ElnEntryType.Table,"Pooling",
#                                                           ElnBaseDataType.EXPERIMENT_DETAIL.data_type_name,
#                                                           field_map_list=table_data,
#                                                           field_definition_list=field_defs, is_renamable=False)
#             self.eln_man.add_experiment_entry(exp_id, criteria)
#
#             eln_experiment_protocol = cast(ElnExperimentProtocol, context.active_protocol)
#             pooling_step = ELNStepFactory.create_table_step(eln_experiment_protocol, "Pooling",
#                                                             ELNSampleDetailModel.data_type_name.__str__())
#         return pooling_step
#
#     def create_pooling_entry(self, context: SapioWebhookContext, pooling_step: ElnEntryStep,
#                              entry: Dict[str, Union[SampleModel, Decimal]]):
#         step = self.exp_handler.get_step("PoolingEntry")
#         pooling_entry = self.exp_handler.add_eln_row(step, ELNSampleDetailModel)
#         pooling_entry.set_field_value("Sample", entry["sample"].get_SampleId_field())
#         pooling_entry.set_field_value("SampleVolume", float(entry["sample_volume"]))
#         pooling_entry.set_field_value("BufferVolume", float(entry["buffer_volume"]))
#         self.rec_man.store_and_commit()
