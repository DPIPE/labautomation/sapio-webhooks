import io

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import AttachmentModel
from utilities.webhook_handler import OsloWebhookHandler

SAMPLE_ID = "Sample_ID"
SAMPLE_PROJECT = "Sample_Project"
BREAK_LINE = "\n"
COMMA = ","


class MiSeqSampleSheetUpdater(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        step_records = self.exp_handler.get_step_records("Generate Sample Sheet")

        def consume_data(chunk: bytes):
            file_data.write(chunk)

        # get attachment data from record(s)
        for record in step_records:
            with io.BytesIO() as file_data:
                context.data_record_manager.get_attachment_data(record, consume_data)
                file_data.flush()
                file_data.seek(0)
                file_bytes = file_data.read()
                file_bytes = self.process_file(context, file_bytes)
                # Update existing attachment data
                context.data_record_manager.set_attachment_data(record, record.get_field_value(
                    AttachmentModel.FILEPATH__FIELD_NAME.__str__()), io.BytesIO(file_bytes))
                file_data.close()
                break

        return SapioWebhookResult(True)

    def process_file(self, context, bytes_data):
        # Convert the bytes data to a string & split lines
        string_data = bytes_data.decode()
        lines = string_data.split(BREAK_LINE)

        # Find the line that contains the Sample_Project column name
        header_line = None
        for line in lines:
            if SAMPLE_PROJECT in line:
                header_line = line
                break
        if header_line is None:
            return bytes_data

        # Get the index of the Sample_Project and Sample_ID column name
        sample_project_index = header_line.split(COMMA).index(SAMPLE_PROJECT)
        sample_id_index = header_line.split(COMMA).index(SAMPLE_ID)

        output_lines = []
        skip_section = False
        for line in lines:
            if line.startswith("[Settings]"):
                skip_section = True
            elif line.startswith("[Data]"):
                skip_section = False
                output_lines.append(line)
            elif line.startswith("[Header]"):
                skip_section = False
                output_lines.append(line)
            elif not skip_section:
                output_lines.append(line)

        # Join the updated lines with line breaks
        updated_data_string = BREAK_LINE.join(output_lines)

        # Prepend the original lines above the header line to the updated data string
        # original_lines = BREAK_LINE.join(lines[:lines.index(header_line)])

        bytes_data = updated_data_string.encode()
        return bytes_data
