import io

from openpyxl.styles import Border, Side
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from entry_toolbar.add_samples_to_plate_ngs_snp_id import ElnEntryStep
from utilities.data_models import SampleModel, ELNSampleDetailModel, IndexBarcodeModel, ProjectModel
from utilities.oslo_exception import OsloException
from utilities.sample_utils import SampleUtils
from utilities.xlsx_utils import XLSXUtils
from workflows.nsc.generate_nsc_file_base import GenerateNSCFileBase


ENTRY_TAG: str = "GENERATED PIPETTING WORKSHEET"
SAMPLES_ENTRY_TAG: str = "NSC SAMPLES"


class GenerateNSCSamplePrepPipettingWorksheet(GenerateNSCFileBase):
    def set_attachment_step(self) -> None:
        try:
            self.attachment_step = self.exp_handler.get_step_by_option(ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{ENTRY_TAG}\" was found in the experiment.")

    def generate_file_data(self) -> (str, bytes):
        # Grab the samples from the samples entry and load the related records we'll need for the file.
        try:
            samples_step: ElnEntryStep = self.exp_handler.get_step_by_option(SAMPLES_ENTRY_TAG)
        except Exception:
            raise OsloException(f"No entry with tag \"{SAMPLES_ENTRY_TAG}\" was found in the experiment.")
        samples: list[SampleModel] | None = self.exp_handler.get_step_models(samples_step, SampleModel)
        if not samples:
            raise OsloException(f"No samples have been added to the {samples_step.get_name()} entry.")

        sample_details: list[ELNSampleDetailModel] = (
            self.exp_handler.get_step_models("Input Concentration and Volume Calculation", ELNSampleDetailModel))
        samples_to_details: dict[SampleModel, ELNSampleDetailModel] = self.map_samples_to_sample_details(samples,
                                                                                                         sample_details)
        self.rel_man.load_path_of_type(samples, RelationshipPath().parent_type(SampleModel)
                                       .parent_type(ProjectModel))
        self.rel_man.load_path_of_type(samples, RelationshipPath().parent_type(SampleModel)
                                       .child_type(IndexBarcodeModel))

        # Sort the samples before writing their data to the file.
        sorted_samples: list[SampleModel] = SampleUtils.sort_samples_by_well_positions(samples, horizontal=False)

        # Write the sample data with formatting matching the example file provided.
        headers: list[str] = ["Project", "Sample name", "Position", "Input (ng)", "Vol total (µL)", "Volume DNA (µL)",
                              "Volume Buffer(µL)", "Index name"]
        workbook: Workbook = Workbook()
        worksheet: Worksheet = workbook.active
        border: Border = Border(left=Side(style="thin"), right=Side(style="thin"), top=Side(style="thin"),
                                bottom=Side(style="thin"))
        row_index: int = 1
        for x, header in enumerate(headers, start=1):
            XLSXUtils.write_cell(worksheet, row_index, x, header, border)
        row_index += 1
        for x, sample in enumerate(sorted_samples, start=row_index):
            sample_detail: ELNSampleDetailModel = samples_to_details[sample]
            try:
                XLSXUtils.write_cell(worksheet, x, 1, sample.get_parent_of_type(SampleModel)
                                     .get_parent_of_type(ProjectModel).get_ProjectName_field(), border)
            except Exception:
                XLSXUtils.write_cell(worksheet, x, 1, "", border)
            XLSXUtils.write_cell(worksheet, x, 2, sample.get_OtherSampleId_field(), border)
            XLSXUtils.write_cell(worksheet, x, 3, SampleUtils.get_row_col_position_str(sample), border)
            XLSXUtils.write_cell(worksheet, x, 4, sample_detail.get_field_value("InputConcentration"), border)
            XLSXUtils.write_cell(worksheet, x, 5, sample_detail.get_field_value("TotalVolume"), border)
            XLSXUtils.write_cell(worksheet, x, 6, sample_detail.get_field_value("SampleVolume"), border)
            XLSXUtils.write_cell(worksheet, x, 7, sample_detail.get_field_value("NucleaseFreeWaterVolume"), border)
            # TODO: Figure out how to get the index tag populated from the sample manifest uploaded. Use the index ID for now for the demo.
            try:
                # If there are for some reason multiple index barcode children, just use the first one.
                # TODO: See if this is feasible or not outside of the S20 demo.
                index_tag: str = (sample.get_parent_of_type(SampleModel).get_children_of_type(IndexBarcodeModel)[0]
                                  .get_IndexId_field())
                XLSXUtils.write_cell(worksheet, x, 8, index_tag, border)
            except Exception:
                XLSXUtils.write_cell(worksheet, x, 8, "", border)

        # Save the file data to a bytes buffer and return it along with the file extension.
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)
        return "xlsx", buffer.getvalue()
