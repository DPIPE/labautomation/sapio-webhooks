from abc import abstractmethod, ABC
from typing import cast

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import AttachmentModel, SampleModel, ELNSampleDetailModel
from utilities.oslo_exception import OsloException
from utilities.webhook_handler import OsloWebhookHandler


class GenerateNSCFileBase(OsloWebhookHandler, ABC):
    attachment_step: ElnEntryStep
    active_step: ElnEntryStep

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.active_step = cast(ElnEntryStep, context.active_step)

        # Set the attachment step so that we have a step to add the attachment to.
        try:
            self.set_attachment_step()
        except Exception as e:
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            else:
                raise e

        # Generate the file data, download the file to the user.
        try:
            file_ext, file_bytes = self.generate_file_data()
            file_name: str = (f"{TimeUtil.now_in_format('%Y-%m-%d')}-NSC-{self.user.username}"
                              f"-{self.exp_handler.get_template_name()}.{file_ext}")
        except Exception as e:
            self.exp_handler.uninitialize_step(self.attachment_step)
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            raise e
        self.callback.write_file(file_name, file_bytes)

        # Add the attachment to the entry.
        self._add_attachment(file_name, file_bytes)

        return SapioWebhookResult(True, refresh_data=True)

    @abstractmethod
    def set_attachment_step(self) -> None:
        pass

    @abstractmethod
    def generate_file_data(self) -> (str, bytes):
        pass

    def _add_attachment(self, file_name: str, file_bytes: bytes) -> None:
        # Get the attachment from the entry if one exists.
        attachments: list[AttachmentModel] = self.exp_handler.get_step_models(self.attachment_step, AttachmentModel)

        # If there is an attachment, update it with the new file data. If there isn't, then create a new one and add it
        # to the entry.
        crit: ElnAttachmentEntryUpdateCriteria = ElnAttachmentEntryUpdateCriteria()
        if attachments:
            attachment: AttachmentModel = attachments[0]
            AttachmentUtil.set_attachment_bytes(self.context, attachment, file_name, file_bytes)
        else:
            attachment: AttachmentModel = (
                AttachmentUtil.create_attachment(self.context, file_name, file_bytes, AttachmentModel))
            crit.record_id = attachment.record_id

        crit.attachment_name = attachment.get_FilePath_field()
        crit.template_item_fulfilled_timestamp = TimeUtil.now_in_millis()
        self.context.eln_manager.update_experiment_entry(self.attachment_step.protocol.get_id(),
                                                         self.attachment_step.get_id(), crit)

    def map_samples_to_sample_details(self, samples: list[SampleModel],
                                      sample_details: list[ELNSampleDetailModel] | None = None) -> dict[SampleModel, ELNSampleDetailModel]:
        if not sample_details:
            sample_details: list[ELNSampleDetailModel] | None = (
                self.exp_handler.get_step_models(cast(ElnEntryStep, self.context.active_step), ELNSampleDetailModel))
        if not sample_details:
            raise OsloException("There are no sample details for the samples.")
        samples_to_details: dict[SampleModel, ELNSampleDetailModel] = {}
        for sample in samples:
            samples_to_details[sample] = \
                [x for x in sample_details if x.get_SampleId_field() == sample.get_SampleId_field()][0]
        return samples_to_details
