import io
import json
from io import BytesIO
from typing import Any

import openpyxl
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl.styles import Side
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import AttachmentModel
from utilities.data_models import ProjectModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_DOWNLOAD: str = "fileDownload"

FILE_NAME: str = "L gDNA worksheet_[PROJECT NAME]_[DATE].xlsx"

THIN_BORDER: Border = Border(left=Side("thin"), right=Side("thin"), top=Side("thin"), bottom=Side("thin"))

TABLE_SPACE_AMOUNT: int = 5


class GeneratePipettingWorksheet(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_FILE_DOWNLOAD:
                return SapioWebhookResult(True)

        # Read data from the template file so that we can add to it.
        workbook: Workbook = openpyxl.load_workbook("./template_files/labeling_and_staining_file_template.xlsx")
        worksheet: Worksheet = workbook.get_sheet_by_name("Sheet1")

        # Write data from each of the entries to the file.
        self.write_data(worksheet)

        # Add the generated file to an attachment entry
        bytes_io = BytesIO()
        workbook.save(bytes_io)
        bytes_io.seek(0)

        if self.exp_handler.get_template_name() == "Labelling & Staining of gDNA":
            entry: AttachmentModel = self.inst_man.add_new_record_of_type(AttachmentModel)
            entry.set_FilePath_field("./template_files/labeling_and_staining_file_template.xlsx")
            entry.set_VersionNumber_field(1)
            self.rec_man.store_and_commit()

            AttachmentUtil.set_attachment_bytes(context, entry, "labeling_and_staining_file_template.xlsx", bytes_io.getvalue())
            self.exp_handler.set_step_records("Generated Pipetting Worksheet", [entry])
            self.exp_handler.initialize_step("Generated Pipetting Worksheet")

        # Download the file to the user.
        return self.download_file(workbook)

    def write_data(self, worksheet: Worksheet) -> None:
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        sample_details: list[DataRecord] = self.exp_handler.get_step_records("Dilution of gDNA Samples")
        master_mix_data: list[DataRecord] = self.exp_handler.get_step_records("Labeling Master Mix")
        staining_mix_data: list[DataRecord] = self.exp_handler.get_step_records("Staining Master Mix")
        qubit_solution_data: list[DataRecord] = self.exp_handler.get_step_records("Qubit HS Working Solution")

        curr_row_index: int = TABLE_SPACE_AMOUNT

        # Write data from the Diltuion of gDNA Samples entry.
        for x, sample_detail in enumerate(sample_details, start=curr_row_index):
            worksheet.insert_rows(x)
            self.write_cell(worksheet, x, 1, sample_detail.get_field_value("OtherSampleId"))
            self.write_cell(worksheet, x, 2, sample_detail.get_field_value("gDNAConcentration"))
            self.write_cell(worksheet, x, 3, sample_detail.get_field_value("VolumeOfUltraPureWater"))
            self.write_cell(worksheet, x, 4, sample_detail.get_field_value("gDNAVolume"))
            curr_row_index += 1
        curr_row_index += TABLE_SPACE_AMOUNT

        # Write data from the Labeling Master Mix entry.
        total_master_mix_volume: float = 0.0
        total_master_mix_total: float = 0.0
        for x, master_mix in enumerate(master_mix_data, start=curr_row_index):
            worksheet.insert_rows(x)
            self.write_cell(worksheet, x, 1, master_mix.get_field_value("LabelingReaction"))
            self.write_cell(worksheet, x, 2, master_mix.get_field_value("VolumeForOneSample"))
            self.write_cell(worksheet, x, 3, master_mix.get_field_value("MasterMixExcess"))
            self.write_cell(worksheet, x, 4, master_mix.get_field_value("MasterMixTotal"))
            total_master_mix_volume += float(master_mix.get_field_value("VolumeForOneSample"))
            total_master_mix_total += float(master_mix.get_field_value("MasterMixTotal"))
            curr_row_index += 1
        worksheet.insert_rows(curr_row_index)
        self.write_cell(worksheet, curr_row_index, 1, "Total Master Mix Volume")
        self.write_cell(worksheet, curr_row_index, 2, total_master_mix_volume)
        self.write_cell(worksheet, curr_row_index, 3, "")
        self.write_cell(worksheet, curr_row_index, 4, total_master_mix_total)
        curr_row_index += TABLE_SPACE_AMOUNT

        # Write data from the Staining Master Mix entry.
        total_staining_mix_volume: float = 0.0
        total_staining_mix_total: float = 0.0
        for x, staining_mix in enumerate(staining_mix_data, start=curr_row_index):
            worksheet.insert_rows(x)
            self.write_cell(worksheet, x, 1, staining_mix.get_field_value("StainingReaction"))
            self.write_cell(worksheet, x, 2, staining_mix.get_field_value("VolumeForOneSample"))
            self.write_cell(worksheet, x, 3, staining_mix.get_field_value("MasterMixExcess"))
            self.write_cell(worksheet, x, 4, staining_mix.get_field_value("MasterMixTotal"))
            total_staining_mix_volume += float(staining_mix.get_field_value("VolumeForOneSample"))
            total_staining_mix_total += float(staining_mix.get_field_value("MasterMixTotal"))
            curr_row_index += 1
        worksheet.insert_rows(curr_row_index)
        self.write_cell(worksheet, curr_row_index, 1, "Total Master Mix Volume")
        self.write_cell(worksheet, curr_row_index, 2, total_staining_mix_volume)
        self.write_cell(worksheet, curr_row_index, 3, "")
        self.write_cell(worksheet, curr_row_index, 4, total_staining_mix_total)
        curr_row_index += 3

        # Write data from the Qubit HS Working Solution entry.
        for x, qubit_solution in enumerate(qubit_solution_data, start=curr_row_index):
            worksheet.insert_rows(x)
            self.write_cell(worksheet, x, 1, qubit_solution.get_field_value("WorkingSolution"), bold=True)
            self.write_cell(worksheet, x, 2, qubit_solution.get_field_value("PerSample"))
            self.write_cell(worksheet, x, 3, qubit_solution.get_field_value("MasterMix"))
            curr_row_index += 1

        # Write data for the Qubit Measurement table
        curr_row_index += 3
        self.write_cell(worksheet, curr_row_index, 1, "Qubit Reading (ng/uL)", bold=True)
        curr_row_index += 1
        self.write_cell(worksheet, curr_row_index, 1, "Measurement 1", bold=True)
        self.write_cell(worksheet, curr_row_index, 2, "Measurement 2", bold=True)
        self.write_cell(worksheet, curr_row_index, 3, "Mean* (ng/uL)", bold=True)
        self.write_cell(worksheet, curr_row_index, 4, "Standard deviation", bold=True)
        self.write_cell(worksheet, curr_row_index, 5, "Coeffecient Variation", bold=True)

    def write_cell(self, worksheet: Worksheet, index: int, col: int, data: Any, color: str | None = None,
                   bold: bool = False, size: int | None = None) -> None:
        worksheet.cell(index, col).value = round(data, 2) if isinstance(data, float) else data
        worksheet.cell(index, col).border = THIN_BORDER
        font: Font = Font(name='Calibri')
        if color:
            font.color = color
        if bold:
            font.bold = bold
        if size:
            font.size = size
        worksheet.cell(index, col).font = font

    def download_file(self, workbook: Workbook) -> SapioWebhookResult:
        # Generate the file name.
        file_name: str = FILE_NAME

        # Set the project name on the file name depending on the classifications of samples brought into the experiment.
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        sample_types: set[str] = set([x.get_ExemplarSampleType_field() for x in samples])
        if "Research" in sample_types:
            project_name: str = ",".join(self.get_project_and_order_names(samples))
        else:
            # For now, keep a constant "OGM" project name in the file name if there are only Diagnostic samples.
            project_name: str = "OGM"
        file_name = file_name.replace("[PROJECT NAME]", project_name)

        # Add the date to the file name.
        file_name = file_name.replace("[DATE]", TimeUtil.now_in_format("%Y%m%d"))

        # Save the file data to the in-memory buffer and download the file to the user.
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)
        self.response_map[STATUS] = STATUS_FILE_DOWNLOAD
        return FileUtil.write_file(file_name, buffer.getvalue(), request_context=json.dumps(self.response_map))

    def get_project_and_order_names(self, samples: list[SampleModel]) -> list[str]:
        project_and_order_names: list[str] = []
        self.rel_man.load_parents_of_type(samples, ProjectModel)
        self.rel_man.load_parents_of_type(samples, RequestModel)
        for sample in samples:
            project: ProjectModel | None = sample.get_parent_of_type(ProjectModel)
            if project:
                project_and_order_names.append(project.get_ProjectName_field())
            else:
                order: RequestModel | None = sample.get_parent_of_type(RequestModel)
                if order:
                    project_and_order_names.append(order.get_RequestName_field())

        return project_and_order_names
