import statistics

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, QCDatumModel, ELNExperimentDetailModel
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-811
Description : ELN On save rule to populate qubit results and route samples
"""

# Entry Names
QUBIT_ENTRY = "Qubit Calculations & Sample Routing"
FINAL_RESULTS_ENTRY = "Final Instrument Results"

# TAGS
SOURCE_SAMPLES = "SOURCE SAMPLES"
CV_MAX = "CV MAX"
MIN_CONC = "MIN CONC"
MAX_CONC = "MAX CONC"

# experiment detail fields
MEAN_FIELD = "SampleMean"
SD_FIELD = "SampleStd"
SAMPLE_ID_FIELD = "SampleId"
SAMPLE_NAME_FILED = "OtherSampleId"
QC_STATUS_FIELD = "QCStatus"


class QubitCalculations(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples
        sample_records = get_records_from_entry_with_option(context, SOURCE_SAMPLES)
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        self.rel_man.load_children_of_type(samples, QCDatumModel)

        # map sample id to their concentrations
        qubit_entry = exp_handler.get_step(QUBIT_ENTRY, True)
        field_maps = []
        cv_max = float(qubit_entry.get_options().get(CV_MAX))
        min_conc = float(qubit_entry.get_options().get(MIN_CONC))
        max_conc = float(qubit_entry.get_options().get(MAX_CONC))
        for sample in samples:
            qc_datum_models: list[QCDatumModel] | None = \
                [qc for qc in sample.get_children_of_type(QCDatumModel)
                 if qc.get_ExperimentRecordId_field() == context.eln_experiment.experiment_record_id]
            concentrations = []
            for qc_datum_model in qc_datum_models:
                concentrations.append(qc_datum_model.get_Concentration_field())
            if len(concentrations) > 1:
                mean = statistics.mean(concentrations)
                sd = statistics.stdev(concentrations)
            else:
                mean = concentrations.pop()
                sd = 0
            cv = sd / mean
            fields = dict()
            fields[SAMPLE_ID_FIELD] = sample.get_SampleId_field()
            if sample.get_OtherSampleId_field():
                fields[SAMPLE_NAME_FILED] = sample.get_OtherSampleId_field()
            fields[MEAN_FIELD] = mean
            fields[SD_FIELD] = sd
            if cv < cv_max and min_conc <= mean <= max_conc:
                fields[QC_STATUS_FIELD] = "Passed"
            else:
                fields[QC_STATUS_FIELD] = "Failed"
            field_maps.append(fields)

        # get qubit entry and add these new records
        qubit_records = qubit_entry.get_records()
        if qubit_records:
            self.dr_man.delete_data_record_list(qubit_records)
        self.dr_man.add_data_records_with_data(qubit_entry.get_data_type_names().pop(), field_maps)

        return SapioWebhookResult(True, eln_entry_refresh_list=[qubit_entry.eln_entry])


class QubitSampleRouting(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get qubit entry
        qubit_entry = exp_handler.get_step(QUBIT_ENTRY, True)
        qubit_records = qubit_entry.get_records()
        qubit_models = self.inst_man.add_existing_records_of_type(qubit_records, ELNExperimentDetailModel)

        # map sample id to their QC Status
        sample_id_to_qc_status = dict()
        sample_id_to_mean_conc = dict()
        for qubit_model in qubit_models:
            sample_id_to_qc_status[qubit_model.get_field_value(SAMPLE_ID_FIELD)] = qubit_model\
                .get_field_value(QC_STATUS_FIELD)
            sample_id_to_mean_conc[qubit_model.get_field_value(SAMPLE_ID_FIELD)] = qubit_model \
                .get_field_value(MEAN_FIELD)

        # get final instrument results
        final_results_entry = exp_handler.get_step(FINAL_RESULTS_ENTRY, True)
        final_results_records = final_results_entry.get_records()
        final_results_models = self.inst_man.add_existing_records_of_type(final_results_records,
                                                                          ELNExperimentDetailModel)

        # set qc status on final instrument results
        for final_results_model in final_results_models:
            sample_id = final_results_model.get_field_value(SAMPLE_ID_FIELD)
            final_results_model.set_field_value(QC_STATUS_FIELD, sample_id_to_qc_status[sample_id])

        # set concentration on samples
        sample_records = get_records_from_entry_with_option(context, SOURCE_SAMPLES)
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        for sample in samples:
            if sample.get_SampleId_field() in sample_id_to_mean_conc:
                sample.set_Concentration_field(sample_id_to_mean_conc[sample.get_SampleId_field()])
        self.rec_man.store_and_commit()

        # submit the final results table
        context.eln_manager.submit_experiment_entry(context.eln_experiment.notebook_experiment_id,
                                                    final_results_entry.eln_entry.entry_id)

        return SapioWebhookResult(True, eln_entry_refresh_list=[final_results_entry.eln_entry])
