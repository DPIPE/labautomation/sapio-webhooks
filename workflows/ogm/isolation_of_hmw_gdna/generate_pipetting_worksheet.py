import datetime
import io
from typing import List

from openpyxl.cell import Cell
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl.styles import Side
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ProjectModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-809
Description : ELN Entry toolbar button that generates pipetting worksheet
"""


class GeneratePipettingWorkSheet(OsloWebhookHandler):
    # Entry Names
    SB_ENTRY_NAME = "Stabilizing Buffer (SB)"
    HEMO_CELL_CUE_ENTRY_NAME = "HemoCue Cell count"
    LYSIS_AND_DIGESTION_COCKTAIL_MASTER_MIX = "Lysis and Digestion Cocktail Master Mix (LD MM)"
    SAMPLES_ENTRY = "Samples"

    # Entry names to fields
    ENTRY_NAME_TO_FIELDS = {
            SB_ENTRY_NAME: {
                "SB Component": "SBComponent",
                "SB Component Volume (μl)": "SBComponentVolume",
                "SB Component Total Volume (μl)": "SBComponentTotalVolume"
                },
            HEMO_CELL_CUE_ENTRY_NAME: {
                "Cell count* (10^9/L) from hemoCue": "CellCountFromHemoCue",
                "Sample Name": "OtherSampleId",
                "Sample Id": "SampleIdentifier",
                "Archive Position": "ArchivePosition",
                "Transfer Vol* (μl)": "TransferVolume",
                "SB Dilution Vol (μl)": "SBDilutionVol",
                "Removal Vol (μl)": "RemovalVolume"
                },
            LYSIS_AND_DIGESTION_COCKTAIL_MASTER_MIX: {
                "LD MM Component": "LDMMComponent",
                "MM Component V (μl)": "MMComponentV",
                "MM Excess": "MMExcess",
                "MM Component Total V (μl)": "MMComponentTotalV"
                }
            }

    # handlers
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        self.exp_handler = ExperimentHandler(context)

        # get samples
        samples_entry = self.exp_handler.get_step(self.SAMPLES_ENTRY, True)
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # enforce hemo cue cell entry is submitted
        hemo_cue_cell_entry = self.exp_handler.get_step(self.HEMO_CELL_CUE_ENTRY_NAME)
        if hemo_cue_cell_entry.eln_entry.entry_status != ExperimentEntryStatus.Completed:
            return PopupUtil.ok_popup("Error", f"{self.HEMO_CELL_CUE_ENTRY_NAME} needs to be completed")

        # generate file
        workbook = self.__generate_work_book(samples)
        file_bytes = self.__generate_file_bytes(workbook)
        file_name = self.__get_file_name(samples).replace("/", "_")

        # Write file to attachment entry
        params = FileAttachmentParams(
            context,
            file_name,
            file_bytes,
            "Generated Pipetting File",
            self.exp_handler,
            self.inst_man
        )
        attach_file_to_entry(params)

        return SapioWebhookResult(
                True,
                client_callback_request=WriteFileRequest(file_bytes, file_name)
                )

    def __generate_work_book(self, samples):
        # write to a xlsx file
        workbook: Workbook = Workbook()
        worksheet: Worksheet = workbook.active
        curr_row = 1
        cols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for col in cols:
            worksheet.column_dimensions[col].width = 40

        # add SB entry
        curr_row = self.__write_entry_to_worksheet(
                worksheet,
                curr_row,
                self.SB_ENTRY_NAME,
                [1, 2],
                cols
                )

        # add HemoCue Cell Count
        curr_row = self.__write_entry_to_worksheet(
                worksheet,
                curr_row,
                self.HEMO_CELL_CUE_ENTRY_NAME,
                [],
                cols
                )

        # add Lysis and digestion entry
        curr_row = self.__write_entry_to_worksheet(
                worksheet,
                curr_row,
                self.LYSIS_AND_DIGESTION_COCKTAIL_MASTER_MIX,
                [1, 3],
                cols
                )

        cell: Cell = worksheet[cols[1] + str(curr_row)]
        cell.value = "*Pipette LBB and DE Detergent slowly due to high viscosity and risk of bubble formation."
        curr_row += 1

        cell: Cell = worksheet[cols[1] + str(curr_row)]
        cell.value = "** Add right before use"
        curr_row += 2

        # add qubit measurement table
        self.__add_qubit_table(worksheet, curr_row, samples, cols)

        return workbook

    def __add_qubit_table(self, worksheet, curr_row, samples, cols):
        # add entry name
        cell: Cell = worksheet[cols[0] + str(curr_row)]
        cell.value = "Qubit Measurement Table"
        cell.font = Font(size=18, bold=True, color="0080FF")
        curr_row += 2
        headers = ["Sample ID", "Sample Name", "Left", "Middle", "Right"]
        cur_col = 0
        style = Side(border_style="thin")
        for header in headers:
            cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
            cell.value = header
            cell.font = Font(bold=True)
            cell.border = Border(top=style, left=style, right=style, bottom=style)
            cur_col += 1
        curr_row += 1
        self.rel_man.load_parents_of_type(samples, SampleModel)
        for sample in samples:
            parent_sample = sample.get_parent_of_type(SampleModel)
            sample_name = ""
            if parent_sample and parent_sample.get_OtherSampleId_field():
                sample_name = parent_sample.get_OtherSampleId_field()
            values = [sample.get_SampleId_field(), sample_name, "", "", ""]
            cur_col = 0
            for value in values:
                cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
                cell.value = value
                cell.border = Border(top=style, left=style, right=style, bottom=style)
                cur_col += 1
            curr_row += 1
        curr_row += 1
        return curr_row


    def __write_entry_to_worksheet(self, worksheet, curr_row, entry_name, track_total_for_cols, cols):

        # add sb entry data to file
        # get entry records
        sb_entry = self.exp_handler.get_step(entry_name)
        records = sb_entry.get_records()

        # add entry name
        cell: Cell = worksheet[cols[0] + str(curr_row)]
        cell.value = entry_name
        cell.font = Font(size=18, bold=True, color="0080FF")

        curr_row += 2

        # add table

        # add headers
        headers = self.ENTRY_NAME_TO_FIELDS[entry_name].keys()
        cur_col = 0
        style = Side(border_style="thin")
        for header in headers:
            cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
            cell.value = header
            cell.font = Font(bold=True)
            cell.border = Border(top=style, left=style, right=style, bottom=style)
            cur_col += 1

        curr_row += 1
        # add table data
        track_total = [0] * (len(track_total_for_cols))
        for record in records:
            cur_col = 0
            for field in headers:
                value = record.get_field_value(self.ENTRY_NAME_TO_FIELDS[entry_name][field])
                if not value:
                    value = ""
                if cur_col in track_total_for_cols:
                    if value == "":
                        value = 0
                    track_total[track_total_for_cols.index(cur_col)] += float(value)
                cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
                cell.value = value
                cell.border = Border(top=style, left=style, right=style, bottom=style)
                cur_col += 1
            curr_row += 1

        # add total value
        if track_total_for_cols:
            # set border
            cur_col = 0
            while cur_col < len(headers):
                cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
                cell.border = Border(top=style, left=style, right=style, bottom=style)
                cur_col += 1
            cur_col = 0
            cell: Cell = worksheet[cols[cur_col] + str(curr_row)]
            cell.value = "Total"
            cell.font = Font(bold=True)

            for i in range(0, len(track_total)):
                cell: Cell = worksheet[cols[track_total_for_cols[i]] + str(curr_row)]
                cell.value = track_total[i]
                if track_total_for_cols[i] == len(headers) - 1:
                    cell.font = Font(bold=True)
            curr_row += 1

        curr_row += 1
        return curr_row

    def __get_file_name(self, samples: List[SampleModel]):
        date = datetime.datetime.now().strftime('%m/%d/%Y')
        is_diagnostic = True
        for sample in samples:
            if sample.get_Classification_field() != "Diagnostics":
                is_diagnostic = False
                break
        if is_diagnostic:
            return "gDNA worksheet_OGM_" + date + ".xlsx"
        projects = set()
        self.an_man.load_ancestors_of_type([sample.backing_model for sample in samples], ProjectModel.DATA_TYPE_NAME)
        for sample in samples:
            if sample.get_Classification_field() == "Diagnostics":
                projects.add("OGM")
            else:
                curr_projects = self.an_man.get_ancestors_of_type(sample.backing_model, ProjectModel.DATA_TYPE_NAME)
                if curr_projects:
                    projects.add(self.inst_man.wrap(curr_projects.pop(), ProjectModel).get_ProjectName_field())
        project_name = "_"
        for project in projects:
            project_name += project + "_"
        if not projects:
            project_name = "_"
        file_name = "gDNA worksheet" + project_name + date + ".xlsx"
        return file_name

    @staticmethod
    def __generate_file_bytes(workbook: Workbook) -> bytes:
        # get the file bytes
        with io.BytesIO() as file_buffer:
            workbook.save(file_buffer)
            file_buffer.flush()
            file_buffer.seek(0)
            file_bytes: bytes = file_buffer.read()

        return file_bytes
