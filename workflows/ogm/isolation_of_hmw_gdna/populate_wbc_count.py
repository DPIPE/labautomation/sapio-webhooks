from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import EDTABloodSamplesModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class SetWBCCountOnIsolation(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)

        # type aliases
        EDTA_opt_t = EDTABloodSamplesModel | None

        # Get the records
        samples = exp_handler.get_step_models("Samples", SampleModel)
        eln_rows: list[PyRecordModel] = exp_handler.add_eln_rows(
                "WBC Count Measurement",
                len(samples),
                )

        self.rel_man.load_children_of_type(samples, EDTABloodSamplesModel)

        # read the records
        for idx, smpl in enumerate(samples):
            blood_sample: EDTA_opt_t = smpl.get_child_of_type(
                    EDTABloodSamplesModel
                )

            if blood_sample:
                eln_rows[idx].set_field_values({
                    "SampleIdentifier": smpl.get_SampleId_field(),
                    "OtherSampleId": smpl.get_OtherSampleId_field(),
                    "MeasurementNumber": "1",
                    "WBCCount": blood_sample.get_WBCCount_field(),
                })

        # insert the records
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
