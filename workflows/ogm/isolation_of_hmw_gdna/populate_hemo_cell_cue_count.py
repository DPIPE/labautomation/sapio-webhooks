from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import EDTABloodSamplesModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-809
Description : ELN On save rule to populate hemo cell cue count field
"""


class PopulateHemoCellCueCount(OsloWebhookHandler):
    # Entry Names
    SAMPLES_ENTRY = "Samples"
    HEMO_CELL_CUE_COUNT_ENTRY = "HemoCue Cell count"

    # experiment detail fields
    CELL_COUNT_FROM_HEMO_CUE_FIELD = "CellCountFromHemoCue"
    SAMPLE_NAME_FIELD = "OtherSampleId"
    SAMPLE_ID_FIELD = "SampleIdentifier"
    ARCHIVE_POSITION_FIELD = "ArchivePosition"
    REMOVAL_VOL_FIELD = "RemovalVolume"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples
        samples_entry = exp_handler.get_step(self.SAMPLES_ENTRY, True)
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        self.rel_man.load_children_of_type(samples, EDTABloodSamplesModel)
        self.rel_man.load_parents_of_type(samples, SampleModel)

        # get the field map list
        field_map_list = []
        for sample in samples:
            edta_blood_sample = sample.get_child_of_type(EDTABloodSamplesModel)
            parent_sample = sample.get_parent_of_type(SampleModel)
            fields = dict()
            fields[self.SAMPLE_ID_FIELD] = sample.get_SampleId_field()
            fields[self.REMOVAL_VOL_FIELD] = 1160
            if sample.get_StorageUnitPath_field():
                fields[self.ARCHIVE_POSITION_FIELD] = sample.get_StorageUnitPath_field()
            if parent_sample and parent_sample.get_OtherSampleId_field():
                fields[self.SAMPLE_NAME_FIELD] = parent_sample.get_OtherSampleId_field()
            if edta_blood_sample and edta_blood_sample.get_WBCCount_field():
                fields[self.CELL_COUNT_FROM_HEMO_CUE_FIELD] = edta_blood_sample.get_WBCCount_field()
            else:
                return SapioWebhookResult(True)
            field_map_list.append(fields)

        # get the hemo cell cue count entry and records to as per the number of samples
        hemo_cell_cue_count_entry = exp_handler.get_step(self.HEMO_CELL_CUE_COUNT_ENTRY, True)
        existing_records = hemo_cell_cue_count_entry.get_records()
        if existing_records:
            self.dr_man.delete_data_record_list(existing_records)
        hemo_cell_cue_records = self.dr_man.add_data_records_with_data(hemo_cell_cue_count_entry.get_data_type_names()
                                                                       .pop(), field_map_list)
        hemo_cell_cue_count_entry.add_records(hemo_cell_cue_records)
        return SapioWebhookResult(True)
