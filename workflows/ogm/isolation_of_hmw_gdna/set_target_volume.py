from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentDetailModel, SampleModel, EDTABloodSamplesModel, ELNSampleDetailModel
from utilities.utils import uninitialize_entry
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-809
Description : ELN On save rule to set target volume based on calculations in the hemo cell cue entry
"""

class SetTargetVolume(OsloWebhookHandler):
    # Entry Names
    HEMO_CELL_CUE_COUNT_ENTRY = "HemoCue Cell count"
    ISOLATE_GDNA_ENTRY = "Isolate gDNA"

    # ELN Experiment Detail Fields
    TARGET_VOLUME_FIELD = "TargetVolume"
    HEMO_SAMPLE_ID_FIELD = "SampleIdentifier"
    REMOVAL_VOL_FIELD = "RemovalVolume"
    TRANSFER_VOLUME_FIELD = "TransferVolume"
    SB_DILUTION_FIELD = "SBDilutionVol"

    # Tags
    ENFORCE_WBC_COUNT_MEASUREMENT_TAG = "ENFORCE WBC COUNT MEASUREMENT"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get hemo cell cue entry and get necessary fields
        hemo_cell_cue_entry = exp_handler.get_step(self.HEMO_CELL_CUE_COUNT_ENTRY, True)
        isolate_entry = exp_handler.get_step(self.ISOLATE_GDNA_ENTRY, True)
        hemo_cell_cue_records = hemo_cell_cue_entry.get_records()
        hemo_cell_cue_models = self.inst_man.add_existing_records_of_type(hemo_cell_cue_records,
                                                                          ELNExperimentDetailModel)
        sample_id_to_target_volume = dict()
        errors = []
        for hemo_cell_cue_model in hemo_cell_cue_models:
            sample_id = hemo_cell_cue_model.get_field_value(self.HEMO_SAMPLE_ID_FIELD)
            transfer_vol = hemo_cell_cue_model.get_field_value(self.TRANSFER_VOLUME_FIELD)
            sb_dilution_vol = hemo_cell_cue_model.get_field_value(self.SB_DILUTION_FIELD)
            removal_vol = hemo_cell_cue_model.get_field_value(self.REMOVAL_VOL_FIELD)
            if not sample_id or not transfer_vol or not sb_dilution_vol or not removal_vol:
                uninitialize_entry(isolate_entry.eln_entry, context)
                return PopupUtil.display_ok_popup("Error", "Not all fields are populated on the Hemo Cell Cue Table")
            target_volume = (transfer_vol + sb_dilution_vol) - removal_vol
            if target_volume > 0:
                sample_id_to_target_volume[sample_id] = target_volume
            else:
                errors.append(sample_id)
        if errors:
            uninitialize_entry(isolate_entry.eln_entry, context)
            return PopupUtil.display_ok_popup("Error", "Following Samples Have Negative Target Volume : " + " ,"
                                              .join(errors))

        # get the Isolate gDNA entry and update fields
        errors = []
        isolate_records = isolate_entry.get_records()
        isolate_models = self.inst_man.add_existing_records_of_type(isolate_records, ELNSampleDetailModel)
        for isolate_model in isolate_models:
            sample_id = isolate_model.get_SampleId_field()
            if sample_id in sample_id_to_target_volume:
                target_volume = sample_id_to_target_volume[sample_id]
                isolate_model.set_field_value(self.TARGET_VOLUME_FIELD, target_volume)
            else:
                errors.append(sample_id)
        if errors:
            uninitialize_entry(isolate_entry.eln_entry, context)
            return PopupUtil.display_ok_popup("Error", "Following Samples Were Not Located In The Hemo Cell Cue entry: "
                                                       "" + " ,".join(errors))
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
