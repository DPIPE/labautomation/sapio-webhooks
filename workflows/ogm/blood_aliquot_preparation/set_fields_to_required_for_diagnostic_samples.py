from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-806
Description : ELN on save rule to make entries mentioned in the tag required if the samples are diagnostic
"""


class SetFieldsToRequiredForDiagnosticSamples(OsloWebhookHandler):
    # Tag for the entries that need to be made required
    SET_ENTRIES_REQUIRED_IF_DIAGNOSTIC_TAG = "SET ENTRIES REQUIRED IF DIAGNOSTIC"
    REQUIRES_MANUAL_VALIDATION_TAG = "REQUIRES MANUAL VALIDATION"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples entry
        sample_steps = exp_handler.get_all_steps(SampleModel)
        samples_entry = None
        for sample_step in sample_steps:
            if self.SET_ENTRIES_REQUIRED_IF_DIAGNOSTIC_TAG in sample_step.get_options():
                samples_entry = sample_step
        if not samples_entry:
            return PopupUtil.ok_popup("Error", "Locating samples entry with tag : " + self
                                      .SET_ENTRIES_REQUIRED_IF_DIAGNOSTIC_TAG)
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # identify if diagnostic samples are present
        is_diagnostic = False
        for sample in samples:
            if sample.get_Classification_field() == "Diagnostics":
                is_diagnostic = True
                break
        if not is_diagnostic:
            # if not diagnostic then set the entries to be not required
            entry_names_to_be_made_required = samples_entry.get_options().get(
                self.SET_ENTRIES_REQUIRED_IF_DIAGNOSTIC_TAG) \
                .split(',')
            for entry_name in entry_names_to_be_made_required:
                entry = exp_handler.get_step(entry_name, True)
                options = entry.get_options()
                if self.REQUIRES_MANUAL_VALIDATION_TAG in options:
                    options.pop(self.REQUIRES_MANUAL_VALIDATION_TAG)
                    entry.set_options(options)
            return SapioWebhookResult(True)

        # if diagnostic then set the entries to be required
        entry_names_to_be_made_required = samples_entry.get_options().get(self.SET_ENTRIES_REQUIRED_IF_DIAGNOSTIC_TAG)\
            .split(',')
        for entry_name in entry_names_to_be_made_required:
            entry = exp_handler.get_step(entry_name, True)
            options = entry.get_options()
            options[self.REQUIRES_MANUAL_VALIDATION_TAG] = ""
            entry.set_options(options)

        return SapioWebhookResult(True)
