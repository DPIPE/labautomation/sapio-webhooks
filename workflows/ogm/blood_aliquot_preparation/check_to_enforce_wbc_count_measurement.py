from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, EDTABloodSamplesModel
from utilities.utils import initialize_entry
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-809
Description : ELN On save rule to determine if we need to enforce wbc count measurement field
"""


class CheckToEnforceWBCCountMeasurement(OsloWebhookHandler):
    # Entry Names
    SAMPLES_ENTRY = "Samples"
    WBC_COUNT_MEASUREMENT_ENTRY_IN_ISOLATION_OF_HMW = "WBC Count Measurement"

    # Tags
    REQUIRES_MANUAL_VALIDATION_TAG = "REQUIRES MANUAL VALIDATION"
    ENFORCE_WBC_COUNT_MEASUREMENT_TAG = "ENFORCE WBC COUNT MEASUREMENT"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get samples
        samples_entry = exp_handler.get_step(self.SAMPLES_ENTRY, True)
        sample_records = samples_entry.get_records()
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        self.rel_man.load_children_of_type(samples, EDTABloodSamplesModel)
        should_enforce = False
        for sample in samples:
            edta_blood_sample = sample.get_child_of_type(EDTABloodSamplesModel)
            if not edta_blood_sample.get_WBCCount_field():
                should_enforce = True
        if not should_enforce:
            return SapioWebhookResult(True)

        # if its being enforced set the tags on respective entries
        options = samples_entry.get_options()
        options[self.REQUIRES_MANUAL_VALIDATION_TAG] = ""
        samples_entry.set_options(options)

        wbc_entry = exp_handler.get_step(self.WBC_COUNT_MEASUREMENT_ENTRY_IN_ISOLATION_OF_HMW, True)
        options = wbc_entry.get_options()
        options[self.ENFORCE_WBC_COUNT_MEASUREMENT_TAG] = ""
        wbc_entry.set_options(options)

        return SapioWebhookResult(True)
