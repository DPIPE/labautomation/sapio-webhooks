from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-806
Description : ELN On save rule to go across and ensure only one replicate proceeds further in the workflow
"""


class RemoveReplicatesFromGoingToNextStep(OsloWebhookHandler):
    # Entry Names
    RESULTING_SAMPLES_ENTRY = "Resulting Samples"
    SAMPLES_TO_BE_REMOVED_ENTRY = "Samples To Be Removed"

    # Accession Tag
    OGM_ACCESSION_TAG = "OGM"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the samples
        samples_step = exp_handler.get_step(self.RESULTING_SAMPLES_ENTRY, True)
        samples_records: List[DataRecord] = samples_step.get_records()
        samples: List[SampleModel] = self.inst_man.add_existing_records_of_type(samples_records, SampleModel)

        # identify if diagnostic samples are present
        is_diagnostic = False
        for sample in samples:
            if sample.get_Classification_field() == "Diagnostics":
                is_diagnostic = True
                break

        # get the accession number for OGM
        accesion_number_ogm = 0
        if is_diagnostic:
            acc_manager = DataMgmtServer.get_accession_manager(context.user)
            pojo = AccessionSystemCriteriaPojo(self.OGM_ACCESSION_TAG)
            accesion_number_ogm = acc_manager.accession_for_system(1, pojo).pop()

        # get samples to be added
        samples_to_be_added: List[DataRecord] = []
        parent_samples_set = set()
        self.rel_man.load_parents_of_type(samples, SampleModel)
        i = 1
        for sample in samples:
            parent_sample = sample.get_parent_of_type(SampleModel)
            if parent_sample.get_SampleId_field() not in parent_samples_set:
                parent_samples_set.add(parent_sample.get_SampleId_field())
                if sample.get_Classification_field() == "Diagnostics":
                    # accession the new samples name
                    sample.set_OtherSampleId_field("OGM"+str(accesion_number_ogm)+"_"+str(i))
                    i += 1
            else:
                samples_to_be_added.append(sample.get_data_record())
            # copy over the classification
            sample.set_Classification_field(parent_sample.get_Classification_field())
        self.rec_man.store_and_commit()

        # add the samples to the entry
        samples_to_be_removed_step = exp_handler.get_step(self.SAMPLES_TO_BE_REMOVED_ENTRY, True)
        samples_to_be_removed_step.add_records(samples_to_be_added)
        samples_to_be_removed_step.complete_step()
        return SapioWebhookResult(True)
