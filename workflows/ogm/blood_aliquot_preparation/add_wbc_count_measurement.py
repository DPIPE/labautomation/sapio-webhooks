from typing import List, cast

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxIntegerFieldDefinition, \
    VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import TableEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import CallbackType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, ELNExperimentDetailModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-806
Description : ELN entry toolbar button to add eln experiment detail records to track wbc count 
for samples over multiple measurements
"""

class AddWbcCountMeasurement(OsloWebhookHandler):
    # Entry Names
    SAMPLES_ENTRY = "Samples"
    WBC_COUNT_MEASUREMENT_ENTRY = "WBC Count Measurement"

    # ELN Experiment Detail Fields
    SAMPLE_ID_FIELD = "SampleIdentifier"
    SAMPLE_NAME_FIELD = "OtherSampleId"
    MEASUREMENT_NUMBER_FIELD = "MeasurementNumber"
    WBC_COUNT_FIELD = "WBCCount"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        if not context.client_callback_result:
            # get the samples
            samples_step = exp_handler.get_step(self.SAMPLES_ENTRY, True)
            samples_records: List[DataRecord] = samples_step.get_records()
            samples: List[SampleModel] = self.inst_man.add_existing_records_of_type(samples_records, SampleModel)

            # get WBC count measurement entry and get measurement number
            wbc_count_measurement_step = exp_handler.get_step(self.WBC_COUNT_MEASUREMENT_ENTRY, True)
            if wbc_count_measurement_step.eln_entry.entry_status == ExperimentEntryStatus.Completed:
                return PopupUtil.ok_popup("Error", self.WBC_COUNT_MEASUREMENT_ENTRY + " has already been completed.")
            wbc_count_measurement_records: List[DataRecord] = wbc_count_measurement_step.get_records()
            wbc_count_measurement_models: List[ELNExperimentDetailModel] = self.inst_man.add_existing_records_of_type(
                wbc_count_measurement_records, ELNExperimentDetailModel)
            measurement_number = 0
            for wbc_count_measurement_model in wbc_count_measurement_models:
                if wbc_count_measurement_model.get_field_value(self.MEASUREMENT_NUMBER_FIELD) > measurement_number:
                    measurement_number = wbc_count_measurement_model.get_field_value(self.MEASUREMENT_NUMBER_FIELD)
            measurement_number += 1

            # get default values
            default_values = []
            for sample in samples:
                row = {self.SAMPLE_ID_FIELD: sample.get_SampleId_field(),
                       self.SAMPLE_NAME_FIELD: sample.get_OtherSampleId_field(),
                       self.MEASUREMENT_NUMBER_FIELD: measurement_number}
                default_values.append(row)

            # generate prompt for user to add in the measurement
            sample_id_field = VeloxStringFieldDefinition(ELNExperimentDetailModel.DATA_TYPE_NAME, self.SAMPLE_ID_FIELD,
                                                         "Sample Id")
            sample_id_field.editable = False
            sample_name_field = VeloxStringFieldDefinition(ELNExperimentDetailModel.DATA_TYPE_NAME,
                                                           self.SAMPLE_NAME_FIELD, "Sample Name")
            sample_name_field.editable = False
            measurement_number_field = VeloxIntegerFieldDefinition(ELNExperimentDetailModel.DATA_TYPE_NAME,
                                                                   self.MEASUREMENT_NUMBER_FIELD, "Measurement Number")
            measurement_number_field.editable = False
            wbc_count_field = VeloxDoubleFieldDefinition(ELNExperimentDetailModel.DATA_TYPE_NAME,
                                                         self.WBC_COUNT_FIELD, "WBC Count")
            wbc_count_field.editable = True
            wbc_count_field.required = True
            return PopupUtil.table_popup("Add Measurements", "Add the WBC count measurement for the following samples",
                                         fields=[sample_id_field, sample_name_field, measurement_number_field,
                                                 wbc_count_field], values=default_values)

        elif context.client_callback_result.get_callback_type() == CallbackType.TABLE_ENTRY_DIALOG and not context\
                .client_callback_result.user_cancelled:
            result: TableEntryDialogResult = cast(TableEntryDialogResult, context.client_callback_result)
            field_map_list = result.user_response_data_list

            # get the WBC count entry and add in the data
            wbc_count_measurement_step = exp_handler.get_step(self.WBC_COUNT_MEASUREMENT_ENTRY, True)
            wbc_count_measurement_records = self.dr_man.add_data_records_with_data(
                wbc_count_measurement_step.get_data_type_names().pop(), field_map_list)
            wbc_count_measurement_step.add_records(wbc_count_measurement_records)

        return SapioWebhookResult(True)
