from typing import List

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentDetailModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-806
Description : ELN on save rule to remove experiment details from table without sample id
"""


class RemoveExperimentDetailsWithoutSampleId(OsloWebhookHandler):
    # Entry Names
    WBC_COUNT_MEASUREMENT_ENTRY = "WBC Count Measurement"

    # ELN Experiment Detail Fields
    SAMPLE_ID_FIELD = "SampleIdentifier"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the experiment_details
        experiment_details_entry = exp_handler.get_step(self.WBC_COUNT_MEASUREMENT_ENTRY, True)
        experiment_detail_records: List[DataRecord] = experiment_details_entry.get_records()
        experiment_details: List[ELNExperimentDetailModel] = self.inst_man.add_existing_records_of_type(
            experiment_detail_records, ELNExperimentDetailModel)

        # if manually added then remove them and alert user
        added_manually = False
        for experiment_detail in experiment_details:
            if not experiment_detail.get_field_value(self.SAMPLE_ID_FIELD):
                added_manually = True
                experiment_detail.delete()
        self.rec_man.store_and_commit()

        if added_manually:
            client_callback.ok_dialog("Error", "Cannot add records here using this button")

        return SapioWebhookResult(True, eln_entry_refresh_list=[experiment_details_entry.eln_entry])
