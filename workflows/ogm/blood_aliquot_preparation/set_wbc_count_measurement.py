from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentDetailModel, SampleModel, EDTABloodSamplesModel
from utilities.utils import disable_step
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-806
Description : ELN On save rule to go across all measurements reported and assign the latest value to the sample
"""

class SetWbcCountMeasurement(OsloWebhookHandler):
    # Entry Names
    SAMPLES_ENTRY = "Samples"
    WBC_COUNT_MEASUREMENT_ENTRY = "WBC Count Measurement"
    HEMO_CELL_CUE_COUNT_ENTRY = "Hemo Cell Cue count"

    # ELN Experiment Detail Fields
    SAMPLE_ID_FIELD = "SampleIdentifier"
    MEASUREMENT_NUMBER_FIELD = "MeasurementNumber"
    WBC_COUNT_FIELD = "WBCCount"

    # Tags
    ENFORCE_WBC_COUNT_MEASUREMENT_TAG = "ENFORCE WBC COUNT MEASUREMENT"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        if not context.client_callback_result:
            # get the WBC step and retrieve sample id to wbc count field for the latest measurement
            wbc_count_measurement_step = exp_handler.get_step(self.WBC_COUNT_MEASUREMENT_ENTRY, True)
            wbc_count_measurement_records: List[DataRecord] = wbc_count_measurement_step.get_records()
            wbc_count_measurement_models: List[ELNExperimentDetailModel] = self.inst_man.add_existing_records_of_type(
                wbc_count_measurement_records, ELNExperimentDetailModel)
            measurement_number = 0
            for wbc_count_measurement_model in wbc_count_measurement_models:
                if wbc_count_measurement_model.get_field_value(self.MEASUREMENT_NUMBER_FIELD) > measurement_number:
                    measurement_number = wbc_count_measurement_model.get_field_value(self.MEASUREMENT_NUMBER_FIELD)
            if measurement_number == 0:
                if self.ENFORCE_WBC_COUNT_MEASUREMENT_TAG in wbc_count_measurement_step.get_options():
                    wbc_count_measurement_step.unlock_step()
                    hemo_cell_cue_entry = exp_handler.get_step(self.HEMO_CELL_CUE_COUNT_ENTRY)
                    disable_step(hemo_cell_cue_entry.eln_entry, context)
                    return PopupUtil.ok_popup("Error", "No WBC count Measurements Were Recorded, please add "
                                                       "measurements")
                return PopupUtil.ok_popup("Warning", "No WBC count Measurements Recorded")
            sample_id_to_wbc_count = dict()
            for wbc_count_measurement_model in wbc_count_measurement_models:
                if wbc_count_measurement_model.get_field_value(self.MEASUREMENT_NUMBER_FIELD) == measurement_number:
                    sample_id_to_wbc_count[wbc_count_measurement_model.get_field_value(self.SAMPLE_ID_FIELD)] = \
                        wbc_count_measurement_model.get_field_value(self.WBC_COUNT_FIELD)

            # get the samples associated
            samples_step = exp_handler.get_step(self.SAMPLES_ENTRY, True)
            samples_records: List[DataRecord] = samples_step.get_records()
            samples: List[SampleModel] = self.inst_man.add_existing_records_of_type(samples_records, SampleModel)

            # populate wbc count field
            self.rel_man.load_children_of_type(samples, EDTABloodSamplesModel)
            for sample in samples:
                edta_blood_sample = sample.get_child_of_type(EDTABloodSamplesModel)
                edta_blood_sample.set_WBCCount_field(sample_id_to_wbc_count.get(sample.get_SampleId_field()))
            self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
