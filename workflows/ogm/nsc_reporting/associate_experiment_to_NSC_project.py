from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ProjectModel, ELNExperimentModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-839
Description : ELN Entry rule that adds the experiment to NSC Project
"""


class AssociateExperimentToNSCProject(OsloWebhookHandler):
    # Project Name
    NSC_PROJECT_NAME = "NSC Project"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        exp_handler: ExperimentHandler = ExperimentHandler(context)

        # get the project
        project = self.dr_man.query_data_records(ProjectModel.DATA_TYPE_NAME, ProjectModel.PROJECTNAME__FIELD_NAME
                                                 .field_name, [self.NSC_PROJECT_NAME]).result_list
        if not project:
            exp_handler.cancel_experiment()
            return PopupUtil.ok_popup("Error", "Unable to locate Project With Project Name : " + self.NSC_PROJECT_NAME +
                                      ". This is required to enforce reviews.")
        else:
            project = project.pop()
            projectModel: ProjectModel = self.inst_man.add_existing_record_of_type(project, ProjectModel)
            elnExperimentRecord = self.dr_man.query_data_records_by_id(ELNExperimentModel.DATA_TYPE_NAME,
                                                                       [context.eln_experiment.experiment_record_id]) \
                .result_list.pop()
            elnExperiment = self.inst_man.add_existing_record_of_type(elnExperimentRecord, ELNExperimentModel)
            projectModel.add_child(elnExperiment)
            self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
