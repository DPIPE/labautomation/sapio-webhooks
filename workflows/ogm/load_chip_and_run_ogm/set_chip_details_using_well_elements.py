import traceback
from typing import List, Dict

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil

from utilities.data_models import PlateModel, SampleModel, PlateDesignerWellElementModel
from utilities.utils import get_sorted_records_by_field
from utilities.webhook_handler import OsloWebhookHandler


class SetChipDetailsUsingWellElements(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        result: ClientCallbackResult = context.client_callback_result
        rec_handler = RecordHandler(context)

        if result:
            if isinstance(result, OptionDialogResult):
                if result.button_text is None or result.button_text == 'OK':
                    return SapioWebhookResult(True)
            if result.user_cancelled:
                return SapioWebhookResult(True)

        try:
            # Get existing plates (chips) from 3d plating step
            plating_step = self.exp_handler.get_step("Load Samples On Chip")

            # Throw error if plating step is not submitted yet
            if plating_step.is_available():
                raise Exception("Please submit 'Load Samples On Chip'.")

            # Get Sample Details
            sample_position_details_step = self.exp_handler.get_step("Sample Position Details")
            sample_detail_records = self.exp_handler.get_step_records(sample_position_details_step)
            if not sample_detail_records:
                return SapioWebhookResult(True)
            sample_details = self.inst_man.add_existing_records(sample_detail_records)
            sample_id_by_sample_detail = RecordModelUtil.map_model_by_field_value(sample_details, "SampleId")

            # Get Samples
            samples_step = self.exp_handler.get_step("Samples")
            sample_records = self.exp_handler.get_step_records(samples_step)
            if not sample_records:
                raise Exception("Sample records not found in the entry named 'Samples'")
            samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

            # Get plates ( chips )
            plates_in_entry: List[PlateModel] = PlateDesignerEntry(plating_step, self.exp_handler).get_plates(
                PlateModel)

            # Get plate well designer elements
            plate_id_to_well_elements = self.get_plate_well_elements(plates_in_entry, rec_handler)

            for plate_id, well_elements in plate_id_to_well_elements.items():
                for plate_well_element in well_elements:
                    # Get matching sample and sample detail
                    sample_detail = None
                    for sample_record in samples:
                        if sample_record.get_field_value(
                                "RecordId") == plate_well_element.get_SourceRecordId_field():
                            sample_detail = sample_id_by_sample_detail.get(sample_record.get_SampleId_field())
                            break

                    # Set chip name and position on chip
                    if sample_detail:
                        sample_detail.set_field_value("ChipName", plate_id)
                        sample_detail.set_field_value("PositionOnChip", plate_well_element.get_ColPosition_field())

            self.rec_man.store_and_commit()
            return SapioWebhookResult(True)
        except Exception as exception:
            self.logger.error(traceback.format_exc())
            return PopupUtil.display_ok_popup("Error", str(exception))

    def get_plate_well_elements(self, plates_in_entry, rec_handler):
        plate_id_to_well_elements : Dict[str, List[PlateDesignerWellElementModel]] = {}

        for plate in plates_in_entry:
            plate_well_elements = rec_handler.query_models(PlateDesignerWellElementModel,
                                                           PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                                           [plate.get_field_value("RecordId")])
            if plate_well_elements:
                plate_well_elements: List[PlateDesignerWellElementModel] = get_sorted_records_by_field(plate_well_elements,PlateDesignerWellElementModel.ROWPOSITION__FIELD_NAME.field_name)
                plate_well_elements: List[PlateDesignerWellElementModel] = get_sorted_records_by_field(plate_well_elements,PlateDesignerWellElementModel.COLPOSITION__FIELD_NAME.field_name)
                plate_id_to_well_elements[plate.get_PlateId_field()] = plate_well_elements

        return plate_id_to_well_elements
