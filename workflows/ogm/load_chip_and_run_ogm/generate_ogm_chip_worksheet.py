import traceback
from typing import Dict

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.instrument_utils import InstrumentUtils
from utilities.utils import get_current_date_in_format
from utilities.utils import get_sorted_records_by_field
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class GenerateOgmChipWorksheet(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        result: ClientCallbackResult = context.client_callback_result
        rec_handler = RecordHandler(context)

        if result:
            if isinstance(result, OptionDialogResult):
                if result.button_text is None or result.button_text == 'OK':
                    return SapioWebhookResult(True)
            if result.user_cancelled:
                return SapioWebhookResult(True)

        try:
            # Get existing plates (chips) from 3d plating step
            plating_step = self.exp_handler.get_step("Load Samples On Chip")

            # Throw error if plating step is not submitted yet
            if plating_step.is_available():
                raise Exception("Please submit 'Load Samples On Chip'.")

            # Get Sample Details
            sample_position_details_step = self.exp_handler.get_step("Sample Position Details")
            sample_detail_records = self.exp_handler.get_step_records(sample_position_details_step)
            if not sample_detail_records:
                return SapioWebhookResult(True)

            # Generate files
            file_dict: dict[str, bytes] = self.generate_files(context, sample_detail_records)

            # Add to attachment entry
            attach_file_to_entry(
                FileAttachmentParams(
                    context,
                    list(file_dict.keys())[0],
                    list(file_dict.values())[0],
                    "Generated OGM Chip Worksheet",
                    self.exp_handler,
                    self.inst_man
                )
            )

            # Send the files to the instrument OR download to browser
            return self.send_file_to_instrument(context, file_dict)

        except Exception as exception:
            self.logger.error(traceback.format_exc())
            return PopupUtil.display_ok_popup("Error", str(exception))

    def generate_files(self, context, sample_detail_records):
        current_date = get_current_date_in_format("%d%m%Y")
        file_dict: Dict[str, bytes] = {}

        # header
        csv_content = "Sample name,Chip name,Position on chip\n"

        sample_detail_records = get_sorted_records_by_field(sample_detail_records, "PositionOnChip")
        sample_detail_records = get_sorted_records_by_field(sample_detail_records, "ChipName")

        for sample_detail in sample_detail_records:
            position = sample_detail.get_field_value("PositionOnChip") if sample_detail.get_field_value("PositionOnChip") else ""
            chip_name = sample_detail.get_field_value("ChipName") if sample_detail.get_field_value("ChipName") else ""
            sample_name = sample_detail.get_field_value("OtherSampleId") if sample_detail.get_field_value("OtherSampleId") else  sample_detail.get_field_value("SampleId")

            # Add row to csv content
            row_data = sample_name + "," + chip_name + "," + position
            csv_content += row_data + "\n"

        file_name = f"OGM_Chip_Worksheet_{context.eln_experiment.notebook_experiment_id}_{current_date}.csv"
        file_data = csv_content.encode("utf-8")
        file_dict[file_name] = file_data

        return file_dict

    def send_file_to_instrument(self, context: SapioWebhookContext, file_datas: dict[str, bytes]) -> SapioWebhookResult:
        instrument_errors: dict[str, str] = {}
        failed_file_bridge_files: dict[str, bytes] = {}
        exp_id: int = context.eln_experiment.notebook_experiment_id

        # Get the instrument name.
        instrument_tracking_entry: ElnEntryStep = WorkflowUtils.get_instrument_tracking_entry(self.exp_handler)
        try:
            instrument_name: str = self.eln_man.get_data_records_for_entry(
                exp_id, instrument_tracking_entry.eln_entry.entry_id).result_list[0].get_field_value("InstrumentUsed")
        except Exception:
            # If there is no instrument selected, leave it blank.
            instrument_name: str = ""

        for key in file_datas.keys():
            try:
                if instrument_name:
                    FileBridge.write_file(context, instrument_name, key, file_datas[key])
                else:
                    raise Exception("Instrument name not found to transfer QuantStudio Input file")
            except Exception as e:
                failed_file_bridge_files[key] = file_datas[key]
                instrument_errors[key] = repr(e)

        if len(failed_file_bridge_files) > 0:
            return self.download_to_browser(context, instrument_name, failed_file_bridge_files, instrument_errors)
        return SapioWebhookResult(True, "Files successfully sent to FileBridge location.")

    def download_to_browser(self, context, instrument_name: str, file_datas: dict[str, bytes],
                            instrument_errors: dict[str, str]) -> SapioWebhookResult:
        # Write an error to the instrument error log
        for key in instrument_errors.keys():
            InstrumentUtils(context).log_instrument_error(instrument_name, key, instrument_errors[key])

        # Download the files to the user
        return FileUtil.write_files(file_datas)
