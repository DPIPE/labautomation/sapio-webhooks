import csv
import io

import openpyxl
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_bridge import FileBridge
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import OptionDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ELNExperimentModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.SureSelectXTLibraryPreparation.populate_mastermix_table import \
    MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED


class GenerateMastermixFiles(OsloWebhookHandler):
    # experiment detail fields
    CONSUMABLE_TYPE_FIELD = "ConsumableType"
    QUANTITY_FIELD = "ConsumableQty"

    # file bridge path for mastermix file
    MASTERMIX_FILES_FILE_BRIDGE_PATH = "MastermixFiles/"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # update fields on the entries
        experiment_handler = ExperimentHandler(context)
        samples_step = experiment_handler.get_step("Samples")
        sample_records = experiment_handler.get_step_records(samples_step)
        number_of_samples = len(sample_records)
        if number_of_samples > 24:
            request = OptionDialogRequest("Error", "Number Of Samples > 24", ["Ok"], 0, True)
            return SapioWebhookResult(True, client_callback_request=request)

        steps = experiment_handler.get_steps(MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED.keys())
        file_information = []
        for step in steps:
            experiment_detail_records = step.get_records()
            experiment_detail_models = self.inst_man.add_existing_records_of_type(
                    experiment_detail_records,
                    ELNExperimentModel
                    )

            file_information.append([step.get_name(), "µL"])

            for experiment_detail_model in experiment_detail_models:
                file_information.append(
                    [
                        experiment_detail_model.get_field_value(self.CONSUMABLE_TYPE_FIELD),
                        str(experiment_detail_model.get_field_value(self.QUANTITY_FIELD))
                    ]
                )

            file_information.append(
                [
                    "Total",
                    str(MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED[step.get_name()]["Total"][number_of_samples//6])
                ]
            )

            file_information.append([])

        excel_file: bytes = self.create_excel_file(file_information)

        file_name = context.eln_experiment.notebook_experiment_name.replace(" ", "_") + '_Mastermix.xlsx'

        # Write file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                excel_file,
                "MasterMix File",
                experiment_handler,
                self.inst_man
            )
        )

        # write file to user
        try:
            FileBridge.write_file(
                    context,
                    "oslo-filebridge",
                    self.MASTERMIX_FILES_FILE_BRIDGE_PATH + file_name,
                    excel_file
                    )

            return SapioWebhookResult(True)
        except:
            pass

        write_file_request: WriteFileRequest = WriteFileRequest(
                file_bytes=excel_file,
                file_path=file_name
                )
        return SapioWebhookResult(True, client_callback_request=write_file_request)

    def create_excel_file(self, lines: list[list[str]]) -> bytes:
        wb = openpyxl.Workbook()
        ws = wb.active

        header_highlight = openpyxl.styles.PatternFill(
            start_color="66FFCC",
            end_color="66FFCC",
            fill_type="solid"
        )

        next_row_header = True

        for idx, row in enumerate(lines, start=1):

            if next_row_header is True:
                for idy, cell_value in enumerate(row, start=1):
                    cell = ws.cell(row=idx, column=idy, value=cell_value)
                    cell.fill = header_highlight

                next_row_header = False
            elif row == []:
                next_row_header = True
            else:
                ws.append(row)

        output_file = io.BytesIO()
        wb.save(output_file)

        return output_file.getvalue()
