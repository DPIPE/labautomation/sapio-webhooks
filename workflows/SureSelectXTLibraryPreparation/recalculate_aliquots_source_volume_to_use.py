from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNSampleDetailModel
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler


class RecalculateAliquotsSourceVolumeToUse(OsloWebhookHandler):
    # entry tags
    SAMPLE_DETAILS_ENTRY_TAG = "ALIQUOT SAMPLE DETAILS TABLE"

    # sample detail fields
    SOURCE_VOLUME_TO_USE_FIELD = "SourceVolumeToUse"
    CONCENTRATION_FIELD = "SourceConcentration"
    TARGET_MASS_FIELD = "TargetMass"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        sample_detail_records = get_records_from_entry_with_option(context, self.SAMPLE_DETAILS_ENTRY_TAG)
        sample_details = self.inst_man.add_existing_records_of_type(sample_detail_records, ELNSampleDetailModel)
        for sample_detail in sample_details:
            if sample_detail.get_field_value(self.SOURCE_VOLUME_TO_USE_FIELD) > 30:
                sample_detail.set_field_value(self.SOURCE_VOLUME_TO_USE_FIELD, 30)
                sample_detail.set_field_value(self.TARGET_MASS_FIELD,
                                                     sample_detail.get_field_value(self.CONCENTRATION_FIELD)*30)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
