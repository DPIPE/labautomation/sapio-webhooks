from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, PlateModel
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler


class PlateSamples(OsloWebhookHandler):
    # entry tags
    ALIQUOT_SAMPLES_ENTRY_TAG = "ALIQUOT SAMPLES"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get samples
        sample_records = get_records_from_entry_with_option(context, self.ALIQUOT_SAMPLES_ENTRY_TAG)
        sample_models = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        self.rel_man.load_parents_of_type(sample_models, SampleModel)

        # set sample well positions
        for sample_model in sample_models:
            parent_sample = sample_model.get_parent_of_type(SampleModel)
            sample_model.set_RowPosition_field(parent_sample.get_RowPosition_field())
            sample_model.set_ColPosition_field(parent_sample.get_ColPosition_field())

        # plate samples
        plate_model = self.inst_man.add_new_record_of_type(PlateModel)

        # Create Plate and accession an ID
        accession_man = DataMgmtServer.get_accession_manager(context.user)
        pojo = AccessionSystemCriteriaPojo("Plate")
        pojo.initial_sequence_value = 1000
        ids = accession_man.accession_for_system(1, pojo).pop()
        plate_model.set_PlateId_field(ids)
        self.rec_man.store_and_commit()

        plate_model.add_children(sample_models)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
