from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.exceptions import SapioCriticalErrorException
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import OptionDialogRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentModel
from utilities.webhook_handler import OsloWebhookHandler

MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED = {
        "ERP (End repair)": {"Klenow DNA Polymerase": [15.2, 28.3, 41, 54.1],
                             "dNTP Mix": [12.1, 22.7, 33.5, 44.1],
                             "T4 DNA Polymerase": [7.6, 14.2, 20.5,	27],
                             "T4 Polynucleotid Kinase":[16.7, 31.2, 45.3, 59.8],
                             "10x End Repair Buffer": [75.8, 141.7, 207.3, 273.2],
                             "Nuclease free water": [266.7, 498.9, 730.9, 963.2],
                             "Total": [394, 737, 1079.6, 1422.8]},
        "ATL (Adenylate/A-tailing)": {"dATP": [9.1, 15.7, 22.3, 28.9],
                                      "10x Klenow Polymerase Buffer": [45.5, 78.5, 111.5, 144.5],
                                      "Nuclease free water": [100.1, 172.7, 245.3, 317.9],
                                      "Exo(-) Klenow": [27.3, 47.1, 66.9, 86.7],
                                      "Total": [182,314, 446, 578]},
        "LIG (Adapter Ligation)": {"Nuclease free water": [195.3, 356.9, 518.07, 679.68],
                                   "SureSelect Adapter Oligo Mix": [8 ,14.6, 21.13, 27.72],
                                   "5x T4 DNA Ligase Buffer": [79.7, 145.7, 211.3, 277.2],
                                   "T4 DNA Ligase": [12, 21.9, 32.1, 42.1],
                                   "Total": [295, 539, 783, 1026.8]},
        "PCR1 (Amplify 1)": {"Nuclease free water": [54.6, 94.2, 133.8, 173.4],
                             "100 mM dNTP Mix": [4.6, 7.9, 11.2, 14.5],
                             "SureSelect Primer": [11.4, 19.6, 27.7, 35.8],
                             "5x Herculase II Reaction Buffer": [9.1, 15.7, 22.3, 28.9],
                             "SureSelect ILM Indexing Pre-Capture PCRReverse Primer": [11.4, 19.6, 27.7, 35.8],
                             "Total": [182, 314, 447, 578]},
        "HYB Buffer (Hybridization buffer)": {"SureSelect Hyb 3": [32.5, 54.4, 76.2, 98.1],
                                              "SureSelect Hyb 2": [3.3, 5.5, 7.8, 10.1],
                                              "SureSelect Hyb 1": [81.2, 135.9, 190.6, 245.3],
                                              "SureSelect Hyb 4": [42.3, 70.8, 99, 127.5],
                                              "Total": [159, 267, 373.8, 481]},
        "HYB Mix (Hybridization mix)": {"Nuclease free water": [18.4, 30.8, 43.1, 55.5],
                                        "SureSelect XT Human All Exon v5 (-80°C)": [61.3, 102.5, 143.8, 185],
                                        "Rnase Block": [6.1, 10.3, 14.4, 18.5],
                                        "Hybridization Buffer Mixture": [159, 267, 373.8, 481],
                                        "Total": [245, 410, 575, 740]},
        "BLK (Block mix)": {"SureSelect Indexing Block 1": [50, 66.5, 82.9, 99.4],
                            "SureSelect ILM Indexing Block 3": [12, 16, 19.9, 23.8],
                            "SureSelect Block 2": [50, 66.5, 82.9, 99.4],
                            "Total": [112, 149, 185.9, 222.8]},
        "PCR2 (Amplify 2)": {"5x Herculase II Reaction Buffer": [72.6, 148.4, 214.4, 280.5],
                             "100 mM dNTP Mix": [3.6, 7.4, 10.6, 13.9],
                             "SureSelect ILM Indexing Post-Captur ForwardPCR Primer": [7.3, 14.8, 21.2, 27.8],
                             "Herculase II Fusion DNA Polymerase": [7.3, 14.8, 21.2, 27.8],
                             "Nuclease free water": [134.3, 274.5, 396.3, 518.4],
                             "Total": [225, 460, 663.8, 868.4]}
    }


class PopulateMastermixTables(OsloWebhookHandler):
    CONSUMABLE_TYPE_FIELD = "ConsumableType"
    QUANTITY_FIELD = "ConsumableQty"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # get entries
        experiment_handler = ExperimentHandler(context)
        samples_step = experiment_handler.get_step("Samples")
        sample_records = experiment_handler.get_step_records(samples_step)
        number_of_samples = len(sample_records)
        if number_of_samples > 24:
            request = OptionDialogRequest("Error", "Number Of Samples > 24", ["Ok"], 0, True)
            return SapioWebhookResult(True, client_callback_request=request)

        # update fields on the entries
        steps = experiment_handler.get_steps(MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED.keys())
        for step in steps:
            experiment_detail_records = step.get_records()
            experiment_detail_models = self.inst_man.add_existing_records_of_type(experiment_detail_records,
                                                                                  ELNExperimentModel)
            for experiment_detail_model in experiment_detail_models:
                type = experiment_detail_model.get_field_value(self.CONSUMABLE_TYPE_FIELD)
                experiment_detail_model.set_field_value(self.QUANTITY_FIELD,
                                                        MATERIAL_TRACKING_ENTRY_NAMES_TO_TYPE_AND_QUANTITY_USED[step
                                                        .get_name()][type][number_of_samples//6])

        # commit
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
