import csv
import io

from sapiopycommons.files.file_bridge import FileBridge
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler


class GenerateRobotFiles(OsloWebhookHandler):
    # entry tags
    ALIQUOT_SAMPLES_ENTRY_TAG = "ALIQUOT SAMPLES"

    # filebridge path for robot file
    ROBOT_FILES_FILE_BRIDGE_PATH = "RobotFiles/"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        # get samples
        sample_records = get_records_from_entry_with_option(context, self.ALIQUOT_SAMPLES_ENTRY_TAG)
        sample_models = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get file data
        file_information = [["SamplePlate", "Volume"]]
        for sample_model in sample_models:
            file_information.append([sample_model.get_RowPosition_field()+sample_model.get_ColPosition_field(),
                                     sample_model.get_Volume_field()])

        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer)
            csv_writer.writerows(file_information)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()

        file_name = f"SureSelect XT Lib Prep Aliquot {context.eln_experiment.notebook_experiment_id}.xls"

        # Add the generated file to the workflow
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(csv_bytes, "utf-8"),
                "Generated Hamilton File",
                None,
                self.inst_man
            )
        )

        # write file to user
        try:
            FileBridge.write_file(
                context,
                "oslo-filebridge",
                self.ROBOT_FILES_FILE_BRIDGE_PATH + file_name,
                csv_bytes
            )
            return SapioWebhookResult(True)
        except:
            pass

        return SapioWebhookResult(
            True,
            client_callback_request=WriteFileRequest(
                    file_bytes=csv_bytes.encode('UTF-8'),
                    file_path=file_name
                )
        )
