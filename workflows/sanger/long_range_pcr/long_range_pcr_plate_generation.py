import json
from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, PlateModel, PrimerSampleModel, PlateDesignerWellElementModel, \
    AssignedProcessModel
from utilities.plating_utils import PlatingUtils
from utilities.sample_utils import SampleUtils
from utilities.utils import initialize_entry
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"


class LongRangePCRPlateGeneration(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    acc_manager: AccessionManager
    response_data: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.response_data = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_data = json.loads(result.callback_context_data)
            if self.response_data[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # Get the samples from the entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models(context.active_step.get_name(), SampleModel)
        if not samples:
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "There are no samples in the current step to do auto-plating.",
                                      request_context=json.dumps(self.response_data))

        # Update the child Primer Samples for each of the aliquots. If an aliquot doesn't have a Primer Sample, create
        # one for it. Also, create a unique set of amplicon values from all the Primer Samples.
        amplicons, primerless_samples = self.get_amplicons(samples)

        # If any of the parent samples didn't have Primer Samples, then error to the user.
        if len(primerless_samples) > 0:
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.table_popup("Error", "The following samples do not have Primer Samples. All samples must"
                                                  " have Primer Samples in order for plating to occur.",
                                         [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID")],
                                         primerless_samples, request_context=json.dumps(self.response_data))

        # Check if len(samples) + len(amplicons) > 96 (everything doesn't fit onto one layer of the plate.) If so,
        # throw an error to the user and don't plate anything.
        if len(samples) + len(amplicons) > len(Constants.PLATE_LOCATIONS):
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "The total sample count (including blank controls) exceeds the plate"
                                               " size. This is not supported, so plate will not be created.",
                                      request_context=json.dumps(self.response_data))

        # Create control samples for each of the amplicon values
        controls: list[SampleModel] = self.create_controls(amplicons)

        # Create the plate and plate the samples and controls.
        current_process: AssignedProcessModel = SampleUtils.get_current_process(samples[0], self.rel_man)
        process_name = current_process.get_ProcessName_field()
        PlatingUtils.auto_plate_samples_and_controls(self, samples, controls, process_name)

        # Add the samples to the result table
        pick_file_entry = self.exp_handler.get_step(PlatingUtils.PICK_FILE_ENTRY, True)
        pick_file_entry.add_records([s.get_data_record() for s in samples + controls])
        initialize_entry(pick_file_entry.eln_entry, context)


        return SapioWebhookResult(True, "Successfully created samples and controls.")

    def get_amplicons(self, samples) -> (list[str], list[dict[str, str]]):
        primerless_samples: list[dict[str, str]] = []

        self.rel_man.load_children_of_type(samples, PrimerSampleModel)
        self.rel_man.load_parents_of_type(samples, SampleModel)
        amplicons: set[str] = set()
        for s in samples:
            parent_primer_sample: PrimerSampleModel = (s.get_child_of_type(PrimerSampleModel))
            if not parent_primer_sample:
                primerless_samples.append({
                    "SampleId": s.get_SampleId_field()
                })
                continue
            primer_sample: PrimerSampleModel = s.get_child_of_type(PrimerSampleModel)
            if not primer_sample:
                primer_sample = self.rec_handler.add_model(PrimerSampleModel)
                s.add_child(primer_sample)
            primer_sample.set_Variant_field(parent_primer_sample.get_Variant_field())
            primer_sample.set_PCRProgram_field(parent_primer_sample.get_PCRProgram_field())
            primer_sample.set_Verification_field(parent_primer_sample.get_Verification_field())
            primer_sample.set_Amplicon_field(parent_primer_sample.get_Amplicon_field())
            amplicons.add(parent_primer_sample.get_Amplicon_field())

        return list(amplicons), primerless_samples

    def create_controls(self, amplicons: set[str]) -> list[SampleModel]:
        controls: list[SampleModel] = []
        control_sample_ids: list[str] = (
            self.acc_manager.accession_for_system(len(amplicons),
                                                  AccessionSystemCriteriaPojo(Constants.ACCESSIONING_CONTROL)))
        for x in range(len(amplicons)):
            control: SampleModel = self.rec_handler.add_model(SampleModel)
            control.set_SampleId_field(f"{Constants.ACCESSIONING_CONTROL}{control_sample_ids[x]}")
            control.set_IsControl_field(True)
            control.set_ControlType_field(Constants.CONTROL_TYPE_BLANK)
            control.set_Volume_field("2.000")
            control.set_VolumeUnits_field("µL")
            controls.append(control)

        return controls
