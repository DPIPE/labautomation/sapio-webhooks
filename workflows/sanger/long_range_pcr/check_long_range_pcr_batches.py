import json

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnExperimentStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import AssignedProcessModel, PrimerSampleModel, RequestModel
from utilities.data_models import SampleModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_INVALID_PROGRAMS: str = "invalidPrograms"

WORKFLOW_NAME: str = "Long Range PCR"
SAMPLES_ENTRY_NAME: str = "Samples"


class CheckLongRangePCRBatches(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        self.response_map = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_INVALID_PROGRAMS:
                return self.send_samples_back_to_queue(context)

        # Get each PCR program of the primers queued for the experiment.
        samples: list[SampleModel] = self.exp_handler.get_step_models(SAMPLES_ENTRY_NAME, SampleModel)
        self.rel_man.load_children_of_type(samples, PrimerSampleModel)

        # Check to make sure each PCR program value is the same. If they're not, cancel the experiment and notify the
        # user.
        invalid_programs: bool = False
        try:
            program_base_test: str = samples[0].get_child_of_type(PrimerSampleModel).get_PCRProgram_field()
            for s in samples:
                if s.get_child_of_type(PrimerSampleModel).get_PCRProgram_field() != program_base_test:
                    invalid_programs = True
                    break
        except Exception:
            invalid_programs = True

        if invalid_programs:
            self.response_map[STATUS] = STATUS_INVALID_PROGRAMS
            return PopupUtil.ok_popup("Error", "Not all samples have the same PCR program values. This experiment will"
                                               " be cancelled and the samples will be re-queued.",
                                      request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)

    def send_samples_back_to_queue(self, context: SapioWebhookContext) -> SapioWebhookResult:
        samples: list[SampleModel] = self.exp_handler.get_step_models(SAMPLES_ENTRY_NAME, SampleModel)
        self.rel_man.load_parents_of_type(samples, AssignedProcessModel)

        for s in samples:
            # Set the status of the sample.
            statuses: list[str] = s.get_ExemplarSampleStatus_field().split(",")
            new_statuses: list[str] = []
            for status in statuses:
                if WORKFLOW_NAME not in status and status not in new_statuses:
                    new_statuses.append(status)

            s.set_ExemplarSampleStatus_field(",".join(new_statuses))

            # Set the status of the sample's respective assigned process
            assigned_processes: list[AssignedProcessModel] = s.get_parents_of_type(AssignedProcessModel)
            for a in assigned_processes:
                if a.get_Status_field() is not None and WORKFLOW_NAME in a.get_Status_field():
                    a.set_Status_field("")
                    s.remove_parent(a)

        # Store and commit changes to remove the samples from the queue
        self.rec_man.store_and_commit()

        # Send the samples back to the queue
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        for s in samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, samples, "Sanger", 1,
                                              request=[t.get_parent_of_type(RequestModel) for t in top_level_samples if
                                                       t.get_SampleId_field() == s.get_TopLevelSampleId_field()][0])
        self.exp_handler.update_experiment(experiment_status=ElnExperimentStatus.Canceled)

        return SapioWebhookResult(True)
