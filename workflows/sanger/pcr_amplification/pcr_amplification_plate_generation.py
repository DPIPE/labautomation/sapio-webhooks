import json
from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, VariantResultModel, PrimerModel, PlateModel, PrimerSampleModel, \
    PlateDesignerWellElementModel
from utilities.plating_utils import PlatingUtils
from utilities.utils import initialize_entry
from utilities.variant_result_utils import VariantResultUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"


class PCRAmplificationPlateGeneration(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    acc_manager: AccessionManager
    response_data: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.response_data = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_data = json.loads(result.callback_context_data)
            if self.response_data[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # Get the samples from the entry.
        samples: list[SampleModel] = self.exp_handler.get_step_models(context.active_step.get_name(), SampleModel)
        if not samples:
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "There are no samples in the current step.",
                                      request_context=json.dumps(self.response_data))

        # Update the child Primer Samples for each of the aliquots. If an aliquot doesn't have a Primer Sample, create
        # one for it. Also, create a unique set of amplicon values from all the Primer Samples.
        amplicons_to_samples, primerless_samples = self.get_amplicons(samples)

        # If any of the parent samples didn't have Primer Samples, then error to the user.
        if len(primerless_samples) > 0:
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.table_popup("Error", "The following samples do not have Primer Samples. All samples must"
                                                  " have Primer Samples in order for plating to occur.",
                                         [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID")],
                                         primerless_samples, request_context=json.dumps(self.response_data))

            # Check if len(samples) + len(amplicons) > 96 (everything doesn't fit onto one layer of the plate.) If so,
        # throw an error to the user and don't plate anything.
        if len(samples) + len(list(amplicons_to_samples.keys())) > len(Constants.PLATE_LOCATIONS):
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "The total sample count (including blank controls) exceeds the plate"
                                               " size. This is not supported, so plate will not be created.",
                                      request_context=json.dumps(self.response_data))

        # Create control samples for each of the amplicon values
        controls: list[SampleModel] = self.create_controls(set(amplicons_to_samples.keys()))

        # Create the plate and plate the samples and controls.
        self.plate_samples_and_controls(amplicons_to_samples, controls, "Sanger")

        # Add the samples to the result table
        biomek_file_entry = self.exp_handler.get_step("Generate Biomek Files", True)
        biomek_file_entry.add_records([s.get_data_record() for s in samples + controls])
        initialize_entry(biomek_file_entry.eln_entry, context)

        # Add the controls to the biomek_file_entry.
        self.exp_handler.add_step_records(biomek_file_entry, controls)

        return SapioWebhookResult(True, "Successfully created samples and controls.")

    def get_amplicons(self, samples) -> (dict[str, list[SampleModel]], list[dict[str, str]]):
        amplicons_to_samples: dict[str, list[SampleModel]] = {}
        primerless_samples: list[dict[str, str]] = []

        self.rel_man.load_children_of_type(samples, PrimerSampleModel)
        self.rel_man.load_parents_of_type(samples, SampleModel)
        parent_samples: list[SampleModel] = []
        for s in samples:
            parent_samples.append(s.get_parent_of_type(SampleModel))
        self.rel_man.load_children_of_type(parent_samples, PrimerSampleModel)
        for s in samples:
            parent_primer_sample: PrimerSampleModel = (s.get_parent_of_type(SampleModel)
                                                       .get_child_of_type(PrimerSampleModel))
            if not parent_primer_sample:
                primerless_samples.append({
                    "SampleId": s.get_parent_of_type(SampleModel).get_SampleId_field()
                })
                continue
            primer_sample: PrimerSampleModel = s.get_child_of_type(PrimerSampleModel)
            if not primer_sample:
                primer_sample = self.rec_handler.add_model(PrimerSampleModel)
                s.add_child(primer_sample)
            primer_sample.set_Variant_field(parent_primer_sample.get_Variant_field())
            primer_sample.set_PCRProgram_field(parent_primer_sample.get_PCRProgram_field())
            primer_sample.set_Verification_field(parent_primer_sample.get_Verification_field())

            amplicon: str = parent_primer_sample.get_Amplicon_field()
            primer_sample.set_Amplicon_field(amplicon)
            if amplicon not in list(amplicons_to_samples.keys()):
                amplicons_to_samples[amplicon] = []
            amplicons_to_samples[amplicon].append(s)

        return amplicons_to_samples, primerless_samples

    def create_controls(self, amplicons: set[str]) -> list[SampleModel]:
        controls: list[SampleModel] = []
        control_sample_ids: list[str] = (
            self.acc_manager.accession_for_system(len(amplicons),
                                                  AccessionSystemCriteriaPojo(Constants.ACCESSIONING_CONTROL)))
        for x in range(len(amplicons)):
            control: SampleModel = self.rec_handler.add_model(SampleModel)
            control.set_SampleId_field(f"{Constants.ACCESSIONING_CONTROL}{control_sample_ids[x]}")
            control.set_IsControl_field(True)
            control.set_ControlType_field(Constants.CONTROL_TYPE_BLANK)
            controls.append(control)

        return controls

    def plate_samples_and_controls(self, amplicons_to_samples: dict[str, list[SampleModel]],
                                   controls: list[SampleModel], process_name : str )-> None:
        # Create the plate and add the samples and controls to it.
        plate: PlateModel = PlatingUtils.generate_plate_in_experiment(self.rec_handler, self.acc_manager, "Sanger")

        # Store and commit changes so that we get a plate ID generated.
        if "[WORKFLOW NAME]" in plate.get_PlateId_field():
            plate.set_PlateId_field(plate.get_PlateId_field().replace("[WORKFLOW NAME]", process_name))
        self.rec_man.store_and_commit()

        # Remove samples from existing plate if present
        all_samplemodels = [model for models in amplicons_to_samples.values() for model in models]
        self.rel_man.load_parents_of_type(all_samplemodels, PlateModel)
        for sample in all_samplemodels:
            if sample.get_parent_of_type(PlateModel):
                sample.remove_parent(sample.get_parent_of_type(PlateModel))
        self.rec_man.store_and_commit()

        # Add the samples to the plate while keeping samples grouped by amplicon, adding the control sample at the end
        # of the group.
        plate_wel_designer_elements: List[PlateDesignerWellElementModel] = self.inst_man.add_new_records_of_type(len(
            all_samplemodels) + (len(controls) if controls else 0), PlateDesignerWellElementModel)
        current_plate_index: int = 0
        ctrl_index: int = 0
        for amplicon in amplicons_to_samples:
            for sample in amplicons_to_samples[amplicon]:
                well_location: str = Constants.PLATE_LOCATIONS[current_plate_index]
                sample.set_RowPosition_field(well_location[0:1])
                sample.set_ColPosition_field(well_location[1:len(well_location)])
                sample.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                plate.add_child(sample)
                plate_wel_designer_elements[current_plate_index].set_PlateRecordId_field(plate.record_id)
                plate_wel_designer_elements[current_plate_index].set_Layer_field(1)
                plate_wel_designer_elements[current_plate_index].set_RowPosition_field(well_location[0:1])
                plate_wel_designer_elements[current_plate_index].set_ColPosition_field(well_location[1:len(well_location)])
                plate_wel_designer_elements[current_plate_index].set_SourceDataTypeName_field( SampleModel.DATA_TYPE_NAME)
                plate_wel_designer_elements[current_plate_index].set_SourceRecordId_field(sample.record_id)
                current_plate_index += 1
            well_location = Constants.PLATE_LOCATIONS[current_plate_index]
            controls[ctrl_index].set_RowPosition_field(well_location[0:1])
            controls[ctrl_index].set_ColPosition_field(well_location[1:len(well_location)])
            controls[ctrl_index].set_StorageLocationBarcode_field(plate.get_PlateId_field())
            plate.add_child(controls[ctrl_index])
            plate_wel_designer_elements[current_plate_index].set_PlateRecordId_field(plate.record_id)
            plate_wel_designer_elements[current_plate_index].set_Layer_field(1)
            plate_wel_designer_elements[current_plate_index].set_RowPosition_field(well_location[0:1])
            plate_wel_designer_elements[current_plate_index].set_ColPosition_field( well_location[1:len(well_location)])
            plate_wel_designer_elements[current_plate_index].set_SourceDataTypeName_field( SampleModel.DATA_TYPE_NAME)
            plate_wel_designer_elements[current_plate_index].set_SourceRecordId_field(controls[ctrl_index].record_id)
            plate_wel_designer_elements[current_plate_index].set_IsControl_field(True)
            current_plate_index += 1
            ctrl_index += 1
        self.rec_man.store_and_commit()

        # set up 3d plater to show this plate
        plate_entry = self.exp_handler.get_step(PlatingUtils.PLATES_ENTRY_NAME)
        options = plate_entry.get_options()
        if "MultiLayerPlating_Plate_RecordIdList" not in options:
            options["MultiLayerPlating_Plate_RecordIdList"] = ""
        options["MultiLayerPlating_Plate_RecordIdList"] = str(plate.record_id)
        plate_entry.set_options(options)
        initialize_entry(plate_entry.eln_entry, self.context)
        self.rec_man.store_and_commit()