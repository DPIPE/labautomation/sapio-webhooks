import json
from typing import cast, Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel, PrimerSampleModel, AssignedProcessModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_SAMPLE_SELECTION: str = "sampleSelection"
STATUS_ERROR: str = "error"

WORKFLOW_NAME: str = "PCR Amplification"


class AddSamplesToExperiment(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_SAMPLE_SELECTION:
                return self.process_sample_selection(context, cast(DataRecordSelectionResult, result))
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # Prompt the user to select any samples queued for the process. If there are no samples currently queued for
        # the process, notify the user.
        queued_samples: list[SampleModel] = (
            self.rec_handler.query_models(SampleModel, SampleModel.EXEMPLARSAMPLESTATUS__FIELD_NAME.field_name,
                                          [f"Ready for - {WORKFLOW_NAME}"]))
        if not queued_samples:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", f"There are currently no samples queued for {WORKFLOW_NAME}.",
                                      request_context=json.dumps(self.response_map))

        self.response_map[STATUS] = STATUS_SAMPLE_SELECTION
        return PopupUtil.record_selection_popup(context, "Select samples to add to the experiment.",
                                                [SampleModel.SAMPLEID__FIELD_NAME.field_name,
                                                 SampleModel.STORAGELOCATIONBARCODE__FIELD_NAME.field_name,
                                                 SampleModel.ROWPOSITION__FIELD_NAME.field_name,
                                                 SampleModel.COLPOSITION__FIELD_NAME.field_name],
                                                queued_samples,
                                                request_context=json.dumps(self.response_map))

    def process_sample_selection(self, context: SapioWebhookContext,
                                 result: DataRecordSelectionResult) -> SapioWebhookResult:
        # Get all the tube barcodes of the samples so that we can query for them.
        selections: list[dict[str, Any]] = result.selected_field_map_list
        if not selections:
            return SapioWebhookResult(True)
        sample_ids: list[str] = []
        for i, d in enumerate(selections):
            sample_ids.append(d[SampleModel.SAMPLEID__FIELD_NAME.field_name])

        # Add the selected samples to the experiment entry and update their sample statuses to the proper process
        # tracking value.
        selected_samples: list[SampleModel] = (
            self.rec_handler.query_models(SampleModel, SampleModel.SAMPLEID__FIELD_NAME.field_name, sample_ids))
        entry: ElnEntryStep = self.exp_handler.get_step(context.active_step.get_name())
        entry.add_records([s.get_data_record() for s in selected_samples])
        self.rel_man.load_parents_of_type(selected_samples, AssignedProcessModel)
        for s in selected_samples:
            statuses: list[str] = s.get_ExemplarSampleStatus_field().split(",")
            assigned_process: AssignedProcessModel = [a for a in s.get_parents_of_type(AssignedProcessModel)
                                                      if a.get_ProcessName_field() == "Sanger"
                                                      and a.get_ProcessStepNumber_field() == 2][0]
            assigned_process.set_Status_field(f"In Process - {WORKFLOW_NAME}")
            for x in range(len(statuses)):
                if statuses[x] == f"Ready for - {WORKFLOW_NAME}":
                    statuses[x] = f"In Process - {WORKFLOW_NAME}"
                    break
            s.set_ExemplarSampleStatus_field(",".join(statuses))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
