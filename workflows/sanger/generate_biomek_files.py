import json
import re

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PrimerModel
from utilities.data_models import PrimerSampleModel
from utilities.data_models import SampleModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.instrument_utils import InstrumentUtils
from utilities.webhook_handler import OsloWebhookHandler

FILE_HEADERS: list[str] = ["Sample_source_name", "Sample_source_pos", "Destination_plate_name",
                           "Destination_plate_pos", "Primer_rack_name", "Primer_rack_pos", "Mastermix_rack",
                           "Mastermix_rack_pos", "MQ_plate_name", "MQ_source_name", "MQ_plate_pos"]

FILE_ROW: str = ("[source rack],[source rack pos],[destination plate name],[destination position],"
                 "[name of primer rack from Verso],[position of primer rack],MM,1,MQ-[destination plate name],MQ,1")

PRIMER_FILE_NAME: str = "[Project Name]_Primer_MM_MQ_Biomek-robotil.csv"
VERSO_SAMPLE_FILE_NAME: str = "[Project Name]_Robotfil_2_gDNA_2D_Biomek.csv"
NON_VERSO_SAMPLE_FILE_NAME: str = "[Project Name]_Robotfil_2_gDNA_gamle_Biomek.csv"

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_DOWNLOAD: str = "fileDownload"


class GenerateBiomekFiles(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    instrument_utils: InstrumentUtils
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.instrument_utils = InstrumentUtils(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_FILE_DOWNLOAD:
                return SapioWebhookResult(True)

        # Get all the aliquots from the experiment and load their parent samples.
        step_samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Biomek Files", SampleModel)
        if not step_samples:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "There are no aliquots, so a Biomek file cannot be generated.",
                                      request_context=json.dumps(self.response_map))
        aliquots = [s for s in step_samples if not s.get_IsControl_field()]
        self.rel_man.load_parents_of_type(aliquots, SampleModel)

        # Map each parent sample to it's corresponding Primer.
        # OSLO-1021, we are not creating aliquots anymore
        parent_samples: list[SampleModel] = aliquots
        parent_samples_to_primers: dict[SampleModel, PrimerModel] = self.get_samples_to_primers(parent_samples)

        # Write data to each of the files.
        primer_file_data, verso_file_data, non_verso_file_data = self.write_file_data(aliquots,
                                                                                      parent_samples_to_primers)


        # Send the files to FileBridge or download them to the user.
        return self.send_files(context, primer_file_data, verso_file_data, non_verso_file_data,
                               aliquots[0].get_StorageLocationBarcode_field())

    def write_file_data(self, aliquots: list[SampleModel],
                        parent_samples_to_primers: dict[SampleModel, PrimerModel]) -> (str, str, str):
        primer_file_data: str = ",".join(FILE_HEADERS)
        verso_sample_file_data: str = ",".join(FILE_HEADERS)
        non_verso_sample_file_data: str = ",".join(FILE_HEADERS)

        # For each aliquot, add data to the correct files.
        verso_pattern = re.compile(r"^H[A-Z]\w+")
        for a in aliquots:
            # Get the parent sample of the aliquot. If there isn't one, just use blank values for any fields that stem
            # from the parent sample so that way we don't have to write multiple if or try/catch statements.
            parent_sample: SampleModel = a
            if not parent_sample:
                source_rack: str = ""
                source_rack_pos: str = ""
            else:
                source_rack: str = parent_sample.get_StorageLocationBarcode_field()
                source_rack_pos: str = f"{parent_sample.get_RowPosition_field()}{parent_sample.get_ColPosition_field()}"

            # Get the parent sample's corresponding Primer. If one doesn't exist, then just use blank values for the
            # same reasoning above.
            primer: PrimerModel = parent_samples_to_primers[parent_sample]
            if not primer:
                primer_rack: str = ""
                primer_rack_pos: str = ""
            else:
                primer_rack: str = primer.get_StorageLocationBarcode_field()
                primer_rack_pos: str = f"{primer.get_RowPosition_field()}{primer.get_ColPosition_field()}"

            primer_file_data += "\n" + (
                FILE_ROW.replace("[source rack]", source_rack)
                .replace("[source rack pos]", source_rack_pos)
                .replace("[destination plate name]", a.get_StorageLocationBarcode_field())
                .replace("[destination position]",
                         f"{a.get_RowPosition_field()}{a.get_ColPosition_field()}")
                .replace("[name of primer rack from Verso]", primer_rack)
                .replace("[position of primer rack]", primer_rack_pos)
                .replace("[destination plate name]", a.get_StorageLocationBarcode_field()))

            # If the parent sample came from a Verso fridge, add it to the Verso sample file. Otherwise, add it to the
            # non-Verso sample file.
            if verso_pattern.match(parent_sample.get_TubeBarcode_field()):
                verso_sample_file_data += "\n" + (
                    FILE_ROW.replace("[source rack]", source_rack)
                    .replace("[source rack pos]", source_rack_pos)
                    .replace("[destination plate name]", a.get_StorageLocationBarcode_field())
                    .replace("[destination position]",
                             f"{a.get_RowPosition_field()}{a.get_ColPosition_field()}")
                    .replace("[name of primer rack from Verso]", primer_rack)
                    .replace("[position of primer rack]", primer_rack_pos)
                    .replace("[destination plate name]", a.get_StorageLocationBarcode_field()))
            else:
                non_verso_sample_file_data += "\n" + (
                    FILE_ROW.replace("[source rack]", source_rack)
                    .replace("[source rack pos]", source_rack_pos)
                    .replace("[destination plate name]", a.get_StorageLocationBarcode_field())
                    .replace("[destination position]",
                             f"{a.get_RowPosition_field()}{a.get_ColPosition_field()}")
                    .replace("[name of primer rack from Verso]", primer_rack)
                    .replace("[position of primer rack]", primer_rack_pos)
                    .replace("[destination plate name]", a.get_StorageLocationBarcode_field()))

        return primer_file_data, verso_sample_file_data, non_verso_sample_file_data

    def send_files(self, context: SapioWebhookContext, primer_file_data: str, verso_file_data: str,
                   non_verso_file_data: str, project_name: str) -> SapioWebhookResult:
        # Send the files to the FileBridge connection, and if it fails, download them to the user.
        primer_file_name: str = PRIMER_FILE_NAME.replace("[Project Name]", project_name)
        verso_file_name: str = VERSO_SAMPLE_FILE_NAME.replace("[Project Name]", project_name)
        non_verso_sample_file_name: str = NON_VERSO_SAMPLE_FILE_NAME.replace("[Project Name]", project_name)

        file_dict: dict[str, bytes] = {}
        params: list[FileAttachmentParams] = []

        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", primer_file_name,
                                           primer_file_data.encode("utf-8"), "Biomek", self.instrument_utils):
            file_dict[primer_file_name] = primer_file_data.encode("utf-8")

        params.append(FileAttachmentParams(
                context,
                primer_file_name,
                bytes(primer_file_data, encoding="utf-8"),
                "Generated Primer File",
                self.exp_handler,
                self.inst_man
        ))

        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", verso_file_name,
                                           verso_file_data.encode("utf-8"), "Biomek", self.instrument_utils):
            file_dict[verso_file_name] = verso_file_data.encode("utf-8")
        params.append(FileAttachmentParams(
                context,
                verso_file_name,
                bytes(verso_file_data, encoding="utf-8"),
                "Generated Verso File (Instrument)",
                self.exp_handler,
                self.inst_man
        ))

        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", non_verso_sample_file_name,
                                           non_verso_file_data.encode("utf-8"), "Biomek", self.instrument_utils):
            file_dict[non_verso_sample_file_name] = non_verso_file_data.encode("utf-8")
        params.append(FileAttachmentParams(
                context,
                non_verso_sample_file_name,
                bytes(non_verso_file_data, encoding="utf-8"),
                "Generated Other File",
                self.exp_handler,
                self.inst_man
        ))

        # Add to attachment entries in the experiment
        for p in params:
            print(p)
            attach_file_to_entry(p)

        if len(list(file_dict.keys())) > 0:
            self.response_map[STATUS] = STATUS_FILE_DOWNLOAD
            return FileUtil.write_files(file_dict, request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)

    def get_samples_to_primers(self, samples: list[SampleModel]) -> dict[SampleModel, PrimerModel]:
        samples_to_primers: dict[SampleModel, PrimerModel] = {}
        self.rel_man.load_children_of_type(samples, PrimerSampleModel)
        variants: list[str] = [s.get_child_of_type(PrimerSampleModel).get_Variant_field() for s in samples]
        primers: list[PrimerModel] = (
            self.rec_handler.query_models(PrimerModel, PrimerModel.VARIANT__FIELD_NAME.field_name, variants))
        for s in samples:
            samples_to_primers[s] = [p for p in primers if p.get_Variant_field() ==
                                     s.get_child_of_type(PrimerSampleModel).get_Variant_field()][0]
        return samples_to_primers
