import json
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, PlateDesignerWellElementModel
from utilities.webhook_handler import OsloWebhookHandler


class UpdateSamplesPosition(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        # Get existing plates from 3d plating step
        plating_step = exp_handler.get_step(context.experiment_entry.entry_name)

        # Get the value for aliquoting in options.
        entry_options = plating_step.get_options()
        aliquot_disabled = False
        for option in entry_options:
            if option == 'MultiLayerPlating_Entry_PrePlating_Prefs':
                prefs = entry_options[option]
                prefs_dict = json.loads(prefs)
                aliquot_disabled: bool = prefs_dict.get('aliquotDisabled')

        if aliquot_disabled:
            # Grab the samples' position on the wells.
            plate_designer_entry = PlateDesignerEntry(plating_step, exp_handler)
            wells: list[PlateDesignerWellElementModel] = plate_designer_entry.get_plate_designer_well_elements(
                PlateDesignerWellElementModel)
            sample_ids: list[int] = []
            for well in wells:
                if well.get_SourceDataTypeName_field() == "Sample" and well.get_SourceRecordId_field():
                    sample_ids.append(well.get_SourceRecordId_field())
            sample_records = self.dr_man.query_data_records_by_id(SampleModel.DATA_TYPE_NAME.__str__(),
                                                                  sample_ids).result_list
            samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
            for well in wells:
                for sample in samples:
                    if well.get_SourceDataTypeName_field() == "Sample":
                        if well.get_SourceRecordId_field() and well.get_SourceRecordId_field() == sample.record_id:
                            sample.set_RowPosition_field(well.get_RowPosition_field())
                            sample.set_ColPosition_field(well.get_ColPosition_field())
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
