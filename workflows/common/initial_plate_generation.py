from typing import cast

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import SampleModel, AssignedProcessModel, InHouseModel
from utilities.plating_utils import PlatingUtils
from utilities.sample_utils import SampleUtils
from utilities.utils import initialize_entry
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


ELISA_REQUIRED_REAGENTS: list[str] = ["GPIHBP1-C1", "GPIHBP1-C2", "GPIHBP1-C3", "GPIHBP1-C4", "GPIHBP1-C5",
                                      "GPIHBP1-C6", "GPIHBP1-C7", "GPIHBP1-Pos ctrl"]


class InitialPlateGeneration(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    acc_manager: AccessionManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)

        # try:
        # Get the samples from the entry.
        samples_step = self.exp_handler.get_step("Samples", True)
        if not samples_step:
            self.callback.ok_dialog("Error", "'Samples' step not found'")
        samples: list[SampleModel] = self.exp_handler.get_step_models(samples_step, SampleModel)
        if not samples:
            self.callback.ok_dialog("Error", "There are no samples in 'Samples' step to do auto-plating.")

        non_controls = [s for s in samples if
                        s.get_ExemplarSampleType_field() != "Promega" and s.get_ExemplarSampleType_field() != "In House"
                        and not s.get_IsControl_field()]
        current_process: AssignedProcessModel = SampleUtils.get_current_process(non_controls[0], self.rel_man)
        if not current_process:
            raise Exception("Samples not linked to a process")
        process_name = current_process.get_ProcessName_field()

        # [OSLO-1120]: If we're in ELISA, then follow a different logic.
        if process_name == "ELISA":
            return self._handle_elisa(samples)

        # Add the samples to the result table
        pick_file_entry = self._add_samples_to_pick_files_entry(context, samples)

        # Create the plate and plate the samples and controls.
        PlatingUtils.auto_plate_samples_and_controls(self, samples, [], process_name)

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, eln_entry_refresh_list=[samples_step.eln_entry, pick_file_entry.eln_entry])

    def _add_samples_to_pick_files_entry(self, context, samples):
        pick_file_entry = self.exp_handler.get_step(PlatingUtils.PICK_FILE_ENTRY, True)
        pick_file_entry.add_records([s.get_data_record() for s in samples])
        initialize_entry(pick_file_entry.eln_entry, context)
        return pick_file_entry

    def _handle_elisa(self, samples: list[SampleModel]) -> SapioWebhookResult:
        in_house_entry: ElnEntryStep = self.exp_handler.get_step("In House Reagent Tracking")
        in_house_reagents: list[InHouseModel] = (
            WorkflowUtils.get_in_house_reagents_from_reagent_tracking_entry(self.user, self.rec_handler, self.inst_man,
                                                                            self.exp_handler, in_house_entry))

        # Check to make sure all of the required in-house reagents have been selected.
        in_house_reagents_types = [r.get_ConsumableType_field() for r in in_house_reagents]
        all_reagents_selected = True
        for reagent in ELISA_REQUIRED_REAGENTS:
            if reagent not in in_house_reagents_types:
                all_reagents_selected = False
                break

        if not all_reagents_selected:
            self.callback.ok_dialog("Error", "Please select all required reagents to proceed.")
            self.exp_handler.uninitialize_step(cast(ElnEntryStep, self.context.active_step))
            return SapioWebhookResult(True)
        
        self.rel_man.load_path_of_type(in_house_reagents,
                                       RelationshipPath().parent_type(SampleModel).child_type(InHouseModel))
        in_house_samples: list[SampleModel] = [x.get_parent_of_type(SampleModel) for x in in_house_reagents]

        # Create the plate and plate the samples and controls.
        try:
            PlatingUtils.auto_plate_samples_and_controls(self, samples, in_house_samples, "ELISA")
        except Exception as e:
            self.callback.ok_dialog("Error", f"An error occurred while auto-plating the samples.{str(e)}")
            self.exp_handler.uninitialize_step(cast(ElnEntryStep, self.context.active_step))
            # Raise the exception anyway so we can see the full error in the webhook logs.
            raise e

        return SapioWebhookResult(True)
