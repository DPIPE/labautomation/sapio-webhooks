from typing import cast

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.Protocols import ElnExperimentProtocol
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import (
    RecordModelInstanceManager,
)
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from utilities.data_models import ConsumableItemModel
from utilities.data_models import ddPCRAssayModel
from utilities.data_models import SampleModel
from utilities.data_models import VariantResultModel
from utilities.webhook_handler import OsloWebhookHandler


class StandardDilutionCreator(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        experiment_handler = ExperimentHandler(context)
        self.rec_handler = RecordHandler(context)

        # Get the sample step
        samples_step: ElnEntryStep = experiment_handler.get_step("Samples")

        # Get the variant records that are children of the samples
        samples_records: list[DataRecord] = experiment_handler.get_step_records(samples_step)
        sample_models: list[SampleModel] = self.inst_man.add_existing_records_of_type(
            samples_records, SampleModel
        )
        variant_models: list[VariantResultModel] = []

        self.rel_man.load_children_of_type(sample_models, VariantResultModel)
        for sample in sample_models:
            variant_models.extend(sample.get_children_of_type(VariantResultModel))

        ddpcr_assay_models: list[ddPCRAssayModel] = []

        # Get the ddPCR Assay records and their positive control records
        for variant_model in variant_models:
            ddPcrAssays = self.rec_handler.query_models(
                ddPCRAssayModel,
                ddPCRAssayModel.VARIANT__FIELD_NAME.field_name,
                [variant_model.get_Variant_field()],
            )

            if ddPcrAssays:
                ddpcr_assay_models.append(ddPcrAssays[0])

        positive_control_models = self.get_positive_controls(ddpcr_assay_models)

        # Create the third dilution record
        standard_dilution_records = self.create_dilution_rec(positive_control_models)

        # Add the dilution record to the samples step
        experiment_handler.add_step_records(samples_step, standard_dilution_records)

        return SapioWebhookResult(
            True,
            display_text="Standard dilution record created and added to the samples step.",
        )

    def get_positive_controls(self, ddpcr_assay_models: list[ddPCRAssayModel]) -> list[SampleModel]:
        positive_control_models: list[SampleModel] = []

        for ddpcr_assay_model in ddpcr_assay_models:
            positive_control_record_id = ddpcr_assay_model.get_PositiveControl_field()

            if positive_control_record_id:
                positive_control_model = self.rec_handler.query_models_by_id(
                    ConsumableItemModel, [positive_control_record_id]
                )

                if positive_control_model:
                    self.rel_man.load_parents_of_type(positive_control_model, SampleModel)
                    sample_std = positive_control_model[0].get_parent_of_type(SampleModel)
                    if sample_std:
                        positive_control_models.append(sample_std)

        return positive_control_models

    def create_dilution_rec(self, positive_control_models: list[SampleModel]) -> list[SampleModel]:
        standard_dilution_records: list[SampleModel] = []

        for control in positive_control_models:
            standard_dilution_record: SampleModel = self.inst_man.add_new_record_of_type(SampleModel)
            standard_dilution_record.set_OtherSampleId_field("S3 Dilution")
            standard_dilution_record.set_Volume_field(2000.0)
            standard_dilution_record.set_ExemplarSampleType_field(
                control.get_ExemplarSampleType_field()
            )
            standard_dilution_records.append(standard_dilution_record)

        self.rec_man.store_and_commit()
        return standard_dilution_records
