import time

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln import ElnExperiment
from sapiopylib.rest.pojo.eln import ExperimentEntry
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnTableEntryUpdateCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import ElnExperimentDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnExperimentProtocol
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import ConsumableItemModel
from utilities.data_models import ddPCRAssayModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler
 
 
def fail(ctx: SapioWebhookContext) -> SapioWebhookResult:
    return SapioWebhookResult(False, display_text = "Control must be registered and linked to Assay first")


class LaunchBlockDilution(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_handler = RecordHandler(context)
        eln_manager: ElnManager = DataMgmtServer.get_eln_manager(context.user)

        current_assay: ddPCRAssayModel = rec_handler.wrap_models([context.data_record], ddPCRAssayModel)[0]

        try:
            self.rel_man.load_path_of_type([current_assay], RelationshipPath().forward_side_link("PositiveControl").parent_type(SampleModel))
        except:
            breakpoint()
            return fail(context)

        link = current_assay.get_forward_side_link("PositiveControl", ConsumableItemModel)
        if link is None:
            return fail(context)
            
        parent_sample = link.get_parent_of_type(SampleModel)
        exper: ElnExperiment = ExperimentHandler.create_experiment(context, "Block Dilution")
        first_entry: ExperimentEntry = eln_manager.get_experiment_entry_list(exper.notebook_experiment_id)[1]

        eln_manager.add_records_to_table_entry(exper.notebook_experiment_id, first_entry.entry_id, [parent_sample.get_data_record()])

        update_crit: ElnTableEntryUpdateCriteria = ElnTableEntryUpdateCriteria()
        update_crit.entry_status = ExperimentEntryStatus.Enabled
        update_crit.template_item_fulfilled_timestamp = time.time()
        eln_manager.update_experiment_entry(exper.notebook_experiment_id, first_entry.entry_id, update_crit)

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, directive=ElnExperimentDirective(exper.notebook_experiment_id))
