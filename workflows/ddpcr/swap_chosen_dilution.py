from typing import Any

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.aliases import FieldMap
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.ClientCallbackService import ClientCallback
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import ListDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import ListDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.User import SapioServerException
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class SwapChosenDilution(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_handler = RecordHandler(context)
        callback = ClientCallback(context.user)
        if context.data_record_list is None:
            return SapioWebhookResult(True)


        selected_sample: SampleModel = rec_handler.wrap_model(
            context.data_record_list[0],
            SampleModel
        )

        if not selected_sample.get_IsControl_field():
            return SapioWebhookResult(True, display_text="Please select a control sample")


        if context.client_callback_result is None:
            self.an_man.load_ancestors_of_type([selected_sample.backing_model], "Sample")
            ancestors: list[SampleModel] = list(
                self.an_man.get_ancestors_of_type(selected_sample.backing_model, "Sample")
            )
            if not len(ancestors):
                return SapioWebhookResult(True)

            self.rel_man.load_children_of_type(ancestors, SampleModel)
            children_of_root: list[PyRecordModel] = ancestors[0].get_children_of_type("Sample")
            record_ids: list[str] = [
                    rec.fields["OtherSampleId"] if rec.fields["OtherSampleId"] else rec.fields["SampleId"]
                    for rec in children_of_root]

            return SapioWebhookResult(True, client_callback_request= ListDialogRequest(
                    "Select new record",
                    False,
                    record_ids
                )
            )

        elif isinstance(context.client_callback_result, ListDialogResult):
            result: ListDialogResult = context.client_callback_result
            if result.selected_options_list is None:
                return SapioWebhookResult(True)

            try:
                new_sample: str = result.selected_options_list[0]
            except IndexError:
                return SapioWebhookResult(True)

            if new_sample == selected_sample.get_OtherSampleId_field():
                return SapioWebhookResult(True, display_text="Pre-existing sample selected")

            result = self.dr_man.query_data_records("Sample", "OtherSampleId", [new_sample]).result_list
            if result is None or not len(result):
                result = self.dr_man.query_data_records("Sample", "SampleId", [new_sample]).result_list

            new_sample_rec: DataRecord = result[0]
            experiment_id: int = context.eln_experiment.notebook_experiment_id
            entry_id: int = context.experiment_entry.entry_id

            context.eln_manager.remove_records_from_table_entry(
                experiment_id,
                entry_id,
                [selected_sample.record_id]
            )
            context.eln_manager.add_records_to_table_entry(experiment_id, entry_id, [new_sample_rec])
            return SapioWebhookResult(True)
