from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class RemoveDilutionFromSample(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)
        sample_models: list[SampleModel] = exp_handler.get_step_models(
                context.experiment_entry.entry_name,
                SampleModel
                )

        for smpl in sample_models:
            smpl.set_Volume_field(max(smpl.get_Volume_field() - 2, 0))

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)
