import csv
import io
from datetime import datetime

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.files.file_bridge import FileBridge
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import CustomReportCriteria
from sapiopylib.rest.pojo.CustomReport import RawReportTerm
from sapiopylib.rest.pojo.CustomReport import RawTermOperation
from sapiopylib.rest.pojo.CustomReport import ReportColumn
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import MultiFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ddPCRAssayModel
from utilities.data_models import ELNSampleDetailModel
from utilities.data_models import PatientModel
from utilities.data_models import PlateDesignerWellElementModel
from utilities.data_models import PlateModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.data_models import VariantResultModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-755, OSLO-756
Description : Button and rule to generate the master mix file and dilution worksheet
"""


class CreateDilutionAndMasterMixWorksheet(OsloWebhookHandler):
    NETWORK_PATH = "Files/"
    assay_conc_to_values = {
        "20x": [4.4, 1.1, 0.5, 6],
        "40x": [4.95, 0.55, 0.5, 6],
        "": [0, 0, 0, 0]
    }

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # load all necessary records
        diluted_samples, sample_id_to_diluted_sample_details, source_sample_record_ids_to_pdwe_fields, \
            variant_to_dd_pcr_model = self.__retrieve_necessary_fields(context)

        # generate dilution worksheet
        dilution_worksheet_list, dd_pcr_model_to_wells = self.__generate_dilution_worksheet_rows(
            diluted_samples, source_sample_record_ids_to_pdwe_fields, variant_to_dd_pcr_model,
            sample_id_to_diluted_sample_details)
        dilution_worksheet_bytes = self.__generate_csv_file(dilution_worksheet_list)

        # generate master mix worksheet
        master_mix_worksheet_list = self.__generate_master_mix_worksheet_rows(dd_pcr_model_to_wells)
        master_mix_worksheet_bytes = self.__generate_csv_file(master_mix_worksheet_list)

        # prepare request to provide to user
        # write file to user
        # Get the current date and time
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y%m%d_%H%M%S")
        dilution_file_name = f"Dilution_Worksheet_{formatted_datetime}.csv"
        master_mix_file_name = f"Master_Mix_Worksheet_{formatted_datetime}.csv"
        try:
            FileBridge.write_file(context, "oslo-filebridge", self.NETWORK_PATH + dilution_file_name,
                                  dilution_worksheet_bytes)
            FileBridge.write_file(context, "oslo-filebridge", self.NETWORK_PATH + master_mix_file_name,
                                  master_mix_worksheet_bytes)
            return SapioWebhookResult(True)
        except:
            pass

        files: dict[str, bytes] = {
            dilution_file_name: dilution_worksheet_bytes.encode('UTF-8'),
            master_mix_file_name: master_mix_worksheet_bytes.encode('UTF-8')
        }

        exp_handler = ExperimentHandler(context)
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                dilution_file_name,
                files[dilution_file_name],
                "Generated Dilution File",
                exp_handler,
                self.inst_man,
                self.rec_man
            )
        )

        attach_file_to_entry(
            FileAttachmentParams(
                context,
                master_mix_file_name,
                files[master_mix_file_name],
                "Generated Mastermix File",
                exp_handler,
                self.inst_man
            )
        )

        multi_file_request: MultiFileRequest = MultiFileRequest(files)
        return SapioWebhookResult(True, client_callback_request=multi_file_request)

    @staticmethod
    def __retrieve_pdwe_fields(context, plate_record_ids) -> dict:
        plate_record_ids = [str(x) for x in plate_record_ids]
        term = RawReportTerm(
            PlateDesignerWellElementModel.DATA_TYPE_NAME,
            PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
            RawTermOperation.EQUAL_TO_OPERATOR,
            "{" + ",".join(plate_record_ids) + "}",
            is_negated=False,
        )
        column_list = [
            ReportColumn(
                PlateDesignerWellElementModel.DATA_TYPE_NAME, PlateDesignerWellElementModel
                .SOURCERECORDID__FIELD_NAME.field_name, FieldType.LONG
            ),
            ReportColumn(
                PlateDesignerWellElementModel.DATA_TYPE_NAME, PlateDesignerWellElementModel
                .ROWPOSITION__FIELD_NAME.field_name, FieldType.STRING
            ),
            ReportColumn(
                PlateDesignerWellElementModel.DATA_TYPE_NAME, PlateDesignerWellElementModel
                .COLPOSITION__FIELD_NAME.field_name, FieldType.STRING
            )
        ]
        custom_report_manager = DataMgmtServer.get_custom_report_manager(context.user)
        request = CustomReportCriteria(column_list, term)
        result = custom_report_manager.run_custom_report(request)
        pdwes = result.result_table
        source_sample_record_ids_to_pdwe_fields = dict()
        for pdwe in pdwes:
            source_sample_record_ids_to_pdwe_fields[pdwe[0]] = pdwe[1:]
        return source_sample_record_ids_to_pdwe_fields

    def __retrieve_necessary_fields(self, context):
        exp_handler = ExperimentHandler(context)

        # get diluted samples
        diluted_samples_step = exp_handler.get_step("Diluted Samples")
        diluted_samples: [SampleModel] = self.inst_man.add_existing_records_of_type(
            exp_handler.get_step_records(diluted_samples_step), SampleModel)

        # sample details
        dilution_details_step = exp_handler.get_step("Dilution Details")
        dilution_sample_details: [ELNSampleDetailModel] = self.inst_man.add_existing_records_of_type(
            exp_handler.get_step_records(dilution_details_step), ELNSampleDetailModel)
        sample_id_to_diluted_sample_details: dict[str, ELNSampleDetailModel] = dict()
        for dilution_sample_detail in dilution_sample_details:
            sample_id_to_diluted_sample_details[dilution_sample_detail.get_SampleId_field()] = dilution_sample_detail

        # plates
        plate_setup_step = exp_handler.get_step("Plate Setup")
        plates: [PlateModel] = PlateDesignerEntry(plate_setup_step, exp_handler).get_plates(PlateModel)

        # pdwe fields
        source_sample_record_ids_to_pdwe_fields = self.__retrieve_pdwe_fields(context,
                                                                              [plate.record_id for plate in plates])

        # ddPCR assay
        dd_pcr_models: [ddPCRAssayModel] = self.inst_man.add_existing_records_of_type(
            self.dr_man.query_all_records_of_type(ddPCRAssayModel.DATA_TYPE_NAME).result_list, ddPCRAssayModel)
        variant_to_dd_pcr_model: dict[str, ddPCRAssayModel] = dict()
        for dd_pcr_model in dd_pcr_models:
            variant_to_dd_pcr_model[dd_pcr_model.get_Variant_field()] = dd_pcr_model

        return diluted_samples, sample_id_to_diluted_sample_details, source_sample_record_ids_to_pdwe_fields, \
            variant_to_dd_pcr_model

    def __generate_dilution_worksheet_rows(self, diluted_samples, source_sample_record_ids_to_pdwe_fields,
                                           variant_to_dd_pcr_model, sample_id_to_diluted_sample_details):
        dilution_worksheet_list = [["Well", "National Number", "Sample ID", "Gene", "Variant", "Assay ID",
                                    "DNA conc. (ng/uL)", "DNA vol. (uL)", "Buffer vol. (uL)"]]
        self.rel_man.load_parents_of_type(diluted_samples, SampleModel)
        self.an_man.load_ancestors_of_type([x.backing_model for x in diluted_samples], RequestModel.DATA_TYPE_NAME)
        requests = []
        dd_pcr_model_to_wells = dict()
        diluted_samples_to_request = dict()
        parent_samples = []
        for diluted_sample in diluted_samples:
            parent_sample = diluted_sample.get_parent_of_type(SampleModel)
            parent_samples.append(parent_sample)
            request = self.inst_man.wrap(self.an_man.get_ancestors_of_type(diluted_sample.backing_model,
                                                                           RequestModel.DATA_TYPE_NAME).pop(),
                                         RequestModel)
            requests.append(request)
            diluted_samples_to_request[diluted_sample] = request
        self.rel_man.load_parents_of_type(requests, PatientModel)
        self.rel_man.load_children_of_type(parent_samples, VariantResultModel)
        for diluted_sample in diluted_samples:
            parent_sample = diluted_sample.get_parent_of_type(SampleModel)
            request = diluted_samples_to_request[diluted_sample]
            patient = request.get_parent_of_type(PatientModel)
            variant = parent_sample.get_children_of_type(VariantResultModel).pop()

            well = ""
            if parent_sample.record_id in source_sample_record_ids_to_pdwe_fields:
                well = "".join(source_sample_record_ids_to_pdwe_fields[parent_sample.record_id][0:2])

            national_number = ""
            if patient and patient.get_SocialSecurityNumber_field():
                national_number = patient.get_SocialSecurityNumber_field()

            sample_id = parent_sample.get_OtherSampleId_field()

            gene = ""
            variant_field = ""
            assay_id = ""
            if variant and variant.get_Variant_field() in variant_to_dd_pcr_model:
                dd_pcr_model = variant_to_dd_pcr_model[variant.get_Variant_field()]
                if dd_pcr_model.get_Gene_field():
                    gene = dd_pcr_model.get_Gene_field()
                if dd_pcr_model.get_Variant_field():
                    variant_field = dd_pcr_model.get_Variant_field()
                if dd_pcr_model.get_AssayId_field():
                    assay_id = dd_pcr_model.get_AssayId_field()
                if dd_pcr_model not in dd_pcr_model_to_wells:
                    dd_pcr_model_to_wells[dd_pcr_model] = 0
                dd_pcr_model_to_wells[dd_pcr_model] += 1

            conc = ""
            if parent_sample.get_Concentration_field():
                conc = str(parent_sample.get_Concentration_field())

            dna_vol = ""
            buff_vol = ""
            if parent_sample.get_SampleId_field() in sample_id_to_diluted_sample_details:
                sample_details = sample_id_to_diluted_sample_details[parent_sample.get_SampleId_field()]
                if sample_details.get_field_value("SourceVolumeToUse"):
                    dna_vol = str(sample_details.get_field_value("SourceVolumeToUse"))
                if sample_details.get_field_value("DiluentToUse"):
                    buff_vol = str(sample_details.get_field_value("DiluentToUse"))

            dilution_worksheet_list.append([well, national_number, sample_id, gene, variant_field, assay_id,
                                            conc, dna_vol, buff_vol])
        return dilution_worksheet_list, dd_pcr_model_to_wells

    def __generate_master_mix_worksheet_rows(self, dd_pcr_model_to_wells):
        i = 0
        master_mix_worksheet_list = []
        for dd_pcr_model in dd_pcr_model_to_wells.keys():
            i += 1
            assay_conc = "20x"
            wells = dd_pcr_model_to_wells[dd_pcr_model] + 2
            master_mix_worksheet_list.append([f"Mastermix {i}"])
            master_mix_worksheet_list.append(["Assay: ", dd_pcr_model.get_AssayId_field()])
            master_mix_worksheet_list.append(["Assay concentrations: ", assay_conc])
            master_mix_worksheet_list.append(["Number of wells + 2 wells", wells])
            master_mix_worksheet_list.append(["", "For each well (ul):", "Total (ul):"])
            master_mix_worksheet_list.append(["dH2O:", self.assay_conc_to_values[assay_conc][0],
                                              self.assay_conc_to_values[assay_conc][0] * wells])
            master_mix_worksheet_list.append(["Assay:", self.assay_conc_to_values[assay_conc][1],
                                              self.assay_conc_to_values[assay_conc][1] * wells])
            master_mix_worksheet_list.append(["Enzyme:", self.assay_conc_to_values[assay_conc][2],
                                              self.assay_conc_to_values[assay_conc][2] * wells])
            master_mix_worksheet_list.append(["Total volume:", self.assay_conc_to_values[assay_conc][3],
                                              self.assay_conc_to_values[assay_conc][3] * wells])
            master_mix_worksheet_list.append([""])
        return master_mix_worksheet_list

    @staticmethod
    def __generate_csv_file(lines):
        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer)
            csv_writer.writerows(lines)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()
        return csv_bytes
