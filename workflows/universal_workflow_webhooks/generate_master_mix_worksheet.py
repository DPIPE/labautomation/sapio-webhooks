import json
from typing import TypeAlias

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ConsumableModel
from utilities.data_models import MasterMixIngredientModel
from utilities.data_models import PrimerSampleModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_DOWNLOAD: str = "fileDownload"


class GenerateMasterMixWorksheet(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_FILE_DOWNLOAD:
                return SapioWebhookResult(True)

        # Get all the master mixes in the entry.
        master_mixes: list[ConsumableModel] = self.get_master_mixes_from_entry(context, context.experiment_entry)
        if not master_mixes:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "There are no master mixes selected, so a worksheet will not be"
                                               " generated.", request_context=json.dumps(self.response_map))

        # Calculate how much of each ingredient is needed per master mix.
        ingredient_dict_t: TypeAlias = dict[str, dict[str, float]]
        ingredients_to_volumes: ingredient_dict_t = self.get_volumes_per_ingredients(
            context,
            context.experiment_entry,
            master_mixes
        )

        file_name: str = f"MASTER MIXES {context.eln_experiment.notebook_experiment_name}.csv",
        file_data: str = self.write_worksheet_data(ingredients_to_volumes)

        # Write to the attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(file_data, "utf-8"),
                "Generated Mastermix Worksheet",
                self.exp_handler,
                self.inst_man
            )
        )

        # Write the data and download the file to the user.
        self.response_map[STATUS] = STATUS_FILE_DOWNLOAD
        return FileUtil.write_file(
            file_name,
            file_data.encode("utf-8-sig"),
            request_context=json.dumps(self.response_map)
        )

    def get_master_mixes_from_entry(self, context: SapioWebhookContext,
                                    entry: ExperimentEntry) -> list[ConsumableModel]:
        reagent_records: list[DataRecord] = (
            context.eln_manager.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                           entry.entry_id)).result_list
        if not reagent_records:
            return None
        consumable_types: list[str] = [reagent.get_field_value("ConsumableType") for reagent in reagent_records]
        consumables: list[ConsumableModel] = list(set((
            self.rec_handler.query_models(ConsumableModel, ConsumableModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                                          consumable_types))))
        self.rel_man.load_children_of_type(consumables, MasterMixIngredientModel)
        master_mixes: list[ConsumableModel] = []
        for consumable in consumables:
            if consumable.get_children_of_type(MasterMixIngredientModel):
                master_mixes.append(consumable)
        return master_mixes

    def get_volumes_per_ingredients(self, context: SapioWebhookContext, entry: ExperimentEntry,
                                    master_mixes: list[ConsumableModel]) -> dict[str, dict[str, float]]:
        ingredients_to_volumes: dict[str, dict[str, float]] = {}
        primer_name: str = self.get_primer_name("Generate Pick Files for Samples")
        exp_details: list[DataRecord] = (
            context.eln_manager.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                           entry.entry_id)).result_list
        for exp_detail in exp_details:
            master_mix: ConsumableModel = [master_mix for master_mix in master_mixes
                                           if master_mix.get_ConsumableType_field() ==
                                           exp_detail.get_field_value("ConsumableType")][0]
            factor: float = (float(exp_detail.get_field_value("ConsumableQty")) /
                             master_mix.get_QuantityPerItem_field())
            ingredients_to_volumes[master_mix.get_ConsumableType_field()] = {}
            for ingredient in master_mix.get_children_of_type(MasterMixIngredientModel):
                ingredient_name: str = ingredient.get_ConsumableType_field()
                if ingredient_name == "Primer":
                    ingredient_name = f"Primer ({primer_name})"
                ingredients_to_volumes[master_mix.get_ConsumableType_field()][ingredient_name] = (
                        ingredient.get_Quantity_field() * factor)
        return ingredients_to_volumes

    def get_primer_name(self, samples_step_name: str) -> str:
        sample: SampleModel = self.exp_handler.get_step_models(samples_step_name, SampleModel)[0]
        self.rel_man.load_children_of_type([sample], PrimerSampleModel)
        return sample.get_child_of_type(PrimerSampleModel).get_PCRProgram_field()

    def write_worksheet_data(self, ingredients_to_volumes: dict[str, dict[str, float]]) -> str:

        micro_liter = "µL"
        file_data: str = ""
        for master_mix_name in ingredients_to_volumes:
            total_volume: float = 0.0
            file_data += master_mix_name + "\n"
            for ingredient_name in ingredients_to_volumes[master_mix_name]:
                file_data += ingredient_name + ","
            file_data += "\n"
            for ingredient_name in ingredients_to_volumes[master_mix_name]:
                volume: float = ingredients_to_volumes[master_mix_name][ingredient_name]
                file_data += f"{volume} {micro_liter},"
                total_volume += volume
            file_data += f"\nTotal,{total_volume} {micro_liter}\n\n"
        return file_data
