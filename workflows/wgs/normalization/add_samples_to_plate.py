import json
from typing import Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import PlateModel, SampleModel, RequestModel, FamilyModel, PlateDesignerWellElementModel, \
    FamilyDetailsModel, WGSAssayDetailModel
from utilities.family_utils import FamilyUtils
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

plate_locations: list[str] = [
    'A:1', 'B:1', 'C:1', 'D:1', 'E:1', 'F:1', 'G:1', 'H:1',
    'A:2', 'B:2', 'C:2', 'D:2', 'E:2', 'F:2', 'G:2', 'H:2',
    'A:3', 'B:3', 'C:3', 'D:3', 'E:3', 'F:3', 'G:3', 'H:3',
    'A:4', 'B:4', 'C:4', 'D:4', 'E:4', 'F:4', 'G:4', 'H:4',
    'A:5', 'B:5', 'C:5', 'D:5', 'E:5', 'F:5', 'G:5', 'H:5',
    'A:6', 'B:6', 'C:6', 'D:6', 'E:6', 'F:6', 'G:6', 'H:6',
    'A:7', 'B:7', 'C:7', 'D:7', 'E:7', 'F:7', 'G:7', 'H:7',
    'A:8', 'B:8', 'C:8', 'D:8', 'E:8', 'F:8', 'G:8', 'H:8',
    'A:9', 'B:9', 'C:9', 'D:9', 'E:9', 'F:9', 'G:9', 'H:9',
    'A:10', 'B:10', 'C:10', 'D:10', 'E:10', 'F:10', 'G:10', 'H:10',
    'A:11', 'B:11', 'C:11', 'D:11', 'E:11', 'F:11', 'G:11', 'H:11',
    'A:12', 'B:12', 'C:12', 'D:12', 'E:12', 'F:12', 'G:12', 'H:12'
]


class AddSamplesToPlate(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    response_data: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.response_data = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            self.response_data = json.loads(result.callback_context_data)
            if self.response_data["status"] == "concentration":
                return SapioWebhookResult(True)
            if self.response_data["status"] == "error":
                return SapioWebhookResult(True)

        # If the entry has already been submitted, tell the user that they can't plate the samples again
        status: ExperimentEntryStatus = self.exp_handler.get_step(context.active_step.get_name()).eln_entry.entry_status
        if status == ExperimentEntryStatus.Completed or status == ExperimentEntryStatus.CompletedApproved:
            self.response_data["status"] = "error"
            return PopupUtil.ok_popup("Warning", "The entry has already been completed.",
                                      request_context=json.dumps(self.response_data))

        # Get the plate record
        plate: PlateModel = self.get_plate()

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Group each sample onto the plate
        return self.group_samples(plate, samples)

    def get_plate(self) -> PlateModel:
        plate_entry: ElnEntryStep = self.exp_handler.get_step("Setup Plate")
        record_id: int = int(plate_entry.get_options()["MultiLayerPlating_Plate_RecordIdList"])

        return self.rec_handler.query_models_by_id(PlateModel, [record_id])[0]

    def group_samples(self, plate: PlateModel, samples: list[SampleModel]) -> SapioWebhookResult:
        # Remove any existing well assignments to avoid layers
        existing_wells: list[PlateDesignerWellElementModel] = (
            self.rec_handler.query_models(PlateDesignerWellElementModel,
                                          PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                          [plate.record_id]))
        if existing_wells:
            for e in existing_wells:
                e.delete()
            self.rec_man.store_and_commit()

        # Sort the list of samples by priority
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        SampleUtils.sort_samples_by_priority(samples, self.rec_handler, self.rel_man, WGSAssayDetailModel,
                                             top_level_samples)

        # Get all the families of each sample
        family_map: dict[FamilyModel, list[SampleModel]] = FamilyUtils.get_family_map(self.rec_handler, self.rel_man,
                                                                                      top_level_samples)

        # Place each sample onto the plate
        plated_sample_ids: list[str] = []
        pdwes: list[PlateDesignerWellElementModel] = []
        pl_count: int = 0
        new_column: bool = False
        self.rel_man.load_children_of_type(top_level_samples, FamilyDetailsModel)
        for s in samples:
            # Don't add the sample to the plate if it's already there
            if s.get_SampleId_field() in plated_sample_ids:
                continue

            # Split the row and column positions
            row_col: list[str] = self.get_row_col_positions(pl_count)

            # Create a well element record
            pdwe: PlateDesignerWellElementModel = self.create_well_element(s, plate)

            top_level_sample: SampleModel = [x for x in top_level_samples if
                                             x.get_SampleId_field() == s.get_TopLevelSampleId_field()][0]
            family_details: list[FamilyDetailsModel] = top_level_sample.get_children_of_type(FamilyDetailsModel)
            sample_family_ids: list[str] = [fd.get_FamilyId_field() for fd in family_details]
            sample_families: list[FamilyModel] = [f for f in family_map if f.get_FamilyId_field() in sample_family_ids]

            workflow_family_samples: list[SampleModel] = []
            for sample_family in sample_families:
                for x in samples:
                    top_level_sample: SampleModel = [t for t in top_level_samples if
                                                     t.get_SampleId_field() == x.get_TopLevelSampleId_field()][0]
                    if top_level_sample in family_map[sample_family]:
                        workflow_family_samples.append(x)

            family_samples_count: int = len(workflow_family_samples)
            if family_samples_count > 0:
                potential_wells: list[str] = []
                for x in range(pl_count, pl_count + (family_samples_count - 1)):
                    potential_wells.append(plate_locations[x])

                if family_samples_count == 3:
                    new_column = "H" in potential_wells[0] or "H" in potential_wells[1]
                elif family_samples_count == 2:
                    new_column = "H" in potential_wells[0]
                if new_column:
                    pl_count = self.start_new_column(pl_count)
                    row_col = self.get_row_col_positions(pl_count)

                # Place the first family sample (s)
                pdwe.set_RowPosition_field(row_col[0])
                pdwe.set_ColPosition_field(row_col[1])
                pdwes.append(pdwe)
                plated_sample_ids.append(s.get_SampleId_field())
                pl_count += 1

                # Add the other family members to the same column
                for x in workflow_family_samples:
                    if x.get_SampleId_field() != s.get_SampleId_field():
                        pdwe = self.create_well_element(x, plate)
                        row_col = self.get_row_col_positions(pl_count)
                        pdwe.set_RowPosition_field(row_col[0])
                        pdwe.set_ColPosition_field(row_col[1])
                        pdwes.append(pdwe)
                        plated_sample_ids.append(x.get_SampleId_field())
                        pl_count += 1

            else:
                pdwe.set_RowPosition_field(row_col[0])
                pdwe.set_ColPosition_field(row_col[1])
                pdwes.append(pdwe)
                plated_sample_ids.append(s.get_SampleId_field())
                pl_count += 1

            new_column = False

            if pl_count >= len(plate_locations):
                self.response_data["status"] = "error"
                return PopupUtil.ok_popup("Error", f"Plate {plate.get_PlateId_field()} does not have enough wells"
                                                   f"to contain all samples.",
                                          request_context=json.dumps(self.response_data))

        # Store and commit to get the record IDs of the aliquots
        self.rec_man.store_and_commit()

        # Show a table warning of too-low concentrations and their new values if there are any
        low_concentrations: list[dict[str, str]] = self.get_low_concentrations(pdwes)
        if len(low_concentrations) > 0:
            # Store and commit changes
            self.rec_man.store_and_commit()

            self.response_data["status"] = "concentration"
            return PopupUtil.table_popup("Warning", "The following well locations have too low of concentrations to the"
                                                    " maximum volume of 50 uL. The concentration values were"
                                                    " automatically recalculated and can be seen below.",
                                         [VeloxStringFieldDefinition("Warning", "WellLocation", "Well Location"),
                                          VeloxDoubleFieldDefinition("Warning", "Concentration", "Previous Concentration"),
                                          VeloxDoubleFieldDefinition("Warning", "NewConcentration", "New Concentration")],
                                         low_concentrations,
                                         request_context=json.dumps(self.response_data))

        return SapioWebhookResult(True)

    def create_well_element(self, sample: SampleModel, plate: PlateModel) -> PlateDesignerWellElementModel:

        pdwe: PlateDesignerWellElementModel = self.rec_handler.add_model(PlateDesignerWellElementModel)

        pdwe.set_PlateRecordId_field(plate.record_id)
        pdwe.set_Layer_field(1)

        pdwe.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
        pdwe.set_SourceRecordId_field(sample.record_id)

        if SampleUtils.is_control(sample):
            return pdwe

        # Calculate and set the normalization values for the sample. This is wrapped in a try-catch to prevent a
        # division by zero error ruining the entire plating process.
        try:
            target_mass: float = 1000
            target_vol: float = 50

            pdwe.set_SourceSampleMass_field(sample.get_TotalMass_field())

            if sample.get_TotalMass_field() < target_mass:
                target_mass = sample.get_TotalMass_field()

            pdwe.set_TargetMass_field(target_mass)

            src_conc: float = sample.get_Concentration_field()
            pdwe.set_SourceSampleConcentration_field(src_conc)

            src_vol_to_use: float = target_mass / src_conc
            pdwe.set_SourceVolumeToRemove_field(src_vol_to_use)

            pdwe.set_SourceSampleVolume_field(sample.get_Volume_field())

            pdwe.set_Volume_field(target_vol)

            target_conc: float = target_mass / target_vol
            pdwe.set_Concentration_field(target_conc)
        except Exception:
            ...

        return pdwe

    # noinspection PyMethodMayBeStatic
    def get_row_col_positions(self, pl_count) -> list[str]:
        return plate_locations[pl_count].split(":")

    # noinspection PyMethodMayBeStatic
    def start_new_column(self, pl_count) -> int:
        return (pl_count // 8 + 1) * 8

    # noinspection PyMethodMayBeStatic
    def get_low_concentrations(self, well_elements: list[PlateDesignerWellElementModel]) -> list[dict[str, str]]:
        # Check the normalization values of each sample, and warn for each normalization that needed greater than 50 uL
        # of volume to get the target mass
        low_concentrations: list[dict[str, str]] = []

        for pdwe in well_elements:
            try:
                src_vol_to_use: float = pdwe.get_SourceVolumeToRemove_field()
                if src_vol_to_use > 50:
                    prev_conc: float = pdwe.get_SourceSampleConcentration_field()
                    pdwe.set_SourceVolumeToRemove_field(50)
                    new_conc: float = pdwe.get_TargetMass_field() / 50
                    pdwe.set_Concentration_field(new_conc)

                    low_concentrations.append({
                        "WellLocation": f"{pdwe.get_RowPosition_field()}{pdwe.get_ColPosition_field()}",
                        "Concentration": prev_conc,
                        "NewConcentration": new_conc
                    })
            except Exception:
                continue

        return low_concentrations
