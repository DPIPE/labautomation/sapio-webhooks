from typing import List

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import FamilyModel, SampleModel
from utilities.family_utils import FamilyUtils
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket: OSLO-785
Author: Aryan
Description: If single sample is being sent to wgs, then route them to SNP-Id too
"""


class RouteSingleSamplesToSNPId(OsloWebhookHandler):
    # Entry Names
    SAMPLES_ENTRY = "Samples"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_handler = RecordHandler(context)
        exp_handler: ExperimentHandler = ExperimentHandler(context)
        callback_util = CallbackUtil(context)

        # get the samples
        samples_entry = exp_handler.get_step(self.SAMPLES_ENTRY, True)
        sample_records = samples_entry.get_records()
        samples: List[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # route samples as per condition
        error = []
        for s in samples:
            family_map: dict[FamilyModel, list[SampleModel]] = FamilyUtils.get_family_map(rec_handler,
                                                                                          self.rel_man,
                                                                                          [s])

            family_records: list[DataRecord] = [f.get_data_record() for f in family_map]
            families = rec_handler.inst_man.add_existing_records_of_type(family_records, FamilyModel)
            route_sample_to_snp_id = False
            for family in families:
                if family.get_FamilyCount_field() == 1 and family.get_SendAllSamplesToQueue_field():
                    route_sample_to_snp_id = True
                    break
            if route_sample_to_snp_id:
                try:
                    ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME,
                                                      [s.record_id], "SNP-ID", 1, None)
                except:
                    error.append(s.get_SampleId_field())

        # give user apt error message
        if error:
            callback_util.ok_dialog("Error", "Unable to route samples with following sample ids: " + ", ".join(error) +
                                    " to SNP-ID")
        return SapioWebhookResult(True)
