import datetime
import re
from re import Pattern
from io import BytesIO
from typing import AnyStr

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from xlwt import Workbook, Worksheet

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel, PlateDesignerWellElementModel, PlateModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.webhook_handler import OsloWebhookHandler

HEADERS: list[str] = ["Sample_Number", "Archive pos.", "Alt sample ID", "Sample conc.", "Labware", "Position_ID",
                      "Volume_DNA", "Volume_EB", "Destination_Well_ID", "Norm. conc."]


class GenerateHamiltonFile(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get the samples from the entry
        samples: list[SampleModel] = self.exp_handler.get_step_models("Normalized DNA Samples – Generation of robot file", SampleModel)
        if not samples:
            return SapioWebhookResult(False)

        # Get the plate designer well elements from the plate entry
        plate_id: str = samples[0].get_StorageLocationBarcode_field()
        plate: PlateModel = self.rec_handler.query_models(PlateModel, PlateModel.PLATEID__FIELD_NAME.field_name,
                                                          [plate_id])[0]
        pdwes: list[PlateDesignerWellElementModel] = (
            self.rec_handler.query_models(PlateDesignerWellElementModel,
                                          PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                          [plate.record_id]))

        # Write the sample data to the file
        verso_pattern: Pattern[AnyStr] = re.compile(r"^H[A-Z]\w+")
        workbook: Workbook = Workbook()
        sheet: Worksheet = workbook.add_sheet("Sheet1")
        self.write_hamilton_headers(sheet)
        index: int = 1
        self.rel_man.load_parents_of_type(samples, SampleModel)
        for p in pdwes:
            aliquots = [s for s in samples if s.record_id == p.get_AliquotSampleRecordId_field()]
            if not aliquots:
                continue
            aliquot: SampleModel = aliquots[0]
            source_sample: SampleModel = aliquot.get_parent_of_type(SampleModel)

            sheet.write(index, 0, source_sample.get_TubeBarcode_field())
            if verso_pattern.match(source_sample.get_TubeBarcode_field()):
                sheet.write(index, 1, "")
            else:
                sheet.write(index, 1, f"{source_sample.get_RowPosition_field()}{source_sample.get_ColPosition_field()}")

            sheet.write(index, 2, aliquot.get_OtherSampleId_field() if aliquot.get_OtherSampleId_field() else "?")
            sheet.write(index, 3, str(source_sample.get_Concentration_field()))

            # If the sample came from a Verso freezer, then set the Labware value to "Rack2DTubes." If the sample
            # doesn't have a tube barcode, just leave a blank value.
            if verso_pattern.match(source_sample.get_TubeBarcode_field()):
                sheet.write(index, 4, "Rack2DTubes")
            else:  # Otherwise, get the rack or shelf that the plate that the sample is in is on and use that
                sheet.write(index, 4, "Rack1")

            if source_sample.get_RowPosition_field() and source_sample.get_ColPosition_field():
                sheet.write(index, 5, f"{source_sample.get_RowPosition_field()}{source_sample.get_ColPosition_field()}")
            else:
                sheet.write(index, 5, "")
            sheet.write(index, 6, aliquot.get_Volume_field())
            try:    # This is wrapped in a try-catch since there is a possibility that this value won't be filled out.
                sheet.write(index, 7, str(50.0 - p.get_SourceVolumeToRemove_field()))
            except Exception:
                sheet.write(index, 7, "")

            sheet.write(index, 8, f"{p.get_RowPosition_field()}{p.get_ColPosition_field()}")
            sheet.write(index, 9, str(p.get_Concentration_field() if p.get_Concentration_field() else ""))
            index += 1

        file_name: str = f"Hamilton-{str(datetime.date.today())}.xls"
        buffer: BytesIO = BytesIO()
        workbook.save(buffer)

        # Add the generated file to the workflow
        params = FileAttachmentParams(
            self.context,
            file_name,
            buffer.getvalue(),
            "Generated DNA Samples File",
            self.exp_handler,
            self.inst_man
        )
        attach_file_to_entry(params)

        # Send the file to FileBridge, and if it fails, download it to the user
        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", file_name, buffer.getvalue(), ""):
            return FileUtil.write_file(file_name, buffer.getvalue())
        return SapioWebhookResult(True)

    def write_hamilton_headers(self, sheet: Worksheet) -> None:
        hamilton_col_index: int = 0
        for x in HEADERS:
            sheet.write(0, hamilton_col_index, x)
            hamilton_col_index += 1
