from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from utilities.data_models import SampleModel
from utilities.data_models import SequencingMetadataModel 
from utilities.utils import get_entry_by_option
from utilities.webhook_handler import OsloWebhookHandler

class InstrumentSelectionList(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_man = DataRecordManager(context.user)
        mod_man: RecordModelManager = RecordModelManager(context.user)
        instrument_records: list[DataRecord] = context.data_record_manager.query_all_records_of_type('Instrument').result_list

        # Get pool submitted to experiment
        sample_entry = get_entry_by_option(context, "FLOW CELL SAMPLES")
        given_pool = context.eln_manager.get_data_records_for_entry(
            context.eln_experiment.notebook_experiment_id, sample_entry.entry_id).result_list
        given_pool_models: list[SampleModel] = mod_man.instance_manager.add_existing_records_of_type(given_pool, SampleModel)

        mod_man.relationship_manager.load_parents_of_type(given_pool_models, SampleModel)

        # Get parent of the submitted pool and that parent's sequence metadata
        pure_pool: SampleModel = given_pool_models[0].get_parent_of_type(SampleModel)
        mod_man.relationship_manager.load_parents_of_type([pure_pool], SequencingMetadataModel)
        seq_metadata: SequencingMetadataModel = pure_pool.get_parent_of_type(SequencingMetadataModel)
        assert isinstance(seq_metadata, SequencingMetadataModel)

        # Get chosen sequencer type
        seq_type = seq_metadata.get_SequencerType_field()
        if seq_type is None:
            return SapioWebhookResult(True, "No sequencer set")

        illumina_instruments: list[DataRecord] = filter(
                lambda rec: seq_type == rec.get_field_value("InstrumentType"),
                instrument_records
                )
        illumina_selection: list[str] = [rec.get_field_value("InstrumentName") for rec in illumina_instruments]
        return SapioWebhookResult(True, "Success", list_values=illumina_selection)
