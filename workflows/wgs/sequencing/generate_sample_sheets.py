import csv
import functools
import io
from datetime import datetime
from typing import Dict
from typing import List
from typing import Optional

import openpyxl
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import ElnAttachmentEntryUpdateCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnBaseDataType
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import WriteFileResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.autopaging import GetAncestorsListAutoPager
from sapiopylib.rest.utils.autopaging import GetChildrenListAutoPager
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager

from utilities.data_models import SampleModel, AttachmentModel
from utilities.utils import get_entry_by_option
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler

class SampleSheetGenerator(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if isinstance(context.client_callback_result, WriteFileResult):
            return SapioWebhookResult(True)

        self.ancestor_map = None
        self.child_map = None

        # Make the context an object attribute
        self.context = context

        # Get the current user's full name
        user_full_name = f"{context.user.session_additional_data.first_name} {context.user.session_additional_data.last_name}"
        
        # Get the experiment name
        experiment: ElnExperiment = context.eln_experiment
        experiment_name = experiment.notebook_experiment_name
        
        # Get the current datetime
        run_date = datetime.now().strftime("%m/%d/%Y")
        
        # Determine the sequencer type
        sequencer_type = self.get_sequencer_type()
        
        if sequencer_type == "NovaSeq 6000":
            # Generate the NovaSeq 6000 sample sheet
            sample_sheet = self.generate_novaseq_6000_sample_sheet(user_full_name, experiment_name, run_date)
            self.attach_sample_sheet_to_entry(context, "SampleSheet.csv", sample_sheet)
            
            # Return the sample sheet as a file download
            return SapioWebhookResult(True, client_callback_request=WriteFileRequest(sample_sheet.encode(), "SampleSheet.csv"))
        elif sequencer_type == "NovaSeq X":
            sample_sheet = self.generate_novaseq_x_sample_sheet(experiment_name)
            writable_sheet = self.sheet_to_excel(sample_sheet)
            self.attach_sample_sheet_to_entry(context, "SampleSheet.xlsx", writable_sheet)
            return SapioWebhookResult(True, client_callback_request=WriteFileRequest(writable_sheet, "SampleSheet.xlsx"))

        else:
            return SapioWebhookResult(True, display_text=f"Sample sheet generation for {sequencer_type} is not yet supported.")
    

    def get_sequencer_type(self) -> str:
        # Find the parent Sequencing Metadata record for each pool in the experiment
        assignment_entry = get_records_from_entry_with_option(self.context, "FLOW CELL SAMPLES")
        sequencing_metadata_records: List[DataRecord] = []
        for pool_record in assignment_entry:
            ancestors = self.get_ancestors(pool_record, "SequencingMetadata")
            if ancestors:
                sequencing_metadata_records.append(ancestors[0])
        
        # Check the Sequencer Type field on the Sequencing Metadata records
        sequencer_type = sequencing_metadata_records[0].get_field_value("SequencerType")
        if sequencer_type:
            return sequencer_type
        
        return "Invalid type"
    
    def generate_novaseq_6000_sample_sheet(self, investigator_name: str, experiment_name: str, run_date: str) -> str:
        data_record_manager = DataMgmtServer.get_data_record_manager(self.context.user)
        rec_man: RecordModelManager = RecordModelManager(self.context.user)

        sample_sheet = [
            "[Header]",
            f"Investigator Name,{investigator_name}",
            f"Experiment Name,{experiment_name}",
            f"Date,{run_date}",
            "Workflow,GenerateFASTQ",
            "",
            "[Reads]",
            "151",
            "151",
            "[Data]",
            "Sample_ID,Sample_Name,Sample_Plate,Sample_Well,index,index2,Sample_Project,Description"
        ]

        # Get the samples submitted to the experiment (should be in format pool-111_1)
        sample_entry = get_entry_by_option(self.context, "FLOW CELL SAMPLES")
        # given pool is the base pool (ex pool-111)
        given_pool: list[DataRecord] = self.context.eln_manager.get_data_records_for_entry(
            self.context.eln_experiment.notebook_experiment_id, sample_entry.entry_id).result_list
        given_pool_models: list[SampleModel] = rec_man.instance_manager.add_existing_records_of_type(given_pool, SampleModel)

        # Get the library of samples from the parent sample (pool-110)
        rec_man.relationship_manager.load_parents_of_type(given_pool_models, SampleModel)
        pure_pool: SampleModel = given_pool_models[0].get_parent_of_type(SampleModel)

        rec_man.relationship_manager.load_parents_of_type([pure_pool], SampleModel)
        library_sample_models: list[SampleModel] = pure_pool.get_parents_of_type(SampleModel)
        library_samples = [rec.get_data_record() for rec in library_sample_models]

        # Get the assigned indexBarcode on the samples that created the pool
        # indexbarcode is added in the library prep stage
        index_records = [data_record_manager.get_children(child.get_record_id(), "IndexBarcode").result_list for child in library_samples]
        # reduce from 2d array to 1d
        index_records = functools.reduce(lambda x,y :x+y , index_records)

        for idx, pool_record in enumerate(library_samples):
            sample_id = pool_record.get_field_value("SampleId")
            sample_name = pool_record.get_field_value("OtherSampleId")
            storage_location = pool_record.get_field_value("StorageLocationBarcode")
            row_pos = pool_record.get_field_value("RowPosition")
            col_pos = pool_record.get_field_value("ColPosition")

            index_barcode = index_records[idx].get_field_value("IndexTag")
            index_parts = index_barcode.split("-")
            index = index_parts[0]
            index2 = index_parts[1] if len(index_parts) > 1 else ""

            sample_sheet.append(f"{sample_id},{sample_name},{storage_location},{row_pos}:{col_pos},{index},{index2},,")
        
        return "\n".join(sample_sheet)

    def generate_novaseq_x_sample_sheet(self, exper_name: str) -> list[str]:
        rec_man: RecordModelManager = RecordModelManager(self.context.user)

        sample_sheet = [
            "[Header]",
            "FileFormatVersion,2",
            f"RunName,{exper_name}",	
            "InstrumentPlatform,NovaSeqXSeries",
            "IndexOrientation,Forward",
            "",
            "[Reads]",
            "Read1Cycles,151",
            "Read2Cycles,151",
            "Index1Cycles,10",
            "Index2Cycles,10",
            "",
            "[Sequencing_Settings]",
            "LibraryPrepKits,IlluminaDNAPCRFree;TruSeqDNAPCRFree",
            "",
            "[BCLConvert_Settings]",
            "SoftwareVersion,4.1.23",
            "FastqCompressionFormat,dragen",
            "",
            "[BCLConvert_Data]",
            "Lane,Sample_ID,Index,Index2,OverrideCycles,AdapterRead1,AdapterRead2,BarcodeMismatchesIndex1,BarcodeMismatchesIndex2",
        ]

        sample_entry = get_entry_by_option(self.context, "FLOW CELL SAMPLES")
        # given pool is the base pool (ex pool-111)
        given_pool: list[DataRecord] = self.context.eln_manager.get_data_records_for_entry(
            self.context.eln_experiment.notebook_experiment_id, sample_entry.entry_id).result_list
        given_pool_models: list[SampleModel] = rec_man.instance_manager.add_existing_records_of_type(given_pool, SampleModel)

        # Get the library of samples from the parent sample (pool-110)
        rec_man.relationship_manager.load_parents_of_type(given_pool_models, SampleModel)
        pure_pool: SampleModel = given_pool_models[0].get_parent_of_type(SampleModel)

        rec_man.relationship_manager.load_parents_of_type([pure_pool], SampleModel)
        library_sample_models: list[SampleModel] = pure_pool.get_parents_of_type(SampleModel)
        library_samples = [rec.get_data_record() for rec in library_sample_models]

        for rec_model in library_sample_models:
            index_barcode = rec_model.get_C_IndexTag_field()
            if index_barcode:
                index_parts = index_barcode.split("-")
                index = index_parts[0]
                index2 = index_parts[1] if len(index_parts) > 1 else ""
            else:
                index = ""
                index2 = ""

            override = "Y151;I8N2;N2I8;Y151"
            adapter1 = "AGATCGGAAGAGCACACGTCTGAACTCCAGTCA"
            adapter2 = "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT"

            sample_sheet.append(
                    f"3,{rec_model.get_SampleId_field()},{index},{index2},{override},{adapter1},{adapter2},1,1"
                    )

        sample_sheet.extend([
            "",
            "[DragenGermline_Settings]",
            "SoftwareVersion,4.1.23",
            "MapAlignOutFormat,cram",
            "KeepFastq,TRUE",
            "",
            "[DragenGermline_Data]",
            "Sample_ID,ReferenceGenomeDir,VariantCallingMode",
            ])

        for rec_model in library_sample_models:
            sample_sheet.append(f"{rec_model.get_SampleId_field()},hg19-alt_masked.cnv.hla.rna-8-1667494631-2, AllVariantCallers")

        return sample_sheet
    
    def sheet_to_excel(self, sample_data: list[str]) -> bytes:
        wb = openpyxl.Workbook()
        ws = wb.active

        for row in sample_data:
            ws.append(row.split(","))

        # save the file
        output_file = io.BytesIO()
        wb.save(output_file)

        return output_file.getvalue()

    def attach_sample_sheet_to_entry(self, context: SapioWebhookContext, file_name, file_bytes):
        exp_handler = ExperimentHandler(context)

        # get step
        step = exp_handler.get_step("Generate Sample Sheet")
        exp_handler.initialize_step(step)

        # create attachment entry
        attachmentRecord: AttachmentModel = self.inst_man.add_new_record_of_type(AttachmentModel)
        self.rec_man.store_and_commit()

        # add attachment entry
        context.data_record_manager.set_attachment_data(attachmentRecord.get_data_record(),file_name, file_bytes)
        self.rec_man.store_and_commit()

        # add attachment record to entry
        update_criteria: ElnAttachmentEntryUpdateCriteria = ElnAttachmentEntryUpdateCriteria()
        update_criteria.record_id = attachmentRecord.record_id
        update_criteria.attachment_name = file_name
        context.eln_manager.update_experiment_entry(context.eln_experiment.notebook_experiment_id,
                                                    step.eln_entry.entry_id, update_criteria)
        self.rec_man.store_and_commit()

    def get_ancestors(self, record: DataRecord, ancestor_type_name: str, levels: int = 1) -> List[DataRecord]:
        if self.ancestor_map is None:
            pager = GetAncestorsListAutoPager([record.record_id], record.data_type_name, ancestor_type_name, self.context.user)
            self.ancestor_map: dict[MultiMapKey, Set[MultiMapValue]] = pager.get_all_at_once()._store

        ancestors: List[DataRecord] = []
        for record_id, record_ancestors in self.ancestor_map.items():
            ancestors.extend(record_ancestors)

        if len(ancestors) >= levels:
            return ancestors[:levels]
        else:
            return ancestors
