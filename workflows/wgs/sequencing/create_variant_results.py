from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, VariantResultModel, PatientModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class CreateVariantResults(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # For each sample, create a variant result
        self.create_variant_results(context, samples)

        return SapioWebhookResult(True)

    def create_variant_results(self, context: SapioWebhookContext, libraries: list[SampleModel]) -> None:
        # Get all the patients of the samples
        patients_to_samples: dict[PatientModel, list[SampleModel]] = (
            SampleUtils.get_patients_from_samples(context, self.rec_handler, libraries))

        # Create a variant result for each patient
        for p in patients_to_samples:
            libraries: list[SampleModel] = patients_to_samples[p]
            for library in libraries:
                self.add_variant_result(p.record_id, context.eln_experiment.notebook_experiment_id, library)

        self.rec_man.store_and_commit()

    def add_variant_result(self, patient_record_id: int, exp_id: int, library: SampleModel) -> None:
        variant_result: VariantResultModel = self.rec_handler.add_model(VariantResultModel)
        variant_result.set_Patient_field(patient_record_id)
        variant_result.set_AnalysisId_field(str(exp_id))

        library.add_child(variant_result)
