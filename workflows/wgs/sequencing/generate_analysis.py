import functools
import json
from dataclasses import dataclass
from typing import Optional

from utilities.filebridge_utils import FileBridgeUtils
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import MultiFileRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import WriteFileResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelInstanceManager
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from utilities.data_models import AssayDetailModel
from utilities.data_models import AssignedProcessModel
from utilities.data_models import FlowCellLaneModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.data_models import SequencingMetadataModel
from utilities.utils import get_entry_by_option
from utilities.webhook_handler import OsloWebhookHandler

@dataclass
class Details:
    analysis_id: int = None
    priority: str = None
    analysis_name: str = None
    seq_sample_id: str = None
    dna_sample_id: str = None
    seq_file_name: str = None
    project_name: str = None
    flowcell_type: str = None
    flowcell_id: str = None
    sequencer_type: str = None
    sequencer_name: str = None
    sequencer_id: str = None

    def to_json(self):
        return json.dumps(self.__dict__)

class AnalysisFileGenerationHandler(OsloWebhookHandler):
    context: SapioWebhookContext
    rec_man: DataRecordManager
    mod_man: RecordModelManager 
    rel_man: RecordModelRelationshipManager

    def get_analysis_name(self, rec: SampleModel, order_rec: RequestModel) -> str:
        self.mod_man.relationship_manager.load_children_of_type([order_rec], AssayDetailModel)
        assay_recs: list[AssayDetailModel] = order_rec.get_children_of_type(AssayDetailModel)
        ids = []

        for assay in assay_recs:
            ids.append(assay.get_TestCodeId_field())

        self.mod_man.relationship_manager.load_parents_of_type([rec], AssignedProcessModel)
        assigned_processes: list[AssignedProcessModel] = rec.get_parents_of_type(AssignedProcessModel)
        if not len(assigned_processes):
            return ""

        ap_proc_name = assigned_processes[-1].get_ProcessName_field()
        assert isinstance(ap_proc_name, str)

        for i in ids:
            if i == ap_proc_name:
                return ap_proc_name

        return ""

    def get_project_name(self, rec_model: SampleModel) -> str:
        rec = rec_model.get_data_record()
        try:
            project: DataRecord = self.rec_man.get_ancestors(rec.get_record_id(), rec.get_data_type_name(), "Project").result_list[0]
            return project.get_field_value("DataRecordName")
        except:
            return "DefaultProjName"


    def get_seq_file_name(self, rec:SampleModel, idx: int) -> str:
        fields = [
            rec.get_SampleId_field(),
            "S28", # demultiplexer - TODO: OSLO-538 (unknown value so far)
            "L" + str(idx),
            "R1",
            "001"
        ]

        return "_".join(fields)


    def get_run_details(self) -> dict[str, str]:
        ret = {}

        run_details: Optional[ExperimentEntry] = get_entry_by_option(self.context,"ILLUMINA SEQUENCING RUN MODE ENTRY")
        entry_data_record = self.context.eln_manager.get_data_records_for_entry(
                self.context.eln_experiment.notebook_experiment_id,
                run_details.entry_id
            ).result_list[0]

        instrument_records = self.context.data_record_manager.query_all_records_of_type('Instrument').result_list

        ret["seq_mode"] = entry_data_record.get_field_value("SequencingRunMode")
        ret["sequencer"] = entry_data_record.get_field_value("SequencerInstrument")

        for r in instrument_records:
            if r.get_field_value('InstrumentName') == ret["sequencer"]:
                ret["instrument"] = r.get_field_value("SerialNumber")
                break

        if "instrument" not in ret.keys():
            ret["instrument"] = ""

        return ret

    def generate_file(self, pool: SampleModel, given_pool: list[DataRecord], experiment_id: int) -> tuple[str, str]:
        # Get the related sequence metadata from the parent sample (pool-110)
        pure_pool: SampleModel = pool.get_parent_of_type(SampleModel)
        self.mod_man.relationship_manager.load_parents_of_type([pure_pool], SequencingMetadataModel)
        seq_metadata: SequencingMetadataModel = pure_pool.get_parent_of_type(SequencingMetadataModel)
        assert isinstance(seq_metadata, SequencingMetadataModel)

        # Get samples from pool
        self.mod_man.relationship_manager.load_parents_of_type([pure_pool], SampleModel)
        library_sample_models: list[SampleModel] = pure_pool.get_parents_of_type(SampleModel)
        self.mod_man.relationship_manager.load_children_of_type(library_sample_models, SampleModel)
        library_samples = [rec.get_data_record() for rec in library_sample_models]

        request_records = [self.rec_man.get_parents(child.get_record_id(), "Sample", "Request").result_list for child in library_samples]
        # reduce from 2d array to 1d
        request_records: list[DataRecord] = functools.reduce(lambda x,y :x+y , request_records)
        request_records_models: list[RequestModel] = self.mod_man.instance_manager.add_existing_records_of_type(given_pool, RequestModel)

        rec_details: list[Details] = []

        # Get the related order and assay details
        for idx, rec in enumerate(library_sample_models):
            # get samples in subpool
            sub_sample = rec.get_children_of_type(SampleModel)
            self.mod_man.relationship_manager.load_children_of_type(sub_sample, FlowCellLaneModel)

            try:
                flow_cell_rec: FlowCellLaneModel = sub_sample[0].get_children_of_type(FlowCellLaneModel)[0]
            except IndexError:
                flow_cell_rec = ""

            run_details = self.get_run_details()
            try:
                order_priority = request_records_models[idx].get_field_value("Priority")
            except IndexError:
                order_priority = "Not Set"


            d = Details()
            d.analysis_id = str(self.context.eln_experiment.notebook_experiment_id)
            d.priority = order_priority
            d.analysis_name = self.get_analysis_name(rec, library_sample_models[idx]) 
            d.seq_sample_id = pure_pool.get_SampleId_field()
            d.dna_sample_id = rec.get_SampleId_field()
            d.seq_file_name = self.get_seq_file_name(rec, idx)
            d.project_name = self.get_project_name(rec)
            d.flowcell_type = run_details["seq_mode"]
            d.flowcell_id = flow_cell_rec.get_FlowcellId_field() if isinstance(flow_cell_rec, FlowCellLaneModel) else ""
            d.sequencer_type = seq_metadata.get_field_value("SequencerType")
            d.sequencer_name = run_details["sequencer"]
            d.sequencer_id = run_details["instrument"]

            rec_details.append(d)

        # # Write the analysis file to FileBridge
        file_data: str = "["

        for idx, deet in enumerate(rec_details):
            file_data += deet.to_json()

            if idx < len(rec_details) - 1:
                file_data += ","

        file_data += "]"

        return str(experiment_id), file_data

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.context = context
        if isinstance(self.context.client_callback_result, WriteFileResult):
            return SapioWebhookResult(True)
        
        # Get the ELN experiment
        eln_experiment: ElnExperiment = self.context.eln_experiment
        experiment_id: int = eln_experiment.notebook_experiment_id

        self.rec_man = DataMgmtServer.get_data_record_manager(self.context.user)
        self.mod_man: RecordModelManager = RecordModelManager(self.context.user)
        self.rel_man = self.mod_man.relationship_manager

        # Get the samples submitted to the experiment (should be in format pool-110_1)
        sample_entry = get_entry_by_option(self.context, "FLOW CELL SAMPLES")
        # given pool is the base pool (ex pool-111)
        given_pool: list[DataRecord] = self.context.eln_manager.get_data_records_for_entry(
            self.context.eln_experiment.notebook_experiment_id, sample_entry.entry_id).result_list
        given_pool_models: list[SampleModel] = self.mod_man.instance_manager.add_existing_records_of_type(given_pool, SampleModel)

        # import types that I'll need later
        self.mod_man.relationship_manager.load_children_of_type(given_pool_models, SampleModel)
        self.mod_man.relationship_manager.load_parents_of_type(given_pool_models, SampleModel)

        files = {}
        for idx, pool in enumerate(given_pool_models):
            name, data = self.generate_file(pool, given_pool, experiment_id)
            files[f"{name}_{idx + 1}.json"] = data.encode('utf-8')

        unsent_files = {}
        for name, data in files.items():
            if not FileBridgeUtils.upload_file(self.context, "oslo-filebridge", name, data, ""):
                unsent_files[name] = data

        if unsent_files.items():
            return SapioWebhookResult(True, client_callback_request=MultiFileRequest(unsent_files))



