from typing import TypeAlias

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FilePromptRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FilePromptResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelWrapper import WrappedRecordModel, WrappedType

from utilities.data_models import AssignedProcessModel, TapeStationResultModel
from utilities.data_models import ELNExperimentModel
from utilities.data_models import ELNSampleDetailModel
from utilities.data_models import QCDatumModel
from utilities.data_models import SampleModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

# Type Aliases
map_t: TypeAlias = dict[str, dict[str, list[str]]]
id_to_map_t: TypeAlias = tuple[map_t, str]

qpcr_well_map: dict[str, list[str]] = {
    'A1': ['A1', 'A2', 'B1'],
    'A2': ['A3', 'A4', 'B3'],
    'A3': ['A5', 'A6', 'B5'],
    'A4': ['A7', 'A8', 'B7'],
    'A5': ['A9', 'A10', 'B9'],
    'A6': ['A11', 'A12', 'B11'],
    'B1': ['C1', 'C2', 'D1'],
    'B2': ['C3', 'C4', 'D3'],
    'B3': ['C5', 'C6', 'D5'],
    'B4': ['C7', 'C8', 'D7'],
    'B5': ['C9', 'C10', 'D9'],
    'B6': ['C11', 'C12', 'D11'],
    'C1': ['E1', 'E2', 'F1'],
    'C2': ['E3', 'E4', 'F3'],
    'C3': ['E5', 'E6', 'F5'],
    'C4': ['E7', 'E8', 'F7'],
    'C5': ['E9', 'E10', 'F9'],
    'C6': ['E11', 'E12', 'F11'],
    'D1': ['G1', 'G2', 'H1'],
    'D2': ['G3', 'G4', 'H3'],
    'D3': ['G5', 'G6', 'H5'],
    'D4': ['G7', 'G8', 'H7'],
    'D5': ['G9', 'G10', 'H9'],
    'D6': ['G11', 'G12', 'H11'],
    'E1': ['I1', 'I2', 'J1'],
    'E2': ['I3', 'I4', 'J3'],
    'E3': ['I5', 'I6', 'J5'],
    'E4': ['I7', 'I8', 'J7'],
    'E5': ['I9', 'I10', 'J9'],
    'E6': ['I11', 'I12', 'J11'],
    'F1': ['K1', 'K2', 'L1'],
    'F2': ['K3', 'K4', 'L3'],
    'F3': ['K5', 'K6', 'L5'],
    'F4': ['K7', 'K8', 'L7'],
    'F5': ['K9', 'K10', 'L9'],
    'F6': ['K11', 'K12', 'L11'],
    'G1': ['M1', 'M2', 'N1'],
    'G2': ['M3', 'M4', 'N3'],
    'G3': ['M5', 'M6', 'N5'],
    'G4': ['M7', 'M8', 'N7'],
    'G5': ['M9', 'M10', 'N9'],
    'G6': ['M11', 'M12', 'N11'],
    'H1': ['O1', 'O2', 'P1'],
    'H2': ['O3', 'O4', 'P3'],
    'H3': ['O5', 'O6', 'P5'],
    'H4': ['O7', 'O8', 'P7'],
    'H5': ['O9', 'O10', 'P9'],
    'H6': ['O11', 'O12', 'P11'],
}

special_wells = {
    'Std 1': ['B2', 'B4', 'B6'],
    'Std 2': ['D2', 'D4', 'D6'],
    'Std 3': ['F2', 'F4', 'F6'],
    'Std 4': ['H2', 'H4', 'H6'],
    'Std 5': ['J2', 'J4', 'J6'],
    'Std 6': ['L2', 'L4', 'L6'],
    'NTC': ['B8', 'D8', 'F8']
}


class ParseQpcrFile(OsloWebhookHandler):
    # Entries
    EXP_DETAILS_ENTRY = "Std + NTC Replicates"
    ALIQUOTED_SAMPLES_ENTRY = "Aliquoted Samples"
    MOLARITY_CALC_ENTRY = "Molarity Calculations"

    # Options
    LIGHT_CYCLER_OPTION = "LIGHT CYCLER"
    QUANT_STUDIO_OPTION = "QUANT STUDIO"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get the file uploaded
        if not context.client_callback_result:
            request = FilePromptRequest(
                dialog_title="Please upload the qPCR results file",
                file_extension="txt"
            )
            return SapioWebhookResult(True, client_callback_request=request)

        elif not hasattr(context.client_callback_result, "user_cancelled"):
            return SapioWebhookResult(True)

        else:
            return self.compute(context)

    def compute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # retrieve file
        file_provided: FilePromptResult | None = context.client_callback_result
        lines = file_provided.file_bytes.decode("utf-8").split("\n")[1:]

        # get the entry
        self.exp_handler = ExperimentHandler(context)

        exp_details_entry: ElnEntryStep | None = self.exp_handler.get_step(self.EXP_DETAILS_ENTRY)
        if exp_details_entry is None:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.EXP_DETAILS_ENTRY}")

        # based on entry options decide the file being parsed
        selected_option: str | None = self.set_selected_option(self.exp_handler, exp_details_entry)
        if selected_option is None:
            return PopupUtil.display_ok_popup(
                "Error",
                "Unable to determine the instrument selected"
            )

        parsed_file_lines: dict[str, float] = {}
        if self.LIGHT_CYCLER_OPTION == selected_option:
            parsed_file_lines = self.parse_light_cycler(lines[1:])
        elif self.QUANT_STUDIO_OPTION == selected_option:
            parsed_file_lines = self.parse_quant_studio(lines[1:])

        # get all the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models(
            self.ALIQUOTED_SAMPLES_ENTRY,
            SampleModel
        )

        sample_id_to_well: dict[str, str] = {}
        for sample in samples:
            sample_row = sample.get_RowPosition_field()
            sample_col = sample.get_ColPosition_field()
            sample_id = sample.get_SampleId_field()

            if sample_row is None or sample_col is None:
                continue

            sample_id_to_well[sample_id] = f"{sample_row}{sample_col}"

        # map the sample to their qPCR position, sample id, sample name
        sample_id_to_wells: id_to_map_t = self.get_sample_id_to_qpcr_mapping(samples)
        if isinstance(sample_id_to_wells, str):
            return PopupUtil.display_ok_popup("Error", sample_id_to_wells)

        # add all the details and set known fields
        name_to_avg_cp: dict[str, float] = {}
        for name, wells in special_wells.items():
            cp_values = [
                parsed_file_lines[f"{name}{well}"]
                for well in wells
                if f"{name}{well}" in parsed_file_lines
            ]

            name_to_avg_cp[name] = (
                sum(cp_values) / len(cp_values)
                if cp_values
                else 0.0
            )

        name_to_qty: dict[str, float] = {
            "Std 1": 20,
            "Std 2": 2,
            "Std 3": 0.2,
            "Std 4": 0.02,
            "Std 5": 0.002,
            "Std 6": 0.0002
        }

        # delete old records
        self.delete_recs(self.EXP_DETAILS_ENTRY, ELNExperimentModel)
        new_details_models: list[PyRecordModel] = []

        for name, wells in special_wells.items():
            for well in wells:
                std_ntc_detail_model: PyRecordModel = self.exp_handler.add_eln_rows(exp_details_entry, 1)[0]

                cp: float = 0.0
                if f"{name}{well}" in parsed_file_lines:
                    cp = parsed_file_lines[name + well]

                avg_cp = name_to_avg_cp[name]
                difference = None
                outlier = None

                if name != "NTC":
                    if cp and avg_cp:
                        difference = cp - avg_cp

                        if abs(difference) > 0.25:
                            outlier = cp
                            # cp should set only on outlier and should not be included in AvgCp calculation
                            # Remove this CP from id_to_cps for future average calculations
                            cp_values = [
                                parsed_file_lines[f"{name}{well}"]
                                for well in wells
                                if f"{name}{well}" in parsed_file_lines
                            ]
                            # Remove cp value
                            # Calculate avg cp again
                            cp_values.remove(cp)
                            name_to_avg_cp[name] = (
                                sum(cp_values) / len(cp_values)
                                if cp_values
                                else 0.0
                            )
                            avg_cp = name_to_avg_cp[name]
                            cp = None

                qty = name_to_qty[name] if name in name_to_qty.keys() else 0.0
                std_ntc_detail_model.set_field_values({
                    "Pos": well,
                    "Name2": name,
                    "Cp": cp,
                    "Outliers": outlier,
                    "Qty": qty,
                    "AvgCp": avg_cp,
                    "Difference": difference
                })
                new_details_models.append(std_ntc_detail_model)

        name_to_delta_cp: dict[str, float] = {
            "Std 2": name_to_avg_cp["Std 2"] - name_to_avg_cp["Std 1"],
            "Std 3": name_to_avg_cp["Std 3"] - name_to_avg_cp["Std 2"],
            "Std 4": name_to_avg_cp["Std 4"] - name_to_avg_cp["Std 3"],
            "Std 5": name_to_avg_cp["Std 5"] - name_to_avg_cp["Std 4"],
            "Std 6": name_to_avg_cp["Std 6"] - name_to_avg_cp["Std 5"],
            "NTC": name_to_avg_cp["NTC"] - name_to_avg_cp["Std 6"]
        }

        for std_ntc_detail_model in new_details_models:
            name = std_ntc_detail_model.get_field_value("Name2")
            if name:
                delta_cp = name_to_delta_cp[name] if name in name_to_delta_cp.keys() else 0.0
                std_ntc_detail_model.set_field_value("DeltaCq", delta_cp)

        self.delete_recs(self.MOLARITY_CALC_ENTRY, ELNSampleDetailModel)

        # Get the process name
        non_controls = [
            s for s in samples
            if s.get_ExemplarSampleType_field() != "Promega"
               and s.get_ExemplarSampleType_field() != "In House"
               and not s.get_IsControl_field()
        ]
        aliquot = non_controls[0]
        self.rel_man.load_parents_of_type([aliquot], SampleModel)
        parent_sample = aliquot.get_parent_of_type(SampleModel)
        current_process = SampleUtils.get_current_process(parent_sample, self.rel_man).get_ProcessName_field()
        self.rel_man.load_children_of_type(samples, QCDatumModel)



        # create the new molarity records
        molarity_details: list[PyRecordModel] = []
        id_to_cps: dict[str, list[float]] = {}
        sample_errors: list[str] = []

        for sample in samples:
            wells = qpcr_well_map[sample_id_to_well[sample.get_SampleId_field()]]
            id_to_cps[sample.get_SampleId_field()] = []

            for well in wells:
                mol_detail = self.exp_handler.add_eln_rows(self.MOLARITY_CALC_ENTRY, 1)[0]
                mol_detail.set_field_value("WellPosition2", well)

                cp = 0
                if f"{sample.get_OtherSampleId_field()}{well}" in parsed_file_lines.keys():
                    cp = parsed_file_lines[sample.get_OtherSampleId_field() + well]
                else:
                    sample_errors.append(
                        f"Sample with Sample Name : {sample.get_OtherSampleId_field()} at well {well}"
                    )

                bp_val = 560 if current_process == "WGS" else self.get_bp_result_size(sample)
                mol_detail.set_field_values({
                    "CP": cp,
                    "SampleId": sample.get_SampleId_field(),
                    "OtherSampleId": sample.get_OtherSampleId_field(),
                    "Dilution": 3600,
                    "AverageFragmentLengthbp": bp_val
                })

                id_to_cps[sample.get_SampleId_field()].append(float(cp))
                molarity_details.append(mol_detail)

        sample_id_to_avg_cp: dict[str, float] = {}
        for sample_id in id_to_cps:
            valid_cps = [cp for cp in id_to_cps[sample_id] if cp is not None]
            if valid_cps:
                sample_id_to_avg_cp[sample_id] = sum(valid_cps) / len(valid_cps)

        for mol_detail in molarity_details:
            cp: float = 0.0
            sample_id = mol_detail.get_field_value("SampleId")

            if mol_detail.get_field_value("SampleId") + mol_detail.get_field_value(
                    "WellPosition2") in parsed_file_lines.keys():
                cp = parsed_file_lines[
                    mol_detail.get_field_value("SampleId") + mol_detail.get_field_value("WellPosition2")]

            difference = cp - sample_id_to_avg_cp[mol_detail.get_field_value("SampleId")]
            if abs(difference) > 0.25:
                mol_detail.set_field_value("OUTLIERS", cp)
                # cp should set only on outlier and should not be included in AvgCp calculation
                # Remove this CP from id_to_cps for future average calculations
                if cp in id_to_cps.get(sample_id, []):
                    id_to_cps[sample_id].remove(cp)
                cp = 0.0
                valid_cps = [cp for cp in id_to_cps[sample_id] if cp is not None]
                sample_id_to_avg_cp[sample_id] = sum(valid_cps) / len(valid_cps)
                mol_detail.set_field_value("AVGCP", sample_id_to_avg_cp[sample_id])

            mol_detail.set_field_value("DIFFERENCE", difference)
            mol_detail.set_field_value("CP", cp)

        for sample_id in id_to_cps:
            # Collect outliers for each sample
            sample_outliers = [
                mol_detail.get_field_value("OUTLIERS")
                for mol_detail in molarity_details
                if (mol_detail.get_field_value("SampleId") == sample_id and
                    mol_detail.get_field_value("OUTLIERS") is not None)
            ]

            # If more than 3 outliers, calculate mean of outliers
            if len(sample_outliers) > 3:
                outlier_mean = sum(sample_outliers) / len(sample_outliers)
                # Update AVGCP for all records of this sample
                for mol_detail in molarity_details:
                    if mol_detail.get_field_value("SampleId") == sample_id:
                        mol_detail.set_field_value("AVGCP", outlier_mean)

        if sample_errors:
            return PopupUtil.display_ok_popup(
                "Error",
                "Error Locating Following Samples In The File : " + " ,".join(sample_errors)
            )

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def get_bp_result_size(self, sample: SampleModel) -> float | None:
        qc_records = sample.get_children_of_type(QCDatumModel)
        if qc_records:
            latest_qc_datum = max(qc_records, key=lambda x: x.get_DateCreated_field())

            if latest_qc_datum is not None:
                self.rel_man.load_children_of_type([latest_qc_datum], TapeStationResultModel)
                tapestation_results = latest_qc_datum.get_children_of_type(TapeStationResultModel)
                if tapestation_results:
                    return tapestation_results[0].get_AvgBPSize_field()

        return None

    def get_sample_id_to_qpcr_mapping(self, samples: list[SampleModel]):
        sample_id_to_wells = dict()

        for sample in samples:
            sample_row = sample.get_RowPosition_field()
            sample_col = sample.get_ColPosition_field()
            sample_id = sample.get_SampleId_field()

            if sample_row is None or sample_col is None:
                continue

            rowcol = f"{sample_row}{sample_col}"
            if rowcol in qpcr_well_map.keys():
                sample_id_to_wells[sample_id] = qpcr_well_map[rowcol]
            else:
                return f"Cannot find pos {sample_row}{sample_col}"

        return sample_id_to_wells

    def set_selected_option(self, exp_handler: ExperimentHandler, entry: ElnEntryStep) -> str | None:
        exp_details_options = exp_handler.get_step_options(entry)

        if self.LIGHT_CYCLER_OPTION in exp_details_options.keys():
            return self.LIGHT_CYCLER_OPTION
        elif self.QUANT_STUDIO_OPTION in exp_details_options.keys():
            return self.QUANT_STUDIO_OPTION
        else:
            return None

    def parse_light_cycler(self, lines: list[str]) -> dict[str, float]:
        ret: dict[str, float] = {}
        for line in lines[1:]:
            line = line.split("\t")
            if len(line) <= 4:
                break

            try:
                cp = float(line[4])
            except ValueError:
                cp = 0.0

            well = line[2].strip()
            sample_name = line[3].strip()

            # If NTC have value “0”, it should automatically be set to 30
            # (so the Delta Cp does not give negative value)
            if sample_name == "NTC" and cp == 0.0:
                cp = 30
            ret[sample_name + well] = cp

        return ret

    def parse_quant_studio(self, lines: list[str]) -> dict[str, float]:
        ret: dict[str, float] = {}

        start_point = 0
        for idx, line in enumerate(lines):
            if "Results" in line:
                start_point = idx - 1
                break

        for line in lines[start_point + 2:]:
            line = line.split("\t")

            try:
                cp = float(line[2])
            except IndexError:
                continue
            except ValueError:
                continue

            well = line[0].strip()
            sample_name = line[1].strip()

            # If NTC have value “0”, it should automatically be set to 30
            # (so the Delta Cp does not give negative value)
            if sample_name == "NTC" and cp == 0.0:
                cp = 30
            ret[sample_name + well] = cp

        return ret

    def delete_recs(self, step_name: str, step_type: type[WrappedType]):
        records = self.exp_handler.get_step_models(step_name, step_type)
        for rec in records:
            rec.delete()
