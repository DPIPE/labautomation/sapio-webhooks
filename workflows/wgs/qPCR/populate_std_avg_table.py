import math

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentDetailModel
from utilities.webhook_handler import OsloWebhookHandler


class PopulateStdAvgTable(OsloWebhookHandler):
    # Entries
    REPLICATES_ENTRY = "Std + NTC Replicates"
    STD_AVG_ENTRY = "Std Averages"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get the sample details values
        experiment_handler = ExperimentHandler(context)
        replicates_entry = experiment_handler.get_step(self.REPLICATES_ENTRY)
        if not replicates_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.REPLICATES_ENTRY}")

        replicate_records = experiment_handler.get_step_records(replicates_entry)
        replicate_details = self.inst_man.add_existing_records_of_type(replicate_records, ELNExperimentDetailModel)
        stds = []
        for replicate_detail in replicate_details:
            if replicate_detail.get_field_value("Pos") in ["B2", "D2", "F2", "H2", "J2", "L2"]:
                name = replicate_detail.get_field_value("Name2")
                concentration = replicate_detail.get_field_value("Qty")
                log_concentration = math.log10(concentration)
                avg_cp = replicate_detail.get_field_value("AvgCp")
                delta_cp = replicate_detail.get_field_value("DeltaCq")
                stds.append([name, concentration, log_concentration, avg_cp, delta_cp])

        # update the sample details in the std avg table and remove the additional sample details
        std_avg_entry = experiment_handler.get_step(self.STD_AVG_ENTRY)
        if not std_avg_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.STD_AVG_ENTRY}")

        std_avg_records = experiment_handler.get_step_records(std_avg_entry)
        std_details = self.inst_man.add_existing_records_of_type(std_avg_records, ELNExperimentDetailModel)
        for std_detail in std_details:
            std_detail.delete()

        std_details = []
        for fields in stds:
            std_detail_model = experiment_handler.add_eln_rows(std_avg_entry, 1).pop()
            std_detail = self.inst_man.wrap(std_detail_model, ELNExperimentDetailModel)
            std_detail.set_field_value("DNAStd", fields[0])
            std_detail.set_field_value("ConcpM", fields[1])
            std_detail.set_field_value("Logconc", fields[2])
            std_detail.set_field_value("AvgCq", fields[3])
            std_detail.set_field_value("DeltaCq", fields[4])
            std_details.append(std_detail)

        # store the changes
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, eln_entry_refresh_list=[std_avg_entry.eln_entry])
