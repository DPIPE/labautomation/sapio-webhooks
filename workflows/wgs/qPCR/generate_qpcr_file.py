import csv
import io
from datetime import datetime

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ELNExperimentDetailModel
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler
from workflows.wgs.qPCR.parse_qpcr_file import qpcr_well_map
from workflows.wgs.qPCR.parse_qpcr_file import special_wells


class GenerateQPCRFile(OsloWebhookHandler):
    # file bridge path
    NETWORK_PATH = "Files/"

    # Entries
    EXP_DETAILS_ENTRY = "Std + NTC Replicates"
    ALIQUOT_SAMPLES_ENTRY = "Aliquoted Samples"
    INSTRUMENT_TRACKING_ENTRY = "Instrument Tracking"

    # Options
    LIGHT_CYCLER_OPTION = "LIGHT CYCLER"
    QUANT_STUDIO_OPTION = "QUANT STUDIO"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # ensure that the instrument tracking entry is submitted
        experiment_handler = ExperimentHandler(context)
        instrument_tracking_entry = experiment_handler.get_step(self.INSTRUMENT_TRACKING_ENTRY)
        if instrument_tracking_entry.eln_entry.entry_status != ExperimentEntryStatus.Completed:
            return PopupUtil.display_ok_popup("Error", "Instrument tracking must be submitted")

        # get the instrument tracking records
        experiment_detail_records = experiment_handler.get_step_records(instrument_tracking_entry)
        experiment_details = self.inst_man.add_existing_records_of_type(experiment_detail_records,
                                                                        ELNExperimentDetailModel)
        instrument_selected = None
        for experiment_detail in experiment_details:
            if experiment_detail.get_field_value("InstrumentUsed"):
                if "LightCycler" in experiment_detail.get_field_value("InstrumentType"):
                    instrument_selected = self.LIGHT_CYCLER_OPTION
                if "QuantStudio" in experiment_detail.get_field_value("InstrumentType"):
                    instrument_selected = self.QUANT_STUDIO_OPTION
        if not instrument_selected:
            instrument_tracking_entry.unlock_step()
            return PopupUtil.display_ok_popup("Error", "Could not determine which instrument is being used")

        # get all the samples
        aliquoted_samples_entry = experiment_handler.get_step(self.ALIQUOT_SAMPLES_ENTRY)
        if not aliquoted_samples_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.ALIQUOT_SAMPLES_ENTRY}")
        sample_records = experiment_handler.get_step_records(aliquoted_samples_entry)
        samples: [SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get the sample mapping and map well pos to sample name
        well_pos_to_sample_name = dict()
        for sample in samples:
            try:
                well_pos = sample.get_RowPosition_field().strip() + sample.get_ColPosition_field().strip()
                if well_pos in qpcr_well_map:
                    for mapped_well in qpcr_well_map[well_pos]:
                        well_pos_to_sample_name[mapped_well] = sample.get_OtherSampleId_field()
                else:
                    return PopupUtil.display_ok_popup("Error", f"Well Position Mapping "
                                                               f"Error For {sample.get_OtherSampleId_field()}")
                for name in special_wells:
                    for well in special_wells[name]:
                        well_pos_to_sample_name[well] = name
            except:
                return PopupUtil.display_ok_popup("Error", f"Error determining well pos for "
                                                           f"{sample.get_OtherSampleId_field()}")

        # get the base template that will be updated
        lines = []
        rows = "ABCDEFGHIJKLMNOP"
        for row in rows:
            col = 1
            while col < 25:
                well_pos = row+str(col)
                sample_name = 0
                if well_pos in well_pos_to_sample_name:
                    sample_name = well_pos_to_sample_name[well_pos]
                lines.append([well_pos, sample_name])
                col += 1

        # generate the file as per the instrument
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y%m%d_%H%M%S")
        if instrument_selected == self.LIGHT_CYCLER_OPTION:
            file_name = "LightCycler_" + formatted_datetime + ".txt"
            lines.insert(0, ["Pos", "Sample Name"])
        else:
            file_name = "QuantStudio_" + formatted_datetime + ".txt"
            lines.insert(0, ["Well", "Sample Name"])
        # generate file
        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer,delimiter="\t")
            csv_writer.writerows(lines)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()

        # add the selected instrument as an entry option on STD + NTC entry
        exp_details_entry = experiment_handler.get_step(self.EXP_DETAILS_ENTRY)
        if not exp_details_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.EXP_DETAILS_ENTRY}")
        options = exp_details_entry.get_options()
        if self.LIGHT_CYCLER_OPTION in options:
            options.pop(self.LIGHT_CYCLER_OPTION)
        if self.QUANT_STUDIO_OPTION in options:
            options.pop(self.QUANT_STUDIO_OPTION)
        options[instrument_selected] = ""
        exp_details_entry.set_options(options)

        # Add the generated file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                bytes(csv_bytes, "utf-8"),
                "Generated Instrument File",
            )
        )

        # write file to user
        # Get the current date and time
        try:
            FileBridge.write_file(
                context,
                "oslo-filebridge",
                self.NETWORK_PATH + file_name,
                csv_bytes
            )
            return SapioWebhookResult(True)
        except:
            pass

        return SapioWebhookResult(
            True,
            client_callback_request=WriteFileRequest(
                    file_bytes=csv_bytes.encode('UTF-8'),
                    file_path=file_name
                )
        )
