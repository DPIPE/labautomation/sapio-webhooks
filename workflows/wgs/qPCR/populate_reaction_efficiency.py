from scipy.stats import linregress
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNSampleDetailModel, ELNExperimentDetailModel
from utilities.webhook_handler import OsloWebhookHandler


class PopulateReactionEfficiency(OsloWebhookHandler):
    # Entries
    STD_AVG_ENTRY = "Std Averages"
    EFFICIENCY_ENTRY = "QC Standard and NTC"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get the sample details values
        experiment_handler = ExperimentHandler(context)
        exp_details_entry = experiment_handler.get_step(self.STD_AVG_ENTRY)
        if not exp_details_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.STD_AVG_ENTRY}")

        exp_detail_records = experiment_handler.get_step_records(exp_details_entry)
        exp_details = self.inst_man.add_existing_records_of_type(exp_detail_records, ELNSampleDetailModel)
        x = []
        y = []
        for exp_detail in exp_details:
            x.append(exp_detail.get_field_value("AvgCq"))
            y.append(exp_detail.get_field_value("Logconc"))

        # update the sample details in the eff entry
        efficiency_entry = experiment_handler.get_step(self.EFFICIENCY_ENTRY)
        if not efficiency_entry:
            return PopupUtil.display_ok_popup("Error", f"Could not find entry {self.EFFICIENCY_ENTRY}")

        eff_records = experiment_handler.get_step_records(efficiency_entry)
        eff_detail = self.inst_man.add_existing_records_of_type(eff_records, ELNExperimentDetailModel).pop()
        slope, intercept, r_value, p_value, std_err = linregress(y, x)
        eff = abs(((1 / slope) ** 10.0) - 1.0) * 100
        r_value = abs(r_value)
        eff_detail.set_field_value("Efficiency", eff)
        eff_detail.set_field_value("Slope", slope)
        eff_detail.set_field_value("Rsquared", r_value)
        eff_detail.set_field_value("Intercept", intercept)

        if eff_detail.get_field_value("CurveStatus") == "Failed":  # Means delta cp is out of range
            eff_detail.set_field_value("OverrideStatus", "Failed")
        elif 90 < eff < 110 and 0.99 <= r_value <= 1.00:
            eff_detail.set_field_value("CurveStatus", "Passed")
            eff_detail.set_field_value("OverrideStatus", "Passed")
        else:
            eff_detail.set_field_value("CurveStatus", "Failed")
            eff_detail.set_field_value("OverrideStatus", "Failed")

        # store the changes
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, eln_entry_refresh_list=[efficiency_entry.eln_entry])
