from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelInstanceManager

from utilities.data_models import SampleModel, AssignedProcessModel
from utilities.sample_utils import SampleUtils
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler
from utilities.utils import get_entry_by_option


class InsertControls(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get count of runs
        client_config: DataRecord = self.dr_man.query_all_records_of_type("ClientConfigurations").result_list[0]
        # OSLO-685, based on wgs and wes determine the current interval and current_count
        if "WGS" in context.eln_experiment.notebook_experiment_name:
            interval = client_config.get_field_value("WgsControlInterval")
            current_count = client_config.get_field_value("WgsControlRunCount")
        else:
            interval = client_config.get_field_value("WESControlInterval")
            current_count = client_config.get_field_value("WESControlRunCount")

        data_record_manager = DataMgmtServer.get_data_record_manager(context.user)
        # Increment
        current_count += 1

        if current_count == interval:
            client_callback = CallbackUtil(context)
            exp_handler: ExperimentHandler = ExperimentHandler(context)
            client_callback.ok_dialog("", "Adding control samples")
            # Reset to 0
            if "WGS" in context.eln_experiment.notebook_experiment_name:
                client_config.set_field_value("WgsControlRunCount", 0)
            else:
                client_config.set_field_value("WESControlRunCount", 0)
            data_record_manager.commit_data_records([client_config])

            record_handler: RecordHandler = RecordHandler(context)
            inst_man: RecordModelInstanceManager = record_handler.rec_man.instance_manager

            sample_records: list[DataRecord] = get_records_from_entry_with_option(context,  "OSLO SAMPLES TO PREP")
            sample_models: list[SampleModel] = inst_man.add_existing_records_of_type(sample_records, SampleModel)

            # Create new control
            new_control: SampleModel = SampleUtils.register_sample(record_handler,
                                                                   DataMgmtServer.get_accession_manager(context.user))
            new_control.set_OtherSampleId_field("Positive_control")
            new_control.set_IsControl_field(True)

            # Add control to records
            self.rel_man.load_parents_of_type(sample_models, AssignedProcessModel)
            aps = sample_models[0].get_parents_of_type(AssignedProcessModel)
            ap = None
            for curr_ap in aps:
                if "In Process - " in curr_ap.get_Status_field():
                    ap = curr_ap
            sample_models.append(new_control)
            sample_records.append(new_control.get_data_record())
            record_handler.rec_man.store_and_commit()

            # add its assigned process
            new_assigned_process: AssignedProcessModel = record_handler.inst_man.add_new_record_of_type(
                AssignedProcessModel)
            new_assigned_process.set_SampleId_field(new_control.get_SampleId_field())
            new_assigned_process.set_OtherSampleId_field(new_control.get_OtherSampleId_field())
            new_assigned_process.set_SampleRecordId_field(new_control.record_id)
            new_assigned_process.set_ProcessName_field(ap.get_ProcessName_field())
            new_assigned_process.set_ProcessStepNumber_field(ap.get_ProcessStepNumber_field())
            new_assigned_process.set_Status_field(ap.get_Status_field())
            new_assigned_process.set_BranchLongId_field(ap.get_BranchLongId_field())
            new_control.add_parent(new_assigned_process)
            self.rec_man.store_and_commit()

            # Update entry with control
            samples_requested_entry: ExperimentEntry = get_entry_by_option(context, "OSLO SAMPLES TO PREP")
            samples_entry = exp_handler.get_step(samples_requested_entry.entry_name)
            exp_handler.add_step_records(samples_entry, [new_control])

            return SapioWebhookResult(True, eln_entry_refresh_list=[samples_entry.eln_entry])
        else:
            # Commit increment value
            if "WGS" in context.eln_experiment.notebook_experiment_name:
                client_config.set_field_value("WgsControlRunCount", current_count)
            else:
                client_config.set_field_value("WESControlRunCount", current_count)
            data_record_manager.commit_data_records([client_config])
            return SapioWebhookResult(True)
