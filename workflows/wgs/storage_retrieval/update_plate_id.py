from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PlateModel, SampleModel, ELNExperimentModel
from utilities.webhook_handler import OsloWebhookHandler

OPTION_KEY: str = "ID READER PLATE ID"


class UpdatePlateID(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        # Get the options of the entry
        plate_record_id: int = (
            int(self.exp_handler.get_step_options("Plate Setup")["MultiLayerPlating_Plate_RecordIdList"]))
        plate: PlateModel = self.rec_handler.query_models_by_id(PlateModel, [plate_record_id])[0]

        options: dict[str, str] = (
            DataMgmtServer.get_eln_manager(context.user)
            .get_notebook_experiment_options(context.eln_experiment.notebook_experiment_id))
        options[OPTION_KEY] = plate.get_PlateId_field()

        self.exp_handler.update_experiment(context.eln_experiment.notebook_experiment_name,
                                           experiment_option_map=options)

        # Add the samples as children of the current experiment
        exp: ELNExperimentModel = self.exp_handler.get_experiment_model(ELNExperimentModel)

        self.rel_man.load_children_of_type([plate], SampleModel)
        samples: list[SampleModel] = plate.get_children_of_type(SampleModel)

        for s in samples:
            exp.add_child(s)

        # Store and commit
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
