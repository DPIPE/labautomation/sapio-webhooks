import json
from typing import Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import PlateModel, SampleModel, PlateDesignerWellElementModel, RequestModel, \
    WGSAssayDetailModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

plate_locations: list[str] = [
    'A:1', 'B:1', 'C:1', 'D:1', 'E:1', 'F:1', 'G:1', 'H:1',
    'A:2', 'B:2', 'C:2', 'D:2', 'E:2', 'F:2', 'G:2', 'H:2',
    'A:3', 'B:3', 'C:3', 'D:3', 'E:3', 'F:3', 'G:3', 'H:3',
    'A:4', 'B:4', 'C:4', 'D:4', 'E:4', 'F:4', 'G:4', 'H:4',
    'A:5', 'B:5', 'C:5', 'D:5', 'E:5', 'F:5', 'G:5', 'H:5',
    'A:6', 'B:6', 'C:6', 'D:6', 'E:6', 'F:6', 'G:6', 'H:6',
    'A:7', 'B:7', 'C:7', 'D:7', 'E:7', 'F:7', 'G:7', 'H:7',
    'A:8', 'B:8', 'C:8', 'D:8', 'E:8', 'F:8', 'G:8', 'H:8',
    'A:9', 'B:9', 'C:9', 'D:9', 'E:9', 'F:9', 'G:9', 'H:9',
    'A:10', 'B:10', 'C:10', 'D:10', 'E:10', 'F:10', 'G:10', 'H:10',
    'A:11', 'B:11', 'C:11', 'D:11', 'E:11', 'F:11', 'G:11', 'H:11',
    'A:12', 'B:12', 'C:12', 'D:12', 'E:12', 'F:12', 'G:12', 'H:12'
]


class WGSAddSamplesToPlate(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    response_data: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        self.response_data = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            self.response_data = json.loads(result.callback_context_data)

            if self.response_data["status"] == "concentration":
                return SapioWebhookResult(True)

            if self.response_data["status"] == "error":
                return SapioWebhookResult(True)

        # Get the plate record
        plate: PlateModel = self.get_plate()

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Pick Files for DNA Samples", SampleModel)

        # Group each sample onto the plate
        return self.group_samples(plate, samples)

    def get_plate(self) -> PlateModel:
        plate_entry: ElnEntryStep = self.exp_handler.get_step("Plate Setup")
        record_id: int = int(plate_entry.get_options()["MultiLayerPlating_Plate_RecordIdList"])

        return self.rec_handler.query_models_by_id(PlateModel, [record_id])[0]

    def group_samples(self, plate: PlateModel, samples: list[SampleModel]) -> SapioWebhookResult:
        # Sort the list of samples by priority
        SampleUtils.sort_samples_by_priority(samples, self.rec_handler, self.rel_man, WGSAssayDetailModel)

        # Place each sample onto the plate
        pl_count: int = 0
        for s in samples:
            # Split the row and column positions
            row_col: list[str] = self.get_row_col_positions(pl_count)

            # Create a well element record
            self.create_well_element(s, plate, row_col)
            pl_count += 1
            if pl_count >= len(plate_locations):
                self.response_data["status"] = "error"
                return PopupUtil.ok_popup("Error", f"Plate {plate.get_PlateId_field()} does not have enough wells"
                                                   f"to contain all samples.",
                                          request_context=json.dumps(self.response_data))

        # Store and commit to get the record IDs of the aliquots
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def create_well_element(self, sample: SampleModel, plate: PlateModel, row_col: list[str]) -> None:

        pdwe: PlateDesignerWellElementModel = self.rec_handler.add_model(PlateDesignerWellElementModel)

        pdwe.set_PlateRecordId_field(plate.record_id)
        pdwe.set_Layer_field(1)

        pdwe.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
        pdwe.set_SourceRecordId_field(sample.record_id)

        pdwe.set_RowPosition_field(row_col[0])
        pdwe.set_ColPosition_field(row_col[1])

    # noinspection PyMethodMayBeStatic
    def get_row_col_positions(self, pl_count) -> list[str]:
        return plate_locations[pl_count].split(":")

    # noinspection PyMethodMayBeStatic
    def start_new_column(self, pl_count) -> int:
        return pl_count + (8 - pl_count)
