from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

HEADERS: list[str] = ["RackID", "LocationCell", "TubeCode"]


class GenerateTestIDReaderFile(OsloWebhookHandler):
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.exp_handler = ExperimentHandler(context)

        samples: list[SampleModel] = self.exp_handler.get_step_models(context.active_step.get_name(), SampleModel)

        file_data: str = ",".join(HEADERS) + "\n"

        for s in samples:
            file_data += (f"{s.get_StorageLocationBarcode_field()},"
                          f"{s.get_RowPosition_field()}{s.get_ColPosition_field()},{s.get_TubeBarcode_field()}\n")

        file_name: str = f"Rack File - {samples[0].get_StorageLocationBarcode_field()}.csv"

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(context, file_name, bytes(file_data, "utf-8"))
        )

        return FileUtil.write_file(file_name, bytes(file_data, "utf-8"))
