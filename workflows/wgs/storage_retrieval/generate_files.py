import re
from typing import Optional
from typing import TypeAlias

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import WriteFileResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import FamilyDetailsModel
from utilities.data_models import PatientModel
from utilities.data_models import PlateModel
from utilities.data_models import PrimerSampleModel
from utilities.data_models import SampleModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.sample_utils import SampleUtils
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler

file_t: TypeAlias = list[list[str]]


class MalformedDataException(Exception):
    pass


class GenerateWGSConfigs(OsloWebhookHandler):
    verso_storage: list[DataRecord] = []
    fridge_storage: list[DataRecord] = []
    rec_handler: RecordHandler
    callback_util: CallbackUtil

    def row_to_number(self, letter: str) -> str:
        """Convert a row letter to its corresponding number.

        Args:
            letter (str): The row letter.

        Returns:
            str: The corresponding row.
        """
        return str(ord(letter) - ord("A") + 1)

    def calculate_well_position(self, row: str, column: int) -> int:
        """Calculate the well position based on the row and column.

        Args:
            row (str): The row letter (e.g., 'A', 'B', ..., 'H').
            column (int): The column number (1 to 12).

        Returns:
            int: The well position number.
        """
        # Convert row letter to its corresponding index (A=0, B=1, ..., H=7)
        row_index = ord(row) - ord("A")
        well_position = row_index * 12 + column
        return well_position

    def generate_verso_file(self) -> Optional[list[list[str]]]:
        txt_contents: file_t = [["TubeId", "DestinationRack", "Row", "Column"]]

        for sample in self.verso_storage:
            tube_name = sample.get_field_value("OtherSampleId")
            row_position = sample.get_field_value("RowPosition")
            column_position = sample.get_field_value("ColPosition")

            well_position = ""
            row_to_number = ""

            if column_position and row_position:
                well_position = self.calculate_well_position(row_position, int(column_position)) // 96 + 1
                row_to_number = self.row_to_number(row_position)

            txt_contents.append([
                tube_name if tube_name else sample.get_field_value("TubeBarcode"),
                well_position,
                row_to_number,
                column_position,
            ])

        return None if len(txt_contents) == 1 else txt_contents

    def get_ark_pos(self, sample: SampleModel) -> str:
        return (
            (sample.get_StorageUnitPath_field() if sample.get_StorageUnitPath_field() else sample.get_PlateId_field())
                                                                                          + " - "
                                                                                          + sample.get_RowPosition_field()
                                                                                          + sample.get_ColPosition_field()
        )

    def get_family_id(self, sample: SampleModel) -> str:
        try:
            child = sample.get_children_of_type(FamilyDetailsModel)[0]
            return child.get_field_value("FamilyId")
        except IndexError:
            return ""

    def generate_fridge_file(self, context: SapioWebhookContext) -> str | None:
        xlsx_data: str = "NR,PRV_ID,FDSLNR,KONS,ARK_POS,FUNNET,Trio ID\n"

        # [OSLO-623]: Get all the patients for the samples and load their family details children outside the loop so
        # that the webhook doesn't time out.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.fridge_storage, SampleModel)
        if not samples:
            return None

        try:
            non_ctrl_samples = [s for s in samples if
                                not (
                                        s.get_ExemplarSampleType_field() == "In House" or s.get_ExemplarSampleType_field() == "Reagent" or s.get_ExemplarSampleType_field() == "Promega")]
            samples_to_patients: dict[SampleModel, PatientModel] = (
                SampleUtils.get_samples_to_patients(context, self.rec_handler, non_ctrl_samples))
        except IndexError:
            raise MalformedDataException("Given fridge samples have no related patient data")

        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_children_of_type(top_level_samples, FamilyDetailsModel)

        SampleUtils.sort_samples_by_well_positions(samples)
        for idx, sample in enumerate(samples):
            if sample in samples_to_patients:
                security_number_field = samples_to_patients[sample].get_SocialSecurityNumber_field()
            else:
                security_number_field = ""
            xlsx_data += (f"{idx + 1},{sample.get_OtherSampleId_field()},"
                          f"{security_number_field},"
                          f"{(round(sample.get_Concentration_field(), 3) if sample.get_Concentration_field() else None)},{self.get_ark_pos(sample)},,\n")

        xlsx_data += (",\n"
                      ",\n"
                      "Konroll av prøver:\n"
                      "1. Sjekk at prøvenummer (PRV_ID) og fødselsnummer er likt på DNA rør og\n"
                      "plukkliste når prøvene hentes (siste nummer kan være ulikt).\n"
                      "For Shire-prøver (D-nr) må arkivposisjon og fødselsdato sammenlignes på rør og plukkliste\n"
                      "2. Hvis det er for lite volum, be om reisolering i SWL og velg ny prøve\n"
                      "3. En erfaren labingeniør må sjekke at riktige prøver er plukket ved å sammenligne prøvenummer\n"
                      "og fødselsnummer i prøvelisten og signere for dette i Clarity (ved normaliseringssteget)\n"
                      "NB! Det siste sifferet i prøvenummeret kan være ulikt på rør og i prøveliste\n"
                      ",\n"
                      "DENNE LISTEN SKAL KUN BENYTTES TIL Å HENTE PRØVER")

        return xlsx_data

    def transfer_files(self, context: SapioWebhookContext, verso_file: file_t,
                       fridge_file: str) -> SapioWebhookResult:
        def to_bytes(file: file_t, sep: str) -> bytes:
            if file is None:
                return bytes()
            csv_bytes = ""
            for row in file:
                r = sep.join([str(x) for x in row])
                csv_bytes += r + "\r\n"

            return bytes(csv_bytes, "utf-8")

        # Get the sample.
        if verso_file:
            sample_datarecord: list[DataRecord] = self.verso_storage
        else:
            sample_datarecord: list[DataRecord] = self.fridge_storage
        samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_datarecord, SampleModel)

        # Get the plate parent, samples can only have one plate parent.
        self.rel_man.load_parents_of_type([samples[0]], PlateModel)
        plate: PlateModel | None = samples[0].get_parent_of_type(PlateModel)

        # Send the files to FileBridge to download them to the user upon failure.
        plate_id_field = plate.get_PlateId_field() if plate else None
        if verso_file:
            if plate:
                verso_filename = f"Verso_{plate_id_field}_{self.context.user.username}.txt"
            else:
                verso_filename = f"Verso_1_{self.context.user.username}.txt"

            verso_bytes: bytes = to_bytes(verso_file, ";")
            verso_success: bool = (
                FileBridgeUtils.upload_file(context, "oslo-filebridge", verso_filename, verso_bytes, ""))
            if not verso_success:
                self.callback_util.write_file(verso_filename, verso_bytes)

            if self.exp_handler.step_exists("Generated Verso File"):
                attach_file_to_entry(
                    FileAttachmentParams(
                        self.context,
                        verso_filename,
                        verso_bytes,
                        "Generated Verso File",
                        self.exp_handler,
                        self.inst_man,
                        self.rec_man
                    )
                )

        if fridge_file:
            if plate_id_field is None:
                template_name: str = self.exp_handler.get_experiment_template().template_name
                if template_name == "ELISA":
                    plate_id_field = samples[0].get_StorageLocationBarcode_field()
                elif template_name == "aCGH" or template_name == "Array Comparative Genomic Hybridization":
                    non_control = next(s for s in samples if s.get_ExemplarSampleType_field() not in ["Promega",
                                                                                                       "In House"] and not s.get_IsControl_field())
                    plate_id_field = non_control.get_PlateId_field() if non_control else ""
                else:
                    raise Exception("Plate not found for the samples")

            fridge_filename = f"Fridge_{plate_id_field}_{self.context.user.username}.xlsx"
            fridge_bytes: bytes = FileUtil.csv_to_xlsx(bytes(fridge_file, "utf-8"))
            fridge_success: bool = (
                FileBridgeUtils.upload_file(context, "oslo-filebridge", fridge_filename, fridge_bytes, ""))
            if not fridge_success:
                self.callback_util.write_file(fridge_filename, fridge_bytes)

            if self.exp_handler.step_exists("Generated Fridge File"):
                attach_file_to_entry(
                    FileAttachmentParams(
                        self.context,
                        fridge_filename,
                        fridge_bytes,
                        "Generated Fridge File",
                        self.exp_handler,
                        self.inst_man
                    )
                )

        return SapioWebhookResult(True)

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.callback_util = CallbackUtil(context)

        if isinstance(context.client_callback_result, WriteFileResult):
            return SapioWebhookResult(True)

        # Read the samples StorageLocationBarcode
        # This assumes every sample has the same barcode currently.
        sample_records: list[DataRecord] = get_records_from_entry_with_option(context, "OSLO SAMPLES TO PREP")
        if sample_records is None:
            return SapioWebhookResult(True, display_text="No appropriate sample records found")

        primers: list[DataRecord] = self.dr_man.query_all_records_of_type("Primer").result_list
        primer_records: list[DataRecord] = []

        # OSLO-545: Regex patterns for the different types of barcodes
        # start with an H, then any other letter, then any number of alphanumeric symbols
        verso_pattern = re.compile(r"^H[A-Z]\w+")
        swl_pattern = re.compile(r"^\d{11}")  # Exactly 11 digits
        shire_pattern = re.compile(r"^D\d{2}-\d{4}")  # start with a D, then two digits, then a dash, then 4 digits

        sample_models: list[SampleModel] = self.rec_handler.wrap_models(sample_records, SampleModel)
        self.rel_man.load_children_of_type(sample_models, PrimerSampleModel)
        self.rel_man.load_parents_of_type(sample_models, SampleModel)

        # Flush cached values.
        self.verso_storage = []
        self.fridge_storage = []

        # [OSLO-1121]: Get the template name to determine if the samples are from ELISA.
        template_name: str = self.exp_handler.get_experiment_template().template_name

        try:
            sample_models = SampleUtils.sort_samples_by_well_positions(sample_models)
            for sample in sample_models:
                # OSLO-654 - Detect if sample has a PrimerSample child
                primer_children = sample.get_children_of_type(PrimerSampleModel)

                if primer_children:
                    variant = primer_children[0].get_field_value("Variant")
                    relevant_primers = [p for p in primers if p.get_field_value("Variant") == variant]
                    if relevant_primers:
                        primer_records += relevant_primers

                barcode = sample.get_field_value("TubeBarcode")

                if verso_pattern.match(barcode):
                    self.verso_storage.append(sample.get_data_record())
                # [OSLO-1121]: Allow the fridge file to be generated for plasma samples in ELISA.
                elif template_name == "ELISA":
                    # Source samples should be included in the file, not the aliquots
                    parent_sample = sample.get_parent_of_type(SampleModel)
                    if parent_sample and parent_sample.get_SampleId_field() not in [s.get_field_value("SampleId") for s
                                                                                    in self.fridge_storage]:
                        self.fridge_storage.append(parent_sample.get_data_record())
                elif template_name == "aCGH" or template_name == "Array Comparative Genomic Hybridization":
                    if sample.get_ExemplarSampleType_field() != "Promega":
                        self.fridge_storage.append(sample.get_data_record())
                else:
                    # OSLO-1106 -Any number not in the Verso should be in the fridge pick file.
                    self.fridge_storage.append(sample.get_data_record())

            for primer in primer_records:
                barcode = primer.get_field_value("barcode")

                if verso_pattern.match(barcode):
                    self.verso_storage.append(primer)
                else:
                    self.fridge_storage.append(primer)

            if not len(self.verso_storage) and not len(self.fridge_storage):
                return SapioWebhookResult(True, display_text="No appropriate samples given")

            # If it's from the Verso Freezer, generate one file #OSLO-487
            # otherwise, generate the other file. #OSLO-488
            verso_file: Optional[file_t] = None
            fridge_file: str | None = None

            if self.verso_storage:
                verso_file = self.generate_verso_file()
            if self.fridge_storage:
                try:
                    fridge_file = self.generate_fridge_file(context)
                except MalformedDataException as e:
                    return SapioWebhookResult(False, display_text=str(e))

            if not verso_file and not fridge_file:
                return SapioWebhookResult(True, display_text="No file to generate - invalid sample barcodes")

            files = self.transfer_files(context, verso_file, fridge_file)
            return files
        except Exception as e:
            return SapioWebhookResult(True, display_text=f"Error: {str(e)}")
