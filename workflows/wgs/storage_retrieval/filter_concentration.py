from typing import Optional

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel
from utilities.utils import get_entry_by_option
from utilities.webhook_handler import OsloWebhookHandler


class FilterConcentration(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get the samples
        experiment_handler: ExperimentHandler = ExperimentHandler(context)
        mod_man: RecordModelManager = RecordModelManager(context.user)

        entry: Optional[ExperimentEntry] = get_entry_by_option(context, "OSLO SAMPLES TO PREP")
        assert isinstance(entry, ExperimentEntry)
        sample_entries: list[SampleModel] = experiment_handler.get_step_models(entry.entry_name, SampleModel)

        for smpl in sample_entries:
            if smpl.get_Concentration_field():
                smpl.set_Concentration_field(0.0)

        mod_man.store_and_commit()
        return SapioWebhookResult(True)
