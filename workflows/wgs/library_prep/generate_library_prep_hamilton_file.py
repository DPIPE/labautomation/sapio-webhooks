import datetime

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.webhook_handler import OsloWebhookHandler

WELL_LOCATIONS: list[str] = [
    'A01', 'A02', 'A03', 'A04', 'A05', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12',
    'B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B09', 'B10', 'B11', 'B12',
    'C01', 'C02', 'C03', 'C04', 'C05', 'C06', 'C07', 'C08', 'C09', 'C10', 'C11', 'C12',
    'D01', 'D02', 'D03', 'D04', 'D05', 'D06', 'D07', 'D08', 'D09', 'D10', 'D11', 'D12',
    'E01', 'E02', 'E03', 'E04', 'E05', 'E06', 'E07', 'E08', 'E09', 'E10', 'E11', 'E12',
    'F01', 'F02', 'F03', 'F04', 'F05', 'F06', 'F07', 'F08', 'F09', 'F10', 'F11', 'F12',
    'G01', 'G02', 'G03', 'G04', 'G05', 'G06', 'G07', 'G08', 'G09', 'G10', 'G11', 'G12',
    'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'H07', 'H08', 'H09', 'H10', 'H11', 'H12'
]


class GenerateLibraryPrepHamiltonFile(OsloWebhookHandler):
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.exp_handler = ExperimentHandler(context)

        # Get all the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Write the file data
        file_data: str = ("Version 2\n"
                          f"ID\t{context.eln_experiment.notebook_experiment_id}\n"
                          f"Assay\t{self.exp_handler.get_template_name()}\n"
                          f"IndexReads\t2\n"
                          f"IndexCycles\t8\n")

        for well_location in WELL_LOCATIONS:
            matching_sample: SampleModel = self.get_sample_from_well_location(well_location, samples)
            if matching_sample is None:
                file_data += f"{well_location}\n"

            else:
                file_data += (f"{well_location} {matching_sample.get_OtherSampleId_field()}\t"
                              f"D7{well_location[1:len(well_location)]}\tD50{ord([well_location[0:1]]) - 64}\n")

        file_data += "[AssaySettings]"

        # Send the file to FileBridge, or download the file if the FileBridge upload fails
        file_name: str = f"Hamilton-{str(datetime.date.today())}.txt"
        file_bytes: bytes = bytes(file_data, "utf-8")

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(context, file_name, file_bytes)
        )

        success: bool = FileBridgeUtils.upload_file(context, "oslo-filebridge", file_name, file_bytes, "Hamilton Star")
        if not success:
            return FileUtil.write_file(file_name, file_bytes)

        return SapioWebhookResult(True)

    def get_sample_from_well_location(self, well_location: str, samples: list[SampleModel]) -> SampleModel:
        for s in samples:
            # Add a zero in front of the column number if the number is a single digit (in order to match with values)
            sample_col: str = s.get_ColPosition_field()
            if int(sample_col) < 10:
                sample_col = f"0{sample_col[0]}"

            # Compare the two
            row_col: str = f"{s.get_RowPosition_field()}{sample_col}"

            if row_col == well_location:
                return s

        return None
