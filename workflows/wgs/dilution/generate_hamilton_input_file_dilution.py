from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.instrument_utils import InstrumentUtils
from utilities.sequencing_utils import SequencingUtils
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils

NOVA_SEQ_6000_FILE_NAME: str = "Poolname-#.csv"
NOVA_SEQ_X_FILE_NAME: str = "NoveSeqX-#.csv"

PLATE_LOCATIONS: list[str] = [
    'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1',
    'A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7',
]


class GenerateHamiltonInputFileDilution(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler

    instrument_utils: InstrumentUtils

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        self.instrument_utils = InstrumentUtils(context)

        # Get the sequencer type from the entry
        pools: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        sequencer_type: str = SequencingUtils.get_sequencer_type(self.rel_man, pools)

        # Get the respective dilution details entry's record
        dilution_details_entry: ElnEntryStep = (
            self.exp_handler.get_step(f"Robot File Generation ({sequencer_type})"))
        records: list[DataRecord] = (
            self.eln_man.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                    dilution_details_entry.eln_entry.entry_id).result_list)

        # Generate the respective file for the sequencer type
        file_datas: dict[str, bytes] = {}
        if sequencer_type == "NovaSeq 6000":
            file_datas = self.generate_novaseq_6000_hamilton_file(records)

        else:
            file_datas = self.generate_novaseq_x_hamilton_file(records, pools)

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                list(file_datas.keys())[0],
                list(file_datas.values())[0],
            )
        )

        # Download the file to the user
        return self.download_files(context, file_datas)

    def generate_novaseq_6000_hamilton_file(self, records: list[DataRecord]) -> dict[str, bytes]:
        file_datas: dict[str, bytes] = {}
        for r in records:
            file_data: str = ",".join(SequencingUtils.HAMILTON_HEADERS_NOVASEQ_6000) + "\n"
            file_data += (
                f"{r.get_field_value('SampleId').replace(',', '|')},{r.get_field_value('SourceVolumeToUse')},"
                f"{r.get_field_value('DiluentToUse')},{r.get_field_value('PhiXv3uL')},"
                f"{r.get_field_value('TargetVolume')},77.5,77.5\n")
            file_datas[NOVA_SEQ_6000_FILE_NAME.replace("Poolname",
                                                       r.get_field_value("SampleId"))
            .replace("#", TimeUtil.now_in_format("%m.%d.%Y %H.%M"))] = bytes(file_data, "utf-8")

        return file_datas

    def generate_novaseq_x_hamilton_file(self, records: list[DataRecord], pools: list[SampleModel]) -> dict[str, bytes]:
        file_data: str = ",".join(SequencingUtils.HAMILTON_HEADERS_NOVASEQ_X) + "\n"

        self.rel_man.load_parents_of_type(pools, PlateModel)
        pool_count: int = 1
        tube_count: int = 1
        for r in records:
            pool: SampleModel = [p for p in pools if p.get_SampleId_field() == r.get_field_value("SampleId")][0]
            source_plate: str = self.get_source_plate_field(pool)
            file_data += f"{source_plate},"
            if "Tube" not in source_plate:
                file_data += f"{pool.get_RowPosition_field()}{pool.get_ColPosition_field()},"
            else:
                file_data += f"{tube_count},"
                tube_count += 1

            file_data += (
                f"{PLATE_LOCATIONS[pool_count - 1]},Pool #{pool_count},{r.get_field_value('SourceVolumeToUse')},"
                f"{r.get_field_value('RSBBuffer')}\n")
            pool_count += 1

        return {NOVA_SEQ_X_FILE_NAME.replace("#", TimeUtil.now_in_format("%m.%d.%Y %H.%M")):
                    bytes(file_data, "utf-8")}

    def get_source_plate_field(self, pool: SampleModel) -> str:
        plates: list[PlateModel] = pool.get_parents_of_type(PlateModel)
        if plates is None or len(plates) == 0:
            return "Tube"

        storage_location_barcode: str = pool.get_StorageLocationBarcode_field()
        for p in plates:
            if p.get_PlateId_field() == storage_location_barcode:
                return storage_location_barcode

        return "Tube"

    def download_files(self, context: SapioWebhookContext, file_datas: dict[str, bytes]) -> SapioWebhookResult:
        instrument_errors: dict[str, str] = {}
        failed_file_bridge_files: dict[str, bytes] = {}
        exp_id: int = context.eln_experiment.notebook_experiment_id

        for key in file_datas.keys():
            try:
                FileBridge.write_file(context, "oslo-filebridge", key, file_datas[key])
            except Exception as e:
                failed_file_bridge_files[key] = file_datas[key]
                instrument_errors[key] = repr(e)

        if len(failed_file_bridge_files) > 0:
            return self.download_to_browser(failed_file_bridge_files, instrument_errors, exp_id)
        return SapioWebhookResult(True, "Files successfully sent to FileBridge location.")

    def download_to_browser(self, file_datas: dict[str, bytes], instrument_errors: dict[str, str],
                            exp_id: int) -> SapioWebhookResult:
        # Get the instrument name. If there is no instrument selected, leave it blank.
        instrument_tracking_entry: ElnEntryStep = WorkflowUtils.get_instrument_tracking_entry(self.exp_handler)
        try:
            instrument_name: str = self.eln_man.get_data_records_for_entry(
                exp_id,
                instrument_tracking_entry.eln_entry.entry_id
            ).result_list[0].get_field_value("InstrumentUsed")
        except Exception:
            instrument_name: str = ""

        # Write an error to the instrument error log
        for key in instrument_errors.keys():
            self.instrument_utils.log_instrument_error(instrument_name, key, instrument_errors[key])

        # Download the files to the user
        return FileUtil.write_files(file_datas)
