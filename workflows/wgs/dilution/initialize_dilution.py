import json

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.SapioELNEnums import ElnExperimentStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import AssignedProcessModel, RequestModel
from utilities.data_models import SampleModel

from utilities.sample_utils import SampleUtils
from utilities.sequencing_utils import SequencingUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS_ERROR: str = "error"
STATUS_INVALID_SEQUENCES: str = "invalidsequencers"
WORKFLOW_NAME: str = "Denature & Dilution"


class InitializeDilution(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        self.response_map = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            return self.client_callback_handler(context, result)

        # Get the sequencer that's being used from the pools
        pools: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        sequencer_type: str = SequencingUtils.get_sequencer_type(self.rel_man, pools)
        if sequencer_type is None:
            self.response_map["status"] = STATUS_INVALID_SEQUENCES
            return PopupUtil.ok_popup("Error", "Not all pools are using the same sequencers. This experiment will be"
                                               " cancelled and all the pools will be sent back to the queue.",
                                      request_context=json.dumps(self.response_map))

        # Hide the entries that do not contain the instrument type in the INSTRUMENT USED tag
        self.hide_irrelevant_entries(sequencer_type, context.eln_experiment.notebook_experiment_id)

        return SapioWebhookResult(True)

    def client_callback_handler(self, context: SapioWebhookContext, result: ClientCallbackResult) -> SapioWebhookResult:
        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_map = json.loads(result.callback_context_data)

        if self.response_map["status"] == STATUS_ERROR:
            return SapioWebhookResult(True)

        if self.response_map["status"] == STATUS_INVALID_SEQUENCES:
            self.send_pools_back_to_queue(context)
            self.exp_handler.update_experiment(experiment_status=ElnExperimentStatus.Canceled)

            return SapioWebhookResult(True)

    def hide_irrelevant_entries(self, sequencer_type: str, exp_id: int) -> None:
        entries: list[ExperimentEntry] = self.eln_man.get_experiment_entry_list(exp_id)

        for e in entries:
            options: dict[str, str] = self.eln_man.get_experiment_entry_options(exp_id, e.entry_id)

            if SequencingUtils.TAG_SEQUENCER_TYPE in options:
                tag_val: str = options[SequencingUtils.TAG_SEQUENCER_TYPE]

                if tag_val is not None and sequencer_type not in tag_val:
                    self.exp_handler.update_step(e.entry_name, is_hidden=True)

    def send_pools_back_to_queue(self, context: SapioWebhookContext) -> None:
        pools: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        self.rel_man.load_parents_of_type(pools, AssignedProcessModel)

        for p in pools:
            # Set the status of the pool
            statuses: list[str] = p.get_ExemplarSampleStatus_field().split(",")
            new_statuses: list[str] = []
            for status in statuses:
                if WORKFLOW_NAME not in status and status not in new_statuses:
                    new_statuses.append(status)

            p.set_ExemplarSampleStatus_field(",".join(new_statuses))

            # Set the status of the pool's respective assigned process
            assigned_processes: list[AssignedProcessModel] = p.get_parents_of_type(AssignedProcessModel)
            for a in assigned_processes:
                if a.get_Status_field() is not None and WORKFLOW_NAME in a.get_Status_field():
                    a.set_Status_field("")
                    p.remove_parent(a)

        # Store and commit changes to remove the samples from the queue
        self.rec_man.store_and_commit()

        # Send the samples back to the queue
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, pools)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        for p in pools:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [p], SequencingUtils.PROCESS_NAME,
                                              SequencingUtils.SEQUENCING_STEP_NUM, None,
                                              [t.get_parent_of_type(RequestModel) for t in top_level_samples if
                                                       t.get_SampleId_field() == p.get_TopLevelSampleId_field()][0])
