from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel
from utilities.data_models import SequencingMetadataModel
from utilities.sequencing_utils import SequencingUtils
from utilities.webhook_handler import OsloWebhookHandler


class Dilution(OsloWebhookHandler):
    exp_handler: ExperimentHandler
    eln_man: ElnManager
    data_record_man: DataRecordManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)
        self.data_record_man = DataMgmtServer.get_data_record_manager(context.user)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get the sequencer type from the entry tag
        pools: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        sequencer_type: str = SequencingUtils.get_sequencer_type(self.rel_man, pools)

        # Get or create the sample detail records from the sample details entry
        records: list[DataRecord] = (
            self.get_or_create_sample_details_records(sequencer_type, context.eln_experiment.notebook_experiment_id))

        # Calculate the dilution values
        self.calculate_dilution_values(pools, records, sequencer_type)

        return SapioWebhookResult(True)

    def get_sequencer_type(self, pool: SampleModel) -> str:
        self.rel_man.load_parents_of_type([pool], SequencingMetadataModel)
        return pool.get_parent_of_type(SequencingMetadataModel).get_SequencerType_field()

    def get_or_create_sample_details_records(self, sequencer_type: str, exp_id: int) -> list[DataRecord]:
        sample_details_entry: ElnEntryStep = (
            self.exp_handler.get_step(f"{SequencingUtils.ENTRY_NAME_TUBE_AMOUNTS_PER_SAMPLE} ({sequencer_type})"))
        return self.eln_man.get_data_records_for_entry(exp_id, sample_details_entry.eln_entry.entry_id).result_list

    def calculate_dilution_values(self, pools: list[SampleModel], records: list[DataRecord],
                                  sequencer_type: str) -> None:
        target_vol: float
        target_conc: float
        src_vol_to_use: float
        diluent_to_use: float
        phix_2_5_nm: float
        phix_v3: float
        phix_vol: float
        excess_vol: float
        rsb_buffer: float
        src_vol: float
        src_conc: float

        if sequencer_type == SequencingUtils.SEQUENCER_NOVASEQ_6000:
            for p in pools:
                record: DataRecord = [r for r in records if r.get_field_value("SampleId") == p.get_SampleId_field()][0]

                target_vol = float(record.get_field_value("TargetVolume"))
                target_conc = float(record.get_field_value("TargetConcentration"))
                src_vol = float(record.get_field_value("SourceVolume"))
                src_conc = float(record.get_field_value("SourceConcentration"))
                excess_vol = float(record.get_field_value("ExcessVolume"))
                phix_2_5_nm = bool(record.get_field_value("PhiX2_5nM"))

                src_vol_to_use = src_vol * target_conc / src_conc
                record.set_field_value("SourceVolumeToUse", src_vol_to_use)

                diluent_to_use = (target_vol + excess_vol) - src_vol_to_use
                if diluent_to_use < 0:
                    diluent_to_use = 0
                record.set_field_value("DiluentToUse", diluent_to_use)

                if phix_2_5_nm:
                    phix_v3 = (target_conc / 100) * (target_vol + excess_vol) / SequencingUtils.PHIX_2_5_CONSTANT
                    record.set_field_value("PhiXv3uL", phix_v3)

        else:
            for p in pools:
                record: DataRecord = [r for r in records if r.get_field_value("SampleId") == p.get_SampleId_field()][0]

                target_vol = float(record.get_field_value("TargetVolume"))
                target_conc = float(record.get_field_value("TargetConcentration"))
                phix_vol = float(record.get_field_value("PhiXVolume"))
                src_conc = float(record.get_field_value("SourceConcentration"))

                src_vol_to_use = (target_conc / 1000) * target_vol / src_conc
                record.set_field_value("SourceVolumeToUse", src_vol_to_use)

                rsb_buffer = SequencingUtils.RSB_CONSTANT - src_vol_to_use - phix_vol
                record.set_field_value("RSBBuffer", rsb_buffer)

        # Commit changes
        self.data_record_man.commit_data_records(records)
