from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ELNExperimentDetailModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class AddReagentVolume(OsloWebhookHandler):
    # Entry Names
    REAGENT_TRACKING = "Reagent Tracking"
    ALIQUOT_SAMPLE_TABLE_1 = "Aliquoted Samples"
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        exp_handler = ExperimentHandler(context)

        # get reagent total volumes
        reagent_step = exp_handler.get_step(self.REAGENT_TRACKING)
        eln_detail_records = reagent_step.get_records()
        eln_detail_models = self.inst_man.add_existing_records_of_type(eln_detail_records, ELNExperimentDetailModel)
        total_volume = 0
        for eln_detail_model in eln_detail_models:
            if eln_detail_model.get_field_value("ConsumableQty"):
                total_volume += eln_detail_model.get_field_value("ConsumableQty")

        # get samples
        aliquot_step = exp_handler.get_step(self.ALIQUOT_SAMPLE_TABLE_1)
        sample_records = aliquot_step.get_records()
        sample_models = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # update sample modules
        volume_to_add = total_volume/len(sample_models)
        for sample_model in sample_models:
            sample_model.set_Volume_field(sample_model.get_Volume_field()+volume_to_add)
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
