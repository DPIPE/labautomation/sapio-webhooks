from typing import Any
from typing import Optional

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import AbstractVeloxFieldDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import ListMode
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxSelectionFieldDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.constants import Constants
from utilities.data_models import FamilyDetailsModel, WGSAssayDetailModel, ELNExperimentDetailModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.pooling_utils import PoolingUtils
from utilities.sample_utils import SampleUtils
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler


class SamplePoolingHandler:
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    rel_man: RecordModelRelationshipManager

    tube_samples: dict[str, list[SampleModel]]
    plate_samples: dict[str, list[SampleModel]]
    plate_id: str
    well_count: int

    def __init__(self, rec_handler: RecordHandler, acc_manager: AccessionManager, plate_id: str):
        self.rec_handler = rec_handler
        self.acc_manager = acc_manager
        self.plate_id = plate_id
        self.tube_samples = {}
        self.plate_samples = {}
        self.pooling_count = 0
        self.plate_pooling_count = 0
        self.prev_family_id = 0
        self.family_count = 0
        self.well_count = 0
        self.num_tube_pools = 1
        self.curr_well_index = 0

    def pool_sample(self, smpl: SampleModel, dest_type: str, high_point: int) -> None:
        if dest_type == PoolingUtils.DEST_TYPE_TUBE:
            if len(self.tube_samples.keys()) == 0:
                self.tube_samples[self.new_tube()] = []

            if self.pooling_count >= high_point:
                self.tube_samples[self.new_tube()] = []
                self.pooling_count = 0

            curr_tube = list(self.tube_samples)[-1]

            self.tube_samples[curr_tube].append(smpl)

            self.pooling_count += 1

        if dest_type == PoolingUtils.DEST_TYPE_PLATE:
            if len(self.plate_samples.keys()) == 0:
                self.plate_samples[self.new_well()] = []

            if self.plate_pooling_count >= high_point:
                self.plate_samples[self.new_well()] = []
                self.plate_pooling_count = 0

            curr_tube = list(self.plate_samples)[-1]

            self.plate_samples[curr_tube].append(smpl)

            self.plate_pooling_count += 1

        # familys are sets of 3 - we won't fit a whole family in the
        # remaining space in that tube, so we need a new one
        if self.pooling_count >= (high_point - 2) and self.family_count == 0:
            self.tube_samples[self.new_tube()] = []
            self.pooling_count = 0

        curr_family_id: str | None = self.get_family_id(smpl)
        if not curr_family_id:
            self.pooling_count += 1
            return
        elif curr_family_id != self.prev_family_id:
            self.family_count = 0
            self.prev_family_id = curr_family_id
        else:
            self.family_count += 1

        self.pooling_count += 1

    def new_tube(self) -> str:
        tube: str = f"Tube {self.num_tube_pools}"

        self.num_tube_pools += 1

        return tube

    def new_well(self) -> str:
        well: str = f"{self.plate_id}-{PoolingUtils.PLATE_LOCATIONS[self.curr_well_index]}"

        self.curr_well_index += 1

        return well

    def get_family_id(self, smpl: SampleModel) -> str | None:
        # [OSLO-1017]: Changed Family ID field from int to string.
        family_details: FamilyDetailsModel | None = smpl.get_child_of_type(FamilyDetailsModel)
        return None if not family_details else family_details.get_FamilyId_field()


# https://github.com/sapiosciences/sapio-py-tutorials/blob/master/7_webhook_server.py#L37
class PoolingLogic(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    acc_manager: AccessionManager
    eln_man: ElnManager
    data_record_man: DataRecordManager
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.data_record_man = DataMgmtServer.get_data_record_manager(context.user)
        self.callback_util = CallbackUtil(context)

        # [OSLO-1024]: Check to make sure there are samples to pool.
        samples: list[SampleModel] | None = self.exp_handler.get_step_models("Samples", SampleModel)
        if not samples:
            self.callback_util.ok_dialog("Error", "There are no samples in this experiment, hence pooling cannot"
                                                  " occur.")
            return SapioWebhookResult(True)

        # Get chosen instrument (We'll need this on either branch below)
        # [OSLO-1024]: Improved None check.
        instrument_records: list[DataRecord] | None = (
            get_records_from_entry_with_option(context, Constants.TAG_INSTRUMENT_TRACKING))
        try:
            instrument_type: str = [r.get_field_value("InstrumentType") for r in instrument_records
                                    if r.get_field_value("InstrumentUsed") is not None
                                    and r.get_field_value("InstrumentUsed") != ""][0]
        except Exception:
            self.callback_util.ok_dialog("Error", "An instrument is required for pooling to occur.")
            return SapioWebhookResult(True)

        # Prompt the form to enter pooling fields.
        form_results: dict[str, Any] = self.prompt_form(instrument_type)
        pool_vol: float = float(form_results["pool_volume"])
        pool_mol: float = float(form_results["pool_molarity"])
        plate_id: str = form_results["plate_id"]
        if plate_id == "":
            plate_id = f"pooling_plate_{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo('pooling_plate_'))[0]}"

        # Put each sample in its respective pool
        sph: SamplePoolingHandler = SamplePoolingHandler(self.rec_handler, self.acc_manager, plate_id)
        volumes, invalid_ranges = self.pool_samples(samples, form_results, sph, instrument_type, pool_vol, pool_mol)

        # Populate the Pools entry
        tube_pools_to_samples: dict[str, list[SampleModel]] = sph.tube_samples
        plate_pools_to_samples: dict[str, list[SampleModel]] = sph.plate_samples

        # Populate the Pooling Details entry
        self.populate_pooling_details(tube_pools_to_samples, plate_pools_to_samples, volumes, pool_vol, pool_mol,
                                      context.eln_experiment.notebook_experiment_id)

        # Show a list of invalid volumes if there are any.
        if len(invalid_ranges) > 0:
            self.callback_util.table_dialog("Invalid Volumes", "The following samples have volumes outside the"
                                                               " range of 1.5 uL and 18 uL. If you do not readjust"
                                                               " the pooling volume and molarity, you will have"
                                                               " to pipette manually.",
                                            [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID"),
                                             VeloxDoubleFieldDefinition("Volume", "Volume", "Volume")],
                                            invalid_ranges)

        return SapioWebhookResult(True)

    def prompt_form(self, instrument_type: str) -> dict[str, Any]:
        # set default values
        default_vol: float = 0.0
        default_mol: float = 0.0
        default_dropdown_val: Optional[str] = None

        if instrument_type == PoolingUtils.NOVASEQ_6000_INSTRUMENT_NAME:
            default_vol = 330.0
            default_mol = 1.2
            default_dropdown_val = PoolingUtils.DEST_TYPE_TUBE

        elif instrument_type == PoolingUtils.NOVASEQ_X_INSTRUMENT_NAME:
            default_vol = 100.0
            default_mol = 2.0

        # Create fields for the form to display
        fields: list[AbstractVeloxFieldDefinition] = []

        pool_volume_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("PoolingForm", "pool_volume",
                                                                                   "Pool Volume")
        pool_volume_field.editable = True
        pool_volume_field.required = True
        fields.append(pool_volume_field)

        pool_molarity_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("PoolingForm", "pool_molarity",
                                                                                     "Pool Molarity")
        pool_molarity_field.editable = True
        pool_molarity_field.required = True
        fields.append(pool_molarity_field)

        dest_type_field: VeloxSelectionFieldDefinition = VeloxSelectionFieldDefinition("PoolingForm",
                                                                                       "destination_type",
                                                                                       "Destination Type",
                                                                                       ListMode.LIST,
                                                                                       pick_list_name="WGS Pooling Destination")
        dest_type_field.required = True

        if instrument_type == PoolingUtils.NOVASEQ_6000_INSTRUMENT_NAME:
            dest_type_field.editable = False
        else:
            dest_type_field.editable = True

        fields.append(dest_type_field)

        plate_id_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("PoolingForm", "plate_id", "Plate ID")

        if instrument_type == PoolingUtils.NOVASEQ_6000_INSTRUMENT_NAME:
            plate_id_field.editable = False
        else:
            plate_id_field.editable = True

        fields.append(plate_id_field)

        values: dict[str, Any] = {
            "pool_volume": default_vol,
            "pool_molarity": default_mol,
            "destination_type": default_dropdown_val,
            "plate_id": ""
        }

        return self.callback_util.form_dialog("Pooling", f"Enter pooling details for {instrument_type}.", fields,
                                              values, data_type="PoolingForm")

    def pool_samples(self, samples: list[SampleModel], form_results: dict[str, Any], sph: SamplePoolingHandler,
                     instrument_type: str, pool_vol: float,
                     pool_mol: float) -> (dict[SampleModel, float], list[dict[str, Any]]):
        volumes: dict[SampleModel, float] = {}
        invalid_ranges: list[dict[str, Any]] = []
        high_point: int = 0

        # Sort the samples by priority before pooling
        SampleUtils.sort_samples_by_priority(samples, self.rec_handler, self.rel_man, WGSAssayDetailModel)

        # [OSLO-1017]: Load all family details children of the samples.
        self.rel_man.load_children_of_type(samples, FamilyDetailsModel)

        for smpl in samples:
            # Put samples in tubes based on machine
            if instrument_type == PoolingUtils.NOVASEQ_6000_INSTRUMENT_NAME:
                high_point = 24
                dest: str = PoolingUtils.DEST_TYPE_TUBE
            else:   # [OSLO-1024]: Ensure that other instrument types outside the NovaSeq family can be used.
                dest: str = form_results["destination_type"]
                if dest == PoolingUtils.DEST_TYPE_TUBE:
                    high_point = 64
                elif dest == PoolingUtils.DEST_TYPE_PLATE:
                    high_point = 8

            # Add the sample to the pool
            sph.pool_sample(smpl, dest, high_point)

        # Calculate the sample volumes to use
        tube_pools_to_samples: dict[str, list[SampleModel]] = sph.tube_samples
        plate_pools_to_samples: dict[str, list[SampleModel]] = sph.plate_samples

        for s in samples:
            try:
                pool_samples: list[SampleModel] = [tube_pools_to_samples[p] for p in tube_pools_to_samples
                                                   if s in tube_pools_to_samples[p]][0]
            except Exception:
                pool_samples: list[SampleModel] = [plate_pools_to_samples[p] for p in plate_pools_to_samples
                                                   if s in plate_pools_to_samples[p]][0]

            sample_vol, invalid_range = PoolingUtils.calculate_volume_to_use(s, pool_mol, pool_vol, len(pool_samples))
            volumes[s] = sample_vol

            if invalid_range is not None:
                invalid_ranges.append(invalid_range)

        return volumes, invalid_ranges

    def populate_pooling_details(self, tube_pools_to_samples: dict[str, list[SampleModel]],
                                 plate_pools_to_samples: dict[str, list[SampleModel]],
                                 volumes: dict[SampleModel, float], pool_vol: float, pool_mol: float,
                                 exp_id: int) -> None:

        pooling_details_entry: ElnEntryStep = self.exp_handler.get_step("Pooling Details")
        pooling_details_data_type_name: str = pooling_details_entry.get_data_type_names()[0]

        existing_records: list[DataRecord] = (
            self.eln_man.get_data_records_for_entry(exp_id, pooling_details_entry.eln_entry.entry_id).result_list)
        pools: list[str] = [p for p in tube_pools_to_samples] + [p for p in plate_pools_to_samples]
        top_level_samples: list[SampleModel] = self.get_top_level_pool_samples(tube_pools_to_samples,
                                                                               plate_pools_to_samples, pools)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        orders: list[RequestModel] = []
        for sample in top_level_samples:
            orders.append(sample.get_parent_of_type(RequestModel))
        if orders:
            self.rel_man.load_children_of_type(orders, WGSAssayDetailModel)

        new_records: list[ELNExperimentDetailModel] = []

        for pool in pools:
            try:
                samples: list[SampleModel] = tube_pools_to_samples[pool]
            except Exception:
                samples: list[SampleModel] = plate_pools_to_samples[pool]

            familial_records: list[ELNExperimentDetailModel] = []
            orphan_records: list[ELNExperimentDetailModel] = []

            for s in samples:
                try:
                    top_level_sample: SampleModel = [t for t in top_level_samples
                                                     if t.get_SampleId_field() == s.get_TopLevelSampleId_field()][0]
                    order: RequestModel | None = top_level_sample.get_parent_of_type(RequestModel)
                except IndexError:
                    order = None

                try:
                    record: DataRecord = [r for r in existing_records
                                          if r.get_field_value("SampleId") == s.get_SampleId_field()][0]
                except Exception:
                    record: ELNExperimentDetailModel = self.exp_handler.add_eln_row(pooling_details_entry, ELNExperimentDetailModel)
                    record.set_field_value("SampleId", s.get_SampleId_field())
                    new_records.append(record)

                record.set_field_value("SampleVolume", float(volumes[s]))
                record.set_field_value("TargetPool", pool)
                record.set_field_value("PoolVolume", pool_vol)
                record.set_field_value("PoolMolarity", pool_mol)

                # For the purposes of sorting
                if order is None:
                    orphan_records.append(record)
                else:
                    try:
                        record.set_field_value("Priority",
                                               order.get_child_of_type(WGSAssayDetailModel).get_Priority_field())
                    except Exception:
                        ...
                    familial_records.append(record)

            familial_records += orphan_records

            # Store and commit new records and add them to the entry
            if len(familial_records) > 0:
                self.rec_man.store_and_commit()

            # Commit any existing records
            if len(existing_records) > 0:
                self.data_record_man.commit_data_records(existing_records)

    def get_top_level_pool_samples(self, tube_pools_to_samples: dict[str, list[SampleModel]],
                                   plate_pools_to_samples: dict[str, list[SampleModel]],
                                   pools: list[str]) -> list[SampleModel]:
        all_pool_samples: list[SampleModel] = []
        for pool in pools:
            try:
                all_pool_samples.extend(tube_pools_to_samples[pool])
            except Exception:
                all_pool_samples.extend(plate_pools_to_samples[pool])

        return SampleUtils.get_top_level_samples(self.rec_handler, all_pool_samples)
