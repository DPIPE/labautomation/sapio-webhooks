import io

import xlwt
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from xlwt import Workbook
from xlwt import Worksheet

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.pooling_utils import PoolingUtils
from utilities.webhook_handler import OsloWebhookHandler


class GenerateHamiltonInputAndPipetteFilePooling(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        # Get the records from the pooling details entry
        exp_id: int = context.eln_experiment.notebook_experiment_id

        pooling_details_entry: ElnEntryStep = self.exp_handler.get_step("Pooling Details")
        records: list[DataRecord] = (
            self.eln_man.get_data_records_for_entry(exp_id, pooling_details_entry.eln_entry.entry_id).result_list)

        # Get the values for each record, and then add it to the appropriate file depending on the pool volume
        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        pipette_file_data: str = ",".join(PoolingUtils.HEADERS_PIPETTE_FILE) + "\n"

        # Create the workbook for the hamilton file, since it needs to be an .xls file
        workbook: Workbook = xlwt.Workbook()
        sheet: Worksheet = workbook.add_sheet("Sheet1")

        # Write the headers of the hamilton file
        self.write_hamilton_headers(sheet)

        hamilton_index: int = 1
        for r in records:
            sample_vol: float = float(r.get_field_value("SampleVolume"))

            if PoolingUtils.MIN_HAMILTON_POOL_VOL <= sample_vol <= PoolingUtils.MAX_HAMILTON_POOL_VOL:
                self.append_hamilton_data(r, [s for s in samples
                                              if s.get_SampleId_field() == r.get_field_value("SampleId")][0],
                                          sheet, hamilton_index)
                hamilton_index += 1

            else:
                pipette_file_data += self.append_pipette_data(r)

        # Download the files
        return self.download_files(context, workbook, pipette_file_data, exp_id)

    def write_hamilton_headers(self, sheet: Worksheet) -> None:
        hamilton_col_index: int = 0
        for x in PoolingUtils.HEADERS_HAMILTON_INPUT_FILE:
            sheet.write(0, hamilton_col_index, x)
            hamilton_col_index += 1

    def append_hamilton_data(self, record: DataRecord, sample: SampleModel, sheet: Worksheet, index: int) -> None:
        target_pool: str = record.get_field_value("TargetPool")
        if "Tube" in target_pool:
            dest_well: str = target_pool.replace("Tube", "")

        else:
            target_pool: list[str] = record.get_field_value('TargetPool').split('-')
            dest_well: str = f"{target_pool[len(target_pool) - 1]}"

        sheet.write(index, 0, sample.get_StorageLocationBarcode_field())
        sheet.write(index, 1, f"{sample.get_RowPosition_field()}{sample.get_ColPosition_field()}")
        sheet.write(index, 2, dest_well)
        sheet.write(index, 3, str(record.get_field_value("SampleVolume")))
        sheet.write(index, 4, str(record.get_field_value("PoolVolume")))

    def append_pipette_data(self, record: DataRecord) -> str:
        return (f"{record.get_field_value('SampleId')},{record.get_field_value('SampleName')},"
                f"{record.get_field_value('SampleVolume')},{record.get_field_value('TargetPool')},"
                f"{record.get_field_value('PoolVolume')},{record.get_field_value('PoolMolarity')}\n")

    def download_files(self, context: SapioWebhookContext, hamilton_workbook: Workbook, pipette_file_data: str,
                       exp_id: int) -> SapioWebhookResult:
        # Format the file names
        date: str = TimeUtil.now_in_format("%d.%m.%Y %H.%M")

        hamilton_file_name: str = PoolingUtils.HAMILTON_INPUT_FILE_NAME.replace("[experiment ID]", str(exp_id)).replace("[date]", date)
        pipette_file_name: str = PoolingUtils.PIPETTE_FILE_NAME.replace("[experiment ID]", str(exp_id)).replace("[date]", date)

        # Convert the file data strings to bytes
        buffer = io.BytesIO()
        hamilton_workbook.save(buffer)

        hamilton_file_bytes: bytes = buffer.getvalue()
        pipette_file_bytes: bytes = bytes(pipette_file_data, "utf-8")

        # Download the files to FileBridge or the web browser
        unsent_files: list[str] = []
        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", hamilton_file_name, hamilton_file_bytes,
                                           PoolingUtils.HAMILTON_STAR_INSTRUMENT_NAME):
            unsent_files.append(hamilton_file_name)
            FileUtil.write_file(hamilton_file_name, hamilton_file_bytes)

        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", pipette_file_name, pipette_file_bytes, ""):
            unsent_files.append(pipette_file_name)
            FileUtil.write_file(pipette_file_name, pipette_file_bytes)

        if len(unsent_files) == 2:
            return FileUtil.write_files({hamilton_file_name: hamilton_file_bytes,
                                         pipette_file_name: pipette_file_bytes})

        elif len(unsent_files) == 1:
            if unsent_files[0] == hamilton_file_name:
                return FileUtil.write_file(hamilton_file_name, hamilton_file_bytes)

            else:
                return FileUtil.write_file(pipette_file_name, pipette_file_bytes)

        # Add hamilton file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                hamilton_file_name,
                hamilton_file_bytes
            )
        )

        return SapioWebhookResult(True)
