from typing import cast

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.webhook_handler import OsloWebhookHandler


class VerifyWGSPoolingInstrumentTrackingSubmission(OsloWebhookHandler):
    exp_handler: ExperimentHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.exp_handler = ExperimentHandler(context)
        self.callback_util = CallbackUtil(context)

        # [OSLO-1024]: Improvements.
        # Check to make sure the user selected an instrument.
        records: list[PyRecordModel] = (
            self.inst_man.add_existing_records(self.exp_handler.get_step_records(cast(ElnEntryStep,
                                                                                      context.active_step))))
        if not records:
            self.callback_util.ok_dialog("Error", "An instrument is required for pooling.")
            self.exp_handler.update_step(cast(ElnEntryStep, context.active_step),
                                         entry_status=ExperimentEntryStatus.UnlockedChangesRequired)
            return SapioWebhookResult(True)

        # [OSLO-1024]: Improvements.
        # Check how many instruments are selected. If there's more than one, prompt the user to choose one and clear the
        # row featuring the other instrument.
        instrument_types: set[str] = set()
        for record in records:
            instrument_type: str = record.get_field_value("InstrumentUsed")
            if instrument_type is not None and instrument_type != "":
                instrument_types.add(instrument_type)
        if not instrument_types:
            self.callback_util.ok_dialog("Error", "An instrument is required for pooling.")
            self.exp_handler.update_step(cast(ElnEntryStep, context.active_step),
                                         entry_status=ExperimentEntryStatus.UnlockedChangesRequired)
        elif len(instrument_types) > 1:
            try:
                selection: str = self.callback_util.list_dialog("Select an instrument to use.",
                                                                list(instrument_types))[0]
            except Exception:
                self.callback_util.ok_dialog("Error", "Only one instrument can be used for pooling.")
                self.exp_handler.update_step(cast(ElnEntryStep, context.active_step),
                                             entry_status=ExperimentEntryStatus.UnlockedChangesRequired)
                return SapioWebhookResult(True)

            for record in records:
                if record.get_field_value("InstrumentUsed") != selection:
                    record.set_field_value("InstrumentUsed", "")
                    record.set_field_value("Technician", "")
                    record.set_field_value("Date", None)
                    record.set_field_value("ModelNumber", "")
                    record.set_field_value("SerialNumber", "")
                    record.set_field_value("DatePurchased", None)
                    record.set_field_value("WorkstationId", None)
                    record.set_field_value("InstrumentStatus", "")
                    record.set_field_value("LastMaintenanceDate", None)
                    record.set_field_value("NextMaintenanceDate", None)

            self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
