from typing import Any

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel
from utilities.pooling_utils import PoolingUtils
from utilities.webhook_handler import OsloWebhookHandler


class CalculatePoolingVolumes(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)

        pooling_details: ElnEntryStep = self.exp_handler.get_step("Pooling Details")
        records: list[DataRecord] = (
            self.eln_man.get_data_records_for_entry(context.eln_experiment.notebook_experiment_id,
                                                    pooling_details.eln_entry.entry_id).result_list)

        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)
        invalid_ranges: list[dict[str, Any]] = []
        for r in records:
            sample: SampleModel = [s for s in samples if s.get_SampleId_field() == r.get_field_value("SampleId")][0]

            volume, invalid_range = PoolingUtils.calculate_volume_to_use(sample,
                                                                         float(r.get_field_value("PoolMolarity")),
                                                                         float(r.get_field_value("PoolVolume")),
                                                                         len(samples))
            if invalid_range is not None:
                invalid_ranges.append(invalid_range)

            r.set_field_value("SampleVolume", volume)

        self.rec_man.store_and_commit()

        if len(invalid_ranges) > 0:
            return PopupUtil.table_popup("Invalid Volumes", "The following samples have volumes outside the"
                                                            " range of 1.5 uL and 18 uL. Please adjust the"
                                                            " pool volume and pool molarity accordingly and"
                                                            " rerun the calculation.",
                                         [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID"),
                                          VeloxDoubleFieldDefinition("Volume", "Volume", "Volume")],
                                         invalid_ranges)

        return SapioWebhookResult(True)
