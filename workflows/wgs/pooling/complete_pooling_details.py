import json
from io import BytesIO
from typing import Any

import xlwt
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from xlwt import Workbook
from xlwt import Worksheet

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.pooling_utils import PoolingUtils
from utilities.webhook_handler import OsloWebhookHandler


class CompletePoolingDetails(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler
    eln_man: ElnManager
    acc_man: AccessionManager

    response_data: dict[str, Any]

    STATUS_FILE_DOWNLOAD: str = "file_download"
    STATUS_NEGATIVE_VOLUMES: str = "negative_volumes"
    STATUS_ERROR: str = "error"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.acc_man = DataMgmtServer.get_accession_manager(context.user)

        self.response_data = {"status": ""}

        # Get everything we'll need for each action
        exp_id: int = context.eln_experiment.notebook_experiment_id

        pooling_details: ElnEntryStep = self.exp_handler.get_step("Pooling Details")
        records: list[DataRecord] = (
            self.eln_man.get_data_records_for_entry(exp_id, pooling_details.eln_entry.entry_id).result_list)

        samples: list[SampleModel] = self.exp_handler.get_step_models("Samples", SampleModel)

        # Handle any client callback results
        result: ClientCallbackResult = context.client_callback_result
        if result is not None:
            return self.handle_client_callback_result(context, result, exp_id, records, samples)

        # Update the sample volumes
        negative_volumes: list[dict[str, Any]] = self.update_sample_volumes(samples, records)
        if len(negative_volumes) > 0:
            self.response_data["status"] = self.STATUS_NEGATIVE_VOLUMES
            return PopupUtil.table_popup("Warning", "The following samples do not have enough source volume to deduct "
                                                    "from and have been set to 0.",
                                         [VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID"),
                                          VeloxDoubleFieldDefinition("Volume", "Volume", "Volume")],
                                         negative_volumes,
                                         request_context=json.dumps(self.response_data))

        # Create the pool records and add them to the experiment
        self.create_pools(samples, records)

        # Generate and download the hamilton and pipette files
        return self.create_and_download_files(context, samples, records, exp_id)

    def handle_client_callback_result(self, context: SapioWebhookContext,
                                      result: ClientCallbackResult, exp_id: int, records: list[DataRecord],
                                      samples: list[SampleModel]) -> SapioWebhookResult:
        self.response_data = json.loads(result.callback_context_data)

        if (self.response_data["status"] == self.STATUS_FILE_DOWNLOAD
                or self.response_data["status"] == self.STATUS_ERROR):
            return SapioWebhookResult(True)

        if self.response_data["status"] == self.STATUS_NEGATIVE_VOLUMES:
            # Create the pool records and add them to the experiment
            self.create_pools(samples, records)

            # Generate and download the hamilton and pipette files
            return self.create_and_download_files(context, samples, records, exp_id)

    def update_sample_volumes(self, samples: list[SampleModel], records: list[DataRecord]) -> list[dict[str, Any]]:
        negative_volumes: list[dict[str, Any]] = []

        for s in samples:
            record: DataRecord = [r for r in records if s.get_SampleId_field() in r.get_field_value("SampleId")][0]
            rec_vol = 0.0 if record.get_field_value("SampleVolume") is None else float(record.get_field_value("SampleVolume"))
            sample_vol = 0.0 if s.get_Volume_field() is None else s.get_Volume_field()

            vol: float = sample_vol - rec_vol
            if vol <= 0:
                negative_volumes.append({
                    "SampleId": s.get_SampleId_field(),
                    "Volume": s.get_Volume_field()
                })
                s.set_Volume_field(0)

            else:
                s.set_Volume_field(vol)

        # Store and commit to update the volume changes
        self.rec_man.store_and_commit()

        return negative_volumes

    def create_pools(self, samples: list[SampleModel], records: list[DataRecord]) -> None:
        plate_pools: dict[str, list[SampleModel]] = {}
        tube_pools: dict[str, list[SampleModel]] = {}

        pools: list[SampleModel] = []

        pool_vol: float = float(records[0].get_field_value("PoolVolume"))
        pool_mol: float = float(records[0].get_field_value("PoolMolarity"))

        # Group the samples by pool
        for r in records:
            target_pool: str = r.get_field_value("TargetPool")

            if "Tube" in target_pool:
                if target_pool not in tube_pools:
                    tube_pools[target_pool] = []

                tube_pools[target_pool].append([s for s in samples if
                                                r.get_field_value("SampleId") == s.get_SampleId_field()][0])

            else:
                if target_pool not in plate_pools:
                    plate_pools[target_pool] = []

                plate_pools[target_pool].append([s for s in samples if
                                                r.get_field_value("SampleId") == s.get_SampleId_field()][0])

        # Get the number of pools total and generate accessioned sample IDs for them
        num_pools: int = len(plate_pools.keys()) + len(tube_pools.keys())
        pool_ids: list[str] = (
            self.acc_man.accession_for_system(num_pools, AccessionSystemCriteriaPojo(PoolingUtils.POOL_ACCESSION_VAL)))

        # Create the pools
        pool_id_index: int = 0
        for p in tube_pools:
            sample_names: list[str] = []
            pool: SampleModel = self.add_pool(pool_ids, pool_id_index, pool_vol, pool_mol)

            for s in tube_pools[p]:
                s.add_child(pool)
                sample_names.append(s.get_OtherSampleId_field())

            pool.set_OtherSampleId_field(",".join(sample_names))
            pools.append(pool)

            pool_id_index += 1

        if len(plate_pools) > 0:
            plate: PlateModel = self.rec_handler.add_model(PlateModel)

            plate_id: str = list(plate_pools.keys())[0].split("-")[0]
            plate.set_PlateId_field(plate_id)

            for p in plate_pools:
                sample_names: list[str] = []
                split_target_pool: list[str] = p.split("-")

                pool: SampleModel = self.add_pool(pool_ids, pool_id_index, pool_vol, pool_mol)

                pool.set_StorageLocationBarcode_field(plate_id)
                plate.add_child(pool)

                well_location: str = split_target_pool[len(split_target_pool) - 1]
                pool.set_RowPosition_field(well_location[0])
                pool.set_ColPosition_field(well_location[1:len(well_location)])

                for s in plate_pools[p]:
                    s.add_child(pool)
                    sample_names.append(s.get_OtherSampleId_field())

                pool.set_OtherSampleId_field(",".join(sample_names))
                pools.append(pool)

                pool_id_index += 1

        # Store and commit the pools and add them to the "Pools" entry
        self.rec_man.store_and_commit()
        self.exp_handler.add_step_records("Pools", pools)

    def add_pool(self, pool_ids: list[str], pool_id_index: int, pool_vol: float, pool_mol: float) -> SampleModel:
        pool: SampleModel = self.rec_handler.add_model(SampleModel)
        pool.set_SampleId_field(f"{PoolingUtils.POOL_ACCESSION_VAL}{pool_ids[pool_id_index]}")

        pool.set_Volume_field(pool_vol)
        pool.set_VolumeUnits_field("µL")

        pool.set_Concentration_field(pool_mol)
        pool.set_ConcentrationUnits_field("nmol/µL")

        pool.set_ExemplarSampleType_field("DNA")

        return pool

    def create_and_download_files(self, context: SapioWebhookContext, samples: list[SampleModel],
                                  records: list[DataRecord], exp_id: int) -> SapioWebhookResult:
        pipette_file_data: str = ",".join(PoolingUtils.HEADERS_PIPETTE_FILE) + "\n"

        # Create the workbook for the hamilton file, since it needs to be an .xls file
        workbook: Workbook = xlwt.Workbook()
        sheet: Worksheet = workbook.add_sheet("Sheet1")

        # Write the headers of the hamilton file
        self.write_hamilton_headers(sheet)

        hamilton_index: int = 1
        for r in records:
            sample_vol: float = float(r.get_field_value("SampleVolume"))

            if PoolingUtils.MIN_HAMILTON_POOL_VOL <= sample_vol <= PoolingUtils.MAX_HAMILTON_POOL_VOL:
                self.append_hamilton_data(r, [s for s in samples
                                              if s.get_SampleId_field() == r.get_field_value("SampleId")][0],
                                          sheet, hamilton_index)
                hamilton_index += 1

            else:
                pipette_file_data += self.append_pipette_data(r)

        # Download the files
        return self.download_files(context, workbook, pipette_file_data, exp_id)

    def write_hamilton_headers(self, sheet: Worksheet) -> None:
        hamilton_col_index: int = 0
        for x in PoolingUtils.HEADERS_HAMILTON_INPUT_FILE:
            sheet.write(0, hamilton_col_index, x)
            hamilton_col_index += 1

    def append_hamilton_data(self, record: DataRecord, sample: SampleModel, sheet: Worksheet, index: int) -> None:
        target_pool: str = record.get_field_value("TargetPool")
        if "Tube" in target_pool:
            dest_well: str = target_pool.replace("Tube", "")

        else:
            target_pool: list[str] = record.get_field_value('TargetPool').split('-')
            dest_well: str = f"{target_pool[len(target_pool) - 1]}"

        sheet.write(index, 0, sample.get_StorageLocationBarcode_field())
        sheet.write(index, 1, f"{sample.get_RowPosition_field()}{sample.get_ColPosition_field()}")
        sheet.write(index, 2, dest_well)
        sheet.write(index, 3, str(record.get_field_value("SampleVolume")))
        sheet.write(index, 4, str(record.get_field_value("PoolVolume")))

    def append_pipette_data(self, record: DataRecord) -> str:
        return (f"{record.get_field_value('SampleId')},,"
                f"{record.get_field_value('SampleVolume')},{record.get_field_value('TargetPool')},"
                f"{record.get_field_value('PoolVolume')},{record.get_field_value('PoolMolarity')}\n")

    def download_files(self, context: SapioWebhookContext, hamilton_workbook: Workbook, pipette_file_data: str,
                       exp_id: int) -> SapioWebhookResult:
        # Format the file names
        date: str = TimeUtil.now_in_format("%m_%d_%Y")

        hamilton_file_name: str = PoolingUtils.HAMILTON_INPUT_FILE_NAME.replace("[experiment ID]",
                                                                                str(exp_id)).replace("[date]", date)
        pipette_file_name: str = PoolingUtils.PIPETTE_FILE_NAME.replace("[experiment ID]",
                                                                        str(exp_id)).replace("[date]", date)

        # Convert the file data strings to bytes
        buffer: BytesIO = BytesIO()
        hamilton_workbook.save(buffer)
        hamilton_file_bytes: bytes = buffer.getvalue()
        pipette_file_bytes: bytes = bytes(pipette_file_data, "utf-8")

        # Download the files to FileBridge or the web browser
        unsent_files: list[str] = []
        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", hamilton_file_name, hamilton_file_bytes,
                                           PoolingUtils.HAMILTON_STAR_INSTRUMENT_NAME):
            unsent_files.append(hamilton_file_name)

        if not FileBridgeUtils.upload_file(context, "oslo-filebridge", pipette_file_name, pipette_file_bytes, ""):
            unsent_files.append(pipette_file_name)

        if len(unsent_files) == 2:
            self.response_data["status"] = self.STATUS_FILE_DOWNLOAD
            return FileUtil.write_files({hamilton_file_name: hamilton_file_bytes,
                                         pipette_file_name: pipette_file_bytes},
                                        request_context=json.dumps(self.response_data))

        if len(unsent_files) == 1:
            self.response_data["status"] = self.STATUS_FILE_DOWNLOAD
            if unsent_files[0] == hamilton_file_name:
                return FileUtil.write_file(hamilton_file_name, hamilton_file_bytes,
                                           request_context=json.dumps(self.response_data))

            else:
                return FileUtil.write_file(pipette_file_name, pipette_file_bytes,
                                           request_context=json.dumps(self.response_data))
        # Add hamilton file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(context, hamilton_file_name, hamilton_file_bytes)
        )

        return SapioWebhookResult(True)

    def generate_hamilton_file_bytes(self, file_name: str) -> bytes:
        with open(file_name, 'rb') as file:
            xls_bytes = file.read()

        return xls_bytes
