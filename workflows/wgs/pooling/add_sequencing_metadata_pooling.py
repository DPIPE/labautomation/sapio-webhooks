from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.data_models import SampleModel, SequencingMetadataModel
from utilities.webhook_handler import OsloWebhookHandler


class AddSequencingMetadataPooling(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        self.rec_handler = RecordHandler(context)

        instrument_tracking_entry: ElnEntryStep = self.exp_handler.get_step("Instrument Tracking")
        instrument_records: list[DataRecord] = self.eln_man.get_data_records_for_entry(
            context.eln_experiment.notebook_experiment_id,
            instrument_tracking_entry.eln_entry.entry_id).result_list
        instrument_record: DataRecord = [r for r in instrument_records if r.get_field_value("InstrumentUsed")
                                         is not None and r.get_field_value("InstrumentUsed") != ""][0]

        pools: list[SampleModel] = self.exp_handler.get_step_models("Pools", SampleModel)
        self.rel_man.load_parents_of_type(pools, SequencingMetadataModel)
        for p in pools:
            sm: SequencingMetadataModel = p.get_parent_of_type(SequencingMetadataModel)
            if not sm:
                sm: SequencingMetadataModel = self.rec_handler.add_model(SequencingMetadataModel)
                sm.add_child(p)
            sm.set_SequencerType_field(instrument_record.get_field_value("InstrumentType"))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
