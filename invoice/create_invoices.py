import datetime

from pandas import DataFrame
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.CustomReportService import CustomReportManager
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CompositeReportTerm, \
    CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import InvoiceModel, TestCodeConfigurationModel
from utilities.webhook_handler import OsloWebhookHandler

from datetime import date


class CreateInvoices(OsloWebhookHandler):
    rec_handler: RecordHandler
    custom_report_man: CustomReportManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.custom_report_man = CustomReportManager(context.user)

        # Get all analyses that have been completed in the last month
        analyses: list[TestCodeConfigurationModel] = self.get_analyses()
        if not analyses:
            return SapioWebhookResult(True, "No analyses were completed during the previous month.")

        # Create invoice record children for each of the analyses
        for a in analyses:
            a.add_child(self.rec_handler.add_model(InvoiceModel))

        # Store and commit changes
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def get_analyses(self) -> list[TestCodeConfigurationModel]:
        """
        Run a custom report to get all analyses that were completed within the last month
        :return: List of analyses
        """
        # Run a custom report to get all analyses completed in the previous month
        custom_report: CustomReportCriteria = self.build_custom_report()

        result_frame: DataFrame = self.custom_report_man.run_custom_report(custom_report).get_data_frame()
        if result_frame.size == 0:
            return None

        test_code_ids: list[str] = []
        for x in range(0, result_frame.index.size):
            test_code_ids.append(result_frame.iat[x, 1])

        # Query for all the qualifying analyses
        return self.rec_handler.query_models(TestCodeConfigurationModel, "Completed", [True])

    def build_custom_report(self) -> CustomReportCriteria:
        """
        Builds a custom report that gets all analyses that were completed within the last month
        :return: CustomReportCriteria
        """
        # Get the previous month
        today: date = date.today()
        first: date = today.replace(day=1)
        last_month: date = first - datetime.timedelta(days=1)

        today_str: str = first.strftime("%m/%d/%Y")
        last_month_str: str = last_month.strftime("%m/%d/%Y")

        # Set up a custom report to get all qualifying analyses
        column_list: list[ReportColumn] = [
            ReportColumn(TestCodeConfigurationModel.DATA_TYPE_NAME,
                         TestCodeConfigurationModel.TESTCODEID__FIELD_NAME.field_name, FieldType.STRING)]

        terms: list[RawReportTerm] = [
            RawReportTerm(TestCodeConfigurationModel.DATA_TYPE_NAME,
                          TestCodeConfigurationModel.DATE_COMPLETED_FIELD.field_name,
                          RawTermOperation.GREATER_THAN_OR_EQUAL_TO_OPERATOR,
                          str(TimeUtil.format_to_millis(last_month_str, "%m/%d/%Y"))),

            RawReportTerm(TestCodeConfigurationModel.DATA_TYPE_NAME,
                          TestCodeConfigurationModel.DATE_COMPLETED_FIELD.field_name,
                          RawTermOperation.LESS_THAN_OPERATOR,
                          str(TimeUtil.format_to_millis(today_str, "%m/%d/%Y")))
        ]

        root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeReportTerm.AND_OPERATOR, terms[1])

        return CustomReportCriteria(column_list, root_term)
