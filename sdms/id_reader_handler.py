from typing import Any

from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.data_models import SampleModel, IDReaderScanModel, PlateModel
from utilities.instrument_utils import InstrumentUtils

HEADERS: list[str] = ["RackID", "LocationCell", "TubeCode"]
OPTION_KEY: str = "ID READER PLATE ID"


class IDReaderHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    instrument_utils: InstrumentUtils
    file_path: str
    file_bytes: bytes
    eln_man: ElnManager
    data_record_man: DataRecordManager

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager,
                 file_path: str, file_bytes: bytes):
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_path = file_path
        self.file_bytes = file_bytes

        self.eln_man = DataMgmtServer.get_eln_manager(self.context.user)
        self.data_record_man = DataMgmtServer.get_data_record_manager(self.context.user)

        self.instrument_utils = InstrumentUtils(self.context)

    def execute(self) -> SapioWebhookResult:
        # Tokenize the data in the file
        try:
            file_data: list[dict[str, str]] = FileUtil.tokenize_csv(self.file_bytes, HEADERS)[0]
        except Exception as e:
            return self.instrument_utils.log_instrument_error("IDReader", self.file_path, repr(e))

        # [OSLO-952]: Get the plate from the file name. If it doesn't exist in the system, then create it.
        plate: PlateModel = self.get_plate()

        # Get the samples from the file.
        samples: list[SampleModel] | None = self.get_samples(file_data)
        if not samples:
            return self.instrument_utils.log_instrument_error("IDReader", self.file_path, "No samples in the file were"
                                                                                          " found in the system.")

        # [OSLO-952]: Create ID Reader Scan record for each sample and add the plate as a side-link. If there already
        # is one, just update it.
        id_reader_scans: list[IDReaderScanModel] | None = (
            self.rec_handler.query_models(IDReaderScanModel, IDReaderScanModel.PLATE__FIELD_NAME.field_name,
                                          [plate.record_id]))
        for i, d in enumerate(file_data):
            try:
                id_reader_scan: IDReaderScanModel = \
                    [x for x in id_reader_scans if x.get_TubeBarcode_field() == d["TubeCode"]][0]
            except Exception:
                id_reader_scan = self.rec_handler.add_model(IDReaderScanModel)
                id_reader_scan.set_Plate_field(plate.record_id)
            id_reader_scan.set_TubeBarcode_field(d["TubeCode"])
            id_reader_scan.set_RowPosition_field(d["LocationCell"][0:1])
            id_reader_scan.set_ColPosition_field(d["LocationCell"][1:len(d["LocationCell"])])

        # Store and commit
        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def get_plate(self) -> PlateModel:
        plate_id: str = self.file_path.split(" - ")[1].replace(".csv", "")
        plates: list[PlateModel] | None = self.rec_handler.query_models(PlateModel,
                                                                        PlateModel.PLATEID__FIELD_NAME.field_name,
                                                                        [plate_id])
        if not plates:
            plate: PlateModel = self.rec_handler.add_model(PlateModel)
            plate.set_PlateId_field(plate_id)
            plate.set_PlateRows_field(8)
            plate.set_PlateColumns_field(12)
            self.rec_handler.rec_man.store_and_commit()
        else:
            plate: PlateModel = plates[0]
        return plate

    def get_samples(self, file_data: list[dict[str, Any]]) -> list[SampleModel] | None:
        tube_barcodes: list[str] = []
        for i, d in enumerate(file_data):
            tube_barcodes.append(d["TubeCode"])
        return self.rec_handler.query_models(SampleModel, SampleModel.TUBEBARCODE__FIELD_NAME.field_name, tube_barcodes)