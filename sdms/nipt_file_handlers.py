from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CompositeReportTerm, \
    CompositeTermOperation, CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities import utils
from utilities.data_models import BatchModel, SampleModel, BatchFileErrorModel, AttachmentModel, ConsumableItemModel, \
    TrackedReagentModel, InstrumentModel, RequestModel, NIPTAssayDetailModel, NIPTQCSequencingResultModel, \
    NIPTQCLibraryQuantResultModel, NIPTQCPoolResultModel, NIPTFinalResultModel
from utilities.instrument_utils import InstrumentUtils
from utilities.nipt_utils import NIPTUtils

INIT_FILE_ERROR: str = "Tube barcode of \"[TUBE BARCODE]\" was not found in the system."
INIT_FILE_ORDER_ERROR: str = "\"[ORDER]\" has not yet been approved."
LIB_REAGENT_FILE_ERROR: str = ("Reagent of type \"[TYPE]\" with lot number of \"[LOT NUMBER]\" was not found in the "
                               "system.")
INSTRUMENT_FILE_ERROR: str = "Instrument with serial number \"[SERIAL NUMBER]\" was not found in the system."


class NIPTBatchInititationReportFileHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes

    def execute(self) -> SapioWebhookResult:
        # Create a new batch record and set its fields.
        file_name_parts: list[str] = self.file_name.split("_")
        batch: BatchModel = self.rec_handler.add_model(BatchModel)
        batch.set_BatchId_field(file_name_parts[0])
        batch.set_StartDate_field(int(file_name_parts[4]))
        batch.set_WorkflowName_field("NIPT")

        # Check the samples and ensure that all are present. If they aren't all in the system, then don't generate the
        # sample sheet.
        samples_present, samples = NIPTBatchInititationReportFileHandler.check_and_add_samples(self.rec_handler,
                                                                                               self.rel_man,
                                                                                               self.file_name,
                                                                                               self.file_bytes, batch)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        # Check to make sure all the orders for the samples are approved. If they are, then generate the sample sheet.
        # Otherwise, log errors to the batch.
        orders_to_samples: dict[RequestModel, list[SampleModel]] = utils.map_orders_to_samples(self.rel_man, samples)
        not_all_orders_approved: bool = (
            NIPTBatchInititationReportFileHandler.check_order_approvals(self, batch, orders_to_samples, self.file_name))

        # Don't generate the file if there's missing samples or unapproved orders.
        if not samples_present or not_all_orders_approved:
            self.rec_handler.rec_man.store_and_commit()

            return SapioWebhookResult(True)

        # Generate the sample sheet and add it as an attachment to the batch.
        NIPTBatchInititationReportFileHandler.generate_sample_sheet(self, self.context, batch, orders_to_samples)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def check_and_add_samples(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, file_name: str,
                              file_bytes: bytes, batch: BatchModel) -> (bool, list[SampleModel]):
        all_resolved: bool = True

        # Get all the samples in the system from the barcodes in the file.
        file_data: str = file_bytes.decode("utf-8")
        rows: list[str] = file_data.split("\n")
        file_barcodes: list[str] = []
        for row in rows[1:]:
            file_barcodes.append(row.split("\t")[1])
        samples: list[SampleModel] = []
        samples.extend(rec_handler.query_models(SampleModel, SampleModel.TUBEBARCODE__FIELD_NAME.field_name,
                                                file_barcodes))
        existing_barcodes: list[str] = [x.get_TubeBarcode_field() for x in samples] if samples else []

        # Update any error records that contain the now existing sample barcodes.
        rel_man.load_children_of_type([batch], BatchFileErrorModel)
        all_error_records: list[BatchFileErrorModel] = batch.get_children_of_type(BatchFileErrorModel)
        try:
            init_file_error_records: list[BatchFileErrorModel] = \
                [x for x in all_error_records if x.get_FileName_field() == file_name]
        except Exception:
            init_file_error_records: list[BatchFileErrorModel] = []
        for file_barcode in file_barcodes:
            error: str = INIT_FILE_ERROR.replace("[TUBE BARCODE]", file_barcode)
            if file_barcode in existing_barcodes:
                try:
                    error_record: BatchFileErrorModel = \
                        [x for x in init_file_error_records if x.get_Description_field() == error][0]
                    error_record.set_Status_field("Resolved")
                except Exception:
                    continue
            else:
                all_resolved = False
                try:
                    error_record: BatchFileErrorModel = \
                        [x for x in init_file_error_records if x.get_Description_field() == error][0]
                except Exception:
                    error_record: BatchFileErrorModel = rec_handler.add_model(BatchFileErrorModel)
                    error_record.set_FileName_field(file_name)
                    error_record.set_Description_field(error)
                    batch.add_child(error_record)
                error_record.set_Status_field("Unresolved")

        # Add samples to the batch if they aren't already.
        rel_man.load_children_of_type([batch], SampleModel)
        child_samples: list[SampleModel] = batch.get_children_of_type(SampleModel)
        for sample in samples:
            if sample not in child_samples:
                batch.add_child(sample)
                child_samples.append(sample)

        # Update the number of samples field on the batch so that it displays in the table views.
        batch.set_NumberOfSamples_field(len(child_samples))

        # Set the batch status appropriately.
        if all_resolved:
            NIPTUtils.remove_batch_status(batch, "Invalid sample barcode(s)")
        else:
            NIPTUtils.add_batch_status(batch, "Invalid sample barcode(s)")

        return all_resolved, child_samples

    @staticmethod
    def check_order_approvals(self, batch: BatchModel, orders_to_samples: dict[RequestModel, list[SampleModel]],
                              file_name: str) -> bool:
        cannot_generate: bool = False
        self.rel_man.load_children_of_type([batch], BatchFileErrorModel)

        # Check to make sure all the orders are approved.
        batch_file_errors: list[BatchFileErrorModel] = batch.get_children_of_type(BatchFileErrorModel)
        for order in orders_to_samples:
            error_msg: str = INIT_FILE_ORDER_ERROR.replace("[ORDER]", order.get_RequestId_field())
            if not order.get_RequestApproved_field():
                cannot_generate = True
                try:
                    batch_error: BatchFileErrorModel = \
                        [x for x in batch_file_errors if error_msg == x.get_Description_field()
                         and x.get_FileName_field() == file_name][0]
                except Exception:
                    batch_error: BatchFileErrorModel = self.rec_handler.add_model(BatchFileErrorModel)
                    batch_error.set_Description_field(error_msg)
                    batch_error.set_FileName_field(file_name)
                    batch.add_child(batch_error)
                batch_error.set_Status_field("Unresolved")
            else:
                try:
                    batch_error: BatchFileErrorModel = \
                        [x for x in batch_file_errors if error_msg == x.get_Description_field()
                         and x.get_FileName_field() == file_name][0]
                    batch_error.set_Status_field("Resolved")
                except Exception:
                    ...

        # Set the batch status appropriately.
        if cannot_generate:
            NIPTUtils.add_batch_status(batch, "Unapproved order(s)")
        else:
            NIPTUtils.remove_batch_status(batch, "Unapproved order(s)")

        return cannot_generate

    @staticmethod
    def generate_sample_sheet(self, context: SapioWebhookContext, batch: BatchModel,
                              orders_to_samples: dict[RequestModel, list[SampleModel]]) -> None:
        NIPTUtils.remove_batch_status(batch, "Invalid sample barcode(s)")
        NIPTUtils.remove_batch_status(batch, "Unapproved order(s)")

        # Write the file data.
        self.rel_man.load_children_of_type([x for x in orders_to_samples], NIPTAssayDetailModel)
        file_data: str = "batch_name\tsample_barcode\tsample_type\tsex_chromosomes\tscreen_type"
        for order in orders_to_samples:
            assay_detail: NIPTAssayDetailModel = order.get_child_of_type(NIPTAssayDetailModel)
            for sample in orders_to_samples[order]:
                file_data += (f"\n{batch.get_BatchId_field()}\t{sample.get_OtherSampleId_field()}\t"
                              f"{sample.get_ExemplarSampleType_field()}\t"
                              f"{'yes' if assay_detail.get_SexChromosomes_field() else 'no'}\t"
                              f"{assay_detail.get_ScreenType_field()}")

        # Add the sample sheet as an attachment to the batch.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, f"NIPT-sample-sheet-{batch.get_BatchId_field()}.txt",
                                             file_data.encode(), AttachmentModel))
        batch.add_child(attachment)

        # TODO: For some reason we can no longer catch any exceptions that are thrown during FileBridge sending over a file. For the sake of having short time upon writing this, this is commented out until we figure out a fix.
        # # Send the file via FileBridge to the VeriSeq server.
        # try:
        #     FileBridge.write_file(context, "josh-oslo", attachment.get_FilePath_field(), file_data)
        # except Exception as e:
        #     InstrumentUtils(context).log_instrument_error("VeriSeq", attachment.get_FilePath_field(),
        #                                                   f"Failed to send the sample sheet to the VeriSeq server.\n"
        #                                                   f"{str(e)}")

        batch.set_ExemplarBatchStatus_field("Sample sheet generated")


class NIPTLibraryReagentReportHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes
    instrument_utils: InstrumentUtils

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Run the logic to add reagents to the batch.
        NIPTLibraryReagentReportHandler.check_and_add_reagents(self.context, self.rec_handler, self.rel_man,
                                                               batch, self.instrument_utils, self.file_name,
                                                               self.file_bytes)

        # Add the file as an attachment to the batch.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def check_and_add_reagents(context: SapioWebhookContext, rec_handler: RecordHandler,
                               rel_man: RecordModelRelationshipManager, batch: BatchModel,
                               instrument_utils: InstrumentUtils, file_name: str, file_bytes: bytes) -> None:
        # Split the file and get each reagent listed in it.
        reagent_datas: list[list[str]] = []
        rows: list[str] = file_bytes.decode().split("\n")
        for row in rows[1:]:
            items: list[str] = row.split("\t")
            reagent_datas.append([items[2], items[3]])

        # Run a custom report for the reagents in the system.
        columns: list[ReportColumn] = [ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)]

        raw_terms: list[list[RawReportTerm]] = []
        for data in reagent_datas:
            raw_terms.append([RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.CONSUMABLETYPE__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR, data[0]),
                              RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR, data[1])])
        composite_terms: list[CompositeReportTerm] = []
        for raw_term in raw_terms:
            composite_terms.append(CompositeReportTerm(raw_term[0], CompositeTermOperation.AND_OPERATOR, raw_term[1]))
        root_term: CompositeReportTerm = composite_terms[0]
        if len(composite_terms) > 1:
            for composite_term in composite_terms[1:]:
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.OR_OPERATOR, composite_term)
        results: list[list[str]] = (
            DataMgmtServer.get_custom_report_manager(context.user)
            .run_custom_report(CustomReportCriteria(columns, root_term)).result_table)
        try:
            reagents: list[ConsumableItemModel] = (
                rec_handler.query_models_by_id(ConsumableItemModel, [int(x[0]) for x in results]))
        except Exception:
            reagents: list[ConsumableItemModel] = []

        # Add the reagents as children of the batch if they aren't already, while updating or adding any batch file
        # errors under the batch.
        invalid_reagents: bool = False
        rel_man.load_children_of_type([batch], TrackedReagentModel)
        rel_man.load_children_of_type([batch], BatchFileErrorModel)
        all_error_records: list[BatchFileErrorModel] = batch.get_children_of_type(BatchFileErrorModel)
        try:
            lib_reagent_errors: list[BatchFileErrorModel] = \
                [x for x in all_error_records if x.get_FileName_field() == file_name]
        except Exception:
            lib_reagent_errors: list[BatchFileErrorModel] = []
        for reagent_data in reagent_datas:
            error: str = LIB_REAGENT_FILE_ERROR.replace("[TYPE]", reagent_data[0])
            error = error.replace("[LOT NUMBER]", reagent_data[1])
            try:
                reagent: ConsumableItemModel = \
                    [x for x in reagents if x.get_ConsumableType_field() == reagent_data[0]
                     and x.get_LotNumber_field() == reagent_data[1]][0]
                try:
                    [x for x in batch.get_children_of_type(TrackedReagentModel)
                     if x.get_ReagentType_field() == reagent_data[0]
                     and x.get_LotNumber_field() == reagent_data[1]][0]
                except Exception:
                    tracked_reagent: TrackedReagentModel = rec_handler.add_model(TrackedReagentModel)
                    tracked_reagent.set_ReagentType_field(reagent.get_ConsumableType_field())
                    tracked_reagent.set_LotNumber_field(reagent.get_LotNumber_field())
                    batch.add_child(tracked_reagent)
                    try:
                        batch_file_error: BatchFileErrorModel = \
                            [x for x in lib_reagent_errors if error == x.get_Description_field()][0]
                        batch_file_error.set_Status_field("Resolved")
                    except Exception:
                        ...
            except Exception:
                invalid_reagents = True
                try:
                    [x for x in batch.get_children_of_type(BatchFileErrorModel)
                     if error == x.get_Description_field()][0]
                except Exception:
                    batch_file_error: BatchFileErrorModel = rec_handler.add_model(BatchFileErrorModel)
                    batch_file_error.set_Description_field(error)
                    batch_file_error.set_FileName_field(file_name)
                    batch_file_error.set_Status_field("Unresolved")
                    batch.add_child(batch_file_error)
        if not invalid_reagents:
            batch.set_ExemplarBatchStatus_field("Library Reagent Report File received")
        else:
            NIPTUtils.add_batch_status(batch, "Invalid reagent(s)")


class NIPTInstrumentFileHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes
    instrument_utils: InstrumentUtils
    contains_hamilton: bool

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes,
                 contains_hamilton: bool) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)
        self.contains_hamilton = contains_hamilton

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Run the instrument check.
        NIPTInstrumentFileHandler.check_instruments(self.rec_handler, self.rel_man, batch, self.file_name,
                                                    self.file_bytes, self.contains_hamilton)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def check_instruments(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, batch: BatchModel,
                          file_name: str, file_bytes: bytes, contains_hamilton: bool) -> None:
        # Get the instruments and operators from the file.
        rows: list[str] = file_bytes.decode("utf-8").split("\n")
        instrument_serial_numbers: list[str] = []
        operators: set[str] = set()
        for row in rows[1:]:
            items: list[str] = row.split("\t")
            if contains_hamilton:
                instrument_serial_numbers.append(items[3])
                operators.add(items[2])
            else:
                instrument_serial_numbers.append(items[2])
        instruments: list[InstrumentModel] | None = (
            rec_handler.query_models(InstrumentModel, InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name,
                                     instrument_serial_numbers))
        if not instruments:
            instruments = []

        # Add the instruments to the batch if they aren't already, while updating and adding batch error logs when
        # needed.
        invalid_instruments: bool = False
        rel_man.load_children_of_type([batch], InstrumentModel)
        rel_man.load_children_of_type([batch], BatchFileErrorModel)
        batch_instruments: list[InstrumentModel] = batch.get_children_of_type(InstrumentModel)
        all_batch_errors: list[BatchFileErrorModel] = batch.get_children_of_type(BatchFileErrorModel)
        try:
            process_log_errors: list[BatchFileErrorModel] = \
                [x for x in all_batch_errors if x.get_FileName_field() == file_name]
        except Exception:
            process_log_errors: list[BatchFileErrorModel] = []
        for serial_number in instrument_serial_numbers:
            error: str = INSTRUMENT_FILE_ERROR.replace("[SERIAL NUMBER]", serial_number)
            try:
                instrument: InstrumentModel = [x for x in instruments if x.get_SerialNumber_field() == serial_number][0]
                if instrument not in batch_instruments:
                    batch.add_child(instrument)
            except Exception:
                invalid_instruments = True
                try:
                    batch_error: BatchFileErrorModel = \
                        [x for x in process_log_errors if error == x.get_Description_field()][0]
                except Exception:
                    batch_error: BatchFileErrorModel = rec_handler.add_model(BatchFileErrorModel)
                    batch_error.set_FileName_field(file_name)
                    batch_error.set_Description_field(error)
                    batch.add_child(batch_error)
                batch_error.set_Status_field("Unresolved")
            try:
                batch_error: BatchFileErrorModel = \
                    [x for x in process_log_errors if error == x.get_Description_field()][0]
                batch_error.set_Status_field("Resolved")
            except Exception:
                ...
        if not invalid_instruments:
            batch.set_ExemplarBatchStatus_field("Process Log File received" if contains_hamilton else "Sequencing QC File received")
        else:
            NIPTUtils.add_batch_status(batch, "Invalid instrument(s)")

        # [OSLO-315]: Update the operators field on the batch with the operators in the file.
        if operators:
            batch.set_Operator_field(",".join(operators))


class NIPTLibraryQuantReportHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Read the file and create the library QC results.
        NIPTLibraryQuantReportHandler.create_library_qc_results(self.rec_handler, self.rel_man, self.file_bytes,
                                                                batch)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def create_library_qc_results(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                                  file_bytes: bytes, batch: BatchModel) -> None:
        def get_date(date_str: str) -> int:
            date_parts: list[str] = date_str.split("+")
            return TimeUtil.format_to_millis(date_parts[0], "%Y-%m-%dT%H:%M:%S")

        # Add or update any QC result records under the batch in accordance to the contents of the file.
        rel_man.load_children_of_type([batch], NIPTQCLibraryQuantResultModel)
        qc_results: list[NIPTQCLibraryQuantResultModel] = batch.get_children_of_type(NIPTQCLibraryQuantResultModel)
        rows: list[str] = file_bytes.decode("utf-8").split("\n")
        for row in rows[1:]:
            items: list[str] = row.split("\t")
            try:
                qc_result: NIPTQCLibraryQuantResultModel = \
                    [x for x in qc_results if x.get_QuantId_field() == items[1]][0]
            except Exception:
                qc_result: NIPTQCLibraryQuantResultModel = rec_handler.add_model(NIPTQCLibraryQuantResultModel)
                qc_result.set_BatchName_field(items[0])
                qc_result.set_QuantId_field(items[1])
                batch.add_child(qc_result)
            qc_result.set_Instrument_field(items[2])
            qc_result.set_StandardRSquared_field(float(items[3]))
            qc_result.set_StandardIntercept_field(float(items[4]))
            qc_result.set_StandardSlope_field(float(items[5]))
            qc_result.set_MedianCCNPguL_field(float(items[6]))
            qc_result.set_QCStatus_field(items[7])
            qc_result.set_QCReason_field(items[8])
            qc_result.set_Initiated_field(get_date(items[9]))

        batch.set_ExemplarBatchStatus_field("Library Quant Report File received")


class NIPTQCSequencingReportHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Read the file and create the library QC results.
        NIPTQCSequencingReportHandler.create_sequencing_qc_results(self.rec_handler, self.rel_man, self.file_bytes,
                                                                   batch)

        # Add any instruments to the instrument tracking of the batch.
        NIPTInstrumentFileHandler.check_instruments(self.rec_handler, self.rel_man, batch, self.file_name,
                                                    self.file_bytes, False)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def create_sequencing_qc_results(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager,
                                     file_bytes: bytes, batch: BatchModel) -> None:
        def get_date(date_str: str) -> int:
            date_parts: list[str] = date_str.split("+")
            return TimeUtil.format_to_millis(date_parts[0], "%Y-%m-%dT%H:%M:%S")

        # Add or update any QC result records under the batch in accordance to the contents of the file.
        rel_man.load_children_of_type([batch], NIPTQCSequencingResultModel)
        qc_results: list[NIPTQCSequencingResultModel] = batch.get_children_of_type(NIPTQCSequencingResultModel)
        rows: list[str] = file_bytes.decode("utf-8").split("\n")
        for row in rows[1:]:
            items: list[str] = row.split("\t")
            try:
                qc_result: NIPTQCSequencingResultModel = \
                    [x for x in qc_results if x.get_PoolBarcode_field() == items[1]][0]
            except Exception:
                qc_result: NIPTQCSequencingResultModel = rec_handler.add_model(NIPTQCSequencingResultModel)
                qc_result.set_BatchName_field(items[0])
                qc_result.set_PoolBarcode_field(items[1])
                batch.add_child(qc_result)
            qc_result.set_Instrument_field(items[2])
            qc_result.set_FlowCell_field(items[3])
            qc_result.set_SoftwareVersion_field(items[4])
            qc_result.set_RunFolder_field(items[5])
            qc_result.set_SequencingStatus_field(items[6])
            qc_result.set_QCStatus_field(items[7])
            qc_result.set_QCReason_field(items[8])
            qc_result.set_ClusterDensity_field(float(items[9]))
            qc_result.set_PCTq30_field(float(items[10]))
            qc_result.set_PCTPF_field(float(items[11]))
            qc_result.set_Phasing_field(float(items[12]))
            qc_result.set_Prephasing_field(float(items[13]))
            qc_result.set_PredictedAlignedReads_field(float(items[14]))
            qc_result.set_Started_field(get_date(items[15]))
            qc_result.set_Completed_field(get_date(items[16]))

        NIPTUtils.add_batch_status(batch, "Sequencing QC File received")


class NIPTPoolReportHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Read the file and create the pool result records.
        NIPTPoolReportHandler.create_pool_results(self.rec_handler, self.rel_man, self.file_bytes, batch)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def create_pool_results(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, file_bytes: bytes,
                            batch: BatchModel) -> None:
        # Add or update any QC result records under the batch in accordance to the contents of the file.
        rel_man.load_children_of_type([batch], NIPTQCPoolResultModel)
        qc_results: list[NIPTQCPoolResultModel] = batch.get_children_of_type(NIPTQCPoolResultModel)
        rows: list[str] = file_bytes.decode("utf-8").split("\n")
        for row in rows[1:]:
            items: list[str] = row.split("\t")
            try:
                qc_result: NIPTQCPoolResultModel = \
                    [x for x in qc_results if x.get_SampleBarcode_field() == items[1]][0]
            except Exception:
                qc_result: NIPTQCPoolResultModel = rec_handler.add_model(NIPTQCPoolResultModel)
                qc_result.set_BatchName_field(items[0])
                qc_result.set_SampleBarcode_field(items[1])
                batch.add_child(qc_result)
            qc_result.set_PoolBarcode_field(items[2])
            qc_result.set_PoolType_field(items[3])
            qc_result.set_PoolingVolume_field(float(items[4]))
            qc_result.set_PoolingComments_field(items[5])

        NIPTUtils.add_batch_status(batch, "Pool Report File received")


class NIPTBatchAttachmentHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)


class NIPTReportHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    file_name: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager, file_name: str, file_bytes: bytes) -> None:
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_name = file_name
        self.file_bytes = file_bytes
        self.instrument_utils = InstrumentUtils(context)

    def execute(self) -> SapioWebhookResult:
        # Get the batch referenced in the file name.
        batch_id: str = self.file_name[0:self.file_name.find("_")]
        try:
            batch: BatchModel = self.rec_handler.query_models(BatchModel, BatchModel.BATCHID__FIELD_NAME.field_name,
                                                              [batch_id])[0]
        except Exception:
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"No batch with a batch ID of \"{batch_id}\" was found in"
                                                              f" the system.")

        # Don't proceed with the file upload if the batch has a status of "Cancelled."
        if batch.get_ExemplarBatchStatus_field() == "Cancelled":
            return self.instrument_utils.log_instrument_error("N/A", self.file_name,
                                                              f"The batch \"{batch_id}\" is cancelled and cannot have"
                                                              f" NIPT files uploaded for it.")

        # Read the file and create NIPT result records.
        NIPTReportHandler.create_nipt_results(self.rec_handler, self.rel_man, self.file_bytes, batch)

        # Add the file as an attachment to the batch record.
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, self.file_name, self.file_bytes, AttachmentModel))
        batch.add_child(attachment)

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    @staticmethod
    def create_nipt_results(rec_handler: RecordHandler, rel_man: RecordModelRelationshipManager, file_bytes: bytes,
                            batch: BatchModel) -> None:
        rel_man.load_children_of_type([batch], NIPTFinalResultModel)
        final_results: list[NIPTFinalResultModel] = batch.get_children_of_type(NIPTFinalResultModel)
        rows: list[str] = file_bytes.decode("utf-8").split("\n")
        for row in rows[1:]:
            if row[0] == "#":
                break
            items: list[str] = row.split("\t")
            try:
                final_result: NIPTFinalResultModel = [x for x in final_results if x.get_SampleBarcode_field() == row][0]
            except Exception:
                final_result: NIPTFinalResultModel = rec_handler.add_model(NIPTFinalResultModel)
                final_result.set_BatchName_field(items[0])
                final_result.set_SampleBarcode_field(items[1])
                batch.add_child(final_result)
            final_result.set_SampleType_field(items[2])
            final_result.set_SexChromosome_field(True if items[3] == "yes" else False)
            final_result.set_ScreenType_field(items[4])
            final_result.set_FlowCell_field(items[5])
            final_result.set_ClassSX_field(items[6])
            final_result.set_ClassAuto_field(items[7])
            final_result.set_AnomalyDescription_field(items[8])
            final_result.set_QCFlag_field(items[9])
            final_result.set_QCReason_field(items[10])
            final_result.set_FF_field(float(items[11][0:items[11].find("%")]))

        # Check if every sample in the batch has as result. If so, then set the batch status to "Completed"
        not_all_results: bool = False
        rel_man.load_children_of_type([batch], SampleModel)
        all_final_results: list[NIPTFinalResultModel] = batch.get_children_of_type(NIPTFinalResultModel)
        for sample in batch.get_children_of_type(SampleModel):
            try:
                [x for x in all_final_results if x.get_SampleBarcode_field() == sample.get_TubeBarcode_field()][0]
            except Exception:
                not_all_results = True
                break
        if not not_all_results:
            batch.set_ExemplarBatchStatus_field("Completed")
