from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelInstanceManager

from utilities.data_models import ExemplarSDMSFileModel
from utilities.instrument_utils import InstrumentUtils
from utilities.workflow_utils import WorkflowUtils


class TaqManHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    eln_man: ElnManager
    instrument_utils: InstrumentUtils

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler, inst_man: RecordModelInstanceManager,
                 sdms_file: ExemplarSDMSFileModel):
        self.context = context
        self.rec_handler = rec_handler
        self.inst_man = inst_man
        self.sdms_file = sdms_file
        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.instrument_utils = InstrumentUtils(self.context)

    def execute(self) -> SapioWebhookResult:
        # Get the experiment with the same experiment ID specified in the file name. If one doesn't exist, then log an
        # instrument error to the system.
        file_path: str = self.sdms_file.get_FilePath_field()
        try:
            experiment_id: str = file_path[file_path.rfind("_") + 1:file_path.rfind(".")]
        except Exception:
            return self.instrument_utils.log_instrument_error("TaqMan Genotyper", file_path,
                                                              f"The file name '{file_path}' does not contain a "
                                                              f"valid experiment ID.")
        try:
            experiment: ElnExperiment = [e for e in self.eln_man.get_eln_experiment_list()
                                         if int(experiment_id) == e.notebook_experiment_id][0]
        except Exception:
            return self.instrument_utils.log_instrument_error("TaqMan Genotyper", file_path,
                                                              f"No experiment with an experiment ID of "
                                                              f"'{experiment_id}' exists in the system.")

        # Create an attachment record from the file and add it to the "Attachments" entry of the experiment if it's not
        # there already. If it's already there, then just update it with the latest file data.
        try:
            WorkflowUtils.add_or_update_sdms_attachment_on_entry(self.eln_man, self.inst_man,
                                                                 experiment.notebook_experiment_id, "TaqMan Upload",
                                                                 self.sdms_file)
        except Exception as e:
            return self.instrument_utils.log_instrument_error("TaqMan Genotyper", file_path, repr(e))

        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)
