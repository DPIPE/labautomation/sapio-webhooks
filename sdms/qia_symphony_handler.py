from typing import Any
from xml.etree.ElementTree import Element
from sapiopylib.rest.AccessionService import AccessionManager
from xml.etree import ElementTree

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CompositeReportTerm, \
    CompositeTermOperation, CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment, ElnExperimentUpdateCriteria
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.eln.ExperimentEntryCriteria import AbstractElnEntryUpdateCriteria, ElnTableEntryUpdateCriteria
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.constants import Constants
from utilities.data_models import SampleModel, PlateModel, ConsumableItemModel
from utilities.filebridge_utils import FileBridgeUtils
from utilities.instrument_utils import InstrumentUtils
from utilities.sample_utils import SampleUtils


class QIASymphonyHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    eln_man: ElnManager
    acc_manager: AccessionManager
    instrument_utils: InstrumentUtils
    file_path: str
    file_bytes: bytes
    instrument: str
    batch_map: dict[str, str]

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager,
                 file_path: str, file_bytes: bytes):
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_path = file_path
        self.file_bytes = file_bytes
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.eln_man = DataMgmtServer.get_eln_manager(self.context.user)
        self.instrument_utils = InstrumentUtils(self.context)
        self.instrument = None
        self.batch_map = {}

    def execute(self) -> SapioWebhookResult:
        data = self.parse_qiasymphony_file()
        if not data:
            return SapioWebhookResult(False, "Errors encountered. Check the Instrument Error logs for details.")

        self.create_extraction_workflow(data[0], data[1], data[2], data[3])
        self.send_pipette_files(data[1])

        return SapioWebhookResult(True)

    def parse_qiasymphony_file(self) -> (list[SampleModel], list[SampleModel], str, str):
        parent_samples: list[SampleModel] = []
        dna_aliquots: list[SampleModel] = []

        # Get the root element
        root: Element = ElementTree.fromstring(self.file_bytes.decode('utf-8'))

        # Create a plate record if the plate doesn't already exist in the system.
        try:
            plate_id: str = root.findall("PlateID")[0].text
        except Exception:
            self.instrument_utils.log_instrument_error("QIAsymphony", self.file_path, "Plate ID not found in file or "
                                                                                      "invalid.")
            return None
        plates: list[PlateModel] = self.rec_handler.query_models(PlateModel, PlateModel.PLATEID__FIELD_NAME.field_name,
                                                                 [plate_id])
        if not plates:
            plate: PlateModel = self.rec_handler.add_model(PlateModel)
            plate.set_PlateId_field(plate_id)
            plate.set_PlateRows_field(root.findall("NofRows")[0].text)
            plate.set_PlateColumns_field(root.findall("NofCols")[0].text)
        else:
            plate: PlateModel = plates[0]

        # [OSLO-590]: Get the instrument name and add the corresponding instrument to the Instrument Tracking entry.
        try:
            instrument: str = root.findall("Instrument")[0].text
            self.instrument = "QIAsymphony A" if instrument == "qssp11431" else "QIAsymphony B"
        except Exception:
            self.instrument_utils.log_instrument_error("QIAsymphony", self.file_path, "Instrument not found in file or "
                                                                                      "invalid.")
            return None

        # [OSLO-590]: Get all the Reagents from the file.
        reagent_elements: list[Element] = root.find("ReagentRackTrack").findall("ReagentTrack")
        reagents: list[ConsumableItemModel] = []
        if reagent_elements:
            lots_and_parts: list[(str, str)] = [(element.find("Lot").text, element.find("Id").text) for element in
                                                reagent_elements]
            reagents.extend(self.get_matching_reagents(lots_and_parts))

        # [OSLO-590]: Get all the samples from the file and load their children.
        batch_elements: list[Element] = root.findall("BatchTrack")
        if not batch_elements:
            self.instrument_utils.log_instrument_error(self.instrument, self.file_path,
                                                       "No batches present in the file.")
            return None
        all_sample_elements: list[Element] = []
        for element in batch_elements:
            all_sample_elements.extend(element.findall("SampleTrack"))
        if not all_sample_elements:
            self.instrument_utils.log_instrument_error(self.instrument, self.file_path, "No samples found in the file.")
            return None
        sample_ids: list[str] = [e.find("SampleCode").text.replace(" ", "") for e in all_sample_elements]
        matching_samples: list[SampleModel] = self.rec_handler.query_models(SampleModel,
                                                                            SampleModel.SAMPLEID__FIELD_NAME.field_name,
                                                                            sample_ids)
        if len(matching_samples) != len(all_sample_elements):
            self.instrument_utils.log_instrument_error(self.instrument, self.file_path,
                                                       "Not all samples were found in the file.")
            return None
        self.rel_man.load_children_of_type(matching_samples, SampleModel)

        # Create aliquots for each sample and add them to the plate.
        for element in all_sample_elements:
            try:
                parent_sample: SampleModel = \
                    [x for x in matching_samples if x.get_SampleId_field() == element.find('SampleCode').text][0]
            except Exception:
                self.instrument_utils.log_instrument_error(self.instrument, self.file_path,
                                                           f"Could not find sample {element.find('SampleCode').text}.")
                return None

            dna_aliquot: SampleModel = SampleUtils.create_aliquot(self.rec_handler, self.rel_man, self.acc_manager,
                                                                  parent_sample, "DNA",
                                                                  tube_barcode=element.find('EluateTubeBarcode').text)

            # [OSLO-590]: Set the position of the aliquot on the plate based on the "SamplePosition" value in the file.
            position: str = Constants.PLATE_LOCATIONS_MAP[int(element.find("SamplePosition").text)]
            dna_aliquot.set_RowPosition_field(position[0:1])
            dna_aliquot.set_ColPosition_field(position[1:len(position)])

            # Add the new aliquot as a child of the plate.
            plate.add_child(dna_aliquot)

            # Append the samples to the lists to return.
            parent_samples.append(parent_sample)
            dna_aliquots.append(dna_aliquot)

        # [OSLO-590]: Populate the batch map so that we can keep track of each batch for the pipette file generation.
        for aliquot in dna_aliquots:
            parent_sample: SampleModel = [sample for sample in parent_samples
                                          if sample.get_SampleId_field() == aliquot.get_TopLevelSampleId_field()][0]
            for batch_element in batch_elements:
                sample_elements: list[Element] = batch_element.findall("SampleTrack")
                for sample_element in sample_elements:
                    if parent_sample.get_SampleId_field() == sample_element.find("SampleCode").text:
                        self.batch_map[aliquot.get_SampleId_field()] = batch_element.find("BatchID").text

        # Store and commit
        self.rec_handler.rec_man.store_and_commit()

        return parent_samples, dna_aliquots, plate_id, reagents

    def create_extraction_workflow(self, parent_samples: list[SampleModel], dna_aliquots: list[SampleModel],
                                   batch_id: str, reagents: list[ConsumableItemModel]) -> SapioWebhookResult:

        # [OSLO-590]: Changed template name to "QIA DNA Extraction."
        experiment: ElnExperiment = ExperimentHandler.create_experiment(self.context, "QIA DNA Extraction")

        # [OSLO-590]: Create an update criteria to auto-complete entries.
        criteria: ElnTableEntryUpdateCriteria = ElnTableEntryUpdateCriteria()
        criteria.entry_status = ExperimentEntryStatus.Completed

        # Add the samples to their appropriate tables in the experiment.
        entries: list[ExperimentEntry] = self.eln_man.get_experiment_entry_list(experiment.notebook_experiment_id)

        samples_entry: ExperimentEntry = [e for e in entries if e.entry_name == "Samples"][0]
        samples_entry.requires_grabber_plugin = False
        samples_entry.is_initialization_required = False
        # [OSLO-590]: Auto-complete the entry.
        self.eln_man.add_records_to_table_entry(experiment.notebook_experiment_id, samples_entry.entry_id,
                                                [s.get_data_record() for s in parent_samples])
        self.eln_man.update_experiment_entry(experiment.notebook_experiment_id, samples_entry.entry_id, criteria)

        # [OSLO-590]: Changed step name to "Extracted Samples" and auto-complete entry.
        extracted_samples_entry: ExperimentEntry = [e for e in entries if e.entry_name == "Extracted Samples"][0]
        self.eln_man.add_records_to_table_entry(experiment.notebook_experiment_id, extracted_samples_entry.entry_id,
                                                [s.get_data_record() for s in dna_aliquots])
        self.eln_man.update_experiment_entry(experiment.notebook_experiment_id, extracted_samples_entry.entry_id,
                                             criteria)

        # [OSLO-590]: Grab the corresponding instrument and add it to the Instrument Tracking entry, then change the
        # experiment name to reflect the instrument and the batch ID, and auto-complete the entry.
        try:
            instrument_tracking_entry: ExperimentEntry = \
                [entry for entry in entries if "INSTRUMENT TRACKING" in
                 list(self.eln_man.get_experiment_entry_options(experiment.notebook_experiment_id, entry.entry_id)
                      .keys())][0]
            record_model: PyRecordModel = (
                self.rec_handler.inst_man.add_new_record(instrument_tracking_entry.data_type_name))
            record_model.set_field_value("InstrumentUsed", self.instrument)
            record_model.set_field_value("InstrumentType", "QIAsymphony")
            self.eln_man.update_notebook_experiment(experiment.notebook_experiment_id, ElnExperimentUpdateCriteria(
                new_experiment_name=f"QIA DNA Extraction BATCHID-{batch_id} "
                                    f"[{'A' if self.instrument == 'qssp11431' else 'B'}]"))
            self.eln_man.update_experiment_entry(experiment.notebook_experiment_id, instrument_tracking_entry.entry_id,
                                                 criteria)
        except Exception:
            self.instrument_utils.log_instrument_error(self.instrument, self.file_path,
                                                       f"No insturment tracking entry in experiment.")
            return SapioWebhookResult(False)

        # [OSLO-590]: Add all the Reagents to the Reagent Tracking entry and complete it.
        if reagents:
            try:
                reagent_tracking_entry: ExperimentEntry = \
                    [entry for entry in entries if "CONSUMABLE TRACKING" in
                     list(self.eln_man.get_experiment_entry_options(experiment.notebook_experiment_id, entry.entry_id)
                          .keys())][0]
                for reagent in reagents:
                    record_model: PyRecordModel = (
                        self.rec_handler.inst_man.add_new_record(reagent_tracking_entry.data_type_name))
                    record_model.set_field_value("ConsumableType", reagent.get_ConsumableType_field())
                    record_model.set_field_value("ConsumableLot", reagent.get_LotNumber_field())
                    record_model.set_field_value("PartNumber", reagent.get_PartNumber_field())
                    record_model.set_field_value("ExpirationDate", reagent.get_ExpirationDate_field())
                    self.eln_man.update_experiment_entry(experiment.notebook_experiment_id,
                                                         reagent_tracking_entry.entry_id,
                                                         criteria)
            except Exception:
                self.instrument_utils.log_instrument_error(self.instrument, self.file_path,
                                                           f"No Reagent Tracking entry in experiment.")
                return SapioWebhookResult(False)

        # Store and commit
        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def send_pipette_files(self, samples: list[SampleModel]) -> None:
        # Generate and send the pipette files to FileBridge
        file_data: dict[str, bytes] = self.generate_pipette_file_data(samples)
        for file_name in file_data:
            FileBridgeUtils.upload_file(self.context, "oslo-filebridge", file_name, file_data[file_name], "QIAxpert")

    def generate_pipette_file_data(self, samples: list[SampleModel]) -> dict[str, bytes]:
        file_name_to_samples_dict: dict[str, list[SampleModel]] = {}
        file_data_dict: dict[str, bytes] = {}

        for s in samples:
            batch_id: str = self.batch_map[s.get_SampleId_field()]
            slide_no: int = self.get_slide_no_from_sample(s)

            file_name: str = f"{batch_id}_{slide_no}.csv"
            if file_name not in file_name_to_samples_dict:
                file_name_to_samples_dict[file_name] = []

            file_name_to_samples_dict[file_name].append(s)

        for file_name in file_name_to_samples_dict:
            file_data: str = ""
            slide_data_dict: dict[str, str] = self.create_slide_dict()

            for s in file_name_to_samples_dict[file_name]:
                slide_data_dict[self.calc_slide_well_pos(s)] = s.get_TubeBarcode_field()

            for well_location in slide_data_dict:
                file_data += f"{well_location},{slide_data_dict[well_location]}\n"

            file_data_dict[file_name] = bytes(file_data, "utf-8")

        return file_data_dict

    def get_slide_no_from_sample(self, sample: SampleModel) -> int:
        slide_no: int = 0

        col: int = int(sample.get_ColPosition_field())
        if col == 1 or col == 2:
            slide_no = 1
        elif col == 3 or col == 4:
            slide_no = 2
        elif col == 5 or col == 6:
            slide_no = 3
        elif col == 7 or col == 8:
            slide_no = 4
        elif col == 9 or col == 10:
            slide_no = 5
        elif col == 11 or col == 12:
            slide_no = 6

        return slide_no

    def calc_slide_well_pos(self, sample: SampleModel) -> str:
        if int(sample.get_ColPosition_field()) % 2 == 0:
            col: str = "2"
        else:
            col: str = "1"
        return f"{sample.get_RowPosition_field()}{col}"

    def create_slide_dict(self) -> dict[str, str]:
        return {
            "A1": "",
            "B1": "",
            "C1": "",
            "D1": "",
            "E1": "",
            "F1": "",
            "G1": "",
            "H1": "",
            "A2": "",
            "B2": "",
            "C2": "",
            "D2": "",
            "E2": "",
            "F2": "",
            "G2": "",
            "H2": "",
        }

    def get_matching_reagents(self, lots_and_parts: list[(str, str)]) -> list[ConsumableItemModel]:
        columns: list[ReportColumn] = [ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)]
        raw_terms: list[(RawReportTerm, RawReportTerm)] = []
        for lot_and_part in lots_and_parts:
            raw_terms.append((RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            lot_and_part[0]),
                              RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.PARTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.EQUAL_TO_OPERATOR,
                                            lot_and_part[1])))
        composite_terms: list[CompositeReportTerm] = [
            CompositeReportTerm(raw_terms[0][0], CompositeTermOperation.AND_OPERATOR,
                                raw_terms[0][1])]
        root_term: CompositeReportTerm = composite_terms[0]
        if len(raw_terms) > 1:
            for x in range(1, len(raw_terms)):
                composite_terms.append(CompositeReportTerm(raw_terms[x][0], CompositeTermOperation.AND_OPERATOR,
                                                           raw_terms[x][1]))
            for x in range(1, len(composite_terms)):
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.OR_OPERATOR, composite_terms[x])
        else:
            root_term = CompositeReportTerm(raw_terms[0][0], CompositeTermOperation.AND_OPERATOR, raw_terms[0][1])
        criteria: CustomReportCriteria = CustomReportCriteria(columns, root_term)

        results: list[list[Any]] = (DataMgmtServer.get_custom_report_manager(self.context.user)
                                    .run_custom_report(criteria)
                                    .result_table)
        if not results:
            return None

        record_ids: list[int] = [x[0] for x in results]
        return self.rec_handler.query_models_by_id(ConsumableItemModel, record_ids)
