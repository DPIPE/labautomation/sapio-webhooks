import hashlib
import json
import re
from typing import cast

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FilePromptResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from sdms.id_reader_handler import IDReaderHandler
from sdms.nipt_file_handlers import NIPTBatchInititationReportFileHandler, NIPTLibraryReagentReportHandler, \
    NIPTInstrumentFileHandler, NIPTLibraryQuantReportHandler, NIPTQCSequencingReportHandler, NIPTPoolReportHandler, \
    NIPTBatchAttachmentHandler, NIPTReportHandler
from sdms.qia_symphony_handler import QIASymphonyHandler
from sdms.qiaxpert_handler import QIAxpertHandler
from utilities.constants import Constants
from utilities.data_models import ExemplarSDMSFileModel
from utilities.instrument_utils import InstrumentUtils
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_FILE_UPLOAD: str = "fileUpload"


class SDMSFileHandlerTest(OsloWebhookHandler):
    instrument_utils: InstrumentUtils
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_FILE_UPLOAD:
                return self.call_handler(context, cast(FilePromptResult, result))
        self.response_map[STATUS] = STATUS_FILE_UPLOAD
        return FileUtil.request_file(context, "Upload File", request_context=json.dumps(self.response_map))[0]

    def call_handler(self, context: SapioWebhookContext, result: FilePromptResult) -> SapioWebhookResult:
        self.instrument_utils: InstrumentUtils = InstrumentUtils(context)

        # Get the bytes and name of the file.
        file_bytes: bytes = result.file_bytes
        file_path: list[str] = result.file_path.split("/")
        file_name: str = file_path[len(file_path) - 1]

        # Check to make sure the file doesn't already exist in the system. If it does, then log an instrument error. We
        # need this check in order to mimic SDMS's actual behavior.
        if self.check_if_already_uploaded(file_name, file_bytes):
            return self.instrument_utils.log_instrument_error("Unknown", file_name,
                                                              "This file has already been uploaded to the system.")

        # Check the file type based on the file name, then call the appropriate handler.
        if re.match(Constants.RE_QIASYMPHONY, file_name):
            qia_symphony_handler = QIASymphonyHandler(context, self.rec_handler, self.rel_man, file_name,
                                                      file_bytes)
            return qia_symphony_handler.execute()
        elif re.match(Constants.RE_QIAXPERT, file_name):
            qiaxpert_handler: QIAxpertHandler = QIAxpertHandler(context, self.rec_handler, self.rel_man,
                                                                file_name, file_bytes)
            return qiaxpert_handler.execute()
        elif re.match(Constants.RE_IDREADER, file_name):
            id_reader_handler = IDReaderHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes)
            return id_reader_handler.execute()
        elif re.match(Constants.RE_QUANTSTUDIO, file_name):
            # Create an SDMS file record and let the rule handle the rest of the logic.
            self.create_sdms_file(context, file_name, result.file_path, file_bytes)
            return SapioWebhookResult(True)
        elif re.match(Constants.RE_TAQMAN, file_name):
            # Create an SDMS file record and let the rule handle the rest of the logic.
            self.create_sdms_file(context, file_name, result.file_path, file_bytes)
            return SapioWebhookResult(True)
        elif re.match(Constants.RE_BATCH_INIT, file_name):
            init_handler = (
                NIPTBatchInititationReportFileHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return init_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_REAGENT, file_name):
            lib_reagent_handler = (
                NIPTLibraryReagentReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return lib_reagent_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_PROCESS_LOG, file_name):
            instrument_file_handler = (
                NIPTInstrumentFileHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes, True))
            return instrument_file_handler.execute()
        elif re.match(Constants.RE_BATCH_SEQ_REPORT, file_name):
            seq_report_handler = (
                NIPTQCSequencingReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return seq_report_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_QUANT_REPORT, file_name):
            lib_quant_handler = (
                NIPTLibraryQuantReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return lib_quant_handler.execute()
        elif re.match(Constants.RE_BATCH_POOL_REPORT, file_name):
            pool_report_handler = (
                NIPTPoolReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return pool_report_handler.execute()
        elif re.match(Constants.RE_BATCH_CONCENTRATIONS, file_name) or re.match(Constants.RE_BATCH_CONCENTRATION_MAP,
                                                                                file_name):
            batch_attachment_handler = (
                NIPTBatchAttachmentHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return batch_attachment_handler.execute()
        elif re.match(Constants.RE_BATCH_NIPT_REPORT, file_name):
            nipt_report_handler = (
                NIPTReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return nipt_report_handler.execute()
        else:
            return self.instrument_utils.log_instrument_error("Unknown", file_name,
                                                              "The instrument file does not have any linked "
                                                              "configurations.")

    def check_if_already_uploaded(self, file_path: str, file_bytes: bytes) -> bool:
        # If the file cannot be hashed by its contents, then just query it by file name.
        try:
            file_hash: str = hashlib.md5(file_bytes).hexdigest()
            existing_files: list[ExemplarSDMSFileModel] = (
                self.rec_handler.query_models(ExemplarSDMSFileModel,
                                              ExemplarSDMSFileModel.FILEHASH__FIELD_NAME.field_name,
                                              [file_hash]))
        except Exception:
            existing_files: list[ExemplarSDMSFileModel] = (
                self.rec_handler.query_models(ExemplarSDMSFileModel,
                                              ExemplarSDMSFileModel.FILEPATH__FIELD_NAME.field_name,
                                              [file_path]))
        return len(existing_files) > 0

    def create_sdms_file(self, context: SapioWebhookContext, file_name: str, file_path: str, file_bytes: bytes) -> None:
        rec_handler: RecordHandler = RecordHandler(context)
        sdms_file: ExemplarSDMSFileModel = rec_handler.add_model(ExemplarSDMSFileModel)
        sdms_file.set_FilePath_field(file_name)
        sdms_file.set_FileSourcePath_field(file_path)
        self.rec_man.store_and_commit()
        AttachmentUtil.set_attachment_bytes(context, sdms_file, file_name, file_bytes)
        sdms_file.set_FileHash_field(hashlib.md5(file_bytes).hexdigest())
        self.rec_man.store_and_commit()
