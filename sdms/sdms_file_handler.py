import re

from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from sdms.id_reader_handler import IDReaderHandler
from sdms.nipt_file_handlers import NIPTBatchInititationReportFileHandler, NIPTLibraryReagentReportHandler, \
    NIPTInstrumentFileHandler, NIPTQCSequencingReportHandler, NIPTLibraryQuantReportHandler, NIPTPoolReportHandler, \
    NIPTBatchAttachmentHandler, NIPTReportHandler
from sdms.qia_symphony_handler import QIASymphonyHandler
from sdms.qiaxpert_handler import QIAxpertHandler
from sdms.quant_studio_handler import QuantStudioHandler
from sdms.taqman_handler import TaqManHandler
from utilities.constants import Constants
from utilities.data_models import ExemplarSDMSFileModel
from utilities.instrument_utils import InstrumentUtils
from utilities.webhook_handler import OsloWebhookHandler


class SDMSFileHandler(OsloWebhookHandler):
    rec_handler: RecordHandler
    instrument_utils: InstrumentUtils

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Check if the file is a QIAsymphony or QIAxpert file, then decide what to do from there
        self.rec_handler = RecordHandler(context)
        self.instrument_utils = InstrumentUtils(context)
        sdms_file: ExemplarSDMSFileModel = self.rec_handler.wrap_model(context.data_record, ExemplarSDMSFileModel)
        file_name: str = sdms_file.get_FilePath_field()

        # Get the bytes from the file
        file_bytes: bytes = AttachmentUtil.get_attachment_bytes(context, sdms_file)

        # Check the file type based on the file name, then call the appropriate handler.
        if re.match(Constants.RE_QIASYMPHONY, file_name):
            qia_symphony_handler = QIASymphonyHandler(context, self.rec_handler, self.rel_man, file_name,
                                                      file_bytes)
            return qia_symphony_handler.execute()
        elif re.match(Constants.RE_QIAXPERT, file_name):
            qiaxpert_handler: QIAxpertHandler = QIAxpertHandler(context, self.rec_handler, self.rel_man, file_name,
                                                                file_bytes)
            return qiaxpert_handler.execute()
        elif re.match(Constants.RE_IDREADER, file_name):
            id_reader_handler = IDReaderHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes)
            return id_reader_handler.execute()
        elif re.match(Constants.RE_QUANTSTUDIO, file_name):
            quant_studio_handler = QuantStudioHandler(context, self.rec_handler, self.inst_man, sdms_file)
            return quant_studio_handler.execute()
        elif re.match(Constants.RE_TAQMAN, file_name):
            taqman_handler = TaqManHandler(context, self.rec_handler, self.inst_man, sdms_file)
            return taqman_handler.execute()
        elif re.match(Constants.RE_BATCH_INIT, file_name):
            init_handler = (
                NIPTBatchInititationReportFileHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return init_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_REAGENT, file_name):
            lib_reagent_handler = (
                NIPTLibraryReagentReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return lib_reagent_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_PROCESS_LOG, file_name):
            instrument_file_handler = (
                NIPTInstrumentFileHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes, True))
            return instrument_file_handler.execute()
        elif re.match(Constants.RE_BATCH_SEQ_REPORT, file_name):
            seq_report_handler = (
                NIPTQCSequencingReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return seq_report_handler.execute()
        elif re.match(Constants.RE_BATCH_LIB_QUANT_REPORT, file_name):
            lib_quant_handler = (
                NIPTLibraryQuantReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return lib_quant_handler.execute()
        elif re.match(Constants.RE_BATCH_POOL_REPORT, file_name):
            pool_report_handler = (
                NIPTPoolReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return pool_report_handler.execute()
        elif re.match(Constants.RE_BATCH_CONCENTRATIONS, file_name) or re.match(Constants.RE_BATCH_CONCENTRATION_MAP,
                                                                                file_name):
            batch_attachment_handler = (
                NIPTBatchAttachmentHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return batch_attachment_handler.execute()
        elif re.match(Constants.RE_BATCH_NIPT_REPORT, file_name):
            nipt_report_handler = (
                NIPTReportHandler(context, self.rec_handler, self.rel_man, file_name, file_bytes))
            return nipt_report_handler.execute()
        else:
            instrument_utils: InstrumentUtils = InstrumentUtils(context)
            return instrument_utils.log_instrument_error("Unknown", file_name,
                                                         "The instrument file does not have any linked "
                                                         "configurations.")
