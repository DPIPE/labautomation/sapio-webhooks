from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ElnExperiment import ElnExperiment
from sapiopylib.rest.pojo.eln.ExperimentEntry import ExperimentEntry
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelRelationshipManager

from utilities.data_models import SampleModel
from utilities.instrument_utils import InstrumentUtils

HEADERS: list[str] = ["Position", "Sample name", "dsDNA (ng/ul)", "Nucleic acids (ng/ul)", "Impurities (A260)",
                      "Background (A260)", "Residue (%)", "A260", "A260/A280", "A260/A230"]


class QIAxpertHandler:
    context: SapioWebhookContext
    rec_handler: RecordHandler
    rel_man: RecordModelRelationshipManager
    eln_man: ElnManager
    instrument_utils: InstrumentUtils
    file_path: str
    file_bytes: bytes

    def __init__(self, context: SapioWebhookContext, rec_handler: RecordHandler,
                 rel_man: RecordModelRelationshipManager,
                 file_path: str, file_bytes: bytes):
        self.context = context
        self.rec_handler = rec_handler
        self.rel_man = rel_man
        self.file_path = file_path
        self.file_bytes = file_bytes

        self.eln_man = DataMgmtServer.get_eln_manager(self.context.user)

        self.instrument_utils = InstrumentUtils(self.context)

    def execute(self) -> SapioWebhookResult:
        return self.parse_qiaxpert_file()

    def parse_qiaxpert_file(self) -> SapioWebhookResult:
        # Get the experiment with the matching batch ID
        batch_id: str = self.file_path.split("_")[4]

        # [OSLO-590]: Updated generated experiment name.
        experiments: list[ElnExperiment] = [e for e in self.eln_man.get_eln_experiment_list()
                                            if f"BATCHID-{batch_id}" in e.notebook_experiment_name]
        if not experiments:
            return self.instrument_utils.log_instrument_error("QIAxpert", self.file_path,
                                                              f"No matching experiment for batch ID {batch_id} found.")

        experiment: ElnExperiment = experiments[0]

        # Parse the file and add data to the QIAxpert Results entry
        try:
            file_data: list[dict[str, str]] = FileUtil.tokenize_csv(self.file_bytes, HEADERS)[0]

        except Exception as e:
            return self.instrument_utils.log_instrument_error("QIAxpert", self.file_path, repr(e))

        qiaxpert_entry: ExperimentEntry = [e for e in
                                           self.eln_man.get_experiment_entry_list(experiment.notebook_experiment_id)
                                           if e.entry_name == "QIAxpert Results"][0]
        data_type_name: str = qiaxpert_entry.data_type_name

        # [OSLO-590]: Renamed entry to "Extracted Samples."
        extracted_samples: ExperimentEntry = [e for e in
                                              self.eln_man.get_experiment_entry_list(experiment.notebook_experiment_id)
                                              if e.entry_name == "Extracted Samples"][0]
        samples: list[SampleModel] = (
            self.rec_handler.wrap_models(self.eln_man.get_data_records_for_entry(experiment.notebook_experiment_id,
                                                                                 extracted_samples.entry_id).result_list,
                                         SampleModel))
        sample_ids: list[str] = [s.get_SampleId_field() for s in samples]

        table_records: list[PyRecordModel] = []
        for i, d in enumerate(file_data, start=2):
            record: PyRecordModel = self.rec_handler.inst_man.add_new_record(data_type_name)
            record.set_field_value("SampleName", d["Sample name"])

            if d["Sample name"] in sample_ids:
                record.set_field_value("Result", "Passed")
            else:
                record.set_field_value("Result", "Unknown")

            record.set_field_value("dsDNA", float(d["dsDNA (ng/ul)"]))
            record.set_field_value("NucleicAcids", float(d["Nucleic acids (ng/ul)"]))
            record.set_field_value("Residue", float(d["Residue (%)"]))
            record.set_field_value("A260A230", float(d["A260/A230"]))
            record.set_field_value("A260A280", float(d["A260/A280"]))

        # Store and commit so we can get the data records
        self.rec_handler.rec_man.store_and_commit()

        self.eln_man.add_records_to_table_entry(experiment.notebook_experiment_id, qiaxpert_entry.entry_id,
                                                [t.get_data_record() for t in table_records])

        # Store and commit
        self.rec_handler.rec_man.store_and_commit()

        return SapioWebhookResult(True)
