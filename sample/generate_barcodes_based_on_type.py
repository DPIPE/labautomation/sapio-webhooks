import csv
import io
from datetime import datetime

from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-633
Description : Sample Data Record Button, Sample Data Table Toolbar button and ELN Entry Toolbar button Restricted on 
Sample Type and Table Entry Type Button to gnerarte barcode based on sample type.
> If sample type = STRECK  -> Print NIPT label 
> Any other sample type -> Print primary sample label 

"""

class GenerateBarcodesBasedOnType(OsloWebhookHandler):
    NIPT_FIELDS = [SampleModel.SAMPLEID__FIELD_NAME.field_name, SampleModel.SAMPLEID__FIELD_NAME.field_name,
                   SampleModel.PLATEID__FIELD_NAME.field_name]
    PRIMARY_SAMPLE_FIELDS = [SampleModel.SAMPLEID__FIELD_NAME.field_name]
    NETWORK_PATH = "Files/"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)
        sample_records = context.data_record_list
        if not sample_records and context.eln_experiment and context.experiment_entry:
            sample_records = context.eln_manager.get_data_records_for_entry\
                (context.eln_experiment.notebook_experiment_id, context.experiment_entry.entry_id)

        # if no associated samples found then throw error
        if not sample_records:
            return PopupUtil.ok_popup("Error", f" No Samples Found")

        # Ensure all are STRECK type or None are
        samples = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)
        has_streck = False
        first = True
        for sample in samples:
            if sample.get_ExemplarSampleType_field() == "STRECK":
                if first:
                    has_streck = True
                elif not has_streck:
                    return PopupUtil.ok_popup("Error", f" Mix Of STRECK And Other Sample Types Found")
            elif has_streck:
                return PopupUtil.ok_popup("Error", f" Mix Of STRECK And Other Sample Types Found")
            if first:
                first = False

        # iterate over samples and generate files accordingly
        type  = samples[0].get_ExemplarSampleType_field()
        file_lines = []
        if "STRECK" == type:
            file_lines.append(self.NIPT_FIELDS)
            for sample in samples:
                row = []
                for field in self.NIPT_FIELDS:
                    row.append(sample.get_field_value(field))
                file_lines.append(row)
        else:
            file_lines.append(self.PRIMARY_SAMPLE_FIELDS)
            for sample in samples:
                row = []
                for field in self.PRIMARY_SAMPLE_FIELDS:
                    row.append(sample.get_field_value(field))
                file_lines.append(row)

        # generate file
        with io.StringIO(newline='') as csv_buffer:
            csv_writer = csv.writer(csv_buffer)
            csv_writer.writerows(file_lines)
            csv_buffer.seek(0)
            csv_bytes = csv_buffer.read()

        # write file to user
        # Get the current date and time
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y%m%d_%H%M%S")
        file_name = context.user.username+"_Sample_" + formatted_datetime + ".csv"
        try:
            FileBridge.write_file(context, "oslo-filebridge", self.NETWORK_PATH + file_name, csv_bytes)
            return SapioWebhookResult(True)
        except:
            pass
        write_file_request: WriteFileRequest = WriteFileRequest(file_bytes=csv_bytes.encode('UTF-8'),
                                                                file_path=file_name)
        return SapioWebhookResult(True, client_callback_request=write_file_request)
