import re
from typing import List

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, PromegaModel, PromegaPartModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-1000
Description : Action button webhook on promega to perform dilution as per requirements
"""


class PerformDilutionPromega(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)

        # Get the sample
        promega: PromegaModel = self.inst_man.add_existing_record_of_type(context.data_record, PromegaModel)
        self.rel_man.load_parents_of_type([promega], SampleModel)
        sample: SampleModel = promega.get_parent_of_type(SampleModel)
        self.rel_man.load_parents_of_type([sample], PromegaPartModel)

        # get the parent part
        promega_part: PromegaPartModel = sample.get_parent_of_type(PromegaPartModel)
        self.rel_man.load_children_of_type([promega_part], SampleModel)
        existing_samples: List[SampleModel] = promega_part.get_children_of_type(SampleModel)
        self.rel_man.load_children_of_type(existing_samples, PromegaModel)

        # get a set of lot numbers that already exists to ensure no duplicates
        existing_lot_numbers = set()
        for s in existing_samples:
            existing_promega = s.get_child_of_type(PromegaModel)
            if existing_promega:
                existing_lot_numbers.add(existing_promega.get_LotNumber_field())

        # Prompt user to select the dilution concentration
        option = client_callback.list_dialog("Please select the dilution being performed", ["12 ng/μl", "15 ng/μl"])\
            .pop()
        concentration = 15
        if option == "12 ng/μl":
            concentration = 12

        # create the new sample and set its fields
        new_sample: SampleModel = self.inst_man.add_new_record_of_type(SampleModel)
        new_promega: PromegaModel = self.inst_man.add_new_record_of_type(PromegaModel)
        new_promega.set_PartNumber_field(promega.get_PartNumber_field())
        lot_number = promega.get_LotNumber_field() + f"_{concentration}"
        # if lot number exits then add a unique incrementing number at end
        if lot_number in existing_lot_numbers:
            existing_lot_numbers = list(existing_lot_numbers)
            pattern = re.compile(fr"{lot_number}_"+r'\d+$')
            lot_number_count = 1
            for existing_lot_number in existing_lot_numbers:
                if pattern.match(existing_lot_number):
                    count = int(existing_lot_number.split("_")[-1])
                    if count > lot_number_count:
                        lot_number_count = count
            lot_number = lot_number + f"_{lot_number_count}"
        new_promega.set_LotNumber_field(lot_number)
        new_sample.set_Concentration_field(concentration)
        new_promega.set_ProbeMix_field(promega.get_ProbeMix_field())
        new_promega.set_ConsumableType_field(promega.get_ConsumableType_field())
        new_promega.set_Status_field("Pending")

        # perform the dilutions
        source_volume = sample.get_Volume_field()
        if not source_volume or source_volume - 2 < 0:
            source_volume = 2
        sample.set_Volume_field(source_volume-2)

        source_concentration = sample.get_Concentration_field()
        if not source_concentration:
            client_callback.ok_dialog("Error", "Source concentration not specified")
            return SapioWebhookResult(True)
        mass = source_concentration/2
        new_volume = mass/concentration
        if new_volume < 0:
            new_volume = 0
        new_sample.set_Volume_field(new_volume)
        new_sample.add_child(new_promega)
        promega_part.add_child(new_sample)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, display_text="Dilution Performed")
