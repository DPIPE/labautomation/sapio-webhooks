from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import RawReportTerm, RawTermOperation, ReportColumn, \
    CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PromegaPartModel, InHousePartModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-1001
Description : Webhook to populate selection list for the control fields on probe mix
"""


class ListOfAllPromegaAndInHouseTypes(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        list_of_all_types = self.__get_available_in_house_types() + self.__get_available_promega_types()
        return SapioWebhookResult(True, list_values=list_of_all_types)

    def __get_available_promega_types(self) -> list[str]:
        # set terms as per the params
        promega_term = RawReportTerm(
            PromegaPartModel.DATA_TYPE_NAME,
            PromegaPartModel.QUANTITYONHAND__FIELD_NAME.field_name,
            RawTermOperation.GREATER_THAN_OR_EQUAL_OPERATOR,
            "0",
            is_negated=False,
        )

        # set column to retrieve
        column_list = [
            ReportColumn(
                PromegaPartModel.DATA_TYPE_NAME, PromegaPartModel.CONSUMABLETYPE__FIELD_NAME.field_name, FieldType
                .STRING
            )
        ]

        # run search
        custom_report_manager = DataMgmtServer.get_custom_report_manager(self.context.user)
        request = CustomReportCriteria(column_list, promega_term)
        results = custom_report_manager.run_custom_report(request).result_table

        # get the sample record ids
        result = []
        if not results:
            return result
        else:
            for r in results:
                result.append(r.pop())

        # ensure uniqueness
        result = list(set(result))

        return result

    def __get_available_in_house_types(self) -> list[str]:
        # set terms as per the params
        in_house_term = RawReportTerm(
            InHousePartModel.DATA_TYPE_NAME,
            InHousePartModel.QUANTITYONHAND__FIELD_NAME.field_name,
            RawTermOperation.GREATER_THAN_OR_EQUAL_OPERATOR,
            "0",
            is_negated=False,
        )

        # set column to retrieve
        column_list = [
            ReportColumn(
                InHousePartModel.DATA_TYPE_NAME, InHousePartModel.CONSUMABLETYPE__FIELD_NAME.field_name, FieldType
                .STRING
            )
        ]

        # run search
        custom_report_manager = DataMgmtServer.get_custom_report_manager(self.context.user)
        request = CustomReportCriteria(column_list, in_house_term)
        results = custom_report_manager.run_custom_report(request).result_table

        # get the sample record ids
        result = []
        if not results:
            return result
        else:
            for r in results:
                result.append(r.pop())

        # ensure uniqueness
        result = list(set(result))

        return result
