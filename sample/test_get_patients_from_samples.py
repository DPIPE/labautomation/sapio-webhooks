from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, PatientModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class TestGetPatientsFromSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        sample: SampleModel = self.rec_handler.wrap_model(context.data_record, SampleModel)
        patients_to_samples: dict[PatientModel, list[SampleModel]] = (
            SampleUtils.get_patients_from_samples(context, self.rec_handler, [sample]))
        if not patients_to_samples:
            return SapioWebhookResult(False, "Patient could not be found.")
        return SapioWebhookResult(True, directive=FormDirective(list(patients_to_samples.keys())[0].get_data_record()))
