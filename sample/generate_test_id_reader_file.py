from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler

location_cells: list[str] = ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12',
                             'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12',
                             'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12',
                             'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12',
                             'E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10', 'E11', 'E12',
                             'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12',
                             'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10', 'G11', 'G12',
                             'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9', 'H10', 'H11', 'H12']

headers: list[str] = ["RackID", "LocationCell", "TubeCode"]


class GenerateTestIDReaderFile(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.callback_util = CallbackUtil(context.user)

        samples: list[SampleModel] = self.rec_handler.wrap_models(context.data_record_list, SampleModel)

        # Check to see if a new plate ID should be created based on if the samples are all on the sample plate or not.
        plate_ids: list[str] = list(set([sample.get_StorageLocationBarcode_field() for sample in samples]))
        new_plate_id: bool = len(plate_ids) > 1 or plate_ids[0] == ""
        if new_plate_id:
            self.callback_util.ok_dialog("Warning", "Not all samples selected are on the same plate. A new plate ID "
                                                    "will be generated for them.")
            plate_id: str = (f"{Constants.ACCESSIONING_TEST_PLATE_ID}"
                             f"{DataMgmtServer.get_accession_manager(context.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PLATE_ID))[0]}")
        else:
            plate_id = plate_ids.pop()

        # Write data to the file.
        file_data: str = ",".join(headers) + "\n"
        for x, sample in enumerate(samples):
            file_data += f"{plate_id},{location_cells[x] if new_plate_id else sample.get_RowPosition_field() + sample.get_ColPosition_field()},{sample.get_TubeBarcode_field()}\n"

        # Download the file to the user.
        self.callback_util.write_file(f"Rack File - {plate_id}.csv", file_data)
        return SapioWebhookResult(True)
