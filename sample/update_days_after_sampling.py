from typing import Any

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities import utils
from utilities.data_models import SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class UpdateDaysAfterSampling(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            samples: list[SampleModel] = self.rec_handler.wrap_models(context.data_record_list, SampleModel)
        except Exception:
            # Get the samples that need to be updated by running the predefined search.
            results: list[list[Any]] = (DataMgmtServer.get_custom_report_manager(context.user)
                                        .run_system_report_by_name("Samples Days after Sampling to Update")
                                        .result_table)
            if not results:
                return SapioWebhookResult(True)
            samples: list[SampleModel] = self.rec_handler.query_models_by_id(SampleModel, [x[0] for x in results])

        # Set the days after labeling field for each sample by the number of days that have passed from the labeling
        # date.
        for sample in samples:
            sample.set_DaysAfterSampling_field(utils.get_days_since_date(sample.get_SamplingDate_field()))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
