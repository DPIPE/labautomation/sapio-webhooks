from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import TableDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import FamilyModel, SampleModel
from utilities.family_utils import FamilyUtils
from utilities.webhook_handler import OsloWebhookHandler


class ShowSampleFamilies(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)

        sample: SampleModel = self.rec_handler.wrap_model(context.data_record, SampleModel)

        family_map: dict[FamilyModel, list[SampleModel]] = FamilyUtils.get_family_map(self.rec_handler,
                                                                                      self.rel_man,
                                                                                      [sample])

        families: list[DataRecord] = [f.get_data_record() for f in family_map]
        if families is None or len(families) == 0:
            return PopupUtil.ok_popup("Notice", "This sample is not in any families.")

        return SapioWebhookResult(True, directive=TableDirective(families))
