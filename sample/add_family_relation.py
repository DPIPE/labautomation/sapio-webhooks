import json
from typing import Any, cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxIntegerFieldDefinition, VeloxPickListFieldDefinition, \
    VeloxStringFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult, FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.data_models import SampleModel, FamilyModel, FamilyDetailsModel
from utilities.family_utils import FamilyUtils
from utilities.webhook_handler import OsloWebhookHandler


class AddFamilyRelation(OsloWebhookHandler):
    rec_handler: RecordHandler

    response_map: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        self.response_map = {"status": ""}

        # Get the sample
        sample: SampleModel = self.rec_handler.wrap_model(context.data_record, SampleModel)

        result: ClientCallbackResult = context.client_callback_result

        if result is None:
            in_family: bool = self.check_if_sample_in_family(sample)
            if not in_family:
                return self.prompt_family_params()

            else:
                return self.prompt_add_new_family()

        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_map = json.loads(result.callback_context_data)

        if self.response_map["status"] == "familyParams":
            return self.add_sample_to_family(sample, cast(FormEntryDialogResult, result))

        if self.response_map["status"] == "familyConfirm":
            return self.handle_confirm_input(cast(OptionDialogResult, result))

        if self.response_map["status"] == "error":
            return SapioWebhookResult(True)

    def check_if_sample_in_family(self, sample: SampleModel) -> bool:
        if len(FamilyUtils.get_families(self.rec_handler, self.rel_man, sample)) == 0:
            return False

        return True

    def prompt_family_params(self) -> SapioWebhookResult:
        form_builder: FormBuilder = FormBuilder()

        family_number_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                     "Family ID",
                                                                                     "Family ID")
        family_number_field.editable = True
        family_number_field.required = True
        form_builder.add_field(family_number_field)

        family_role_field: VeloxPickListFieldDefinition = VeloxPickListFieldDefinition(
            form_builder.get_data_type_name(),
            "Family Role",
            "Family Role",
            "Family Role")
        family_role_field.editable = True
        family_role_field.required = True
        form_builder.add_field(family_role_field)

        temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()

        self.response_map["status"] = "familyParams"
        request: FormEntryDialogRequest = FormEntryDialogRequest("Enter Family Info",
                                                                 "Enter the required family info",
                                                                 temp_dt,
                                                                 callback_context_data=json.dumps(self.response_map))

        return SapioWebhookResult(True, client_callback_request=request)

    def prompt_add_new_family(self) -> SapioWebhookResult:
        self.response_map["status"] = "familyConfirm"
        return PopupUtil.yes_no_popup("Warning", "This sample is already part of one or more families. Would you like"
                                                 " to add it to a new one?",
                                      request_context=json.dumps(self.response_map))

    def add_sample_to_family(self, sample: SampleModel, result: FormEntryDialogResult) -> SapioWebhookResult:
        user_response_map: dict[str, Any] = result.user_response_map

        # Get the family ID from the response map
        family_id: str = user_response_map["Family ID"]
        selected_family_role: str = user_response_map["Family Role"]

        # Check if the family ID exists, if so, verify that the sample can be added to the family
        family: FamilyModel

        families: list[FamilyModel] = FamilyUtils.get_families(self.rec_handler, self.rel_man, sample, [family_id])
        if len(families) > 0:
            family = families[0]

            # Check the count of the family to make sure it isn't full
            if family.get_FamilyCount_field() == 3:
                self.response_map["status"] = "error"
                return PopupUtil.ok_popup("Error", f"Family with Family ID of {family_id} is already full.",
                                          request_context=json.dumps(self.response_map))

            # Check to make sure that the sample is not already in the family
            family_details_list: list[FamilyDetailsModel] = FamilyUtils.get_family_details(self.rec_handler, family_id)
            sample_family_details: list[FamilyDetailsModel] = sample.get_children_of_type(FamilyDetailsModel)

            for s in sample_family_details:
                if s in family_details_list:
                    self.response_map["status"] = "error"
                    return PopupUtil.ok_popup("Error", f"This sample already exists in family with Family ID of "
                                                       f"{family_id}.",
                                              request_context=json.dumps(self.response_map))

            # Check the family role selection from the user to ensure that it doesn't already exist in the family
            family_roles: list[str] = [f.get_FamilyRole_field() for f in family_details_list]

            if selected_family_role in family_roles:
                self.response_map["status"] = "error"
                return PopupUtil.ok_popup("Error", f"Family role of {selected_family_role} is already in family with"
                                                   f" Family ID of {family_id}.",
                                          request_context=json.dumps(self.response_map))

        else:
            family = self.rec_handler.add_model(FamilyModel)
            family.set_FamilyId_field(family_id)

        # Add the family details record if there are no complications
        family_details: FamilyDetailsModel = self.rec_handler.add_model(FamilyDetailsModel)
        family_details.set_FamilyId_field(family_id)
        family_details.set_FamilyRole_field(selected_family_role)

        # Add the family details record as a child of the sample
        sample.add_child(family_details)

        # Increment the family count
        FamilyUtils.increment_family_count(family)

        # Store and commit changes
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, "Added sample to family.")

    def handle_confirm_input(self, result: OptionDialogResult) -> SapioWebhookResult:
        if result.selection == 0:
            return self.prompt_family_params()

        else:
            return SapioWebhookResult(True)
