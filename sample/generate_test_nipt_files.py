import re

from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, ConsumableItemModel, InstrumentModel
from utilities.webhook_handler import OsloWebhookHandler


class GenerateTestNIPTFiles(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Prompt the user to select which type of file to generate.
        file_type: list[str] | None = (
            self.callback.list_dialog("Generate Test NIPT File",
                                      ["Batch Initiation Report file", "Library Reagent Report file",
                                       "Library Process Log file", "Sequencing Report file",
                                       "Library Quantification Report file", "Pool Report file",
                                       "NIPT Report file"],))
        if not file_type:
            return SapioWebhookResult(True)

        # Generate the file that the user selected.
        if file_type[0] == "Batch Initiation Report file":
            return self.generate_batch_init_file()
        elif file_type[0] == "Library Reagent Report file":
            return self.generate_library_reagent_report_file()
        elif file_type[0] == "Library Process Log file":
            return self.generate_library_process_log_file()
        elif file_type[0] == "Sequencing Report file":
            return self.generate_sequencing_report_file()
        elif file_type[0] == "Library Quantification Report file":
            return self.generate_library_quant_report_file()
        elif file_type[0] == "Pool Report file":
            return self.generate_pool_report_file()
        elif file_type[0] == "NIPT Report file":
            return self.generate_nipt_report_file()

        return SapioWebhookResult(True)

    def generate_batch_init_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != SampleModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not samples.")
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = ("batch_name\tsample_barcode\tsample_type\twell\tassay\tmethod_version"
                          "\tworkflow_manager_version")

        # Write data for each sample.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.context.data_record_list, SampleModel)
        for sample in samples:
            file_data += (f"\n{project_name}\t{sample.get_TubeBarcode_field()}\t{sample.get_RowPosition_field()}"
                          f"{sample.get_ColPosition_field()}")

        # Download the file to the user.
        file_name: str = (f"{project_name}_batch_initiation_report_{TimeUtil.now_in_format('%Y%m%d')}_"
                          f"{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_library_reagent_report_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != ConsumableItemModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not reagents.")
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = "batch_name\tprocess\treagent_name\tlot\texpiration_date\toperator\tinitiated"

        # Write data for each reagent.
        reagents: list[ConsumableItemModel] = (
            self.rec_handler.wrap_models(self.context.data_record_list, ConsumableItemModel))
        for reagent in reagents:
            file_data += (f"\n{project_name}\tLIBRARY:setup\t{reagent.get_ConsumableType_field()}\t"
                          f"{reagent.get_LotNumber_field()}\t"
                          f"{TimeUtil.millis_to_format(reagent.get_ExpirationDate_field(), '%Y-%m-%d') if reagent.get_ExpirationDate_field() else ''}\t"
                          f"{self.user.username}\t{TimeUtil.now_in_format('%Y-%m-%d')}")

        # Download the file to the user.
        file_name: str = (f"{project_name}_library_reagent_report_{TimeUtil.now_in_format('%Y%m%d')}_"
                          f"{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_library_process_log_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != InstrumentModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not instruments.")
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = "batch_name\tprocess\toperator\tinstrument\tstarted\tfinished\tstatus"

        # Write data for each reagent.
        instruments: list[InstrumentModel] = (
            self.rec_handler.wrap_models(self.context.data_record_list, InstrumentModel))
        for instrument in instruments:
            file_data += (f"\n{project_name}\tLIBRARY:setup\t{self.user.username}\t"
                          f"{instrument.get_SerialNumber_field()}")

        # Download the file to the user.
        file_name: str = f"{project_name}_library_process_log.tab"
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_sequencing_report_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != SampleModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not samples.")
            return SapioWebhookResult(True)

        # Prompt the user to select an instrument.
        all_instruments: list[InstrumentModel] | None = self.rec_handler.query_all_models(InstrumentModel)
        if not all_instruments:
            self.callback.ok_dialog("Error", "No instruments are present in the system.")
            return SapioWebhookResult(True)
        instruments: list[InstrumentModel] | None = (
            self.callback.record_selection_dialog("Select an insturment.",
                                                  [InstrumentModel.INSTRUMENTNAME__FIELD_NAME.field_name,
                                                   InstrumentModel.INSTRUMENTTYPE__FIELD_NAME.field_name,
                                                   InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name],
                                                  all_instruments, False))
        if not instruments:
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = ("batch_name\tpool_barcode\tinstrument\tflowcell\tsoftware_version\trun_folder"
                          "\tsequencing_status\tqc_status\tqc_reason\tcluster_density\tpct_q30\tpct_pf\tphasing"
                          "\tprephasing\tpredicted_aligned_reads\tstarted\tcompleted")

        # Write data for the samples.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.context.data_record_list, SampleModel)
        for sample in samples:
            file_data += (f"\n{project_name}\t{sample.get_TubeBarcode_field()}\t"
                          f"{instruments[0].get_SerialNumber_field()}\t{instruments[0].get_InstrumentName_field()}\t"
                          f"v1.0\tRUN_FOLDER\tcompleted\tpass\treason\t0.1\t4.32\t0.98\t0.01\t0.01\t1000000\t"
                          f"{TimeUtil.now_in_format('%Y-%m-%dT%H:%M:%S')}+2:00\t"
                          f"{TimeUtil.now_in_format('%Y-%m-%dT%H:%M:%S')}+2:00")

        # Download the file to the user.
        file_name: str = (f"{project_name}_C_TEST1234_{re.sub('[^a-zA-Z0-9]+', '', instruments[0].get_InstrumentName_field())}_sequencing_report_"
                          f"{TimeUtil.now_in_format('%Y%m%d')}_{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_library_quant_report_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != SampleModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not samples.")
            return SapioWebhookResult(True)

        # Prompt the user to select an instrument.
        all_instruments: list[InstrumentModel] | None = self.rec_handler.query_all_models(InstrumentModel)
        if not all_instruments:
            self.callback.ok_dialog("Error", "No instruments are present in the system.")
            return SapioWebhookResult(True)
        instruments: list[InstrumentModel] | None = (
            self.callback.record_selection_dialog("Select an insturment.",
                                                  [InstrumentModel.INSTRUMENTNAME__FIELD_NAME.field_name,
                                                   InstrumentModel.INSTRUMENTTYPE__FIELD_NAME.field_name,
                                                   InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name],
                                                  all_instruments, False))
        if not instruments:
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = ("batch_name\tquant_id\tinstrument\tstandard_r_squared\tstandard_intercept\tstandard_slope\t"
                          "median_ccn_pg_ul\tqc_status\tqc_reason\tinitiated")

        # Write data for the samples.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.context.data_record_list, SampleModel)
        for sample in samples:
            file_data += (f"\n{project_name}\t{sample.get_TubeBarcode_field()}\t"
                          f"{instruments[0].get_InstrumentName_field()}\t2.3\t5.3\t9.0\t2.2\tpass\t543231\t"
                          f"{TimeUtil.now_in_format('%Y-%m-%dT%H:%M:%S')}+2:00")

        # Download the file to the user.
        file_name: str = (f"{project_name}_library_quant_report_{TimeUtil.now_in_format('%Y%m%d')}_"
                          f"{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_pool_report_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != SampleModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not samples.")
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = "batch_name\tsample_barcode\tpool_barcode\tpool_type\tpooling_volume_ul\tpooling_comments"

        # Write data for the samples.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.context.data_record_list, SampleModel)
        for sample in samples:
            file_data += f"\n{project_name}\t{sample.get_TubeBarcode_field()}\tPT12345\tC\t3.21\ttest"

        # Download the file to the user.
        file_name: str = (f"{project_name}_PT12345_pool_report_{TimeUtil.now_in_format('%Y%m%d')}_"
                          f"{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)

    def generate_nipt_report_file(self) -> SapioWebhookResult:
        # Ensure that the correct data type is present in the context before continuing.
        if self.context.data_type_name != SampleModel.DATA_TYPE_NAME:
            self.callback.ok_dialog("Error", "The records present in the context are not samples.")
            return SapioWebhookResult(True)

        # Prompt the user to select an instrument.
        all_instruments: list[InstrumentModel] | None = self.rec_handler.query_all_models(InstrumentModel)
        if not all_instruments:
            self.callback.ok_dialog("Error", "No instruments are present in the system.")
            return SapioWebhookResult(True)
        instruments: list[InstrumentModel] | None = (
            self.callback.record_selection_dialog("Select an insturment.",
                                                  [InstrumentModel.INSTRUMENTNAME__FIELD_NAME.field_name,
                                                   InstrumentModel.INSTRUMENTTYPE__FIELD_NAME.field_name,
                                                   InstrumentModel.SERIALNUMBER__FIELD_NAME.field_name],
                                                  all_instruments, False))
        if not instruments:
            return SapioWebhookResult(True)

        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            project_name = f"{Constants.ACCESSIONING_TEST_PROJECT}{DataMgmtServer.get_accession_manager(self.user).accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PROJECT))[0]}"

        # Write the headers.
        file_data: str = ("batch_name\tsample_barcode\tsample_type\tsex_chrom\tscreen_type\tflowcell"
                          "\tclass_sx\tclass_auto\tanomaly_description\tqc_flag\tqc_reason\tff")

        # Write data for the samples.
        samples: list[SampleModel] = self.rec_handler.wrap_models(self.context.data_record_list, SampleModel)
        for sample in samples:
            file_data += (f"\n{project_name}\t{sample.get_TubeBarcode_field()}"
                          f"\t{sample.get_ExemplarSampleType_field()}\t{'yes'}\tbasic"
                          f"\t{instruments[0].get_InstrumentName_field()}\tNOT TESTED\tNOT ANOMALY DETECTED"
                          f"\tNO ANOMALY DECTECTED\tPASS\tNONE\t16%")

        # Download the file to the user.
        file_name: str = (f"{project_name}_C_PT12345_"
                          f"{re.sub('[^a-zA-Z0-9]+', '', instruments[0].get_InstrumentName_field())}_nipt_report_"
                          f"{TimeUtil.now_in_format('%Y%m%d')}_{TimeUtil.now_in_format('%H%M%S')}.tab")
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)
