from typing import Any

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.ClientCallbackService import ClientCallback
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxSelectionFieldDefinition, ListMode, \
    VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, AssignedProcessModel
from utilities.webhook_handler import OsloWebhookHandler


class DequeueSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    callback_util: CallbackUtil
    client_callback: ClientCallback
    data_type_man: DataTypeManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.callback_util = CallbackUtil(context)
        self.client_callback = ClientCallback(context.user)
        self.data_type_man = DataMgmtServer.get_data_type_manager(context.user)

        # Get all the processes that have a status of "Ready for -" and add a row for each of those samples.
        samples: list[SampleModel] = self.rec_handler.wrap_models(context.data_record_list, SampleModel)
        samples_to_processes, rows = self.create_rows(samples)
        if not rows:
            self.callback_util.ok_dialog("Warning", "None of the selected samples are currently queued for a process.")
            return SapioWebhookResult(True)

        # Display a table dialog for the user to select processes to dequeue samples from.
        results: list[dict[str, Any]] = self.prompt_table(rows)

        # Prompt a confirmation.
        confirm: bool = self.callback_util.yes_no_dialog("Confirm", "Are you sure you want to dequeue the samples?",
                                                         default_yes=False)
        if not confirm:
            return SapioWebhookResult(True)

        # Dequeue the samples from the removed processes.
        self.dequeue_samples(samples, samples_to_processes, results)

        return SapioWebhookResult(True)

    def create_rows(self, samples: list[SampleModel]) -> (dict[SampleModel, list[str]], list[dict[str, Any]]):
        rows: list[dict[str, Any]] = []
        samples_to_processes: dict[SampleModel, str] = {}
        self.rel_man.load_parents_of_type(samples, AssignedProcessModel)
        for sample in samples:
            row: dict[str, Any] = {"OtherSampleId": sample.get_OtherSampleId_field(),
                                   "SampleId": sample.get_SampleId_field(),
                                   "QueuedProcesses": None}
            sample_processes: list[AssignedProcessModel] = \
                [process for process in sample.get_parents_of_type(AssignedProcessModel) if "Ready for -" in
                 process.get_Status_field()]
            if sample_processes:
                data: str = ",".join([f"{process.get_ProcessName_field()} | "
                                      f"{process.get_Status_field().split('- ')[1]}"
                                      for process in sample_processes])
                row["QueuedProcesses"] = data
                samples_to_processes[sample] = data
                rows.append(row)
        return samples_to_processes, rows

    def prompt_table(self, rows: list[dict[str, Any]]) -> list[dict[str, Any]]:
        sample_name_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("OtherSampleId", "OtherSampleId",
                                                                                   "Sample Name")
        sample_id_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID")
        process_field: VeloxSelectionFieldDefinition = VeloxSelectionFieldDefinition("QueuedProcesses",
                                                                                     "QueuedProcesses",
                                                                                     "Queues sample is in",
                                                                                     ListMode.PLUGIN,
                                                                                     plugin_name="test")
        process_field.editable = True
        process_field.multi_select = True
        return self.callback_util.table_dialog("Dequeue Samples", "The following selected samples are currently queued"
                                                                  " for a process. Remove a process from the column to"
                                                                  " dequeue them.",
                                               [sample_name_field, sample_id_field, process_field], rows)

    def dequeue_samples(self, samples: list[SampleModel], samples_to_processes: dict[SampleModel, str],
                        results: list[dict[str, Any]]) -> None:
        for i, d in enumerate(results):
            sample: SampleModel = [sample for sample in samples if sample.get_SampleId_field() == d["SampleId"]][0]
            current_processes: list[str] = samples_to_processes[sample].split(",")
            selected_processes: list[str] = d["QueuedProcesses"].split(",") if d["QueuedProcesses"] else []
            processes_to_remove: set[str] = set(current_processes) - set(selected_processes)

            # Change the status of the corresponding assigned process record.
            for process in current_processes:
                if process in processes_to_remove:
                    split_data: list[str] = process.split(" | ")
                    process_name: str = split_data[0]
                    step_name: str = split_data[1]
                    assigned_process: AssignedProcessModel = \
                        [x for x in sample.get_parents_of_type(AssignedProcessModel)
                         if x.get_ProcessName_field() == process_name
                         and x.get_Status_field() == f"Ready for - {step_name}"][0]
                    assigned_process.set_Status_field(f"Canceled - {process_name}")

            # Set the sample status according to the number of processes the sample is queued for.
            if set(current_processes) == processes_to_remove:
                sample.set_ExemplarSampleStatus_field(sample.get_ExemplarSampleStatus_field().replace("Ready for",
                                                                                                      "Canceled"))
            else:
                status_processes: list[str] = sample.get_ExemplarSampleStatus_field().split(" - ")[1].split(", ")
                for process in processes_to_remove:
                    status_processes.remove(process.split(" | ")[1])
                if status_processes:
                    sample.set_ExemplarSampleStatus_field(f"Ready for - {', '.join(status_processes)}")
                else:
                    sample.set_ExemplarSampleStatus_field("")

        self.rec_man.store_and_commit()

