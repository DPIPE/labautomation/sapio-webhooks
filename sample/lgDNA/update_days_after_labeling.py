from typing import Any

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities import utils
from utilities.data_models import LabeledgDNAModel
from utilities.webhook_handler import OsloWebhookHandler


class UpdateDaysAfterLabeling(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            # Get all the L gDNA samples that had their labelled dates changes.
            lgDNAs: list[LabeledgDNAModel] = self.rec_handler.wrap_models(context.data_record_list, LabeledgDNAModel)
        except Exception:
            # Get the L gDNA extension records from each of the samples by running a predefined search.
            results: list[list[Any]] = (DataMgmtServer.get_custom_report_manager(context.user)
                                        .run_system_report_by_name("L gDNAs to Update").result_table)
            if not results:
                return SapioWebhookResult(True)
            lgDNAs: list[LabeledgDNAModel] = self.rec_handler.query_models_by_id(LabeledgDNAModel,
                                                                                 [x[0] for x in results])

        # Update all the days after labelling for each of the samples.
        for lgDNA in lgDNAs:
            lgDNA.set_DaysAfterLabeling_field(utils.get_days_since_date(lgDNA.get_LabellingDate_field()))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
