import json
from typing import cast

from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxPickListFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.data_models import SequencingMetadataModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS_ERROR: str = "error"
STATUS_FORM_INPUT: str = "input"

FIELD_SEQUENCER_TYPE: str = "Sequencer Type"


class AddSequencingMetadata(OsloWebhookHandler):
    rec_handler: RecordHandler

    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        self.response_map = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is None:
            return self.prompt_sequencing_metadata()

        return self.handle_client_callback_result(context, result)

    def prompt_sequencing_metadata(self) -> SapioWebhookResult:
        form_builder: FormBuilder = FormBuilder()

        sequencer_type_field: VeloxPickListFieldDefinition = VeloxPickListFieldDefinition(
            form_builder.get_data_type_name(),
            "Sequencer Type",
            FIELD_SEQUENCER_TYPE,
            "Sequencer Type")
        sequencer_type_field.editable = True
        sequencer_type_field.required = True

        form_builder.add_field(sequencer_type_field)

        temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()
        self.response_map["status"] = STATUS_FORM_INPUT
        request: FormEntryDialogRequest = FormEntryDialogRequest("Add Sequencing Metadata", "Select a sequencer type"
                                                                                            " for this sample.",
                                                                 temp_dt,
                                                                 callback_context_data=json.dumps(self.response_map))

        return SapioWebhookResult(True, client_callback_request=request)

    def handle_client_callback_result(self, context: SapioWebhookContext,
                                      result: ClientCallbackResult) -> SapioWebhookResult:
        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_map = json.loads(result.callback_context_data)

        if self.response_map["status"] == STATUS_ERROR:
            return SapioWebhookResult(True)

        if self.response_map["status"] == STATUS_FORM_INPUT:
            sample: SampleModel = self.rec_handler.wrap_model(context.data_record, SampleModel)
            return self.handle_form_input(cast(FormEntryDialogResult, result), sample)

    def handle_form_input(self, result: FormEntryDialogResult, sample: SampleModel) -> SapioWebhookResult:
        sequencer_type: str = result.user_response_map[FIELD_SEQUENCER_TYPE]

        self.rel_man.load_parents_of_type([sample], SequencingMetadataModel)
        sm: SequencingMetadataModel = sample.get_parent_of_type(SequencingMetadataModel)

        if sm is None:
            sm: SequencingMetadataModel = self.rec_handler.add_model(SequencingMetadataModel)
            sm.add_child(sample)

        sm.set_SequencerType_field(sequencer_type)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
