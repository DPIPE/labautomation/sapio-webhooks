import re
from typing import List

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, InHouseModel, InHousePartModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-1000
Description : Action button webhook on in house to perform dilution as per requirements
"""


class PerformDilutionInHouse(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        client_callback = CallbackUtil(context)

        # Get the sample
        in_house: InHouseModel = self.inst_man.add_existing_record_of_type(context.data_record, InHouseModel)
        self.rel_man.load_parents_of_type([in_house], SampleModel)
        sample: SampleModel = in_house.get_parent_of_type(SampleModel)
        self.rel_man.load_parents_of_type([sample], InHousePartModel)

        # get the parent part
        in_house_part: InHousePartModel = sample.get_parent_of_type(InHousePartModel)
        self.rel_man.load_children_of_type([in_house_part], SampleModel)
        existing_samples: List[SampleModel] = in_house_part.get_children_of_type(SampleModel)
        self.rel_man.load_children_of_type(existing_samples, InHouseModel)

        # get a set of lot numbers that already exists to ensure no duplicates
        existing_lot_numbers = set()
        for s in existing_samples:
            existing_in_house = s.get_child_of_type(InHouseModel)
            if existing_in_house:
                existing_lot_numbers.add(existing_in_house.get_LotNumber_field())

        # Prompt user to select the dilution concentration
        option = client_callback.list_dialog("Please select the dilution being performed", ["12 ng/μl", "15 ng/μl"])\
            .pop()
        concentration = 15
        if option == "12 ng/μl":
            concentration = 12

        # create the new sample and set its fields
        new_sample: SampleModel = self.inst_man.add_new_record_of_type(SampleModel)
        new_in_house: InHouseModel = self.inst_man.add_new_record_of_type(InHouseModel)
        new_in_house.set_PartNumber_field(in_house.get_PartNumber_field())
        lot_number = in_house.get_LotNumber_field() + f"_{concentration}"
        # if lot number exits then add a unique incrementing number at end
        if lot_number in existing_lot_numbers:
            existing_lot_numbers = list(existing_lot_numbers)
            pattern = re.compile(fr"{lot_number}_"+r'\d+$')
            lot_number_count = 1
            for existing_lot_number in existing_lot_numbers:
                if pattern.match(existing_lot_number):
                    count = int(existing_lot_number.split("_")[-1])
                    if count > lot_number_count:
                        lot_number_count = count
            lot_number = lot_number + f"_{lot_number_count}"
        new_in_house.set_LotNumber_field(lot_number)
        new_sample.set_Concentration_field(concentration)
        new_in_house.set_ProbeMixKit_field(in_house.get_ProbeMixKit_field())
        new_in_house.set_Status_field("Pending")
        new_in_house.set_OrderName_field(in_house.get_OrderName_field())
        new_in_house.set_DNAPosition_field(in_house.get_DNAPosition_field())

        # perform the dilutions
        source_volume = sample.get_Volume_field()
        if not source_volume or source_volume - 2 < 0:
            source_volume = 2
        sample.set_Volume_field(source_volume-2)

        source_concentration = sample.get_Concentration_field()
        if not source_concentration:
            client_callback.ok_dialog("Error", "Source concentration not specified")
            return SapioWebhookResult(True)
        new_in_house.set_DNAConcentration_field(source_concentration)
        new_in_house.set_ConsumableType_field(in_house.get_ConsumableType_field())
        mass = source_concentration/2
        new_volume = mass/concentration
        volume_te = new_volume - 2
        if new_volume < 0:
            new_volume = 0
        if volume_te < 0:
            volume_te = 0
        new_sample.set_Volume_field(new_volume)
        new_in_house.set_VolumeDNA_field(2)
        new_in_house.set_VolumeTE_field(volume_te)
        new_sample.add_child(new_in_house)
        in_house_part.add_child(new_sample)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True, display_text="Dilution Performed")
