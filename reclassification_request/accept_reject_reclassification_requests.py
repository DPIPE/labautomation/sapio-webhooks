from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ReclassificationRequestModel, VariantResultModel
from utilities.reclassification_request_utils import ReclassificationRequestUtils
from utilities.webhook_handler import OsloWebhookHandler


class AcceptReclassificationRequests(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.data_record_list is None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)

        reclassification_requests: list[ReclassificationRequestModel] = (
            self.rec_handler.wrap_models(context.data_record_list, ReclassificationRequestModel))

        self.rel_man.load_children_of_type(reclassification_requests, VariantResultModel)

        for r in reclassification_requests:
            r.set_Status_field(ReclassificationRequestUtils.STATUS_ACCEPTED)

            variant_results: list[VariantResultModel] = r.get_children_of_type(VariantResultModel)

            for v in variant_results:
                v.set_Classification_field(r.get_NewClassification_field())

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)


class RejectReclassificationRequests(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.data_record_list is None:
            return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)

        reclassification_requests: list[ReclassificationRequestModel] = (
            self.rec_handler.wrap_models(context.data_record_list, ReclassificationRequestModel))

        for r in reclassification_requests:
            r.set_Status_field(ReclassificationRequestUtils.STATUS_REJECTED)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
