from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import DataRecordDialogRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.webhook_handler import OsloWebhookHandler

DIALOG_TITLE: str = "Reclassification Request"


class ShowReclassificationRequestFormView(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result is not None:
            return SapioWebhookResult(True)

        data_record = context.data_record
        if data_record is None:
            return SapioWebhookResult(True)

        data_type_manager: DataTypeManager = DataMgmtServer.get_data_type_manager(context.user)

        data_type_layout = data_type_manager.get_data_type_layout_list(data_type_name=context.data_type_name)[0]

        request = DataRecordDialogRequest(data_record=data_record, data_type_layout=data_type_layout,
                                          title=DIALOG_TITLE)

        return SapioWebhookResult(True, client_callback_request=request)
