from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.constants import Constants
from utilities.data_models import ConsumableItemModel
from utilities.webhook_handler import OsloWebhookHandler


class GenerateTestNIPTLibraryReagentReportFile(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Prompt the user for the project name. If one isn't entered, just auto-generate one.
        project_name: str | None = self.callback.string_input_dialog("Generate Test NIPT File",
                                                                     "Enter the project name. If this is left"
                                                                     " blank, an auto-accessioned value will be"
                                                                     " used.", "Project Name")
        if not project_name:
            testbatch: str = Constants.ACCESSIONING_TEST_PROJECT
            pojo = AccessionSystemCriteriaPojo(testbatch)
            acc_man = DataMgmtServer.get_accession_manager(self.user)
            project_name = f"{testbatch}{acc_man.accession_for_system(1, pojo)[0]}"

        # Write the headers.
        file_data: str = "batch_name\tprocess\treagent_name\tlot\texpiration_date\toperator\tinitiated"

        # Write data for each reagent.
        reagents: list[ConsumableItemModel] = (
            self.rec_handler.wrap_models(self.context.data_record_list, ConsumableItemModel))
        for reagent in reagents:
            file_data += (f"\n{project_name}\tLIBRARY:setup\t{reagent.get_ConsumableType_field()}\t"
                          f"{reagent.get_LotNumber_field()}\t"
                          f"{TimeUtil.millis_to_format(reagent.get_ExpirationDate_field(), '%Y-%m-%d')}\t"
                          f"{self.user.username}\t{TimeUtil.now_in_format('%Y-%m-%d')}")

        file_name: str = (f"{project_name}_library_reagent_report_{TimeUtil.now_in_format('%Y%m%d')}_"
                          f"{TimeUtil.now_in_format('%H%M%S')}.tab")

        # Add to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(context, file_name, file_data)
        )

        # Download the file to the user.
        self.callback.write_file(file_name, file_data)

        return SapioWebhookResult(True)
