from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import RequestModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler


class UpdateOrderName(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        order.set_RequestName_field(RequestUtils.generate_order_name(DataMgmtServer.get_accession_manager(context.user)))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
