from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopycommons.rules.on_save_rule_handler import OnSaveRuleHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ConsumableItemModel, ReagentPlateColumnModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class CreateReagentPlateColumns(OsloWebhookHandler):
    rec_handler: RecordHandler
    rule_handler: OnSaveRuleHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.rule_handler = OnSaveRuleHandler(context)

        # Get each new consumable item and load their parent samples.
        new_consumables: list[ConsumableItemModel] = self.rule_handler.get_models(ConsumableItemModel)
        self.rel_man.load_parents_of_type(new_consumables, SampleModel)

        # Create 12 Reagent Plate Column records, one for each column value, and assign each column value uniquely.
        reagent_plate_columns_list: list[str] = (DataMgmtServer.get_picklist_manager(context.user)
                                                 .get_picklist("Reagent Plate Columns").entry_list)
        for consumable in new_consumables:
            sample: SampleModel = consumable.get_parent_of_type(SampleModel)
            columns: list[ReagentPlateColumnModel] = (
                self.rec_handler.add_models(ReagentPlateColumnModel, len(reagent_plate_columns_list)))
            for x in range(len(reagent_plate_columns_list)):
                columns[x].set_Col_field(reagent_plate_columns_list[x])
            sample.add_children(columns)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
