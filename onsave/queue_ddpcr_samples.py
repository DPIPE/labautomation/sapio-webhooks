from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopycommons.rules.on_save_rule_handler import OnSaveRuleHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sqlalchemy.testing import AssertsExecutionResults

from utilities.data_models import ddPCRAssayModel, VariantResultModel, SampleModel, RequestModel, RequesterModel, \
    InvoiceModel, AssayDetailModel, ddPCRAssayDetailModel, AssignedProcessModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class QueueDDPCRSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    rule_handler: OnSaveRuleHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.rule_handler = OnSaveRuleHandler(context)

        # Get all the ddPCR Assay records that just had both their approved booleans set to True, and grab their
        # associated Variant Results.
        ddPCRAssays: list[ddPCRAssayModel] = self.rule_handler.get_models(ddPCRAssayModel)
        variants: list[str] = [ddPCRAssay.get_Variant_field() for ddPCRAssay in ddPCRAssays]
        variant_results: list[VariantResultModel] = (
            self.rec_handler.query_models(VariantResultModel, VariantResultModel.VARIANT__FIELD_NAME.field_name,
                                          variants))
        if not variant_results:
            return SapioWebhookResult(True)

        # Send each sample associated with the variant results to the ddPCR queue, and add a ddPCR assay detail
        # extension to the order.
        self.rel_man.load_parents_of_type(variant_results, SampleModel)
        samples: list[SampleModel] = []
        for variant_result in variant_results:
            samples.append(variant_result.get_parent_of_type(SampleModel))
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
        orders: list[RequestModel] = []
        for sample in top_level_samples:
            orders.append(sample.get_parent_of_type(RequestModel))
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
            RequestUtils.get_orders_to_assay_details(self.rel_man, orders))

        assay_detail_exists: bool = False
        self.rel_man.load_children_of_type(orders, ddPCRAssayDetailModel)
        for sample in samples:
            top_level_sample: SampleModel = [t for t in top_level_samples
                                             if t.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
            order: RequestModel = top_level_sample.get_parent_of_type(RequestModel)
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [sample], "ddPCR", request=order)

            # If the order has a ddPCR assay detail, then add the appropriate extension record to the order.
            if not order.get_children_of_type(ddPCRAssayDetailModel):
                assay_details: list[AssayDetailModel] = orders_to_assay_details[order]
                try:
                    assay_detail: AssayDetailModel = [x for x in assay_details if x.get_Assay_field() == "ddPCR"][0]
                    order.add_child(self.rec_handler.add_models_with_data(ddPCRAssayDetailModel,
                                                                          [{"Priority": assay_detail.get_Priority_field(),
                                                                            "AnalysisCreationDate": assay_detail.get_DateCreated_field()}])[0])
                    assay_detail_exists = True
                except Exception:
                    ...

        if assay_detail_exists:
            self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
