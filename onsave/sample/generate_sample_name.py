from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, RequestModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class GenerateSampleName(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get the sample and request.
        sample: SampleModel = self.rule_handler.get_models(SampleModel)[0]
        order: RequestModel = self.rule_handler.get_models(RequestModel)[0]

        # Get the next value for the sample name and set it.
        sample.set_OtherSampleId_field(SampleUtils.generate_next_sample_name(self.rel_man, order))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)