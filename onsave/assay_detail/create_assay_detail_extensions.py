from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopycommons.rules.on_save_rule_handler import OnSaveRuleHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, AssayDetailModel, RequestModel
from utilities.webhook_handler import OsloWebhookHandler


class CreateAssayDetailExtensions(OsloWebhookHandler):
    rec_handler: RecordHandler
    on_save_rule_handler: OnSaveRuleHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.on_save_rule_handler = OnSaveRuleHandler(context)

        # Get the order from the on save rule handler.
        order: RequestModel = self.on_save_rule_handler.get_models(RequestModel)[0]

        # Get the names and priorities of the assay details.
        assay_details: list[AssayDetailModel] = self.on_save_rule_handler.get_models(AssayDetailModel)
        if not assay_details:
            return SapioWebhookResult(True)
        assay_names_to_priority: dict[str, str] = {}
        for assay_detail in assay_details:
            assay_names_to_priority[assay_detail.get_Assay_field()] = assay_detail.get_Priority_field()

        # Add one assay detail child record to each sample in the samples list.

