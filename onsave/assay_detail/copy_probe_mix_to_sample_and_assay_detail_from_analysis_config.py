from typing import List

from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import TestCodeConfigurationModel, AssayDetailModel, RequestModel, SampleModel, \
    ProbeMixSampleModel, InvoiceModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-1002
Description : On save rule for analysis config to to update assay detail and samples with the probe mix field
"""


class CopyProbeMixToSampleAndAssayDetailFromAnalysisConfig(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # get the config
        analysis_config_records = context.data_record_list
        analysis_configs: List[TestCodeConfigurationModel] = self.inst_man.add_existing_records_of_type(
            analysis_config_records, TestCodeConfigurationModel)

        # get assay detail
        analysis_config_to_assay_details = dict()
        all_assay_details = []
        for analysis_config in analysis_configs:
            test_code = analysis_config.get_TestCodeID_field()
            if test_code:
                assay_detail_records = self.dr_man.query_data_records(AssayDetailModel.DATA_TYPE_NAME,
                                                                      AssayDetailModel.TESTCODEID__FIELD_NAME
                                                                      .field_name, [test_code]).result_list
                if assay_detail_records:
                    assay_details = self.inst_man.add_existing_records_of_type(assay_detail_records, AssayDetailModel)
                    all_assay_details.extend(assay_details)
                    analysis_config_to_assay_details[analysis_config] = assay_details

        # map assay detail to samples
        self.rel_man.load_parents_of_type(all_assay_details, InvoiceModel)
        all_invoices = []
        assay_detail_to_invoices = dict()
        for assay_detail in all_assay_details:
            invoices = assay_detail.get_parents_of_type(InvoiceModel)
            if invoices:
                all_invoices.extend(invoices)
                assay_detail_to_invoices[assay_detail] = invoices
        self.rel_man.load_parents_of_type(all_invoices, RequestModel)
        all_orders = []
        assay_detail_to_orders = dict()
        for assay_detail in assay_detail_to_invoices:
            invoices = assay_detail_to_invoices[assay_detail]
            for invoice in invoices:
                orders = invoice.get_parents_of_type(RequestModel)
                all_orders.extend(orders)
                if assay_detail not in assay_detail_to_orders:
                    assay_detail_to_orders[assay_detail] = []
                assay_detail_to_orders[assay_detail].extend(orders)
        self.rel_man.load_children_of_type(all_orders, SampleModel)
        assay_detail_to_samples = dict()
        all_samples = []
        for assay_detail in assay_detail_to_orders:
            orders = assay_detail_to_orders[assay_detail]
            for order in orders:
                samples = order.get_children_of_type(SampleModel)
                if samples:
                    if assay_detail not in assay_detail_to_samples:
                        assay_detail_to_samples[assay_detail] = []
                    assay_detail_to_samples[assay_detail].extend(samples)
                    all_samples.extend(samples)
        self.rel_man.load_children_of_type(all_samples, ProbeMixSampleModel)

        # set value on assay details and on samples
        for analysis_config in analysis_config_to_assay_details:
            assay_details = analysis_config_to_assay_details[analysis_config]
            for assay_detail in assay_details:
                assay_detail.set_ProbeMix_field(analysis_config.get_ProbeMix_field())
                if assay_detail in assay_detail_to_samples:
                    samples = assay_detail_to_samples[assay_detail]
                    for sample in samples:
                        probe_mixes = sample.get_children_of_type(ProbeMixSampleModel)
                        if not probe_mixes and analysis_config.get_ProbeMix_field():
                            probe_mix = self.inst_man.add_new_record_of_type(ProbeMixSampleModel)
                            sample.add_child(probe_mix)
                            probe_mixes.append(probe_mix)
                        for probe_mix in probe_mixes:
                            probe_mix.set_ProbeMix_field(analysis_config.get_ProbeMix_field())
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
