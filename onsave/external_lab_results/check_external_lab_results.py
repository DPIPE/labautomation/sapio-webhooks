from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ConsultationModel, ExternalLabResultsModel, PatientModel
from utilities.webhook_handler import OsloWebhookHandler


class CheckExternalLabResults(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Check to make sure this is the latest record being modified.
        consultation: ConsultationModel = self.rule_handler.get_models(ConsultationModel)[0]
        self.rel_man.load_children_of_type([consultation], ExternalLabResultsModel)
        results: list[ExternalLabResultsModel] | None = consultation.get_children_of_type(ExternalLabResultsModel)
        if not results:
            return SapioWebhookResult(True)

        # Get the latest external lab results record.
        curr_result: ExternalLabResultsModel = self.rule_handler.get_models(ExternalLabResultsModel)[0]
        max_date: int = max([x.get_DateOfMeasurement_field() for x in results])
        latest_result: ExternalLabResultsModel = [x for x in results if x.get_DateOfMeasurement_field() == max_date][0]
        if curr_result.record_id != latest_result.record_id and curr_result.get_DateOfMeasurement_field() != max_date:
            return SapioWebhookResult(True)

        # Set the patient's autoantibody candidate field to the proper value depending on the reading.
        patient: PatientModel = self.rule_handler.get_models(PatientModel)[0]
        patient.set_AutoantibodyCandidate_field(curr_result.get_C_Result_field() >= 20)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
