from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopycommons.rules.eln_rule_handler import ElnRuleHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PlateModel, SampleModel
from utilities.plating_utils import PlatingUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class UpdatePlateIDOnSave(OsloWebhookHandler):
    rec_handler: RecordHandler
    on_save_rule_handler: ElnRuleHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.on_save_rule_handler = ElnRuleHandler(context)

        # Grab the plate and samples.
        plate: PlateModel = self.on_save_rule_handler.get_models(PlateModel)[0]
        sample: SampleModel = self.on_save_rule_handler.get_models(SampleModel)[0]

        # Generate accessioned plate IDs based on the prefix and assign them to the plates.
        plate.set_PlateId_field(PlatingUtils.generate_unique_process_plate_id(
            DataMgmtServer.get_accession_manager(context.user),
            SampleUtils.get_current_process(sample, self.rel_man).get_ProcessName_field()))

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
