import json

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ddPCRAssayModel, VariantResultModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"


class QueueDDPCRSamples(OsloWebhookHandler):
    """
    THIS IS NOT BEING USED, BUT SHOULD BE KEPT AROUND FOR FUTURE USE.
    """
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)

        # Get all the ddPCR Assay records.
        ddPCRAssays: list[ddPCRAssayModel] = self.rec_handler.wrap_models(context.data_record_list, ddPCRAssayModel)
        if not ddPCRAssays:
            return SapioWebhookResult(True)

        # Get all the Variants from the Variant Result records that exist in the system.
        variants: list[str] = [ddPCRAssay.get_Variant_field() for ddPCRAssay in ddPCRAssays]
        variant_results: list[VariantResultModel] = (
            self.rec_handler.query_models(VariantResultModel, VariantResultModel.VARIANT__FIELD_NAME.field_name,
                                          variants))
        try:
            variant_result_variants: list[str] = [variant_result.get_Variant_field() for variant_result in
                                                  variant_results]
        except Exception:
            variant_result_variants: list[str] = []

        # Filter the list to only include ddPCRAssays that have been completely approved, and add any disqualifying ones
        # to a list to display to the user.
        qualifying_variants: list[str] = []
        disqualifiers: list[dict[str, str]] = []
        disqualified: bool = False
        for ddPCRAssay in ddPCRAssays:
            if not ddPCRAssay.get_ControlDesignApproved_field():
                disqualifiers.append({
                    "ddPCRAssay": f"{ddPCRAssay.get_Gene_field()}{ddPCRAssay.get_Variant_field()}",
                    "Reason": "The control for this assay has not been approved."
                })
                disqualified = True
            if not ddPCRAssay.get_AssayDesignApproved_field():
                disqualifiers.append({
                    "ddPCRAssay": f"{ddPCRAssay.get_Gene_field()}{ddPCRAssay.get_Variant_field()}",
                    "Reason": "The assay has not been approved."
                })
                disqualified = True
            if ddPCRAssay.get_Variant_field() not in variant_result_variants:
                disqualifiers.append({
                    "ddPCRAssay": f"{ddPCRAssay.get_Gene_field()}{ddPCRAssay.get_Variant_field()}",
                    "Reason": f"No variant result for the '{ddPCRAssay.get_Variant_field()}' exists in the system."
                })
                disqualified = True

            if not disqualified:
                qualifying_variants.append(ddPCRAssay.get_Variant_field())
                disqualified = False

        # Send each sample associated with the qualifying variant results to the ddPCR queue.
        self.rel_man.load_parents_of_type(variant_results, SampleModel)
        samples: list[SampleModel] = []
        for variant_result in variant_results:
            if variant_result.get_Variant_field() in qualifying_variants:
                samples.append(variant_result.get_parent_of_type(SampleModel))
        if samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, samples, "ddPCR")

        # If there were any disqualifying ddPCRAssays, show a table of them to the user.
        if disqualifiers:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.table_popup("Warning", "The following ddPCRAssays did not qualify to have their samples "
                                                    "queued for the following reasons. Other samples have been "
                                                    "queued.",
                                         [VeloxStringFieldDefinition("ddPCRAssay", "ddPCRAssay", "ddPCRAssay"),
                                          VeloxStringFieldDefinition("Reason", "Reason", "Reason")], disqualifiers,
                                         request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)
