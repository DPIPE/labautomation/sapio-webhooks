from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopycommons.rules.on_save_rule_handler import OnSaveRuleHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import VariantResultModel, SampleModel, PrimerModel, PrimerSampleModel, RequestModel, \
    ddPCRAssayModel, AssayDetailModel, ddPCRAssayDetailModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.variant_result_utils import VariantResultUtils
from utilities.webhook_handler import OsloWebhookHandler


class QueueSuggestedVerificationSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    rule_handler: OnSaveRuleHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.rule_handler = OnSaveRuleHandler(context)

        # Get the variant results from the rule and load their parent samples.
        variant_results: list[VariantResultModel] = self.rule_handler.get_models(VariantResultModel)
        self.rel_man.load_parents_of_type(variant_results, SampleModel)

        # Get all the Variant Result parent samples.
        samples: list[SampleModel] = [result.get_parent_of_type(SampleModel) for result in variant_results]

        # Get all the top level samples and load their child samples.
        top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, samples)
        self.rel_man.load_children_of_type(top_level_samples, SampleModel)
        self.rel_man.load_parents_of_type(top_level_samples, RequestModel)

        # For any variant results with a non-blank Suggested Verification field, map the archived DNA sample to the
        # suggested verification process.
        sanger_samples: list[(SampleModel, VariantResultModel)] = []
        ddpcr_samples: list[(SampleModel, VariantResultModel)] = []
        process_map: dict[str, list[SampleModel]] = {}
        for result in variant_results:
            if result.get_SuggestedVerification_field() and result.get_SuggestedVerification_field() != "":
                sample: SampleModel = result.get_parent_of_type(SampleModel)
                archived_dna: SampleModel = self.get_archived_source_dna_from_sample(sample, top_level_samples)

                process_name: str = result.get_SuggestedVerification_field()
                if process_name == "Sanger":
                    sanger_samples.append((archived_dna, result))
                    continue
                if process_name == "ddPCR":
                    ddpcr_samples.append((archived_dna, result))
                    continue

                if process_name not in list(process_map.keys()):
                    process_map[process_name] = []
                process_map[process_name].append(archived_dna)

        # Handle sending samples to verifications.
        self.send_sanger_samples(context, sanger_samples, top_level_samples)
        self.send_ddpcr_samples(context, ddpcr_samples, top_level_samples)

        # Send each of the archived DNA samples to their respective verification process.
        for process in process_map:
            for archived_dna in process_map[process]:
                ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [archived_dna], process,
                                                  request=[t.get_parent_of_type(RequestModel) for t
                                                           in top_level_samples
                                                           if t.get_SampleId_field() ==
                                                           archived_dna.get_TopLevelSampleId_field()][0])

        return SapioWebhookResult(True)

    def get_archived_source_dna_from_sample(self, sample: SampleModel,
                                            top_level_samples: list[SampleModel]) -> SampleModel:
        top_level_sample: SampleModel = [t for t in top_level_samples
                                         if t.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
        if top_level_sample.get_ExemplarSampleType_field() == "DNA":
            return top_level_sample
        else:
            # Get the DNA sample that our sample is a descendant of by computing the string of the archived DNA's
            # sample ID. This way we don't need to traverse up the hierarchy and load unnecessary records. Instead of
            # catching an out of range exception and returning None, let an error occur so that we have a webhook log to
            # reference.
            index: int = sample.get_SampleId_field().find("_")
            archived_dna_id: str = sample.get_SampleId_field()[0:index + 2]
            return [x for x in top_level_sample.get_children_of_type(SampleModel)
                    if x.get_SampleId_field() == archived_dna_id][0]

    def send_sanger_samples(self, context: SapioWebhookContext,
                            samples_to_variant_result: list[(SampleModel, VariantResultModel)],
                            top_level_samples: list[SampleModel]) -> None:
        lr_pcr_samples: list[SampleModel] = []
        pcr_amp_samples: list[SampleModel] = []
        samples: list[SampleModel] = [x[0] for x in samples_to_variant_result]
        variant_results: list[VariantResultModel] = [x[1] for x in samples_to_variant_result]
        variant_results_to_primers: dict[VariantResultModel, PrimerModel] = (
            VariantResultUtils.get_matching_primers(self.rec_handler, variant_results))
        self.rel_man.load_children_of_type(samples, PrimerSampleModel)
        for pair in samples_to_variant_result:
            # If the primer doesn't exist, then don't add the sample to any process queue.
            if not variant_results_to_primers[pair[1]]:
                continue

            # Create a primer sample extension record if the sample doesn't already have one,
            # and copy the "Variant" and "PCR Program" fields to them from the Variant Result.
            primer_sample: PrimerSampleModel = pair[0].get_child_of_type(PrimerSampleModel)
            if not primer_sample:
                primer_sample: PrimerSampleModel = self.rec_handler.add_model(PrimerSampleModel)
                pair[0].add_child(primer_sample)
            primer_sample.set_Variant_field(pair[1].get_Variant_field())
            primer_sample.set_PCRProgram_field(pair[1].get_PCRProgram_field())
            primer_sample.set_Verification_field(True)
            primer_sample.set_Amplicon_field(variant_results_to_primers[pair[1]].get_Amplicon_field())

            # If the primer exists, and it's boolean is set True, then add it to the Long Range PCR queue. If the
            # boolean is False, then queue it for the PCR Amplification step.
            if variant_results_to_primers[pair[1]].get_LongRange_field():
                lr_pcr_samples.append(pair[0])
            else:
                pcr_amp_samples.append(pair[0])

        # Store and commit changes so update the Primer Sample
        self.rec_man.store_and_commit()

        # Queue the samples for their respective processes if there are any.
        for archived_dna in lr_pcr_samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [archived_dna], "Sanger", 1,
                                              request=[t.get_parent_of_type(RequestModel) for t in top_level_samples if
                                                       t.get_SampleId_field() == archived_dna.get_TopLevelSampleId_field()][0])
        for archived_dna in pcr_amp_samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [archived_dna], "Sanger", 2,
                                              request=[t.get_parent_of_type(RequestModel) for t in top_level_samples if
                                                       t.get_SampleId_field() == archived_dna.get_TopLevelSampleId_field()][0])

    def send_ddpcr_samples(self, context: SapioWebhookContext,
                           samples_to_variant_list: list[(SampleModel, VariantResultModel)],
                           top_level_samples: list[SampleModel]) -> None:
        # Get all the ddPCR Assay records in the system.
        variants: list[str] = [x[1].get_Variant_field() for x in samples_to_variant_list]
        ddPCRAssays: list[ddPCRAssayModel] = (
            self.rec_handler.query_models(ddPCRAssayModel, ddPCRAssayModel.VARIANT__FIELD_NAME.field_name, variants))

        # If there is a ddPCR Assay record with a matching variant, and both "Approved" booleans are true, then push the
        # sample to the queue. If no variant is found, then create a new ddPCR Assay record and populate just the
        # variant field so that it can be added to the unapproved variants queue.
        queue_samples: list[SampleModel] = []
        new_ddPCRAssays: bool = False
        for x in samples_to_variant_list:
            variant_result: VariantResultModel = x[1]
            try:
                ddPCRAssay: ddPCRAssayModel = \
                    [ddPCRAssay for ddPCRAssay in ddPCRAssays
                     if ddPCRAssay.get_VariantName_field() == variant_result.get_Variant_field()][0]
                if ddPCRAssay.get_AssayDesignApproved_field() and ddPCRAssay.get_ControlDesignApproved_field():
                    queue_samples.append(x[0])
            except Exception:
                ddPCRAssay: ddPCRAssayModel = self.rec_handler.add_model(ddPCRAssayModel)
                ddPCRAssay.set_VariantName_field(variant_result.get_Variant_field())
                new_ddPCRAssays = True

        # Create any new ddPCRAssay records.
        if new_ddPCRAssays:
            self.rec_man.store_and_commit()

        # Queue all the eligible samples for the ddPCR workflow, and add a ddPCR assay detail extension to the order if
        # applicable.
        assay_detail_exists: bool = False
        if queue_samples:
            top_level_samples: list[SampleModel] = SampleUtils.get_top_level_samples(self.rec_handler, queue_samples)
            orders: list[RequestModel] = []
            self.rel_man.load_parents_of_type(top_level_samples, RequestModel)
            for sample in top_level_samples:
                orders.append(sample.get_parent_of_type(RequestModel))
            orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
                RequestUtils.get_orders_to_assay_details(self.rel_man, orders))
            for sample in queue_samples:
                top_level_sample: SampleModel = [t for t in top_level_samples
                                                 if t.get_SampleId_field() == sample.get_TopLevelSampleId_field()][0]
                order: RequestModel = top_level_sample.get_parent_of_type(RequestModel)
                ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, queue_samples, "ddPCR",
                                                  request=order)

                # If the order has a ddPCR assay detail, then add the appropriate extension record to the order.
                if not order.get_children_of_type(ddPCRAssayDetailModel):
                    assay_details: list[AssayDetailModel] = orders_to_assay_details[order]
                    try:
                        assay_detail: AssayDetailModel = [x for x in assay_details if x.get_Assay_field() == "ddPCR"][0]
                        order.add_child(self.rec_handler.add_models_with_data(ddPCRAssayDetailModel,
                                                                              [{"Priority": assay_detail.get_Priority_field(),
                                                                                "AnalysisCreationDate": assay_detail.get_DateCreated_field()}])[0])
                        assay_detail_exists = True
                    except Exception:
                        ...

            if assay_detail_exists:
                self.rec_man.store_and_commit()

