from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.webhook_handler import OsloWebhookHandler


class OverrideFamilySizeCheckHandler(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # self.rec_handler = RecordHandler(context)
        #
        # # Get the family record
        # family: FamilyModel = self.rec_handler.wrap_model(context.data_record, FamilyModel)
        #
        # family_samples: list[SampleModel] = FamilyUtils.get_family_samples(self.rec_handler, self.rel_man,
        #                                                                    family.get_FamilyId_field())
        #
        # # Get the orders to assay details dict
        # orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
        #     SampleUtils.get_orders_to_assay_details(self.rel_man, family_samples))
        #
        # for s in family_samples:
        #     if FamilyUtils.wgs_family_check(self.rec_handler, self.rel_man, s):
        #         ProcessTracking.assign_to_process(context, s.DATA_TYPE_NAME, [s.record_id], "WGS", 1, None,
        #                                           None)

        return SapioWebhookResult(True)
