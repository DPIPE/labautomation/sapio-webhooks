import json
import random
from typing import Any, cast

import requests
from requests import Response
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CompositeReportTerm, \
    CompositeTermOperation, CustomReportCriteria
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities import utils
from utilities.data_models import FamilyModel, PatientModel, PedigreeConfigurationModel, AttachmentModel, \
    PedigreeNodeModel, RequestModel, SampleModel, VariantResultModel, ClientConfigurationsModel, \
    PedigreeRelationshipModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_API_ERROR: str = "apiError"
STATUS_RECORD_SELECTION: str = "recordSelection"
STATUS_PAYLOAD_DOWNLOAD: str = "payloadDownload"
PAYLOAD: str = "payload"

RELATIONSHIP_MAP: dict[str, str] = {
    "Marriage / Relationship": "",
    "Separation / Divorce": "divorced",
    "Consanguineous Marriage / Relationship": "",
    "Dizygotic Twins": "dztwin",
    "Monozygotic Twins": "mztwin"
}


class GeneratePedigreeImage(OsloWebhookHandler):
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_PAYLOAD_DOWNLOAD:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_RECORD_SELECTION:
                family: FamilyModel = self.rec_handler.wrap_model(context.data_record, FamilyModel)
                return self.process_pedigree_config_selection(context, cast(DataRecordSelectionResult, result), family)
            if self.response_map[STATUS] == STATUS_API_ERROR:
                self.response_map[STATUS] = STATUS_PAYLOAD_DOWNLOAD
                file_bytes: bytes = self.response_map[PAYLOAD].encode()
                return FileUtil.write_file("payload.json", file_bytes, request_context=json.dumps(self.response_map))

        # Prompt the user to select Pedigree Configuration records that have either the same family ID as this family,
        # or have a null Family ID.
        family: FamilyModel = self.rec_handler.wrap_model(context.data_record, FamilyModel)
        return self.prompt_pedigree_configs(context, family.record_id)

    def prompt_pedigree_configs(self, context: SapioWebhookContext, family_record_id: int) -> SapioWebhookResult:
        # Run a custom report to get all qualifying Pedigree Configuration records in the system and prompt them to the
        # user.
        columns: list[ReportColumn] = [ReportColumn("PedigreeConfiguration", "RecordId", FieldType.INTEGER)]
        terms: list[RawReportTerm] = [RawReportTerm("PedigreeConfiguration", "FamilyID",
                                                    RawTermOperation.EQUAL_TO_OPERATOR,
                                                    str(family_record_id)),
                                      RawReportTerm("PedigreeConfiguration", "FamilyID",
                                                    RawTermOperation.EQUAL_TO_OPERATOR, "")]
        root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeTermOperation.OR_OPERATOR, terms[1])
        criteria: CustomReportCriteria = CustomReportCriteria(columns, root_term)
        results: list[list[Any]] = (DataMgmtServer.get_custom_report_manager(context.user).run_custom_report(criteria)
                                    .result_table)
        if not results:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "No Pedigree Configuration records have blank family IDs and no Family"
                                                 " IDs that match the selected family.",
                                      request_context=json.dumps(self.response_map))

        record_ids: list[int] = [x[0] for x in results]
        pedigree_configs: list[DataRecord] = ((DataMgmtServer.get_data_record_manager(context.user)
                                               .query_data_records_by_id("PedigreeConfiguration", record_ids))
                                              .result_list)
        self.response_map[STATUS] = STATUS_RECORD_SELECTION
        return PopupUtil.record_selection_popup(context, "Select a Pedigree Configuration",
                                                ["ConfigurationName", "FamilyID", "Labels", "IncludedVariants",
                                                 "IncludedFamilyMembers", "RecordId"],
                                                pedigree_configs, False, request_context=json.dumps(self.response_map))

    def process_pedigree_config_selection(self, context: SapioWebhookContext,
                                          result: DataRecordSelectionResult, family: FamilyModel) -> SapioWebhookResult:
        # Get the selected Pedigree Configuration record.
        selections: list[dict[str, Any]] = result.selected_field_map_list
        if not selections:
            return SapioWebhookResult(True)
        pedigree_config: PedigreeConfigurationModel = (
            self.rec_handler.query_models_by_id(PedigreeConfigurationModel, [int(selections[0]["RecordId"])]))[0]

        # Check to make sure the selected config has linked family members.
        if not pedigree_config.get_IncludedFamilyMembers_field():
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "The selected Pedigree Configuration has no family members.",
                                      request_context=json.dumps(self.response_map))

        # Grab the Pedigree API's URL from the client config record. If the URL is blank, then throw an error to the
        # user saying so.
        client_config: ClientConfigurationsModel = self.rec_handler.query_all_models(ClientConfigurationsModel)[0]
        url: str | None = client_config.get_PedigreeWebServiceUrl_field()
        if not url or url == "":
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "No Pedigree Web Service URL specified in the Client Config, so a"
                                               " Pedigree image cannot be generated.",
                                      request_context=json.dumps(self.response_map))
        port: int | None = client_config.get_PedigreeWebServicePort_field()

        # Create the payload.
        payload: dict[str, Any] = self.create_payload(pedigree_config)

        # Send a POST request to the Pedigree server and grab the returned .svg image. Then add the image as an
        # attachment to the family record. If the API call fails because the host can't be reached, then throw an error
        # to the user saying so. If the host is reached and the call still fails, throw an error to the user with the
        # reason and download the payload to the user.
        try:
            response: Response = requests.post(f"{url}/export", json=payload)
        except Exception:
            try:
                response: Response = requests.post(f"{url}:{port}/export")
            except Exception:
                self.response_map[STATUS] = STATUS_ERROR
                return PopupUtil.ok_popup("Error", f"Error calling Pedigree server endpoint. URL is invalid or Pedigree"
                                                   f" server is not running. Pedigree image cannot be generated.",
                                          request_context=json.dumps(self.response_map))
        if response.status_code != 200:
            self.response_map[STATUS] = STATUS_API_ERROR
            self.response_map[PAYLOAD] = json.dumps(payload)
            return PopupUtil.ok_popup("Error", f"Error calling Pedigree server endpoint. Please see the generated"
                                               f" payload file for any possible errors and the reason below."
                                               f"\n{response.status_code} {response.reason}",
                                      request_context=json.dumps(self.response_map))
        attachment_record: AttachmentModel = (
            AttachmentUtil.create_attachment(context, f"{family.get_FamilyId_field()} PID.svg", response.text.encode(),
                                             AttachmentModel))
        family.add_child(attachment_record)

        self.rec_man.store_and_commit()

        # Direct the user to the new attachment record.
        return SapioWebhookResult(True, directive=FormDirective(attachment_record.get_data_record()))

    def create_payload(self, pedigree_config: PedigreeConfigurationModel) -> dict[str, Any]:
        # Initialize our payload object.
        payload: dict[str, Any] = {"pedigree": [], "diseases": [], "labels": []}

        # Get all the pedigree nodes from the pedigree config and update the patients to pedigree nodes map. Also, load
        # the parents of each node so that we can grab them later as all the node's relationships.
        pedigree_nodes: list[PedigreeNodeModel] = (
            self.rec_handler.query_models_by_id(PedigreeNodeModel,
                                                [int(x) for x in pedigree_config.get_IncludedFamilyMembers_field()
                                                .split(",")]))
        self.rel_man.load_parents_of_type(pedigree_nodes, PedigreeNodeModel)
        self.rel_man.load_children_of_type(pedigree_nodes, PedigreeRelationshipModel)
        patient_record_ids: list[int] = [x.get_Patient_field() for x in pedigree_nodes if x.get_Patient_field()]
        patients: list[PatientModel] = self.rec_handler.query_models_by_id(PatientModel, patient_record_ids)
        pedigree_nodes_to_patient: dict[PedigreeNodeModel, PatientModel] = {}
        for pedigree_node in pedigree_nodes:
            try:
                pedigree_nodes_to_patient[pedigree_node] = \
                    [x for x in patients if pedigree_node.get_Patient_field() and
                     pedigree_node.get_Patient_field() == x.record_id][0]
            except Exception:
                ...

        # Get all the variants linked to each patient.
        patients_to_variants: dict[PatientModel, list[str]] | None = self.get_patients_to_variants(patients)

        # Add data to the payload for each patient.
        included_variants: list[str] = pedigree_config.get_IncludedVariants_field().split(",")
        labels: list[str] = pedigree_config.get_Labels_field().split(",")
        payload_labels: set[str] = set()
        twin_map: dict[int, str] = {}
        patient_datas: list[dict[str, Any]] = []
        curr_twin_index: int = 1
        pedigree_node_record_ids: list[int] = [x.record_id for x in pedigree_nodes]

        for pedigree_node in pedigree_nodes:
            patient_data: dict[str, Any] = {}

            # Add any data according to the specified labels.
            patient_data["display_name"] = ""
            if "first name" in labels:
                patient_data["first_name"] = pedigree_node.get_FirstName_field()
                if pedigree_node.get_FirstName_field() and pedigree_node.get_FirstName_field() != "":
                    patient_data["display_name"] += pedigree_node.get_FirstName_field()
                payload_labels.add("first_name")
            if "last name" in labels:
                patient_data["last_name"] = pedigree_node.get_LastName_field()
                if pedigree_node.get_LastName_field() and pedigree_node.get_LastName_field() != "":
                    patient_data["display_name"] += f" {pedigree_node.get_LastName_field()[0:1]}."
                payload_labels.add("last_name")
            if "age" in labels:
                patient_data["age"] = str(pedigree_node.get_Age_field())
                payload_labels.add("age")
            if "tests" in labels:
                try:
                    patient_data["tests"] = "\n".join(pedigree_nodes_to_patient[pedigree_node]
                                                      .get_CompletedTests_field().split(","))
                    payload_labels.add("tests")
                except Exception:
                    ...
            if "variants" in labels:
                try:
                    variants: list[str] | None = patients_to_variants[pedigree_nodes_to_patient[pedigree_node]]
                    if variants:
                        for variant in variants:
                            if variant in included_variants:
                                patient_data[variant] = "true"
                    payload_labels.add("variants")
                except Exception:
                    ...
            if "first name" in labels and "last name" in labels:
                patient_data["display_name"] = \
                    f"{pedigree_node.get_FirstName_field()} {pedigree_node.get_LastName_field()[0:1]}."

            # Set required fields.
            patient_data["name"] = str(pedigree_node.record_id)
            patient_data["sex"] = pedigree_node.get_Sex_field()[0:1]
            payload_labels.add("sex")
            patient_data["status"] = "1" if pedigree_node.get_IsDeceased_field() else "0"
            payload_labels.add("status")

            # Add any parent/child relationships that exist in Sapio.
            pedigree_node_parents: list[PedigreeNodeModel] = pedigree_node.get_parents_of_type(PedigreeNodeModel)
            motherless: bool = False
            fatherless: bool = False
            if pedigree_node_parents:
                try:
                    patient_data["mother"] = [str(x.record_id) for x in pedigree_node_parents
                                              if x.get_Sex_field() == "Female"
                                              and x.record_id in pedigree_node_record_ids][0]
                except Exception:
                    motherless = True
                try:
                    patient_data["father"] = [str(x.record_id) for x in pedigree_node_parents
                                              if x.get_Sex_field() == "Male"
                                              and x.record_id in pedigree_node_record_ids][0]
                except Exception:
                    fatherless = True
            else:
                patient_data["top_level"] = "true"

            # If there's a missing mother or father node, create them and set their parents as the parents of the
            # present parent node (step-parents).
            if motherless and not fatherless:
                dummy_data: dict[str, Any] = (
                    self.create_dummy_pedigree_node(
                        self.get_patient_data_by_name(patient_data["father"], patient_datas),
                        labels, pedigree_node_record_ids, "F"))
                patient_data["mother"] = dummy_data["name"]
                patient_datas.append(dummy_data)
            if fatherless and not motherless:
                dummy_data: dict[str, Any] = (
                    self.create_dummy_pedigree_node(
                        self.get_patient_data_by_name(patient_data["mother"], patient_datas),
                        labels, pedigree_node_record_ids, "M"))
                patient_data["father"] = dummy_data["name"]
                patient_datas.append(dummy_data)

            # Check the relationships between nodes and add the appropriate fields.
            relationships: list[PedigreeRelationshipModel] | None = (
                pedigree_node.get_children_of_type(PedigreeRelationshipModel))
            if relationships:
                for relationship in relationships:
                    relationship_type: str = RELATIONSHIP_MAP[relationship.get_RelationshipType_field()]
                    if relationship_type == "dztwin" or relationship_type == "mztwin":
                        if relationship.record_id not in twin_map.keys():
                            twin_map[relationship.record_id] = str(curr_twin_index)
                            curr_twin_index += 1
                        patient_data[relationship_type] = twin_map[relationship.record_id]

                    elif relationship_type == "divorced":
                        divorcee_record_id: int | None = (
                            self.get_divorcee(pedigree_nodes, relationship, pedigree_node.record_id))
                        if divorcee_record_id:
                            patient_data[relationship_type] = str(divorcee_record_id)

            patient_datas.append(patient_data)

        payload["pedigree"] = patient_datas
        payload["labels"] = list(payload_labels)

        # Add all the included variants to the JSON payload and generate unique colors for each.
        existing_colors: list[int] = []
        for included_variant in included_variants:
            color_int: int = random.randint(0, 16777216)
            while color_int in existing_colors:
                color_int = random.randint(0, 16777216)
            existing_colors.append(color_int)

            color: str = f"#{utils.add_leading_zeroes(hex(color_int).replace('0x', ''), 6)}"
            payload["diseases"].append({"type": included_variant, "colour": color})

        return payload

    def get_patients_to_variants(self, patients: list[PatientModel]) -> dict[PatientModel, list[str]] | None:
        # Get all the variant results from the Request -> Parent Sample (Non-DNA) -> Aliquot (DNA)
        # OR Request -> Parent Sample (DNA)
        if not patients:
            return None

        patients_to_variants: dict[PatientModel, list[str]] = {}
        self.rel_man.load_path_of_type(patients, RelationshipPath().child_type(RequestModel).child_type(SampleModel)
                                       .child_type(SampleModel).child_type(VariantResultModel))
        self.rel_man.load_path_of_type(patients, RelationshipPath().child_type(RequestModel).child_type(SampleModel)
                                       .child_type(VariantResultModel))
        for patient in patients:
            request_records: list[RequestModel] = patient.get_children_of_type(RequestModel)
            try:
                parent_samples: list[SampleModel] = []
                for request in request_records:
                    parent_samples.extend(request.get_children_of_type(SampleModel))

                variant_results: list[VariantResultModel] = []
                for parent_sample in parent_samples:
                    variant_results.extend(parent_sample.get_children_of_type(VariantResultModel))
                    aliquots: list[SampleModel] = parent_sample.get_children_of_type(SampleModel)
                    for aliquot in aliquots:
                        variant_results.extend(aliquot.get_children_of_type(VariantResultModel))

                patients_to_variants[patient] = [x.get_Variant_field() for x in variant_results if x]
            except Exception:
                continue
        return patients_to_variants

    def create_dummy_pedigree_node(self, partner_patient_data: dict[str, Any], labels: list[str], record_ids: list[int],
                                   sex: str) -> dict[str, Any]:
        dummy_data: dict[str, Any] = {}
        if "first name" in labels:
            dummy_data["first_name"] = "Dummy"
        if "last name" in labels:
            dummy_data["last_name"] = "Node"
        if "age" in labels:
            dummy_data["age"] = "?"

        # Set required fields.
        dummy_data["sex"] = sex

        record_id: int = random.randint(0, max(record_ids))
        while record_id in record_ids:
            record_id = random.randint(0, max(record_ids))
        dummy_data["name"] = str(record_id)
        record_ids.append(record_id)

        # Set the parents to the node's step-parents (their partner's parents). If there's no step-parents, then this is
        # a top-level node.
        try:
            dummy_data["mother"] = partner_patient_data["mother"]
            dummy_data["father"] = partner_patient_data["father"]
            dummy_data["noparents"] = "true"
        except Exception:
            dummy_data["top_level"] = "true"

        return dummy_data

    def get_divorcee(self, pedigree_nodes: list[PedigreeNodeModel], divorce_relationship: PedigreeRelationshipModel,
                     curr_pedigree_node_record_id: int) -> int | None:
        for pedigree_node in pedigree_nodes:
            for relationship in pedigree_node.get_children_of_type(PedigreeRelationshipModel):
                if (relationship.record_id == divorce_relationship.record_id
                        and pedigree_node.record_id != curr_pedigree_node_record_id):
                    return pedigree_node.record_id
        return None

    def get_patient_data_by_name(self, name: str, patient_datas: list[dict[str, Any]]) -> dict[str, Any] | None:
        for i, d in enumerate(patient_datas):
            if d["name"] == name:
                return patient_datas[i]
        return None
