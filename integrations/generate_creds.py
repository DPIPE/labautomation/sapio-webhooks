import base64
import time
from sapiopycommons.callbacks.callback_util import CallbackUtil

import jwt
import requests
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.serialization import NoEncryption
from cryptography.hazmat.primitives.serialization import PrivateFormat
from cryptography.hazmat.primitives.serialization.pkcs12 import load_key_and_certificates
from cryptography.x509 import load_pem_x509_certificate
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.webhook_handler import OsloWebhookHandler


# Code provided by SP in May - Any modifications MUST be confirmed with 
# Ash first!

# I think the generated JWT only works in the OUS test env? - AI

class GenerateCreds(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        cb_handler = CallbackUtil(context)

        # Swap token URLs (pingfed) when this starts working on SD3-SAPIAPOU-61
        token_url = "https://sts.sykehuspartner.no/as/token.oauth2"
        # token_url = "https://sds-pingou-61.ad.ous-hf.no/as/token.oauth2"
        client_id = "I_SAPIOOU"
        
        with open('integrations/I_SAPIOOU_tst_clcert.sikt.no.pfx', 'rb') as pfx_file:
            pfx_data = pfx_file.read()
        
        password = b'X2;K[aUuz[Y[S9'
        
        # Load the key, certificate, and CA certificates
        private_key, certificate, ca_certificates = load_key_and_certificates(
            pfx_data,
            password=password,
            backend=default_backend()
        )
        
        # Display the private key in PEM format (encrypted or unencrypted)
        private_key_pem = private_key.private_bytes(
            encoding=Encoding.PEM,
            format=PrivateFormat.PKCS8,
            encryption_algorithm=NoEncryption()  # You can use a password-based encryption if desired
        )
        
        # Create a JWT to use as a client assertion
        current_time = int(time.time())
        
        jwt_payload = {
            "iss": client_id,  # Issuer, the client ID
            "sub": client_id,  # Subject, the client ID
            "aud": token_url,  # Audience, the token endpoint
            "exp": current_time + 60,  # Token expiration (60 seconds from now)
            "jti": "unique-jwt-id",  # A unique identifier for this JWT
        }
        
        # Sign the JWT with the RSA private key
        client_assertion = jwt.JWT().encode(jwt_payload, jwt.jwk_from_pem(private_key_pem), alg="RS256")
        
        # Define the data for the token request
        data = {
            "grant_type": "client_credentials",
            "client_assertion_type": "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
            "client_assertion": client_assertion,
        }
        
        # Send the POST request to get the token
        response = requests.post(token_url, data=data)
        
        # Check if the request was successful
        if response.status_code == 200:
            token_info = response.json()
            access_token = token_info.get("access_token")
            print(f"Authorization: bearer {access_token}")
            cb_handler.ok_dialog("", f"Authorization: bearer {access_token}")
        else:
            cb_handler.ok_dialog("", f"Failed to get access token.\n{response.status_code} : {response.text}")

        return SapioWebhookResult(True)
