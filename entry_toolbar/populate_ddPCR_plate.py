from collections import defaultdict
from typing import List

from natsort import natsorted, ns
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil

from utilities.data_models import PlateModel, PlateDesignerWellElementModel, SampleModel, VariantResultModel, \
    ddPCRAssayModel, ConsumableItemModel
from utilities.plating_utils import PlatingUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class PopulateDDPCRPlate(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        experiment_handler = ExperimentHandler(context)
        rec_handler = RecordHandler(context)
        result: ClientCallbackResult = context.client_callback_result

        if result:
            if isinstance(result, OptionDialogResult):
                if result.button_text is None or result.button_text == 'OK':
                    return SapioWebhookResult(True)
            if result.user_cancelled:
                return SapioWebhookResult(True)

        try:
            # Get Samples
            samples_step = experiment_handler.get_step("Samples")
            sample_records = experiment_handler.get_step_records(samples_step)
            if not sample_records:
                raise Exception("Sample records not found in the entry named 'Samples'")
            samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

            # Get reagents present in reagent tracking entry
            reagents: list[ConsumableItemModel] = get_reagents(experiment_handler, rec_handler)
            if not reagents:
                raise Exception("Reagents selected in Reagent Tracking step are not found in the system")

            # Get controls
            filler, pos_control, wt_control = get_controls_to_plate(reagents)

            # Get existing plates from 3d plating step
            _3d_plate_step = experiment_handler.get_step("Plate Setup")
            plates_in_entry: List[PlateModel] = PlateDesignerEntry(_3d_plate_step, experiment_handler).get_plates(
                PlateModel)
            plate = plates_in_entry[0]
            if not plate:
                raise Exception("Plate not found in the 'Plate Setup' step")

            plate.set_PlateId_field(PlatingUtils.generate_unique_process_plate_id(
                DataMgmtServer.get_accession_manager(context.user),
                SampleUtils.get_current_process(samples[0], self.rel_man).get_ProcessName_field()))

            # Group samples by fluorophore and variant
            fluorophore_to_variant_samples = group_samples_by_fluorophore(self, rec_handler, samples)

            # Create plate well designer elements for the plate
            self.auto_plate_samples_and_controls(filler, fluorophore_to_variant_samples, plate, pos_control, wt_control)
            self.rec_man.store_and_commit()

        except Exception as exception:
            self.logger.error(f"Error while trying to populate ddPCR plate: {exception}")
            return PopupUtil.display_ok_popup("Error", str(exception))
        return SapioWebhookResult(True)

    def auto_plate_samples_and_controls(self, filler, fluorophore_to_variant_samples, plate, pos_control, wt_control):

        rows = 'ABCDEFGH'
        current_column = 1
        row_index = 0
        next_index_to_use = 0
        item_to_plate = "WTCONTROL"

        def move_to_next_column():
            nonlocal current_column, row_index
            current_column += 3
            row_index = 0

        def fill_remaining_column(row_idx):
            while row_idx < len(rows):
                self.create_well_element("ConsumableItem", plate,
                                         rows[row_idx],
                                         current_column,
                                         filler.get_field_value("RecordId"))
                row_idx += 1
            move_to_next_column()

        for fluorophore, variant_samples_dict in fluorophore_to_variant_samples.items():
            if not variant_samples_dict:
                continue
            if current_column > 10:
                raise Exception("Samples can't fit in single plate")

            if row_index != 0 and current_column <= 10:
                # Add Fillers if the column is not complete
                fill_remaining_column(row_index)
                next_index_to_use = 0

            row_index = next_index_to_use
            for variant, samples in variant_samples_dict.items():
                # WT control
                if item_to_plate == "WTCONTROL":
                    if row_index >= len(rows):
                        move_to_next_column()
                        if current_column > 10:
                            raise Exception("Samples can't fit in a single plate")
                    self.create_well_element("ConsumableItem", plate,
                                             rows[row_index],
                                             current_column,
                                             wt_control.get_field_value("RecordId"))
                    row_index += 1
                    item_to_plate = "SAMPLE"

                # Samples
                if item_to_plate == "SAMPLE":
                    for sample in samples:
                        if row_index >= len(rows):
                            move_to_next_column()
                            if current_column > 10:
                                raise Exception("Samples can't fit in a single plate")
                        self.create_well_element("Sample", plate,
                                                 rows[row_index],
                                                 current_column,
                                                 sample.get_field_value("RecordId"))
                        row_index += 1
                    item_to_plate = "SPIKEINCONTROL"

                # Spike-in control
                if item_to_plate == "SPIKEINCONTROL":
                    if row_index >= len(rows):
                        move_to_next_column()
                        if current_column > 10:
                            raise Exception("Samples can't fit in a single plate")
                    self.create_well_element("ConsumableItem", plate,
                                             rows[row_index],
                                             current_column,
                                             pos_control.get_field_value("RecordId"))
                    row_index += 1
                    item_to_plate = "BLANK"

                # Blank
                if item_to_plate == "BLANK":
                    if row_index >= len(rows):
                        move_to_next_column()
                        if current_column > 10:
                            raise Exception("Samples can't fit in a single plate")
                    self.create_well_element("BLANK", plate,
                                             rows[row_index],
                                             current_column,
                                             "")
                    row_index += 1
                    item_to_plate = "WTCONTROL"

                next_index_to_use = row_index
                if next_index_to_use >= len(rows):
                    move_to_next_column()
                    if current_column > 10:
                        raise Exception("Samples can't fit in a single plate")
                    next_index_to_use = 0

        while row_index <= 7 and current_column <= 10:
            fill_remaining_column(row_index)

    def create_well_element(self, pair_type, plate, row_index, current_column, record_id):
        self.set_well_values(pair_type, plate, row_index, current_column, record_id)
        self.set_well_values(pair_type, plate, row_index, current_column + 1, record_id)

    def set_well_values(self, source_type, plate, row, column, record_id):
        well = self.inst_man.add_new_record_of_type(PlateDesignerWellElementModel)
        well.set_field_value("PlateRecordId", plate.get_field_value("RecordId"))
        well.set_field_value("Layer", 1)
        well.set_field_value("RowPosition", row)
        well.set_field_value("ColPosition", column)
        if source_type == "BLANK":
            well.set_field_value("IsControl", True)
            well.set_field_value("ControlType", "BLANK")
            well.set_field_value("SourceDataTypeName", "Sample")
        else:
            well.set_field_value("IsControl", False)
            well.set_field_value("ControlType", None)
            well.set_field_value("SourceDataTypeName", source_type)
            well.set_field_value("SourceRecordId", record_id)


def get_controls_to_plate(reagents):
    wt_controls = [reagent for reagent in reagents if
                   reagent.get_ConsumableType_field() == "Human skin genomic DNA"]
    if not wt_controls:
        raise Exception("'Human skin genomic DNA' reagent is required to auto-plate the samples")
    wt_control = wt_controls[0]

    pos_controls = [reagent for reagent in reagents if
                    (reagent.get_ConsumableType_field() != "Human skin genomic DNA"
                     and reagent.get_ConsumableType_field() != "dPCR Supermix for Probes")]
    if not pos_controls:
        raise Exception("Positive control reagent is required to auto-plate the samples")
    pos_control: ConsumableItemModel = pos_controls[0]

    fillers = [reagent for reagent in reagents if
               reagent.get_ConsumableType_field() == "dPCR Supermix for Probes"]
    if not fillers:
        raise Exception("'dPCR Supermix for Probes' reagent is required to auto-plate the samples")
    filler: ConsumableItemModel = fillers[0]
    return filler, pos_control, wt_control


def get_reagents(experiment_handler, rec_handler):
    reagent_step = experiment_handler.get_step("Reagent Tracking")
    reagent_tracking_records = experiment_handler.get_step_records(reagent_step)
    if not reagent_tracking_records:
        raise Exception("Reagents not found in the entry named 'Reagent Tracking'")
    reagent_part_numbers: List[str] = list()
    for reagent in reagent_tracking_records:
        part_number = reagent.get_field_value("PartNumber")
        if part_number:
            reagent_part_numbers.append(part_number)
    reagents: list[ConsumableItemModel] = (
        rec_handler.query_models(ConsumableItemModel, ConsumableItemModel.PARTNUMBER__FIELD_NAME.field_name,
                                 reagent_part_numbers))
    return reagents


def group_samples_by_fluorophore(self, rec_handler, samples):
    self.rel_man.load_children_of_type(samples, VariantResultModel)
    variant_models: list[VariantResultModel] = []
    variant_to_samples: dict[str, list[SampleModel]] = {}
    variant_to_fluorophore: dict[str, str] = {}

    # Group samples by variants and get fluorophore value of each variant
    for sample in samples:
        variants = sample.get_children_of_type(VariantResultModel)
        if variants and variants[0].get_Variant_field():
            variant_values = RecordModelUtil.get_value_list(self.inst_man.unwrap_list(variants),
                                                            VariantResultModel.VARIANT__FIELD_NAME.field_name)
            ddPcrAssays = rec_handler.query_models(ddPCRAssayModel,
                                                   ddPCRAssayModel.VARIANT__FIELD_NAME.field_name,
                                                   variant_values)
            if not ddPcrAssays:
                raise Exception("No ddPCR Assays found for the sample " + sample.get_SampleId_field())

            if variants[0].get_Variant_field() in variant_to_samples:
                variant_to_samples[variants[0].get_Variant_field()].append(sample)
            else:
                variant_to_samples[variants[0].get_Variant_field()] = [sample]
            variant_models.extend(variants)
            variant_to_fluorophore[variants[0].get_Variant_field()] = ddPcrAssays[0].get_Fluorophore_field()
    if not variant_models:
        raise Exception("No variants found under the samples")

    fluorophore_to_variant_samples = defaultdict(lambda: defaultdict(list))
    # Sort variant keys in natural order
    sorted_variant_keys = natsorted(variant_to_samples.keys())
    for variant in sorted_variant_keys:
        samples = variant_to_samples[variant]
        fluorophore = variant_to_fluorophore.get(variant)
        if fluorophore:
            fluorophore_to_variant_samples[fluorophore][variant].extend(samples)
    # Convert defaultdict to regular dict
    fluorophore_to_variant_samples = {k: dict(v) for k, v in fluorophore_to_variant_samples.items()}
    return fluorophore_to_variant_samples


def get_sorted_records_by_field(records, field_name):
    if not records:
        return list()
    sorted_records = natsorted(records, key=lambda rec: (
        rec.get_field_value(field_name)), alg=ns.IGNORECASE)
    return sorted_records
