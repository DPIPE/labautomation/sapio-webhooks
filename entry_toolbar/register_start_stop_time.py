import time

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.pojo.DataRecord import DataRecord

from utilities.webhook_handler import OsloWebhookHandler


class RegisterStartStopTime(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        data_record = context.data_record
        if not data_record:
            raise Exception("Record not found in the entry to register start/stop time")

        result_message = update_time_fields(data_record)

        DataMgmtServer.get_data_record_manager(self.context.user).commit_data_records([data_record])
        return SapioWebhookResult(True, display_text=result_message)


def update_time_fields(data_record: DataRecord) -> str:
    start_time_value = data_record.get_field_value('StartTime')
    stop_time_value = data_record.get_field_value('StopTime')

    if not start_time_value:
        data_record.set_field_value('StartTime', round(time.time() * 1000))
        return 'Start time has been set to the current time.'
    elif not stop_time_value:
        data_record.set_field_value('StopTime', round(time.time() * 1000))
        return 'Stop time has been set to the current time.'
    else:
        return 'Start & Stop Time has already been recorded. Please update manually if required'
