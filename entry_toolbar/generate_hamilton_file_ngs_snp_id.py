import io
import re

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.files.file_bridge import FileBridge
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import WriteFileRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from xlwt import Workbook
from xlwt import Worksheet

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import SampleModel
from utilities.utils import get_records_from_entry_with_option
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-712
Description : ELN Entry Tool bar button and on submit rule to generate hamilton robot file
"""


class GenerateHamiltonFileNgsSnpId(OsloWebhookHandler):
    # entry tags
    ALIQUOT_SAMPLES_ENTRY_TAG = "HAMILTON ALIQUOT SAMPLES"

    # filebridge path for robot file
    ROBOT_FILES_FILE_BRIDGE_PATH = "RobotFiles/"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # get samples
        sample_records = get_records_from_entry_with_option(context, self.ALIQUOT_SAMPLES_ENTRY_TAG)
        sample_models = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

        # get file data
        file_information = [["Sample_Number", "Labware", "Position_ID", "Volume_DNA", "Destination_Well_ID"]]
        self.rel_man.load_parents_of_type(sample_models, SampleModel)
        verso_pattern = re.compile(r"^H[A-Z]\w+")
        for sample_model in sample_models:
            parent_sample = sample_model.get_parent_of_type(SampleModel)
            sample_number = parent_sample.get_TubeBarcode_field()
            labware = "Rack2DTubes" if verso_pattern.match(parent_sample.get_TubeBarcode_field()) else "Rack1"
            position_id = ""
            if parent_sample.get_RowPosition_field() and parent_sample.get_ColPosition_field():
                position_id = parent_sample.get_RowPosition_field()+parent_sample.get_ColPosition_field()
            vol = sample_model.get_Volume_field()
            dest_well = ""
            if sample_model.get_RowPosition_field() and sample_model.get_ColPosition_field():
                dest_well = sample_model.get_RowPosition_field()+sample_model.get_ColPosition_field()
            file_information.append([sample_number, labware, position_id, vol, dest_well])

        # generate file
        workbook: Workbook = Workbook()
        worksheet: Worksheet = workbook.add_sheet("Sheet1")
        r = 0
        for line in file_information:
            c = 0
            for val in line:
                worksheet.write(r, c, val)
                c += 1
            r += 1
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)

        file_name = f"SNP-ID_Hamilton_{context.eln_experiment.notebook_experiment_id}.xls"

        # Add file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                file_name,
                buffer.getvalue(),
                "Generated Hamilton File",
                ExperimentHandler(context),
                self.inst_man
            )
        )

        # write file to user
        try:
            FileBridge.write_file(
                context,
                "oslo-filebridge",
                self.ROBOT_FILES_FILE_BRIDGE_PATH + file_name,
                buffer.getvalue()
            )
            return SapioWebhookResult(True)

        except:
            pass

        return SapioWebhookResult(
            True,
            client_callback_request=WriteFileRequest(
                    file_bytes=buffer.getvalue(),
                    file_path=file_name
                )
            )
