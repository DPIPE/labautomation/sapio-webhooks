import io
import os
import xml.etree.ElementTree as ET
import zipfile
from datetime import datetime
from typing import List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.files.file_util import FileUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import PlateModel
from utilities.data_models import SampleModel
from utilities.instrument_utils import InstrumentUtils
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class GenerateQuantStudioInputFiles(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler
    instrument_utils: InstrumentUtils

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        self.instrument_utils = InstrumentUtils(context)

        # Get existing plates from 3d plating step
        hamilton_plate_step = self.exp_handler.get_step("Hamilton 96-well Plate")
        plates_in_entry: List[PlateModel] = PlateDesignerEntry(hamilton_plate_step, self.exp_handler).get_plates(
            PlateModel)

        # As of now we expect only one hamilton plate
        if len(plates_in_entry) > 1:
            return SapioWebhookResult(True, display_text="Error: More than one Hamilton plate found.")

        # Determine if the PCR is performed in separate machine
        pce_machine_step = self.exp_handler.get_step("PCR machine details")
        pcr_machine_details = self.exp_handler.get_step_records(pce_machine_step)
        is_separate_pcr_machine: bool = pcr_machine_details[0].get_field_value("PerformPCRonseparatePCRMachine")

        # generate input files
        set_of_edt_files = self.generate_edt_files(is_separate_pcr_machine, plates_in_entry,
                                                   context.eln_experiment.notebook_experiment_id)




        # Send the files to the instrument OR download to browser
        return self.send_file_to_instrument(context, set_of_edt_files)

    def generate_edt_files(self, use_post_run_read: bool, plates_in_entry: List[PlateModel], exp_id):
        # Define the base directory for the templates
        base_dir = os.path.join('template_files')

        # Choose the specific subfolder based on the boolean flag
        subfolder = 'PCRandPostRunRead_template' if use_post_run_read else 'PostRunRead_template'

        # Determine read type for the input file name
        read_type = 'PCRandPostRunRead' if use_post_run_read else 'PostRunRead'

        # Full path to the XML files
        xml_folder = os.path.join(base_dir, subfolder, 'apldbio', 'sds')

        set_of_edt_files: dict[str, bytes] = {}
        plate = plates_in_entry[0]

        # Get samples to use
        self.rel_man.load_children_of_type([plate], SampleModel)
        samples = plate.get_children_of_type(SampleModel)

        # Segregate hamilton plate samples into QuantStudio assay plates based on well position
        assay_plates_dict = generate_assay_plates_dcit(samples)

        for assay_plate_number, set_of_samples in assay_plates_dict.items():
            if set_of_samples:
                # Create a BytesIO object to hold the archive file in memory
                edt_bytes_io = io.BytesIO()

                # Create an archive file
                with zipfile.ZipFile(edt_bytes_io, 'w', zipfile.ZIP_DEFLATED) as zip_file:
                    # Add static XML files to the ZIP file
                    for static_file in static_files:
                        static_file_path = os.path.join(xml_folder, static_file)
                        if os.path.exists(static_file_path):
                            with open(static_file_path, 'r', encoding='utf-8') as file:
                                zip_file.writestr(f"apldbio/sds/{static_file}", file.read())

                    # Modify and add other XML files as needed
                    for filename in os.listdir(xml_folder):
                        if filename.endswith(".xml") and filename not in static_files:
                            file_path = os.path.join(xml_folder, filename)
                            with open(file_path, 'r', encoding='utf-8') as file:
                                if filename == "experiment.xml":
                                    experiment_xml = self.update_experiment_xml(assay_plate_number, set_of_samples,
                                                                                exp_id, file_path)
                                    zip_file.writestr(f"apldbio/sds/{filename}", experiment_xml)
                                elif filename == "plate_setup.xml":
                                    plate_setup_xml  = self.update_plate_setup_xml(assay_plate_number, set_of_samples,
                                                                                 file_path)
                                    zip_file.writestr(f"apldbio/sds/{filename}", plate_setup_xml)
                                else:
                                    zip_file.writestr(f"apldbio/sds/{filename}", file.read())
                    # Get the bytes of the archive file
                    edt_bytes = edt_bytes_io.getvalue()
                    set_of_edt_files[
                        f"SNP-ID_QuantStudio_{read_type}_{datetime.now().strftime('%d%m%y')}_Plate{assay_plate_number}.edt"] = edt_bytes

        return set_of_edt_files

    def update_experiment_xml(self, assay_plate_number: str, samples: List[SampleModel], experiment_id,
                              experiment_xml_path):
        # Read the experiment.xml template
        tree = ET.parse(experiment_xml_path)
        root = tree.getroot()

        # Update the experiment name
        experiment_name = f"SNP-ID_{datetime.now().strftime('%d%m%y')}_Plate{assay_plate_number}_{experiment_id}"
        root.find('Name').text = experiment_name

        # Create well position to sample name map
        well_to_sample_name, sample_name_to_well_pos = get_well_position_map()

        # Get all sample elements and create dict of sample name to element
        sample_elements = root.findall('Samples')
        sample_elements_dict = get_sample_elements_dict(sample_elements)

        # Map samples to well positions
        well_to_sample = {sample.get_RowPosition_field() + sample.get_ColPosition_field(): sample for sample in samples}

        # Update elements for samples found
        for well_position, (sample_name, set_number) in well_to_sample_name.items():
            sample_element = sample_elements_dict[sample_name]
            if well_position in well_to_sample:
                sample = well_to_sample[well_position]
                if sample_element and sample:
                    name_element = sample_element.find('Name')
                    if name_element is not None:
                        # Set Sample ID
                        name_element.text = sample.get_SampleId_field()

        # Convert the updated XML tree to a string
        updated_xml_str = ET.tostring(root, encoding='unicode')
        return updated_xml_str

    def update_plate_setup_xml(self, assay_plate_number: str, samples, plate_setup_xml_path):
        # Read the plate_setup.xml template
        tree = ET.parse(plate_setup_xml_path)
        root = tree.getroot()

        # Map samples to well positions
        well_to_sample = {sample.get_RowPosition_field() + sample.get_ColPosition_field(): sample for sample in samples}

        # Create well position to sample name map
        well_to_sample_name, sample_name_to_well_pos = get_well_position_map()

        # Update elements for samples found
        feature_maps = root.findall('.//FeatureMap')
        for feature_map in feature_maps:
            feature_items = feature_map.findall('.//FeatureValue/FeatureItem')
            for idx, feature_item in enumerate(feature_items):
                sample_element = feature_item.find('Sample')
                if sample_element is not None:
                    name_element = sample_element.find('Name')
                    name: str = name_element.text
                    well_position = sample_name_to_well_pos.get((assay_plate_number, name), None)
                    if well_position in well_to_sample:
                        sample: SampleModel = well_to_sample.get(well_position, None)
                        if sample:
                            # Set Sample ID
                            name_element.text = sample.get_SampleId_field()

        # Convert the updated XML tree to a string
        updated_xml_str = ET.tostring(root, encoding='unicode')
        return updated_xml_str

    def send_file_to_instrument(self, context: SapioWebhookContext, file_datas: dict[str, bytes]) -> SapioWebhookResult:
        instrument_errors: dict[str, str] = {}
        failed_file_bridge_files: dict[str, bytes] = {}
        exp_id: int = context.eln_experiment.notebook_experiment_id

        # Get the instrument name.
        instrument_tracking_entry: ElnEntryStep = WorkflowUtils.get_instrument_tracking_entry(self.exp_handler)
        try:
            instrument_name: str = self.eln_man.get_data_records_for_entry(
                exp_id, instrument_tracking_entry.eln_entry.entry_id).result_list[0].get_field_value("InstrumentUsed")
        except Exception:
            # If there is no instrument selected, leave it blank.
            instrument_name: str = ""

        if len(file_datas.keys()) == 0:
            return SapioWebhookResult(True)

        for key in file_datas.keys():
            try:
                if instrument_name:
                    FileBridge.write_file(context, instrument_name, key, file_datas[key])
                else:
                    raise Exception("Instrument name not found to transfer QuantStudio Input file")
            except Exception as e:
                failed_file_bridge_files[key] = file_datas[key]
                instrument_errors[key] = repr(e)

        # Add files to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                list(file_datas.keys())[0],
                list(file_datas.values())[0],
                "Generated QuantStudio File",
                None,
                self.inst_man
            )
        )

        if len(failed_file_bridge_files) > 0:
            return self.download_to_browser(instrument_name, failed_file_bridge_files, instrument_errors)
        return SapioWebhookResult(True, "Files successfully sent to FileBridge location.")

    def download_to_browser(self, instrument_name: str, file_datas: dict[str, bytes],
                            instrument_errors: dict[str, str]) -> SapioWebhookResult:
        # Write an error to the instrument error log
        for key in instrument_errors.keys():
            self.instrument_utils.log_instrument_error(instrument_name, key, instrument_errors[key])

        # Download the files to the user
        return FileUtil.write_files(file_datas)


def get_sample_elements_dict(sample_elements):
    sample_dict = {}
    for sample_element in sample_elements:
        sample_name = sample_element.find('Name').text
        sample_dict[sample_name] = sample_element
    return sample_dict


# Define static XML files
static_files = [
    "Manifest.mf",
    "notification_setting.xml",
    "export_setting.xml",
    "tools.xml",
    "tcprotocol.xml",
    "analysis_protocol.xml"
]

# Define the well position ranges for each set
set1_positions = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2",
                  "A3", "B3", "C3", "D3", "E3", "F3", "G3", "H3"]
set2_positions = ["A4", "B4", "C4", "D4", "E4", "F4", "G4", "H4", "A5", "B5", "C5", "D5", "E5", "F5", "G5", "H5",
                  "A6", "B6", "C6", "D6", "E6", "F6", "G6", "H6"]
set3_positions = ["A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7", "A8", "B8", "C8", "D8", "E8", "F8", "G8", "H8",
                  "A9", "B9", "C9", "D9", "E9", "F9", "G9", "H9"]
set4_positions = ["A10", "B10", "C10", "D10", "E10", "F10", "G10", "H10", "A11", "B11", "C11", "D11", "E11", "F11",
                  "G11", "H11", "A12", "B12", "C12", "D12", "E12", "F12", "G12", "H12"]


def get_well_position_map():
    rows = "ABCDEFGH"
    columns = range(1, 13)
    well_positions = [f"{row}{col}" for col in columns for row in rows]
    well_to_sample_name = {}
    sample_name_to_well_pos = {}

    for idx, well_position in enumerate(well_positions):
        sample_name = f"SAMPLE{(idx % 24) + 1:02d}"
        if well_position in set1_positions:
            assay_plate_number = "1"
        elif well_position in set2_positions:
            assay_plate_number = "2"
        elif well_position in set3_positions:
            assay_plate_number = "3"
        elif well_position in set4_positions:
            assay_plate_number = "4"
        else:
            assay_plate_number = None

        well_to_sample_name[well_position] = (sample_name, assay_plate_number)
        sample_name_to_well_pos[(assay_plate_number, sample_name)] = well_position

    return well_to_sample_name, sample_name_to_well_pos


def generate_assay_plates_dcit(samples):
    # Initialize dictionaries to hold samples for each set (assay plate)
    set1_samples = []
    set2_samples = []
    set3_samples = []
    set4_samples = []

    # Create dictionaries to hold samples for each set (assay plate)
    assay_plates_dict = {
        "1": set1_samples,
        "2": set2_samples,
        "3": set3_samples,
        "4": set4_samples
    }

    # Distribute samples into sets based on well positions
    for sample in samples:
        if sample.get_RowPosition_field() and sample.get_ColPosition_field():
            well_position = sample.get_RowPosition_field() + sample.get_ColPosition_field()
            if well_position in set1_positions:
                assay_plates_dict["1"].append(sample)
            elif well_position in set2_positions:
                assay_plates_dict["2"].append(sample)
            elif well_position in set3_positions:
                assay_plates_dict["3"].append(sample)
            elif well_position in set4_positions:
                assay_plates_dict["4"].append(sample)
    return assay_plates_dict
