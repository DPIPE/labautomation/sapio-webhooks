import traceback
from io import BytesIO
from typing import List, cast

import numpy as np
import pandas as pd
from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sklearn.linear_model import LinearRegression

from utilities.constants import Constants
from utilities.data_models import SampleModel, QCDatumModel
from utilities.webhook_handler import OsloWebhookHandler


class QuantitQcOutputFileParser(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get file from the user

        file_name, file_bytes =  CallbackUtil(context).request_file("Upload File", ["xlsx"])
        if not file_name or not file_bytes:
            return SapioWebhookResult(True)

        try:
            # Get stds and samples data from file
            standards_instrument_values, sample_dict = parse_excel_data(file_bytes)
            slope = calculate_slope(standards_instrument_values)

            # Get the samples from experiment
            try:
                samples_step = self.exp_handler.get_step_by_option(Constants.TAG_QUANT_IT_SAMPLES_ENTRY)
            except Exception:
                raise Exception(f"No entry with tag \"{Constants.TAG_QUANT_IT_SAMPLES_ENTRY}\" was found in the "
                                f"experiment.")
            samples: list[SampleModel] = self.exp_handler.get_step_models(samples_step, SampleModel)
            if not samples:
                raise Exception("Samples not found in the designated Quant-iT samples step")

            # [OSLO-1185]: Set the concentration threshold failure depending on the workflow.
            template_name: str = self.exp_handler.get_template_name()
            if template_name == "16S":
                concentration_threshold: float = 6.0
            else:
                concentration_threshold: float = 25.0

            # Calculate conc for the samples and create qc records
            qc_data_record_models: List[QCDatumModel] = list()
            for sample in samples:
                if sample.get_RowPosition_field() and sample.get_ColPosition_field():
                    sample_fluors_value = sample_dict[
                        str(sample.get_RowPosition_field()) + str(sample.get_ColPosition_field())]
                    if sample_fluors_value:
                        conc: float = sample_fluors_value / slope / 2
                        sample.set_Concentration_field(conc)
                        qc_datum: QCDatumModel = self.rec_handler.add_model(QCDatumModel)
                        sample.add_child(qc_datum)
                        qc_datum.set_SampleId_field(sample.get_SampleId_field())
                        qc_datum.set_OtherSampleId_field(sample.get_OtherSampleId_field())
                        qc_datum.set_RawDataValue_field(sample_fluors_value)
                        qc_datum.set_Concentration_field(conc)
                        if conc < concentration_threshold:
                            qc_datum.set_QCStatus_field("Failed - Continue")
                            qc_datum.set_SampleFinalQCStatus_field("Failed - Continue")
                        else:
                            qc_datum.set_QCStatus_field("Passed")
                            qc_datum.set_SampleFinalQCStatus_field("Passed")
                        qc_data_record_models.append(qc_datum)

            self.rec_man.store_and_commit()

            # Add the qc records to the entry
            self.exp_handler.add_step_records(cast(ElnEntryStep, context.active_step), qc_data_record_models)
            return SapioWebhookResult(True, display_text="Successfully Loaded Quant-iT QC data")

        except Exception as exception:
            self.logger.error(
                "Error ({user:s}):\n{trc:s}".format(user=context.user.username, trc=traceback.format_exc()))
            self.callback.ok_dialog("ERROR", str(exception))


def calculate_slope(standards_instrument_values):
    standards = [0, 50, 100, 200, 400, 600, 800, 1000]
    # subtract std 1 from standards well
    subtracted_stds = list()
    std1 = standards_instrument_values[0]
    index = 0
    for standard_value in standards_instrument_values:
        subtracted_stds.append(standard_value - std1)
        index += 1

    X = np.array(standards)
    Y = np.array(subtracted_stds)

    # Create a LinearRegression object with fit_intercept set to False
    model = LinearRegression(fit_intercept=False)

    # Fit the model
    model.fit(X[:, np.newaxis], Y)

    # Get the slope
    slope = model.coef_[0]
    if slope <= 0:
        raise Exception("Calculated slope is not greater than zero. Please check the standards data.")
    return slope


def parse_excel_data(file_bytes) -> (list[str], dict):
    try:
        # Load the Excel file from bytes
        xls = pd.ExcelFile(BytesIO(file_bytes))
    except Exception as e:
        raise ValueError(f"Failed to load Excel file: {e}")

    try:
        # Load the specific sheet into a DataFrame
        df = pd.read_excel(xls, sheet_name='excap238')
    except ValueError:
        raise ValueError("Sheet 'excap238' not found in the Excel file.")
    except Exception as e:
        raise ValueError(f"Failed to read the sheet: {e}")

    # Check if the DataFrame is empty
    if df.empty:
        raise ValueError("The sheet 'excap238' is empty.")

    # Function to detect the start of the 96-well plate layout with periods
    def find_layout_starts(dataframe):
        layout_starts = []
        for i in range(len(dataframe)):
            row = dataframe.iloc[i, 2:14]
            # Replace commas only if the row contains string values
            if row.apply(lambda x: isinstance(x, str)).any():
                row = row.str.replace(',', '')
            # Check if the row contains numeric values with periods
            if pd.to_numeric(row, errors='coerce').notnull().all():
                layout_starts.append(i)
        return layout_starts

    # Locate the starts of the 96-well plate layouts with periods
    layout_starts = find_layout_starts(df)
    if not layout_starts:
        raise ValueError("96-well plate data NOT found in the provided file.")

    if len(layout_starts) == 1:
        layout_start = layout_starts[0] + 1
    else:
        # Use the second layout
        layout_start = layout_starts[1] + 1

    try:
        # Extract the data section for standards and samples
        data_section = df.iloc[layout_start:layout_start + 8, 2:14]
        data_section = data_section.apply(lambda x: pd.to_numeric(x.astype(str).str.replace(',', ''), errors='coerce'))
    except Exception as e:
        raise ValueError(f"Error processing data section: {e}")

    # Separate the standards and samples
    try:
        standards = data_section.iloc[:, 0].tolist()
        samples = data_section.iloc[:, 1:].values
    except IndexError:
        raise ValueError("Data section does not have enough columns for standards and samples.")

    # Check for missing values in standards or samples
    if any(pd.isnull(standards)):
        raise ValueError("Some standard values are missing.")
    if any(pd.isnull(samples.flatten())):
        raise ValueError("Some sample values are missing.")

    # Reorder samples row by row, column by column
    reordered_samples = []
    num_rows, num_cols = samples.shape
    for col in range(num_cols):
        for row in range(num_rows):
            reordered_samples.append(samples[row, col])

    # Generate well positions in the specified order
    well_positions = []
    columns = range(1, 13)
    rows = 'ABCDEFGH'
    for col in columns:
        for row in rows:
            well_positions.append(f"{row}{col}")

    # Create a dictionary of ordered sample values
    sample_dict = dict(zip(well_positions[:len(reordered_samples)], reordered_samples))
    return standards, sample_dict
