import traceback

from natsort import natsorted
from natsort import ns
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopycommons.files.file_bridge import FileBridge
from sapiopycommons.files.file_util import FileUtil
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel
from sapiopylib.rest.WebhookService import SapioWebhookContext
from sapiopylib.rest.WebhookService import SapioWebhookResult

from entry_toolbar.populate_ddPCR_plate import get_controls_to_plate
from entry_toolbar.populate_ddPCR_plate import get_reagents
from entry_toolbar.populate_ddPCR_plate import group_samples_by_fluorophore
from utilities.attach_files_util import attach_file_to_entry
from utilities.attach_files_util import FileAttachmentParams
from utilities.data_models import ConsumableItemModel
from utilities.data_models import PlateDesignerWellElementModel
from utilities.data_models import PlateModel
from utilities.data_models import ProjectModel
from utilities.data_models import SampleModel
from utilities.instrument_utils import InstrumentUtils
from utilities.utils import get_current_date_in_format
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class GenerateQuantasoftInputFiles(OsloWebhookHandler):
    eln_man: ElnManager
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        self.eln_man = DataMgmtServer.get_eln_manager(context.user)
        self.exp_handler = ExperimentHandler(context)
        result: ClientCallbackResult = context.client_callback_result
        rec_handler = RecordHandler(context)

        if result:
            if isinstance(result, OptionDialogResult):
                if result.button_text is None or result.button_text == 'OK':
                    return SapioWebhookResult(True)
            if result.user_cancelled:
                return SapioWebhookResult(True)

        try:
            # Get existing plates from 3d plating step
            plating_step = self.exp_handler.get_step("Plate Setup")

            # Throw error if plating step is not submitted yet
            if plating_step.is_available():
                raise Exception("Please submit 'Plate Setup' step to generate quantsoft input files.")

            # Get plate
            plates_in_entry: list[PlateModel] = PlateDesignerEntry(plating_step, self.exp_handler).get_plates(
                PlateModel)

            # We expect only one plate here
            if len(plates_in_entry) > 1:
                raise Exception("More than one plate found in 'Plate Setup' step.")

            # Get Samples
            samples_step = self.exp_handler.get_step("Samples")
            sample_records = self.exp_handler.get_step_records(samples_step)
            if not sample_records:
                raise Exception("Sample records not found in the entry named 'Samples'")
            samples: list[SampleModel] = self.inst_man.add_existing_records_of_type(sample_records, SampleModel)

            # Get reagents present in reagent tracking entry
            reagents: list[ConsumableItemModel] = get_reagents(self.exp_handler, rec_handler)
            if not reagents:
                raise Exception("Reagents selected in Reagent Tracking step are not found in the system")

            # Get controls
            filler, pos_control, wt_control = get_controls_to_plate(reagents)

            # Get plate well designer elements
            plate_well_elements = self.get_plate_well_elements(plates_in_entry, rec_handler)

            # Generate files
            file_dict: dict[str, bytes] = self.generate_files(context, filler, samples,
                                                              plate_well_elements, pos_control,
                                                              wt_control, rec_handler)

            # Send the files to the instrument OR download to browser
            return self.send_file_to_instrument(context, file_dict)

        except Exception as exception:
            self.logger.error(traceback.format_exc())
            return PopupUtil.display_ok_popup("Error", str(exception))

    def generate_files(self, context, filler, samples, plate_well_elements, pos_control,
                       wt_control, rec_handler):

        current_date = get_current_date_in_format("%d%m%Y")
        file_dict: dict[str, bytes] = {}
        last_well_index = 0
        last_sample_record_id = 0

        # Group samples by fluorophore and variant values
        fluorophore_to_variant_samples = group_samples_by_fluorophore(self, rec_handler, samples)

        for fluorophore, variant_samples_dict in fluorophore_to_variant_samples.items():

            # header
            csv_content = ("Well,Sample,Ch1 Target,Ch1 Target Type,Ch2 Target,Ch2 Target Type,Experiment,Expt Type,"
                           "Expt FG Color,Expt BG Color,ReferenceCopyNumber,TargetCopyNumber,ReferenceAssayNumber,"
                           "TargetAssayNumber,ReactionVolume,DilutionFactor,Supermix,Cartridge,Expt Comments\n")

            # Get the last sample plated in this group
            flat_sample_list: list[SampleModel] = flatten_data_objects(variant_samples_dict)
            last_sample_in_the_group: SampleModel = flat_sample_list[-1]

            # Get project ID
            project_id = self.get_project_id(last_sample_in_the_group)

            last_sample_in_group_found = False
            _break_loop_when_wt_control_found = False

            for well_index in range(last_well_index, len(plate_well_elements)):

                plate_well_element = plate_well_elements[well_index]
                last_well_index = well_index
                sample_column_value = ""
                if (plate_well_element.get_SourceDataTypeName_field() == "ConsumableItem"
                        and (plate_well_element.get_SourceRecordId_field() == wt_control.get_field_value(
                            "RecordId"))):
                    if last_sample_in_group_found and ((int(plate_well_element.get_ColPosition_field()) - 1) % 3 == 0):
                        break  # switch to next fluorophore
                    else:
                        sample_column_value = "WT"
                elif (plate_well_element.get_SourceDataTypeName_field() == "ConsumableItem"
                      and (plate_well_element.get_SourceRecordId_field() == pos_control.get_field_value(
                            "RecordId"))):
                    sample_column_value = "WT+1%"
                elif (plate_well_element.get_SourceDataTypeName_field() == "ConsumableItem"
                      and (plate_well_element.get_SourceRecordId_field() == filler.get_field_value("RecordId"))):
                    sample_column_value = "SuperMix + dH2O"
                elif (plate_well_element.get_SourceDataTypeName_field() == "Sample" and
                      (plate_well_element.get_SourceRecordId_field() is None)):
                    sample_column_value = "BLANK"
                elif (plate_well_element.get_SourceDataTypeName_field() == "Sample" and
                      (plate_well_element.get_SourceRecordId_field() is not None)):
                    for sample_record in flat_sample_list:
                        if sample_record.get_field_value(
                                "RecordId") == plate_well_element.get_SourceRecordId_field():
                            sample_column_value = sample_record.get_OtherSampleId_field() if sample_record.get_OtherSampleId_field() else sample_record.get_SampleId_field()
                            last_sample_record_id = sample_record.get_field_value("RecordId")
                            break

                # Add row to csv content
                well = f"{plate_well_element.get_RowPosition_field()}{int(plate_well_element.get_ColPosition_field()):02}" if (
                        plate_well_element.get_RowPosition_field() and plate_well_element.get_ColPosition_field() is not None) else ""
                row_data = well + "," + sample_column_value + ","
                csv_content += row_data + get_static_data()

                if str(last_sample_record_id) == str(last_sample_in_the_group.get_field_value("RecordId")):
                    last_sample_in_group_found = True

            file_name = f"ddPCR_{project_id}_{current_date}_{context.user.username}_{fluorophore}.csv"
            file_data = csv_content.encode("utf-8")
            file_dict[file_name] = file_data

        return file_dict

    def get_plate_well_elements(self, plates_in_entry, rec_handler):
        plate_well_elements = rec_handler.query_models(PlateDesignerWellElementModel,
                                                       PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                                       [plates_in_entry[0].get_field_value("RecordId")])
        plate_well_elements: list[PlateDesignerWellElementModel] = get_sorted_records_by_field(plate_well_elements,
                                                                                               PlateDesignerWellElementModel.ROWPOSITION__FIELD_NAME.field_name)
        plate_well_elements: list[PlateDesignerWellElementModel] = get_sorted_records_by_field(plate_well_elements,
                                                                                               PlateDesignerWellElementModel.COLPOSITION__FIELD_NAME.field_name)
        return plate_well_elements

    def get_project_id(self, last_sample_in_the_group):
        self.an_man.load_ancestors_of_type([last_sample_in_the_group.backing_model],
                                           ProjectModel.DATA_TYPE_NAME.__str__())
        ancestor_projects: set[PyRecordModel] = self.an_man.get_ancestors_of_type(
            last_sample_in_the_group.backing_model,
            ProjectModel.DATA_TYPE_NAME.__str__())
        if ancestor_projects:
            project_id = next(iter(ancestor_projects)).get_field_value("ProjectId")
        else:
            project_id = ""
        return project_id

    def send_file_to_instrument(self, context: SapioWebhookContext, file_datas: dict[str, bytes]) -> SapioWebhookResult:
        instrument_errors: dict[str, str] = {}
        failed_file_bridge_files: dict[str, bytes] = {}
        exp_id: int = context.eln_experiment.notebook_experiment_id

        # Get the instrument name.
        instrument_tracking_entry: ElnEntryStep = WorkflowUtils.get_instrument_tracking_entry(self.exp_handler)
        try:
            instrument_name: str = self.eln_man.get_data_records_for_entry(
                exp_id, instrument_tracking_entry.eln_entry.entry_id).result_list[0].get_field_value("InstrumentUsed")
        except Exception:
            # If there is no instrument selected, leave it blank.
            instrument_name: str = ""

        if len(file_datas.keys()) == 0:
            return SapioWebhookResult(True)

        for key in file_datas.keys():
            try:
                if instrument_name:
                    FileBridge.write_file(context, instrument_name, key, file_datas[key])
                else:
                    raise Exception("Instrument name not found to transfer QuantStudio Input file")
            except Exception as e:
                failed_file_bridge_files[key] = file_datas[key]
                instrument_errors[key] = repr(e)

        # Add file to attachment entry
        attach_file_to_entry(
            FileAttachmentParams(
                context,
                list(file_datas.keys())[0],
                list(file_datas.values())[0],
                "Generated Description File",
                None,
                self.inst_man
            )
        )

        if len(failed_file_bridge_files) > 0:
            return self.download_to_browser(context, instrument_name, failed_file_bridge_files, instrument_errors)

        return SapioWebhookResult(True, "Files successfully sent to FileBridge location.")

    def download_to_browser(self, context, instrument_name: str, file_datas: dict[str, bytes],
                            instrument_errors: dict[str, str]) -> SapioWebhookResult:
        # Write an error to the instrument error log
        for key in instrument_errors.keys():
            InstrumentUtils(context).log_instrument_error(instrument_name, key, instrument_errors[key])

        # Download the files to the user
        return FileUtil.write_files(file_datas)


def get_sorted_records_by_field(records, field_name):
    if not records:
        return list()
    sorted_records = natsorted(records, key=lambda rec: (
        rec.get_field_value(field_name)), alg=ns.IGNORECASE)
    return sorted_records


def get_static_data():
    ch1_target = ""
    ch1_target_type = "Unknown"
    ch2_target = ""
    ch2_target_type = "Unknown"
    experiment = "RED"
    expt_type = "Rare Event Detection"
    expt_fg_color = "255_255_255_255"
    expt_bg_color = "255_128_0_0"
    ref_copy_number = "2"
    target_copy_number = "1"
    ref_assay_number = "1"
    target_assay_number = "1"
    reaction_volume = "20"
    dilution_factor = "1"
    supermix = "ddPCR Supermix for Probes (no dUTP)"
    cartridge = "186-4008"
    expt_comments = "Experiment comment"

    return f"{ch1_target},{ch1_target_type},{ch2_target},{ch2_target_type},{experiment},{expt_type}," \
           f"{expt_fg_color},{expt_bg_color},{ref_copy_number},{target_copy_number},{ref_assay_number}," \
           f"{target_assay_number},{reaction_volume},{dilution_factor},{supermix},{cartridge},{expt_comments}\n"


def flatten_data_objects(dict_data):
    flat_list = []
    for key, list_ in dict_data.items():
        flat_list.extend(list_)
    return flat_list
