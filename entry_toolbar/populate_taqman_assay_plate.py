from typing import List

from natsort import natsorted, ns
from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.eln.plate_designer import PlateDesignerEntry
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.WebhookService import SapioWebhookContext, SapioWebhookResult

from utilities.data_models import PlateModel, PlateDesignerWellElementModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class PopulateTaqmanAssayPlate(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        experiment_handler = ExperimentHandler(context)
        acc_manager = DataMgmtServer.get_accession_manager(context.user)

        # Get Hamilton Samples
        hamilton_samples_step = experiment_handler.get_step("Hamilton Samples")
        samples = experiment_handler.get_step_records(hamilton_samples_step)
        if not samples:
            raise Exception("Sample records not found in the entry named 'Hamilton Samples'")
        samples = get_sorted_records_by_field(samples, SampleModel.COLPOSITION__FIELD_NAME.field_name)
        samples = get_sorted_records_by_field(samples, SampleModel.ROWPOSITION__FIELD_NAME.field_name)

        # Determine no of plates required
        samples_per_plate = 24
        no_of_plates_to_required: int = calculate_plates(len(samples), samples_per_plate)

        # Get existing plates from 3d plating step
        _3d_plate_step = experiment_handler.get_step("TaqMan Assay 384-well Plate")
        plates_in_entry: List[PlateModel] = PlateDesignerEntry(_3d_plate_step, experiment_handler).get_plates(PlateModel)

        new_plates: List[PlateModel] = list()
        if no_of_plates_to_required > len(plates_in_entry):
            no_of_new_plates_to_create: int = no_of_plates_to_required - len(plates_in_entry)
            new_plates = self.inst_man.add_new_records_of_type(no_of_new_plates_to_create, PlateModel)
            plates_in_entry = plates_in_entry + new_plates

        self.set_plate_id_on_plates(acc_manager, plates_in_entry)
        self.rec_man.store_and_commit()

        if new_plates:
            # Add new plates to the 3d plating step
            PlateDesignerEntry(_3d_plate_step, experiment_handler).add_plates(new_plates)

        # populate samples on the plates
        self.auto_plate_samples(no_of_plates_to_required, plates_in_entry, samples, samples_per_plate)

        return SapioWebhookResult(True)

    def set_plate_id_on_plates(self, acc_manager, plates_in_entry):
        # Generate plate ids
        pojo = AccessionSystemCriteriaPojo("TaqManAssayPlate")
        pojo.prefix = "TaqMan-"
        pojo.initial_sequence_value = 1000
        plate_ids: list[str] = acc_manager.accession_for_system(len(plates_in_entry), pojo)
        id_index = 0
        for plate in plates_in_entry:
            plate.set_PlateRows_field(16)
            plate.set_PlateColumns_field(24)
            plate.set_PlateId_field(plate_ids[id_index])
            id_index += 1

    def auto_plate_samples(self, no_of_plates_to_required, plates_in_entry, samples, samples_per_plate):
        rows = 'ABCDEFGHIJKLMNOP'
        for plate_index in range(no_of_plates_to_required):
            sample_index_on_plate = 0
            plate: PlateModel = plates_in_entry[plate_index]

            # Get samples to assign to this plate
            start_index = plate_index * samples_per_plate
            end_index = min(start_index + samples_per_plate, len(samples))
            samples_for_current_plate = samples[start_index:end_index]

            for sample in samples_for_current_plate:
                # Create plate designer well elements for each sample replicate
                sample_plate_designer_wells = self.inst_man.add_new_records_of_type(16, PlateDesignerWellElementModel)
                if sample_index_on_plate < 16:
                    # Determine the row and column for samples 1-16
                    if sample_index_on_plate < 8:
                        row_idx = sample_index_on_plate * 2
                    else:
                        row_idx = (sample_index_on_plate - 7) * 2 - 1

                    for well_index in range(16):
                        well = sample_plate_designer_wells[well_index]
                        row = rows[row_idx]
                        col = well_index + 1
                        # set values on plate well designer element
                        set_well_values(col, plate, row, sample, well)
                else:
                    # Determine the row and column for samples 17-24
                    row_idx = ((sample_index_on_plate - 16) % 8) * 2  # Alternates every 8 samples starting from A
                    col_idx_base = 17
                    for well_index in range(16):
                        well = sample_plate_designer_wells[well_index]
                        row = rows[row_idx + (well_index // 8)]
                        col = col_idx_base + (well_index % 8)
                        # set values on plate well designer element
                        set_well_values(col, plate, row, sample, well)
                sample_index_on_plate += 1
        self.rec_man.store_and_commit()


def set_well_values(col, plate, row, sample, well):
    well.set_field_value("SourceRecordId", sample.get_record_id())
    well.set_field_value("PlateRecordId", plate.get_field_value("RecordId"))
    well.set_field_value("Layer", 1)
    well.set_field_value("RowPosition", row)
    well.set_field_value("ColPosition", col)
    well.set_field_value("SourceDataTypeName", "Sample")
    well.set_field_value("IsControl", False)
    well.set_field_value("ControlType", None)


def calculate_plates(number_of_samples, samples_per_plate):
    plates_needed = (number_of_samples + samples_per_plate - 1) // samples_per_plate
    return plates_needed


def get_sorted_records_by_field(records, field_name):
    if not records:
        return list()
    sorted_records = natsorted(records, key=lambda rec: (
        rec.get_field_value(field_name)), alg=ns.IGNORECASE)
    return sorted_records
