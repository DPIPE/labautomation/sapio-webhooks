import re

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.eln.SapioELNEnums import ExperimentEntryStatus
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.Protocols import ElnEntryStep

from utilities.constants import Constants
from utilities.data_models import PlateModel, SampleModel, PlateDesignerWellElementModel
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-711
Description : ELN Entry Toolbar button to plate samples with following specifications:
Add a 3D Plater entry to this tab. We should automatically position the samples using an entry toolbar button, 
similar to how we do it in WGS and WES. The sorting should leave all Verso samples in their current positions, 
and fill fridge samples in after (column-wise). The fridge samples can be ordered arbitrarily as long as they are all 
after the Verso samples, so let’s sort by Sample ID for consistency.

Sample volume to use should be 2 uL. HOWEVER - if the source concentration is > 180 ng/uL, sample volume to use should 
be 1 uL instead. There is no good way to handle this with 3D Plater settings. At best we can set the default to 2, then 
use a rule to check concentration and change the aliquot volume to 1 (plus refund a point of volume to the source). 
If rules can’t do this, a webhook will be required.
"""


class AddSamplesToPlateNgsSnpId(OsloWebhookHandler):
    rec_handler: RecordHandler
    exp_handler: ExperimentHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.exp_handler = ExperimentHandler(context)

        result: ClientCallbackResult = context.client_callback_result
        if result:
            return SapioWebhookResult(True)

        # If the entry has already been submitted, tell the user that they can't plate the samples again
        status: ExperimentEntryStatus = self.exp_handler.get_step(context.active_step.get_name()).eln_entry.entry_status
        if status == ExperimentEntryStatus.Completed or status == ExperimentEntryStatus.CompletedApproved:
            return PopupUtil.ok_popup("Warning", "The entry has already been completed.")

        # Get the plate record
        plate: PlateModel = self.get_plate()

        # Get the samples
        samples: list[SampleModel] = self.exp_handler.get_step_models("Generate Pick Files for Samples", SampleModel)

        # Group each sample onto the plate
        return self.group_samples(plate, samples)

    def get_plate(self) -> PlateModel:
        plate_entry: ElnEntryStep = self.exp_handler.get_step("Hamilton 96-well Plate")
        record_id: int = int(plate_entry.get_options()["MultiLayerPlating_Plate_RecordIdList"])
        return self.rec_handler.query_models_by_id(PlateModel, [record_id])[0]

    def group_samples(self, plate: PlateModel, samples: list[SampleModel]) -> SapioWebhookResult:
        # Remove any existing well assignments to avoid layers
        existing_wells: list[PlateDesignerWellElementModel] = (
            self.rec_handler.query_models(PlateDesignerWellElementModel,
                                          PlateDesignerWellElementModel.PLATERECORDID__FIELD_NAME.field_name,
                                          [plate.record_id]))
        if existing_wells:
            for e in existing_wells:
                e.delete()
            self.rec_man.store_and_commit()

        # Iterate and add all Verso samples in their respective positions
        warning = []
        if len(samples) >= len(Constants.PLATE_LOCATIONS):
            return PopupUtil.ok_popup("Error", f"Plate {plate.get_PlateId_field()} does not have enough wells"
                                               f"to contain all samples.")
        pdwes: list[PlateDesignerWellElementModel] = []
        verso_pattern = re.compile(r"^H[A-Z]\w+")
        well_locations_filled = []
        for sample in samples:
            if verso_pattern.match(sample.get_TubeBarcode_field()):
                if sample.get_ColPosition_field() and sample.get_RowPosition_field():
                    pdwe: PlateDesignerWellElementModel = self.create_well_element(sample, plate)
                    well_locations_filled.append(sample.get_RowPosition_field()+sample.get_ColPosition_field())
                    pdwe.set_RowPosition_field(sample.get_RowPosition_field())
                    pdwe.set_ColPosition_field(sample.get_ColPosition_field())
                    pdwes.append(pdwe)
                    samples.remove(sample)
                else:
                    warning.append(sample.get_SampleId_field())

        # Sort the list by sample ID
        samples.sort(key=lambda x: x.get_SampleId_field())

        # Place each sample onto the plate
        plate_locations = [i for i in Constants.PLATE_LOCATIONS if i not in well_locations_filled]
        for i, s in enumerate(samples):
            pdwe: PlateDesignerWellElementModel = self.create_well_element(s, plate)
            well_location: str = plate_locations[i]
            pdwe.set_RowPosition_field(well_location[0:1])
            pdwe.set_ColPosition_field(well_location[1: len(well_location)])
            pdwes.append(pdwe)

        # Store and commit to get the record IDs of the aliquots
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def create_well_element(self, sample: SampleModel, plate: PlateModel) -> PlateDesignerWellElementModel:
        pdwe: PlateDesignerWellElementModel = self.rec_handler.add_model(PlateDesignerWellElementModel)
        pdwe.set_PlateRecordId_field(plate.record_id)
        pdwe.set_Layer_field(1)
        pdwe.set_SourceDataTypeName_field(SampleModel.DATA_TYPE_NAME)
        pdwe.set_SourceRecordId_field(sample.record_id)

        # If the sample is a control sample, then just return the well element without doing any normalization.
        if SampleUtils.is_control(sample):
            return pdwe

        # Update source volume to use as needed
        if sample.get_Concentration_field() and sample.get_Concentration_field() > 180:
            pdwe.set_Volume_field(1)
        else:
            pdwe.set_Volume_field(2)
        return pdwe
