from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import InvoiceModel
from utilities.webhook_handler import OsloWebhookHandler


class SendInvoices(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        # Get all the invoice records
        invoices: list[InvoiceModel] = self.rec_handler.wrap_models(context.data_record_list, InvoiceModel)

        # For now, set all of their sent fields to True
        for i in invoices:

            i.set_Sent_field(True)

        # Store and commit changes
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, "All unsent invoices sent.")
