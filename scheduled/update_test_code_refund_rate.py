from pandas import DataFrame
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import TestCodeConfigurationModel, NLKCodeModel, AssayDetailModel, InvoiceModel
from utilities.webhook_handler import OsloWebhookHandler


class UpdateTestCodeRefundRate(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        # Get either the NLK code or test code config record
        data_type_name: str = context.data_record.data_type_name
        test_code_configs: list[TestCodeConfigurationModel] = []
        if data_type_name == TestCodeConfigurationModel.DATA_TYPE_NAME:
            test_code_config: TestCodeConfigurationModel = self.rec_handler.wrap_model(context.data_record,
                                                                                       TestCodeConfigurationModel)
            test_code_configs.append(test_code_config)
        else:
            nlk_code: NLKCodeModel = self.rec_handler.wrap_model(context.data_record, NLKCodeModel)

            self.rel_man.load_parents_of_type([nlk_code], TestCodeConfigurationModel)
            test_code_configs: list[TestCodeConfigurationModel] = (
                nlk_code.get_parents_of_type(TestCodeConfigurationModel))
            if not test_code_configs:
                return SapioWebhookResult(True)

        # Get all the assay details linked to this config
        test_code_ids: list[str] = [t.get_TestCodeID_field() for t in test_code_configs]
        all_assay_details: list[AssayDetailModel] = self.rec_handler.query_models(AssayDetailModel,
                                                                                  AssayDetailModel.TESTCODEID__FIELD_NAME.field_name,
                                                                                  test_code_ids)

        # Get the NLK code config children of the test code config
        self.rel_man.load_children_of_type(test_code_configs, NLKCodeModel)

        # Calculate the total refund rate from all the NLK codes and set it. Also set the total refund rate on the
        # invoice
        for t in test_code_configs:
            nlk_codes: list[NLKCodeModel] = t.get_children_of_type(NLKCodeModel)
            total_refund_rate: float = 0.0
            for n in nlk_codes:
                try:
                    total_refund_rate += n.get_RefundRate_field()
                except Exception:
                    continue
            t.set_TotalRefundRate_field(total_refund_rate)

            # Set the total refund rate on all the linked assay details
            assay_details: list[AssayDetailModel] = [a for a in all_assay_details if
                                                     a.get_TestCodeId_field() == t.get_TestCodeID_field()]
            for a in assay_details:
                a.set_NLKRefundRate_field(total_refund_rate)

        # Set the refund rate on the invoices
        self.rel_man.load_parents_of_type(all_assay_details, InvoiceModel)
        invoices: list[InvoiceModel] = [a.get_parent_of_type(InvoiceModel) for a in all_assay_details if
                                        a.get_parent_of_type(InvoiceModel)]
        if not invoices:
            return SapioWebhookResult(True)
        self.rel_man.load_children_of_type(invoices, AssayDetailModel)
        for i in invoices:
            assay_details: list[AssayDetailModel] = i.get_children_of_type(AssayDetailModel)
            refund_rate: float = 0.0
            for a in assay_details:
                try:
                    refund_rate += a.get_NLKRefundRate_field()
                except Exception:
                    continue
            i.set_RefundRate_field(refund_rate)

        # Store and commit
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
