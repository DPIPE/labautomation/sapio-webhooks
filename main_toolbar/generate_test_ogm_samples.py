from typing import Any

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities import utils
from utilities.data_models import ProjectModel, SampleModel, RequestModel, PatientModel, AssayDetailModel, InvoiceModel, \
    OGMAssayDetailModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler


class GenerateTestOGMSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.callback_util = CallbackUtil(context)

        # Prompt the user to enter the number of samples to create.
        num_samples: int | None = self.callback_util.integer_input_dialog("Create Test OGM Samples",
                                                                          "Enter the number of samples to create.",
                                                                          "Number of samples", 1, 1)
        if not num_samples:
            return SapioWebhookResult(True)

        # Prompt the user to create research or diagnostic samples.
        classification: str | None = self.prompt_classification(context.user)
        if not classification:
            return SapioWebhookResult(True)

        # Create fields for each of the samples and create them.
        fields: list[dict[str, Any]] = [{"Concentration": 50.0, "ConcentrationUnits": "ng/μL", "Volume": 50.0,
                                         "VolumeUnits": "μL", "ExemplarSampleType": "EDTA Blood",
                                         "Classification": classification} for x in range(num_samples)]
        samples: list[SampleModel] = self.rec_handler.add_models_with_data(SampleModel, fields)

        # If the samples are research samples, prompt the user for a project to put the samples under. Otherwise, create
        # an order and use a dummy patient to put the samples under instead.
        if classification == "Research":
            project: ProjectModel | None = self.prompt_project()
            if not project:
                return SapioWebhookResult(True)

            # Add the samples under the project.
            project.add_children(samples)

            # Store and commit changes so that we can assign the samples to the OGM process.
            self.rec_man.store_and_commit()

            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, samples, "OGM")
        else:
            order: RequestModel = self.rec_handler.add_model(RequestModel)
            order.set_Comments_field("Generated from \"Generate Test OGM Samples\" button.")

            # Store and commit changes so that a proper order name is set on the order and to get the order's record ID.
            self.rec_man.store_and_commit()

            # Add an OGM assay detail to the order along with an invoice.
            assay_detail: AssayDetailModel | None = (
                RequestUtils.generate_assay_detail(self.rec_handler, "OGM", order.record_id))
            if assay_detail:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, order.get_RequestName_field(),
                                                  assay_detail.get_Assay_field()))
                invoice.add_child(assay_detail)
            else:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, order.get_RequestName_field(), "OGM"))
            order.add_child(invoice)

            # Add the appropriate assay detail extension to the order.
            extension: OGMAssayDetailModel = self.rec_handler.add_model(OGMAssayDetailModel)
            extension.set_Priority_field("Normal")
            extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
            order.add_child(extension)

            # Get the test patient or create it if it doesn't exist.
            try:
                patient: PatientModel = (
                    self.rec_handler.query_models(PatientModel,
                                                  PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                                  ["555-55-5555"]))[0]
            except:
                patient: PatientModel = self.rec_handler.add_model(PatientModel)
                patient.set_SocialSecurityNumber_field("555-55-5555")
                patient.set_FirstName_field("Dummy")
                patient.set_LastName_field("Patient")

            # Set the proper sample names and sample IDs for diagnostic samples.
            for x, sample in enumerate(samples, start=1):
                sample_id: str = (f"{order.get_RequestName_field()}"
                                  f"{utils.add_leading_zeroes(str(x), 2)}")
                sample.set_OtherSampleId_field(sample_id)
                sample.set_SampleId_field(sample_id)

            # Add the samples under the order and the order under the patient.
            order.add_children(samples)
            patient.add_child(order)

            # Approve the order to automatically queue the samples.
            order.set_AllSamplesReceived_field(True)
            order.set_RequestApproved_field(True)

            # Store and commit changes so that we can assign the samples to the OGM process.
            self.rec_man.store_and_commit()

            # We still need to assign the samples to the process via webhook due to the on-save rule for approving
            # requests not working with this.
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, samples, "OGM", 1, request=order)

        self.callback_util.ok_dialog("Notice", f"Successfully queued {len(samples)} samples for OGM.")
        return SapioWebhookResult(True)

    def prompt_classification(self, user: SapioUser) -> str | None:
        classifications: list[str] = (
            DataMgmtServer.get_picklist_manager(user).get_picklist("Classification").entry_list)
        selected_classifications: list[str] | None = (
            self.callback_util.list_dialog("Select classification for samples", classifications))
        if not selected_classifications:
            return None
        return selected_classifications[0]

    def prompt_project(self) -> ProjectModel | None:
        # Ask the user if they want to put the samples under an existing project or create a new one.
        project_selection: str | None = self.callback_util.option_dialog("Create Test OGM Samples",
                                                                         "Would you like to create these samples under"
                                                                         " an existing or new project?",
                                                                         ["Existing Project", "New Project"],
                                                                         user_can_cancel=True)
        if not project_selection:
            return None
        if project_selection == "Existing Project":
            # If there are no existing research projects in the system, prompt the user to create one. Otherwise, prompt
            # the user with a list of existing projects to choose from.
            projects: list[ProjectModel] | None = (
                self.rec_handler.query_models(ProjectModel, ProjectModel.CLASSIFICATION__FIELD_NAME.field_name,
                                              ["Research"]))
            if not projects:
                project: ProjectModel | None = self.create_new_project("There are currently no existing research"
                                                                       " projects in the system. Enter the name of the"
                                                                       " new project.")
                return project
            selected_projects: list[ProjectModel] | None = (
                self.callback_util.record_selection_dialog("Select project",
                                                           ["ProjectName", "DateCreated", "CreatedBy"], projects,
                                                           False))
            if not selected_projects:
                return None
            project: ProjectModel = selected_projects[0]
            return project
        else:
            return self.create_new_project("Enter the name of the new project.")

    def create_new_project(self, msg: str) -> ProjectModel | None:
        # Prompt the user to enter the new project's name.
        project_name: str | None = self.callback_util.string_input_dialog("Create Test OGM Samples",
                                                                          msg, "Project name")
        if not project_name:
            return None
        project: ProjectModel = self.rec_handler.add_model(ProjectModel)
        project.set_ProjectName_field(project_name)
        project.set_Classification_field("Research")
        project.set_ProjectDesc_field("Generated from \"Generate Test OGM Samples\" button.")
        return project
