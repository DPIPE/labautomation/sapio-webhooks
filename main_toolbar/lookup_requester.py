import json
from typing import cast, Any

import zeep
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import TempTableSelectionRequest, DataRecordSelectionRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import RequesterModel
from utilities.webhook_handler import OsloWebhookHandler

from zeep import helpers

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_INPUT: str = "input"
STATUS_INVALID_INPUT: str = "invalidInput"
STATUS_NO_RESULTS: str = "noResults"
STATUS_RESULT_SELECTION: str = "resultSelection"


class Søkeparametre(object):
    pass


class LookupRequester(OsloWebhookHandler):
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_INPUT:
                return self.process_input(cast(FormEntryDialogResult, result))
            if self.response_map[STATUS] == STATUS_INVALID_INPUT or self.response_map[STATUS] == STATUS_NO_RESULTS:
                return self.prompt_input()
            if self.response_map[STATUS] == STATUS_RESULT_SELECTION:
                return self.process_record_selection(cast(DataRecordSelectionResult, result))

        # Prompt the user to enter the last name or HPR number of the doctor
        return self.prompt_input()

    def prompt_input(self) -> SapioWebhookResult:
        self.response_map[STATUS] = STATUS_INPUT
        last_name_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("LastName", "LastName", "Last name")
        last_name_field.editable = True
        hpr_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("HPRNumber", "HPRNumber", "HPR #")
        hpr_field.editable = True
        return PopupUtil.form_popup("Enter Requester Data", "Enter the last name or HPR number of the requester.",
                                    [last_name_field, hpr_field], request_context=json.dumps(self.response_map))

    def process_input(self, result: FormEntryDialogResult) -> SapioWebhookResult:
        # Get the data entered by the user, and check for input errors.
        result_data: dict[str, Any] = result.user_response_map
        if result_data["LastName"] == "" and result_data["HPRNumber"] == "":
            self.response_map[STATUS] = STATUS_INVALID_INPUT
            return PopupUtil.ok_popup("Error", "Please enter a last name or HPR #.",
                                      request_context=json.dumps(self.response_map))
        if result_data["HPRNumber"] != "":
            try:
                int(result_data["HPRNumber"])
            except Exception:
                self.response_map[STATUS] = STATUS_INVALID_INPUT
                return PopupUtil.ok_popup("Error", "Invalid HPR #.", request_context=json.dumps(self.response_map))

        # Call the API. If there are no results returned, notify the user and prompt the dialog again.
        api_results: list[dict[str, Any]] = self.call_api(result_data)
        if not api_results:
            self.response_map[STATUS] = STATUS_NO_RESULTS
            return PopupUtil.ok_popup("Warning", f"No results found for last name of '{result_data['LastName']}' "
                                                 f"and/or HPR # of '{result_data['HPRNumber']}'",
                                      request_context=json.dumps(self.response_map))

        # If there is more than one result returned, present a table dialog to the user to select the Requester.
        # Otherwise, grab the single result and process it.
        if len(api_results) > 1:
            self.response_map[STATUS] = STATUS_RESULT_SELECTION
            request: DataRecordSelectionRequest = (
                DataRecordSelectionRequest("Result", "Results", [
                    VeloxStringFieldDefinition("HPRNumber", "HPRNumber", "HPR #"),
                    VeloxStringFieldDefinition("Title", "Title", "Title"),
                    VeloxStringFieldDefinition("FirstName", "FirstName", "First name"),
                    VeloxStringFieldDefinition("LastName", "LastName", "Last name")
                ], api_results, "There are multiple results. Select the requester from the list below.",
                                           callback_context_data=json.dumps(self.response_map)))
            return SapioWebhookResult(True, client_callback_request=request)
        else:
            return self.get_or_create_requester(api_results[0])

    def process_record_selection(self, result: DataRecordSelectionResult) -> SapioWebhookResult:
        selection: list[dict[str, Any]] = result.selected_field_map_list

        # If nothing was selected, treat it as a cancel operation.
        if not selection:
            return SapioWebhookResult(True)

        # Get or create the Requester record corresponding to the selection, and navigate the user to it.
        return self.get_or_create_requester(selection[0])

    def call_api(self, parameters: dict[str, Any]) -> list[dict[str, Any]]:
        # Initialize our client.
        # TODO: Once we have access to the remotely hosted WSDL, change the client to use that URL instead of our dummy.
        # client: Client = Client(wsdl="https://register-web.test.nhn.no/v2/HPR?wsdl")
        client: zeep.Client = zeep.Client(wsdl="./integrations/HPR - local.wsdl")
        client.settings.strict = False

        # Set up a search parameter instance that use any parameters that aren't blank.
        search_params: Søkeparametre = Søkeparametre()
        if parameters["LastName"] != "":
            search_params.Etternavn = parameters["LastName"]
        if parameters["HPRNumber"] != "":
            try:
                search_params.HprNummer = int(parameters["HPRNumber"])
            except Exception:
                ...

        # Call the API using the search parameters and parse the results for each doctor returned from them.
        result_set = client.service.Søk2(search=search_params)
        serialized_result_set: dict[str, Any] = helpers.serialize_object(result_set.Personer)
        doctors: list[dict[str, Any]] = serialized_result_set["Person"]
        api_results: list[dict[str, Any]] = []
        if doctors:
            for doctor in doctors:
                # Get the titles of the doctor by traversing down the result set's hierarchy.
                titles: list[str] = []
                godkjenninger_data: dict[str, Any] = doctor["Godkjenninger"]
                for godkjenning in godkjenninger_data:
                    for godkjenning_item in godkjenninger_data[godkjenning]:
                        spesialiteter_data: dict[str, Any] = godkjenning_item["Spesialiteter"]
                        for spesialitet in spesialiteter_data:
                            for spesialitet_item in spesialiteter_data[spesialitet]:
                                titles.append(spesialitet_item["Type"]["Kodegruppenavn"])

                # Append the results to the API results dict.
                api_results.append({"FirstName": doctor["Fornavn"], "LastName": doctor["Etternavn"],
                                    "HPRNumber": doctor["HPRNummer"], "Title": ", ".join(titles)})
        return api_results

    def get_or_create_requester(self, fields: dict[str, Any]) -> SapioWebhookResult:
        # Check if a Requester with a matching HPR number exists. If it doesn't then create one with the fields
        # from the user's selection.
        requesters: list[RequesterModel] = (
            self.rec_handler.query_models(RequesterModel, RequesterModel.HPRNUMBER__FIELD_NAME.field_name,
                                          [int(fields["HPRNumber"])]))
        if not requesters:
            requester: RequesterModel = self.rec_handler.add_model(RequesterModel)
            requester.set_HPRNumber_field(int(fields["HPRNumber"]))
            requester.set_FirstName_field(fields["FirstName"])
            requester.set_LastName_field(fields["LastName"])
            requester.set_Specialty_field(fields["Title"])
            self.rec_man.store_and_commit()
        else:
            requester: RequesterModel = requesters[0]

        # Return a directive to the Requester record.
        return SapioWebhookResult(True, directive=FormDirective(requester.get_data_record()))
