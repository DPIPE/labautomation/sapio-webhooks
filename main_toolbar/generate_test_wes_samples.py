import json
import random
from typing import cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import PatientModel, AssayDetailModel, InvoiceModel, WESAssayDetailModel
from utilities.data_models import RequestModel
from utilities.data_models import SampleModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_GROUP_INPUT: str = "groupInput"
STATUS_PROCESS_SELECTION: str = "processSelection"

NUM_GROUPS: str = "numGroups"


class GenerateTestWESSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.response_map = {STATUS: "", NUM_GROUPS: ""}


        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_GROUP_INPUT:
                try:
                    num_groups: int = int(cast(FormEntryDialogResult, result).user_response_map["Groups"])
                except Exception:
                    self.response_map[STATUS] = STATUS_ERROR
                    return PopupUtil.ok_popup("Error", "Please enter an integer.",
                                              request_context=json.dumps(self.response_map))
                return self.create_samples(context, num_groups)
        self.response_map[STATUS] = STATUS_GROUP_INPUT
        return PopupUtil.integer_field_popup("Number of Samples", "Enter how many groups of samples to make "
                                                                  "(groups of 4)", "Groups", 1, 1,
                                             request_context=json.dumps(self.response_map))

    def create_samples(self, context: SapioWebhookContext, num_groups: int) -> SapioWebhookResult:
        try:
            patient: PatientModel = self.rec_handler.query_models(PatientModel,
                                                                  PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                                                  ["WES test"])[0]
        except Exception:
            patient: PatientModel = self.rec_handler.add_model(PatientModel)
            patient.set_SocialSecurityNumber_field("WES test")

        # Create parent blood and child DNA samples. Also create an Order for each blood sample with a normal priority.
        samples: list[SampleModel] = []
        aliquots_to_order: dict[SampleModel, RequestModel] = {}
        for x in range(num_groups):
            for y in range(4):
                request: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                request.set_Comments_field("Generated from \"Generate Test WES Samples\" button.")
                # [OSLO-996]: Approve the request to mimic user behavior.
                request.set_RequestApproved_field(True)

                # Add the appropriate assay detail extension to the order.
                extension: WESAssayDetailModel = self.rec_handler.add_model(WESAssayDetailModel)
                extension.set_Priority_field("Normal")
                extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
                request.add_child(extension)

                blood_sample: SampleModel = SampleUtils.register_sample(self.rec_handler, self.acc_manager,
                                                                        sample_type="Blood")
                request.add_child(blood_sample)
                dna_aliquot: SampleModel = self.create_aliquot(blood_sample, "DNA", y % 2)
                dna_aliquot.set_Concentration_field(50.0)
                dna_aliquot.set_ConcentrationUnits_field("ng/µL")
                dna_aliquot.set_Volume_field(100.0)
                dna_aliquot.set_VolumeUnits_field("µL")
                samples.append(dna_aliquot)

                aliquots_to_order[dna_aliquot] = request
                patient.add_child(request)

        self.rec_man.store_and_commit()

        # [OSLO-996]: Add an assay detail to the orders.
        for aliquot in aliquots_to_order:
            assay_detail: AssayDetailModel | None = (
                RequestUtils.generate_assay_detail(self.rec_handler, "WES", aliquots_to_order[aliquot].record_id))
            if assay_detail:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  assay_detail.get_Assay_field()))
                invoice.add_child(assay_detail)
            else:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  "WES"))
            aliquots_to_order[aliquot].add_child(invoice)

        self.rec_man.store_and_commit()

        # Queue all the DNA samples for WES.
        for aliquot in aliquots_to_order:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [aliquot],
                                              "WES", request=aliquots_to_order[aliquot])

        return SapioWebhookResult(True, f"Successfully created and queued samples for WES.")

    def create_aliquot(self, parent_sample: SampleModel, sample_type: str, toggle: int) -> SampleModel:
        aliquot: SampleModel = self.rec_handler.add_model(SampleModel)
        aliquot.set_SampleId_field(parent_sample.get_SampleId_field() + "_1")
        aliquot.set_TopLevelSampleId_field(parent_sample.get_TopLevelSampleId_field())

        if toggle:
            aliquot.set_TubeBarcode_field(f"HZ{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE))[0]}")
        else:
            # create aliquots with fridge barcodes 
            aliquot.set_TubeBarcode_field(f"D{random.randint(0,99):2d}-{random.randint(0,9999):4d}")

        aliquot.set_ExemplarSampleType_field(sample_type)
        parent_sample.add_child(aliquot)
        return aliquot
