from typing import Any

from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.ClientCallbackService import ClientCallback
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxIntegerFieldDefinition, AbstractVeloxFieldDefinition
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import InputDialogCriteria, ListDialogRequest, \
    OptionDialogRequest, DataRecordSelectionRequest
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective, TableDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, PatientModel, PlateModel, RequestModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class GenerateTestSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    client_callback: ClientCallback

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.client_callback = DataMgmtServer.get_client_callback(context.user)

        # Prompt the user for the number of samples to create
        num_samples: int = self.prompt_num_samples()
        if num_samples is None or num_samples == 0:
            return SapioWebhookResult(True)
        if num_samples < 0 or num_samples > 96:
            return SapioWebhookResult(False, "Number of samples must be between 1 and 96 inclusively.")

        # Prompt the user for the type of samples to create
        sample_type: str = self.prompt_sample_type(context.user)
        if sample_type is None:
            return SapioWebhookResult(True)

        # If the user is creating DNA samples, ask them if they want to create parent blood samples for them as well
        create_parent_blood_samples: int = 1
        if sample_type == "DNA":
            create_parent_blood_samples = self.prompt_yes_no_dialog("Do you want to create parent Blood samples for"
                                                                    " these new DNA samples?")
            if create_parent_blood_samples is None:
                return SapioWebhookResult(True)

        # Ask the user if the samples should be plated
        plated: int = self.prompt_yes_no_dialog("Do you want to put these samples onto a plate?")
        if plated is None:
            return SapioWebhookResult(True)

        # Ask the user if they want to create orders for the samples. If so, prompt them to select a patient to assign
        # the orders to
        create_orders: int = self.prompt_yes_no_dialog("Do you want to create orders for the new samples?")
        if create_orders is None:
            return SapioWebhookResult(True)

        patient: PatientModel = None
        if create_orders == 0:
            all_patients: list[PatientModel] = self.rec_handler.query_all_models(PatientModel)
            patient, valid = self.prompt_patient(context.user, all_patients)
            if not valid:
                return SapioWebhookResult(True)

        # Create the samples
        return self.create_samples(num_samples, sample_type, create_parent_blood_samples, plated, create_orders,
                                   patient)

    def prompt_num_samples(self) -> int:
        return self.client_callback.show_input_dialog(InputDialogCriteria("Create Samples",
                                                                          "Enter the number of samples to "
                                                                          "create.",
                                                                          VeloxIntegerFieldDefinition("num", "num",
                                                                                                      "Number of "
                                                                                                      "Samples")))

    def prompt_sample_type(self, user: SapioUser) -> str:
        sample_types: list[str] = \
            [l for l in DataMgmtServer.get_picklist_manager(user).get_picklist_config_list()
             if l.pick_list_name == "Exemplar Sample Types"][0].entry_list
        sample_type_selection: list[str] = self.client_callback.show_list_dialog(
            ListDialogRequest("Select Sample Type", False,
                              sample_types))
        if not sample_type_selection:
            return None

        return sample_type_selection[0]

    def prompt_yes_no_dialog(self, msg: str) -> int:
        return self.client_callback.show_option_dialog(OptionDialogRequest("Create Samples", "Do you want to put "
                                                                                             "these samples onto a "
                                                                                             "plate? (If you are "
                                                                                             "creating parent blood "
                                                                                             "samples, then the "
                                                                                             "aliquots will be plated.)",
                                                                           ["Yes", "No"]))

    def prompt_patient(self, user: SapioUser, patients: list[PatientModel]) -> (PatientModel, bool):
        field_map_list: list[dict[str, Any]] = [p.get_data_record().get_fields() for p in patients]
        field_def_list: list[AbstractVeloxFieldDefinition] = (
            DataMgmtServer.get_data_type_manager(user).get_field_definition_list(PatientModel.DATA_TYPE_NAME))

        selections: list[dict[str, Any]] = self.client_callback.show_data_record_selection_dialog(
            DataRecordSelectionRequest(PatientModel.DATA_TYPE_NAME,
                                       f"{PatientModel.DATA_TYPE_NAME}s",
                                       field_def_list, field_map_list,
                                       "Select the patient to"
                                       " register the orders under."))
        if selections is None or len(selections) == 0:
            return None, False

        selection: dict[str, Any] = selections[0]
        return [p for p in patients if p.get_SocialSecurityNumber_field() ==
                selection[PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name]][0], True

    def create_samples(self, num_samples: int, sample_type: str, create_parent_blood_samples: int, plated: int,
                       create_orders: int, patient: PatientModel) -> SapioWebhookResult:
        samples: list[SampleModel] = []
        samples_to_parents: dict[SampleModel, SampleModel] = {}

        # Create a plate if the user chose to do so
        plate: PlateModel = None
        if plated == 1:
            plate = self.rec_handler.add_model(PlateModel)
            plate.set_PlateId_field(
                f"{Constants.ACCESSIONING_TEST_PLATE_ID}{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PLATE_ID))[0]}")

        # Create all the samples
        for x in range(0, num_samples):
            # Create the samples and their parents if the user chose to do so
            parent_sample: SampleModel = None
            sample: SampleModel = None
            if create_parent_blood_samples == 1:
                parent_sample = SampleUtils.register_sample(self.rec_handler, self.acc_manager,
                                                            sample_type="Blood")
                sample = SampleUtils.create_aliquot(self.rec_handler, self.rel_man, self.acc_manager, parent_sample,
                                                    sample_type=sample_type)
                samples_to_parents[sample] = parent_sample
            else:
                sample = SampleUtils.register_sample(self.rec_handler, self.acc_manager, sample_type=sample_type)

            samples.append(sample)

            # Create the order if the user chose to do so
            order: RequestModel = None
            if create_orders == 1:
                order = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                if parent_sample is not None:
                    order.add_child(parent_sample)
                else:
                    order.add_child(sample)

                # Add the order and sample to the patient if the user chose to do so
                patient.add_child(order)
                if parent_sample is not None:
                    patient.add_child(parent_sample)
                else:
                    patient.add_child(sample)

        # Plate the samples if the user chose to do so
        if plate is not None:
            plate_location_index: int = 0
            for s in samples:
                row: str = Constants.PLATE_LOCATIONS[plate_location_index][0:1]
                col: str = Constants.PLATE_LOCATIONS[plate_location_index][1: len(Constants.PLATE_LOCATIONS[plate_location_index])]
                s.set_RowPosition_field(row)
                s.set_ColPosition_field(col)
                s.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                plate.add_child(s)
                plate_location_index += 1

        # Store and commit changes
        self.rec_man.store_and_commit()

        # Return a directive to the new samples
        if plate is not None:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=FormDirective(plate.get_data_record()))

        if not create_parent_blood_samples:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=TableDirective([s.get_data_record() for s in samples]))
        else:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=TableDirective([s.get_data_record() for s in samples] +
                                                               [samples_to_parents[s].get_data_record() for s in
                                                                samples]))
