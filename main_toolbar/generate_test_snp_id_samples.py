import json
from typing import cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo, AccessionManager
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, RequestModel, VariantResultModel, AssayDetailModel, InvoiceModel, \
    SNPIDAssayDetailModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_GROUP_INPUT: str = "groupInput"
STATUS_PROCESS_SELECTION: str = "processSelection"

NUM_GROUPS: str = "numGroups"


class GenerateTestSNPIDSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.response_map = {STATUS: "", NUM_GROUPS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_GROUP_INPUT:
                try:
                    num_groups: int = int(cast(FormEntryDialogResult, result).user_response_map["Groups"])
                except Exception:
                    self.response_map[STATUS] = STATUS_ERROR
                    return PopupUtil.ok_popup("Error", "Please enter an integer.",
                                              request_context=json.dumps(self.response_map))
                return self.create_samples(context, num_groups)
        self.response_map[STATUS] = STATUS_GROUP_INPUT
        return PopupUtil.integer_field_popup("Number of Samples", "Enter how many groups of samples to make "
                                                                  "(groups of 4)", "Groups", 1, 1,
                                             request_context=json.dumps(self.response_map))

    def create_samples(self, context: SapioWebhookContext, num_groups: int) -> SapioWebhookResult:
        # Create parent blood and child DNA samples. Also create an Order for each blood sample with a normal priority.
        # Also, create a Variant Result child for each DNA aliquot.
        samples: list[SampleModel] = []
        aliquots_to_order: dict[SampleModel, RequestModel] = {}
        variants: list[str] = self.acc_manager.accession_for_system(num_groups * 4,
                                                                    AccessionSystemCriteriaPojo("TEST VARIANT - "))
        variant_index: int = 0
        for x in range(num_groups):
            for y in range(4):
                request: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                request.set_Comments_field("Generated from \"Generate Test SNP-ID Samples\" button.")
                # [OSLO-996]: Approve the request to mimic user behavior.
                self.rec_man.store_and_commit()

                request.set_RequestApproved_field(True)
                assay_detail: AssayDetailModel | None = (
                    RequestUtils.generate_assay_detail(self.rec_handler, "SNP-ID", request.record_id))
                if assay_detail:
                    request.add_child(assay_detail)

                # Add the appropriate assay detail extension to the order.
                extension: SNPIDAssayDetailModel = self.rec_handler.add_model(SNPIDAssayDetailModel)
                extension.set_Priority_field("Normal")
                extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
                request.add_child(extension)

                blood_sample: SampleModel = SampleUtils.register_sample(self.rec_handler, self.acc_manager,
                                                                        sample_type="Blood")
                request.add_child(blood_sample)
                dna_aliquot: SampleModel = self.create_aliquot(blood_sample, "DNA")
                dna_aliquot.set_Concentration_field(13.0)
                dna_aliquot.set_ConcentrationUnits_field("ng/µL")
                dna_aliquot.set_Volume_field(4.0)
                dna_aliquot.set_VolumeUnits_field("µL")

                variant_result: VariantResultModel = self.rec_handler.add_model(VariantResultModel)
                variant_result.set_Variant_field(f"TEST VARIANT - {variants[variant_index]}")
                dna_aliquot.add_child(variant_result)

                samples.append(dna_aliquot)

                aliquots_to_order[dna_aliquot] = request

                variant_index += 1

        self.rec_man.store_and_commit()

        # [OSLO-996]: Add an assay detail to the orders.
        for aliquot in aliquots_to_order:
            assay_detail: AssayDetailModel | None = (
                RequestUtils.generate_assay_detail(self.rec_handler, "SNP-ID", aliquots_to_order[aliquot].record_id))
            if assay_detail:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  assay_detail.get_Assay_field()))
                invoice.add_child(assay_detail)
            else:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  "SNP-ID"))
            aliquots_to_order[aliquot].add_child(invoice)

        self.rec_man.store_and_commit()

        # Queue all the DNA samples for SNP-ID.
        for aliquot in aliquots_to_order:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [aliquot],
                                              "SNP-ID", request=aliquots_to_order[aliquot])

        return SapioWebhookResult(True, f"Successfully created and queued samples for SNP-ID.")

    def create_aliquot(self, parent_sample: SampleModel, sample_type: str) -> SampleModel:
        aliquot: SampleModel = self.rec_handler.add_model(SampleModel)
        aliquot.set_SampleId_field(parent_sample.get_SampleId_field() + "_1")
        aliquot.set_TopLevelSampleId_field(parent_sample.get_TopLevelSampleId_field())
        aliquot.set_TubeBarcode_field(f"HZ{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE))[0]}")
        aliquot.set_ExemplarSampleType_field(sample_type)
        parent_sample.add_child(aliquot)
        return aliquot
