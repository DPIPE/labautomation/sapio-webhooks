import io
import random

from openpyxl.styles import Border
from openpyxl.styles import PatternFill
from openpyxl.styles import Side
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.AccessionService import AccessionSystemCriteriaPojo
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.oslo_exception import OsloException
from utilities.webhook_handler import OsloWebhookHandler
from utilities.xlsx_utils import XLSXUtils

SHORT_READ_HEADERS: list[str] = \
    ["Sample Number", "Sample Name",
     "Position in Plate", "Plate Number (if > 1 plate submitted)", "Pool Name (if applicable)",
     "Sample/Pool Volume (µl)", "Sample/Pool Conc. (ng/µl)", "A260/280", "A260/230",
     "Sample Type (e.g. gDNA, amplicon, library etc.)",
     "Index Name (mandatory for libraries, can be left blank for DNA/RNA)",
     "Index Sequence (mandatory for libraries, leave blank for DNA/RNA)",
     "Comments (e.g. RIN values, pooling strategies, number of reads/Gb requested etc.)"]
LONG_READ_HEADERS: list[str] = []


class GenerateTestNSCSampleFile(OsloWebhookHandler):
    acc_man: AccessionManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.acc_man = AccessionManager(self.user)

        try:
            # Prompt the user to generate a short-read or long-read file.
            project_type_selection: list[str] = self.callback.list_dialog("Select Project Type",
                                                                          ["Short-Read", "Long-Read"])
            if not project_type_selection:
                return SapioWebhookResult(True)

            # Generate the file data depending on the user's selection and download the file to the user.
            if project_type_selection[0] == "Short-Read":
                file_name, file_bytes = self._generate_short_read_file_data()
            else:
                self.callback.ok_dialog("Notice", "Work in progress.")
                return SapioWebhookResult(True)
                # file_name, file_bytes = self._generate_long_read_file_data()


            self.callback.write_file(file_name, file_bytes)

        except Exception as e:
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
            raise e

        return SapioWebhookResult(True)

    def _generate_short_read_file_data(self) -> (str, bytes):
        # Generate accession values for 96 samples, with 3 pools of 31 samples each.
        sample_name_accession_values: list[str] = (
            self.acc_man.accession_for_system(96, AccessionSystemCriteriaPojo("-TestNSC")))
        sample_names: list[str] = [f"{x}-TestNSC" for x in sample_name_accession_values]
        pool_id_accession_values: list[str] = self.acc_man.accession_for_system(3, AccessionSystemCriteriaPojo("Pool"))
        pool_ids: list[str] = [f"Pool{x}" for x in pool_id_accession_values]

        # Set up our workbook to write the data to.
        workbook: Workbook = Workbook()
        sheet: Worksheet = workbook.active
        border: Border = Border(left=Side("thin"), right=Side("thin"), top=Side("thin"), bottom=Side("thin"))
        header_fill: PatternFill = PatternFill(start_color="B8CCE4", end_color="B8CCE4", fill_type="solid")

        # Write the headers.
        for x, header in enumerate(SHORT_READ_HEADERS, start=1):
            XLSXUtils.write_cell(sheet, 1, x, header, border, header_fill)

        # Write data for the samples to the file.
        pool_id_index: int = 0
        for x in range(2, 94):
            XLSXUtils.write_cell(sheet, x, 1, str(x - 2), border)
            XLSXUtils.write_cell(sheet, x, 2, sample_names[x], border)
            XLSXUtils.write_cell(sheet, x, 3, Constants.PLATE_LOCATIONS[x - 2], border)
            XLSXUtils.write_cell(sheet, x, 4, "", border)
            XLSXUtils.write_cell(sheet, x, 5, pool_ids[pool_id_index], border)
            XLSXUtils.write_cell(sheet, x, 6, random.randint(1, 100), border)
            XLSXUtils.write_cell(sheet, x, 7, random.randint(1, 100), border)
            XLSXUtils.write_cell(sheet, x, 8, random.randint(1, 100), border)
            XLSXUtils.write_cell(sheet, x, 9, random.randint(1, 100), border)
            XLSXUtils.write_cell(sheet, x, 10, "DNA", border)
            XLSXUtils.write_cell(sheet, x, 11, f"D70{x}-D50{x}", border)
            XLSXUtils.write_cell(sheet, x, 12, "ATTACTCG-TATAGCCT", border)
            XLSXUtils.write_cell(sheet, x, 13, "Test sample generated using webhook.", border)

        #     # Increment the pool index if necessary.
        #     if x % 31 == 0:
        #         pool_id_index += 1
        #
        # # Write data for the pools to the file.
        # pool_id_index = 0
        # for x in range(94, 97):
        #     XLSXUtils.write_cell(sheet, x, 1, x - 1, border)
        #     XLSXUtils.write_cell(sheet, x, 2, pool_ids[pool_id_index], border)
        #     XLSXUtils.write_cell(sheet, x, 3, Constants.PLATE_LOCATIONS[x - 1], border)
        #     XLSXUtils.write_cell(sheet, x, 4, "", border)
        #     XLSXUtils.write_cell(sheet, x, 5, "", border)
        #     XLSXUtils.write_cell(sheet, x, 6, random.randint(1, 100), border)
        #     XLSXUtils.write_cell(sheet, x, 7, random.randint(1, 100), border)
        #     XLSXUtils.write_cell(sheet, x, 8, random.randint(1, 100), border)
        #     XLSXUtils.write_cell(sheet, x, 9, random.randint(1, 100), border)
        #     XLSXUtils.write_cell(sheet, x, 10, "DNA", border)
        #     XLSXUtils.write_cell(sheet, x, 11, f"D70{x}-D50{x}", border)
        #     XLSXUtils.write_cell(sheet, x, 12, "ATTACTCG-TATAGCCT", border)
        #     pool_id_index += 1

        # Save the workbook to a byte stream and return it along with the file name.
        buffer: io.BytesIO = io.BytesIO()
        workbook.save(buffer)
        return "TEST Short-read NSC Samples.xlsx", buffer.getvalue()

    def _generate_long_read_file_data(self) -> (str, bytes):
        # TODO: Implement when we have long read file parsing completed for S21.
        return None
