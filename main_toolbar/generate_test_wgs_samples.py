from typing import Any

from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import HomePageDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import PatientModel, RequestModel, SampleModel, FamilyDetailsModel, FamilyModel, \
    ExemplarConfigModel, PlateModel, AssayDetailModel, InvoiceModel, WGSAssayDetailModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

NUM_SAMPLES: int = 9


class GenerateTestWGSSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = AccessionManager(context.user)

        try:
            patient: PatientModel = self.rec_handler.query_models(PatientModel,
                                                                  PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                                                  ["WGS test"])[0]
        except Exception:
            patient: PatientModel = self.rec_handler.add_model(PatientModel)
            patient.set_SocialSecurityNumber_field("WGS test")

        # Get the min WGS concentration threshold
        config: ExemplarConfigModel = self.rec_handler.query_all_models(ExemplarConfigModel)[0]
        min_threshold: float = max(config.get_WGSLowConcentrationThreshold_field(), 1.0)
        min_threshold_units: str = config.get_WGSConcentrationThresholdUnits_field()

        # Create a plate for the blood samples and another for the DNA aliquots.
        plates: list[PlateModel] = self.rec_handler.add_models(PlateModel, 2)
        plate_ids: list[str] = self.acc_manager.accession_for_system(2, AccessionSystemCriteriaPojo(
            Constants.ACCESSIONING_TEST_PLATE_ID))
        plates[0].set_PlateId_field(f"{Constants.ACCESSIONING_TEST_PLATE_ID}{plate_ids[0]}")
        plates[1].set_PlateId_field(f"{Constants.ACCESSIONING_TEST_PLATE_ID}{plate_ids[1]}")

        # Create 3 family IDs and families
        ids: list[str] = self.acc_manager.accession_for_system(3, AccessionSystemCriteriaPojo(
            Constants.ACCESSIONING_FAMILY_ID))
        family_ids: list[dict[str, Any]] = [{"FamilyId": f"{Constants.ACCESSIONING_FAMILY_ID}{i}"} for i in ids]
        self.rec_handler.add_models_with_data(FamilyModel, family_ids)

        # Create 9 orders
        orders: list[RequestModel] = []
        for x in range(0, NUM_SAMPLES):
            order: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
            order.set_Comments_field("Generated from \"Generate Test WGS Samples\" button.")
            # [OSLO-996]: Approve the request to mimic user behavior.
            order.set_RequestApproved_field(True)

            # Add the appropriate assay detail extension to the order.
            # TODO: Have these correspond to the trio IDs.
            extension: WGSAssayDetailModel = self.rec_handler.add_model(WGSAssayDetailModel)
            extension.set_Priority_field("Normal")
            extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
            order.add_child(extension)

            orders.append(order)
            patient.add_child(order)

        # Create 9 blood samples and DNA aliquots for each of them
        plate_index: int = 0
        samples: list[SampleModel] = []
        aliquots: list[SampleModel] = []
        aliquots_to_order: dict[SampleModel, RequestModel] = {}
        for x in range(0, 3):
            for y in range(x * 3, (x * 3) + 3):
                well_location: str = Constants.PLATE_LOCATIONS[plate_index]

                sample: SampleModel = SampleUtils.register_sample(self.rec_handler, self.acc_manager, False, "Blood")
                sample.set_Concentration_field(x)
                sample.set_ConcentrationUnits_field(min_threshold_units)
                sample.set_Volume_field(16)
                sample.set_VolumeUnits_field("µL")
                sample.set_RowPosition_field(well_location[0:1])
                sample.set_ColPosition_field(well_location[1:len(well_location)])
                orders[y].add_child(sample)
                plates[0].add_child(sample)
                samples.append(sample)

                family_details: FamilyDetailsModel = self.rec_handler.add_model(FamilyDetailsModel)
                family_details.set_FamilyId_field(family_ids[x]["FamilyId"])
                sample.add_child(family_details)

                aliquot: SampleModel = SampleUtils.create_aliquot(self.rec_handler, self.rel_man, self.acc_manager,
                                                                  sample, "DNA", shire=True)
                aliquot.set_Concentration_field(min_threshold + 1)
                aliquot.set_ConcentrationUnits_field(min_threshold_units)
                aliquot.set_Volume_field(16)
                aliquot.set_VolumeUnits_field("µL")
                aliquot.set_RowPosition_field(well_location[0:1])
                aliquot.set_ColPosition_field(well_location[1:len(well_location)])
                sample.add_child(aliquot)
                plates[1].add_child(aliquot)
                aliquots.append(aliquot)
                aliquots_to_order[aliquot] = orders[y]

                plate_index += 1

        # Store and commit
        self.rec_man.store_and_commit()

        # [OSLO-996]: Add an assay detail to the orders.
        for aliquot in aliquots_to_order:
            assay_detail: AssayDetailModel | None = (
                RequestUtils.generate_assay_detail(self.rec_handler, "WGS", aliquots_to_order[aliquot].record_id))
            if assay_detail:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  assay_detail.get_Assay_field()))
                invoice.add_child(assay_detail)
            else:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, aliquots_to_order[aliquot].get_RequestName_field(),
                                                  "WGS"))
            aliquots_to_order[aliquot].add_child(invoice)

        self.rec_man.store_and_commit()

        # Send the aliquots to WGS and return to the home page
        for a in aliquots:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, [a], "WGS",
                                              request=aliquots_to_order[a])
        return SapioWebhookResult(True, "Successfully created and sent test samples to WGS.",
                                  directive=HomePageDirective())
