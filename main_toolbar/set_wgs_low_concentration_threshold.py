import json
from typing import Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxDoubleFieldDefinition, VeloxPickListFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, OptionDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.data_models import ExemplarConfigModel, SampleModel
from utilities.webhook_handler import OsloWebhookHandler


class SetWGSLowConcentrationThreshold(OsloWebhookHandler):
    rec_handler: RecordHandler

    response_data: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        result: ClientCallbackResult = context.client_callback_result

        self.response_data = {"status": ""}

        if result is None:
            return self.prompt_form()

        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_data = json.loads(result.callback_context_data)

        if isinstance(result, FormEntryDialogResult):
            return self.process_input(result)

        elif isinstance(result, OptionDialogResult):
            if self.response_data["status"] == "error":
                return SapioWebhookResult(True)

    def prompt_form(self) -> SapioWebhookResult:
        # Get the sapio config record
        config: ExemplarConfigModel = self.rec_handler.query_all_models(ExemplarConfigModel)[0]

        # Construct the form
        form_builder: FormBuilder = FormBuilder()

        threshold_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition(form_builder.get_data_type_name(),
                                                                                 "Threshold",
                                                                                 "Threshold",
                                                                                 default_value=config.get_WGSLowConcentrationThreshold_field())
        threshold_field.editable = True
        threshold_field.required = True
        form_builder.add_field(threshold_field)

        threshold_units_field: VeloxPickListFieldDefinition = VeloxPickListFieldDefinition(form_builder.get_data_type_name(),
                                                                                           "Units",
                                                                                           "Units",
                                                                                           "Concentration Units",
                                                                                           default_value=config.get_WGSConcentrationThresholdUnits_field())
        threshold_units_field.editable = True
        threshold_units_field.required = True
        form_builder.add_field(threshold_units_field)

        self.response_data["status"] = "input"

        # Return the form
        temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()
        request: FormEntryDialogRequest = FormEntryDialogRequest("WGS Concentration Threshold",
                                                                 "Enter the minimum amount of concentration that a "
                                                                 "sample must have in order to be queued for the WGS "
                                                                 "workflow.", temp_dt,
                                                                 callback_context_data=json.dumps(self.response_data))

        return SapioWebhookResult(True, client_callback_request=request)

    def process_input(self, result: FormEntryDialogResult) -> SapioWebhookResult:
        input_map: dict[str, Any] = result.user_response_map

        # Get the sapio config record
        config: ExemplarConfigModel = self.rec_handler.query_all_models(ExemplarConfigModel)[0]

        # Update the data
        try:
            config.set_WGSLowConcentrationThreshold_field(float(input_map["Threshold"]))

        except Exception:
            self.response_data["status"] = "error"
            return PopupUtil.ok_popup("Error", "Threshold must be a decimal.",
                                      request_context=json.dumps(self.response_data))

        try:
            config.set_WGSConcentrationThresholdUnits_field(input_map["Units"])

        except Exception:
            self.response_data["status"] = "error"
            return PopupUtil.ok_popup("Error", "Invalid concentration units.",
                                      request_context=json.dumps(self.response_data))

        # Store and commit
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, "Updated threshold.")
