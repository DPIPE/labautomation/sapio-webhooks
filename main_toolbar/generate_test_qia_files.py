import random
from typing import Any

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType, VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, ConsumableItemModel
from utilities.webhook_handler import OsloWebhookHandler

plate_locations: list[str] = [
    'A:1', 'B:1', 'C:1', 'D:1', 'E:1', 'F:1', 'G:1', 'H:1',
    'A:2', 'B:2', 'C:2', 'D:2', 'E:2', 'F:2', 'G:2', 'H:2',
    'A:3', 'B:3', 'C:3', 'D:3', 'E:3', 'F:3', 'G:3', 'H:3',
    'A:4', 'B:4', 'C:4', 'D:4', 'E:4', 'F:4', 'G:4', 'H:4',
    'A:5', 'B:5', 'C:5', 'D:5', 'E:5', 'F:5', 'G:5', 'H:5',
    'A:6', 'B:6', 'C:6', 'D:6', 'E:6', 'F:6', 'G:6', 'H:6',
    'A:7', 'B:7', 'C:7', 'D:7', 'E:7', 'F:7', 'G:7', 'H:7',
    'A:8', 'B:8', 'C:8', 'D:8', 'E:8', 'F:8', 'G:8', 'H:8',
    'A:9', 'B:9', 'C:9', 'D:9', 'E:9', 'F:9', 'G:9', 'H:9',
    'A:10', 'B:10', 'C:10', 'D:10', 'E:10', 'F:10', 'G:10', 'H:10',
    'A:11', 'B:11', 'C:11', 'D:11', 'E:11', 'F:11', 'G:11', 'H:11',
    'A:12', 'B:12', 'C:12', 'D:12', 'E:12', 'F:12', 'G:12', 'H:12'
]

qiaxpert_headers: list[str] = ["Position", "Sample name", "dsDNA (ng/ul)", "Nucleic acids (ng/ul)", "Impurities (A260)",
                               "Background (A260)", "Residue (%)", "A260", "A260/A280", "A260/A230"]


class GenerateTestQIAFiles(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.callback_util = CallbackUtil(context)

        # Get all the blood samples. If there aren't any, throw an error telling the user.
        results: list[list[Any]] | None = (
            DataMgmtServer.get_custom_report_manager(context.user).run_system_report_by_name("All Blood Samples")
            .result_table)
        if not results:
            self.callback_util.ok_dialog("Warning", "There are currently no blood samples in the system.")
            return SapioWebhookResult(True)

        # Prompt the user to select samples.
        rows: list[dict[str, Any]] = [{"SampleId": x[0], "OtherSampleId": x[1], "TubeBarcode": x[2]} for x in results]
        invalid_data: bool = False
        while not invalid_data:
            selections: list[dict[str, Any]] | None = (
                self.callback_util.selection_dialog("Select samples to generate QIA files for.", [
                    VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID"),
                    VeloxStringFieldDefinition("OtherSampleId", "OtherSampleId", "Sample Name"),
                    VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode")], rows))
            if not selections:
                return SapioWebhookResult(True)

            # Check to make sure the user selected a multiple of 24 samples.
            if len(selections) % 24 != 0:
                self.callback_util.ok_dialog("Warning", "Please select a multiple of 24 samples.")
                continue
            invalid_data = True

        return self.generate_qia_files(context.user, selections)

    def generate_qia_files(self, user: SapioUser, selections: list[dict[str, Any]]) -> SapioWebhookResult:
        file_datas: dict[str, bytes] = {}

        # Get all the sample IDs
        sample_ids: list[str] = []
        for i, d in enumerate(selections):
            sample_ids.append(d["SampleId"])

        # Write data to the QIAsymphony file
        plate_id: str = f"TESTPLATE{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PLATE_ID))[0]}"
        batch_ids: list[str] = self.acc_manager.accession_for_system(len(selections) // 24,
                                                                     AccessionSystemCriteriaPojo("TESTBATCH"))
        tube_barcodes: list[str] = self.acc_manager.accession_for_system(len(sample_ids),
                                                                         AccessionSystemCriteriaPojo(
                                                                             Constants.ACCESSIONING_VERSO_TUBE_BARCODE))

        file_data: str = (f"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                          f"<FullPlateTrack Type=\"String\" Class=\"FullPlateTrack\">\n"
                          f"\t<NofCols>12</NofCols>\n"
                          f"\t<NofRows>8</NofRows>\n"
                          f"\t<PlateID Type=\"String\">{plate_id}</PlateID>\n"
                          f"\t<Instrument Type=\"String\">qssp34192</Instrument>\n")

        batch_runs: int = 0

        for b in batch_ids:
            file_data += (f"\t<BatchTrack Type=\"Object\" Class=\"BatchTrack\">\n"
                          f"\t\t<BatchID Type=\"UInt\">TESTBATCH-{b}</BatchID>\n")
            for x in range(0 + (24 * batch_runs), (24 * (batch_runs + 1))):
                file_data += (f"\t\t<SampleTrack Type=\"Object\" Class=\"SampleTrack\">\n"
                              f"\t\t\t<SampleCode Type=\"String\">{sample_ids[x]}</SampleCode>\n"
                              f"\t\t\t<EluateTubeBarcode Type=\"String\">HZ{tube_barcodes[x]}</EluateTubeBarcode>\n"
                              f"\t\t\t<SamplePosition Type=\"String\">{x + 1}</SamplePosition>\n"
                              f"\t\t</SampleTrack>\n")
            file_data += "\t</BatchTrack>\n"
            batch_runs += 1

        # [OSLO-590]: Add two random Reagents to the file.
        valid_reagents: list[ConsumableItemModel] = self.get_valid_reagents(user)
        if valid_reagents:
            if len(valid_reagents) > 1:
                reagents: list[ConsumableItemModel] = [valid_reagents.pop(random.randint(0, len(valid_reagents) - 1)),
                                                       valid_reagents.pop(random.randint(0, len(valid_reagents) - 1))]
            else:
                reagents: list[ConsumableItemModel] = [valid_reagents[0]]

            file_data += (f"\t<ReagentRackTrack Type=\"Object\" Class=\"ReagentRackTrack\">\n"
                          f"\t\t<ReagentRackLabel Type=\"String\">Reagent rack number</ReagentRackLabel>\n")
            for reagent in reagents:
                file_data += (f"\t\t<ReagentTrack Type=\"Object\" Class=\"ReagentTrack\">\n"
                              f"\t\t\t<Id Type=\"String\">{reagent.get_PartNumber_field()}</Id>\n"
                              f"\t\t\t<Lot Type=\"String\">{reagent.get_LotNumber_field()}</Lot>\n"
                              f"\t\t</ReagentTrack>\n")

            file_data += f"\t</ReagentRackTrack>\n"
        file_data += "</FullPlateTrack>"

        file_datas[f"qiasymphony_{plate_id}.xml"] = bytes(file_data, "utf-8")

        # Write data to the QIAxpert file
        qiaxpert_file_data: str = ""

        # Get the aliquot names of all the samples
        samples: list[SampleModel] = self.rec_handler.query_models(SampleModel,
                                                                   SampleModel.SAMPLEID__FIELD_NAME.field_name,
                                                                   sample_ids)
        self.rel_man.load_children_of_type(samples, SampleModel)
        children_dict: dict[str, int] = {}
        for s in samples:
            children_dict[s.get_SampleId_field()] = len(s.get_children_of_type(SampleModel)) + 1

        batch_runs = 0
        for b, x in enumerate(batch_ids, start=1):
            qiaxpert_file_data = ",".join(qiaxpert_headers) + "\n"
            for y in range(0 + (24 * batch_runs), (24 * (batch_runs + 1))):
                qiaxpert_file_data += (f"test,{sample_ids[y]}_{children_dict[sample_ids[y]]},117,121.8,0.19,0,0.3,2.53,"
                                       f"1.94,0.69\n")
            batch_runs += 1

            file_datas[f"{TimeUtil.now_in_format('%Y-%m-%d_%Hh%Mm%S')}_1234_TEST_{plate_id}_{x}.csv"] = (
                bytes(qiaxpert_file_data, "utf-8"))

        self.callback_util.write_zip_file("Test QIA Files.zip", file_datas)
        return SapioWebhookResult(True)

    def get_valid_reagents(self, user: SapioUser) -> list[ConsumableItemModel]:
        columns: list[ReportColumn] = [ReportColumn(ConsumableItemModel.DATA_TYPE_NAME, "RecordId", FieldType.INTEGER)]
        term: RawReportTerm = RawReportTerm(ConsumableItemModel.DATA_TYPE_NAME,
                                            ConsumableItemModel.LOTNUMBER__FIELD_NAME.field_name,
                                            RawTermOperation.NOT_EQUAL_TO_OPERATOR, "%<ENTER LOT NUMBER HERE>%")
        criteria: CustomReportCriteria = CustomReportCriteria(columns, term)
        results: list[list[Any]] = (DataMgmtServer.get_custom_report_manager(user).run_custom_report(criteria)
                                    .result_table)
        if not results:
            return None
        record_ids: list[int] = [x[0] for x in results]
        return self.rec_handler.query_models_by_id(ConsumableItemModel, record_ids)
