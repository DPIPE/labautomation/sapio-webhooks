import json
from typing import Any, cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxIntegerFieldDefinition
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import ListDialogRequest, OptionDialogRequest, \
    FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, ListDialogResult, \
    OptionDialogResult, DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective, TableDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.constants import Constants
from utilities.data_models import SampleModel, PatientModel, PlateModel, RequestModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
ERROR: str = "error"
NUM_SAMPLES: str = "numSamples"
SAMPLE_TYPE: str = "sampleType"
CREATE_PARENT_BLOOD_SAMPLES: str = "createParentBloodSamples"
PLATED: str = "plated"
CREATE_ORDERS: str = "createOrders"
PATIENT: str = "patient"


class GenerateTestSamplesOldClientCallback(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager

    response_data: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)

        self.response_data = {STATUS: "", NUM_SAMPLES: "", SAMPLE_TYPE: "", CREATE_PARENT_BLOOD_SAMPLES: "0",
                              PLATED: "", CREATE_ORDERS: "", PATIENT: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result is None:
            return self.prompt_num_samples()
        else:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            self.response_data = json.loads(result.callback_context_data)
            if self.response_data[STATUS] == ERROR:
                return SapioWebhookResult(True)

            if self.response_data[STATUS] == NUM_SAMPLES:
                num_samples: str = cast(FormEntryDialogResult, result).user_response_map[NUM_SAMPLES]
                if num_samples is None or num_samples == "":
                    return SapioWebhookResult(True)
                num_samples_int = int(num_samples)
                if num_samples_int < 0 or num_samples_int > 96:
                    return SapioWebhookResult(False, "Number of samples must be between 1 and 96 inclusively.")

                self.response_data[NUM_SAMPLES] = num_samples
                return self.prompt_sample_type(context.user)

            if self.response_data[STATUS] == SAMPLE_TYPE:
                selection_list: list[str] = cast(ListDialogResult, result).selected_options_list
                if selection_list is None or len(selection_list) == 0:
                    return SapioWebhookResult(True)

                self.response_data[SAMPLE_TYPE] = selection_list[0]
                if self.response_data[SAMPLE_TYPE] == "DNA":
                    return self.prompt_yes_no_dialog(CREATE_PARENT_BLOOD_SAMPLES, "Do you want to create parent Blood "
                                                                                  "samples for these new DNA samples?")
                return self.prompt_yes_no_dialog(PLATED, "Do you want to put these samples onto a plate? (If you are "
                                                         "creating parent blood samples, the aliquots will be plated.)")

            if self.response_data[STATUS] == CREATE_PARENT_BLOOD_SAMPLES:
                self.response_data[CREATE_PARENT_BLOOD_SAMPLES] = str(cast(OptionDialogResult, result).selection)
                return self.prompt_yes_no_dialog(PLATED, "Do you want to put these samples onto a plate? (If you are "
                                                         "creating parent blood samples, the aliquots will be plated.)")

            if self.response_data[STATUS] == PLATED:
                self.response_data[PLATED] = str(cast(OptionDialogResult, result).selection)
                return self.prompt_yes_no_dialog(CREATE_ORDERS, "Do you want to create orders for the new samples?")

            if self.response_data[STATUS] == CREATE_ORDERS:
                self.response_data[CREATE_ORDERS] = str(cast(OptionDialogResult, result).selection)
                if int(self.response_data[CREATE_ORDERS]) == 0:
                    return self.create_samples()
                patients: list[PatientModel] = self.rec_handler.query_all_models(PatientModel)
                return self.prompt_patient(context, patients)

            if self.response_data[STATUS] == PATIENT:
                selected_field_map_list: list[dict[str, Any]] = (
                    cast(DataRecordSelectionResult, result).selected_field_map_list)
                if selected_field_map_list is None or len(selected_field_map_list) == 0:
                    return SapioWebhookResult(True)
                self.response_data[PATIENT] = (
                    selected_field_map_list)[0][PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name]

            return self.create_samples()

    def prompt_num_samples(self) -> SapioWebhookResult:
        form_builder: FormBuilder = FormBuilder()
        field: VeloxIntegerFieldDefinition = VeloxIntegerFieldDefinition(form_builder.get_data_type_name(),
                                                                         NUM_SAMPLES, "Number of Samples")
        field.editable = True
        field.required = True
        form_builder.add_field(field)
        temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()
        self.response_data[STATUS] = NUM_SAMPLES
        request: FormEntryDialogRequest = FormEntryDialogRequest("Create Samples",
                                                                 "Enter the number of samples to create.", temp_dt,
                                                                 callback_context_data=json.dumps(self.response_data))
        return SapioWebhookResult(True, client_callback_request=request)

    def prompt_sample_type(self, user: SapioUser) -> SapioWebhookResult:
        sample_types: list[str] = \
            [l for l in DataMgmtServer.get_picklist_manager(user).get_picklist_config_list()
             if l.pick_list_name == "Exemplar Sample Types"][0].entry_list
        self.response_data[STATUS] = SAMPLE_TYPE
        return SapioWebhookResult(True, client_callback_request=ListDialogRequest("Select Sample Type", False,
                                                                                  sample_types,
                                                                                  callback_context_data=json.dumps(
                                                                                      self.response_data)))

    def prompt_yes_no_dialog(self, status: str, msg: str) -> SapioWebhookResult:
        self.response_data[STATUS] = status
        return SapioWebhookResult(True,
                                  client_callback_request=OptionDialogRequest("Create Samples", msg, ["No", "Yes"],
                                                                              default_selection=0,
                                                                              callback_context_data=
                                                                              json.dumps(self.response_data)))

    def prompt_patient(self, context: SapioWebhookContext, patients: list[PatientModel]) -> SapioWebhookResult:
        self.response_data[STATUS] = PATIENT
        return PopupUtil.record_selection_popup(context, "Select a patient to register the orders under.",
                                                [PatientModel.FIRSTNAME__FIELD_NAME.field_name,
                                                 PatientModel.LASTNAME__FIELD_NAME.field_name,
                                                 PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name],
                                                patients, request_context=json.dumps(self.response_data))

    def create_samples(self) -> SapioWebhookResult:
        samples: list[SampleModel] = []
        samples_to_parents: dict[SampleModel, SampleModel] = {}

        # Collect all the fields from the input dialogs
        num_samples: int = int(self.response_data[NUM_SAMPLES])
        sample_type: str = self.response_data[SAMPLE_TYPE]
        plated: int = int(self.response_data[PLATED])
        create_parent_blood_samples: int = int(self.response_data[CREATE_PARENT_BLOOD_SAMPLES])
        create_orders: int = int(self.response_data[CREATE_ORDERS])

        # Create a plate if the user chose to do so
        plate: PlateModel = None
        if plated == 1:
            plate = self.rec_handler.add_model(PlateModel)
            plate.set_PlateId_field(f"{Constants.ACCESSIONING_TEST_PLATE_ID}{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TEST_PLATE_ID))[0]}")

        # Get the patient to put orders under if the user chose to do so
        patient: PatientModel = None
        try:
            patient = self.rec_handler.query_models(PatientModel,
                                                    PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                                    [self.response_data[PATIENT]])[0]
        except Exception:
            pass

        # Create all the samples
        for x in range(0, num_samples):
            # Create the samples and their parents if the user chose to do so
            parent_sample: SampleModel = None
            sample: SampleModel = None
            if create_parent_blood_samples == 1:
                parent_sample = SampleUtils.register_sample(self.rec_handler, self.acc_manager,
                                                            sample_type="Blood")
                sample = SampleUtils.create_aliquot(self.rec_handler, self.rel_man, self.acc_manager, parent_sample,
                                                    sample_type=sample_type)
                samples_to_parents[sample] = parent_sample
            else:
                sample = SampleUtils.register_sample(self.rec_handler, self.acc_manager, sample_type=sample_type)

            samples.append(sample)

            # Create the order if the user chose to do so
            order: RequestModel = None
            if create_orders == 1:
                order = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                if parent_sample is not None:
                    order.add_child(parent_sample)
                else:
                    order.add_child(sample)

                # Add the order and sample to the patient if the user chose to do so
                patient.add_child(order)
                if parent_sample is not None:
                    patient.add_child(parent_sample)
                else:
                    patient.add_child(sample)

        # Plate the samples if the user chose to do so
        if plate is not None:
            plate_location_index: int = 0
            for s in samples:
                row: str = Constants.PLATE_LOCATIONS[plate_location_index][0:1]
                col: str = Constants.PLATE_LOCATIONS[plate_location_index][1: len(Constants.PLATE_LOCATIONS[plate_location_index])]
                s.set_RowPosition_field(row)
                s.set_ColPosition_field(col)
                s.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                plate.add_child(s)
                plate_location_index += 1

        # Store and commit changes
        self.rec_man.store_and_commit()

        # Return a directive to the new samples depending on the conditions
        if plate is not None:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=FormDirective(plate.get_data_record()))

        if not create_parent_blood_samples:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=TableDirective([s.get_data_record() for s in samples]))
        else:
            return SapioWebhookResult(True, "Successfully created new samples",
                                      directive=TableDirective([s.get_data_record() for s in samples] +
                                                               [samples_to_parents[s].get_data_record() for s in
                                                                samples]))
