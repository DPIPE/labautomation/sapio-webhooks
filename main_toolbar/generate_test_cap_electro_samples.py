import random

from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import SampleModel, RequestModel, AssayDetailModel, InvoiceModel, PlateModel, \
    CapillaryElectrophoresisSampleModel, ProbeMixModel, PatientModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


NUMS_TO_PLATE_IDS: dict[int, str] = {
    0: "MLPA_[DATE]_",
    1: "MS-MLPA_[DATE]_",
    2: "HTT_[DATE]_"
}

NUMS_TO_STANDARDS: dict[int, str] = {
    0: "500 LIZ",
    1: "600 LIZ",
    2: "1200 LIZ",
    3: "ROX1000"
}


class GenerateTestCapillaryElectrophoresisSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.callback_util = CallbackUtil(context)

        # Prompt the user with a number of plates to create.
        num_plates: int | None = self.callback_util.integer_input_dialog("Number of Plates",
                                                                         "Enter the number of plates to create.",
                                                                         "Plates", max_value=4)
        if not num_plates:
            return SapioWebhookResult(True)

        # Create the samples.
        return self.create_samples(context, num_plates)

    def create_samples(self, context: SapioWebhookContext, num_plates: int) -> SapioWebhookResult:
        # Generate a new order for all the samples.
        order: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
        order.set_Comments_field("Generated from \"Generate Test Capillary Electrophoresis Samples\" button.")

        self.rec_man.store_and_commit()

        # Generate an assay detail for the order.
        order.set_RequestApproved_field(True)
        assay_detail: AssayDetailModel | None = (
            RequestUtils.generate_assay_detail(self.rec_handler, "Capillary Electrophoresis", order.record_id))
        if assay_detail:
            order.add_child(assay_detail)

        # Get the dummy patient and add it as a parent of the order.
        try:
            patient: PatientModel = (
                    self.rec_handler.query_models(PatientModel,
                                                  PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                                  ["555-55-5555"]))[0]
        except Exception:
            patient: PatientModel = self.rec_handler.add_model(PatientModel)
            patient.set_SocialSecurityNumber_field("555-55-5555")
            patient.set_FirstName_field("Dummy")
            patient.set_LastName_field("Patient")
        patient.add_child(order)

        # TODO: Figure out what processes exactly will be used here, then update accordingly.
        # Add the appropriate assay detail extension to the order.
        # extension: SNPIDAssayDetailModel = self.rec_handler.add_model(SNPIDAssayDetailModel)
        # extension.set_Priority_field("Normal")
        # extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
        # request.add_child(extension)

        # Generate the plates with unique IDs.
        plates: list[PlateModel] = []
        for x in range(num_plates):
            plate: PlateModel = self.rec_handler.add_model(PlateModel)

            index: int = random.randint(0, 2)
            plate_id: str = NUMS_TO_PLATE_IDS[index]
            val: str = (
                self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(NUMS_TO_PLATE_IDS[index])))[0]
            plate.set_PlateId_field(plate_id.replace("[DATE]", TimeUtil.now_in_format("%m%d%Y")) + val)

            plate.set_PlateRows_field(8)
            plate.set_PlateColumns_field(12)

            plates.append(plate)

        # Generate the samples along with probe mixes and capillary electrophoresis children.
        all_samples: list[SampleModel] = []
        for x, plate in enumerate(plates, start=1):
            samples: list[SampleModel] = (
                SampleUtils.register_samples(self.rec_handler, self.acc_manager, 96,
                                             sample_type="Capillary Electrophoresis Sample"))
            for y, sample in enumerate(samples):
                probe_mix: ProbeMixModel = self.rec_handler.add_model(ProbeMixModel)
                probe_mix.set_ProbeMixName_field(f"Probe Mix {1}")
                sample.add_child(probe_mix)

                cap_electro: CapillaryElectrophoresisSampleModel = (
                    self.rec_handler.add_model(CapillaryElectrophoresisSampleModel))
                cap_electro.set_InstrumentProtocol_field(f"Test Instrument Protocol {x}")
                cap_electro.set_Standard_field(NUMS_TO_STANDARDS[x - 1])
                cap_electro.set_ProbeMixName_field(probe_mix.get_ProbeMixName_field())
                sample.add_child(cap_electro)

                sample.set_StorageLocationBarcode_field(plate.get_PlateId_field())
                well_location: str = Constants.PLATE_LOCATIONS[y]
                sample.set_RowPosition_field(well_location[0:1])
                sample.set_ColPosition_field(well_location[1:len(well_location)])
                plate.add_child(sample)

            order.add_children(samples)
            all_samples.extend(samples)

        self.rec_man.store_and_commit()

        # Queue all the samples for the process.
        ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, all_samples, "Capillary Electrophoresis",
                                          request=order)
        return SapioWebhookResult(True, f"Successfully queued {len(all_samples)} for Capillary Electrophoresis.")

