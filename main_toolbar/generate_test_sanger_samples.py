import json
from typing import cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager, AccessionSystemCriteriaPojo
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, ListDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import PrimerModel, SampleModel, VariantResultModel, RequestModel, AssayDetailModel, \
    InvoiceModel, SangerAssayDetailModel
from utilities.request_utils import RequestUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_GROUP_INPUT: str = "groupInput"
STATUS_PROCESS_SELECTION: str = "processSelection"

NUM_GROUPS: str = "numGroups"

LR_VARIANT_NAME_BASE: str = "TEST LR VARIANT - "
VARIANT_NAME_BASE: str = "TEST VARIANT - "
AMPLICON_NAME_BASE: str = "TEST AMP - "


class GenerateTestSangerSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    response_data: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = DataMgmtServer.get_accession_manager(context.user)
        self.response_data = {STATUS: "", NUM_GROUPS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_data = json.loads(result.callback_context_data)
            if self.response_data[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_data[STATUS] == STATUS_GROUP_INPUT:
                try:
                    num_groups: int = int(cast(FormEntryDialogResult, result).user_response_map["Groups"])
                    self.response_data[NUM_GROUPS] = str(num_groups)
                    return self.prompt_process()
                except Exception:
                    self.response_data[STATUS] = STATUS_ERROR
                    return PopupUtil.ok_popup("Error", "Please enter an integer.",
                                              request_context=json.dumps(self.response_data))
            if self.response_data[STATUS] == STATUS_PROCESS_SELECTION:
                try:
                    process: str = cast(ListDialogResult, result).selected_options_list[0]
                except Exception:
                    self.response_data[STATUS] = STATUS_ERROR
                    return PopupUtil.ok_popup("Error", "You must select a process.",
                                              request_context=json.dumps(self.response_data))
                return self.create_samples(process)

        return self.prompt_num_samples()

    def prompt_num_samples(self) -> SapioWebhookResult:
        self.response_data[STATUS] = STATUS_GROUP_INPUT
        return PopupUtil.integer_field_popup("Number of Samples", "Enter how many groups of samples to make "
                                                                  "(groups of 4)", "Groups", 1, 1,
                                             request_context=json.dumps(self.response_data))

    def prompt_process(self) -> SapioWebhookResult:
        self.response_data[STATUS] = STATUS_PROCESS_SELECTION
        return PopupUtil.list_popup("Select Process", ["Long Range PCR", "PCR Amplification"],
                                    request_context=json.dumps(self.response_data))

    def create_samples(self, process: str) -> SapioWebhookResult:
        num_groups: int = int(self.response_data[NUM_GROUPS])

        # Create or grab all the Primer records
        variant_names: list[str] = []
        amplicon_names: list[str] = []
        for x in range(num_groups):
            variant_names.append(f"{LR_VARIANT_NAME_BASE}{x}" if process == "Long Range PCR"
                                 else f"{VARIANT_NAME_BASE}{x}")
            amplicon_names.append(f"{AMPLICON_NAME_BASE}{x}")
        primers: list[PrimerModel] = self.rec_handler.query_models(PrimerModel,
                                                                   PrimerModel.VARIANT__FIELD_NAME.field_name,
                                                                   variant_names)
        existing_variant_names: list[str] = [x.get_Variant_field() for x in primers]
        for x in range(num_groups):
            if variant_names[x] not in existing_variant_names:
                primer: PrimerModel = self.rec_handler.add_model(PrimerModel)
                primer.set_Variant_field(variant_names[x])
                primer.set_Amplicon_field(amplicon_names[x])
                primer.set_LongRange_field(process == "Long Range PCR")
                primer.set_Status_field("In use")
                existing_variant_names.append(variant_names[x])

        # Store and commit any new Primer records.
        self.rec_man.store_and_commit()

        # Create the Variant Results and samples. Also create an Order for each blood sample with a normal priority.
        requests: list[RequestModel] = []
        for x in range(num_groups):
            for y in range(4):
                request: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                request.set_Comments_field("Generated from \"Generate Test Sanger Samples\" button.")
                # [OSLO-996]: Approve the request to mimic user behavior.
                request.set_RequestApproved_field(True)

                extension: SangerAssayDetailModel = self.rec_handler.add_model(SangerAssayDetailModel)
                extension.set_Priority_field("Normal")
                extension.set_AnalysisCreationDate_field(TimeUtil.now_in_millis())
                request.add_child(extension)

                blood_sample: SampleModel = SampleUtils.register_sample(self.rec_handler, self.acc_manager,
                                                                        sample_type="Blood")
                request.add_child(blood_sample)
                archived_dna_sample: SampleModel = self.create_aliquot(blood_sample, "DNA")
                archived_dna_sample.set_Concentration_field(13.0)
                archived_dna_sample.set_ConcentrationUnits_field("ng/µL")
                archived_dna_sample.set_Volume_field(4.0)
                archived_dna_sample.set_VolumeUnits_field("µL")
                lowest_sample: SampleModel = self.create_aliquot(archived_dna_sample, "DNA")
                variant_result: VariantResultModel = self.rec_handler.add_model(VariantResultModel)
                variant_result.set_Variant_field(variant_names[x])
                variant_result.set_SuggestedVerification_field("Sanger")
                variant_result.set_PCRProgram_field("TEST PCR")
                lowest_sample.add_child(variant_result)

                requests.append(request)

        self.rec_man.store_and_commit()

        # [OSLO-996]: Add an assay detail to the orders.
        for request in requests:
            assay_detail: AssayDetailModel | None = (
                RequestUtils.generate_assay_detail(self.rec_handler, "Sanger", request.record_id))
            if assay_detail:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, request.get_RequestName_field(),
                                                  assay_detail.get_Assay_field()))
                invoice.add_child(assay_detail)
            else:
                invoice: InvoiceModel = (
                    RequestUtils.generate_invoice(self.rec_handler, request.get_RequestName_field(),
                                                  "Sanger"))
            request.add_child(invoice)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, f"Successfully created and queued samples for {process}.")

    def create_aliquot(self, parent_sample: SampleModel, sample_type: str) -> SampleModel:
        aliquot: SampleModel = self.rec_handler.add_model(SampleModel)
        aliquot.set_SampleId_field(parent_sample.get_SampleId_field() + "_1")
        aliquot.set_TopLevelSampleId_field(parent_sample.get_TopLevelSampleId_field())
        aliquot.set_TubeBarcode_field(f"HZ{self.acc_manager.accession_for_system(1, AccessionSystemCriteriaPojo(Constants.ACCESSIONING_TUBE_BARCODE))[0]}")
        aliquot.set_ExemplarSampleType_field(sample_type)
        parent_sample.add_child(aliquot)
        return aliquot
