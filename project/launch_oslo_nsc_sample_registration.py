from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.ELNService import ElnManager
from sapiopylib.rest.pojo.eln.ElnExperiment import TemplateExperimentQueryPojo, ElnTemplate, \
    InitializeNotebookExperimentPojo, ElnExperiment
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import ElnExperimentDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ProjectModel
from utilities.webhook_handler import OsloWebhookHandler


class LaunchOsloNSCSampleRegistration(OsloWebhookHandler):
    eln_manager: ElnManager
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.eln_manager = DataMgmtServer.get_eln_manager(context.user)
        self.rec_handler = RecordHandler(context)

        # Get the template for the experiment.
        query: TemplateExperimentQueryPojo = TemplateExperimentQueryPojo(latest_version_only=True,
                                                                         active_templates_only=True)
        templates: list[ElnTemplate] = context.eln_manager.get_template_experiment_list(query)
        launch: ElnTemplate | None = None
        for template in templates:
            if template.template_name == "OSLO NSC Sample Registration":
                launch = template
                break
        if launch is None:
            return SapioWebhookResult(False, display_text="No OSLO NSC Sample Registration template found.")

        # Create the experiment and include the project name along with the current date in the experiment's name.
        project: ProjectModel = self.rec_handler.wrap_model(context.data_record, ProjectModel)
        notebook_init: InitializeNotebookExperimentPojo = (
            InitializeNotebookExperimentPojo(f"NSC Sample Registration", launch.template_id,
                                             project.get_data_record()))
        experiment: ElnExperiment = self.eln_manager.create_notebook_experiment(notebook_init)

        # Return a directive to the newly created experiment.
        return SapioWebhookResult(True, directive=ElnExperimentDirective(experiment.notebook_experiment_id))
