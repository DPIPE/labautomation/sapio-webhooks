from typing import Any

from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxSelectionFieldDefinition, \
    ListMode
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ProjectModel, SampleModel, RequestModel
from utilities.oslo_exception import OsloException
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_PROCESS_PROMPT: str = "processPrompt"
STATUS_SAMPLES_SENT: str = "samplesSent"
STATUS_TABLE_PROMPT: str = "tablePrompt"

SAMPLE_PREP_WORKFLOWS: dict[str, int] = {"TruSeq Nano Sample Prep": 1, "Thruplex": 2, "TruSeq Stranded mRNA Prep": 3,
                                         "miRNA Sample Prep": 4, "Illumina DNA Sample Prep": 5, "16S": 6}


class NSCAssignToProcess(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            # Check if there's any unassigned samples registered under the project. Error to the user if there isn't.
            samples: list[SampleModel] | None = self._get_samples(context)
            if not samples:
                raise OsloException("This project has no unassigned samples under it.")

            # Prompt a table dialog of samples and let the user select processes to send them to.
            results: list[dict[str, Any]] | None = self._prompt_table_dialog(context, samples)
            if not results:
                return SapioWebhookResult(True)

            # Check if any samples were selected to be sent to NSC Sample Prep. If any were, then prompt a new dialog
            # for the user to select the sample prep workflow to send them to.
            sample_prep_selections: list[dict[str, Any]] = [x for x in results if x["Process"] == "NSC Sample Prep"]
            if sample_prep_selections:
                sample_prep_results: list[dict[str, Any]] | None = (
                    self._prompt_sample_prep_selection(sample_prep_selections))
                if not sample_prep_results:
                    return SapioWebhookResult(True)

                # Update the initial results with the new workflow values selected by the user.
                for result in sample_prep_results:
                    matching_init_result: dict[str, Any] = \
                        [x for x in results if x["SampleId"] == result["SampleId"]][0]
                    matching_init_result["Process"] = result["Workflow"]

            # Send the samples to their selected processes.
            self._send_samples(context, samples, results)

        except Exception as e:
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            raise e

        return SapioWebhookResult(True, "Successfully queued samples for processes.")

    def _get_samples(self, context: SapioWebhookContext) -> list[SampleModel] | None:
        project: ProjectModel = self.rec_handler.wrap_model(context.data_record, ProjectModel)
        self.rel_man.load_children_of_type([project], SampleModel)
        try:
            return [x for x in project.get_children_of_type(SampleModel)
                    if x.get_ExemplarSampleStatus_field() == "Logged" or
                    not x.get_ExemplarSampleStatus_field() or x.get_ExemplarSampleStatus_field() == ""]
        except Exception:
            return None

    def _prompt_table_dialog(self, context: SapioWebhookContext,
                             samples: list[SampleModel]) -> list[dict[str, Any]] | None:
        # Get the list of processes that the NSC group members can assign samples to.
        processes: list[str] | None = self.get_process_names(context)
        if not processes:
            raise OsloException("The NSC Lab Technician group is not assigned access for any processes.")

        # Create fields for the table dialog.
        sample_id_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID")
        sample_id_field.editable = False
        sample_name_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("OtherSampleId", "OtherSampleId", "Sample Name"))
        sample_name_field.editable = False
        tube_barcode_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode"))
        tube_barcode_field.editable = False
        process_field: VeloxSelectionFieldDefinition = (
            VeloxSelectionFieldDefinition("Process", "Process", "Process", ListMode.REPORT,
                                          custom_report_name="NSC Processes"))
        process_field.editable = True

        # Create the table dialog and prompt the user.
        values: list[dict[str, Any]] = [{"SampleId": x.get_SampleId_field(),
                                         "OtherSampleId": x.get_OtherSampleId_field(),
                                         "TubeBarcode": x.get_TubeBarcode_field(),
                                         "Process": ""} for x in samples]
        return self.callback.table_dialog("Select Processes", "Select a process for each sample.",
                                          [sample_id_field, sample_name_field, tube_barcode_field, process_field],
                                          values)

    def _prompt_sample_prep_selection(self, sample_prep_results: list[dict[str, Any]]) -> list[dict[str, Any]] | None:
        # Create fields for the table dialog.
        sample_id_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition("SampleId", "SampleId", "Sample ID")
        sample_id_field.editable = False
        sample_name_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("OtherSampleId", "OtherSampleId", "Sample Name"))
        sample_name_field.editable = False
        tube_barcode_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode"))
        tube_barcode_field.editable = False
        workflow_field: VeloxSelectionFieldDefinition = (
            VeloxSelectionFieldDefinition("Workflow", "Workflow", "Workflow", ListMode.LIST,
                                          pick_list_name="NSC Sample Prep Workflows"))
        workflow_field.editable = True

        # Create the table dialog and prompt the user.
        values: list[dict[str, Any]] = [{"SampleId": x["SampleId"],
                                         "OtherSampleId": x["OtherSampleId"],
                                         "TubeBarcode": x["TubeBarcode"],
                                         "Workflow": ""} for x in sample_prep_results]
        return self.callback.table_dialog("Select Workflows", "Select a Sample Prep workflow for each sample.",
                                          [sample_id_field, sample_name_field, tube_barcode_field, workflow_field],
                                          values)

    def _send_samples(self, context: SapioWebhookContext, samples: list[SampleModel],
                      results: list[dict[str, Any]]) -> None:
        # Create a dummy request since process tracking requires samples queued to be under one, and to help reduce
        # webhook execution time.
        dummy_request: RequestModel = RequestUtils.add_request(self.rec_handler,
                                                               DataMgmtServer.get_accession_manager(context.user))

        self.rec_man.store_and_commit()

        # Assign each sample to the process the user selected for it.
        processes_to_samples: dict[str, list[SampleModel]] = {}
        for i, d in enumerate(results):
            if d["Process"]:
                (processes_to_samples.setdefault(d["Process"], [])
                 .append([x for x in samples if x.get_SampleId_field() == d["SampleId"]][0]))
        for process in processes_to_samples:
            if process not in SAMPLE_PREP_WORKFLOWS.keys():
                ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, processes_to_samples[process],
                                                  process, request=dummy_request)
            else:
                ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, processes_to_samples[process],
                                                  "NSC Sample Prep", step_number=SAMPLE_PREP_WORKFLOWS[process],
                                                  request=dummy_request)
        if not processes_to_samples.keys():
            raise OsloException("No processes were selected, so no samples were sent.")

    def get_process_names(self, context: SapioWebhookContext) -> list[str] | None:
        results: list[list[Any]] | None = (
            DataMgmtServer.get_custom_report_manager(context.user).run_system_report_by_name(
                "NSC Processes")).result_table
        return None if not results else [x[0] for x in results]
