import io
import re
from typing import Any

from docx import Document
from sapiopycommons.datatype.attachment_util import AttachmentUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.constants import Constants
from utilities.data_models import AttachmentModel, ProjectModel, ProjectSubmissionFormModel
from utilities.docx_utils import DOCXUtils
from utilities.oslo_exception import OsloException
from utilities.webhook_handler import OsloWebhookHandler


class UploadSubmissionForm(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        try:
            # Prompt the user to select a project type.
            project_type: str | None = self._prompt_project_type()
            if not project_type:
                return SapioWebhookResult(True)

            # Prompt the user to upload a file for the project and parse the file according to the selected project
            # name.
            try:
                file_name, file_bytes = self.callback.request_file("Upload Submission Form File", [".docx"])
                if not file_bytes:
                    return SapioWebhookResult(True)
            except Exception:
                raise OsloException("Invalid file.")
            if project_type == "Short Read":
                # Check to make sure the file is a short read file.
                offline: re.Match[str] = re.match(Constants.RE_SHORT_READ, file_name)
                online: re.Match[str] = re.match(Constants.RE_SHORT_READ_ONLINE, file_name)
                if not offline and not online:
                    raise OsloException("File uploaded is not for a short read project.")

                # Parse the file according to the format.
                if offline:
                    field_map: dict[str, Any] = self._parse_offline_short_read_file(file_bytes)
                else:
                    # TODO: Implement this parser for S21.
                    # field_map: dict[str, Any] = self._parse_online_short_read_file(file_name, file_bytes)
                    return SapioWebhookResult(True)
            else:
                # TODO: Implement this parser for S21.
                # field_map: dict[str, Any] = self._parse_long_read_file(file_name, file_bytes)
                return SapioWebhookResult(True)

            # Create a new project submission form to update the existing one under the project.
            project: ProjectModel = self.rec_handler.wrap_model(context.data_record, ProjectModel)
            self._add_project_submission_form(project, field_map, project_type)

            # Attach the uploaded file under the project.
            attachment: AttachmentModel = (
                AttachmentUtil.create_attachment(self.context, file_name, file_bytes, AttachmentModel))
            project.add_child(attachment)

            self.rec_man.store_and_commit()
        except Exception as e:
            if isinstance(e, OsloException):
                self.callback.ok_dialog("Error", str(e))
                return SapioWebhookResult(True)
            raise e

        return SapioWebhookResult(True)

    def _prompt_project_type(self) -> str | None:
        try:
            project_types: list[str] = (
                DataMgmtServer.get_picklist_manager(self.user).get_picklist("NSC Project Type").entry_list)
        except Exception:
            raise OsloException("\"Project Type\" pick list does not exist in the system.")
        project_type: list[str] = self.callback.list_dialog("Select Project Type", project_types)
        if not project_type:
            return None
        return project_type[0]

    def _parse_offline_short_read_file(self, file_bytes: bytes) -> dict[str, Any]:
        field_map: dict[str, Any] = {}
        doc: Document = Document(io.BytesIO(file_bytes))

        # Get the field values from the table values.
        delivery_method_field_names: list[str] = ["Norstore", "NeLS project", "User HDD", "New HDD", "TSD project"]
        for x, field_name in enumerate(delivery_method_field_names):
            checkbox_values: list[str] = DOCXUtils.get_all_checkbox_values_in_row(doc.tables[4].rows[x])
            text_box_values: list[str] = DOCXUtils.get_text_box_values_from_cell(doc.tables[4].cell(x, 1))
            if checkbox_values[0] == "☒":
                field_map["DeliveryMethod"] = field_name
                if field_name == "NeLS project":
                    field_map["NeLSProjectId"] = text_box_values[0]
                elif field_name == "TSD project":
                    field_map["TSDProjectId"] = text_box_values[0]
                break

        field_map["ReferenceGenome"] = DOCXUtils.get_text_box_values_from_row(doc.tables[3].rows[3])[0]
        field_map["Funded"] = DOCXUtils.get_all_checkbox_values_in_row(doc.tables[0].rows[5])[0] == "☒"
        field_map["BioinformaticServices"] = DOCXUtils.get_all_checkbox_values_in_row(doc.tables[5].rows[0])[0] == "☒"

        field_map["ContactPerson"] = DOCXUtils.get_text_box_values_from_row(doc.tables[6].rows[0])[0]
        field_map["ContactInstitution"] = DOCXUtils.get_text_box_values_from_row(doc.tables[6].rows[1])[0]
        field_map["ContactAddress"] = DOCXUtils.get_text_box_values_from_row(doc.tables[6].rows[2])[0]
        field_map["ContactEmail"] = DOCXUtils.get_text_box_values_from_row(doc.tables[6].rows[3])[0]
        field_map["ContactTelephone"] = DOCXUtils.get_text_box_values_from_row(doc.tables[6].rows[4])[0]

        field_map["BillingInstitution"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[0])[0]
        field_map["BillingContactPerson"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[1])[0]
        field_map["BillingEmail"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[2])[0]
        field_map["BillingTelephone"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[3])[0]
        field_map["BillingAddress"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[4])[0]
        field_map["BillingPostcode"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[5])[0]
        field_map["PurchaseOrderNumber"] = DOCXUtils.get_text_box_values_from_row(doc.tables[7].rows[6])[0]

        field_map["Kontostreng"] = DOCXUtils.get_text_box_values_from_row(doc.tables[8].rows[0])[0]

        field_map["InstitutionVATNumber"] = DOCXUtils.get_text_box_values_from_row(doc.tables[9].rows[0])[0]

        return field_map

    def _parse_long_read_file(self, file_name: str, file_bytes: bytes) -> None:
        # Check to make sure the file is a long read file.
        if not re.match(Constants.RE_LONG_READ, file_name):
            raise OsloException("File uploaded is not for a long read project.")

    def _add_project_submission_form(self, project: ProjectModel, field_map: dict[str, Any],
                                     project_type: str) -> None:
        self.rel_man.load_children_of_type([project], ProjectSubmissionFormModel)
        try:
            project_submission_form: ProjectSubmissionFormModel | None = (
                project.get_children_of_type(ProjectSubmissionFormModel)[0])
        except Exception:
            project_submission_form = self.rec_handler.add_model(ProjectSubmissionFormModel)
            project.add_child(project_submission_form)
        project_submission_form.set_ProjectType_field(project_type)
        project_submission_form.set_field_values(field_map)

    def _add_attachment(self, project: ProjectModel, file_name: str, file_bytes: bytes) -> None:
        self.rel_man.load_children_of_type([project], AttachmentModel)
        if project.get_children_of_type(AttachmentModel):
            raise OsloException("A submission form has already been uploaded to this project.")
        attachment: AttachmentModel = (
            AttachmentUtil.create_attachment(self.context, file_name, file_bytes, AttachmentModel))
        project.add_child(attachment)
