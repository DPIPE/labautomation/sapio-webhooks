import io
import time
from typing import Optional, Dict

from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import MultiFileRequest
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelManager import RecordModelManager

from utilities.data_models import AttachmentModel


def query_for_report(context, file_name):
    return RecordHandler(context).query_models(AttachmentModel, AttachmentModel.FILEPATH__FIELD_NAME.field_name,
                                               [file_name])


def sleep_and_poll_report(context, file_name):
    reports = query_for_report(context, file_name)
    count = 10  # try for 10 times
    while len(reports) < 1 and count > 0:
        time.sleep(5)  # Wait for 3 seconds
        reports = query_for_report(context, file_name)
        count = count - 1
    return reports


def download_report_to_user(context, file_name, reports):
    def consume_data(chunk: bytes):
        file_data.write(chunk)

    files: Optional[Dict[str, bytes]] = {}
    for record in reports:
        with io.BytesIO() as file_data:
            # get file bytes from attachment record
            context.data_record_manager.get_attachment_data(record.get_data_record(), consume_data)
            file_data.flush()
            file_data.seek(0)
            file_bytes = file_data.read()
            files.update({file_name: file_bytes})
    return SapioWebhookResult(True, client_callback_request=MultiFileRequest(files))


def update_and_download_new_report(context, existing_report, reports):
    def consume_data(chunk: bytes):
        file_data.write(chunk)

    temp_report = reports[0]
    temp_report_record = temp_report.get_data_record()
    with io.BytesIO() as file_data:
        context.data_record_manager.get_attachment_data(temp_report_record, consume_data)
        file_data.flush()
        file_data.seek(0)
        file_bytes = file_data.read()
        context.data_record_manager.set_attachment_data(existing_report.get_data_record(),
                                        existing_report.get_FilePath_field(),
                                        io.BytesIO(file_bytes))
        temp_report.delete()
        RecordModelManager(context.user).store_and_commit()
        return SapioWebhookResult(True, client_callback_request=MultiFileRequest(
            {existing_report.get_FilePath_field(): file_bytes}))
