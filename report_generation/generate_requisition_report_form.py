import base64
import json
import re
import time
import traceback
from datetime import datetime
from io import BytesIO
from typing import Any, Dict

from PIL import Image
from barcode.codex import Code128
from barcode.writer import ImageWriter
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.reportbuilder.ReportBuilderEntryContext import AbstractReportEntryDataContext, \
    StaticTextEntryDataContext, RbEntryType
from sapiopylib.rest.pojo.reportbuilder.VeloxReportBuilder import ReportDataContext
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult, WriteFileResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from report_generation import report_gen_util
from utilities.data_models import RequestModel, AttachmentModel, ClinicalRequisitionModel, \
    PatientModel, RequesterModel
from utilities.webhook_handler import OsloWebhookHandler

REQUISITION_REPORT_TEMPLATE_ID = "RequisitionReportForm"
EXECUTION_STATUS = "Status"
INITIAL_CALL = "Initial"
REPORT_RUNNING_IN_BACKGROUND = "ReportRunningInBackground"
DOWNLOAD_EXISTING = "DownloadExisting"
REGENERATION = "Regeneration"
YES = "YES"
NO = "NO"
TEMP_REPORT_NAME = "TempReportName"


class RequisitionReportFormGenerator(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        client_callback = context.client_callback_result
        if client_callback and client_callback.user_cancelled is True:
            return SapioWebhookResult(True)

        # Using callback context data to store the status of execution
        response_map: Dict[Any, Any] = {}
        if client_callback and client_callback.callback_context_data:
            response_map = json.loads(client_callback.callback_context_data)
        else:
            response_map[EXECUTION_STATUS] = INITIAL_CALL

        if isinstance(client_callback, WriteFileResult):
            return SapioWebhookResult(True)
        if isinstance(client_callback, OptionDialogResult):
            if client_callback.button_text is None or client_callback.button_text == 'OK':  # Error popup
                return SapioWebhookResult(True)
            if client_callback.button_text == 'Yes' and response_map[EXECUTION_STATUS] == DOWNLOAD_EXISTING:
                response_map[DOWNLOAD_EXISTING] = YES
            else:
                response_map[DOWNLOAD_EXISTING] = NO

        try:
            # Get the Clinical Requisition from context
            record = context.data_record
            requisition_model: ClinicalRequisitionModel = None
            if record:
                requisition_model = self.inst_man.add_existing_record_of_type(record, ClinicalRequisitionModel)
            if not requisition_model:
                raise Exception("Clinical Requisition is not found to generate 'Requisition Report Form'.")

            # Generate file name
            file_name = "Requisition_Report_Form_" + "_" + str(requisition_model.record_id) + ".pdf"

            # Get existing report if available
            self.rel_man.load_children_of_type([requisition_model], AttachmentModel)
            existing_attachments = requisition_model.get_children_of_type(AttachmentModel)
            existing_report = None
            if existing_attachments:
                existing_report = existing_attachments[0]

            # If report generation running in background already, query and download it
            if response_map[EXECUTION_STATUS] == REPORT_RUNNING_IN_BACKGROUND:
                if response_map[REGENERATION] == YES:  # change file name to temp report name
                    file_name = response_map[TEMP_REPORT_NAME]

                reports = report_gen_util.sleep_and_poll_report(context, file_name)
                if not reports:
                    return PopupUtil.option_popup("Info",
                                                  "Report is getting generated...\nPlease wait for few seconds.",
                                                  ["Okay"], 0, False, request_context=json.dumps(response_map))

                if response_map[REGENERATION] == YES:
                    return report_gen_util.update_and_download_new_report(context, existing_report, reports)
                else:  # First time generation
                    return report_gen_util.download_report_to_user(context, file_name, reports)

            if response_map[EXECUTION_STATUS] == INITIAL_CALL:
                if existing_report:
                    response_map[EXECUTION_STATUS] = DOWNLOAD_EXISTING
                    response_map[DOWNLOAD_EXISTING] = ""
                    return PopupUtil.option_popup("Requisition Report Form available already",
                                                  "Do you want to download the existing form ?\n\n",
                                                  ["Yes", "No, Regenerate it"], 0, True,
                                                  request_context=json.dumps(response_map))

            if response_map[EXECUTION_STATUS] == DOWNLOAD_EXISTING:
                if response_map[DOWNLOAD_EXISTING] == YES and existing_report:
                    return report_gen_util.download_report_to_user(context, file_name, [existing_report])
                if response_map[DOWNLOAD_EXISTING] == NO and existing_report:
                    response_map[EXECUTION_STATUS] = REGENERATION

            # Perform the substitutions on report template
            report_man = DataMgmtServer.get_report_manager(context.user)
            if response_map[EXECUTION_STATUS] == REGENERATION:
                # report needs to be re-generated
                response_map[REGENERATION] = YES
                file_name = "TempRequisitionReport" + datetime.today().strftime("%Y%m%d_%H%M%S") + ".pdf"
                response_map[TEMP_REPORT_NAME] = file_name

                report_data_context = perform_substitutions(self, file_name, report_man, requisition_model,
                                                            context, existing_report)
            else:
                response_map[REGENERATION] = NO
                report_data_context = perform_substitutions(self, file_name, report_man, requisition_model,
                                                            context, existing_report)

            # Invoke report generation
            report_man.generate_report_pdf(REQUISITION_REPORT_TEMPLATE_ID, AttachmentModel.DATA_TYPE_NAME.__str__(),
                                           report_data_context)

            # Wait for 5 seconds
            time.sleep(5)
            if response_map[EXECUTION_STATUS] == INITIAL_CALL:
                # Query for the generated report
                reports = report_gen_util.sleep_and_poll_report(context, file_name)
                if not reports:
                    response_map[EXECUTION_STATUS] = REPORT_RUNNING_IN_BACKGROUND
                    return PopupUtil.option_popup("Info",
                                                  "Report is getting generated...\nPlease wait for few seconds.",
                                                  ["Okay"], 0, False, request_context=json.dumps(response_map))

                return report_gen_util.download_report_to_user(context, file_name, reports)

            if response_map[EXECUTION_STATUS] == REGENERATION and response_map[REGENERATION] == YES and existing_report:
                # Change file name to temp report name
                file_name = response_map[TEMP_REPORT_NAME]
                reports = report_gen_util.sleep_and_poll_report(context, file_name)
                if not reports:
                    response_map[EXECUTION_STATUS] = REPORT_RUNNING_IN_BACKGROUND
                    return PopupUtil.option_popup("Info",
                                                  "Report is getting generated...\nPlease wait for few seconds.",
                                                  ["Okay"], 0, False, request_context=json.dumps(response_map))

                return report_gen_util.update_and_download_new_report(context, existing_report, reports)

        except Exception as exception:
            self.logger.error("Error ({user:s}):\n{trc:s}"
                              .format(user=context.user.username, trc=traceback.format_exc()))
            return PopupUtil.display_ok_popup("ERROR", str(exception))


def perform_substitutions(self, file_name, report_man, requisition,
                          context, existing_report):
    # Get the report entry info list
    entry_info_list = report_man.get_report_entry_info_list(REQUISITION_REPORT_TEMPLATE_ID)
    entry_data_context_map: dict[str, AbstractReportEntryDataContext] = {}

    request, requester, patient = get_required_entities(context, requisition, self)

    # Populate the entry_data_context_map
    for entry_info in entry_info_list:
        entry_data_context_map[entry_info.get_entry_id()] = entry_info.entry_data_context

    for entry in entry_info_list:
        # if this is not a static HTML entry, it can't be populated. skip
        if RbEntryType.STATIC_TEXT is not entry.entry_type:
            continue

        # Handle static entries
        static_text_entry: StaticTextEntryDataContext = entry.entry_data_context
        html_string: str = static_text_entry.static_html
        search_pattern = re.compile(r'\[(.*?)\]')
        replacement_lookup_table = []
        if html_string is None:
            continue

        # Extract all the tags from the HTML
        tags_in_text = re.findall(search_pattern, html_string)
        for tag in tags_in_text:
            if "TAG" in tag:
                value_to_replace = tag_switch(tag, requisition, requester, patient)
                replacement_lookup_table.append([tag, value_to_replace])
            else:
                # It will be in the format of [DataType:DataField]
                split_tags = tag.split(':')
                if len(split_tags) < 2:
                    continue
                data_type = split_tags[0]
                data_field = split_tags[1]
                lookup_value = None
                # Get data from records to replace tag values
                if "Patient" == data_type and patient:
                    lookup_value = patient.get_field_value(data_field)
                if "ClinicalRequisition" == data_type and requisition:
                    lookup_value = requisition.get_field_value(data_field)
                if "Requester" == data_type and requester:
                    lookup_value = requester.get_field_value(data_field)
                if "Date" in data_field and lookup_value:
                    seconds = lookup_value / 1000
                    date_object = datetime.fromtimestamp(seconds)
                    date_value = date_object.strftime("%d %b %Y")
                    replacement_lookup_table.append([tag, date_value])
                else:
                    replacement_lookup_table.append([tag, lookup_value])

        # we now have a complete lookup table (a list with entries in ["original tag", "value to replace it with"] format
        for replacement_pair in replacement_lookup_table:
            if replacement_pair:
                if replacement_pair[1]:
                    strip = str(replacement_pair[1]).strip("[").strip("]")
                    html_string = html_string.replace("[" + replacement_pair[0] + "]", strip)
                else:
                    html_string = html_string.replace("[" + replacement_pair[0] + "]", "N/A")

        # update the entry by setting the context map
        static_text_entry.static_html = html_string
        entry.entry_data_context = static_text_entry
        entry_data_context_map[entry.get_entry_id()] = entry.entry_data_context

    if existing_report:
        return ReportDataContext({}, entry_data_context_map, None, file_name)
    else:
        return ReportDataContext({}, entry_data_context_map, requisition.get_data_record(), file_name)


def get_required_entities(context, requisition_model, self):
    # Load Order and Patient records
    path: RelationshipPath = RelationshipPath()
    path: RelationshipPath = path.parent_type(RequestModel).parent_type(PatientModel)
    self.rel_man.load_path(self.inst_man.unwrap_list([requisition_model]), path)
    # Get Order, Patient
    request = requisition_model.get_parent_of_type(RequestModel)
    if not request:
        raise Exception("Order not found for the clinical requisition.")
    patient = request.get_parent_of_type(PatientModel)
    if not patient:
        raise Exception("Patient not found for the clinical requisition.")
    # Find requestor based on “HPR Number” found on order
    hpr_number = request.get_HPRNumber_field()
    if not hpr_number:
        raise Exception("HPR Number not present in the linked Order record.")
    requesters_found = RecordHandler(context).query_models(RequesterModel,
                                                           RequesterModel.HPRNUMBER__FIELD_NAME.field_name,
                                                           [hpr_number])
    if not requesters_found:
        raise Exception(f"Requester not found in the system for the HPR number {hpr_number}.")
    requester = requesters_found[0]
    return request, requester, patient


def tag_switch(tag, requisition, requester, patient):
    if "BARCODE_TAG" in tag:
        split_tags = tag.split(':')
        if len(split_tags) == 3:
            data_type = split_tags[1]
            data_field = split_tags[2]
            value_for_barcode = None
            if "Patient" == data_type and patient:
                value_for_barcode = patient.get_field_value(data_field)
            if "ClinicalRequisition" == data_type and requisition:
                value_for_barcode = requisition.get_field_value(data_field)
            if "Requester" == data_type and requester:
                value_for_barcode = requester.get_field_value(data_field)

            if value_for_barcode:
                # Generate the barcode
                barcode_bytes = generate_barcode_bytes(str(value_for_barcode), 0.8, 0.2)

                # Convert the barcode bytes to a base64 string
                barcode_base64 = base64.b64encode(barcode_bytes).decode('utf-8')

                # Create the HTML content with the embedded barcode image
                html_content = f'<img src="data:image/png;base64,{barcode_base64}" alt="{data_field} barcode">'''
                return html_content
    return None


def generate_barcode_bytes(data, height_cm, width_factor):
    # Create a custom ImageWriter with text disabled
    dpi = 300
    writer = ImageWriter()
    writer.text = False
    writer.set_options({
        "module_height": height_cm,  # Use target height for barcode module height
        "module_width": width_factor,
        "write_text": False,
        "dpi": dpi
    })

    # Create a Code128 barcode with the custom writer
    barcode = Code128(data, writer=writer)

    # Create an in-memory bytes buffer
    buffer = BytesIO()

    # Write the barcode to the buffer
    barcode.write(buffer)

    # Reset buffer position to the start
    buffer.seek(0)

    # Load the image with PIL
    image = Image.open(buffer)

    # Resize the image
    aspect_ratio = image.width / image.height
    target_height_pixels = int(height_cm * dpi / 2.54)
    new_width = int(target_height_pixels * aspect_ratio)
    resized_image = image.resize((new_width, target_height_pixels), Image.Resampling.LANCZOS)

    # Save the resized image to a new buffer
    output_buffer = BytesIO()
    resized_image.save(output_buffer, format='PNG', dpi=(dpi, dpi))
    barcode_bytes = output_buffer.getvalue()
    return barcode_bytes