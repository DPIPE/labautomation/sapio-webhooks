import re
from datetime import datetime
from typing import Dict, List

from sapiopycommons.eln.experiment_handler import ExperimentHandler
from sapiopycommons.general.popup_util import PopupUtil
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.DataRecord import DataRecord
from sapiopylib.rest.pojo.reportbuilder.ReportBuilderEntryContext import AbstractReportEntryDataContext, RbEntryType, \
    StaticTextEntryDataContext
from sapiopylib.rest.pojo.reportbuilder.VeloxReportBuilder import ReportDataContext
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from report_generation.report_gen_util import sleep_and_poll_report
from utilities.data_models import AttachmentModel, SampleModel, OGMRunParametersModel, ProjectModel, \
    AssignedProcessModel
from utilities.utils import initialize_entry
from utilities.webhook_handler import OsloWebhookHandler

"""
Author : Aryan Singh
Ticket : OSLO-839
Description : ELN Entry toolbar button and on save rule that generates nsc report
"""


class GenerateNSCReport(OsloWebhookHandler):
    # process names
    OGM_PROCESS_NAME = "OGM"

    # Entry Names
    SAMPLES_ENTRY = "Samples"
    NSC_REPORT_ENTRY = "NSC Report"

    # RB Template Ids
    NSC_REPORT_TEMPLATE = "NSC Reporting OGM"

    # Map Keys
    PROJECT_NAME_FIELD = "Project:ProjectName"
    SAMPLE_NAMES_FIELD = "Sample:OtherSampleId"
    ANALYSIS_PERFORMED_FIELD = "OGMRunParameters:AnalysisType"
    ANALYSIS_PERFORMED_BY_FIELD = "VeloxUser:Username"
    SOFTWARE_USED_FIELD = "OGMRunParameters:OgmAccessVersion"
    DATE_FIELD = "CurrentDate"

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        if context.client_callback_result:
            return SapioWebhookResult(True)

        # load/retrieve all necessary data
        exp_handler: ExperimentHandler = ExperimentHandler(context)
        samples_step = exp_handler.get_step(self.SAMPLES_ENTRY, True)
        samples_records: List[DataRecord] = samples_step.get_records()
        samples: List[SampleModel] = self.inst_man.add_existing_records_of_type(samples_records, SampleModel)
        self.rel_man.load_children_of_type(samples, OGMRunParametersModel)
        self.an_man.load_ancestors_of_type([sample.backing_model for sample in samples], ProjectModel.DATA_TYPE_NAME)
        self.rel_man.load_parents_of_type(samples, AssignedProcessModel)

        # verify samples ap is only in process for the current process
        error = []
        for sample in samples:
            aps = sample.get_parents_of_type(AssignedProcessModel)
            for ap in aps:
                if "In Process" in ap.get_Status_field() and ap.get_ProcessName_field() != self.OGM_PROCESS_NAME:
                    error.append(sample.get_OtherSampleId_field())
        if error:
            return PopupUtil.ok_popup("Error", "Following samples are in progress in other processes : " +
                                      " ,".join(error))

        # get project names
        projects = set()
        for sample in samples:
            if sample.get_Classification_field() == "Diagnostics":
                projects.add("OGM")
            else:
                curr_projects = self.an_man.get_ancestors_of_type(sample.backing_model, ProjectModel.DATA_TYPE_NAME)
                if curr_projects:
                    projects.add(self.inst_man.wrap(curr_projects.pop(), ProjectModel).get_ProjectName_field())
        projects = list(projects)
        project_names = " ,".join(projects)
        if not project_names:
            project_names = "OGM"

        # get sample names
        sample_names = [sample.get_OtherSampleId_field() for sample in samples]
        sample_names = " ,".join(sample_names)

        # get run analysis
        run_analysis = set()
        software_used = set()
        for sample in samples:
            ogm_params: List[OGMRunParametersModel] = sample.get_children_of_type(OGMRunParametersModel)
            for ogm_param in ogm_params:
                run_analysis.add(ogm_param.get_AnalysisType_field())
                software_used.add(ogm_param.get_OgmAccessVersion_field())
        run_analysis = list(run_analysis)
        run_analysis = " ,".join(run_analysis)
        software_used = list(software_used)
        software_used = " ,".join(software_used)
        current_date = datetime.today().strftime("%Y-%m-%d")

        # build out the substitution map
        substitutions = {self.PROJECT_NAME_FIELD: project_names, self.SAMPLE_NAMES_FIELD: sample_names,
                         self.ANALYSIS_PERFORMED_FIELD: run_analysis, self.ANALYSIS_PERFORMED_BY_FIELD:
                             context.user.username, self.SOFTWARE_USED_FIELD: software_used, self.DATE_FIELD:
                             current_date}

        # invoke report generation
        file_name = context.eln_experiment.notebook_experiment_name.replace(" ", "_") + ".pdf"
        report_man = DataMgmtServer.get_report_manager(context.user)
        entry_info_list = report_man.get_report_entry_info_list(self.NSC_REPORT_TEMPLATE)
        report_data_context = self.__perform_substitutions(substitutions, entry_info_list, file_name)
        report_man.generate_report_pdf(self.NSC_REPORT_TEMPLATE, AttachmentModel.DATA_TYPE_NAME.__str__(),
                                       report_data_context)

        # get the attachment entry
        attachment_entry = exp_handler.get_step(self.NSC_REPORT_ENTRY, True)
        attachment_records = attachment_entry.get_records()
        attachment_models = self.inst_man.add_existing_records_of_type(attachment_records, AttachmentModel)
        for attachment in attachment_models:
            attachment.delete()
        self.rec_man.store_and_commit()

        # query attachment record and add as child to entry
        report = sleep_and_poll_report(context, file_name).pop()
        initialize_entry(attachment_entry.eln_entry, context)
        report.set_FilePath_field(file_name)
        self.rec_man.store_and_commit()
        attachment_entry.set_records([report.get_data_record()])
        return SapioWebhookResult(True, eln_entry_refresh_list=[attachment_entry.eln_entry],
                                  refresh_notebook_experiment=True)

    @staticmethod
    def __perform_substitutions(substitutions: Dict[str, str], entry_info_list, file_name):
        entry_data_context_map: [Dict[str, AbstractReportEntryDataContext]] = {}
        # get the existing report builder context. note: this context is NOT contained in the entry_data_context_map
        # that we send back to veloxity. instead, the context is found in report_entry_info_list.
        for entry in entry_info_list:
            # initialize entry_data_context_map for this entry
            entry_data_context_map[entry.get_entry_id()] = entry
            # if this is not a static HTML entry, it can't be populated. skip
            if RbEntryType.STATIC_TEXT is not entry.entry_type:
                continue
            # else, perform substitutions. macros are defined in double brackets.
            static_text_entry: StaticTextEntryDataContext = entry.entry_data_context
            html: str = static_text_entry.static_html
            if html is None:
                continue
            # replace data
            for group in re.findall("\[\w+:?\w+]", html):
                macro: str = group[1: len(group) - 1]
                if macro in substitutions.keys():
                    html = html.replace(group, substitutions[macro])
                else:
                    html = html.replace(group, "")
            # update the entry by setting the context map
            static_text_entry.static_html = html
            entry.entry_data_context = static_text_entry
            entry_data_context_map[entry.get_entry_id()] = entry
        return ReportDataContext({}, entry_data_context_map, None, file_name)
