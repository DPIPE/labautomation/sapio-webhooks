import json
from typing import Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ddPCRAssayModel
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_UNAPPROVED: str = "unapproved"


class ApproveControlDesigns(OsloWebhookHandler):
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_UNAPPROVED:
                return SapioWebhookResult(True)

        # Get each ddPCR Assay record.
        ddPCRAssays: list[ddPCRAssayModel] = self.rec_handler.wrap_models(context.data_record_list, ddPCRAssayModel)

        # For each record, set their "Control Design Approved" fields to True if their "Positive Control" field isn't
        # blank. Otherwise, append it to the unapproved assays list to show to the user.
        unapproved_assays: list[dict[str, Any]] = []
        for ddPCRAssay in ddPCRAssays:
            if ddPCRAssay.get_PositiveControl_field():
                ddPCRAssay.set_ControlDesignApproved_field(True)
            else:
                unapproved_assays.append({
                    "ddPCRAssay": f"{ddPCRAssay.get_Gene_field()} {ddPCRAssay.get_Variant_field()}"
                })

        self.rec_man.store_and_commit()

        if len(unapproved_assays) > 0:
            self.response_map[STATUS] = STATUS_UNAPPROVED
            return PopupUtil.table_popup("Warning", "The following ddPCR Assays do not have Positive Controls assigned"
                                                    " to them, so their control designs could not be approved.",
                                         [VeloxStringFieldDefinition("ddPCRAssay", "ddPCRAssay", "ddPCR Assay")],
                                         unapproved_assays, request_context=json.dumps(self.response_map))

        return SapioWebhookResult(True)
