import os
from sapiopycommons.general.time_util import TimeUtil
from sapiopylib.rest.WebhookService import WebhookConfiguration, WebhookServerFactory
from waitress import serve

from assay_detail.select_indication import SelectIndication
from batch.add_instruments import AddInstruments
from batch.add_reagents import AddReagents
from batch.cancel_batches import CancelBatches
from batch.check_samples_for_nipt import CheckSamplesForNIPT
from main_toolbar.generate_test_cap_electro_samples import GenerateTestCapillaryElectrophoresisSamples
from main_toolbar.generate_test_nsc_sample_file import GenerateTestNSCSampleFile
from main_toolbar.generate_test_ogm_samples import GenerateTestOGMSamples
from onsave.assay_detail.copy_probe_mix_to_sample_and_assay_detail_from_analysis_config import \
    CopyProbeMixToSampleAndAssayDetailFromAnalysisConfig
from onsave.external_lab_results.check_external_lab_results import CheckExternalLabResults
from onsave.sample.generate_sample_name import GenerateSampleName
from onsave.update_plate_id_on_save import UpdatePlateIDOnSave
from primer.create_primer import CreatePrimer
from primer.initiate_primer_design import InitiatePrimerDesign
from project.upload_submission_form import UploadSubmissionForm
from request.remove_assay import RemoveAssay
from sample.InHouse.perform_dilution_in_house import PerformDilutionInHouse
from sample.Promega.perform_dilution_promega import PerformDilutionPromega
from sample.generate_test_nipt_files import GenerateTestNIPTFiles
from sample.lgDNA.update_days_after_labeling import UpdateDaysAfterLabeling
from sample.list_of_all_promega_and_in_house_types import ListOfAllPromegaAndInHouseTypes
from sample.update_days_after_sampling import UpdateDaysAfterSampling
from workflows.acgh.create_rows_for_microarray_scan import CreateRowsForMicroarrayScanning
from workflows.acgh.generate_array_id_for_samples import SortSamplesAndGenerateArrayIds
from workflows.acgh.generate_dilution_worksheet import GenerateDilutionWorksheetForaCGH
from workflows.acgh.generate_nxclinical_dec_file import PopulateNxClinicalDescStep, GenerateNxclinicalDescriptionFile
from workflows.acgh.populate_dna_reconcentration_entry import PopulateDNAReconcentrationEntry
from workflows.acgh.populate_pipetting_details import PopulatePipettingDetailsSteps
from workflows.capillary_electrophoresis.copy_probe_mixes import CopyProbeMixes
from workflows.capillary_electrophoresis.create_standard_formamide_tables import CreateStandardFormamideTables
from workflows.capillary_electrophoresis.generate_plate_ids import GeneratePlateIDs
from workflows.capillary_electrophoresis.print_abi_input_files import PrintABIInputFiles
from workflows.capillary_electrophoresis.print_merged_plates import PrintMergedPlates
from workflows.common.initial_plate_generation import InitialPlateGeneration
from workflows.fragment_analysis.add_controls_pcr_preparation import AddControlsPcrPreparation
from workflows.fragment_analysis.add_reagents_as_per_fragment_analysis_kit import AddReagentsAsPerFragmentAnalysisKit
from workflows.fragment_analysis.generate_dilution_file import GenerateDilutionFile
from workflows.fragment_analysis.generate_file_for_reaction_mix_recipie import GenerateFileForReactionMixRecipie
from workflows.common.update_samples_position import UpdateSamplesPosition
from workflows.idreader.requeue_samples import RequeueSamples
from workflows.nsc._10nm_dilution_worklfow.generate_normalization_worksheet import GenerateNormalizationSheet
from workflows.nsc._10nm_dilution_worklfow.set_normalization_table_fields import SetNormalizationTableFields
from workflows.nsc.final_qc.calculate_molarity import CalculateMolarity
from workflows.nsc.generate_nsc_sample_prep_pipetting_worksheet import GenerateNSCSamplePrepPipettingWorksheet
from workflows.nsc.generate_nsc_sample_prep_robot_file import GenerateNSCSamplePrepRobotFile
from workflows.nsc.miseq.update_sample_sheet import MiSeqSampleSheetUpdater
from workflows.nsc.sixteenS.autoplate_nsc_sixteenS_samples import AutoplateNSCSixteenSSamples
from workflows.nsc.sixteenS.copy_pooling_fields import CopyPoolingFields
from workflows.nsc.sixteenS.copy_quant_it_concentrations import CopyQuantITConcentrations
from workflows.nsc.sixteenS.generate_nsc_pooling_worksheet import GenerateNSCPoolingWorksheet
from workflows.nsc.sixteenS.generate_nsc_quant_it_worksheet import GenerateNSCQuantITWorksheet
from workflows.nsc.sixteenS.generate_nsc_sixteenS_robot_file import GenerateNSCSixteenSRobotFile
from workflows.nsc.sixteenS.pooling_dialog import PoolingDialog
from workflows.ogm.blood_aliquot_preparation.add_wbc_count_measurement import AddWbcCountMeasurement
from workflows.ogm.blood_aliquot_preparation.remove_experiment_details_without_sample_id import \
    RemoveExperimentDetailsWithoutSampleId
from workflows.ogm.nsc_reporting.associate_experiment_to_NSC_project import AssociateExperimentToNSCProject
from workflows.ogm.blood_aliquot_preparation.check_to_enforce_wbc_count_measurement import \
    CheckToEnforceWBCCountMeasurement
from workflows.ogm.load_chip_and_run_ogm.generate_ogm_chip_worksheet import GenerateOgmChipWorksheet
from workflows.ogm.isolation_of_hmw_gdna.generate_pipetting_worksheet import GeneratePipettingWorkSheet
from workflows.ogm.isolation_of_hmw_gdna.populate_hemo_cell_cue_count import PopulateHemoCellCueCount
from workflows.ogm.isolation_of_hmw_gdna.qubit_calculations_and_sample_routing import QubitCalculations, \
    QubitSampleRouting
from workflows.ogm.blood_aliquot_preparation.remove_replicates_from_going_to_next_step import \
    RemoveReplicatesFromGoingToNextStep
from workflows.ogm.load_chip_and_run_ogm.set_chip_details_using_well_elements import SetChipDetailsUsingWellElements
from workflows.ogm.blood_aliquot_preparation.set_fields_to_required_for_diagnostic_samples import \
    SetFieldsToRequiredForDiagnosticSamples
from workflows.ogm.isolation_of_hmw_gdna.set_target_volume import SetTargetVolume
from workflows.ogm.blood_aliquot_preparation.set_wbc_count_measurement import SetWbcCountMeasurement
from ddPCR_assay.approve_control_designs import ApproveControlDesigns
from ddPCR_assay.approve_ddpcr_assays import ApproveDDPCRAssays
from entry_toolbar.generate_quantasoft_input_files import GenerateQuantasoftInputFiles
from entry_toolbar.generate_quantstudio_input_files import GenerateQuantStudioInputFiles
from entry_toolbar.add_samples_to_plate_ngs_snp_id import AddSamplesToPlateNgsSnpId
from entry_toolbar.generate_hamilton_file_ngs_snp_id import GenerateHamiltonFileNgsSnpId
from entry_toolbar.parse_quantit_qc_output_file import QuantitQcOutputFileParser
from entry_toolbar.populate_ddPCR_plate import PopulateDDPCRPlate
from entry_toolbar.populate_taqman_assay_plate import PopulateTaqmanAssayPlate
from entry_toolbar.register_start_stop_time import RegisterStartStopTime
from family.generate_pedigree_image import GeneratePedigreeImage
from family.override_family_size_check_handler import OverrideFamilySizeCheckHandler
from main_toolbar.generate_test_ddpcr_samples import GenerateTestDDPCRSamples
from main_toolbar.generate_test_sanger_samples import GenerateTestSangerSamples
from main_toolbar.generate_test_snp_id_samples import GenerateTestSNPIDSamples
from main_toolbar.generate_test_wes_samples import GenerateTestWESSamples
from main_toolbar.lookup_requester import LookupRequester
from onsave.create_reagent_plate_columns import CreateReagentPlateColumns
from onsave.queue_ddpcr_samples import QueueDDPCRSamples
from onsave.queue_suggested_verification_samples import QueueSuggestedVerificationSamples
from onsave.update_order_name import UpdateOrderName
from patient.create_order_and_clinical_requisition import CreateOrderAndClinicalRequisition
from pedigree_configuration.edit_included_family_memebers import EditIncludedFamilyMembers
from pedigree_configuration.related_variant_list import RelatedVariantList
from primer.edit_gene import EditGene
from project.launch_oslo_nsc_sample_registration import LaunchOsloNSCSampleRegistration
from project.nsc_assign_to_process import NSCAssignToProcess
from report_generation.generate_nsc_report import GenerateNSCReport
from report_generation.generate_requisition_report_form import RequisitionReportFormGenerator
from request.receive_samples import ReceiveSamples
from request.register_patient import RegisterPatient
from request.select_samples import SelectSamples
from sample.dequeue_samples import DequeueSamples
from sample.generate_barcodes_based_on_type import GenerateBarcodesBasedOnType
from sample.generate_test_id_reader_file import GenerateTestIDReaderFile
from sample.test_get_patients_from_samples import TestGetPatientsFromSamples
from scheduled.send_invoices import SendInvoices
from probe_result.review_probe_results import ReviewProbeResults
from workflows.acgh.promega_lots import ACGHPromegaLots, UpdateSampleStepWithPromegaControls, \
    AddPromegaLotsToAppendControlsStep
from workflows.acgh.sample_dilution_init import ACGHSampleDilutionSet

from utilities.workflow_utils import SendSamplesToSNPID, PopulateCompletedTests
from variant_result.copy_variant_to_family import CopyVariantResultToFamily
from workflows.SureSelectXTLibraryPreparation.generate_master_mix_file import GenerateMastermixFiles
from workflows.SureSelectXTLibraryPreparation.generate_robot_file import GenerateRobotFiles
from workflows.SureSelectXTLibraryPreparation.plate_samples import PlateSamples
from workflows.SureSelectXTLibraryPreparation.populate_mastermix_table import PopulateMastermixTables
from workflows.SureSelectXTLibraryPreparation.recalculate_aliquots_source_volume_to_use import \
    RecalculateAliquotsSourceVolumeToUse
from workflows.ogm.labelling_and_staining.generate_pipetting_worksheet import GeneratePipettingWorksheet
from workflows.ddpcr.create_dilution_and_mastermix_worksheet import CreateDilutionAndMasterMixWorksheet
from workflows.instrument_booking.autofill_reserved_instruments import AutoFillReservedInstruments
from invoice.create_invoices import CreateInvoices
from main_toolbar.generate_test_samples_old_client_callback import GenerateTestSamplesOldClientCallback
from main_toolbar.generate_test_wgs_samples import GenerateTestWGSSamples
from main_toolbar.set_wgs_low_concentration_threshold import SetWGSLowConcentrationThreshold
from reclassification_request.accept_reject_reclassification_requests import AcceptReclassificationRequests, \
    RejectReclassificationRequests
from reclassification_request.show_reclassification_request_form_view import ShowReclassificationRequestFormView

from request.check_sample_concentrations_for_wgs import CheckSampleConcentrationsForWGS
from sample.add_sequencing_metadata import AddSequencingMetadata
from sample.show_sample_families import ShowSampleFamilies
from sdms.sdms_file_handler import SDMSFileHandler
from workflows.oslo_nsc_sample_registration.complete_nsc_sample_registration import CompleteNSCSampleRegistration
from workflows.refresh import Refresh
from workflows.rna_isolation.generate_rna_iso_qiaxpert_input_file import GenerateRnaIsoQiaxpertInputFile
from workflows.rna_isolation.parse_rna_iso_qiaxpert_results_file import ParseRnaIsoQiaxpertResultsFile
from workflows.sanger.generate_biomek_files import GenerateBiomekFiles
from workflows.sanger.long_range_pcr.check_long_range_pcr_batches import CheckLongRangePCRBatches
from workflows.sanger.pcr_amplification.add_samples_to_experiment import AddSamplesToExperiment
from workflows.sanger.long_range_pcr.long_range_pcr_plate_generation import LongRangePCRPlateGeneration
from workflows.sanger.pcr_amplification.pcr_amplification_plate_generation import PCRAmplificationPlateGeneration
from workflows.snp_id.analyze_taq_man_qc_results import AnalyzeTaqManQCResults
from workflows.snp_id.generate_test_taqman_file import GenerateTestTaqManFile
from workflows.universal_workflow_webhooks.generate_master_mix_worksheet import GenerateMasterMixWorksheet
from workflows.variant_result.validate_block_variant_results import ValidateVariantResults, BlockVariantResults
from workflows.extraction.send_samples_from_extraction_to_workflows import SendSamplesFromExtractionToWorkflows
from patient.create_order import CreateOrder
from request.select_assay import SelectAssay
from request.send_samples_to_workflows import SendSamplesToWorkflows
from sample.add_family_relation import AddFamilyRelation
from scheduled.update_test_code_refund_rate import UpdateTestCodeRefundRate
from main_toolbar.generate_test_qia_files import GenerateTestQIAFiles
from sdms.sdms_file_handler_test import SDMSFileHandlerTest
from workflows.instrument_booking.create_bookings import CreateBooking
from workflows.maxwell_extraction.print_worklist import PrintWorklist
from workflows.qiasymphony.generate_test_qiasymphony_file import GenerateTestQiasymphonyFile
from workflows.qiasymphony.parse_qiasymphony_file import ParseQIASymphonyFile
from workflows.qiaxpert.generate_pipette_files import GeneratePipetteFiles
from workflows.wes.fragmentation.mark_covaris_plate import MarkCovarisPlate
from workflows.wes.library_qc.check_adapter_dimer_readings import CheckAdapterDimerReadings
from workflows.wes.normalization.add_samples_to_plate_wes_normalization import AddSamplesToPlateWESNormalization
from workflows.wgs.dilution.add_reagent_volume import AddReagentVolume
from workflows.wgs.dilution.dilution import Dilution
from workflows.wgs.dilution.generate_hamilton_input_file_dilution import GenerateHamiltonInputFileDilution
from workflows.wgs.dilution.initialize_dilution import InitializeDilution
from workflows.wgs.library_prep.generate_library_prep_hamilton_file import GenerateLibraryPrepHamiltonFile
from workflows.wgs.normalization.add_samples_to_plate import AddSamplesToPlate
from workflows.wgs.normalization.generate_hamilton_file import GenerateHamiltonFile
from workflows.wgs.normalization.route_single_samples_to_snp_id import RouteSingleSamplesToSNPId
from workflows.wgs.pooling.add_sequencing_metadata_pooling import AddSequencingMetadataPooling
from workflows.wgs.pooling.calculate_pooling_volumes import CalculatePoolingVolumes
from workflows.wgs.pooling.complete_pooling_details import CompletePoolingDetails
from workflows.wgs.pooling.generate_hamilton_input_and_pipette_file_pooling import \
    GenerateHamiltonInputAndPipetteFilePooling
from workflows.wgs.pooling.pooling_logic import PoolingLogic
from workflows.wgs.pooling.verify_wgs_pooling_instrument_tracking_submission import \
    VerifyWGSPoolingInstrumentTrackingSubmission
from workflows.wgs.qPCR.generate_qpcr_file import GenerateQPCRFile
from workflows.wgs.qPCR.parse_qpcr_file import ParseQpcrFile
from workflows.wgs.qPCR.populate_reaction_efficiency import PopulateReactionEfficiency
from workflows.wgs.qPCR.populate_std_avg_table import PopulateStdAvgTable
from workflows.wgs.sequencing.create_variant_results import CreateVariantResults
from workflows.wgs.sequencing.generate_analysis import AnalysisFileGenerationHandler
from workflows.wgs.sequencing.generate_sample_sheets import SampleSheetGenerator
from workflows.wgs.sequencing.instrument_selection_list import InstrumentSelectionList
from workflows.ddpcr.remove_block_dilution import RemoveDilutionFromSample
from workflows.ddpcr.swap_chosen_dilution import SwapChosenDilution

from workflows.wgs.storage_retrieval.filter_concentration import FilterConcentration
from workflows.wgs.storage_retrieval.generate_files import GenerateWGSConfigs
from workflows.wgs.storage_retrieval.insert_controls import InsertControls
from workflows.wgs.storage_retrieval.update_plate_id import UpdatePlateID
from workflows.wgs.storage_retrieval.wgs_add_samples_to_plate import WGSAddSamplesToPlate
from workflows.ddpcr.launch_block_dilution import LaunchBlockDilution
from workflows.idreader.idreader_check import IDReaderCheck
from workflows.ddpcr.find_dilution_control import StandardDilutionCreator
from workflows.wes.fragmentation.last_used_col import LastUsedCol
from workflows.wes.sureselect.autoplate import SureSelectAutoPlate

from workflows.mlpa.denature_dilute.add_controls import MLPA_Add_Controls
from workflows.mlpa.denature_dilute.generate_dilution_files import Generate_dilution_files
from workflows.mlpa.denature_dilute.hybridization import MLPA_Hybridization
from workflows.mlpa.denature_dilute.populate_mlpa_plate import Populate_MLPA_Plate
from workflows.ogm.isolation_of_hmw_gdna.populate_wbc_count import SetWBCCountOnIsolation

from integrations.generate_creds import GenerateCreds

TimeUtil.set_default_timezone('UTC')

config: WebhookConfiguration = WebhookConfiguration(verify_sapio_cert=True, debug=False)

if os.environ.get('SapioWebhooksInsecure') == "True":
    config.verify_sapio_cert = False

### integrations
config.register('/generate-creds', GenerateCreds)

### Patient
config.register('/create-order', CreateOrder)
config.register('/create_order_and_clinical_requisition', CreateOrderAndClinicalRequisition)

### Request
config.register('/select-assay', SelectAssay)
config.register('/send-samples-to-workflows', SendSamplesToWorkflows)
config.register('/check-sample-concentrations-for-wgs', CheckSampleConcentrationsForWGS)
config.register('/find-patient', RegisterPatient)
config.register('/receive-samples', ReceiveSamples)
config.register('/select_samples', SelectSamples)
config.register('/remove_assay', RemoveAssay)

### Sample
config.register('/add-family-relation', AddFamilyRelation)
config.register('/show-sample-families', ShowSampleFamilies)
config.register('/add-sequencing-metadata', AddSequencingMetadata)
config.register('/test-get-patients-from-samples', TestGetPatientsFromSamples)
config.register('/generate_barcodes_based_on_type', GenerateBarcodesBasedOnType)
config.register('/dequeue-samples', DequeueSamples)
config.register('/generate-test-nipt-files', GenerateTestNIPTFiles)

### AssayDetail
config.register('/select-indications', SelectIndication)

### Invoice
config.register('/send-invoices', SendInvoices)
config.register('/create-invoices', CreateInvoices)

### Family
config.register('/override-family-size-check-handler', OverrideFamilySizeCheckHandler)
config.register('/generate-pedigree-image', GeneratePedigreeImage)

### Probe Results
config.register('/review-probe-results', ReviewProbeResults)

### Reclassification Request
config.register('/show-reclassification-request-form-view', ShowReclassificationRequestFormView)
config.register('/accept-reclassification-requests', AcceptReclassificationRequests)
config.register('/reject-reclassification-requests', RejectReclassificationRequests)

### Variant Results
config.register('/validate-variant-results', ValidateVariantResults)
config.register('/block-variant-results', BlockVariantResults)
config.register('/copy_variant_result_to_family', CopyVariantResultToFamily)

### ddPCR Assay
config.register('/approve-control-designs', ApproveControlDesigns)
config.register("/find-diluted-control", StandardDilutionCreator)
config.register('/approve-ddpcr-assays', ApproveDDPCRAssays)

### Project
config.register('/launch-oslo-nsc-sample-registration', LaunchOsloNSCSampleRegistration)
config.register('/nsc-assign-to-process', NSCAssignToProcess)
config.register('/generate-test-nsc-sample-file', GenerateTestNSCSampleFile)
config.register('/upload-submission-form', UploadSubmissionForm)

### Primer
config.register('/edit-gene', EditGene)
config.register('/initiate-primer-design', InitiatePrimerDesign)
config.register('/create-primer', CreatePrimer)

### L gDNA
config.register('/update-days-after-labeling', UpdateDaysAfterLabeling)

### Promega Parts and In House Parts
config.register('/list_of_all_promega_and_in_house_types', ListOfAllPromegaAndInHouseTypes)
config.register('/perform_dilution_promega', PerformDilutionPromega)
config.register('/perform_dilution_in_house', PerformDilutionInHouse)

### Batch
config.register('/check-samples-for-nipt', CheckSamplesForNIPT)
config.register('/add-reagents', AddReagents)
config.register('/add-instruments', AddInstruments)
config.register('/cancel-batches', CancelBatches)

### Main Toolbar
config.register('/sdms-file-handler-test', SDMSFileHandlerTest)
config.register('/generate-test-qia-files', GenerateTestQIAFiles)
config.register('/generate-test-wgs-samples', GenerateTestWGSSamples)
config.register('/set-wgs-low-concentration-threshold', SetWGSLowConcentrationThreshold)
config.register('/generate-test-samples', GenerateTestSamplesOldClientCallback)
config.register('/generate-test-sanger-samples', GenerateTestSangerSamples)
config.register('/generate-test-wes-samples', GenerateTestWESSamples)
config.register('/generate-test-snp-id-samples', GenerateTestSNPIDSamples)
config.register('/generate-test-ddpcr-samples', GenerateTestDDPCRSamples)
config.register('/generate-test-ogm-samples', GenerateTestOGMSamples)
config.register('/generate-test-cap-electro-samples', GenerateTestCapillaryElectrophoresisSamples)
config.register('/lookup-requester', LookupRequester)

### ELN
config.register('/upload-qiasymphony-file', ParseQIASymphonyFile)
config.register('/generate-test-qiasymphony-file', GenerateTestQiasymphonyFile)
config.register('/generate-pipette-files', GeneratePipetteFiles)
config.register('/print-worklist', PrintWorklist)
config.register('/send-samples-from-extraction-to-workflows', SendSamplesFromExtractionToWorkflows)
config.register('/add-samples-to-plate', AddSamplesToPlate)
config.register('/update-plate-id', UpdatePlateID)
config.register('/generate-test-id-reader-file', GenerateTestIDReaderFile)
config.register('/generate-hamilton-file', GenerateHamiltonFile)
config.register('/generate-library-prep-hamilton-file', GenerateLibraryPrepHamiltonFile)
config.register('/wgs-add-samples-to-plate', WGSAddSamplesToPlate)
config.register('/verify-wgs-pooling-instrument-tracking-submission', VerifyWGSPoolingInstrumentTrackingSubmission)
config.register('/initialize-dilution', InitializeDilution)
config.register('/dilution', Dilution)
config.register('/add_Reagent_volume', AddReagentVolume)
config.register('/generate-hamilton-input-file-dilution', GenerateHamiltonInputFileDilution)
config.register('/generate-hamilton-input-and-pipette-file-pooling', GenerateHamiltonInputAndPipetteFilePooling)
config.register('/add-sequencing-metadata-pooling', AddSequencingMetadataPooling)
config.register('/calculate-pooling-volumes', CalculatePoolingVolumes)
config.register('/complete-pooling-details', CompletePoolingDetails)
config.register('/parse-quantit-qc-output-file', QuantitQcOutputFileParser)
config.register('/register_start_stop_time', RegisterStartStopTime)
config.register('/populate_mastermix_table', PopulateMastermixTables)
config.register('/generate_master_mix_file', GenerateMastermixFiles)
config.register('/recalculate_aliquots_source_volume_to_use', RecalculateAliquotsSourceVolumeToUse)
config.register('/plate_samples', PlateSamples)
config.register('/generate_robot_file', GenerateRobotFiles)
config.register('/populate_taqman_assay_plate', PopulateTaqmanAssayPlate)
config.register('/send-samples-to-snpid', SendSamplesToSNPID)
config.register('/add_samples_to_plate_ngs_snp_id', AddSamplesToPlateNgsSnpId)
config.register('/generate_hamilton_file_ngs_snp_id', GenerateHamiltonFileNgsSnpId)
config.register('/populate_ddPCR_plate', PopulateDDPCRPlate)
config.register('/generate_quantasoft_input_files', GenerateQuantasoftInputFiles)
config.register('/populate-completed-tests', PopulateCompletedTests)
config.register('/generate_quantstudio_input_files', GenerateQuantStudioInputFiles)
config.register('/generate-master-mix-worksheet', GenerateMasterMixWorksheet)
config.register('/complete-nsc-sample-registration', CompleteNSCSampleRegistration)
config.register('/generate-pipetting-worksheet', GeneratePipettingWorksheet)
config.register('/requeue-samples', RequeueSamples)
config.register('/update-plate-id-on-save', UpdatePlateIDOnSave)
config.register('/update-samples-position', UpdateSamplesPosition)

# ELN- Common
config.register('/generate_plate_for_initial_plating', InitialPlateGeneration)

# Instrument Booking
config.register('/autofill-reserved-instruments', AutoFillReservedInstruments)
config.register('/reserve-machine-time', CreateBooking)

### Scheduled
config.register('/update-total-refund-rate', UpdateTestCodeRefundRate)
config.register('/update-order-name', UpdateOrderName)
config.register('/update-days-after-sampling', UpdateDaysAfterSampling)

### WGS Storage Retrieval
config.register('/filter-concentration', FilterConcentration)
config.register('/generate-files', GenerateWGSConfigs)
config.register('/insert-controls', InsertControls)

# WGS sequencing
config.register('/generate-analysis', AnalysisFileGenerationHandler)
config.register('/generate-sample-sheets', SampleSheetGenerator)
config.register('/instrument-selection-list', InstrumentSelectionList)
config.register('/create-variant-results', CreateVariantResults)

# WGS
config.register('/pooling-logic', PoolingLogic)
config.register('/route_single_samples_to_snp_id', RouteSingleSamplesToSNPId)

# WGS and WES QPCR
config.register('/parse_qpcr_file', ParseQpcrFile)
config.register('/generate_qpcr_file', GenerateQPCRFile)
config.register('/populate_std_avg_table', PopulateStdAvgTable)
config.register('/populate_reaction_efficiency', PopulateReactionEfficiency)

# WES
## Normalization
config.register('/add-samples-to-plate-wes-normalization', AddSamplesToPlateWESNormalization)
## Fragmentation
config.register('/mark-covaris-plate', MarkCovarisPlate)
config.register('/last-used-col', LastUsedCol)
## Library QC
config.register('/check-adapter-dimer-readings', CheckAdapterDimerReadings)
## Sureselect
config.register('/sureselect-autoplating', SureSelectAutoPlate)

# Capillary Electrophoresis
config.register('/generate-plate-ids', GeneratePlateIDs)
config.register('/print-merged-plates', PrintMergedPlates)
config.register('/create-standard-formamide-tables', CreateStandardFormamideTables)
config.register('/print-abi-input-files', PrintABIInputFiles)
config.register('/copy-probe-mixes', CopyProbeMixes)

### SDMS
config.register('/sdms-file-handler', SDMSFileHandler)

### Onsave
## Variant Result
config.register('/queue-suggested-verification-samples', QueueSuggestedVerificationSamples)
## Reagent
config.register('/create-reagent-plate-columns', CreateReagentPlateColumns)
## ddPCR Assay
config.register('/queue-ddpcr-samples', QueueDDPCRSamples)
## Analysis config
config.register("/copy_probe_mix_to_sample_and_assay_detail_from_analysis_config",
                CopyProbeMixToSampleAndAssayDetailFromAnalysisConfig)
## Sample
config.register('/generate-sample-name', GenerateSampleName)
## External Lab Results
config.register('/check-external-lab-results', CheckExternalLabResults)

### Sanger
## Long Range PCR
config.register('/generate-biomek-files', GenerateBiomekFiles)
config.register('/long-range-pcr-plate-generation', LongRangePCRPlateGeneration)
config.register('/check-long-range-pcr-batches', CheckLongRangePCRBatches)
## PCR Amplification
config.register('/pcr-amplification-plate-generation', PCRAmplificationPlateGeneration)
config.register('/add-samples-to-experiment', AddSamplesToExperiment)

### SNP-ID
## Generate Pick Files for Samples
config.register('/generate-test-taqman-results', GenerateTestTaqManFile)
## TaqMan Genotype QC
config.register('/analyze-taq-man-qc-results', AnalyzeTaqManQCResults)
## QuantStudio
config.register('/generate_quantstudio_input_files', GenerateQuantStudioInputFiles)

# DDPCR
config.register("/remove-block-dilution", RemoveDilutionFromSample)
config.register('/launch-block-dilution', LaunchBlockDilution)
config.register("/create_dilution_and_mastermix_worksheet", CreateDilutionAndMasterMixWorksheet)
config.register("/swap-dilutions", SwapChosenDilution)

### SNP_ID
config.register("/id-reader-check", IDReaderCheck)

### Report Generation
config.register("/generate_requisition_report_form", RequisitionReportFormGenerator)
config.register("/generate_nsc_report", GenerateNSCReport)

# Pedigree Configurations
config.register("/edit_included_family_members", EditIncludedFamilyMembers)
config.register("/related_variant_list", RelatedVariantList)

### OGM
# Blood Aliquots Preparation
config.register("/add_wbc_count_measurement", AddWbcCountMeasurement)
config.register("/set_wbc_count_measurement", SetWbcCountMeasurement)
config.register("/set_fields_to_required_for_diagnostic_samples", SetFieldsToRequiredForDiagnosticSamples)
config.register("/remove_replicates_from_going_to_next_step", RemoveReplicatesFromGoingToNextStep)
config.register("/remove_experiment_details_without_sample_id", RemoveExperimentDetailsWithoutSampleId)
# Isolation of HMW gDNA workflow
config.register("/check_to_enforce_wbc_count_measurement", CheckToEnforceWBCCountMeasurement)
config.register("/populate_hemo_cell_cue_count", PopulateHemoCellCueCount)
config.register("/generate_pipetting_worksheet", GeneratePipettingWorkSheet)
config.register("/set_target_volume", SetTargetVolume)
config.register("/qubit_calculations", QubitCalculations)
config.register("/qubit_sample_routing", QubitSampleRouting)
config.register("/set_wbc_count_on_isolation", SetWBCCountOnIsolation)
# Load chip and Run OGM
config.register("/set_chip_details_using_well_elements", SetChipDetailsUsingWellElements)
config.register("/generate_ogm_chip_worksheet", GenerateOgmChipWorksheet)
# NSC Reporting
config.register("/associate_experiment_to_nsc_project", AssociateExperimentToNSCProject)

# MLPA
config.register("/mlpa-add-controls", MLPA_Add_Controls)
config.register("/mlpa-generate-dilution-files", Generate_dilution_files)
config.register("/mlpa-hybridization", MLPA_Hybridization)
config.register("/mlpa-populate-plates", Populate_MLPA_Plate)

# Fragment Analysis
## PCR Preparation
config.register("/generate_file_for_reaction_mix_recipie", GenerateFileForReactionMixRecipie)
config.register("/add_controls_pcr_preparation", AddControlsPcrPreparation)
config.register("/add_reagents_as_per_fragment_analysis_kit", AddReagentsAsPerFragmentAnalysisKit)
config.register("/generate_dilution_file", GenerateDilutionFile)

# ACGH
config.register("/populate_promega_lots", AddPromegaLotsToAppendControlsStep)
config.register("/promega-lots", ACGHPromegaLots)
config.register("/update_samples_step_with_promega_controls", UpdateSampleStepWithPromegaControls)
config.register("/populate-dna-reconcentration-entry", PopulateDNAReconcentrationEntry)
config.register("/acgh-sample-dilution-set", ACGHSampleDilutionSet)
config.register("/generate_acgh_dilution_worksheet", GenerateDilutionWorksheetForaCGH)
config.register("/add_rows_for_microarray_scanning", CreateRowsForMicroarrayScanning)
config.register("/generate_array_id_for_samples", SortSamplesAndGenerateArrayIds)
config.register("/generate_nxclinical_desc_file", GenerateNxclinicalDescriptionFile)
config.register("/populate_nxclinical_desc_file_details", PopulateNxClinicalDescStep)
config.register("/populate_acgh_pipetting_details", PopulatePipettingDetailsSteps)

# NSC
## Illumina DNA Sample Prep
config.register('/generate-nsc-sample-prep-pipetting-worksheet', GenerateNSCSamplePrepPipettingWorksheet)
config.register('/generate-nsc-sample-prep-illumina-robot-file', GenerateNSCSamplePrepRobotFile)
## 10nM Dilution
config.register('/set_normalization_fields', SetNormalizationTableFields)
config.register('/generate_10nM_normalization_sheet', GenerateNormalizationSheet)
## NSC Pooling
# config.register('/generate_pooling_entry', DynamicPoolingEntries)
## Final QC
config.register('/calculate_molarity_for_final_qc', CalculateMolarity)
#MiSeq Sequecning
config.register('/update_miseq_sample_sheet', MiSeqSampleSheetUpdater)

## 16S
config.register('/generate-nsc-quant-it-worksheet', GenerateNSCQuantITWorksheet)
config.register('/autoplate-nsc-sixteenS-samples', AutoplateNSCSixteenSSamples)
config.register('/generate-nsc-sixteenS-robot-file', GenerateNSCSixteenSRobotFile)
config.register('/copy-quant-it-concentrations', CopyQuantITConcentrations)
config.register('/generate-nsc-pooling-worksheet', GenerateNSCPoolingWorksheet)
config.register('/pooling-dialog', PoolingDialog)
config.register('/copy-pooling-fields', CopyPoolingFields)

# RNA Analysis

# RNA Isolation
config.register('/generate-rna-iso-qia-input-file', GenerateRnaIsoQiaxpertInputFile)
config.register('/parse-rna-iso-qia-results-file', ParseRnaIsoQiaxpertResultsFile)

config.register('/refresh', Refresh)

app = WebhookServerFactory.configure_flask_app(app=None, config=config)


@app.route("/ping")
def ping() -> str:
    return "Pong!"


if __name__ == '__main__':
    host = "0.0.0.0"
    # This port must match the EXPOSE value in the Dockerfile when deploying.
    port = 8080
    if os.environ.get('SapioWebhooksDebug') == "True":
        app.run(host=host, port=port, debug=True)
    else:
        serve(app, host=host, port=port)
