import json
from typing import Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult, DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, PatientModel, RequestModel, InvoiceModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler


class CreateOrder(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager

    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = AccessionManager(context.user)

        self.response_map = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is None:
            return self.prompt_creation_method()

        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_map = json.loads(result.callback_context_data)

        # If there are any errors, return a webhook result of true, since a popup will already have been shown to the
        # user
        if self.response_map["status"] == "error":
            return SapioWebhookResult(True)

        if isinstance(result, OptionDialogResult):
            if self.response_map["status"] == "creation":
                return self.process_creation_result(context, result)

        if isinstance(result, DataRecordSelectionResult):
            return self.process_existing_samples_result(result)

        else:
            return SapioWebhookResult(True)

    def process_creation_result(self, context: SapioWebhookContext, result: OptionDialogResult) -> SapioWebhookResult:
        """
        Create an order with existing samples or with no sample, based on the selection of the user
        :param context: SapioWebhookContext
        :param result: OptionDialogResult with user selection
        :return: Sample selection prompt or view of the new order
        """
        selection: int = result.selection

        # Create with existing sample
        if selection == 0:
            return self.prompt_existing_samples(context)

        else:   # Create with no sample
            return self.create_order(context)

    def process_existing_samples_result(self, result: DataRecordSelectionResult) -> SapioWebhookResult:
        """
        Create a new order with existing samples based on the samples selected in the dialog
        :param result: DataRecordSelectionResult with selected samples
        :return: View of the new order
        """
        result_map: list[dict[str, Any]] = result.selected_field_map_list

        # Get the sample's request and set it's AllSamplesApproved field to true
        sample: SampleModel = self.rec_handler.query_models(SampleModel, SampleModel.SAMPLEID__FIELD_NAME.field_name,
                                                            [result_map[0]["SampleId"]])[0]
        self.rel_man.load_parents_of_type([sample], RequestModel)

        request: RequestModel = sample.get_parent_of_type(RequestModel)
        request.set_AllSamplesReceived_field(True)

        # Add an invoice as a child of the order
        request.add_child(self.rec_handler.add_model(InvoiceModel))

        # Store and commit changes
        self.rec_man.store_and_commit()

        # Return a directive to the request record
        return SapioWebhookResult(True, directive=FormDirective(request.get_data_record()))

    # noinspection PyMethodMayBeStatic
    def prompt_creation_method(self) -> SapioWebhookResult:
        """
        Prompt the user with an option dialog of how they wish to create the order
        :return: OptionDialogResult
        """
        self.response_map["status"] = "creation"
        return PopupUtil.display_option_popup("Create Order", "How would you like to create the order?",
                                              ["Create with Existing Sample", "Create with No Sample"],
                                              True, request_context=json.dumps(self.response_map))

    def prompt_existing_samples(self, context: SapioWebhookContext) -> SapioWebhookResult:
        """
        Prompt the user to select from any samples with unfinished orders
        :param context: SapioWebhookContext
        :return: Sample selection dialog if there are qualifying samples, or an error if there aren't any
        """
        # Get a list of all the patient's samples
        patient: PatientModel = self.rec_handler.wrap_model(context.data_record, PatientModel)

        self.rel_man.load_children_of_type([patient], RequestModel)
        requests: list[RequestModel] = [r for r in patient.get_children_of_type(RequestModel)
                                        if not r.get_AllSamplesReceived_field()]

        if not requests:    # If there are no available samples, error to the user
            self.response_map["status"] = "error"
            return PopupUtil.ok_popup("Error", "This patient has no available samples.",
                                      request_context=json.dumps(self.response_map))

        self.rel_man.load_children_of_type(requests, SampleModel)
        samples: list[SampleModel] = []
        for r in requests:
            child_samples: list[SampleModel] = r.get_children_of_type(SampleModel)
            samples.extend([s for s in child_samples if s.get_SampleId_field() == s.get_TopLevelSampleId_field()])

        if len(samples) == 0:
            self.response_map["status"] = "error"
            return PopupUtil.ok_popup("Error", "There are no existing samples under this patient.",
                                      request_context=json.dumps(self.response_map))

        self.response_map["status"] = "good"
        return PopupUtil.record_selection_popup(context, "Select Sample", ["DataRecordName", "SampleId"], samples,
                                                False, request_context=json.dumps(self.response_map))

    def create_order(self, context: SapioWebhookContext) -> SapioWebhookResult:
        """
        Create an order with no samples
        :param context: SapioWebhookContext
        :return: Directive to the new order
        """
        # Create the request under the patient
        request: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
        patient: PatientModel = self.rec_handler.wrap_model(context.data_record, PatientModel)
        patient.add_child(request)

        # Store and commit changes
        self.rec_man.store_and_commit()

        # Return a directive to the order
        return SapioWebhookResult(True, directive=FormDirective(request.get_data_record()))
