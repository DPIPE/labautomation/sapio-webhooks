import json
from typing import Any, Dict, Optional, cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.AccessionService import AccessionManager
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.datatype.DataType import DataTypeDefinition
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.data_models import PatientModel, RequestModel, ClinicalRequisitionModel, SampleModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler


class CreateOrderAndClinicalRequisition(OsloWebhookHandler):
    rec_handler: RecordHandler
    acc_manager: AccessionManager
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.acc_manager = AccessionManager(context.user)
        self.response_map = {"status": ""}

        result: ClientCallbackResult = context.client_callback_result
        if result is None:
            return self.create_order(context)
        if result.user_cancelled:
            return SapioWebhookResult(True)

        self.response_map = json.loads(result.callback_context_data)
        request: RequestModel = None
        requisition: ClinicalRequisitionModel = None
        try:
            if self.response_map["status"] == "RequestCreation":
                # Get field values entered by user
                form_result: Optional[FormEntryDialogResult] = cast(
                    Optional[FormEntryDialogResult], context.client_callback_result)
                field_map: Dict[str, Any] = form_result.user_response_map
                field_map = {k: v for k, v in field_map.items() if v is not None and v != ""}

                # Create the request under the patient
                request: RequestModel = RequestUtils.add_request(self.rec_handler, self.acc_manager)
                patient: PatientModel = self.rec_handler.wrap_model(context.data_record, PatientModel)
                request.set_field_values(field_map)
                patient.add_child(request)
                self.rec_man.store_and_commit()
                return self.create_clinincal_requisition(context, request)

            if self.response_map["status"] == "ClinicalRequisitionCreation":
                # Get field values entered by user
                form_result: Optional[FormEntryDialogResult] = cast(
                    Optional[FormEntryDialogResult], context.client_callback_result)
                field_map: Dict[str, Any] = form_result.user_response_map
                field_map = {k: v for k, v in field_map.items() if v is not None and v != ""}

                # Get Order created in previous call
                patient: PatientModel = self.rec_handler.wrap_model(context.data_record, PatientModel)
                self.rel_man.load_children_of_type([patient], RequestModel)
                requests = patient.get_children_of_type(RequestModel)
                request = [request for request in requests if
                           str(request.get_field_value("RecordId")) == self.response_map["request_recordid"]][0]

                # Create the Clinical Requisition under the order
                requisition: ClinicalRequisitionModel = self.rec_handler.add_model(ClinicalRequisitionModel)
                requisition.set_field_values(field_map)
                request.add_child(requisition)

                # Check if patient has a descendant sample of the same type as the “Sample material” field in requisition
                self.an_man.load_descendant_of_type([patient.backing_model], SampleModel.DATA_TYPE_NAME.__str__())
                samples = self.an_man.get_descendant_of_type(patient.backing_model, SampleModel.DATA_TYPE_NAME.__str__())
                samples = [sample for sample in samples if
                           str(sample.get_field_value(SampleModel.EXEMPLARSAMPLETYPE__FIELD_NAME.field_name)) == requisition.get_SampleMaterial_field()]
                if samples:
                    # if available, then mark the Sample material already available? to be true
                    requisition.set_IsSampleMaterialAvailable_field(True)

                self.rec_man.store_and_commit()

        except Exception as exception:
            # Delete the new records created if there is an unexpected error
            if request: request.delete()
            if requisition: requisition.delete()
            self.rec_man.store_and_commit()
            self.logger.error(f"Error while trying to create order/clinical requisition: {exception}")
            return PopupUtil.display_ok_popup("Error", str(exception))

        return SapioWebhookResult(True,
                                  display_text=f"Successfully created Order {request.get_RequestName_field()} and Clinical requisition")

    def create_order(self, context: SapioWebhookContext) -> SapioWebhookResult:

        # Get Request datatype and field definitions
        data_type_manager: DataTypeManager = DataMgmtServer.get_data_type_manager(context.user)
        datatype_def: DataTypeDefinition = data_type_manager.get_data_type_definition(
            RequestModel.DATA_TYPE_NAME.__str__())
        data_field_defs = data_type_manager.get_field_definition_list(
            datatype_def.get_data_type_name())
        field_def_by_field_name = {x.data_field_name: x for x in data_field_defs}

        # Find the default_layout
        request_default_layout = None
        for layout in data_type_manager.get_data_type_layout_list(datatype_def.get_data_type_name()):
            if layout.default_layout:
                request_default_layout = layout
                break

        # Create a FormBuilder instance
        form_builder: FormBuilder = FormBuilder()
        order_name = RequestUtils.generate_order_name(self.acc_manager)
        for field_name, field_definition in field_def_by_field_name.items():
            if field_name == RequestModel.REQUESTNAME__FIELD_NAME.field_name:
                field_def = cast(Optional[VeloxStringFieldDefinition], field_definition)
                field_def.default_value = order_name
                form_builder.add_field(field_def)
            else:
                form_builder.add_field(field_definition)

        # Get the temporary data type from the form builder
        temp_dt = form_builder.get_temporary_data_type()

        # setting default layout for the form for Cell Line Data Type
        temp_dt.data_type_layout = request_default_layout

        # Return a form entry dialog request
        self.response_map["status"] = "RequestCreation"
        form_popup = FormEntryDialogRequest("Create Order for the Patient",
                                            "Enter order information", temp_dt,
                                            callback_context_data=json.dumps(self.response_map))
        return SapioWebhookResult(True, client_callback_request=form_popup)

    def create_clinincal_requisition(self, context: SapioWebhookContext, request: RequestModel) -> SapioWebhookResult:

        # Get Clinical Requisition datatype and field definitions
        data_type_manager: DataTypeManager = DataMgmtServer.get_data_type_manager(context.user)
        datatype_def: DataTypeDefinition = data_type_manager.get_data_type_definition(
            ClinicalRequisitionModel.DATA_TYPE_NAME.__str__())
        data_field_defs = data_type_manager.get_field_definition_list(
            datatype_def.get_data_type_name())

        # Find the default_layout
        clinical_req_default_layout = None
        for layout in data_type_manager.get_data_type_layout_list(datatype_def.get_data_type_name()):
            if layout.default_layout:
                clinical_req_default_layout = layout
                break

        # Create a FormBuilder instance
        form_builder: FormBuilder = FormBuilder()
        for field_definition in data_field_defs:
            form_builder.add_field(field_definition)

        # Get the temporary data type from the form builder
        temp_dt = form_builder.get_temporary_data_type()

        # setting default layout for the form for Cell Line Data Type
        temp_dt.data_type_layout = clinical_req_default_layout

        # Return a form entry dialog request
        self.response_map["status"] = "ClinicalRequisitionCreation"
        self.response_map["request_recordid"] = str(request.record_id)
        form_popup = FormEntryDialogRequest("Create Clinical Requisition",
                                            "Enter Clinical Requisition Information", temp_dt,
                                            callback_context_data=json.dumps(self.response_map))
        return SapioWebhookResult(True, client_callback_request=form_popup)
