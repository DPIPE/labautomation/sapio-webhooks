from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import TestCodeConfigurationModel, AssayDetailModel
from utilities.webhook_handler import OsloWebhookHandler


class SelectIndication(OsloWebhookHandler):
    rec_handler: RecordHandler

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)

        indications: list[str] = []

        # Get the TestCodeConfig record that matches the ID of the AssayDetail record
        assay_detail: AssayDetailModel = self.rec_handler.wrap_model(context.data_record, AssayDetailModel)
        test_code_config: TestCodeConfigurationModel = (
            self.rec_handler.query_models(TestCodeConfigurationModel,
                                          TestCodeConfigurationModel.TESTCODEID__FIELD_NAME.field_name,
                                          [assay_detail.get_TestCodeId_field()]))[0]

        # Add all the indications of the TestCodeConfig to the list
        indications.extend(test_code_config.get_Indications_field().split(","))

        # Return the list of indications
        return SapioWebhookResult(True, list_values=indications)
