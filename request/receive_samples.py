import json
from typing import cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import OptionDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import RequestModel, SampleModel, AssayDetailModel
from utilities.request_utils import OrderStatus
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


STATUS: str = "status"
STATUS_SEND_TO_QC_PROMPT: str = "sendToQC"
STATUS_ERROR: str = "error"


class ReceiveSamples(OsloWebhookHandler):
    rec_handler: RecordHandler
    response_data: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_data = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            self.response_data = json.loads(result.callback_context_data)
            if self.response_data[STATUS] == STATUS_SEND_TO_QC_PROMPT:
                send_to_qc: bool = True if cast(OptionDialogResult, result).selection == 0 else False

                order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
                self.rel_man.load_children_of_type([order], SampleModel)
                samples: list[SampleModel] = order.get_children_of_type(SampleModel)
                dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "DNA"]
                non_dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() != "DNA"]

                return self.send_samples(context, order, dna_samples, non_dna_samples, send_to_qc)
            else:
                return SapioWebhookResult(True)

        # Get all the direct child samples of the order. If there are no samples, show an error to the user.
        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        self.rel_man.load_children_of_type([order], SampleModel)
        samples: list[SampleModel] = order.get_children_of_type(SampleModel)
        if not samples:
            self.response_data[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "This order has no samples.",
                                      request_context=json.dumps(self.response_data))

        dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "DNA"]
        non_dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() != "DNA"]

        # If there are any DNA samples, prompt the user if they would like to queue them from Quality Control.
        if len(dna_samples) > 0:
            self.response_data[STATUS] = STATUS_SEND_TO_QC_PROMPT
            return PopupUtil.yes_no_popup("Send DNA Samples to Quality Control (QC)?",
                                          "The order has DNA samples registered under it. Would you like to send them "
                                          "to Quality Control (QC)?", request_context=json.dumps(self.response_data))

        return self.send_samples(context, order, dna_samples, non_dna_samples)

    def send_samples(self, context: SapioWebhookContext, order: RequestModel, dna_samples: list[SampleModel],
                     non_dna_samples: list[SampleModel], send_to_qc: bool = False) -> SapioWebhookResult:

        # Send each of those samples to their corresponding extractions
        qiasymphony_samples: list[SampleModel] = [s for s in non_dna_samples
                                                  if s.get_ExemplarSampleType_field() == "Blood"]
        if qiasymphony_samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, qiasymphony_samples,
                                              WorkflowUtils.ASSAY_NAME_QIASYMPHONY_DNA_EXTRACTION, request=order)
        # [OSLO-1118]: Don't send plasma samples to extraction since they need to be queued for ELISA.
        maxwell_samples: list[SampleModel] = [s for s in non_dna_samples if s.get_ExemplarSampleType_field() != "Blood"
                                              and s.get_ExemplarSampleType_field() != "EDTA Blood"
                                              and s.get_ExemplarSampleType_field() != "PAX Blood"
                                              and s.get_ExemplarSampleType_field() != "Plasma"]
        if maxwell_samples:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, maxwell_samples,
                                              WorkflowUtils.ASSAY_NAME_MAXWELL_DNA_EXTRACTION, request=order)
        if send_to_qc:
            ProcessTracking.assign_to_process(context, SampleModel.DATA_TYPE_NAME, dna_samples,
                                              WorkflowUtils.ASSAY_NAME_DNA_QUALITY_CONTROL, request=order)

        # Set the All Samples Received field to True
        # [OSLO-1136]: Set the order status to "Ready for technician."
        order.set_AllSamplesReceived_field(True)
        order.set_OrderStatus_field(OrderStatus.READY_FOR_TECHNICIAN.value)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, "Successfully received samples.")
