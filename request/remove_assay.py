from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import AssayDetailModel, RequestModel, InvoiceModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_ASSAY_REMOVE: str = "assayRemove"


class RemoveAssay(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:

        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)

        # Do not execute the logic if order is already approved ( assuming samples are in queue already )
        if order.get_RequestApproved_field():
            self.callback.display_error("Order is already approved. Can't remove assays. Please contact admin."
                                        "\nSample(s) should be dequeued and order should be marked as not approved to remove added assays.")
            return SapioWebhookResult(False)

        self.rel_man.load_children_of_type([order], InvoiceModel)
        invoices = order.get_children_of_type(InvoiceModel)
        if not invoices:
            self.callback.ok_dialog("Error", "No assay available to remove")

        self.rel_man.load_children_of_type(invoices, AssayDetailModel)
        assay_details: list[AssayDetailModel] = list()
        assay_detail_to_invoice = {}
        assay_detail_to_extension = {}

        data_type_name_list = DataMgmtServer.get_data_type_manager(context.user).get_data_type_name_list()

        for invoice in invoices:
            extension_type: str = f"{invoice.get_AssayName_field()}AssayDetail"
            assay_detail_ext_rec = None
            if extension_type in data_type_name_list:
                self.rel_man.load_children([order.backing_model], extension_type)
                assay_detail_ext_rec = order.backing_model.get_child_of_type(extension_type)
            assay_detail_recs = invoice.get_children_of_type(AssayDetailModel)
            if assay_detail_recs:
                assay_details.append(assay_detail_recs[0])
                assay_detail_to_invoice[assay_detail_recs[0].record_id] = invoice
                if assay_detail_ext_rec:
                    assay_detail_to_extension[assay_detail_recs[0].record_id] = assay_detail_ext_rec

        return self._prompt_assay_selection(assay_details, assay_detail_to_invoice, assay_detail_to_extension)

    def _prompt_assay_selection(self, assay_details: list[AssayDetailModel], assay_detail_to_invoice,
                                assay_detail_to_extension) -> SapioWebhookResult:

        fields: list[str] = ["Assay", "TestCodeId", "AnalysisLabel"]
        selected_records = self.callback.record_selection_dialog("Select assay to be removed", fields, assay_details)
        if not selected_records:
            return SapioWebhookResult(True)

        for rec in selected_records:
            invoice = assay_detail_to_invoice[rec.record_id]
            if invoice:
                invoice.delete()
            rec.delete()
            assay_detail_ext_rec = assay_detail_to_extension[rec.record_id]
            if assay_detail_ext_rec:
                assay_detail_ext_rec.delete()

        self.rec_man.store_and_commit()
        return SapioWebhookResult(True,
                                  f"Successfully removed selected assay(s) and associated invoice(s)")
