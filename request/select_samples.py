from typing import Any

from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import RawReportTerm, RawTermOperation, ReportColumn, CustomReportCriteria
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxPickListFieldDefinition, \
    VeloxDoubleFieldDefinition, AbstractVeloxFieldDefinition, VeloxIntegerFieldDefinition, FieldType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from utilities.data_models import SampleModel, RequestModel, PrenatalSampleModel, PatientModel, FamilyModel, \
    FamilyDetailsModel
from utilities.family_utils import FamilyUtils
from utilities.sample_utils import SampleUtils
from utilities.webhook_handler import OsloWebhookHandler


class SelectSamples(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Check if the order is associated with a patient.
        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        self.rel_man.load_parents_of_type([order], PatientModel)
        patient: PatientModel | None = order.get_parent_of_type(PatientModel)
        if not patient:
            self.callback.ok_dialog("Error", "There is no patient associated with this order, so samples cannot be"
                                             " added.")
            return SapioWebhookResult(True)

        # Prompt the user to either add new or create an existing sample.
        selection: str | None = (
            self.callback.option_dialog("Select Sample", "How would you like to add a sample to the order?",
                                        ["Add New Sample", "Add Existing Sample", "Add Proband (Trio) Sample"],
                                        user_can_cancel=True))
        if not selection:
            return SapioWebhookResult(True)

        # Add the sample to the order based on the selection.
        if selection == "Add New Sample":
            sample: SampleModel | None = self._add_new_sample(order)
        elif selection == "Add Existing Sample":
            sample: SampleModel | None = self._add_existing_sample(patient, order)
        else:
            sample: SampleModel | None = self._add_proband_sample(patient, order)
        if not sample:
            return SapioWebhookResult(True)

        order.add_child(sample)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def _add_new_sample(self, order: RequestModel) -> SampleModel | None:
        # Create the fields for the field entry dialog.
        values: dict[str, Any] = {"TubeBarcode": "",
                                  "SampleType": "",
                                  "Concentration": 0.0,
                                  "ConcentrationUnits": "",
                                  "Quantity": 0.0,
                                  "QuantityUnits": "",
                                  "ContainerType": ""}

        fields: list[AbstractVeloxFieldDefinition] = []
        tube_barcode_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode", unique_value=True))
        tube_barcode_field.editable = True
        fields.append(tube_barcode_field)
        sample_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("SampleType", "SampleType", "Sample Type", "Exemplar Sample Types"))
        sample_type_field.editable = True
        sample_type_field.required = True
        fields.append(sample_type_field)
        sample_source_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("SampleSource", "SampleSource", "Sample Source", "Sample Source"))
        sample_source_type_field.editable = True
        sample_source_type_field.required = True
        fields.append(sample_source_type_field)
        concentration_field: VeloxDoubleFieldDefinition = (
            VeloxDoubleFieldDefinition("Concentration", "Concentration", "Concentration"))
        concentration_field.editable = True
        fields.append(concentration_field)
        concentration_units_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("ConcentrationUnits", "ConcentrationUnits", "Concentration Units",
                                         "Concentration Units"))
        concentration_units_field.editable = True
        fields.append(concentration_units_field)
        quantity_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("Quantity", "Quantity", "Volume")
        quantity_field.editable = True
        fields.append(quantity_field)
        quantity_units_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("QuantityUnits", "QuantityUnits", "Quantity Units", "Volume Units"))
        quantity_units_field.editable = True
        fields.append(quantity_units_field)
        container_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("ContainerType", "ContainerType", "Container Type", "Container Type List"))
        container_type_field.editable = True
        fields.append(container_type_field)

        # Prompt the user to enter field values for the new sample.
        results: dict[str, Any] | None = (
            self.callback.form_dialog("Add New Sample", "Enter the field values for the new sample.", fields, values))
        if not results:
            return None

        # Check to make sure that the tube barcode, if entered, is unique.
        tube_barcode: str = results["TubeBarcode"]
        if tube_barcode:
            if not self._check_unique_tube_barcode(tube_barcode):
                self.callback.ok_dialog("Error", f"Tube barcode \"{tube_barcode}\" is already being used by another"
                                                 f" sample in the system.")
                return None

        # Create a new sample with the specified field values.
        sample: SampleModel = self.rec_handler.add_model(SampleModel)
        sample.set_TubeBarcode_field(results["TubeBarcode"])
        sample.set_ExemplarSampleType_field(results["SampleType"])
        if results["Concentration"]:
            sample.set_Concentration_field(float(results["Concentration"]))
        sample.set_ConcentrationUnits_field(results["ConcentrationUnits"])
        if results["Quantity"]:
            sample.set_Volume_field(float(results["Quantity"]))
        sample.set_VolumeUnits_field(results["QuantityUnits"])
        sample.set_ContainerType_field(results["ContainerType"])
        sample.set_SampleSource_field(results["SampleSource"])

        # OSLO-1158 Feedback
        self.add_extension_if_prenatal_sample(sample)

        # [OSLO-1102]: All samples, not just EDTA Blood samples should have their sample name set to <order name><#>,
        # where # is the next number in the sequence of samples under the order.
        sample.set_OtherSampleId_field(SampleUtils.generate_next_sample_name(self.rel_man, order))

        return sample

    def add_extension_if_prenatal_sample(self, sample):
        # Check if the sample source is present in Prenatal sample sources list
        picklist_config = DataMgmtServer.get_picklist_manager(self.user).get_picklist("Prenatal Sample Sources")
        if not picklist_config:
            return
        prenatal_sample_sources: list[str] = picklist_config.entry_list
        prenatal = sample.get_SampleSource_field() and (sample.get_SampleSource_field() in prenatal_sample_sources)

        if prenatal:
            # prompt user to enter values for prenatal sample
            prenatal_values: dict[str, Any] = {"FetalSex": "",
                                               "GestationalAge": ""}
            prenatal_fields: list[AbstractVeloxFieldDefinition] = []
            fetal_sex_field: VeloxPickListFieldDefinition = (
                VeloxPickListFieldDefinition("FetalSex", "FetalSex", "Fetal Sex", "Sex"))
            fetal_sex_field.editable = True
            prenatal_fields.append(fetal_sex_field)
            gestational_age_field: VeloxIntegerFieldDefinition = (
                VeloxIntegerFieldDefinition("GestationalAge", "GestationalAge", "Gestational Age"))
            gestational_age_field.editable = True
            prenatal_fields.append(gestational_age_field)
            results: dict[str, Any] | None = (
                self.callback.form_dialog("Prenatal Sample Details", "Enter the values for the prenatal sample.",
                                          prenatal_fields,
                                          prenatal_values))

            sample.set_Prenatal_field(prenatal)
            self.rel_man.load_children_of_type([sample], PrenatalSampleModel)
            prenatal_sample = sample.get_child_of_type(PrenatalSampleModel)
            if prenatal_sample is None:
                prenatal_sample: PrenatalSampleModel = self.rec_handler.add_model(PrenatalSampleModel)
                sample.add_child(prenatal_sample)
            if results:
                prenatal_sample.set_FetalSex_field(results["FetalSex"])
                if results["GestationalAge"]:
                    prenatal_sample.set_GestationalAge_field(int(results["GestationalAge"]))

    def _add_existing_sample(self, patient: PatientModel, order: RequestModel) -> SampleModel | None:
        # Get associated samples.
        samples: list[SampleModel] | None = self._get_associated_samples(patient, order)
        if not samples:
            self.callback.ok_dialog("Error", "No associated samples found for patient, please register new ones.")
            return None

        # Get all the samples currently under the order to avoid showing them in the prompt.
        self.rel_man.load_children_of_type([order], SampleModel)
        existing_samples: list[SampleModel] = order.get_children_of_type(SampleModel)
        record_ids: list[int] = [x.record_id for x in existing_samples]
        samples_to_show: list[SampleModel] | None = [x for x in samples if x.record_id not in record_ids]
        if not samples_to_show:
            self.callback.ok_dialog("Error", "All samples under this order's patient are already under this order.")
            return None

        # Generate the record selection prompt and prompt the user to select samples.
        display_fields = [
            SampleModel.SAMPLEID__FIELD_NAME.field_name,
            SampleModel.OTHERSAMPLEID__FIELD_NAME.field_name,
            SampleModel.CONCENTRATION__FIELD_NAME.field_name,
            SampleModel.CONCENTRATIONUNITS__FIELD_NAME.field_name,
            SampleModel.VOLUME__FIELD_NAME.field_name,
            SampleModel.VOLUMEUNITS__FIELD_NAME.field_name,
            SampleModel.SAMPLESOURCE__FIELD_NAME.field_name,
            SampleModel.EXEMPLARSAMPLETYPE__FIELD_NAME.field_name,
            SampleModel.PRENATAL__FIELD_NAME.field_name
        ]
        selected_samples: list[SampleModel] | None = (
            self.callback.record_selection_dialog("Select a sample to add to the order.", display_fields,
                                                  samples_to_show, multi_select=False))
        if not selected_samples:
            return None
        return selected_samples[0]

    def _add_proband_sample(self, patient: PatientModel, order: RequestModel) -> SampleModel | None:
        # Get trio samples
        trio_samples: list[SampleModel] | None = self._get_trio_samples(patient)
        if trio_samples is None:
            self.callback.ok_dialog("Error",
                                    "Patient's family is not found. Please add the patient's sample to a family.")
            self.log_to_system(
                f"RecordId: {patient.record_id} Patient's family is not found. Please add the patient's sample to a family.")
            return None

        # [OSLO-900]: Only link the proband to the order itself, then reference the mother/father on the assay level
        proband_samples: list[SampleModel] = []
        self.rel_man.load_children_of_type(trio_samples, FamilyDetailsModel)
        for sample in trio_samples:
            family_details: list[FamilyDetailsModel] = sample.get_children_of_type(FamilyDetailsModel)
            if family_details:
                family_role: str = family_details[0].get_FamilyRole_field()
                if "Proband" == family_role:
                    proband_samples.append(sample)
        if not proband_samples:
            self.callback.ok_dialog("Error", "There are no proband samples under the patient's family.")
            return None

        # Get all the samples currently under the order to avoid showing them in the prompt.
        self.rel_man.load_children_of_type([order], SampleModel)
        existing_samples: list[SampleModel] = order.get_children_of_type(SampleModel)
        record_ids: list[int] = [x.record_id for x in existing_samples]
        samples_to_show: list[SampleModel] | None = [x for x in proband_samples if x.record_id not in record_ids]
        if not samples_to_show:
            self.callback.ok_dialog("Error", "All proband samples under this order's patient are already under this "
                                             "order.")
            return None

        # Generate the record selection prompt and prompt the user to select samples.
        display_fields = [
            SampleModel.SAMPLEID__FIELD_NAME.field_name,
            SampleModel.OTHERSAMPLEID__FIELD_NAME.field_name,
            SampleModel.CONCENTRATION__FIELD_NAME.field_name,
            SampleModel.CONCENTRATIONUNITS__FIELD_NAME.field_name,
            SampleModel.VOLUME__FIELD_NAME.field_name,
            SampleModel.VOLUMEUNITS__FIELD_NAME.field_name,
            SampleModel.SAMPLESOURCE__FIELD_NAME.field_name,
            SampleModel.EXEMPLARSAMPLETYPE__FIELD_NAME.field_name,
            SampleModel.PRENATAL__FIELD_NAME.field_name
        ]
        selected_samples: list[SampleModel] | None = (
            self.callback.record_selection_dialog("Select a proband sample to add to the order.", display_fields,
                                                  samples_to_show, multi_select=False))
        if not selected_samples:
            return None
        return selected_samples[0]

    def _check_unique_tube_barcode(self, tube_barcode: str) -> bool:
        term: RawReportTerm = RawReportTerm(
            SampleModel.DATA_TYPE_NAME,
            SampleModel.TUBEBARCODE__FIELD_NAME.field_name,
            RawTermOperation.EQUAL_TO_OPERATOR,
            tube_barcode,
            is_negated=False
        )
        column_list: list[ReportColumn] = [ReportColumn(SampleModel.DATA_TYPE_NAME, "TubeBarcode", FieldType.STRING)]
        criteria: CustomReportCriteria = CustomReportCriteria(column_list, term)
        results: list[list[Any]] | None = (DataMgmtServer.get_custom_report_manager(self.user)
                                           .run_custom_report(criteria).result_table)
        if results:
            return False
        return True

    def _get_associated_samples(self, patient, request) -> [SampleModel]:
        self.rel_man.load_children_of_type([patient], RequestModel)
        patients_requests = patient.get_children_of_type(RequestModel)
        self.rel_man.load_children_of_type(patients_requests, SampleModel)
        samples: [SampleModel] = []
        for patients_request in patients_requests:
            if patients_request.record_id != request.record_id:
                associate_samples = patients_request.get_children_of_type(SampleModel)
                if associate_samples:
                    samples += associate_samples
        samples = list(set(samples))
        samples_already_present = request.get_children_of_type(SampleModel)
        if samples_already_present:
            result_samples: [SampleModel] = []
            samples_record_ids_already_present = [s.record_id for s in samples_already_present]
            for sample in samples:
                if sample.record_id not in samples_record_ids_already_present:
                    result_samples.append(sample)
            return result_samples
        else:
            return samples

    def _get_trio_samples(self, patient: PatientModel) -> list[SampleModel]:
        self.rel_man.load_children_of_type([patient], SampleModel)
        samples: list[SampleModel] = patient.get_children_of_type(SampleModel)
        family_map: dict[FamilyModel, list[SampleModel]] = (
            FamilyUtils.get_family_map(self.rec_handler, self.rel_man, samples))
        if not family_map:
            return None

        families: list[FamilyModel] = list(family_map.keys())
        family_samples: list[SampleModel] = (
            FamilyUtils.get_family_samples(self.rec_handler, self.rel_man, families[0].get_FamilyId_field()))
        if not family_samples:
            raise Exception(f"Samples not found for the family id {families[0].get_FamilyId_field()}")

        return family_samples

    def _get_latest_sample_name_num(self, samples: list[SampleModel] | None, order_name: str) -> int:
        if not samples:
            return 1
        sample_names: list[str] | None = \
            [x.get_OtherSampleId_field() for x in samples if f"{order_name}-" in x.get_OtherSampleId_field()]
        if not sample_names:
            return 1
        return max([int(x.split("-")[1]) for x in sample_names]) + 1
