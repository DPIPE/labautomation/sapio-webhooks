import json
from typing import Any, cast

from pandas import DataFrame
from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.CustomReportService import CustomReportManager
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.pojo.CustomReport import (CustomReportCriteria, ReportColumn, RawReportTerm, RawTermOperation,
                                               CompositeReportTerm, CompositeTermOperation)
from sapiopylib.rest.pojo.datatype.FieldDefinition import (VeloxStringFieldDefinition, VeloxPickListFieldDefinition,
                                                           VeloxDateFieldDefinition, FieldType)
from sapiopylib.rest.pojo.datatype.TemporaryDataType import TemporaryDataType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import FormEntryDialogRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, OptionDialogResult, \
    DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.FormBuilder import FormBuilder

from utilities.data_models import PatientModel, RequestModel
from utilities.webhook_handler import OsloWebhookHandler


STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_SEARCH_CRITERIA: str = "searchCriteria"
STATUS_CONFIRM: str = "confirm"
STATUS_PATIENT_SELECT: str = "patientSelect"


class RegisterPatient(OsloWebhookHandler):
    rec_handler: RecordHandler
    custom_report_man: CustomReportManager

    response_map: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.custom_report_man = DataMgmtServer.get_custom_report_manager(context.user)

        result: ClientCallbackResult = context.client_callback_result
        self.response_map = {STATUS: ""}

        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)

            # If there are any errors, return a webhook result of true, since a popup will already have been shown to
            # the user.
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_SEARCH_CRITERIA:
                return self.process_response(context, cast(FormEntryDialogResult, result), order)
            if self.response_map[STATUS] == STATUS_CONFIRM:
                option_result: OptionDialogResult = cast(OptionDialogResult, result)
                if option_result.selection == 0:
                    return self.add_new_patient(order)
                else:
                    return SapioWebhookResult(True)
            # [OSLO-1063]: Add the selected patient from the patient selection dialog to the order.
            if self.response_map[STATUS] == STATUS_PATIENT_SELECT:
                return self.add_selected_patient(order, cast(DataRecordSelectionResult, result))

        # Check if a patient already exists on the order. If there isn't a patient, prompt the user to enter search
        # criteria to find a patient to add.
        self.rel_man.load_parents_of_type([order], PatientModel)
        if order.get_parent_of_type(PatientModel):
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Warning", "This order is already linked to a patient.",
                                          request_context=json.dumps(self.response_map))
        return self.prompt_form()

    # noinspection PyMethodMayBeStatic
    def prompt_form(self) -> SapioWebhookResult:
        """
        Prompt the form to the user where they can enter patient information
        :return: FormEntryDialogRequest
        """
        form_builder: FormBuilder = FormBuilder()

        ssn_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                           "SSN",
                                                                           "National ID")
        ssn_field.editable = True
        form_builder.add_field(ssn_field)

        first_name_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                  "FirstName",
                                                                                  "First Name")
        first_name_field.editable = True
        form_builder.add_field(first_name_field)

        last_name_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                 "LastName",
                                                                                 "Last Name")
        last_name_field.editable = True
        form_builder.add_field(last_name_field)

        sex_field: VeloxPickListFieldDefinition = VeloxPickListFieldDefinition(form_builder.get_data_type_name(),
                                                                               "Sex",
                                                                               "Sex",
                                                                               "Sex")
        sex_field.editable = True
        form_builder.add_field(sex_field)

        address_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                               "Address",
                                                                               "Address")
        address_field.editable = True
        form_builder.add_field(address_field)

        postal_code_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                   "PostalCode",
                                                                                   "Postal Code/Zip Code")
        postal_code_field.editable = True
        form_builder.add_field(postal_code_field)

        hospitalized_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                    "Hospitalized",
                                                                                    "Hospitalized/Outpatient")
        hospitalized_field.editable = True
        form_builder.add_field(hospitalized_field)

        reason_for_referral_field: VeloxPickListFieldDefinition = VeloxPickListFieldDefinition(
            form_builder.get_data_type_name(),
            "ReasonForReferral",
            "Reason for Referral",
            "Reason for Referral")
        reason_for_referral_field.editable = True
        form_builder.add_field(reason_for_referral_field)

        requester_address_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(
            form_builder.get_data_type_name(),
            "RequesterAddress",
            "Requester Institution/Organization Address")
        requester_address_field.editable = True
        form_builder.add_field(requester_address_field)

        referring_doctor_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(
            form_builder.get_data_type_name(),
            "ReferringDoctor",
            "Name of Referring Doctor")
        referring_doctor_field.editable = True
        form_builder.add_field(referring_doctor_field)

        hpr_number_field: VeloxStringFieldDefinition = VeloxStringFieldDefinition(form_builder.get_data_type_name(),
                                                                                  "HPRNumber",
                                                                                  "HPR Number")
        hpr_number_field.editable = True
        form_builder.add_field(hpr_number_field)

        date_sample_received_field: VeloxDateFieldDefinition = VeloxDateFieldDefinition(
            form_builder.get_data_type_name(),
            "DateOfSampleReceived",
            "Date of the Sample Received",
            "MMM dd, yyyy HH:mm")
        date_sample_received_field.editable = True
        form_builder.add_field(date_sample_received_field)

        temp_dt: TemporaryDataType = form_builder.get_temporary_data_type()

        self.response_map[STATUS] = STATUS_SEARCH_CRITERIA
        request: FormEntryDialogRequest = FormEntryDialogRequest("Enter Patient Info",
                                                                 "Enter the required patient info.", temp_dt,
                                                                 callback_context_data=json.dumps(self.response_map))
        return SapioWebhookResult(True, client_callback_request=request)

    def process_response(self, context: SapioWebhookContext,
                         result: FormEntryDialogResult, order: RequestModel) -> SapioWebhookResult:
        """
        Run the custom report and get a list of patients. Prompt for patient creation if none are found. If multiple
        patients are found matching the criteria, return a record selection popup for the user to select a patient.
        :param context: SapioWebhookContext
        :param result: FormEntryDialogResult with data entered by the user
        :param order: The order the potential patient will be under.
        :return: SapioWebhookResult
        """
        self.response_map: dict[str, Any] = result.user_response_map

        self.response_map[STATUS] = "confirm"
        self.response_map.update(result.user_response_map)

        # Find if the patient record exists in the system. If it doesn't exist, then create one with the fields
        # specified in the form
        custom_report: CustomReportCriteria = self.build_custom_report()
        if custom_report is None:
            return SapioWebhookResult(True)

        result_frame: DataFrame = self.custom_report_man.run_custom_report(custom_report).get_data_frame()

        if result_frame.size == 0:
            return PopupUtil.yes_no_popup("Confirm", "No patients matching the criteria entered were found. Would you"
                                                     " like to create a new patient?",
                                          request_context=json.dumps(self.response_map))

        else:
            ssns: list[int] = []
            for x in range(0, result_frame.index.size):
                ssns.append(result_frame.iat[x, 3])

            patients: list[PatientModel] = (
                self.rec_handler.query_models(PatientModel, PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                              ssns))

            if len(patients) == 1:
                order.add_parent(patients[0])
                self.rec_man.store_and_commit()
                return SapioWebhookResult(True)

            # [OSLO-1063]: Return a record prompt for the user to select a patient.
            self.response_map[STATUS] = STATUS_PATIENT_SELECT
            return PopupUtil.record_selection_popup(context, "Multiple patients matching the criteria were found."
                                                             " Please select one.",
                                                    ["SocialSecurityNumber", "FirstName", "LastName", "Sex", "Address",
                                                     "PostalCode",
                                                     "Hospitalized", "ReasonForReferral", "RequestersInstitution",
                                                     "ReferringDoctor", "HPRNumber", "RecordId"],
                                                    [x.get_data_record() for x in patients], False,
                                                    request_context=json.dumps(self.response_map))

    # noinspection PyMethodMayBeStatic
    def build_custom_report(self) -> CustomReportCriteria | None:
        """
        Builds a custom report based on the data entered in the form
        :return: CustomReportCriteria with all non-blank fields
        """
        column_list: list[ReportColumn] = [
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.FIRSTNAME__FIELD_NAME.field_name, FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.LASTNAME__FIELD_NAME.field_name, FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.SEX__FIELD_NAME.field_name, FieldType.PICKLIST),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.ADDRESS__FIELD_NAME.field_name, FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.POSTALCODE__FIELD_NAME.field_name, FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.HOSPITALIZED__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.REASONFORREFERRAL__FIELD_NAME.field_name,
                         FieldType.PICKLIST),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.REQUESTERSINSTITUTION__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.REFERRINGDOCTOR__FIELD_NAME.field_name,
                         FieldType.STRING),
            ReportColumn(PatientModel.DATA_TYPE_NAME, PatientModel.HPRNUMBER__FIELD_NAME.field_name,
                         FieldType.STRING)]

        terms: list[RawReportTerm] = []

        if self.response_map["SSN"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.SOCIALSECURITYNUMBER__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["SSN"]))

        if self.response_map["FirstName"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME, PatientModel.FIRSTNAME__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["FirstName"]))

        if self.response_map["LastName"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME, PatientModel.LASTNAME__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["LastName"]))

        if self.response_map["Sex"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME, PatientModel.SEX__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["Sex"]))

        if self.response_map["Address"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME, PatientModel.ADDRESS__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["Address"]))

        if self.response_map["PostalCode"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.POSTALCODE__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["PostalCode"]))

        if self.response_map["Hospitalized"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.HOSPITALIZED__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["Hospitalized"]))

        if self.response_map["ReasonForReferral"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.REASONFORREFERRAL__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["ReasonForReferral"]))

        if self.response_map["RequesterAddress"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.REQUESTERSINSTITUTION__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["RequesterAddress"]))

        if self.response_map["ReferringDoctor"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.REFERRINGDOCTOR__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["ReferringDoctor"]))

        if self.response_map["HPRNumber"] != "":
            terms.append(RawReportTerm(PatientModel.DATA_TYPE_NAME,
                                       PatientModel.HPRNUMBER__FIELD_NAME.field_name,
                                       RawTermOperation.EQUAL_TO_OPERATOR, self.response_map["HPRNumber"]))

        if len(terms) == 0:
            return None

        if len(terms) == 1:
            return CustomReportCriteria(column_list, terms[0])

        if len(terms) >= 2:
            root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeTermOperation.AND_OPERATOR,
                                                                 terms[1])

            for x in range(2, len(terms)):
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.AND_OPERATOR, terms[x])

            return CustomReportCriteria(column_list, root_term)

    def add_new_patient(self, order: RequestModel) -> SapioWebhookResult:
        """
        Adds a new patient record to the system
        :param order: The order that the patient will be a parent of
        :return: Directive to the new patient record
        """
        # Add the new patient record
        patient: PatientModel = self.rec_handler.add_model(PatientModel)

        if self.response_map["FirstName"] != "":
            patient.set_FirstName_field(self.response_map["FirstName"])

        if self.response_map["LastName"] != "":
            patient.set_LastName_field(self.response_map["LastName"])

        if self.response_map["Sex"] != "":
            patient.set_Sex_field(self.response_map["Sex"])

        if self.response_map["SSN"] != "":
            patient.set_SocialSecurityNumber_field(self.response_map["SSN"])

        if self.response_map["Address"] != "":
            patient.set_Address_field(self.response_map["Address"])

        if self.response_map["PostalCode"] != "":
            patient.set_PostalCode_field(self.response_map["PostalCode"])

        if self.response_map["Hospitalized"] != "":
            patient.set_Hospitalized_field(self.response_map["Hospitalized"])

        if self.response_map["ReasonForReferral"] != "":
            patient.set_ReasonForReferral_field(self.response_map["ReasonForReferral"])

        if self.response_map["RequesterAddress"] != "":
            patient.set_RequestersInstitution_field(self.response_map["RequesterAddress"])

        if self.response_map["ReferringDoctor"] != "":
            patient.set_ReferringDoctor_field(self.response_map["ReferringDoctor"])

        if self.response_map["HPRNumber"] != "":
            patient.set_HPRNumber_field(self.response_map["HPRNumber"])

        if self.response_map["DateOfSampleReceived"] is not None:
            patient.set_DateOfSampleReceived_field(int(self.response_map["DateOfSampleReceived"]))

        # Add the patient as a parent of the order
        order.add_parent(patient)

        # Store and commit changes
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, "New patient created in system.",
                                  directive=FormDirective(patient.get_data_record()))

    def add_selected_patient(self, order: RequestModel, result: DataRecordSelectionResult) -> SapioWebhookResult:
        field_maps: list[dict[str, Any]] = cast(DataRecordSelectionResult, result).selected_field_map_list
        if not field_maps:
            return SapioWebhookResult(True)
        patient: PatientModel = self.rec_handler.query_models_by_id(PatientModel, [field_maps[0]["RecordId"]])[0]
        patient.add_child(order)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
