import json
import re

from typing import cast

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.general.time_util import TimeUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import DataRecordSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.PyRecordModel import PyRecordModel

from utilities.data_models import TestCodeConfigurationModel, AssayDetailModel, RequestModel, InvoiceModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_ASSAY_SELECT: str = "assaySelect"
STATUS_CANNOT_QUEUE: str = "cannotQueue"


class SelectAssay(OsloWebhookHandler):
    data_type_man: DataTypeManager
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.data_type_man = DataMgmtServer.get_data_type_manager(self.user)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR or self.response_map[STATUS] == STATUS_CANNOT_QUEUE:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_ASSAY_SELECT:
                return self.process_input(cast(DataRecordSelectionResult, result), context)

        return self.prompt_data_record_selection(context)

    def prompt_data_record_selection(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Get all the test code configs in the system
        test_code_configs: list[TestCodeConfigurationModel] = (
            self.rec_handler.query_all_models(TestCodeConfigurationModel))
        if not test_code_configs:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.ok_popup("Error", "There are no Test Code Configurations present in the system.",
                                      request_context=json.dumps(self.response_map))

        # Get all the fields for the test code configs
        fields: list[str] = [
            "Assay",
            "TestCodeID",
            "Keywords",
            "Indications",
            "AnalysisLabel",
            "Unit",
            "C_Array",
            "ProbeMix",
            "Description",
            "FreeText",
            "DateCreated",
            "CreatedBy",
            "VeloxLastModifiedDate",
            "VeloxLastModifiedBy"
        ]

        # Return data record selection dialog for searching through test code configs
        self.response_map[STATUS] = STATUS_ASSAY_SELECT
        return PopupUtil.record_selection_popup(context, "Test Code Configurations", fields, test_code_configs, True,
                                                request_context=json.dumps(self.response_map))

    def process_input(self, result: DataRecordSelectionResult, context: SapioWebhookContext) -> SapioWebhookResult:
        # Simply close the dialog like nothing happened if no records are selected
        if len(result.selected_field_map_list) == 0:
            return SapioWebhookResult(True)

        # Get the test code configs of the selections and add new assay details records as children of the order
        test_code_ids: list[str] = []
        for r in result.selected_field_map_list:
            test_code_ids.append(r["TestCodeID"])
        test_code_configurations: list[TestCodeConfigurationModel] = (
            self.rec_handler.query_models(TestCodeConfigurationModel,
                                          TestCodeConfigurationModel.TESTCODEID__FIELD_NAME.field_name, test_code_ids))

        # Get the order
        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)

        # Only add the AssayDetail as a child of the order's invoice if there isn't already an AssayDetail with the
        # same test code under the invoice. Also, make each invoice unique per assay name
        assay_details: list[AssayDetailModel] = []
        self.rel_man.load_children_of_type([order], InvoiceModel)
        invoices: list[InvoiceModel] = order.get_children_of_type(InvoiceModel)
        for test_code_config in test_code_configurations:
            if invoices is not None:
                self.rel_man.load_children_of_type(invoices, AssayDetailModel)
                for i in invoices:
                    assay_details.extend(i.get_children_of_type(AssayDetailModel))
                try:
                    invoice: InvoiceModel = \
                        [i for i in invoices if i.get_AssayName_field() == test_code_config.get_Assay_field()][0]
                    assay_children: list[AssayDetailModel] = invoice.get_children_of_type(AssayDetailModel)
                    test_code_id_exists: bool = False
                    for a in assay_children:
                        if a.get_TestCodeId_field() == test_code_config.get_TestCodeID_field():
                            test_code_id_exists = True
                            break
                    if test_code_id_exists:
                        continue
                    assay_detail: AssayDetailModel | None = (
                        self.create_new_assay_detail(context, test_code_config, order))
                    if not assay_detail:
                        return SapioWebhookResult(False, f"No extension type for "
                                                         f"\"{test_code_config.get_Assay_field()}\" "
                                                         f"exists in the system.")
                    invoice.add_child(assay_detail)
                    assay_details.append(assay_detail)
                except Exception:
                    invoice: InvoiceModel = self.create_invoice(order, test_code_config.get_Assay_field())
                    order.add_child(invoice)
                    assay_detail: AssayDetailModel | None = (
                        self.create_new_assay_detail(context, test_code_config, order))
                    if not assay_detail:
                        return SapioWebhookResult(False, f"No extension type for "
                                                         f"\"{test_code_config.get_Assay_field()}\" "
                                                         f"exists in the system.")
                    invoice.add_child(assay_detail)
                    assay_details.append(assay_detail)
            else:
                invoice: InvoiceModel = self.create_invoice(order, test_code_config.get_Assay_field())
                order.add_child(invoice)
                assay_detail: AssayDetailModel | None = self.create_new_assay_detail(context, test_code_config, order)
                if not assay_detail:
                    return SapioWebhookResult(False, f"No extension type for "
                                                     f"\"{test_code_config.get_Assay_field()}\" "
                                                     f"exists in the system.")
                invoice.add_child(assay_detail)
                assay_details.append(assay_detail)

        # Store and commit changes
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, display_text="Successfully added assay to the order")

    def create_new_assay_detail(self, context: SapioWebhookContext, test_code_config: TestCodeConfigurationModel,
                                order: RequestModel) -> AssayDetailModel | None:
        # [OSLO-1020]: Add the proper assay detail extension to the order depending on the analysis selected.
        assay: str = re.sub("[^a-zA-Z]+", "", test_code_config.get_Assay_field())
        extension_type: str = f"{assay}AssayDetail"
        if extension_type not in DataMgmtServer.get_data_type_manager(context.user).get_data_type_name_list():
            return None

        assay_detail: AssayDetailModel = self.rec_handler.add_model(AssayDetailModel)
        assay_detail.set_TestCodeId_field(test_code_config.get_TestCodeID_field())
        assay_detail.set_ProbeMix_field(test_code_config.get_ProbeMix_field())
        # [OSLO-705]: Set the Assay field on the record so that we can check it in other parts of the system.
        assay_detail.set_Assay_field(test_code_config.get_Assay_field())
        assay_detail.set_AnalysisLabel_field(test_code_config.get_AnalysisLabel_field())

        order_model: PyRecordModel = self.inst_man.unwrap(order)
        self.rel_man.load_children([order_model], extension_type)
        assay_detail_ext = order_model.get_child_of_type(extension_type)
        if not assay_detail_ext:
            assay_detail_ext: PyRecordModel = self.inst_man.add_new_record(extension_type)

        assay_detail_ext.set_field_value("AnalysisCreationDate", TimeUtil.now_in_millis())
        assay_detail_ext.set_field_value("Priority", order.get_Priority_field())

        if assay_detail.get_AnalysisLabel_field().__contains__("Trio"):
            trio_id: str = RequestUtils.generate_trio_id(context)
            assay_detail_ext.set_field_value("TrioId", trio_id)

        # [OSLO-1074]: Copy the array field from the analysis code to the assay detail extension.
        if test_code_config.get_Assay_field() == "aCGH":
            assay_detail_ext.set_field_value("C_Array", test_code_config.get_C_Array_field())
            assay_detail_ext.set_field_value("AnalysisLabel", test_code_config.get_AnalysisLabel_field())
            assay_detail.set_C_Array_field(test_code_config.get_C_Array_field())

        # Copy the probe mix field from the analysis code to the assay detail extension.
        if "ProbeMix" in [x.data_field_name for x in self.data_type_man.get_field_definition_list(extension_type)]:
            assay_detail_ext.set_field_value("ProbeMix", test_code_config.get_ProbeMix_field())

        order.backing_model.add_child(assay_detail_ext)
        return assay_detail

    def create_invoice(self, order: RequestModel, assay_name: str) -> InvoiceModel:
        invoice: InvoiceModel = self.rec_handler.add_model(InvoiceModel)
        invoice.set_InvoiceId_field(f"{order.get_RequestName_field()}{assay_name}")
        invoice.set_AssayName_field(assay_name)
        return invoice
