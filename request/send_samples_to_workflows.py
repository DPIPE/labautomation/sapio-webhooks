from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RelationshipPath import RelationshipPath

from utilities.data_models import RequestModel, AssayDetailModel, InvoiceModel, SampleModel, \
    SampleRegistrationErrorModel
from utilities.request_utils import OrderStatus
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class SendSamplesToWorkflows(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        # Check that the order has been approved, and show an error to the user if it hasn't.
        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        if (order.get_OrderStatus_field() != OrderStatus.ORDER_APPROVED.value and
                order.get_OrderStatus_field() != OrderStatus.ORDER_APPROVAL_ERRORS.value):
            try:
                self.callback.ok_dialog("Warning",
                                        "This order has not yet been approved, so samples cannot be sent to workflows.")
            except Exception:
                ...
            return SapioWebhookResult(True)

        # Get all the samples under the order.
        self.rel_man.load_children_of_type([order], SampleModel)
        samples: list[SampleModel] = order.get_children_of_type(SampleModel)
        if not samples:
            return SapioWebhookResult(True)

        # Wrap the assay details records and error to the user if there aren't any assays for the order.
        self.rel_man.load_path_of_type([order],
                                       RelationshipPath().child_type(InvoiceModel).child_type(AssayDetailModel))
        invoices: list[InvoiceModel] = order.get_children_of_type(InvoiceModel)
        if not invoices:
            try:
                self.callback.ok_dialog("Warning", "This order does not have any assays selected for it.")
            except Exception:
                ...
            return SapioWebhookResult(True)

        assay_details: list[AssayDetailModel] = []
        for i in invoices:
            assay_details.extend(i.get_children_of_type(AssayDetailModel))

        # Load all the orders
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
            WorkflowUtils.get_orders_to_assay_details(self.rel_man, assay_details))

        # [OSLO-968]: Get all the direct DNA and EDTA Blood children and aliquots of non-DNA samples.
        dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "DNA"]
        edta_blood_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "EDTA Blood"]
        # [OSLO-1118]: Get all the direct plasma samples under the order so that they can be queued for ELISA.
        plasma_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "Plasma"]
        # Get all direct PAX Blood samples for RNA Analysis
        pax_blood_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() == "PAX Blood"]
        non_dna_samples: list[SampleModel] = [s for s in samples if s.get_ExemplarSampleType_field() != "DNA"
                                              and s.get_ExemplarSampleType_field() != "EDTA Blood"
                                              and s.get_ExemplarSampleType_field() != "Plasma"]

        self.rel_man.load_children_of_type(non_dna_samples, SampleModel)
        dna_aliquots: list[SampleModel] = []
        for s in non_dna_samples:
            if (s.get_child_of_type(SampleModel)
                    and s.get_child_of_type(SampleModel).get_ExemplarSampleType_field() == "DNA"):
                dna_aliquots.append(s.get_child_of_type(SampleModel))

        # [OSLO-994]: Delete all current sample registration errors under the order.
        self.rel_man.load_children_of_type([order], SampleRegistrationErrorModel)
        order.remove_children(order.get_children_of_type(SampleRegistrationErrorModel))

        # Send each of the order's samples to their assigned workflows
        filtered_samples_list: list[SampleModel] = ([d for d in dna_aliquots if d]
                                                    + edta_blood_samples
                                                    + dna_samples
                                                    + plasma_samples
                                                    + pax_blood_samples)
        errors: dict[RequestModel, list[SampleRegistrationErrorModel]] = (
            WorkflowUtils.send_samples_to_workflows(context, self.rec_handler, self.rel_man, orders_to_assay_details,
                                                    filtered_samples_list))

        for order in errors:
            filtered_errors: list[SampleRegistrationErrorModel] = [x for x in errors[order] if x]

            # [OSLO-994]: For each error, create a sample registration error record under the order to log it.
            # [OSLO-1136]: Set the order status appropriately.
            order.set_SampleRegistrationSuccessful_field(not filtered_errors)
            if filtered_errors:
                order.add_children(filtered_errors)
                order.set_OrderStatus_field(OrderStatus.ORDER_APPROVAL_ERRORS.value)
            else:
                order.set_OrderStatus_field(OrderStatus.ORDER_APPROVED.value)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)
