import json
from typing import Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.datatype.FieldDefinition import VeloxStringFieldDefinition, VeloxDoubleFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import SampleModel, ExemplarConfigModel, RequestModel, AssayDetailModel, \
    AssignedProcessModel, TestCodeConfigurationModel
from utilities.request_utils import RequestUtils
from utilities.webhook_handler import OsloWebhookHandler
from utilities.workflow_utils import WorkflowUtils


class CheckSampleConcentrationsForWGS(OsloWebhookHandler):
    rec_handler: RecordHandler

    response_data: dict[str, Any]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        result: ClientCallbackResult = context.client_callback_result

        if result is not None:
            self.response_data = json.loads(result.callback_context_data)

            if self.response_data["status"] == "error":
                return SapioWebhookResult(True)

            if self.response_data["status"] == "notice":
                return SapioWebhookResult(True)

        self.rec_handler = RecordHandler(context)

        self.response_data = {"status": ""}

        # Get the samples and load their assigned process's parents
        order: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        self.rel_man.load_children_of_type([order], SampleModel)

        samples: list[SampleModel] = order.get_children_of_type(SampleModel)
        if samples is None:
            self.response_data["status"] = "error"
            return PopupUtil.ok_popup("Warning", "This order has no samples.",
                                      request_context=json.dumps(self.response_data))

        self.rel_man.load_parents_of_type(samples, AssignedProcessModel)

        # Get the exemplar config record and the low concentration threshold
        exemplar_config: ExemplarConfigModel = self.rec_handler.query_all_models(ExemplarConfigModel)[0]
        threshold: float = exemplar_config.get_WGSLowConcentrationThreshold_field()

        # Get the assays of the order
        orders_to_assay_details: dict[RequestModel, list[AssayDetailModel]] = (
            RequestUtils.get_orders_to_assay_details(self.rel_man, [order]))

        # If the order is not selected for WGS, throw an error to the user
        selected_for_wgs: bool = False

        assay_details_to_analysis_configurations: dict[AssayDetailModel, TestCodeConfigurationModel] = (
            WorkflowUtils.get_matching_analysis_configurations(self.rec_handler, orders_to_assay_details[order]))

        for a in orders_to_assay_details[order]:
            if assay_details_to_analysis_configurations[a].get_Assay_field() == "WGS":
                selected_for_wgs = True
                break

        if not selected_for_wgs:
            self.response_data["status"] = "error"
            return PopupUtil.ok_popup("Warning", "This order is not selected for the WGS workflow.",
                                      request_context=json.dumps(self.response_data))

        # If the sample's concentration does not meet the threshold, throw an error to the user. Otherwise, queue the
        # sample for WGS if it meets the other qualifications
        disqualifying_samples: list[dict[str, str]] = []

        for s in samples:
            conc: float = s.get_Concentration_field()

            if conc is None or conc < threshold:
                disqualifying_samples.append({
                    "SampleId": s.get_SampleId_field(),
                    "Concentration": conc,
                    "Units": s.get_ConcentrationUnits_field()
                })

            else:
                WorkflowUtils.send_samples_to_workflows(context, self.rec_handler, self.rel_man,
                                                        orders_to_assay_details, s, assay_names=["WGS"])

        # Return a client callback to the user if there are any disqualifying samples
        if len(disqualifying_samples) > 0:
            self.response_data["status"] = "error"
            return PopupUtil.table_popup("Warning", "The following sample pending to be queued for WGS has too low of"
                                                    " a concentration, and therefore cannot be queued. Other samples"
                                                    " not listed here have valid concentrations, and an attempt was "
                                                    " made to send them to WGS. If any of them aren't queued, check the"
                                                    " family of the sample.",
                                         [VeloxStringFieldDefinition("Warning", "SampleId", "Sample ID"),
                                          VeloxDoubleFieldDefinition("Warning", "Concentration", "Concentration"),
                                          VeloxStringFieldDefinition("Warning", "Units", "Units")],
                                         disqualifying_samples,
                                         request_context=json.dumps(self.response_data))

        self.response_data["status"] = "notice"
        return PopupUtil.ok_popup("Notice", "All sample concentrations qualify for WGS. An attempt was made to send"
                                            " the queue the sample. If it is not queued, check the family of the"
                                            " sample.", request_context=json.dumps(self.response_data))
