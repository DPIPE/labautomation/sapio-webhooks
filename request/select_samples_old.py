import json
from typing import cast, Any, List

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.pojo.CustomReport import RawReportTerm, RawTermOperation, CustomReportCriteria, ReportColumn, \
    CompositeReportTerm, CompositeTermOperation
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType, VeloxStringFieldDefinition, \
    VeloxDoubleFieldDefinition, VeloxPickListFieldDefinition, VeloxBooleanFieldDefinition
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import DataRecordSelectionRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import DataRecordSelectionResult, FormEntryDialogResult, \
    OptionDialogResult, TableEntryDialogResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult
from sapiopylib.rest.utils.recordmodel.RecordModelUtil import RecordModelUtil

from utilities import utils
from utilities.data_models import SampleModel, RequestModel, PatientModel, FamilyDetailsModel, FamilyModel, \
    InvoiceModel, AssayDetailModel, ProbeMixSampleModel
from utilities.family_utils import FamilyUtils
from utilities.webhook_handler import OsloWebhookHandler

"""
Ticket : OSLO-632
Description : Data Record Form Button On Request To Link Samples Fitting The Criteria
"""

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_SOURCE_PROMPT: str = "sourcePrompt"
STATUS_NUM_SAMPLES_PROMPT: str = "numSamplesPrompt"
STATUS_SAMPLE_DATA_PROMPT: str = "sampleDataPrompt"
STATUS_SAMPLE_SELECT_PROMPT: str = "sampleSelect"


class SelectSamples(OsloWebhookHandler):
    ORIGINAL_RECORD_ID = "ORIGINAL_RECORD_ID"
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        request: RequestModel = self.rec_handler.wrap_model(context.data_record, RequestModel)
        self.rel_man.load_parents_of_type([request], PatientModel)
        patient: PatientModel = request.get_parent_of_type(PatientModel)

        # Get already added samples to avoid error while adding it again
        self.rel_man.load_children_of_type([request], SampleModel)
        existing_samples = request.get_children_of_type(SampleModel)

        result: ClientCallbackResult = context.client_callback_result
        try:
            if result:
                if result.user_cancelled:
                    return SapioWebhookResult(True)
                if isinstance(result, OptionDialogResult) and (result.button_text == 'OK'):
                    return SapioWebhookResult(True)
                self.response_map = json.loads(result.callback_context_data)
                if self.response_map[STATUS] == STATUS_ERROR:
                    return SapioWebhookResult(True)
                if self.response_map[STATUS] == STATUS_SOURCE_PROMPT:
                    return self.process_source_selection(context, patient, request, cast(OptionDialogResult, result),
                                                         existing_samples)
                if self.response_map[STATUS] == STATUS_NUM_SAMPLES_PROMPT:
                    return self.process_num_samples_prompt(cast(FormEntryDialogResult, result))
                if self.response_map[STATUS] == STATUS_SAMPLE_SELECT_PROMPT:
                    return self.process_sample_selection(patient, request, cast(DataRecordSelectionResult, result),
                                                         existing_samples)
                if self.response_map[STATUS] == STATUS_SAMPLE_DATA_PROMPT:
                    return self.process_sample_data_prompt(context, request, cast(TableEntryDialogResult, result))

            # provide user option to create a new sample record or add existing sample record linked to patient if they
            # exist
            if not patient:
                self.response_map[STATUS] = STATUS_ERROR
                return PopupUtil.ok_popup("Error", "No Patient Associated With Order",
                                          request_context=json.dumps(self.response_map))
            self.response_map[STATUS] = STATUS_SOURCE_PROMPT
            return PopupUtil.option_popup("Select Samples To Add", "Choose Source",
                                          ["Add New Samples", "Add Existing Samples", "Add Proband (Trio) Sample"],
                                          request_context=json.dumps(self.response_map), user_can_cancel=True)
        except Exception as e:
            return PopupUtil.ok_popup("Error", str(e), request_context=json.dumps(self.response_map))

    def process_source_selection(self, context: SapioWebhookContext, patient: PatientModel, request: RequestModel,
                                 result: OptionDialogResult, existing_samples) -> SapioWebhookResult:
        if result.button_text not in ["Add New Samples", "Add Existing Samples", "Add Proband (Trio) Sample"]:
            return SapioWebhookResult(True)
        selected_option = result.button_text
        record_ids = RecordModelUtil.get_value_list(existing_samples, "RecordId")

        # perform as per option
        if selected_option == "Add New Sample":
            self.response_map[STATUS] = STATUS_NUM_SAMPLES_PROMPT
            return PopupUtil.integer_field_popup("Add Sample", "Enter the number of samples to create.",
                                                 "Number of Samples", default_value=1,
                                                 request_context=json.dumps(self.response_map))
        elif selected_option == "Add Existing Sample":
            # get associated samples
            samples = self.get_associated_samples(patient, request)
            if not samples:
                return PopupUtil.ok_popup("Error", "No Associated Samples Found For Patient, "
                                                   "Please Register New Ones")
            samples_to_show = []
            for sample in samples:
                if sample.record_id not in record_ids:
                    tmp = sample.fields.copy_to_dict()
                    tmp.update({self.ORIGINAL_RECORD_ID: sample.record_id})
                    samples_to_show.append(tmp)

            # generate the request
            self.response_map[STATUS] = STATUS_SAMPLE_SELECT_PROMPT
            self.response_map["SOURCE"] = "Patient"
            data_type_manager = DataTypeManager(context.user)
            display_fields = [
                SampleModel.SAMPLEID__FIELD_NAME.field_name,
                SampleModel.OTHERSAMPLEID__FIELD_NAME.field_name,
                SampleModel.CONCENTRATION__FIELD_NAME.field_name,
                SampleModel.CONCENTRATIONUNITS__FIELD_NAME.field_name,
                SampleModel.VOLUME__FIELD_NAME.field_name,
                SampleModel.VOLUMEUNITS__FIELD_NAME.field_name,
                SampleModel.SAMPLESOURCE__FIELD_NAME.field_name,
                SampleModel.EXEMPLARSAMPLETYPE__FIELD_NAME.field_name
            ]
            field_def_list = [field for field in data_type_manager.get_field_definition_list(SampleModel.DATA_TYPE_NAME)
                              if field.data_field_name in display_fields]
            request = DataRecordSelectionRequest("Sample", "Samples", field_def_list, samples_to_show,
                                                 "Please Select The Samples To Add", True,
                                                 callback_context_data=json.dumps(self.response_map))
            return SapioWebhookResult(True, client_callback_request=request)
        else:
            # get trio samples
            samples: list[SampleModel] = self.get_trio_samples(patient)

            # [OSLO-900]: Only link the proband to the order itself, then reference the mother/father on the assay level
            proband_samples = []
            self.rel_man.load_children_of_type(samples, FamilyDetailsModel)
            for sample in samples:
                family_details = sample.get_children_of_type(FamilyDetailsModel)
                if family_details:
                    family_role = family_details[0].get_FamilyRole_field()
                    if "Proband" == family_role:
                        proband_samples.append(sample)

            if not proband_samples:
                raise Exception("Proband sample(s) NOT found under the family.")

            samples = proband_samples
            samples_to_show = []
            for sample in samples:
                if sample.record_id not in record_ids:
                    tmp = sample.fields.copy_to_dict()
                    tmp.update({self.ORIGINAL_RECORD_ID: sample.record_id})
                    samples_to_show.append(tmp)

            if not samples_to_show:
                raise Exception("Proband sample is already added to the order.")

            # generate the request
            self.response_map[STATUS] = STATUS_SAMPLE_SELECT_PROMPT
            self.response_map["SOURCE"] = "Trio"
            data_type_manager = DataTypeManager(context.user)
            display_fields = [
                SampleModel.SAMPLEID__FIELD_NAME.field_name,
                SampleModel.OTHERSAMPLEID__FIELD_NAME.field_name,
                SampleModel.CONCENTRATION__FIELD_NAME.field_name,
                SampleModel.CONCENTRATIONUNITS__FIELD_NAME.field_name,
                SampleModel.VOLUME__FIELD_NAME.field_name,
                SampleModel.VOLUMEUNITS__FIELD_NAME.field_name,
                SampleModel.SAMPLESOURCE__FIELD_NAME.field_name,
                SampleModel.EXEMPLARSAMPLETYPE__FIELD_NAME.field_name
            ]
            field_def_list = [field for field in data_type_manager.get_field_definition_list(SampleModel.DATA_TYPE_NAME)
                              if field.data_field_name in display_fields]
            request = DataRecordSelectionRequest("Sample", "Samples", field_def_list, samples_to_show,
                                                 "Please Select The Proband Sample To Add", False,
                                                 callback_context_data=json.dumps(self.response_map))
            return SapioWebhookResult(True, client_callback_request=request)

    def process_num_samples_prompt(self, result: FormEntryDialogResult) -> SapioWebhookResult:
        if not result.user_response_map:
            return SapioWebhookResult(True)
        num_samples: str | None = result.user_response_map["Number of Samples"]
        if not num_samples or num_samples == "0":
            return SapioWebhookResult(True)

        values: list[dict[str, Any]] = []
        for x in range(int(num_samples)):
            values.append({"TubeBarcode": "",
                           "SampleType": "",
                           "Concentration": 0.0,
                           "ConcentrationUnits": "",
                           "Quantity": 0.0,
                           "QuantityUnits": "",
                           "ContainerType": ""})

        tube_barcode_field: VeloxStringFieldDefinition = (
            VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode", unique_value=True))
        tube_barcode_field.editable = True
        sample_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("SampleType", "SampleType", "Sample Type", "Exemplar Sample Types"))
        sample_type_field.editable = True
        sample_type_field.required = True
        sample_source_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("SampleSource", "SampleSource", "Sample Source", "Sample Source"))
        sample_source_type_field.editable = True
        sample_source_type_field.required = True
        concentration_field: VeloxDoubleFieldDefinition = (
            VeloxDoubleFieldDefinition("Concentration", "Concentration", "Concentration"))
        concentration_field.editable = True
        concentration_units_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("ConcentrationUnits", "ConcentrationUnits", "Concentration Units",
                                         "Concentration Units"))
        concentration_units_field.editable = True
        quantity_field: VeloxDoubleFieldDefinition = VeloxDoubleFieldDefinition("Quantity", "Quantity", "Volume")
        quantity_field.editable = True
        quantity_units_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("QuantityUnits", "QuantityUnits", "Quantity Units", "Volume Units"))
        quantity_units_field.editable = True
        container_type_field: VeloxPickListFieldDefinition = (
            VeloxPickListFieldDefinition("ContainerType", "ContainerType", "Container Type", "Container Type List"))
        container_type_field.editable = True
        prenatal_field: VeloxBooleanFieldDefinition = VeloxBooleanFieldDefinition("Prenatal", "Prenatal", "Prenatal?")
        prenatal_field.editable = True
        prenatal_field.default_value = False

        self.response_map[STATUS] = STATUS_SAMPLE_DATA_PROMPT
        return PopupUtil.table_popup("Add Sample", "Enter fields for the new samples.",
                                     [tube_barcode_field, sample_type_field, sample_source_type_field,
                                      concentration_field, concentration_units_field, quantity_field,
                                      quantity_units_field, container_type_field, prenatal_field], values,
                                     request_context=json.dumps(self.response_map))

    def process_sample_data_prompt(self, context: SapioWebhookContext, request: RequestModel,
                                   result: TableEntryDialogResult) -> SapioWebhookResult:
        # Check to make sure all the tube barcodes entered are unique. If any aren't error to the user showing them the
        # invalid barcodes.
        tube_barcodes: list[str] = []
        for i, d in enumerate(result.user_response_data_list):
            tube_barcodes.append(d["TubeBarcode"])
        invalid_barcodes: list[dict[str, str]] | None = self.get_used_tube_barcodes(context, tube_barcodes)
        if invalid_barcodes:
            self.response_map[STATUS] = STATUS_ERROR
            return PopupUtil.table_popup("Error", "The following tube barcode(s) have already been used for samples, so"
                                                  " samples cannot be added.",
                                         [VeloxStringFieldDefinition("TubeBarcode", "TubeBarcode", "Tube Barcode")],
                                         invalid_barcodes, request_context=json.dumps(self.response_map))

        # Get any EDTA Blood samples and Diagnostics samples under the request.
        self.rel_man.load_children_of_type([request], SampleModel)
        accession_samples: list[SampleModel] = [x for x in request.get_children_of_type(SampleModel)
                                                if x.get_ExemplarSampleType_field() == "EDTA Blood"
                                                and x.get_Classification_field() == "Diagnostics"]
        num_accession_samples: int = len(accession_samples) + 1

        # Create the samples with the values entered by the user.
        samples: list[SampleModel] = []
        for i, d in enumerate(result.user_response_data_list):
            sample: SampleModel = self.rec_handler.add_model(SampleModel)
            sample.set_TubeBarcode_field(d["TubeBarcode"])
            sample.set_ExemplarSampleType_field(d["SampleType"])
            sample.set_Concentration_field(float(d["Concentration"]))
            sample.set_ConcentrationUnits_field(d["ConcentrationUnits"])
            sample.set_Volume_field(float(d["Quantity"]))
            sample.set_VolumeUnits_field(d["QuantityUnits"])
            sample.set_ContainerType_field(d["ContainerType"])
            sample.set_Prenatal_field(d["Prenatal"])

            # [OSLO-1067]: If the sample is a prenatal sample, then add a prenatal extension child to the sample.
            if sample.get_Prenatal_field():
                

            samples.append(sample)

            # For any samples who's sample type is "EDTA Blood," or has a classification of "Diagnostics", then set the
            # Sample Name field to <order name><accession for EDTA samples in order>
            if (sample.get_ExemplarSampleType_field() == "EDTA Blood"
                    and sample.get_Classification_field() == "Diagnostics"):
                sample_id: str = (f"{request.get_RequestName_field()}"
                                  f"{utils.add_leading_zeroes(str(num_accession_samples), 2)}")
                sample.set_SampleId_field(sample_id)
                sample.set_OtherSampleId_field(sample_id)
                num_accession_samples += 1

        request.add_children(samples)

        self.rec_man.store_and_commit()

        # add probe mix to sample if needed
        self.rel_man.load_children_of_type([request], InvoiceModel)
        invoices = request.get_children_of_type(InvoiceModel)
        self.rel_man.load_children_of_type(invoices, AssayDetailModel)
        self.rel_man.load_children_of_type(samples, ProbeMixSampleModel)
        for invoice in invoices:
            assay_details = invoice.get_children_of_type(AssayDetailModel)
            for assay_detail in assay_details:
                if assay_detail.get_ProbeMix_field():
                    for sample in samples:
                        probe_mixes = sample.get_children_of_type(ProbeMixSampleModel)
                        if not probe_mixes:
                            probe_mix = self.inst_man.add_new_record_of_type(ProbeMixSampleModel)
                            sample.add_child(probe_mix)
                            probe_mixes.append(probe_mix)
                        for probe_mix in probe_mixes:
                            probe_mix.set_ProbeMix_field(assay_detail.get_ProbeMix_field())
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def process_sample_selection(self, patient: PatientModel, request: RequestModel, result: DataRecordSelectionResult,
                                 existing_samples: List[SampleModel]) -> SapioWebhookResult:
        # retrieve records selected
        selected_samples_record_ids = [s.get(self.ORIGINAL_RECORD_ID) for s in result.selected_field_map_list]
        if len(selected_samples_record_ids) == 0:
            return PopupUtil.ok_popup("Error", "No Samples Were Selected")

        # ignore samples present in request already
        sample_ids_to_process = []
        if existing_samples:
            for sample in existing_samples:
                if sample.record_id in selected_samples_record_ids:
                    selected_samples_record_ids.remove(sample.record_id)

        if self.response_map["SOURCE"] == "Patient":
            samples: list[SampleModel] = self.get_associated_samples(patient, request)
            samples_to_be_added: list[SampleModel] = []
            for sample in samples:
                if sample.record_id in sample_ids_to_process:
                    samples_to_be_added.append(sample)
        else:
            samples_to_be_added = self.rec_handler.query_models_by_id(SampleModel, selected_samples_record_ids)

        # link request to sample
        request.add_children(samples_to_be_added)
        self.rec_man.store_and_commit()
        return SapioWebhookResult(True)

    def get_associated_samples(self, patient, request) -> [SampleModel]:
        self.rel_man.load_children_of_type([patient], RequestModel)
        patients_requests = patient.get_children_of_type(RequestModel)
        self.rel_man.load_children_of_type(patients_requests, SampleModel)
        samples: [SampleModel] = []
        for patients_request in patients_requests:
            if patients_request.record_id != request.record_id:
                associate_samples = patients_request.get_children_of_type(SampleModel)
                if associate_samples:
                    samples += associate_samples
        samples = list(set(samples))
        samples_already_present = request.get_children_of_type(SampleModel)
        if samples_already_present:
            result_samples: [SampleModel] = []
            samples_record_ids_already_present = [s.record_id for s in samples_already_present]
            for sample in samples:
                if sample.record_id not in samples_record_ids_already_present:
                    result_samples.append(sample)
            return result_samples
        else:
            return samples

    @staticmethod
    def get_used_tube_barcodes(context: SapioWebhookContext, tube_barcodes: list[str]) -> list[dict[str, str]]:
        terms: list[RawReportTerm] = []
        for tube_barcode in tube_barcodes:
            terms.append(RawReportTerm(
                SampleModel.DATA_TYPE_NAME,
                SampleModel.TUBEBARCODE__FIELD_NAME.field_name,
                RawTermOperation.EQUAL_TO_OPERATOR,
                tube_barcode,
                is_negated=False,
            ))
        column_list: list[ReportColumn] = [ReportColumn(SampleModel.DATA_TYPE_NAME, "TubeBarcode", FieldType.LONG)]
        if len(terms) > 1:
            root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeTermOperation.OR_OPERATOR, terms[1])
            for x in range(2, len(terms)):
                root_term = CompositeReportTerm(root_term, CompositeTermOperation.OR_OPERATOR, terms[x])
        else:
            root_term: RawReportTerm = terms[0]
        criteria: CustomReportCriteria = CustomReportCriteria(column_list, root_term)
        results: list[list[Any]] | None = (DataMgmtServer.get_custom_report_manager(context.user)
                                           .run_custom_report(criteria).result_table)
        if results:
            return [{"TubeBarcode": x[0]} for x in results if x[0] and x[0] != ""]
        return None

    def get_trio_samples(self, patient: PatientModel) -> list[SampleModel]:
        self.rel_man.load_children_of_type([patient], SampleModel)
        samples: list[SampleModel] = patient.get_children_of_type(SampleModel)
        family_map: dict[FamilyModel, list[SampleModel]] = (
            FamilyUtils.get_family_map(self.rec_handler, self.rel_man, samples))
        if not family_map:
            raise Exception("Patient's family is not found. Please add the patient's sample to a family.")

        families: list[FamilyModel] = list(family_map.keys())
        family_samples: list[SampleModel] = (
            FamilyUtils.get_family_samples(self.rec_handler, self.rel_man, families[0].get_FamilyId_field()))
        if not family_samples:
            raise Exception(f"Samples not found for the family id {families[0].get_FamilyId_field()}")

        return family_samples
