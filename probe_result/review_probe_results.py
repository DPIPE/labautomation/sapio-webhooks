import requests

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.processtracking.endpoints import ProcessTracking
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import ProbeResultModel
from utilities.data_models import SampleModel
from utilities.data_models import VariantResultModel
from utilities.webhook_handler import OsloWebhookHandler

class ReviewProbeResults(OsloWebhookHandler):
    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        rec_handler = RecordHandler(context)

        if not len(context.data_record_list):
            return PopupUtil.ok_popup("No Records found", "Please select at least one ProbeResult record")

        selected_results: list[ProbeResultModel] = rec_handler.wrap_models(context.data_record_list, ProbeResultModel)
        self.rel_man.load_parents_of_type(selected_results, SampleModel)

        samples_moved: dict[str, str] = {}

        breakpoint()
        for result in selected_results:
            if result.get_Status1_field() != result.get_Status2_field():
                continue

            if result.get_Status1_field() == "":
                # I don't think we want to change empty statuses
                continue

            result.set_FinalStatus_field(result.get_Status1_field())

            if "Reprocess" in result.get_Status1_field():
                reprocess_type = result.get_Status1_field().split("-")[1].strip()

                parent_sample: SampleModel | None = result.get_parent_of_type(SampleModel)
                if parent_sample:
                    samples_moved[parent_sample.get_OtherSampleId_field()] = reprocess_type

                    try:
                        ProcessTracking.assign_to_process(context, "Sample", [parent_sample], reprocess_type)
                    except requests.exceptions.HTTPError:
                        return SapioWebhookResult(False, display_text=f"No process {reprocess_type} found to assign sample {parent_sample.get_SampleId_field()} to")

            elif "Approved" in result.get_Status1_field():
                parent_sample: SampleModel | None = result.get_parent_of_type(SampleModel)
                if not parent_sample:
                    continue

                self.rel_man.load_children_of_type([parent_sample], VariantResultModel)
                sample_variant: VariantResultModel | None = parent_sample.get_child_of_type(VariantResultModel)
                if not sample_variant:
                    new_variant = self.inst_man.add_new_record_of_type(VariantResultModel)
                    new_variant.add_parent(parent_sample)

                    result.set_FinalStatus_field("Review Needed")

        self.rec_man.store_and_commit()

        if len(samples_moved.items()):
            sample_str = {f"{x}:{y}" for x, y in samples_moved.items()} 
            return PopupUtil.ok_popup("Samples moved", sample_str)
        else:
            return SapioWebhookResult(True)
