from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PrimerDesignReportModel, PrimerModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author: Austin Decker

Invocation: On-save rule, triggers when the design review signoff is complete for a Primer Design Report

Description: Creates a proper Primer record and copies all relevant fields onto it.
"""


class CreatePrimer(OsloWebhookHandler):

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        primer_design_report: PrimerDesignReportModel = self.rec_handler.wrap_model(context.data_record,
                                                                                    PrimerDesignReportModel)

        self.rel_man.load_parents_of_type([primer_design_report], PrimerModel)
        if primer_design_report.get_parents_of_type(PrimerModel):
            return SapioWebhookResult(False, "Primer already exists for this design")

        # TODO
        #  finish copying fields - or is there a better way?
        #  field name to field name maybe - would need to enforce similar names
        primer: PrimerModel = self.rec_handler.add_model(PrimerModel)
        primer.set_Gene_field(primer_design_report.get_Gene_field())
        primer.set_Chromosome_field(primer_design_report.get_Chromosome_field())
        primer.set_Strand_field(primer_design_report.get_Strand_field())
        primer.set_ExonIntron_field(primer_design_report.get_ExonIntron_field())
        primer.set_Variant_field(primer_design_report.get_Variant_field())
        primer.set_TranscriptDesignedBy_field(primer_design_report.get_TranscriptDesignedBy_field())
        primer.set_TranscriptInSeqPilot_field(primer_design_report.get_Transcript_field())
        primer.set_ChromosomalGRS_field(primer_design_report.get_ChromosomalGRS_field())
        primer.set_ChromosomalGRS38_field(primer_design_report.get_ChromosomalGRS38_field())
        primer.set_TranscriptDifferences_field(primer_design_report.get_TranscriptDifferences_field())
        primer.set_MANESelect_field(primer_design_report.get_MANESelect_field())
        primer.set_Pseudogene_field(primer_design_report.get_Pseudogene_field())
        primer.set_Amplicon_field(primer_design_report.get_Amplicon_field())
        primer.set_FPrimerName_field(primer_design_report.get_FPrimerName_field())
        primer.set_FPrimerSequence_field(primer_design_report.get_FPrimerSequence_field())
        primer.set_FPrimerSequenceWithMod_field(primer_design_report.get_FPrimerSequenceWithMod_field())
        primer.set_RPrimerName_field(primer_design_report.get_RPrimerName_field())
        primer.set_RPrimerSequence_field(primer_design_report.get_RPrimerSequence_field())
        primer.set_RPrimerSequenceWithMod_field(primer_design_report.get_RPrimerSequenceWithMod_field())

        primer_design_report.add_parent(primer)

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, directive=FormDirective(primer.get_data_record()))
