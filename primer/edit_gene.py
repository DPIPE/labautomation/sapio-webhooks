import json
from typing import cast, Any

from sapiopycommons.general.popup_util import PopupUtil
from sapiopycommons.recordmodel.record_handler import RecordHandler
from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.pojo.CustomReport import ReportColumn, RawReportTerm, RawTermOperation, CustomReportCriteria, \
    CustomReport, CompositeReportTerm, CompositeTermOperation
from sapiopylib.rest.pojo.datatype.FieldDefinition import FieldType
from sapiopylib.rest.pojo.webhook import ClientCallbackResult
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import InputSelectionRequest
from sapiopylib.rest.pojo.webhook.ClientCallbackResult import FormEntryDialogResult, InputSelectionResult
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookEnums import SearchType
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PrimerModel
from utilities.webhook_handler import OsloWebhookHandler

STATUS: str = "status"
STATUS_ERROR: str = "error"
STATUS_GENE_PROMPT: str = "genePrompt"
STATUS_PRIMER_SELECT: str = "primerSelect"

GENE: str = "gene"

PRIMER_FIELDS: list[str] = [PrimerModel.SEQPILOTTRANSCRIPT__FIELD_NAME.field_name,
                            PrimerModel.MANESELECT__FIELD_NAME.field_name,
                            PrimerModel.CHROMOSOME__FIELD_NAME.field_name, PrimerModel.STRAND__FIELD_NAME.field_name,
                            PrimerModel.PSEUDOGENE__FIELD_NAME.field_name,
                            PrimerModel.DIFFERENCESBETWEENTRANSCRIPT__FIELD_NAME.field_name]


class EditGene(OsloWebhookHandler):
    rec_handler: RecordHandler
    response_map: dict[str, str]

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.rec_handler = RecordHandler(context)
        self.response_map = {STATUS: ""}

        result: ClientCallbackResult = context.client_callback_result
        if result:
            if result.user_cancelled:
                return SapioWebhookResult(True)
            self.response_map = json.loads(result.callback_context_data)
            if self.response_map[STATUS] == STATUS_ERROR:
                return SapioWebhookResult(True)
            if self.response_map[STATUS] == STATUS_GENE_PROMPT:
                return self.process_gene_prompt(context, cast(FormEntryDialogResult, result),
                                                self.rec_handler.wrap_model(context.data_record, PrimerModel))
            if self.response_map[STATUS] == STATUS_PRIMER_SELECT:
                return self.process_primer_selection(self.rec_handler.wrap_model(context.data_record, PrimerModel),
                                                     cast(InputSelectionResult, result))

        # Prompt the user to enter the new gene.
        self.response_map[STATUS] = STATUS_GENE_PROMPT
        return PopupUtil.string_field_popup("Edit Gene", "Enter the new gene.", "Gene",
                                            request_context=json.dumps(self.response_map))

    def process_gene_prompt(self, context: SapioWebhookContext, result: FormEntryDialogResult,
                            primer: PrimerModel) -> SapioWebhookResult:
        # Get the gene entered by the user.
        user_response_map: dict[str, Any] | None = result.user_response_map
        if not user_response_map:
            return SapioWebhookResult(True)
        gene: str | None = user_response_map["Gene"]
        if not gene or gene == "":
            return SapioWebhookResult(True)

        # Check for any primer records that have the same gene as the user entered, and prompt the user to select one
        # to copy data from. If there aren't any matches, just set the gene field and return a webhook result of True.
        custom_report: CustomReport = self.get_matching_primers(context.user, primer, gene)
        if not custom_report.result_table:
            primer.set_Gene_field(gene)

            self.rec_man.store_and_commit()

            return SapioWebhookResult(True)

        # Create an InputDialogRequest to prompt the user to select matching primers. Save the gene entered by the user
        # in our response data so that we can use it later.
        self.response_map[GENE] = gene
        self.response_map[STATUS] = STATUS_PRIMER_SELECT
        input_selection_request: InputSelectionRequest = (
            InputSelectionRequest(PrimerModel.DATA_TYPE_NAME,
                                  f"Other primers with the \"{gene}\" gene were found. You can either select an existing"
                                  f" primer to copy data from, or click \"OK\"", [SearchType.QUICK_SEARCH],
                                  show_only_key_fields=False, custom_search=custom_report,
                                  callback_context_data=json.dumps(self.response_map)))
        return SapioWebhookResult(True, client_callback_request=input_selection_request)

    def process_primer_selection(self, primer: PrimerModel, result: InputSelectionResult) -> SapioWebhookResult:
        # Copy fields down from the matching primer to the current primer if the user selected one.
        if result.selected_record_list:
            matching_primer: PrimerModel = self.rec_handler.wrap_model(result.selected_record_list[0], PrimerModel)
            for field in PRIMER_FIELDS:
                primer.set_field_value(field, matching_primer.get_field_value(field))

        # Set the gene field on the current primer.
        primer.set_Gene_field(self.response_map[GENE])

        self.rec_man.store_and_commit()

        return SapioWebhookResult(True)

    def get_matching_primers(self, user: SapioUser, primer: PrimerModel, gene: str) -> CustomReport:
        columns: list[ReportColumn] = [ReportColumn(PrimerModel.DATA_TYPE_NAME,
                                                    PrimerModel.PRIMERID__FIELD_NAME.field_name, FieldType.STRING)]
        columns.extend([ReportColumn(PrimerModel.DATA_TYPE_NAME, field, FieldType.STRING) for field in PRIMER_FIELDS])

        terms: list[RawReportTerm] = [RawReportTerm(PrimerModel.DATA_TYPE_NAME, PrimerModel.GENE__FIELD_NAME.field_name,
                                                    RawTermOperation.EQUAL_TO_OPERATOR, gene),
                                      RawReportTerm(PrimerModel.DATA_TYPE_NAME,
                                                    PrimerModel.PRIMERID__FIELD_NAME.field_name,
                                                    RawTermOperation.NOT_EQUAL_TO_OPERATOR,
                                                    primer.get_PrimerId_field())]
        root_term: CompositeReportTerm = CompositeReportTerm(terms[0], CompositeTermOperation.AND_OPERATOR, terms[1])

        criteria: CustomReportCriteria = CustomReportCriteria(columns, root_term)
        return DataMgmtServer.get_custom_report_manager(user).run_custom_report(criteria)
