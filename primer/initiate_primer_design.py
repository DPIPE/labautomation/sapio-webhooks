from sapiopycommons.callbacks.callback_util import CallbackUtil
from sapiopylib.rest.pojo.webhook.ClientCallbackRequest import PopupType
from sapiopylib.rest.pojo.webhook.WebhookContext import SapioWebhookContext
from sapiopylib.rest.pojo.webhook.WebhookDirective import FormDirective
from sapiopylib.rest.pojo.webhook.WebhookResult import SapioWebhookResult

from utilities.data_models import PrimerDesignReportModel
from utilities.webhook_handler import OsloWebhookHandler

"""
Author: Austin Decker

Invocation: Multi data type toolbar button

Description: Sets appropriate flags on the selected Primer Design Report record to remove it from the dashboard
and enable it for editing. Also prompts the user to select a specific gene and transcript based on the information
on the related assay detail.
"""


class InitiatePrimerDesign(OsloWebhookHandler):
    callback_util: CallbackUtil

    def execute(self, context: SapioWebhookContext) -> SapioWebhookResult:
        self.callback_util = CallbackUtil(context)

        if len(context.selected_field_map_index_list) != 1:
            self.callback_util.toaster_popup("You must select one primer design report.", popup_type=PopupType.Warning)
            return SapioWebhookResult(True)

        field_map = context.field_map_list[0]
        record_id = field_map.get("PrimerDesignReport|RecordId")

        primer_design_report_list = self.rec_handler.query_models_by_id(PrimerDesignReportModel, [record_id])
        if len(primer_design_report_list) != 1:
            return SapioWebhookResult(False,
                                      f"Somehow, unable to find primer design report record from selected record ID: "
                                      f"{record_id}")

        primer_design_report = primer_design_report_list[0]

        gene = primer_design_report.get_Gene_field().strip()
        if not gene:
            gene = self.callback_util.string_input_dialog("", "Enter gene to look up", "Gene")

        primer_design_report.set_DesignStarted_field(True)
        primer_design_report.set_IsLocked_field(False)
        self.rec_man.store_and_commit()

        return SapioWebhookResult(True, directive=FormDirective(primer_design_report.get_data_record()))
